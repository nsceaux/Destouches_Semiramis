\newBookPart #'()
\act "ACTE III"
\sceneDescription\markup\wordwrap-center {
  Le Théâtre représente une vestibule orné de Statues des Rois de Babylone.
}
\scene "Scene Premiere" "Scene I"
\sceneDescription\markup\wordwrap-center {
  \smallCaps Zoroastre seul.
}
% 3-1
\pieceToc\markup\wordwrap {
  Zoroastre :
  \italic { Qu’ai-je appris ! quels forfaits ! quelle injure mortelle }
}
\includeScore "CAAzoroastre"
\newBookPart #'(full-rehearsal)

\scene "Scene Deuxiéme" "Scene II"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Zoroastre, Semiramis.
}
% 3-2
\pieceToc\markup\wordwrap {
  Zoroastre, Semiarmis :
  \italic { Ah ! Perfide, osez-vous soûtenir ma présence }
}
\includeScore "CBAsemiramisZoroastre"
\newBookPart #'(full-rehearsal)

\scene "Scene Troisiéme" "Scene III"
\sceneDescription\markup\wordwrap-center {
  \smallCaps Zoroastre, seul.
}
% 3-3
\pieceToc\markup\wordwrap {
  Zoroastre :
  \italic { Pour tant de maux soufferts, quels maux dois-je lui rendre ? }
}
\includeScore "CCAcoupe"
\newBookPart #'(full-rehearsal)

\scene "Scene Quatriéme" "Scene IV"
\sceneDescription\markup\wordwrap-center {
  \smallCaps Zoroastre, Troupe de Demons, de Magiciens & de Magiciennes.
}
% 3-4 coupé
\markup\pad-around#5 [coupe]
% 3-5
\pieceTocNb "3-5" "Premier air pour les Magiciens"
\includeScore "CDBcoupe"
% 3-6
\pieceTocNb "3-6" \markup\wordwrap {
  Zoroastre : \italic { Semiramis a trahi mon ardeur }
}
\includeScore "CDCzoroastre"
\newBookPart #'(full-rehearsal)
% 3-7
\pieceTocNb "3-7" \markup\wordwrap {
  Chœur : \italic { Versons l’épouvante dans les cœurs }
}
\includeScore "CDDcoupes"
\newBookPart #'(full-rehearsal)
% 3-8
\pieceTocNb "3-8" "Deuxiéme air"
\includeScore "CDEcoupe"
\newBookPart #'(full-rehearsal)
% 3-9 coupé
% 3-10 coupé
\markup\pad-around#5 [coupe]
% 3-11
\pieceTocNb "3-11" \markup\wordwrap {
  Zoroastre : \italic { Arrêtez. Les Enfers sont prêts à m'inspirer }
}
\includeScore "CDHzoroastre"
% 3-12 coupé
\markup\pad-around#5 [coupe]
