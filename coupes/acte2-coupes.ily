\newBookPart #'()
\act "ACTE II"
\sceneDescription\markup\wordwrap-center {
  Le Théâtre représente l’avant-cour du Palais de Semiramis.
  On voit un Temple dans l’éloignement d’un des côtez.
}
\scene "Scene Premiere" "Scene I"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Arsane, Amestris.
}
% 2-1
\pieceToc\markup\wordwrap {
  Arsane, Amestris : \italic { Non, ne craignez point de m’entendre }
}
\includeScore "BAAcoupe"
\newBookPart #'(full-rehearsal)

\scene "Scene Deuxiéme" "Scene II"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Semiramis, Arsane.
}
%% 2-2
\pieceToc\markup\wordwrap {
  Semiramis, Arsane : \italic { Ou courez-vous Arsane ? }
}
\includeScore "BBArecit"
\newBookPart #'(full-rehearsal)

\scene "Scene Troisiéme" "Scene III"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Zoroastre, Semiramis.
}
%% 2-3
\pieceToc "Marche"
\includeScore "BCAmarche"
\newBookPart #'(basse-continue)
%% 2-4
\pieceToc\markup\wordwrap {
  Zoroastre, Semiramis : \italic {
    Belle Semiramis, l’amour & l’esperance
  }
}
\includeScore "BCBzoroastreSemiramis"
\newBookPart #'(full-rehearsal)

\scene "Scene Quatriéme" "Scene IV"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    Le Théâtre change, & représente les célébres Jardins de Semiramis.
  }
  \wordwrap-center {
    \smallCaps { Zoroastre, Semiramis, }
    Troupes de Genies Elementaires, & de Peuples.
  }
}
%% 2-5
\pieceToc "Air"
\includeScore "BDArondeau"
\newBookPart #'(full-rehearsal)
%% 2-6
\pieceToc\markup\wordwrap {
  Air. Un Genie : \italic { L’Art plus prompt que la Nature }
}
\includeScore "BDBair"
%% 2-7
\pieceToc\markup\wordwrap {
  Zoroastre, chœur : \italic { Formez les plus tendres concerts }
}
\includeScore "BDCzoroastreChoeur"
\newBookPart #'(full-rehearsal)
%% 2-8
\pieceToc "Premier air"
\includeScore "BDDair"
%% 2-9
\pieceToc "Deuxiéme air"
\includeScore "BDEair"
\newBookPart #'(full-rehearsal)
%% 2-10 coupé
%% 2-11 coupé
%% 2-12 coupé
%% 2-13 coupé
%% 2-14 coupé
%% 2-15 coupé
%% 2-16 coupé
\markup\pad-around#5 [coupe]
%% 2-17
\pieceTocNb "2-17" \markup\wordwrap {
  Zoroastre, Semiramis :
  \italic { De vos nouveaux Sujets voyez quelle est l’ardeur }
}
\includeScore "BDMrecit"
\newBookPart #'(full-rehearsal)

\scene "Scene Cinquiéme" "Scene V"
\sceneDescription\markup\wordwrap-center\smallCaps { Zoroastre. }
%% 2-18
\pieceTocNb "2-18" \markup\wordwrap {
  Zoroastre :
  \italic { Qu’ai-je entendu ? quel soupçon ! quel effroi }
}
\includeScore "BEArecit"
%% 2-19
\pieceTocNb "2-19" "Entr’acte"
\reIncludeScore "BDHair" "BEBair"
\includeScore "BECair"
