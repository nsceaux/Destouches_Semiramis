\notesSection "Livret"
\markuplist\with-line-width-ratio #0.7 {
\livretAct\larger LIVRET
\livretAct AVERTISSEMENT
\livretParagraph {
  Il est peu de Noms plus celebres que celui de Semiramis. Tous les
  Auteurs ont parlé de son ambition, de sa magnificence & de sa
  mort. Elle périt par la main de son Fils pour qui elle avoit conçû
  une passion criminelle. C’est cet évenement qu’on met sur la
  Scene. On a cherché pour l’amener, les moyens les moins odieux & les
  plus interessans.
}
\livretParagraph {
  On feint que le Ciel est irrité des crimes de Semiramis, qui menacée
  d’être tuée par son Fils, l’avoit fait exposer au moment de sa
  naissance. Maîtresse du Trône, elle y veut placer Arsane jeune
  inconnu qu’elle aime, & en éloigner Amestris sa Niéce, héritiere de
  l’Empire. Elle l’oblige à se consacrer au culte des Dieux, & se sert
  du prétexte de les appaiser, par le choix d’une Prestresse du Sang
  Royal. Le Ciel n’y consant pas ; il veut une Victime. L’Amibiguïté
  des Oracles, si conforme aux détours par lesquels il conduit ses
  vangeances, fait tomber l’aparence du peril sur Amestris. C’est pour
  la délivrer qu’Arsane son Amant fait des efforts qui aboutissent,
  malgré lui, à la mort de Semiramis. Outre le soin qu’on a pris de
  cacher au Fils & à la Mere ce qu’ils sont l’un & l’autre, on a
  rejetté une partie de l’action sur Zoroastre Roy de la Bactriane,
  inventeur de la Magie, Contemporain de Semiramis & trahi par
  elle. Il rend Arsane furieux ; & le désespoir de l’un & le trouble
  de l’autre, servent à executer l’Arrest du Ciel contre la Reine.
}
\livretParagraph {
  Les remords dont elle combat sa passion, ceux qu’elle témoigne en
  reconnoissant son Fils, & en mourant, sont les secours par lesquels
  le Théâtre concilie la pitié aux personnages les plus coupables.
}
\livretParagraph {
  A l’égard d’Amestris, sa consécration n’est pas une idée contraire à
  la vrai-semblance, puisque tant d’Auteurs Sacrez & Profanes assurent
  que longtems avant les Vestales de Rome, l’Idolatrie avoir dévoüé
  des Vierges au service des Autels. On a choisi les circonstances
  dans lesquelles la Princesse se dévoüe sortie d’une longue
  captivité, liée par un serment & par la necessité du bonheur public,
  elle sacrifie ses droits à la Couronne, & une passion
  légitime. Enfin, ses malheurs sont réparez, & sa vertu récompensée.
}
\livretAct\wordwrap-center {
  ACTEURS DE LA TRAGEDIE
}
\fill-line {
  \column {
    \line {
      \character-ambitus #"vbas-dessus" ##{ re' la'' #}
      \smallCaps Semiramis, Reine de Babylone.
    }
    \line {
      \character-ambitus #"vbas-dessus" ##{ re' la'' #}
      \smallCaps Amestris, Princesse du Sang Royal.
    }
    \line {
      \character-ambitus #"vhaute-contre" ##{ fa si' #}
      \smallCaps Arsane ou \smallCaps Ninus, Fils de \smallCaps Semiramis,
      Amant \concat { d’ \smallCaps Amestris. }
    }
    \line {
      \character-ambitus #"vbasse" ##{ sol, fa' #}
      \smallCaps Zoroastre, Roy de la Bactriane,
      Amant de \smallCaps Semiramis.
    }
    \line {
      \character-ambitus #"vhaute-contre" ##{ sol si' #}
      \smallCaps { Un Babylonien. }
    }
    \line {
      \character-ambitus #"vbas-dessus" ##{ fa' fa'' #}
      \smallCaps { Une Babylonienne. }
    }
    \line {
      \character-ambitus #"vbas-dessus" ##{ re' sol'' #}
      \smallCaps { Une Prestresse de Jupiter. }
    }
    \line {
      \character-ambitus #"vbasse" ##{ do mi' #}
      \smallCaps { L'Ordonnateur } des Jeux funebres.
    }
    \line {
      \character-ambitus #"vhaute-contre" ##{ sol si' #}
      \smallCaps { Un Génie. }
    }
    \concat {
      \left-brace#100 \raise#9.5 \line {
        \column {
          \character-ambitus #"vbas-dessus" ##{ re' la'' #}
          \character-ambitus #"vhaute-contre" ##{ la do'' #}
          \character-ambitus #"vtaille" ##{ do sol' #}
          \character-ambitus #"vbasse" ##{ fa, fa' #}
        }
        \override #'(baseline-skip . 5) \column {
          \line { Chœur de Babyloniens. }
          \line { Chœur de Genies Elementaires. }
          \line { Chœur de Demons & de Magiciens. }
          \line { Chœur de Prêtres & Prêtresses de \smallCaps Jupiter. }
          \line {
            Chœur de Peuples pour les Jeux funestes de \smallCaps Ninus.
          }
        }
      }
    }
  }
}
}
\markuplist\page-columns {

\livretAct ACTE I
\livretDescAtt\wordwrap-center {
  Le Théâtre représente un grand Sallon orné pour le Couronnement
  \concat { d’ \smallCaps Arsane } & de ses Nôces avec
  \smallCaps Semiramis.
}
\livretScene SCENE PREMIERE
\livretPers Semiramis
\livretRef #'AABsemiramis
\livretVerse#8 { Pompeux Aprêts, Fête éclatante, }
\livretVerse#10 { Flambeaux sacrez, Autels ornez de fleurs, }
\livretVerse#8 { Hymen si cher à mon attente, }
\livretVerse#8 { Que vous m’allez couter de pleurs ! }

\livretVerse#12 { Rivale des Heros, que devient ma puissance ? }
\livretVerse#12 { Avec un Inconnu, j’en partage l’éclat ; }
\livretVerse#12 { Je la mets à ses pieds, ma gloire s’en offense, }
\livretVerse#12 { Et mon amour encor craint de faire un ingrat. }

\livretVerse#8 { Pompeux Aprêts, Fête éclatante, }
\livretVerse#10 { Flambeaux sacrez, Autels ornez de fleurs, }
\livretVerse#8 { Hymen si cher à mon attente, }
\livretVerse#8 { Que vous m’allez couter de pleurs ! }

\livretVerse#12 { Quels reproches Ninus, n’as-tu point à me faire ? }
\livretVerse#12 { A périr en naissant, j’ai condamné mon Fils. }
\livretVerse#12 { Pour éteindre la race, & les droits de ton frere, }
\livretVerse#8 { Aux Autels j’enchaîne Amestris : }
\livretVerse#8 { Et c’est une main étrangere, }
\livretVerse#12 { Qui de mes attentats va recueillir le prix. }
\livretVerse#6 { Triste Semiramis, }
\livretVerse#8 { Faut-il que ton cœur te trahisse ? }
\livretVerse#12 { Plus cruels que les dieux qui désolent ces bords, }
\livretVerse#8 { L’Amour te guide au precipice. }
\livretVerse#12 { Arrête. Il n’est plus temps. quels combats ! quels remords ? }
\livretVerse#12 { Justifiez, grands Dieux, ou calmez mes transports. }
\livretVerse#12 { On vient. C’est Amestris... quelle est mon injustice ! }
\livretVerse#12 { Captive dès long-tems, quels maux elle a soufferts ! }
\livretVerse#8 { Je ne fais que changer ses fers. }

\livretScene SCENE DEUXIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Amestris, Semiramis.
}
\livretPers Amestris
\livretRef #'ABBas
\livretVerse#12 { Reine, je vais remplir le destin qui m’appelle ; }
\livretVerse#10 { A mon serment vous me verrez fidelle. }
\livretVerse#12 { Tandis que vous suivrez les traces des Heros, }
\livretVerse#8 { Dans une retraite éternelle, }
\livretVerse#8 { Mon cœur va chercher son repos. }
\livretPers Semiramis
\livretVerse#12 { Au repos de ces lieux vôtre cœur s’interesse, }
\livretVerse#12 { Nôtre félicité ne dépend que de vous, }
\livretVerse#12 { Fille de Jupiter, vous serez sa Prêtresse, }
\livretVerse#12 { Ses faveurs par vos mains vont descendre sur nous. }
\livretVerse#12 { Babylonne est l’objet du couroux qui l’anime, }
\livretVerse#12 { Et ce Dieu tant de fois invoqué par mes pleurs, }
\livretVerse#12 { Demande dès long-tems une grande victime ; }
\livretVerse#12 { Je la cherchois en vain : vôtre effort magnanime, }
\livretVerse#12 { Sans nous couter du sang, finira nos malheurs. }
\livretPers Amestris
\livretVerse#12 { J’offre à nos Dieux des jours trop peu dignes d’envie. }
\livretPers Semiramis
\livretVerse#12 { Ces Dieux en ont-ils fait de plus heureux pour moi ? }
\livretVerse#12 { Aux loix d’un Inconnu je vais être asservie, }
\livretVerse#12 { Arsane en ce moment va devenir mon Roi. }
\livretPers Amestris
\livretVerse#12 { Zoroastre esperoit recevoir vôtre foi. }
\livretPers Semiramis
\livretVerse#12 { Amestris, malgré-moi, je lui fais cette offense. }
\livretPers Amestris
\livretVerse#12 { Eh ! ne craignez-vous point les traits de sa vengeance ? }
\livretVerse#12 { Zoroastre commande à cent peuples divers, }
\livretVerse#12 { C’est peu que sa valeur ait fait trembler la Terre, }
\livretVerse#12 { Ce nouveau Conquerant à dompté les Enfers ; }
\livretVerse#12 { Les secrets de l’Olympe à ses yeux sont ouverts : }
\livretVerse#12 { Sa voix force les Dieux à lancer le Tonnerre. }
\livretPers Semiramis
\livretVerse#12 { Arsane est adoré du peuple & des soldats, }
\livretVerse#12 { Dans l’âge où l’on commence à s’instruire aux combats, }
\livretVerse#8 { Il est maître de la Victoire. }
\livretVerse#12 { J’ai vû tous nos Guerriers, soutenus par son bras, }
\livretVerse#12 { Ranimer leur ardeur à l’éclat de sa gloire. }
\livretPers Amestris
\livretVerse#12 { Puisse-t’il de nos maux effacer la memoire ! }
\livretPers Semiramis
\livretVerse#8 { Je vais amener vôtre Roi : }
\livretVerse#12 { Partagez les honneurs de cette auguste fête, }
\livretVerse#12 { Le sang, qui nous unit, vous en fait une loi. }
\livretVerse#12 { Que du Bandeau sacré vos main ornent sa Tête. }

\livretScene SCENE TROISIÉME
\livretPers Amestris
\livretRef #'ACAamestris
\livretVerse#12 { Mes yeux, mes tristes yeux, laissez couler vos larmes, }
\livretVerse#8 { Foible secours des malheureux. }
\livretVerse#12 { Faut-il que d’un amour, dont j’ai bravé les feux, }
\livretVerse#12 { L’importun souvenir me cause tant d’allarmes ? }
\livretVerse#8 { Contre des maux si rigoureux }
\livretVerse#12 { Faut-il que mes soupirs soient mes uniques armes ? }
\livretVerse#8 { Foible secours des malheureux, }
\livretVerse#12 { Mes yeux, mes tristes yeux, laissez couler vos larmes. }
\livretVerse#12 { Reine barbare, non, mes fers, ny ta rigueur, }
\livretVerse#8 { N’ont point ébranlé ma constance : }
\livretVerse#12 { Arsane, c’est pour toi que j’ai craint sa fureur. }
\livretVerse#12 { Arsane, c’est à toi, qu’ingratte en apparence, }
\livretVerse#12 { Je vais donner l’Empire au défaut de mon cœur. }

\livretScene SCENE QUATRIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Arsane, Amestris.
}
\livretPers Arsane
\livretRef #'ADArecit
\livretVerse#12 { Vous, Princesse, en ces lieux ! quel sort vous y ramene ? }
\livretPers Amestris
\livretVerse#12 { J’y viens être témoin du beau jour qui vous luit. }
\livretPers Arsane
\livretVerse#12 { Que l’éclat de ce jour & me trouble & me gêne ! }
\livretPers Amestris
\livretVerse#12 { De vos nobles travaux vous recevez le fruit. }
\livretPers Arsane
\livretVerse#12 { Ingrate, ignorez-vous quel effort m’y reduit ? }
\livretPers Amestris
\livretVerse#12 { L’Hymen qui d’une main vous prépare sa chaîne, }
\livretVerse#12 { Vous présente de l’autre un Empire éclatant }
\livretVerse#12 { Comblé de tant d’honneurs, n’êtes-vous pas content ? }
\livretPers Arsane
\livretVerse#12 { Mon cœur ne l’eût été qu’à vaincre vôtre haine. }
\livretVerse#12 { Je n’adorois que vous ; tant de soins, tant de pleurs, }
\livretVerse#12 { De si tendres soupirs, une ardeur si sincere, }
\livretVerse#8 { Rien n’a pû fléchir vos rigueurs : }
\livretVerse#12 { C’est vous qui m’enchaînez à ces tristes honneurs, }
\livretVerse#12 { Et je vais me punir de n’avoir pu vous plaire. }
\livretPers Amestris
\livretVerse#12 { Adieu, Seigneur… }
\livretPers Arsane
\livretVerse#12 { \transparent { Adieu, Seigneur… } Mon cœur plus que jamais épris… }
\livretPers Amestris
\livretVerse#10 { Adieu, Seigneur, oubliez Amestris. }
\livretPers Arsane
\livretVerse#9 { Vous me fuyez… arrêtez Inhumaine. }
\livretPers Amestris
\livretVerse#8 { Non, non, d’autres destins m’appellent. }
\livretPers Arsane
\livretVerse#3 { Quel mépris. }
\livretPers Amestris
\livretVerse#12 { Je n’écoute plus rien, je vais suivre la Reine. }

\livretScene SCENE CINQUIÉME
\livretPers Arsane
\livretRef #'AEAarsane
\livretVerse#12 { Je vous entends Cruelle, & je perds tout espoir. }
\livretVerse#12 { Dieux ! si d’un sang obscur j’ai reçû la naissance, }
\livretVerse#12 { Deviez-vous au trépas arracher mon enfance ? }
\livretVerse#12 { C’en est trop. Recevons le suprême pouvoir, }
\livretVerse#8 { Par ma vertu mon nom commence. }

\livretScene SCENE SIXIÉME
\livretDescAtt\wordwrap-center {
  \smallCaps { Semiramis, Arsane, Amestris, }
  et les Peuples de Babilonne.
}
\livretPers Semiramis
\livretRef #'AFBrecit
\livretVerse#12 { Enfin, voici l’instant si cher à mes souhaits ! }
\livretVerse#12 { Venez, jeune Heros, venez, que mes sujets }
\livretVerse#12 { Vous placent sur le Trône, où vous auriez dû naître, }
\livretVerse#12 { Et dans leur Défenseur reconnoissent leur Maître. }
\livretPers Arsane
\livretVerse#12 { Croirai-je qu’en ce jour ces Peuples redoutez, }
\livretVerse#13 { Aux loix d’un Inconnu, sans murmure obéïssent ? }
\livretVerse#12 { Plus je vous vois pour moi prodiguer vos bontez, }
\livretVerse#8 { Plus mes esprits sont agitez. }
\livretVerse#8 { Peut-être les Dieux me punissent }
\livretVerse#12 { D’usurper des honneurs que j’ai peu meritez. }
\livretPers Semiramis
\livretVerse#10 { Vôtre valeur ardente à nous défendre, }
\livretVerse#12 { Révele en vous le sang, ou des Rois, ou des Dieux. }
\livretVerse#12 { Et quand je vous éleve à ce rang glorieux }
\livretVerse#12 { Je crois vous le donner bien moins que vous le rendre. }
\livretVerse#12 { Chantez, Peuples, chantez, réünissez vos voix, }
\livretVerse#12 { Celebrez ce Heros, aplaudissez mon choix. }
\livretRef #'AFDchoeur
\livretVerse#12 { Nous recevons un Roi des mains de la Victoire, }
\livretVerse#12 { Qu’il répande sur nous mille nouveaux bienfaits, }
\livretVerse#12 { Qu’il regne, qu’il nous donne une éternelle paix ; }
\livretVerse#12 { Que les Dieux immortels ne séparent jamais }
\livretVerse#8 { Et nôtre bonheur & sa gloire. }
\livretRef #'AFEchaconne
\livretDidasPPage On danse.
\livretPers Une Babilonienne
\livretRef #'AFFbabyloniens
\livretVerse#6 { Dieu charmant de Cythere, }
\livretVerse#5 { Répand tes faveurs ; }
\livretVerse#6 { Et du soin de te plaire }
\livretVerse#5 { Rempli tous nos cœurs. }
\livretVerse#3 { De tes flâmes }
\livretVerse#2 { Nos ames }
\livretVerse#5 { Sentent les douceurs. }
\livretVerse#3 { Plus de peines, }
\livretVerse#2 { Tes chaînes }
\livretVerse#5 { Sont faites de fleurs. }
\livretPers Chœur
\livretVerse#6 { Dieu charmant de Cythere, }
\livretVerse#5 { Répand tes faveurs ; }
\livretVerse#6 { Et du soin de te plaire }
\livretVerse#5 { Rempli tous nos cœurs. }
\livretVerse#3 { De tes flâmes }
\livretVerse#2 { Nos ames }
\livretVerse#5 { Sentent les douceurs. }
\livretVerse#3 { Plus de peines, }
\livretVerse#2 { Tes chaînes }
\livretVerse#5 { Sont faites de fleurs. }
\livretPers Un Babilonien
\livretVerse#6 { De la grandeur suprême }
\livretVerse#5 { Les Dieux sont jaloux ; }
\livretVerse#6 { Mais l’Amour est le même }
\livretVerse#5 { Pour eux & pour nous. }
\livretVerse#6 { Trop aimable Jeunesse, }
\livretVerse#5 { Craignez moins ses coups : }
\livretVerse#6 { Si ce Dieu ne vous blesse, }
\livretVerse#5 { Quel bien goûtez-vous ? }
\livretDidasPPage On danse.
\livretPers Un Babilonien & une Babilonienne
\livretVerse#3 { Doux Empire, }
\livretVerse#7 { Dont les loix sont nos desirs, }
\livretVerse#3 { Quel martyre }
\livretVerse#7 { De résister aux plaisirs ! }
\livretVerse#3 { Qu’on soupire ! }
\livretVerse#4 { On ne respire }
\livretVerse#3 { Que du jour, }
\livretVerse#3 { Où l’Amour }
\livretVerse#3 { Nous inspire. }
\livretVerse#3 { Doux Empire, }
\livretVerse#7 { Dont les loix sont nos desirs, }
\livretVerse#3 { Quel martyre }
\livretVerse#7 { De résister aux plaisirs ! }
\livretPers Chœur
\livretVerse#6 { Celebrons tous tes charmes, }
\livretVerse#5 { Ranime nos voix, }
\livretVerse#6 { Loin de nous tes allarmes. }
\livretVerse#5 { Chantons mille fois : }
\livretVerse#6 { Tendre Amour, de tes armes }
\livretVerse#5 { Laisse-nous le choix. }
\livretVerse#6 { Sans ennuis & sans larmes }
\livretVerse#5 { Vivons sous tes loix. }
\livretPers Un Babilonien & une Babilonienne
\livretVerse#6 { Pour prix de ta victoire }
\livretVerse#5 { Rend nos cœurs contens. }
\livretPers Chœur
\livretVerse#9 { Quel triomphe ! que d’heureux instants ! }
\livretVerse#9 { Quelle gloire ! quels transports charmants ! }

\livretPers Semiramis
\livretRef #'AFGrecit
\livretVerse#12 { C’est assez. Il est tems d’achever mon ouvrage ; }
\livretVerse#12 { Amestris, approchez, faites vôtre devoir. }
\livretPers Amestris
\livretVerse#8 { Seigneur, du suprême pouvoir }
\livretVerse#10 { C’est donc à moi de vous offrir le gage. }
\livretVerse#12 { Mon sang m’avoit donné des droits sur vos Etats ; }
\livretVerse#12 { Vivez heureux, regnez, je n’en murmure pas. }
\livretVerse#12 { Joüissez à jamais de la faveur celeste ; }
\livretVerse#12 { Et recevez mes vœux, c’est tout ce qui me reste... }
\livretPers Arsane
\livretVerse#12 { Quel présent ! quelle main vient ici me l’offrir ! }
\livretDidasP\line { \hspace#30 à part. }
\livretVerse#12 { Genereuse Amestris... Non, dussai-je perir... }

\livretRef #'AFHtonnerre
\livretDidasPPage\line { L’Autel est brisé par le Tonnerre. }
\livretPers Semiramis & Arsane
\livretVerse#12 { Quel tourbillon de feux s’éleve & nous sépare ? }
\livretVerse#8 { Quelle horreur ! quels mugissemens ! }
\livretVerse#12 { La terre tremble, s’ouvre & montre le Tenare, }
\livretVerse#8 { Le Ciel confond les Elemens. }
\livretPers Chœur
\livretVerse#12 { Quels déluges brûlans tombent de toutes parts ? }
\livretVerse#10 { Tu fuis Soleil, tu fuis ! quel est le crime }
\livretVerse#8 { Qui te dérobe à nos regards ? }
\livretVerse#8 { Ciel, que voulez-vous pour victime ? }
\livretPers Semiramis
\livretVerse#12 { Tout l’Olympe en couroux s’arme-t’il contre moi ? }
\livretVerse#12 { Dieux, me punissez-vous d’avoir trahi ma foi ? }
\sep
%\column-break

\livretAct ACTE II
\livretDescAtt\wordwrap-center {
  Le Théâtre représente l’avant-cour du Palais de Semiramis.
  On voit un Temple dans l’éloignement d’un des côtez.
}
\livretScene SCENE PREMIERE
\livretRef #'BAAarsaneAmestris
\livretDescAtt\wordwrap-center\smallCaps {
  Arsane, Amestris.
}
\livretPers Ensemble
\livretPersVerse Arsane \line {
  \livretVerse#8 { Non, ne craignez point de m’entendre. }
}
\livretPersVerse Amestris \line {
  \column {
    \livretVerse#8 { Non, je ne veux point vous entendre. }
    \livretVerse#12 { Les Dieux sont en couroux, songez à les calmer. }
  }
}
\livretPers Arsane
\livretVerse#8 { C’est vous que je dois désarmer ; }
\livretVerse#8 { J’ai trop de graces à vous rendre. }
\livretPers Ensemble
\livretPersVerse Arsane \line {
  \livretVerse#8 { Non, ne craignez point de m’entendre. }
}
\livretPersVerse Amestris \line {
  \livretVerse#8 { Non, je ne veux point vous entendre. }
}
\livretPers Arsane
\livretVerse#12 { Dois-je éprouver encor vôtre injuste rigueur, }
\livretVerse#12 { Quand le Ciel avec moi paroît d’intelligence ? }
\livretVerse#8 { Voulez-vous bannir l’esperance, }
\livretVerse#8 { Qu’il vient ramener dans mon cœur ? }
\livretPers Amestris
\livretVerse#8 { Pouvez-vous perdre sans allarmes, }
\livretVerse#8 { L’attente d’un sort éclatant ? }
\livretVerse#12 { Pour changer vôtre cœur ne faut-il qu’un instant ? }
\livretVerse#12 { Et la gloire pour vous n’a-t’elle plus de charmes ? }
\livretPers Arsane
\livretVerse#8 { Non, mon cœur n’a jamais changé : }
\livretVerse#12 { A ses premiers désirs, il fut toujours fidelle ; }
\livretVerse#12 { Vos yeux n’ont-ils pas vû ma contrainte mortelle, }
\livretVerse#8 { Et l’horreur où j’étois plongé ? }
\livretVerse#8 { Non, mon cœur n’a jamais changé ! }
\livretVerse#12 { Mais, nous n’avez rien vû, Cruelle que vous êtes ? }
\livretVerse#12 { Amestris, insensible à mes peines secretes, }
\livretVerse#8 { Craignoit d’en suspendre le cours : }
\livretVerse#12 { Amestris, insensible à mes peines secretes, }
\livretVerse#12 { Détournoit des regards que je cherchois toûjours... }
\livretVerse#8 { Eh ! vous me les cachez encore ? }
\livretPers Amestris
\livretVerse#12 { Un auguste serment doit engager ma foi. }
\livretPers Arsane
\livretVerse#12 { Quel est donc ce serment ; }
\livretPers Amestris
\livretVerse#12 { \transparent { Quel est donc ce serment ; } Je ne suis plus à moi. }
\livretPers Arsane
\livretVerse#12 { Expliquez-vous. Calmez l’horreur qui me dévore. }
\livretPers Amestris
\livretVerse#12 { Pour la derniere fois recevez mes adieux ; }
\livretVerse#12 { Ne suivez plus mes pas, c’est un soin inutile. }
\livretPers Arsane
\livretVerse#12 { Où fuyez-vous ? }
\livretPers Amestris
\livretVerse#12 { \transparent { Où fuyez-vous ? } Au Temple, & c’est là mon azile. }
\livretVerse#12 { Par des nœuds éternels, je vais m’unir aux Dieux. }
\livretPers Arsane
\livretVerse#12 { Et moi je vous dispute à ces Rivaux terribles ; }
\livretVerse#12 { Et vous me trouverez entre l’Autel & vous. }
\livretPers Amestris
\livretVerse#12 { Ah ! Seigneur, étouffez un impuissant couroux. }
\livretVerse#12 { Aux profanes Mortels ces lieux inaccessibles, }
\livretVerse#12 { Ont en dépôt la foudre, entendez ces éclats : }
\livretVerse#12 { Je la suspens encor par mon obéissance : }
\livretVerse#12 { Craignez ces Dieux, tremblez & ne me forcez pas }
\livretVerse#12 { D’implorer contre vous leur terrible vengeance. }
\livretPers Arsane
\livretVerse#12 { Ah ! dûssai-je y trouvez le plus cruel trépas }
\livretVerse#12 { Je ne souffrirai point... }

\livretScene SCENE DEUXIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Semiramis, Arsane.
}
\livretPers Semiramis
\livretRef #'BBArecit
\livretVerse#12 { \transparent { Je ne souffrirai point... } Ou courez-vous Arsane ? }
\livretVerse#8 { Quel trouble agite vos esprits ? }
\livretPers Arsane
\livretVerse#12 { Reine, qu’ai-je entendu ! que devient Amestris ? }
\livretVerse#12 { Elle fuit de ces lieux. Eh ! qui donc l’y condamne ? }
\livretPers Semiramis
\livretVerse#12 { Les Dieux, ses volontez, la Paix de mes Etats. }
\livretPers Arsane
\livretVerse#8 { Eh quoi ! vous n’y résistez pas ! }
\livretPers Semiramis
\livretVerse#12 { J’ai fait perir mon Fils pour conserver l’Empire. }
\livretVerse#12 { Les Dieux me menaçoient de perir par son bras. }
\livretVerse#12 { Amestris est d’un sang qu’il est tems de proscrire. }
\livretPers Arsane
\livretVerse#12 { De quoy l’accusez-vous ? quels sont ses attentats ? }
\livretPers Semiramis
\livretVerse#12 { Et vous, quel intérêt ?... }
\livretPers Arsane
\livretVerse#12 { \transparent { Et vous, quel intérêt ?... } Celui de vôtre gloire, }
\livretVerse#12 { Le repos de vos jours. }
\livretPers Semiramis
\livretVerse#12 { \transparent { Le repos de vos jours. } Du moins j’aime à le croire. }
\livretVerse#12 { Le tems dévoilera ce mystere à mes yeux. }
\livretVerse#12 { Mais loin de s’appaiser, que demandent les Dieux ? }
\livretVerse#12 { Quel obstacle nouveau font-ils ici renaître ? }
\livretVerse#8 { Zoroastre est prêt d’y paroître. }
\livretVerse#12 { Son char aussi brillant, que le flambeau du jour, }
\livretVerse#12 { Plus prompt que les éclairs, vole & fend les nuages. }
\livretVerse#10 { Mon Peuple admire, & tremble tour à tour, }
\livretVerse#12 { Et l’encens à la main, l’attend sur ces rivages ; }
\livretVerse#12 { Ainsi, vôtre Rival m’apporte ses hommages, }
\livretVerse#12 { Dans l’instant où pour vous je trahis son amour. }
\livretVerse#8 { Il vient, faisons-nous violence. }
\livretPers Arsane
\livretVerse#12 { Croyez-vous l’abuser, ou braver sa vengeance ? }
\livretPers Semiramis
\livretVerse#12 { Arsane, s’il n’a pas consulté les Enfers, }
\livretVerse#8 { Il ignore encor mon offense. }
\livretPers Arsane
\livretVerse#12 { Tout va l’en éclaircir. }
\livretPers Semiramis
\livretVerse#12 { \transparent { Tout va l’en éclaircir. } Préparez ma défense, }
\livretVerse#12 { Assemblez mes Soldats, les momens nous sont chers. }
\livretPers Arsane
\livretVerse#12 { Je suivrai des devoirs dont rien ne me dispense. }
\livretVerse#10 { Si par le Ciel mes vœux sont secondez, }
\livretVerse#12 { Je ferai plus pour vous que vous ne demandez. }

\livretScene SCENE TROISIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Zoroastre, Semiramis.
}
\livretPers Zoroastre
\livretRef #'BCBzoroastreSemiramis
\livretVerse#12 { Belle Semiramis, l’amour & l’esperance }
\livretVerse#12 { Par des chemins nouveaux m’amenent dans ces lieux. }
\livretVerse#8 { Le charme de vôtre présence }
\livretVerse#12 { A déja réparé l’extrême violence }
\livretVerse#12 { Des maux que j’ai soufferts, éloigné de vos yeux. }
\livretVerse#8 { Le Dieu qui lance le Tonnerre }
\livretVerse#12 { M’a remis un pouvoir peu different du sien ; }
\livretVerse#12 { Il m’a rendu des Rois l’Arbitre & le Soûtien. }
\livretVerse#8 { J’éteins & j’allume la guerre, }
\livretVerse#8 { Je fais le destin de la terre, }
\livretVerse#12 { Et c’est dans vos beaux yeux que je cherche le mien. }
\livretPers Semiramis
\livretVerse#12 { Vos plus fiers Ennemis vous cedent la victoire. }
\livretVerse#12 { Vôtre Art, vôtre Valeur peuvent tout surmonter : }
\livretVerse#12 { Un cœur tel que le mien pouroit-il se flatter, }
\livretVerse#8 { De manquer seul à vôtre gloire ? }
\livretPers Zoroastre
\livretVerse#12 { Peuples des Elemens, paroissez à mes yeux. }
\livretVerse#12 { Esprits, qui par l’Amour dispersez en tous lieux, }
\livretVerse#12 { Sur la Terre & sur l’Onde étendez son Empire ; }
\livretVerse#10 { Vous qui volez avec lui dans les Cieux, }
\livretVerse#12 { Joignez tous vos transports à l’ardeur qui m’inspire. }
\livretVerse#8 { De ce jour célebre pour nous, }
\livretVerse#12 { Rendez à l’avenir la memoire durable. }
\livretVerse#12 { L’ouvrage des Mortels comme eux est perissable, }
\livretVerse#12 { Dressez un monument immortel comme vous. }
\livretVerse#8 { Qu’une nouvelle Flore exhale }
\livretVerse#8 { Des parfums du ciel descendus. }
\livretVerse#8 { Et que ces jardins suspendus }
\livretVerse#12 { De la terre & des cieux remplissent l’intervalle. }

\livretScene SCENE QUATRIÉME
\livretDescAtt\center-column {
  \wordwrap-center {
    Le Théâtre change, & représente les célébres Jardins de Semiramis.
  }
  \wordwrap-center {
    \smallCaps { Zoroastre, Semiramis, }
    Troupes de Genies Elementaires, & de Peuples.
  }
}
\livretPers Un Genie
\livretRef #'BDBair
\livretVerse#7 { L’Art plus prompt que la Nature, }
\livretVerse#10 { Dans ces beaux lieux rassemble en même tems, }
\livretVerse#12 { Et des Fleurs & des Fruits la riante parure ; }
\livretVerse#10 { On voit l’Automne à côté du Printems. }
\livretVerse#8 { Aimable maître de nos ames, }
\livretVerse#12 { Amour, ferois-tu moins en faveur de nos flâmes ? }
\livretVerse#8 { Fai naître, & comble nos désirs, }
\livretVerse#12 { Rassemble en même tems l’espoir & les plaisirs. }
\livretPers Zoroastre
\livretRef #'BDCzoroastreChoeur
\livretVerse#8 { Formez les plus tendres concerts. }
\livretVerse#8 { Chantez une Reine charmante : }
\livretVerse#10 { Que de son Nom retentissent les airs, }
\livretVerse#8 { Qu’il vole en cent climats divers : }
\livretVerse#7 { A cette Fête éclatante, }
\livretVerse#7 { Appellez tout l’Univers. }
\livretPers Chœur
\livretVerse#8 { Formons les plus tendres concerts. }
\livretVerse#8 { Chantons une Reine charmante. }
\livretVerse#10 { Que de son Nom retentissent les airs, }
\livretVerse#8 { Qu’il vole en cent climats divers. }
\livretVerse#7 { A cette Fête éclatante, }
\livretVerse#7 { Appellons tout l’Univers. }
\livretPersDidas Zoroastre à Semiramis.
\livretRef #'BDMrecit
\livretVerse#12 { De vos nouveaux Sujets voyez quelle est l’ardeur, }
\livretVerse#12 { Répondez à mes feux, répondez à leur zele ; }
\livretVerse#10 { Je veux devoir l’instant de mon bonheur }
\livretVerse#12 { Bien moins à vos sermens, qu’à mon amour fidele. }
\livretPers Semiramis
\livretVerse#12 { Seigneur, il n’est pas tems d’accomplir vos projets. }
\livretVerse#12 { Amestris est encor trop chere à mes sujets, }
\livretVerse#10 { Je veux contr’elle assurer ma puissance : }
\livretVerse#12 { Je ne puis vous offrir que ma reconoissance. }

\livretScene SCENE CINQUIÉME
\livretPers Zoroastre
\livretRef #'BEArecit
\livretVerse#10 { Qu’ai-je entendu ? quel soupçon ! quel effroi }
\livretVerse#12 { Dans mon cœur agité, s’éleve malgré moi ! }
\livretVerse#12 { Je veux les éclaircir... Amour, soyez mon guide : }
\livretVerse#12 { Mais si je n’ai brûlé que pour une perfide, }
\livretVerse#12 { Fureur, pour me vanger je n’écoute que toi. }

\sep
%\column-break
\livretAct ACTE III
\livretDescAtt\wordwrap-center {
  Le Théâtre représente une vestibule orné de Statues des Rois de Babylone.
}
\livretScene SCENE PREMIERE
\livretPers Zoroastre
\livretRef #'CAAzoroastre
\livretVerse#12 { Qu’ai-je appris ! quels forfaits ! quelle injure mortelle ! }
\livretVerse#12 { C’est pour un inconnu qu’on me manque de foi. }
\livretVerse#12 { O Majesté des Rois ! O puissance éternelle }
\livretVerse#8 { Des Dieux, qu’atestoit l’Infidelle, }
\livretVerse#8 { On vous outrage, comme moi. }
\livretVerse#12 { Haine, transports jaloux, implacable colere, }
\livretVerse#8 { Barbares enfans de l’Amour, }
\livretVerse#12 { Eteignez son flambeau, que le vôtre m’éclaire ; }
\livretVerse#12 { Armez-vous contre lui, regnez à vôtre tour. }
\livretVerse#12 { Mais, quel triste secours me promet ma vangeance ! }
\livretVerse#8 { C’est par mon cœur qu’elle commence. }
\livretVerse#12 { De vains gemissemens, d’inutiles regrets, }
\livretVerse#12 { Des cris perdus, des pleurs dont fremit ma constance, }
\livretVerse#12 { Sont du plus tendre Amour l’unique récompense. }
\livretVerse#12 { Non, non, de ma fureur déployons tous les traits ; }
\livretVerse#12 { Accablons mon Rival, la Reine, ses Sujets. }
\livretVerse#12 { Haine, transports jaloux, implacable colere, }
\livretVerse#8 { Barbares enfans de l’Amour, }
\livretVerse#12 { Eteignez son flambeau, que le vôtre m’éclaire ; }
\livretVerse#12 { Armez-vous contre lui, regnez à vôtre tour. }

\livretScene SCENE DEUXIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Zoroastre, Semiramis.
}
\livretPers Zoroastre
\livretRef #'CBAsemiramisZoroastre
\livretVerse#12 { Ah ! Perfide, osez-vous soûtenir ma présence ? }
\livretPers Semiramis
\livretVerse#12 { Ces noms injurieux me sont-ils adressez ? }
\livretPers Zoroastre
\livretVerse#12 { Avez-vous crû forcer mon dépit au silence ? }
\livretPers Semiramis
\livretVerse#12 { Oubliez-vous mon rang, & qui vous offensez ? }
\livretPers Zoroastre
\livretVerse#12 { Oubliez-vous le mien, & qui vous trahissez ? }
\livretPers Semiramis
\livretVerse#10 { Semiramis ne connoît point de Maître, }
\livretVerse#8 { Le Ciel est seul juge des Rois. }
\livretPers Zoroastre
\livretVerse#12 { Le Ciel vange sur eux le mépris de ses Loix, }
\livretVerse#8 { Et vous l’éprouverez peut-être. }
\livretVerse#8 { Quoi ! les sermens les plus sacrez }
\livretVerse#8 { N’ont pû fixer vôtre inconstance ! }
\livretVerse#12 { Eh ! quel est le Rival que vous me préferez ? }
\livretVerse#12 { Un étranger sans nom, sans états, sans naissance ? }
\livretPers Semiramis
\livretVerse#12 { J’ignore ses Ayeux, je connois ses vertus. }
\livretPers Zoroastre
\livretVerse#12 { Ingrate, il est donc vrai que vous ne m’aimez plus ? }
\livretVerse#12 { Tant de soins, tant d’amour, tant de perseverance, }
\livretVerse#12 { Mon espoir, mon bonheur sont pour jamais perdus. }
\livretVerse#12 { Que ne puis-je étouffer l’ardeur qui me dévore ; }
\livretVerse#12 { Que ne puis-je à mon tour oublier vos attraits, }
\livretVerse#12 { Ces perfides attraits, que malgré moi j’adore. }
\livretVerse#12 { Faut-il, quand vôtre cœur m’abandonne à jamais, }
\livretVerse#10 { Que vos regards me retiennent encore ? }
\livretPers Semiramis
\livretVerse#12 { Eh bien, dans mes regards, lisez donc mes douleurs. }
\livretVerse#10 { Pour vous vanger, joüissez de mes pleurs. }
\livretVerse#12 { Je veux, je crains, j’espere, & mon espoir me gêne ; }
\livretVerse#12 { Je combats, je résiste, & cede tour à tour : }
\livretVerse#8 { Un penchant inconnu m’entraîne, }
\livretVerse#12 { Plus puissant mille fois, & moins doux que l’amour. }
\livretVerse#12 { Ah ! si vous connoissiez l’excès de mes allarmes, }
\livretVerse#12 { Vous-même à mes malheurs vous donneriez des larmes. }
\livretPers Zoroastre
\livretVerse#12 { N’aviez-vous à m’offrir que ce cruel secours ? }
\livretPers Semiramis
\livretVerse#10 { Epargnons-nous d’inutiles discours, }
\livretVerse#8 { Seigneur, respectons nôtre gloire. }
\livretVerse#12 { D’un malheureux amour étouffez la memoire, }
\livretVerse#12 { Laissez mon triste cœurs en proye à ses remords. }
\livretVerse#10 { C’est malgré moi, que je couronne Arsane... }
\livretPers Zoroastre
\livretVerse#12 { Arsane ! ah ! que ce nom redouble mes transports ! }
\livretVerse#12 { C’en est fait, à perir vôtre amour le condamne. }
\livretPers Semiramis
\livretVerse#12 { D’un aveugle couroux nous bravons les efforts. }
\livretVerse#12 { Mais vous-même tremblez d’en être la victime. }
\livretVerse#12 { Le Ciel sera pour lui. }
\livretPers Zoroastre
\livretVerse#12 { \transparent { Le Ciel sera pour lui. } Sera-t’il pour le crime, }
\livretVerse#12 { Il perira. }
\livretPers Semiramis
\livretVerse#12 { \transparent { Il perira. } Craignez les Dieux & sa valeur. }
\livretPers Zoroastre
\livretVerse#8 { Craignez Zoroastre en fureur. }
\livretPers Ensemble
\livretVerse#12 { Tonnez, Dieux immortels, tonnez, lancez la Foudre, }
\livretVerse#12 { Perdez vos Ennemis, qu’ils tombent sous vos coups, }
\livretVerse#8 { Frappez, éclatez, hâtez-vous ! }
\livretVerse#8 { Hâtez-vous de reduire en poudre }
\livretVerse#12 { Les \line { \left-brace#20 \raise#1 \column { superbes parjures } \right-brace#20 } Mortels qui s’arment contre vous. }


\livretScene SCENE TROISIÉME
\livretPers Zoroastre
\livretRef #'CCAzoroastre
\livretVerse#12 { Pour tant de maux soufferts, quels maux dois-je lui rendre ? }
\livretVerse#12 { Et comment me vanger ? mon art va me l’apprendre. }
\livretVerse#8 { Terrible rampart des Enfers }
\livretVerse#12 { Styx affreux, dont les flots environnent les ombres }
\livretVerse#12 { Elevez jusqu’à moi vos vapeurs les plus sombres. }
\livretVerse#12 { Que le Soleil vaincu se cache dans les Mers, }
\livretVerse#8 { Que ce jour manque à l’Univers. }
\livretVerse#8 { Lieux témoins de mon infortune, }
\livretVerse#12 { Lieux que je fais envain retentir de mes cris, }
\livretVerse#12 { Disparoissez, tombez, vôtre aspect m’importune. }
\livretVerse#12 { Qu’un magique Palais naisse de vos débris. }
\livretDidasP\justify {
  Le Théâtre change, & représente un Palais magique,
  orné de Statuës qui portent des flambeaux.
}
\livretVerse#10 { Qu’à ma fureur tout prête ici des armes. }
\livretVerse#10 { Tristes Objets qui portez ces flambeaux, }
\livretVerse#8 { Arrachez du sein des tombeaux ; }
\livretVerse#10 { Animez-vous. Demons, vangez mes larmes, }
\livretVerse#12 { C’est moi qui le premier vous ai donné la Loi. }
\livretVerse#8 { Vous Mortels instruits à mes charmes, }
\livretVerse#12 { Venez de toutes parts, secondez vôtre Roi. }

\livretScene SCENE QUATRIÉME
\livretDescAtt\wordwrap-center {
  \smallCaps Zoroastre, Troupe de Demons, de Magiciens & de Magiciennes.
}
\livretPers Zoroastre
\livretRef #'CDCzoroastre
\livretVerse#10 { Semiramis a trahi mon ardeur, }
\livretVerse#12 { Partagez cet outrage, & servez ma fureur. }
\livretPersDidas Zoroastre alternativement avec le Chœur
\livretRef #'CDDcoupes
\livretVerse#5 { Versons l’épouvante }
\livretVerse#3 { Dans les cœurs ; }
\livretVerse#3 { Que l’attente }
\livretVerse#3 { Des malheurs }
\livretVerse#3 { En augmente }
\livretVerse#3 { Les horreurs. }
\livretVerse#5 { Commande à l’Empire }
\livretVerse#3 { Tenebreux. }
\livretVerse#3 { Tout conspire }
\livretVerse#3 { Pour tes vœux. }
\livretVerse#3 { Qu’on respire }
\livretVerse#3 { Mille feux. }
\livretVerse#4 { Souflons la guerre. }
\livretVerse#4 { Couvrons la terre }
\livretVerse#5 { De sang & de morts. }
\livretVerse#5 { Faisons des efforts }
\livretVerse#5 { Egaux au tonnerre. }
\livretVerse#4 { Soufflons la guerre }
\livretVerse#6 { Peuplons les sombres bords. }
\livretRef #'CDEair
\livretDidasPPage\line { On danse. }
\livretPers Zoroastre
\livretRef #'CDHzoroastre
\livretVerse#12 { Arrêtez. Les Enfers sont prêts à m’inspirer, }
\livretVerse#12 { Qu’en ce jour sur vos soins je puisse m’assurer. }
\livretVerse#12 { Quel noir transport succede à ma douleur profonde ! }
\livretVerse#12 { Le Styx qui fait les loix & les crimes des Dieux, }
\livretVerse#8 { Le Styx se découvre à mes yeux. }
\livretVerse#12 { Quels funeste secrets me revele son onde ! }
\livretVerse#8 { Malheureuse Semiramis, }
\livretVerse#12 { Tremble ! de tes fureurs que le Destin condamne, }
\livretVerse#12 { Ton Fils est échapé, je le vois, c’est Arsane... }
\livretVerse#12 { Mais ! quel spectacle affreux trouble encor mes esprits ! }
\livretVerse#12 { Le glaive est suspendu, Quelle illustre victime }
\livretVerse#12 { Va se précipiter au tenebreux abîme ? }
\livretVerse#12 { Quel sang prêt à couler ? de quels lugubres cris }
\livretVerse#7 { Les Autels retentissent ? }
\livretVerse#13 { Le Peuple est consterné, les Dieux même en fremissent. }
\livretVerse#8 { Et je sens que je m’attendris. }
\livretVerse#12 { Eh ! qui donc va perir ? est-ce vous Amestris ? }
\livretVerse#8 { Tout fuit... Quelle confuse image ! }
\livretVerse#10 { Je ne vois plus qu’à travers un nuage... }
\livretVerse#10 { Je suis vangé. Je vois des malheureux... }
\livretVerse#12 { De pleurs, de cris, de sang marquons ce jour affreux. }
\sep
%\column-break
\livretAct ACTE IV
\livretDescAtt\wordwrap-center {
  Le Théâtre représente le Temple de Jupiter Belus,
  orné pour la Consecration \concat { d’ \smallCaps Amestris. }
}
\livretScene SCENE PREMIERE
\livretPers Arsane
\livretRef #'DAAarsane
\livretVerse#12 { Ou suis-je ? quelle horreur agite mes esprits ? }
\livretVerse#12 { C’est ici que le Dieu du Ciel & de la Terre }
\livretVerse#12 { Présente sa splendeur à nos regards surpris. }
\livretVerse#8 { Y viens-je braver son Tonnerre, }
\livretVerse#12 { Et du sein des Autels arracher Amestris ! }
\livretVerse#12 { Qu’importe que la foudre à mes yeux étincelle ! }
\livretVerse#7 { Dieu barbare, Dieu jaloux, }
\livretVerse#10 { Frappez : je vais au devant de vos coups, }
\livretVerse#8 { Trop heureux de perir pour elle, }
\livretVerse#7 { Dieu barbare, Dieu jaloux, }
\livretVerse#12 { Non, tant que je respire, elle n’est point à vous. }

\livretScene SCENE DEUXIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Semiramis, Arsane.
}
\livretPers Semiramis
\livretRef #'DBArecit
\livretVerse#12 { Arsane, quel spectacle ici vient me surprendre ? }
\livretVerse#12 { Prés de ces murs sacrez, on arrête mes pas. }
\livretVerse#12 { Eh ! qui donc contre moi revolte mes Soldats ? }
\livretVerse#12 { Seroit-ce Zoroastre ? à qui je dois m’en prendre ? }
\livretPers Arsane
\livretVerse#12 { Contre tous ses efforts je sçaurois vous défendre : }
\livretVerse#12 { Mais un peril plus grand doit causer vôtre effroi : }
\livretVerse#12 { Le Peuple aime Amestris, il peut tout entreprendre, }
\livretVerse#12 { Il s’arme contre vous. }
\livretPers Semiramis
\livretVerse#12 { \transparent { Il s’arme contre vous. } Non, Perfide, c’est toi ; }
\livretVerse#12 { C’est toi qui me trahis, mes maux sont ton ouvrage. }
\livretVerse#12 { Mais, tu n’as pas long-tems joui de mon erreur, }
\livretVerse#12 { J’ai lû, je lis encor dans le fond de ton cœur. }
\livretVerse#12 { Cœur indigne du Trône, & fait pour l’esclavage ; }
\livretVerse#12 { J’y vois la trahison, le mépris des bienfaits, }
\livretVerse#12 { J’y vois contre mes jours tes barbares projets, }
\livretVerse#12 { Et tes lâches soupirs pour celle qui m’outrage. }
\livretPers Arsane
\livretVerse#12 { Arsane ne sçait point dissimuler ses feux. }
\livretVerse#12 { L’ensensible Amestris occupoit tous mes vœux, }
\livretVerse#12 { Avant que vôtre main vint m’offrir tant de gloire ; }
\livretVerse#12 { Je l’adore malgré ses mépris rigoureux, }
\livretVerse#12 { C’est de l’Amour sur moi la premiere victoire. }
\livretPers Semiramis
\livretVerse#12 { Redouble cet Amour, il me vange encor mieux. }
\livretVerse#12 { Tu la perds : de leur choix demande compte aux Dieux. }
\livretPers Arsane
\livretVerse#12 { Osez-vous attester des noms si redoutables ? }
\livretVerse#12 { Ont-ils parlé ces Dieux, sçait-on leurs volontez ? }
\livretVerse#12 { Non, la soif de regner, les fureurs implacables, }
\livretVerse#8 { Sont les Dieux que vous consultez. }
\livretVerse#12 { Ah ! ne demandez plus, d’où naissent les présages ; }
\livretVerse#12 { Quel crime attire ici la foudre & les orages ? }
\livretVerse#12 { Vous attentez aux droits dont le Ciel est jaloux, }
\livretVerse#12 { Et la justice éclate à se vanger de vous. }
\livretPers Semiramis
\livretVerse#12 { Je fais pour le fléchir, un effort inutile. }
\livretVerse#12 { Mais, Barbare, est-ce à toi de me le reprocher ? }
\livretVerse#12 { J’esperois avec toi goûter un sort tranquille ; }
\livretVerse#12 { Auprès de tes vertus je cherchois un azile. }
\livretVerse#12 { Non, ta haine pour moi ne sçauroit se cacher ; }
\livretVerse#12 { Augmente tes mépris, triomphe de mes larmes, }
\livretVerse#8 { Contre toi prête-moi des armes. }
\livretDidasPPage\line { a part. }
\livretVerse#12 { Quel ascendant fatal m’a soumise à sa Loi ! }
\livretDidasPPage\line { à Arsane. }
\livretVerse#12 { Ingrat, que m’as-tu fait pour m’attendrir pour toi ? }
\livretPers Arsane
\livretVerse#12 { Achevez, & brisez les fers de la Princesse : }
\livretVerse#12 { Sauvez de tant de Rois le reste précieux : }
\livretVerse#12 { Je ne demande point de l’unir à mes vœux ; }
\livretVerse#12 { Je nourris dans mon cœur une vaine tendresse. }
\livretPers Semiramis
\livretVerse#12 { Eh ! pourquoi donc l’aimer avec tant de transport ! }
\livretVerse#12 { Tu partages enfin les rigueurs de mon sort : }
\livretVerse#12 { Tu connois le tourment d’aimer qui nous abhorre ; }
\livretVerse#8 { Que n’y puis-je ajoûter encore }
\livretVerse#12 { De la rendre sensible, & perfide pour toi. }
\livretVerse#12 { Va, devien, s’il se peut, plus malheureux que moi. }
\livretPers Arsane
\livretVerse#12 { On vient. Voici l’instant du cruel sacrifice. }

\livretScene SCENE TROISIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Arsane, Amestris, Semiramis.
}
\livretPersDidas Arsane à Amestris
\livretRef #'DCArecit
\livretVerse#12 { Ah ! Princesse, faut-il que rien ne vous fléchisse ! }
\livretPers Amestris
\livretVerse#12 { Arsane, respectez Amestris & les Dieux. }
\livretVerse#12 { Quels sont vos droits sur moi, pourquoi troubler mes vœux ? }
\livretVerse#12 { De mes jours à nos Dieux, je fais un libre hommage. }
\livretVerse#12 { J’ai calmé les transports d’un Peuple audacieux. }
\livretVerse#12 { Laissez à ma vertu consommer son ouvrage. }
\livretPers Arsane
\livretVerse#12 { Non, laissez-moi sortir de ces funestes lieux. }
\livretVerse#12 { Je vais de mes Soldats ranimer le courage. }

\livretScene SCENE CINQUIÉME
\livretDescAtt\wordwrap-center {
  \smallCaps { Semiramis, Amestris, }
  Prestres & Prestresses de Jupiter, & le Peuple.
}
\livretPers Amestris
\livretRef #'DECamestris
\livretVerse#10 { J’immole aux Dieux le printems des mes jours ; }
\livretVerse#12 { A l’ombre des Autels avec vous je vais vivre : }
\livretVerse#8 { Heureuse, si vôtre secours }
\livretVerse#12 { De mes troubles secrets pour jamais me délivre ! }
\livretPersDidas Chœur des Prestres & des Prestresses
\livretRef #'DEDchoeur
\livretVerse#5 { Digne sang des Rois, }
\livretVerse#5 { Le Ciel vous appelle, }
\livretVerse#5 { Soûtenez son choix ; }
\livretVerse#5 { Le Ciel vous appelle ; }
\livretVerse#5 { Au Peuple fidelle }
\livretVerse#5 { Dispensez ses Loix. }
\livretPers Petit Chœur
\livretVerse#5 { Sensible à vos vœux }
\livretVerse#5 { Le Dieu du Tonnerre }
\livretVerse#5 { Eteindra ses feux ; }
\livretVerse#5 { Et par vous, la Terre }
\livretVerse#5 { Va s’unir aux Cieux. }
\livretPers Tous
\livretVerse#5 { Digne sang des Rois, &c. }
\livretPersDidas Amestris
\smaller presente l’Encens & les Fleurs à la Statuë de Jupiter.
\livretRef #'DEEamestris
\livretVerse#12 { Recevez cet Encens, ces Couronnes de fleurs, }
\livretVerse#12 { Hommages innocens que vous rend la nature. }
\livretVerse#12 { Je viens y joindre encor une offrande plus pure, }
\livretVerse#12 { Des vœux toûjours nouveaux, seul tribut de nos cœurs. }
\livretPers Semiramis
\livretRef #'DEFsemiramis
\livretVerse#12 { Triomphez, Dieu puissant, qui regnez sur les Dieux, }
\livretVerse#12 { Qu’on vous rende par tout un éternel hommage, }
\livretVerse#12 { Versez sur ces climats mille dons précieux, }
\livretVerse#12 { Loin de ces tristes lieux laissez grondez l’orage. }
\livretPers Chœur
\livretRef #'DEGchoeur
\livretVerse#12 { Triomphez, Dieu puissant, &c. }
\livretRef #'DEHrondeau
\livretDidasPPage On danse.
\livretPersDidas Une Prestresse
\smaller à Amestris, alternativement avec le Chœur.
\livretRef #'DEIpretresseChoeur
\livretVerse#6 { L’Amour verse des larmes, }
\livretVerse#5 { Vous causez ses pleurs ; }
\livretVerse#6 { Il devoit par vos charmes }
\livretVerse#5 { Vaincre tous les cœurs. }
\livretVerse#6 { Sa plus chere esperance }
\livretVerse#5 { S’éteint à jamais ! }
\livretVerse#6 { Eh ! quels yeux désormais }
\livretVerse#6 { Etendront sa puissance, }
\livretVerse#5 { Quels seront ses traits ? }
\livretPers Chœur
\livretVerse#6 { L’Amour verse, &c. }
\livretPers La Prestresse
\livretVerse#6 { Le foible honneur de plaire }
\livretVerse#5 { Coûte des tourmens. }
\livretVerse#6 { La victoire est trop chere ; }
\livretVerse#5 { Fuyons les Amans. }
\livretVerse#6 { Des momens plus tranquilles }
\livretVerse#5 { Vont couler pour vous. }
\livretVerse#6 { Goûtez dans nos aziles }
\livretVerse#5 { Les biens les plus doux. }
\livretPers Chœur
\livretVerse#6 { L’Amour verse, &c. }
\livretRef #'DEJair
\livretDidasPPage On danse.
\livretPers La Prestresse
\livretRef #'DEKpretresse
\livretVerse#10 { Beaux Lieux, soyez toûjours exempts d’allarmes. }
\livretVerse#8 { Amour, n’en trouble point la paix. }
\livretVerse#7 { Trop de pleurs suivent tes traits, }
\livretVerse#3 { Que tes armes, }
\livretVerse#3 { Que tes charmes, }
\livretVerse#7 { S’en éloignent pour jamais. }
\livretVerse#10 { Non, non, à des plaisirs purs & faciles }
\livretVerse#8 { Ne mêle point des soins fâcheux. }
\livretVerse#3 { Ces aziles }
\livretVerse#3 { Si tranquiles, }
\livretVerse#7 { Ne redoutent point tes feux. }
\livretVerse#10 { Vole, descens Amour, vien dans ces lieux, }
\livretVerse#8 { Nos cœurs y bravent ta victoire : }
\livretVerse#10 { Vole, descends Amour, vien dans ces lieux, }
\livretVerse#8 { Voi ta défaite, & nôtre gloire. }
\livretPers Semiramis
\livretRef #'DENsemiramisAmestris
\livretVerse#12 { Amestris, achevez ce noble sacrifice. }
\livretVerse#8 { Qu’il nous rende le Ciel propice. }
\livretVerse#8 { Auguste Interprete des Dieux, }
\livretVerse#12 { Vous tiendrez dans vos mains le bonheur de ces lieux. }
\livretPersDidas Amestris la main sur l’Autel.
\livretVerse#12 { Je quitte pour jamais l’éclat qui m’environne, }
\livretVerse#12 { Maître des Immortels, remplissez tout mon cœur, }
\livretVerse#12 { La pompe, les plaisirs, la suprême grandeur }
\livretVerse#12 { N’ont plus de droits sur moi, je vous les abandonne. }
\livretVerse#10 { Rendez heureux les jours que je vous donne. }
\livretPers Chœur
\livretRef #'DEOchoeur
\livretVerse#8 { Quel bruit affreux nous fait trembler ! }
\livretVerse#12 { Sous nos pas chancelans se dérobe la Terre, }
\livretVerse#12 { Le Dieu menace, il s’arme, il lance le Tonnerre, }
\livretVerse#12 { Ecoutez, fremissez, il est prêt à parler. }

\livretScene SCENE SIXIÉME
\livretDescAtt\wordwrap-center {
  \smallCaps { L'Oracle, Amestris, Semiramis, }
  & les Acteurs de la Scene précédente.
}
\livretPers L'Oracle
\livretRef #'DFAoracle
\livretVerse#10 { Pour apaiser mon couroux légitime, }
\livretVerse#12 { Amestris, c’est trop peu de vœux que tu me fais ; }
\livretVerse#12 { Au tombeau de Ninus va t’offrir en victime }
\livretVerse#12 { Pour m’assurer le sang qu’exigent mes decrets. }
\livretPers Amestris
\livretRef #'DFBamestris
\livretVerse#12 { J’obéïrai, grands Dieux, je vais vous satisfaire : }
\livretVerse#12 { Je reçois une mort qui finit mes tourmens. }
\livretVerse#12 { Reine, à vôtre repos, je ne suis plus contraire ; }
\livretVerse#12 { Laissez-moi m’occuper de mes derniers momens. }
\livretPers Chœur
\livretRef #'DFCchoeur
\livretVerse#12 { Non, non, non, Dieux cruels, gardez vôtre colere, }
\livretVerse#12 { Tonnez plutôt sur nous, armez les Elemens. }

\livretScene SCENE SEPTIÉME
\livretDescAtt\wordwrap-center {
  \smallCaps { Arsane, Amestris, }
  Prestres, Prestresses, & le Peuple.
}
\livretPers Arsane
\livretRef #'DGAaaChoeur
\livretVerse#8 { Quoi ! tout me fuit, tout m’abandonne ! }
\livretVerse#12 { Quel prestige a glacé le cœur de mes Soldats ! }
\livretVerse#12 { Je les excite envain à marcher sur mes pas : }
\livretVerse#12 { Mais, quel trouble nouveau, quelle horreur m’environne ! }
\livretPers Chœur
\livretVerse#12 { Amestris va perir : c’est le Ciel qui l’ordonne. }
\livretPersDidas Amestris au Peuple.
\livretVerse#10 { Dérobez-moi les pleurs que vous m’offrez ; }
\livretVerse#12 { Peuples, éloignez-vous : Arsane, demeurez. }

\livretScene SCENE HUITIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Amestris, Arsane.
}
\livretPers Amestris
\livretRef #'DHArecit
\livretVerse#12 { Je vais subir la loi que le sort me dispense ; }
\livretVerse#10 { De tant d’honneurs promis à ma naissance, }
\livretVerse#12 { Un seul tombeau me reste. Et ce jour que je vois, }
\livretVerse#12 { Cette Terre, ces Cieux, tout va fuir devant moi. }
\livretPers Arsane
\livretVerse#12 { Eh ! vous y consentez, Grands Dieux, le puis-je croire ! }
\livretVerse#12 { Non, non, fuyez ces lieux, venez, sauvez vos jours. }
\livretPers Amestris
\livretVerse#12 { Dois-je les conserver, aux dépens de ma gloire ! }
\livretPers Arsane
\livretVerse#12 { Quoi ! vous me haïssez jusqu’à fuir mon secours ! }
\livretPers Amestris
\livretVerse#8 { Seigneur, je ne puis que vous plaindre. }
\livretPers Arsane
\livretVerse#12 { Dans quel triste moment, plaignez-vous mon malheur ? }
\livretPers Amestris
\livretVerse#8 { Lorsque je n’ai plus à vous craindre. }
\livretVerse#12 { Aux portes du trépas je vous ouvre mon cœur. }
\livretVerse#12 { J’ai connu vos vertus ; & ma feinte rigueur }
\livretVerse#8 { Ne m’a que trop causé d’allarmes ; }
\livretVerse#12 { Que ne pouvois-je, helas ! faire vôtre bonheur ! }
\livretVerse#8 { Le Ciel a vû seul ma douleur, }
\livretVerse#12 { Il sçait combien pour vous, j’ai dévoré de larmes. }
\livretPers Arsane
\livretVerse#8 { Et je vais vous perdre à jamais, }
\livretVerse#12 { Est-ce à vous d’expier de coupables forfaits ! }
\livretVerse#8 { Que le Ciel s’embrase & qu’il tonne, }
\livretVerse#12 { Que la guerre s’allume entre les Elemens. }
\livretVerse#8 { De la superbe Babylone }
\livretVerse#8 { Qu’ils renversent les fondemens : }
\livretVerse#12 { Qu’importe quel trépas leur fureur nous apprête, }
\livretVerse#12 { Vivez, & que sur moy retombe la tempête. }
\livretPers Amestris
\livretVerse#12 { Un Mortel ose-t’il braver les Dieux vangeurs ? }
\livretVerse#12 { A leurs suprêmes loix, il faut que tout se rende. }
\livretPers Arsane
\livretVerse#12 { Ah ! qu’ils cherchent ailleurs leur sacrilege offrande. }
\livretPers Ensemble
\livretVerse#10 {
  Ciel \raise#0.5 \left-brace#20 \raise#1.5 \column {
    \line { Equitable, oubliez ses fureurs. }
    \line { Implacable, épuisez vos rigueurs. }
  }
}
\livretVerse#6 { Tombent sur moi vos coups. }
\livretPers Amestris
\livretVerse#12 { Non, laissez-moi mourir innocente victime ; }
\livretVerse#12 { Mais n’oubliez jamais le beau feu qui m’anime. }
\livretVerse#12 { Adieu. Puisse le Ciel vous voir d’un œil plus doux ! }
\livretVerse#12 { Rendez ce Peuple heureux, qu’il me retrouve en vous : }
\livretVerse#12 { Qu’il rende à vos vertus un tribut légitime ; }
\livretVerse#12 { Que de vôtre bonheur rien ne borne le cours : }
\livretVerse#12 { Qu’ils vous donnent ces Dieux, ce qu’ils m’ôtent de jours. }
\livretPers Arsane
\livretVerse#10 { Moi ! je serois complice de leur crime ! }
\livretVerse#10 { Reine barbare, infidelles Soldats : }
\livretVerse#12 { Je vous attens, osez l’arracher de mes bras. }

\livretScene SCENE NEUVIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Zoroastre, Arsane.
}
\livretPers Zoroastre
\livretRef #'DIArecit
\livretVerse#12 { Arsane, où courez-vous ? }
\livretPers Arsane
\livretVerse#12 { \transparent { Arsane, où courez-vous ? } La sauver du trépas, }
\livretVerse#8 { Je vais secourir l’innocence. }

\livretScene SCENE DIXIÉME
\livretPers Zoroastre
\livretRef #'DJAzoroastre
\livretVerse#12 { Va, malheureux Amant, & plus malheureux Fils ! }
\livretVerse#10 { Enfer, tien-moi ce que tu m’as promis. }
\livretVerse#12 { Fureurs, suivez ses pas, préparez ma vangeance. }
\livretVerse#12 { Est-ce à moi d’épargner l’Ingrate qui m’offense. }
\sep
%\column-break
\livretAct ACTE V
\livretDescAtt\wordwrap-center {
  Le Théâtre représente le Tombeau de \smallCaps Ninus,
  Roy de Babylone : il est au milieu d’une Forest.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center\smallCaps {
  Zoroastre, Semiramis.
}
\livretPers Zoroastre
\livretRef #'EABrecit
\livretVerse#12 { Quoy ! la mort d’Amestris s’aprête dans ces lieux, }
\livretVerse#12 { Et ce Tombeau sacré va recevoir sa cendre ? }
\livretPers Semiramis
\livretVerse#12 { Tranquille sur son sort, elle ordonne les Jeux, }
\livretVerse#12 { Les funebres honneurs, qu’à Ninus on va rendre ; }
\livretVerse#12 { C’est son dernier hommage à ce Roi glorieux. }
\livretPers Zoroastre
\livretVerse#10 { Soutiendrez-vous ce spectacle odieux ? }
\livretPers Semiramis
\livretVerse#12 { Contre l’Arrêt du Ciel qui pourroit la défendre ? }
\livretPers Zoroastre
\livretVerse#12 { Vous, si vos yeux s’ouvroient aux malheurs que je crains : }
\livretVerse#12 { Je devrois vous haïr, malgré moi je vous plains. }
\livretVerse#8 { Voici ce moment redoutable ; }
\livretVerse#12 { Le destin d’Amestris attendrit tous les cœurs. }
\livretVerse#8 { Sa mort trouvera des vangeurs, }
\livretVerse#12 { Croyez-en mon effroi, le trouble qui m’accable, }
\livretVerse#12 { Triste & dernier effort d’un amour déplorable. }
\livretPers Semiramis
\livretVerse#12 { Redoutez moins un Peuple esclave de mes vœux : }
\livretVerse#12 { Babylone craindra mon courage & nos Dieux. }
\livretPers Zoroastre
\livretVerse#12 { Est-ce donc sur vos Dieux que vôtre espoir se fonde ? }
\livretVerse#5 { Une nuit profonde }
\livretVerse#5 { Vous cache leurs coups. }
\livretVerse#5 { Des Maîtres du monde }
\livretVerse#5 { Le Ciel est jaloux : }
\livretVerse#5 { Où fuir son couroux ? }
\livretVerse#5 { Sa haine feconde }
\livretVerse#5 { Nous fait succomber : }
\livretVerse#5 { La foudre qui gronde }
\livretVerse#5 { Est prête à tomber. }
\livretPers Semiramis
\livretVerse#5 { Tout accroit ma rage, }
\livretVerse#5 { J’imite les Dieux. }
\livretVerse#5 { Perisse à mes yeux }
\livretVerse#5 { L’Objet qui m’outrage ; }
\livretVerse#5 { Que mes Ennemis }
\livretVerse#5 { Soient reduits en poudre : }
\livretVerse#5 { Un coup de la foudre }
\livretVerse#5 { M’est cher à ce prix. }
\livretVerse#12 { Que dis-je ? d’Amestris le sort me fait envie. }
\livretVerse#12 { Arsane la plaindra : Dieux, quels troubles secrets ! }
\livretVerse#8 { Ingrat, donne-moi tes regrets : }
\livretVerse#12 { J’acheterois tes pleurs, aux dépens de ma vie. }
\livretPers Zoroastre
\livretVerse#12 { Il est tems d’étouffer de coupables amours. }
\livretVerse#12 { Connoissez-vous Arsane ? }
\livretPers Semiramis
\livretVerse#12 { \transparent { Connoissez-vous Arsane ? } Où tendent ces discours ? }
\livretPers Zoroastre
\livretVerse#12 { Je vais le découvrir ce funeste mystere. }
\livretVerse#12 { Vos feux ont fait pâlir l’astre qui nous éclaire, }
\livretVerse#12 { La terre en a tremblé, moi-même j’en fremis. }
\livretPers Semiramis
\livretVerse#12 { Quel coup vient me frapper ! }
\livretPers Zoroastre
\livretVerse#12 { \transparent { Quel coup vient me frapper ! } Arsane est vôtre Fils. }
\livretPers Semiramis
\livretVerse#12 { Mon Fils... Et j’ai brûlé d’une flâme si noire ? }
\livretVerse#12 { Qui vous l’a révélé ?... }
\livretPers Zoroastre
\livretVerse#12 { \transparent { Qui vous l’a révélé ?... } Les Enfers. }
\livretPers Semiramis
\livretVerse#12 { \transparent { Qui vous l’a révélé ?... Les Enfers. } Lui, mon Fils !... }
\livretVerse#12 { Non, Barbare, je vois ce que tu t’es promis : }
\livretVerse#12 { Tu le souhaites trop pour me le faire croire. }
\livretPers Zoroastre
\livretVerse#12 { Vous connoîtrez l’erreur dont vos sens sont surpris. }
\livretPers Ensemble
\livretVerse#12 { Brisez, brisez les nœuds d’une fatale chaîne. }
\livretVerse#8 {
  \line { \raise#1 \column { Tremblez, Non, non, } \right-brace#20 }
  Votre esperance est vaine
}
\livretVerse#12 {
  Je \line { \left-brace#20 \raise#1 \column {
      \line { prévois des malheurs qui me vangent de vous }
      \line { crains peu des malheurs qui m’arrachent à vous } } }
}
\livretVerse#8 {
  Le Ciel gronde,
  \line {
    \left-brace#20
    \raise#1 \column { craignez j’attens }
    \right-brace#20 }
  ses coups.
}

\livretScene SCENE QUATRIÉME
\livretDescAtt\wordwrap-center {
  \smallCaps { Amestris, Semiramis, }
  \line { Et les Acteurs de la Scene précédente. }
}
\livretPers Amestris
\livretRef #'EDAamestris
\livretVerse#12 { Peuples, qui de Ninus honorez la memoire, }
\livretVerse#8 { Je viens consommer ses bienfaits. }
\livretVerse#12 { Tous ses jours ont coulé pour vous combler de gloire, }
\livretVerse#12 { Le dernier de mes jours va vous donner la paix. }
\livretDidasPPage\line { Elle prend le Fer des Sacrifices. }
\livretVerse#12 { O Ciel, défend mon cœur d’un souvenir trop tendre ! }
\livretVerse#12 { Mânes de mes ayeux, faites place à ma cendre. }

\livretScene SCENE CINQUIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Arsane, Amestris, Semiramis, Chœurs.
}
\livretPersDidas Arsane lui arrachant le Fer.
\livretRef #'EEArecitArsane
\livretVerse#12 { Ah ! Princesse, arrêtez... }
\livretPers Amestris
\livretVerse#12 { \transparent { Ah ! Princesse, arrêtez... } Seigneur, que faites-vous ? }
\livretVerse#12 { Vous irritez les Dieux. }
\livretPers Arsane
\livretVerse#12 { \transparent { Vous irritez les Dieux. } Je brave leur couroux. }
\livretVerse#12 { Quoi ! la vertu perit ! Quelle aveugle vangeance ! }
\livretVerse#12 { Est-ce donc à ces traits qu’ils marquent leur puissance ? }
\livretVerse#12 { Perfides, n’osez-vous défendre l’innocence ? }
\livretPers Semiramis
\livretVerse#12 { Ministres des Autels vangez les droits des Dieux. }
\livretPers Arsane
\livretVerse#10 { Fuyez, tremblez, Esclaves odieux }
\livretVerse#6 { D’une Reine cruelle. }
\livretVerse#12 { Où suis-je ? quels transports ? C’est l’Enfer qui m’appelle, }
\livretVerse#10 { Je vous répons... quelle épaisse vapeur... }
\livretVerse#12 { Je vois devant mes pas le flambeau des Furies : }
\livretVerse#12 { Je vous suis... Epuisez toutes vos barbaries. }
\livretVerse#12 { Versons des flots de sang... Répandons la terreur : }
\livretVerse#8 { Je sens tout l’Enfer dans mon cœur. }
\livretRef #'EEBchoeur
\livretDidasPPage\wordwrap {
  Arsane renverse l’Autel, & se jette sur le Peuple qui fuit devant luy.
}
\livretPers Chœur
\livretVerse#12 { Secourez-nous, ô Dieux ! frappez qui vous offense. }
\livretDidasPPage\wordwrap {
  Semiramis sort à la tête des Prêtres & des Soldats ;
  Arsane les pousuit.
}

\livretScene SCENE SIXIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Amestris, & le Chœur.
}
\livretPers Amestris
\livretRef #'EFAchoeur
\livretVerse#6 { Dieux, prenez sa deffense. }
\livretPers Chœur
\livretVerse#12 { Secourez-nous, ô Dieux ! frappez qui vous offense. }
\livretPers Amestris
\livretVerse#8 { Ciel implacable, Ciel jaloux, }
\livretVerse#12 { Dois-je vous implorer ? il combat contre vous. }

\livretScene SCENE SEPTIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Arsane, Amestris.
}
\livretPers Arsane
\livretRef #'EGArecit
\livretVerse#12 { Vous vivrez, ma Princesse, & le Ciel par mes coups }
\livretVerse#8 { A voulu sauver tant de charmes : }
\livretVerse#12 { Un Dieu me conduisoit, un Dieu guidoit mes armes. }
\livretVerse#12 { Je ne vois point la Reine, allons à ses genoux }
\livretVerse#12 { Expier mon audace, & calmer ses allarmes. }

\livretScene SCENE DERNIERE
\livretDescAtt\wordwrap-center\smallCaps {
  Arsane, Semiramis, Zoroastre, Amestris, Chœur.
}
\livretPers Arsane
\livretRef #'EHArecit
\livretVerse#12 { Grands Dieux ! elle paroît, que mon cœur est glacé ! }
\livretVerse#12 { Je vois couler son sang. }
\livretPers Zoroastre
\livretVerse#12 { \transparent { Je vois couler son sang. } C’est toi qui l’a versé. }
\livretPers Arsane
\livretVerse#12 { La Reine par mon bras, a perdu la lumiere ! }
\livretPers Zoroastre
\livretVerse#12 { Ton sort est plus affreux, Arsane, c’est ta Mere. }
\livretPers Semiramis
\livretVerse#12 { Vous, mon Fils ! quoi je meurs par la main de mon Fils ! }
\livretVerse#10 { Dieux inhumains, vous me l’aviez promis. }
\livretVerse#12 { Ce jour termine enfin mes malheurs & mes crimes. }
\livretPers Arsane
\livretVerse#12 { Terre, pour m’engloutir, ouvre-moi tes abîmes. }
\livretPers Semiramis
\livretVerse#8 { Amestris, calmez ses fureurs. }
\livretVerse#12 { Je vous laisse en mourant la suprême puissance, }
\livretVerse#12 { Le Soleil désormais luira sur l’innocence. }
\livretVerse#12 { De l’éternelle nuit j’entrevois les horreurs. }
\livretVerse#12 { Ninus approchez-vous… je m’affoiblis… je meurs. }
\sep
}
