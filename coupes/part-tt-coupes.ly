\version "2.19.80"
\include "common.ily"

\paper {
  #(set! bookTitleMarkup shortBookTitleMarkup)
}
\header { title = "Semiramis" }

\act "ACTE V"
\sceneDescription\markup\wordwrap-center {
  Le Théâtre représente le Tombeau de \smallCaps Ninus,
  Roy de Babylone : il est au milieu d’une Forest.
}
\scene "Scene Premiere" "Scene I"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Zoroastre, Semiramis.
}
% 5-1
\pieceTocNb "5-1" "Ritournelle"
\includeScore "EAAritournelle"

% 5-2
\pieceTocNb "5-2" \markup\wordwrap {
  Zoroastre, Semiramis :
  \italic { Quoy ! la mort d’Amestris s'aprête dans ces lieux }
}
\includeScore "EABrecit"

\scene "Scene Deuxiéme" "Scene II"
\sceneDescription\markup\wordwrap-center {
  \smallCaps { L'Ordonnateur, }
  Peuples du Babylone, qui viennent rendre hommage au Tombeau de Ninus.
}
% 5-3
\pieceTocNb "5-3" \markup\wordwrap {
   L’Ordonnateur, chœur :
  \italic { Au plus grand de nos Rois adressons nôtre hommage }
}
\includeScore "EBAordoChoeur"

% 5-4
\pieceTocNb "5-4" "Premier air"
\includeScore "EBBair"

% 5-5
\pieceTocNb "5-5" \markup\wordwrap {
   L’Ordonnateur :
  \italic { Fille de la Valeur, immortelle Victoire }
}
\includeScore "EBCordonnateur"
