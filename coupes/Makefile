OUTPUT_DIR=../out
DELIVERY_DIR=/Users/nicolas/Dropbox/LesOmbres/Semiramis/Versailles/

NENUVAR_LIB_PATH=$$(pwd)/../../nenuvar-lib
LILYPOND_CMD=lilypond -I$$(pwd)/../ -I$(NENUVAR_LIB_PATH) \
  --loglevel=WARN -ddelete-intermediate-files \
  -dno-protected-scheme-parsing
PROJECT=Destouches_Semiramis_coupes

# Conducteur
conducteur:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT) main-coupes.ly
.PHONY: conducteur
# Dessus
dessus:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-dessus -dpart=dessus part-coupes.ly
.PHONY: dessus
# Dessus & Hautes-contre
dessus-hc:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-dessus-hc -dpart=dessus-hc part-coupes.ly
.PHONY: dessus-hc
# Dessus 2 & Hautes-contre
dessus2-hc:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-dessus2-hc -dpart=dessus2-hc part-coupes.ly
.PHONY: dessus2-hc
# Hautes-contre
haute-contre:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-haute-contre -dpart=haute-contre part-coupes.ly
.PHONY: haute-contre
# Tailles
taille:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-taille -dpart=taille part-coupes.ly
.PHONY: taille
# Basses
basse:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-basse -dpart=basse part-coupes.ly
.PHONY: basse
# Basse continue
basse-continue:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-basse-continue -dpart=basse-continue part-coupes.ly
.PHONY: basse-continue
# Trompettes
trompette:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-trompette -dpart=trompette part-coupes-tt.ly
.PHONY: trompette
# Timbales
timbales:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-timbales -dpart=timbales part-coupes-tt.ly
.PHONY: timbales
# Chant
bc-chant:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-bc-chant -dpart=chant part-coupes.ly
.PHONY: bc-chant


delivery:
	@mkdir -p $(DELIVERY_DIR)/
	@if [ -e $(OUTPUT_DIR)/$(PROJECT).pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT).pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-dessus.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-dessus.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-dessus-hc.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-dessus-hc.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-dessus2-hc.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-dessus2-hc.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-haute-contre.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-haute-contre.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-taille.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-taille.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-basse.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-basse.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-basse-continue.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-basse-continue.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-trompette.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-trompette.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-timbales.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-timbales.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-bc-chant.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-bc-chant.pdf $(DELIVERY_DIR)/; fi
#	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-1.midi ]; then tar zcf $(DELIVERY_DIR)/$(PROJECT)-midi.tar.gz $(OUTPUT_DIR)/$(PROJECT).midi $(OUTPUT_DIR)/$(PROJECT)-[0-9]*.midi; fi
#	git archive --prefix=$(PROJECT)/ HEAD . | gzip > $(DELIVERY_DIR)/$(PROJECT).tar.gz
.PHONY: delivery

clean:
	@rm -f $(OUTPUT_DIR)/$(PROJECT)-* $(OUTPUT_DIR)/$(PROJECT).*
.PHONY: clean

parts: dessus dessus-hc dessus2-hc haute-contre taille basse basse-continue trompette timbales bc-chant
.PHONY: parts

all: check parts conducteur delivery clean

.PHONY: all

check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in parent directory:"; \
	  echo " cd .. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi
.PHONY: check
