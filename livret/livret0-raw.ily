\notesSection "Livret"
\markuplist\with-line-width-ratio #0.7 {
\livretAct\larger LIVRET
\livretAct AVERTISSEMENT
\livretParagraph {
  Il est peu de Noms plus celebres que celui de Semiramis. Tous les
  Auteurs ont parlé de son ambition, de sa magnificence & de sa
  mort. Elle périt par la main de son Fils pour qui elle avoit conçû
  une passion criminelle. C’est cet évenement qu’on met sur la
  Scene. On a cherché pour l’amener, les moyens les moins odieux & les
  plus interessans.
}
\livretParagraph {
  On feint que le Ciel est irrité des crimes de Semiramis, qui menacée
  d’être tuée par son Fils, l’avoit fait exposer au moment de sa
  naissance. Maîtresse du Trône, elle y veut placer Arsane jeune
  inconnu qu’elle aime, & en éloigner Amestris sa Niéce, héritiere de
  l’Empire. Elle l’oblige à se consacrer au culte des Dieux, & se sert
  du prétexte de les appaiser, par le choix d’une Prestresse du Sang
  Royal. Le Ciel n’y consant pas ; il veut une Victime. L’Amibiguïté
  des Oracles, si conforme aux détours par lesquels il conduit ses
  vangeances, fait tomber l’aparence du peril sur Amestris. C’est pour
  la délivrer qu’Arsane son Amant fait des efforts qui aboutissent,
  malgré lui, à la mort de Semiramis. Outre le soin qu’on a pris de
  cacher au Fils & à la Mere ce qu’ils sont l’un & l’autre, on a
  rejetté une partie de l’action sur Zoroastre Roy de la Bactriane,
  inventeur de la Magie, Contemporain de Semiramis & trahi par
  elle. Il rend Arsane furieux ; & le désespoir de l’un & le trouble
  de l’autre, servent à executer l’Arrest du Ciel contre la Reine.
}
\livretParagraph {
  Les remords dont elle combat sa passion, ceux qu’elle témoigne en
  reconnoissant son Fils, & en mourant, sont les secours par lesquels
  le Théâtre concilie la pitié aux personnages les plus coupables.
}
\livretParagraph {
  A l’égard d’Amestris, sa consécration n’est pas une idée contraire à
  la vrai-semblance, puisque tant d’Auteurs Sacrez & Profanes assurent
  que longtems avant les Vestales de Rome, l’Idolatrie avoir dévoüé
  des Vierges au service des Autels. On a choisi les circonstances
  dans lesquelles la Princesse se dévoüe sortie d’une longue
  captivité, liée par un serment & par la necessité du bonheur public,
  elle sacrifie ses droits à la Couronne, & une passion
  légitime. Enfin, ses malheurs sont réparez, & sa vertu récompensée.
}
\livretAct\wordwrap-center {
  ACTEURS DE LA TRAGEDIE
}
\fill-line {
  \column {
    \line {
      \character-ambitus #"vbas-dessus" ##{ re' la'' #}
      \smallCaps Semiramis, Reine de Babylone.
    }
    \line {
      \character-ambitus #"vbas-dessus" ##{ re' la'' #}
      \smallCaps Amestris, Princesse du Sang Royal.
    }
    \line {
      \character-ambitus #"vhaute-contre" ##{ fa si' #}
      \smallCaps Arsane ou \smallCaps Ninus, Fils de \smallCaps Semiramis,
      Amant \concat { d’ \smallCaps Amestris. }
    }
    \line {
      \character-ambitus #"vbasse" ##{ sol, fa' #}
      \smallCaps Zoroastre, Roy de la Bactriane,
      Amant de \smallCaps Semiramis.
    }
    \line {
      \character-ambitus #"vhaute-contre" ##{ sol si' #}
      \smallCaps { Un Babylonien. }
    }
    \line {
      \character-ambitus #"vbas-dessus" ##{ fa' fa'' #}
      \smallCaps { Une Babylonienne. }
    }
    \line {
      \character-ambitus #"vbas-dessus" ##{ re' sol'' #}
      \smallCaps { Une Prestresse de Jupiter. }
    }
    \line {
      \character-ambitus #"vbasse" ##{ do mi' #}
      \smallCaps { L'Ordonnateur } des Jeux funebres.
    }
    \line {
      \character-ambitus #"vhaute-contre" ##{ sol si' #}
      \smallCaps { Un Génie. }
    }
    \concat {
      \left-brace#100 \raise#9.5 \line {
        \column {
          \character-ambitus #"vbas-dessus" ##{ re' la'' #}
          \character-ambitus #"vhaute-contre" ##{ la do'' #}
          \character-ambitus #"vtaille" ##{ do sol' #}
          \character-ambitus #"vbasse" ##{ fa, fa' #}
        }
        \override #'(baseline-skip . 5) \column {
          \line { Chœur de Babyloniens. }
          \line { Chœur de Genies Elementaires. }
          \line { Chœur de Demons & de Magiciens. }
          \line { Chœur de Prêtres & Prêtresses de \smallCaps Jupiter. }
          \line {
            Chœur de Peuples pour les Jeux funestes de \smallCaps Ninus.
          }
        }
      }
    }
  }
}
}
