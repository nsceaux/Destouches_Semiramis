\livretAct ACTE III
\livretDescAtt\wordwrap-center {
  Le Théâtre représente une vestibule orné de Statues des Rois de Babylone.
}
\livretScene SCENE PREMIERE
\livretPers Zoroastre
\livretRef #'CAAzoroastre
%# Qu'ai-je appris! quels forfaits! quelle injure mortelle!
%# C'est pour un inconnu qu'on me manque de foi.
%# O Majesté des Rois! O puissance éternelle
%# Des Dieux, qu'atestoit l'Infidelle,
%# On vous outrage, comme moi.
%# Haine, transports jaloux, implacable colere,
%# Barbares enfans de l'Amour,
%# Eteignez son flambeau, que le vôtre m'éclaire;
%# Armez-vous contre lui, regnez à vôtre tour.
%# Mais, quel triste secours me promet ma vangeance!
%# C'est par mon cœur qu'elle commence.
%# De vains gemissemens, d'inutiles regrets,
%# Des cris perdus, des pleurs dont fremit ma constance,
%# Sont du plus tendre Amour l'unique récompense.
%# Non, non, de ma fureur déploy=ons tous les traits;
%# Accablons mon Rival, la Reine, ses Sujets.
%# Haine, transports jaloux, implacable colere,
%# Barbares enfans de l'Amour,
%# Eteignez son flambeau, que le vôtre m'éclaire;
%# Armez-vous contre lui, regnez à vôtre tour.

\livretScene SCENE DEUXIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Zoroastre, Semiramis.
}
\livretPers Zoroastre
\livretRef #'CBAsemiramisZoroastre
%# Ah! Perfide, osez-vous soûtenir ma présence?
\livretPers Semiramis
%# Ces noms injuri=eux me sont-ils adressez?
\livretPers Zoroastre
%# Avez-vous crû forcer mon dépit au silence?
\livretPers Semiramis
%# Oubli=ez-vous mon rang, & qui vous offensez?
\livretPers Zoroastre
%# Oubli=ez-vous le mien, & qui vous trahissez?
\livretPers Semiramis
%# Semiramis ne connoît point de Maître,
%# Le Ciel est seul juge des Rois.
\livretPers Zoroastre
%# Le Ciel vange sur eux le mépris de ses Loix,
%# Et vous l'éprouverez peut-être.
%# Quoi! les sermens les plus sacrez
%# N'ont pû fixer vôtre inconstance!
%# Eh! quel est le Rival que vous me préferez?
%# Un étranger sans nom, sans états, sans naissance?
\livretPers Semiramis
%# J'ignore ses A=yeux, je connois ses vertus.
\livretPers Zoroastre
%# Ingrate, il est donc vrai que vous ne m'aimez plus?
%# Tant de soins, tant d'amour, tant de perseverance,
%# Mon espoir, mon bonheur sont pour jamais perdus.
%# Que ne puis-je étouffer l'ardeur qui me dévore;
%# Que ne puis-je à mon tour oubli=er vos attraits,
%# Ces perfides attraits, que malgré moi j'adore.
%# Faut-il, quand vôtre cœur m'abandonne à jamais,
%# Que vos regards me retiennent encore?
\livretPers Semiramis
%# Eh bien, dans mes regards, lisez donc mes douleurs.
%# Pour vous vanger, joü=issez de mes pleurs.
%# Je veux, je crains, j'espere, & mon espoir me gêne;
%# Je combats, je résiste, & cede tour à tour:
%# Un penchant inconnu m'entraîne,
%# Plus puissant mille fois, & moins doux que l'amour.
%# Ah ! si vous connoissiez l'excès de mes allarmes,
%# Vous-même à mes malheurs vous donneriez des larmes.
\livretPers Zoroastre
%# N'aviez-vous à m'offrir que ce cru=el secours?
\livretPers Semiramis
%# Epargnons-nous d'inutiles discours,
%# Seigneur, respectons nôtre gloire.
%# D'un malheureux amour étouffez la memoire,
%# Laissez mon triste cœurs en proye =à ses remords.
%# C'est malgré moi, que je couronne Arsane...
\livretPers Zoroastre
%# Arsane! ah! que ce nom redouble mes transports!
%# C'en est fait, à perir vôtre amour le condamne.
\livretPers Semiramis
%# D'un aveugle couroux nous bravons les efforts.
%# Mais vous-même tremblez d'en être la victime.
%#- Le Ciel sera pour lui.
\livretPers Zoroastre
%#= Sera-t'il pour le crime,
%#- Il perira.
\livretPers Semiramis
%#= Craignez les Dieux & sa valeur.
\livretPers Zoroastre
%# Craignez Zoro=astre en fureur.
\livretPers Ensemble
%# Tonnez, Dieux immortels, tonnez, lancez la Foudre,
%# Perdez vos Ennemis, qu'ils tombent sous vos coups,
%# Frappez, éclatez, hâtez-vous!
%# Hâtez-vous de reduire en poudre
\livretVerse#12 { Les \line { \left-brace#20 \raise#1 \column { superbes parjures } \right-brace#20 } Mortels qui s’arment contre vous. }


\livretScene SCENE TROISIÉME
\livretPers Zoroastre
\livretRef #'CCAzoroastre
%# Pour tant de maux soufferts, quels maux dois-je lui rendre?
%# Et comment me vanger? mon art va me l'apprendre.
%# Terrible rampart des Enfers
%# Styx affreux, dont les flots environnent les ombres
%# Elevez jusqu'à moi vos vapeurs les plus sombres.
%# Que le Soleil vaincu se cache dans les Mers,
%# Que ce jour manque à l'Univers.
%# Lieux témoins de mon infortune,
%# Lieux que je fais envain retentir de mes cris,
%# Disparoissez, tombez, vôtre aspect m'importune.
%# Qu'un magique Palais naisse de vos débris.
\livretDidasP\justify {
  Le Théâtre change, & représente un Palais magique,
  orné de Statuës qui portent des flambeaux.
}
%# Qu’à ma fureur tout prête ici des armes.
%# Tristes Objets qui portez ces flambeaux,
%# Arrachez du sein des tombeaux;
%# Animez-vous. Demons, vangez mes larmes,
%# C'est moi qui le premier vous ai donné la Loi.
%# Vous Mortels instruits à mes charmes,
%# Venez de toutes parts, secondez vôtre Roi.

\livretScene SCENE QUATRIÉME
\livretDescAtt\wordwrap-center {
  \smallCaps Zoroastre, Troupe de Demons, de Magiciens & de Magiciennes.
}
\livretPers Chœur
\livretRef #'CDAchoeur
%# L'univers
%# Porte nos fers,
%# Le Dieux des mers
%# Pour nous fait la guerre.
%# Par nous le Tonnerre
%# Trouble les airs.
%# A nos voix Tremblent les Rois.
%# Toute la Terre
%# Cede à nos loix.
%# Tout mortel nous doit ses vœux.
%# Entrons en partage
%# D'encens & d'hommage
%# Avec les Dieux.
%# Sur ces bords,
%# Tous nos efforts
%# Vont vanger ta gloire:
%# Prevoi ta victoire
%# Dans nos transports.
\livretPers Zoroastre
%# Du Dieu du Styx Ministres inflexibles,
%# Commencez avec moi nos Mysteres terribles.
\livretRef #'CDBair
\livretDidasPPage\line { On danse. }
\livretPers Zoroastre
\livretRef #'CDCzoroastre
%# Semiramis a trahi mon ardeur,
%# Partagez cet outrage, & servez ma fureur.
\livretPersDidas Zoroastre alternativement avec le Chœur
\livretRef #'CDDchoeur
%# Versons l'épouvante
%# Dans les cœurs;
%# Que l'attente
%# Des malheurs
%# En augmente
%# Les horreurs.
%# Souflons la guerre.
%# Couvrons la terre
%# De sang & de morts.
%# Faisons des efforts
%# Egaux au tonnerre.
%# Soufflons la guerre
%# Peuplons les sombres bords.
\livretRef #'CDEair
\livretDidasPPage\line { On danse. }
\livretPers Chœur
\livretRef #'CDFchoeur
%# Commande à l'Empire
%# Tenebreux.
%# Tout conspire
%# Pour tes vœux.
%# Qu'on respire
%# Mille feux.
\livretRef #'CDGair
\livretDidasPPage\line { On danse. }
\livretPers Zoroastre
\livretRef #'CDHzoroastre
%# Arrêtez. Les Enfers sont prêts à m'inspirer,
%# Qu'en ce jour sur vos soins je puisse m'assurer.
%# Quel noir transport succede à ma douleur profonde!
%# Le Styx qui fait les loix & les crimes des Dieux,
%# Le Styx se découvre à mes yeux.
%# Quels funeste secrets me revele son onde!
%# Malheureuse Semiramis,
%# Tremble! de tes fureurs que le Destin condamne,
%# Ton Fils est échapé, je le vois, c'est Arsane...
%# Mais! quel spectacle affreux trouble encor mes esprits!
%# Le glaive est suspendu, Quelle illustre victime
%# Va se précipiter au tenebreux abîme?
%# Quel sang prêt à couler? de quels lugubres cris
%# Les Autels retentissent?
%# Le Peuple est consterné, les Dieux même en fremissent.
%# Et je sens que je m'attendris.
%# Eh! qui donc va perir? est-ce vous Amestris?
%# Tout fuit... Quelle confuse image!
%# Je ne vois plus qu'à travers un nu=age...
%# Je suis vangé. Je vois des malheureux...
%# De pleurs, de cris, de sang marquons ce jour affreux.
\sep
%\column-break
