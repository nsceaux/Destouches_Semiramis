
\livretAct ACTE I
\livretDescAtt\wordwrap-center {
  Le Théâtre représente un grand Sallon orné pour le Couronnement
  \concat { d’ \smallCaps Arsane } & de ses Nôces avec
  \smallCaps Semiramis.
}
\livretScene SCENE PREMIERE
\livretPers Semiramis
\livretRef #'AABsemiramis
%# Pompeux Aprêts, Fête éclatante,
%# Flambeaux sacrez, Autels ornez de fleurs,
%# Hymen si cher à mon attente,
%# Que vous m'allez couter de pleurs !

%# Rivale des Heros, que devient ma puissance?
%# Avec un Inconnu, j'en partage l'éclat ;
%# Je la mets à ses pieds, ma gloire s'en offense,
%# Et mon amour encor craint de faire un ingrat.

%# Pompeux Aprêts, Fête éclatante,
%# Flambeaux sacrez, Autels ornez de fleurs,
%# Hymen si cher à mon attente,
%# Que vous m'allez couter de pleurs !

%# Quels reproches Ninus, n'as-tu point à me faire ?
%# A périr en naissant, j'ai condamné mon Fils.
%# Pour éteindre la race, & les droits de ton frere,
%# Aux Autels j'enchaîne Amestris :
%# Et c'est une main étrangere,
%# Qui de mes attentats va recueillir le prix.
%# Triste Semiramis,
%# Faut-il que ton cœur te trahisse ?
%# Plus cru=els que les dieux qui désolent ces bords,
%# L'Amour te guide au precipice.
%# Arrête. Il n'est plus temps. quels combats ! quels remords ?
%# Justifi=ez, grands Dieux, ou calmez mes transports.
%# On vient. C'est Amestris... quelle est mon injustice !
%# Captive dès long-tems, quels maux elle a soufferts !
%# Je ne fais que changer ses fers.

\livretScene SCENE DEUXIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Amestris, Semiramis.
}
\livretPers Amestris
\livretRef #'ABBas
%# Reine, je vais remplir le destin qui m'appelle;
%# A mon serment vous me verrez fidelle.
%# Tandis que vous suivrez les traces des Heros,
%# Dans une retraite éternelle,
%# Mon cœur va chercher son repos.
\livretPers Semiramis
%# Au repos de ces lieux vôtre cœur s'interesse,
%# Nôtre félicité ne dépend que de vous,
%# Fille de Jupiter, vous serez sa Prêtresse,
%# Ses faveurs par vos mains vont descendre sur nous.
%# Babylonne est l'objet du couroux qui l'anime,
%# Et ce Dieu tant de fois invoqué par mes pleurs,
%# Demande dès long-tems une grande victime;
%# Je la cherchois en vain : vôtre effort magnanime,
%# Sans nous couter du sang, finira nos malheurs.
\livretPers Amestris
%# J'offre à nos Dieux des jours trop peu dignes d'envie.
\livretPers Semiramis
%# Ces Dieux en ont-ils fait de plus heureux pour moi?
%# Aux loix d'un Inconnu je vais être asservie,
%# Arsane en ce moment va devenir mon Roi.
\livretPers Amestris
%# Zoro=astre esperoit recevoir vôtre foi.
\livretPers Semiramis
%# Amestris, malgré-moi, je lui fais cette offense.
\livretPers Amestris
%# Eh! ne craignez-vous point les traits de sa vengeance?
%# Zoro=astre commande à cent peuples divers,
%# C'est peu que sa valeur ait fait trembler la Terre,
%# Ce nouveau Conquerant à dompté les Enfers;
%# Les secrets de l'Olympe à ses yeux sont ouverts:
%# Sa voix force les Dieux à lancer le Tonnerre.
\livretPers Semiramis
%# Arsane est adoré du peuple & des soldats,
%# Dans l'âge où l'on commence à s'instruire aux combats,
%# Il est maître de la Victoire.
%# J'ai vû tous nos Guerriers, soutenus par son bras,
%# Ranimer leur ardeur à l'éclat de sa gloire.
\livretPers Amestris
%# Puisse-t'il de nos maux effacer la memoire!
\livretPers Semiramis
%# Je vais amener vôtre Roi:
%# Partagez les honneurs de cette auguste fête,
%# Le sang, qui nous unit, vous en fait une loi.
%# Que du Bandeau sacré vos main ornent sa Tête.

\livretScene SCENE TROISIÉME
\livretPers Amestris
\livretRef #'ACAamestris
%# Mes yeux, mes tristes yeux, laissez couler vos larmes,
%# Foible secours des malheureux.
%# Faut-il que d'un amour, dont j'ai bravé les feux,
%# L'importun souvenir me cause tant d'allarmes?
%# Contre des maux si rigoureux
%# Faut-il que mes soupirs soient mes uniques armes?
%# Foible secours des malheureux,
%# Mes yeux, mes tristes yeux, laissez couler vos larmes.
%# Reine barbare, non, mes fers, ny ta rigueur,
%# N'ont point ébranlé ma constance:
%# Arsane, c'est pour toi que j'ai craint sa fureur.
%# Arsane, c'est à toi, qu'ingratte en apparence,
%# Je vais donner l'Empire au défaut de mon cœur.

\livretScene SCENE QUATRIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Arsane, Amestris.
}
\livretPers Arsane
\livretRef #'ADArecit
%# Vous, Princesse, en ces lieux! quel sort vous y ramene?
\livretPers Amestris
%# J'y viens être témoin du beau jour qui vous luit.
\livretPers Arsane
%# Que l'éclat de ce jour & me trouble & me gêne!
\livretPers Amestris
%# De vos nobles travaux vous recevez le fruit.
\livretPers Arsane
%# Ingrate, ignorez-vous quel effort m'y reduit?
\livretPers Amestris
%# L'*Hymen qui d'une main vous prépare sa chaîne,
%# Vous présente de l'autre un Empire éclatant
%# Comblé de tant d'honneurs, n'êtes-vous pas content?
\livretPers Arsane
%# Mon cœur ne l'eût été qu'à vaincre vôtre haine.
%# Je n'adorois que vous; tant de soins, tant de pleurs,
%# De si tendres soupirs, une ardeur si sincere,
%# Rien n'a pû fléchir vos rigueurs:
%# C'est vous qui m'enchaînez à ces tristes honneurs,
%# Et je vais me punir de n'avoir pu vous plaire.
\livretPers Amestris
%#- Adieu, Seigneur…
\livretPers Arsane
%#= Mon cœur plus que jamais épris…
\livretPers Amestris
%# Adieu, Seigneur, oubli=ez Amestris.
\livretPers Arsane
%# Vous me fuy=ez… arrêtez Inhumaine.
\livretPers Amestris
%#8 Non, non, d'autres destins m'appellent.
\livretPers Arsane
%# Quel mépris.
\livretPers Amestris
%# Je n'écoute plus rien, je vais suivre la Reine.

\livretScene SCENE CINQUIÉME
\livretPers Arsane
\livretRef #'AEAarsane
%# Je vous entends Cru=elle, & je perds tout espoir.
%# Dieux! si d'un sang obscur j'ai reçû la naissance,
%# Deviez-vous au trépas arracher mon enfance?
%# C'en est trop. Recevons le suprême pouvoir,
%# Par ma vertu mon nom commence.

\livretScene SCENE SIXIÉME
\livretDescAtt\wordwrap-center {
  \smallCaps { Semiramis, Arsane, Amestris, }
  et les Peuples de Babilonne.
}
\livretPers Semiramis
\livretRef #'AFBrecit
%# Enfin, voici l'instant si cher à mes souhaits!
%# Venez, jeune Heros, venez, que mes sujets
%# Vous placent sur le Trône, où vous auriez dû naître,
%# Et dans leur Défenseur reconnoissent leur Maître.
\livretPers Arsane
%# Croirai-je qu'en ce jour ces Peuples redoutez,
%# Aux loix d'un Inconnu, sans murmure obé=ïssent?
%# Plus je vous vois pour moi prodiguer vos bontez,
%# Plus mes esprits sont agitez.
%#8 Peut-être les Dieux me punissent
%# D'usurper des honneurs que j'ai peu meritez.
\livretPers Semiramis
%# Vôtre valeur ardente à nous défendre,
%# Révele en vous le sang, ou des Rois, ou des Dieux.
%# Et quand je vous éleve à ce rang glori=eux
%# Je crois vous le donner bien moins que vous le rendre.
%# Chantez, Peuples, chantez, ré=ünissez vos voix,
%# Celebrez ce Heros, aplaudissez mon choix.
\livretRef #'AFCsemiramis
%# Vous recevez un Roi des mains de la Victoire,
%# Qu'il répande sur vous mille nouveaux bienfaits,
%# Qu'il regne, qu'il vous donne une éternelle paix;
%# Que les Dieux immortels ne séparent jamais
%# Et vôtre bonheur & sa gloire.
\livretPers Chœur
\livretRef #'AFDchoeur
%# Nous recevons un Roi des mains de la Victoire,
%# Qu'il répande sur nous mille nouveaux bienfaits,
%# Qu'il regne, qu'il nous donne une éternelle paix;
%# Que les Dieux immortels ne séparent jamais
%# Et nôtre bonheur & sa gloire.
\livretRef #'AFEchaconne
\livretDidasPPage On danse.
\livretPers Une Babilonienne
\livretRef #'AFFbabyloniens
%# Dieu charmant de Cythere,
%# Répand tes faveurs;
%# Et du soin de te plaire
%# Rempli tous nos cœurs.
%# De tes flâmes
%# Nos ames
%# Sentent les douceurs.
%# Plus de peines,
%# Tes chaînes
%# Sont faites de fleurs.
\livretPers Chœur
%# Dieu charmant de Cythere,
%# Répand tes faveurs;
%# Et du soin de te plaire
%# Rempli tous nos cœurs.
%# De tes flâmes
%# Nos ames
%# Sentent les douceurs.
%# Plus de peines,
%# Tes chaînes
%# Sont faites de fleurs.
\livretPers Un Babilonien
%# De la grandeur suprême
%# Les Dieux sont jaloux;
%# Mais l'Amour est le même
%# Pour eux & pour nous.
%# Trop aimable Jeunesse,
%# Craignez moins ses coups:
%# Si ce Dieu ne vous blesse,
%# Quel bien goûtez-vous?
\livretDidasPPage On danse.
\livretPers Un Babilonien & une Babilonienne
%# Doux Empire,
%# Dont les loix sont nos desirs,
%# Quel martyre
%# De résister aux plaisirs!
%# Qu'on soupire!
%# On ne respire
%# Que du jour,
%# Où l'Amour
%# Nous inspire.
%# Doux Empire,
%# Dont les loix sont nos desirs,
%# Quel martyre
%# De résister aux plaisirs!
\livretPers Chœur
%# Celebrons tous tes charmes,
%# Ranime nos voix,
%# Loin de nous tes allarmes.
%# Chantons mille fois:
%# Tendre Amour, de tes armes
%# Laisse-nous le choix.
%# Sans ennuis & sans larmes
%# Vivons sous tes loix.
\livretPers Un Babilonien & une Babilonienne
%# Pour prix de ta victoire
%# Rend nos cœurs contens.
\livretPers Chœur
%# Quel tri=omphe! que d'heureux instants!
%# Quelle gloire! quels transports charmants!

\livretPers Semiramis
\livretRef #'AFGrecit
%# C'est assez. Il est tems d'achever mon ouvrage;
%# Amestris, approchez, faites vôtre devoir.
\livretPers Amestris
%# Seigneur, du suprême pouvoir
%# C'est donc à moi de vous offrir le gage.
%# Mon sang m'avoit donné des droits sur vos Etats;
%# Vivez heureux, regnez, je n'en murmure pas.
%# Joü=issez à jamais de la faveur celeste;
%# Et recevez mes vœux, c'est tout ce qui me reste...
\livretPers Arsane
%# Quel présent! quelle main vient ici me l'offrir!
\livretDidasP\line { \hspace#30 à part. }
%# Genereuse Amestris... Non, dussai-je perir...

\livretRef #'AFHtonnerre
\livretDidasPPage\line { L’Autel est brisé par le Tonnerre. }
\livretPers Semiramis & Arsane
%# Quel tourbillon de feux s'éleve & nous sépare?
%# Quelle *horreur! quels mugissemens!
%# La terre tremble, s'ouvre & montre le Tenare,
%# Le Ciel confond les Elemens.
\livretPers Chœur
%# Quels déluges brûlans tombent de toutes parts?
%# Tu fuis Soleil, tu fuis! quel est le crime
%# Qui te dérobe à nos regards?
%# Ciel, que voulez-vous pour victime?
\livretPers Semiramis
%# Tout l'Olympe en couroux s'arme-t'il contre moi?
%# Dieux, me punissez-vous d'avoir trahi ma foi?
\sep
%\column-break
