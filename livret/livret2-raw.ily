
\livretAct ACTE II
\livretDescAtt\wordwrap-center {
  Le Théâtre représente l’avant-cour du Palais de Semiramis.
  On voit un Temple dans l’éloignement d’un des côtez.
}
\livretScene SCENE PREMIERE
\livretRef #'BAAarsaneAmestris
\livretDescAtt\wordwrap-center\smallCaps {
  Arsane, Amestris.
}
\livretPers Ensemble
\livretPersVerse Arsane \line {
  \livretVerse#8 { Non, ne craignez point de m’entendre. }
}
\livretPersVerse Amestris \line {
  \column {
    \livretVerse#8 { Non, je ne veux point vous entendre. }
    \livretVerse#12 { Les Dieux sont en couroux, songez à les calmer. }
  }
}
\livretPers Arsane
%# C'est vous que je dois désarmer;
%# J'ai trop de graces à vous rendre.
\livretPers Ensemble
\livretPersVerse Arsane \line {
  \livretVerse#8 { Non, ne craignez point de m’entendre. }
}
\livretPersVerse Amestris \line {
  \livretVerse#8 { Non, je ne veux point vous entendre. }
}
\livretPers Arsane
%# Dois-je éprouver encor vôtre injuste rigueur,
%# Quand le Ciel avec moi paroît d'intelligence?
%# Voulez-vous bannir l'esperance,
%# Qu'il vient ramener dans mon cœur?
\livretPers Amestris
%# Pouvez-vous perdre sans allarmes,
%# L'attente d'un sort éclatant?
%# Pour changer vôtre cœur ne faut-il qu'un instant?
%# Et la gloire pour vous n'a-t'elle plus de charmes?
\livretPers Arsane
%# Non, mon cœur n'a jamais changé:
%# A ses premiers désirs, il fut toujours fidelle;
%# Vos yeux n'ont-ils pas vû ma contrainte mortelle,
%# Et l'*horreur où j'étois plongé?
%# Non, mon cœur n'a jamais changé!
%# Mais, nous n'avez rien vû, Cru=elle que vous êtes?
%# Amestris, insensible à mes peines secretes,
%# Craignoit d'en suspendre le cours:
%# Amestris, insensible à mes peines secretes,
%# Détournoit des regards que je cherchois toûjours...
%# Eh! vous me les cachez encore?
\livretPers Amestris
%# Un auguste serment doit engager ma foi.
\livretPers Arsane
%#- Quel est donc ce serment;
\livretPers Amestris
%#= Je ne suis plus à moi.
\livretPers Arsane
%# Expliquez-vous. Calmez l'*horreur qui me dévore.
\livretPers Amestris
%# Pour la derniere fois recevez mes adieux;
%# Ne suivez plus mes pas, c'est un soin inutile.
\livretPers Arsane
%#- Où fuy=ez-vous?
\livretPers Amestris
%#= Au Temple, & c'est là mon azile.
%# Par des nœuds éternels, je vais m'unir aux Dieux.
\livretPers Arsane
%# Et moi je vous dispute à ces Rivaux terribles;
%# Et vous me trouverez entre l'Autel & vous.
\livretPers Amestris
%# Ah! Seigneur, étouffez un impuissant couroux.
%# Aux profanes Mortels ces lieux inaccessibles,
%# Ont en dépôt la foudre, entendez ces éclats:
%# Je la suspens encor par mon obé=issance:
%# Craignez ces Dieux, tremblez & ne me forcez pas
%# D'implorer contre vous leur terrible vengeance.
\livretPers Arsane
%# Ah! dûssai-je y trouvez le plus cru=el trépas
%#- Je ne souffrirai point...

\livretScene SCENE DEUXIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Semiramis, Arsane.
}
\livretPers Semiramis
\livretRef #'BBArecit
%#= Ou courez-vous Arsane?
%# Quel trouble agite vos esprits?
\livretPers Arsane
%# Reine, qu'ai-je entendu! que devient Amestris?
%# Elle fuit de ces lieux. Eh! qui donc l'y condamne?
\livretPers Semiramis
%# Les Dieux, ses volontez, la Paix de mes Etats.
\livretPers Arsane
%# Eh quoi! vous n'y résistez pas!
\livretPers Semiramis
%# J'ai fait perir mon Fils pour conserver l'Empire.
%# Les Dieux me menaçoient de perir par son bras.
%# Amestris est d'un sang qu'il est tems de proscrire.
\livretPers Arsane
%# De quoy l'accusez-vous? quels sont ses attentats?
\livretPers Semiramis
%#- Et vous, quel intérêt?...
\livretPers Arsane
%#= Celui de vôtre gloire,
%#- Le repos de vos jours.
\livretPers Semiramis
%#= Du moins j'aime à le croire.
%# Le tems dévoilera ce mystere à mes yeux.
%# Mais loin de s'appaiser, que demandent les Dieux?
%# Quel obstacle nouveau font-ils ici renaître?
%# Zoro=astre est prêt d'y paroître.
%# Son char aussi brillant, que le flambeau du jour,
%# Plus prompt que les éclairs, vole & fend les nu=ages.
%# Mon Peuple admire, & tremble tour à tour,
%# Et l'encens à la main, l'attend sur ces rivages;
%# Ainsi, vôtre Rival m'apporte ses hommages,
%# Dans l'instant où pour vous je trahis son amour.
%# Il vient, faisons-nous vi=olence.
\livretPers Arsane
%# Croy=ez-vous l'abuser, ou braver sa vengeance?
\livretPers Semiramis
%# Arsane, s'il n'a pas consulté les Enfers,
%# Il ignore encor mon offense.
\livretPers Arsane
%#- Tout va l'en éclaircir.
\livretPers Semiramis
%#= Préparez ma défense,
%# Assemblez mes Soldats, les momens nous sont chers.
\livretPers Arsane
%# Je suivrai des devoirs dont rien ne me dispense.
%# Si par le Ciel mes vœux sont secondez,
%# Je ferai plus pour vous que vous ne demandez.

\livretScene SCENE TROISIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Zoroastre, Semiramis.
}
\livretPers Zoroastre
\livretRef #'BCBzoroastreSemiramis
%# Belle Semiramis, l'amour & l'esperance
%# Par des chemins nouveaux m'amenent dans ces lieux.
%# Le charme de vôtre présence
%# A déja réparé l'extrême vi=olence
%# Des maux que j'ai soufferts, éloigné de vos yeux.
%# Le Dieu qui lance le Tonnerre
%# M'a remis un pouvoir peu different du sien;
%# Il m'a rendu des Rois l'Arbitre & le Soûtien.
%# J'éteins & j'allume la guerre,
%# Je fais le destin de la terre,
%# Et c'est dans vos beaux yeux que je cherche le mien.
\livretPers Semiramis
%# Vos plus fiers Ennemis vous cedent la victoire.
%# Vôtre Art, vôtre Valeur peuvent tout surmonter:
%# Un cœur tel que le mien pouroit-il se flatter,
%# De manquer seul à vôtre gloire?
\livretPers Zoroastre
%# Peuples des Elemens, paroissez à mes yeux.
%# Esprits, qui par l'Amour dispersez en tous lieux,
%# Sur la Terre & sur l'Onde étendez son Empire;
%# Vous qui volez avec lui dans les Cieux,
%# Joignez tous vos transports à l'ardeur qui m'inspire.
%# De ce jour célebre pour nous,
%# Rendez à l'avenir la memoire durable.
%# L'ouvrage des Mortels comme eux est perissable,
%# Dressez un monument immortel comme vous.
%# Qu'une nouvelle Flore exhale
%# Des parfums du ciel descendus.
%# Et que ces jardins suspendus
%# De la terre & des cieux remplissent l'intervalle.

\livretScene SCENE QUATRIÉME
\livretDescAtt\center-column {
  \wordwrap-center {
    Le Théâtre change, & représente les célébres Jardins de Semiramis.
  }
  \wordwrap-center {
    \smallCaps { Zoroastre, Semiramis, }
    Troupes de Genies Elementaires, & de Peuples.
  }
}
\livretPers Un Genie
\livretRef #'BDBair
%# L'Art plus prompt que la Nature,
%# Dans ces beaux lieux rassemble en même tems,
%# Et des Fleurs & des Fruits la ri=ante parure;
%# On voit l'Automne à côté du Printems.
%# Aimable maître de nos ames,
%# Amour, ferois-tu moins en faveur de nos flâmes?
%# Fai naître, & comble nos désirs,
%# Rassemble en même tems l'espoir & les plaisirs.
\livretPers Zoroastre
\livretRef #'BDCzoroastreChoeur
%# Formez les plus tendres concerts.
%# Chantez une Reine charmante:
%# Que de son Nom retentissent les airs,
%# Qu'il vole en cent climats divers:
%# A cette Fête éclatante,
%# Appellez tout l'Univers.
\livretPers Chœur
%# Formons les plus tendres concerts.
%# Chantons une Reine charmante.
%# Que de son Nom retentissent les airs,
%# Qu'il vole en cent climats divers.
%# A cette Fête éclatante,
%# Appellons tout l'Univers.
\livretRef #'BDDair
\livretDidasPPage\line { On danse. }
\livretPersDidas Un Genie alternativement avec le CHŒUR.
\livretRef #'BDFgenieChoeur
%# Paroissez jeunes Zephirs,
%# Excitez, animez Flore.
%# Que l'ardeur de vos soupirs
%# Hâte ses présens d'éclore.
%# Qu'on les doive à vos plaisirs,
%# Plutôt qu'aux pleurs de l'Aurore.
%# Que Venus sur ce rivage
%# Fixe sa brillante Cour,
%# Qu'on entende nuit & jour
%# Des Oyseaux le doux ramage,
%# Des amans le tendre *hommage,
%# Et l'éloge de l'Amour.
\livretRef #'BDHair
\livretDidasPPage\line { On danse. }
\livretPersDidas Le Genie alternativement avec le CHŒUR.
\livretRef #'BDJgenieChoeur
%# Au Dieux d'Amour il faut se rendre,
%# Lui seul apprend l'art d'être *heureux.
%# Pourquoy se plaindre de ses feux,
%# Que sert d'attendre,
%# Craint-on de prendre
%# De si beaux nœuds?
%# Ne perdez pas des jours aimables,
%# Mais moins durables,
%# Que les Zephirs.
%# Les soins jaloux & les soupirs
%# Sont-ils sans charmes?
%# Non, jusqu'aux larmes,
%# Tout est plaisirs.
\livretPersDidas Zoroastre à Semiramis.
\livretRef #'BDMrecit
%# De vos nouveaux Sujets voy=ez quelle est l'ardeur,
%# Répondez à mes feux, répondez à leur zele;
%# Je veux devoir l'instant de mon bonheur
%# Bien moins à vos sermens, qu'à mon amour fidele.
\livretPers Semiramis
%# Seigneur, il n'est pas tems d'accomplir vos projets.
%# Amestris est encor trop chere à mes sujets,
%# Je veux contr'elle assurer ma puissance:
%# Je ne puis vous offrir que ma reconoissance.

\livretScene SCENE CINQUIÉME
\livretPers Zoroastre
\livretRef #'BEArecit
%# Qu'ai-je entendu? quel soupçon! quel effroi
%# Dans mon cœur agité, s'éleve malgré moi!
%# Je veux les éclaircir... Amour, soy=ez mon guide:
%# Mais si je n'ai brûlé que pour une perfide,
%# Fureur, pour me vanger je n'écoute que toi.

\sep
%\column-break
