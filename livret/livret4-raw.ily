\livretAct ACTE IV
\livretDescAtt\wordwrap-center {
  Le Théâtre représente le Temple de Jupiter Belus,
  orné pour la Consecration \concat { d’ \smallCaps Amestris. }
}
\livretScene SCENE PREMIERE
\livretPers Arsane
\livretRef #'DAAarsane
%# Ou suis-je? quelle *horreur agite mes esprits?
%# C'est ici que le Dieu du Ciel & de la Terre
%# Présente sa splendeur à nos regards surpris.
%# Y viens-je braver son Tonnerre,
%# Et du sein des Autels arracher Amestris!
%# Qu'importe que la foudre à mes yeux étincelle!
%# Dieu barbare, Dieu jaloux,
%# Frappez: je vais au devant de vos coups,
%# Trop heureux de perir pour elle,
%# Dieu barbare, Dieu jaloux,
%# Non, tant que je respire, elle n'est point à vous.

\livretScene SCENE DEUXIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Semiramis, Arsane.
}
\livretPers Semiramis
\livretRef #'DBArecit
%# Arsane, quel spectacle ici vient me surprendre?
%# Prés de ces murs sacrez, on arrête mes pas.
%# Eh! qui donc contre moi revolte mes Soldats?
%# Seroit-ce Zoro=astre? à qui je dois m'en prendre?
\livretPers Arsane
%# Contre tous ses efforts je sçaurois vous défendre:
%# Mais un peril plus grand doit causer vôtre effroi:
%# Le Peuple aime Amestris, il peut tout entreprendre,
%#- Il s'arme contre vous.
\livretPers Semiramis
%#= Non, Perfide, c'est toi;
%# C'est toi qui me trahis, mes maux sont ton ouvrage.
%# Mais, tu n'as pas long-tems jou=i de mon erreur,
%# J'ai lû, je lis encor dans le fond de ton cœur.
%# Cœur indigne du Trône, & fait pour l'esclavage;
%# J'y vois la trahison, le mépris des bienfaits,
%# J'y vois contre mes jours tes barbares projets,
%# Et tes lâches soupirs pour celle qui m'outrage.
\livretPers Arsane
%# Arsane ne sçait point dissimuler ses feux.
%# L'ensensible Amestris occupoit tous mes vœux,
%# Avant que vôtre main vint m'offrir tant de gloire;
%# Je l'adore malgré ses mépris rigoureux,
%# C'est de l'Amour sur moi la premiere victoire.
\livretPers Semiramis
%# Redouble cet Amour, il me vange encor mieux.
%# Tu la perds: de leur choix demande compte aux Dieux.
\livretPers Arsane
%# Osez-vous attester des noms si redoutables?
%# Ont-ils parlé ces Dieux, sçait-on leurs volontez?
%# Non, la soif de regner, les fureurs implacables,
%# Sont les Dieux que vous consultez.
%# Ah! ne demandez plus, d'où naissent les présages;
%# Quel crime attire ici la foudre & les orages?
%# Vous attentez aux droits dont le Ciel est jaloux,
%# Et la justice éclate à se vanger de vous.
\livretPers Semiramis
%# Je fais pour le fléchir, un effort inutile.
%# Mais, Barbare, est-ce à toi de me le reprocher?
%# J'esperois avec toi goûter un sort tranquille;
%# Auprès de tes vertus je cherchois un azile.
%# Non, ta haine pour moi ne sçauroit se cacher;
%# Augmente tes mépris, tri=omphe de mes larmes,
%# Contre toi prête-moi des armes.
\livretDidasPPage\line { a part. }
%# Quel ascendant fatal m'a soumise à sa Loi!
\livretDidasPPage\line { à Arsane. }
%# Ingrat, que m'as-tu fait pour m'attendrir pour toi?
\livretPers Arsane
%# Achevez, & brisez les fers de la Princesse:
%# Sauvez de tant de Rois le reste préci=eux:
%# Je ne demande point de l'unir à mes vœux;
%# Je nourris dans mon cœur une vaine tendresse.
\livretPers Semiramis
%# Eh! pourquoi donc l'aimer avec tant de transport!
%# Tu partages enfin les rigueurs de mon sort:
%# Tu connois le tourment d'aimer qui nous abhorre;
%# Que n'y puis-je ajoûter encore
%# De la rendre sensible, & perfide pour toi.
%# Va, devien, s'il se peut, plus malheureux que moi.
\livretPers Arsane
%# On vient. Voici l'instant du cru=el sacrifice.

\livretScene SCENE TROISIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Arsane, Amestris, Semiramis.
}
\livretPersDidas Arsane à Amestris
\livretRef #'DCArecit
%# Ah! Princesse, faut-il que rien ne vous fléchisse!
\livretPers Amestris
%# Arsane, respectez Amestris & les Dieux.
%# Quels sont vos droits sur moi, pourquoi troubler mes vœux?
%# De mes jours à nos Dieux, je fais un libre *hommage.
%# J'ai calmé les transports d'un Peuple audaci=eux.
%# Laissez à ma vertu consommer son ouvrage.
\livretPers Arsane
%# Non, laissez-moi sortir de ces funestes lieux.
%# Je vais de mes Soldats ranimer le courage.

\livretScene SCENE QUATRIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Semiramis, Amestris.
}
\livretPers Ensemble
\livretRef #'DDAsemiramisAmestris
%# Soutenez Dieux immortels,
%# La majesté de vos Autels.

\livretScene SCENE CINQUIÉME
\livretDescAtt\wordwrap-center {
  \smallCaps { Semiramis, Amestris, }
  Prestres & Prestresses de Jupiter, & le Peuple.
}
\livretPersDidas Chœur des Prestresses
\livretRef #'DEBchoeur
%# Fille de l'Innocence,
%# Mere de la Paix,
%# Douce indifference,
%# Nos cœurs satisfaits
%# Goûtent vos attraits;
%# Des jours sans nu=age
%# S'élevent sur nous.
%# Les biens les plus doux
%# Sont nôtre partage.
%# Ce n'est pas à vous
%# Qu'on doit son hommage,
%# Fortune volage,
%# Nous bravons vos coups.
\livretPers Amestris
\livretRef #'DECamestris
%# J'immole aux Dieux le printems des mes jours;
%# A l'ombre des Autels avec vous je vais vivre:
%# Heureuse, si vôtre secours
%# De mes troubles secrets pour jamais me délivre!
\livretPersDidas Chœur des Prestres & des Prestresses
\livretRef #'DEDchoeur
%# Digne sang des Rois,
%# Le Ciel vous appelle,
%# Soûtenez son choix;
%# Le Ciel vous appelle;
%# Au Peuple fidelle
%# Dispensez ses Loix.
\livretPers Petit Chœur
%# Sensible à vos vœux
%# Le Dieu du Tonnerre
%# Eteindra ses feux;
%# Et par vous, la Terre
%# Va s'unir aux Cieux.
\livretPers Tous
%#5 Digne sang des Rois, &c.
\livretPersDidas Amestris
\smaller presente l’Encens & les Fleurs à la Statuë de Jupiter.
\livretRef #'DEEamestris
%# Recevez cet Encens, ces Couronnes de fleurs,
%# Hommages innocens que vous rend la nature.
%# Je viens y joindre encor une offrande plus pure,
%# Des vœux toûjours nouveaux, seul tribut de nos cœurs.
\livretPers Semiramis
\livretRef #'DEFsemiramis
%# Tri=omphez, Dieu puissant, qui regnez sur les Dieux,
%# Qu'on vous rende par tout un éternel hommage,
%# Versez sur ces climats mille dons préci=eux,
%# Loin de ces tristes lieux laissez grondez l'orage.
\livretPers Chœur
\livretRef #'DEGchoeur
%#12 Tri=omphez, Dieu puissant, &c.
\livretRef #'DEHrondeau
\livretDidasPPage On danse.
\livretPersDidas Une Prestresse
\smaller à Amestris, alternativement avec le Chœur.
\livretRef #'DEIpretresseChoeur
%# L'Amour verse des larmes,
%# Vous causez ses pleurs;
%# Il devoit par vos charmes
%# Vaincre tous les cœurs.
%# Sa plus chere esperance
%# S'éteint à jamais!
%# Eh! quels yeux désormais
%# Etendront sa puissance,
%# Quels seront ses traits?
\livretPers Chœur
%#6 L'Amour verse, &c.
\livretPers La Prestresse
%# Le foible *honneur de plaire
%# Coûte des tourmens.
%# La victoire est trop chere;
%# Fuy=ons les Amans.
%# Des momens plus tranquilles
%# Vont couler pour vous.
%# Goûtez dans nos aziles
%# Les biens les plus doux.
\livretPers Chœur
%#6 L'Amour verse, &c.
\livretRef #'DEJair
\livretDidasPPage On danse.
\livretPers La Prestresse
\livretRef #'DEKpretresse
%# Beaux Lieux, soy=ez toûjours exempts d'allarmes.
%# Amour, n'en trouble point la paix.
%# Trop de pleurs suivent tes traits,
%# Que tes armes,
%# Que tes charmes,
%# S'en éloignent pour jamais.
%# Non, non, à des plaisirs purs & faciles
%# Ne mêle point des soins fâcheux.
%# Ces aziles
%# Si tranquiles,
%# Ne redoutent point tes feux.
%# Vole, descens Amour, vien dans ces lieux,
%# Nos cœurs y bravent ta victoire:
%# Vole, descends Amour, vien dans ces lieux,
%# Voi ta défaite, & nôtre gloire.
\livretRef #'DELair
\livretDidasPPage On danse.
\livretPers Semiramis
\livretRef #'DENsemiramisAmestris
%# Amestris, achevez ce noble sacrifice.
%# Qu'il nous rende le Ciel propice.
%# Auguste Interprete des Dieux,
%# Vous tiendrez dans vos mains le bonheur de ces lieux.
\livretPersDidas Amestris la main sur l’Autel.
%# Je quitte pour jamais l'éclat qui m'environne,
%# Maître des Immortels, remplissez tout mon cœur,
%# La pompe, les plaisirs, la suprême grandeur
%# N'ont plus de droits sur moi, je vous les abandonne.
%# Rendez heureux les jours que je vous donne.
\livretPers Chœur
\livretRef #'DEOchoeur
%# Quel bruit affreux nous fait trembler!
%# Sous nos pas chancelans se dérobe la Terre,
%# Le Dieu menace, il s'arme, il lance le Tonnerre,
%# Ecoutez, fremissez, il est prêt à parler.

\livretScene SCENE SIXIÉME
\livretDescAtt\wordwrap-center {
  \smallCaps { L'Oracle, Amestris, Semiramis, }
  & les Acteurs de la Scene précédente.
}
\livretPers L'Oracle
\livretRef #'DFAoracle
%# Pour apaiser mon couroux légitime,
%# Amestris, c'est trop peu de vœux que tu me fais;
%# Au tombeau de Ninus va t'offrir en victime
%# Pour m'assurer le sang qu'exigent mes decrets.
\livretPers Amestris
\livretRef #'DFBamestris
%# J'obé=ïrai, grands Dieux, je vais vous satisfaire:
%# Je reçois une mort qui finit mes tourmens.
%# Reine, à vôtre repos, je ne suis plus contraire;
%# Laissez-moi m'occuper de mes derniers momens.
\livretPers Chœur
\livretRef #'DFCchoeur
%# Non, non, non, Dieux cru=els, gardez vôtre colere,
%# Tonnez plutôt sur nous, armez les Elemens.

\livretScene SCENE SEPTIÉME
\livretDescAtt\wordwrap-center {
  \smallCaps { Arsane, Amestris, }
  Prestres, Prestresses, & le Peuple.
}
\livretPers Arsane
\livretRef #'DGAaaChoeur
%# Quoi! tout me fuit, tout m'abandonne!
%# Quel prestige a glacé le cœur de mes Soldats!
%# Je les excite envain à marcher sur mes pas:
%# Mais, quel trouble nouveau, quelle *horreur m'environne!
\livretPers Chœur
%# Amestris va perir: c'est le Ciel qui l'ordonne.
\livretPersDidas Amestris au Peuple.
%# Dérobez-moi les pleurs que vous m'offrez;
%# Peuples, éloignez-vous: Arsane, demeurez.

\livretScene SCENE HUITIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Amestris, Arsane.
}
\livretPers Amestris
\livretRef #'DHArecit
%# Je vais subir la loi que le sort me dispense;
%# De tant d'honneurs promis à ma naissance,
%# Un seul tombeau me reste. Et ce jour que je vois,
%# Cette Terre, ces Cieux, tout va fuir devant moi.
\livretPers Arsane
%# Eh! vous y consentez, Grands Dieux, le puis-je croire!
%# Non, non, fuy=ez ces lieux, venez, sauvez vos jours.
\livretPers Amestris
%# Dois-je les conserver, aux dépens de ma gloire!
\livretPers Arsane
%# Quoi! vous me ha=ïssez jusqu'à fuir mon secours!
\livretPers Amestris
%# Seigneur, je ne puis que vous plaindre.
\livretPers Arsane
%# Dans quel triste moment, plaignez-vous mon malheur?
\livretPers Amestris
%# Lorsque je n'ai plus à vous craindre.
%# Aux portes du trépas je vous ouvre mon cœur.
%# J'ai connu vos vertus; & ma feinte rigueur
%# Ne m'a que trop causé d'allarmes;
%# Que ne pouvois-je, *helas! faire vôtre bonheur!
%# Le Ciel a vû seul ma douleur,
%# Il sçait combien pour vous, j'ai dévoré de larmes.
\livretPers Arsane
%# Et je vais vous perdre à jamais,
%# Est-ce à vous d'expi=er de coupables forfaits!
%# Que le Ciel s'embrase & qu'il tonne,
%# Que la guerre s'allume entre les Elemens.
%# De la superbe Babylone
%# Qu'ils renversent les fondemens:
%# Qu'importe quel trépas leur fureur nous apprête,
%# Vivez, & que sur moy retombe la tempête.
\livretPers Amestris
%# Un Mortel ose-t'il braver les Dieux vangeurs?
%# A leurs suprêmes loix, il faut que tout se rende.
\livretPers Arsane
%# Ah! qu'ils cherchent ailleurs leur sacrilege offrande.
\livretPers Ensemble
\livretVerse#10 {
  Ciel \raise#0.5 \left-brace#20 \raise#1.5 \column {
    \line { Equitable, oubliez ses fureurs. }
    \line { Implacable, épuisez vos rigueurs. }
  }
}
%# Tombent sur moi vos coups.
\livretPers Amestris
%# Non, laissez-moi mourir innocente victime;
%# Mais n'oubli=ez jamais le beau feu qui m'anime.
%# Adieu. Puisse le Ciel vous voir d'un œil plus doux!
%# Rendez ce Peuple *heureux, qu'il me retrouve en vous:
%# Qu'il rende à vos vertus un tribut légitime;
%# Que de vôtre bonheur rien ne borne le cours:
%# Qu'ils vous donnent ces Dieux, ce qu'ils m'ôtent de jours.
\livretPers Arsane
%# Moi! je serois complice de leur crime!
%# Reine barbare, infidelles Soldats:
%# Je vous attens, osez l'arracher de mes bras.

\livretScene SCENE NEUVIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Zoroastre, Arsane.
}
\livretPers Zoroastre
\livretRef #'DIArecit
%#- Arsane, où courez-vous?
\livretPers Arsane
%#= La sauver du trépas,
%# Je vais secourir l'innocence.

\livretScene SCENE DIXIÉME
\livretPers Zoroastre
\livretRef #'DJAzoroastre
%# Va, malheureux Amant, & plus malheureux Fils!
%# Enfer, tien-moi ce que tu m'as promis.
%# Fureurs, suivez ses pas, préparez ma vangeance.
%# Est-ce à moi d'épargner l'Ingrate qui m'offense.
\sep
%\column-break
