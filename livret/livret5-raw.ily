\livretAct ACTE V
\livretDescAtt\wordwrap-center {
  Le Théâtre représente le Tombeau de \smallCaps Ninus,
  Roy de Babylone : il est au milieu d’une Forest.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center\smallCaps {
  Zoroastre, Semiramis.
}
\livretPers Zoroastre
\livretRef #'EABrecit
%# Quoy! la mort d'Amestris s'aprête dans ces lieux,
%# Et ce Tombeau sacré va recevoir sa cendre?
\livretPers Semiramis
%# Tranquille sur son sort, elle ordonne les Jeux,
%# Les funebres honneurs, qu'à Ninus on va rendre;
%# C'est son dernier hommage à ce Roi glori=eux.
\livretPers Zoroastre
%# Soutiendrez-vous ce spectacle odi=eux?
\livretPers Semiramis
%# Contre l'Arrêt du Ciel qui pourroit la défendre?
\livretPers Zoroastre
%# Vous, si vos yeux s'ouvroient aux malheurs que je crains:
%# Je devrois vous ha=ïr, malgré moi je vous plains.
%# Voici ce moment redoutable;
%# Le destin d'Amestris attendrit tous les cœurs.
%# Sa mort trouvera des vangeurs,
%# Croy=ez-en mon effroi, le trouble qui m'accable,
%# Triste & dernier effort d'un amour déplorable.
\livretPers Semiramis
%# Redoutez moins un Peuple esclave de mes vœux:
%# Babylone craindra mon courage & nos Dieux.
\livretPers Zoroastre
%# Est-ce donc sur vos Dieux que vôtre espoir se fonde?
%# Une nuit profonde
%# Vous cache leurs coups.
%# Des Maîtres du monde
%# Le Ciel est jaloux:
%# Où fuir son couroux?
%# Sa haine feconde
%# Nous fait succomber:
%# La foudre qui gronde
%# Est prête à tomber.
\livretPers Semiramis
%# Tout accroit ma rage,
%# J'imite les Dieux.
%# Perisse à mes yeux
%# L'Objet qui m'outrage;
%# Que mes Ennemis
%# Soient reduits en poudre:
%# Un coup de la foudre
%# M'est cher à ce prix.
%# Que dis-je? d'Amestris le sort me fait envie.
%# Arsane la plaindra: Dieux, quels troubles secrets!
%# Ingrat, donne-moi tes regrets:
%# J'acheterois tes pleurs, aux dépens de ma vie.
\livretPers Zoroastre
%# Il est tems d'étouffer de coupables amours.
%#- Connoissez-vous Arsane?
\livretPers Semiramis
%#= Où tendent ces discours?
\livretPers Zoroastre
%# Je vais le découvrir ce funeste mystere.
%# Vos feux ont fait pâlir l'astre qui nous éclaire,
%# La terre en a tremblé, moi-même j'en fremis.
\livretPers Semiramis
%#- Quel coup vient me frapper!
\livretPers Zoroastre
%#= Arsane est vôtre Fils.
\livretPers Semiramis
%# Mon Fils... Et j'ai brûlé d'une flâme si noire?
%#- Qui vous l'a révélé?...
\livretPers Zoroastre
%#- Les Enfers.
\livretPers Semiramis
%#= Lui, mon Fils!...
%# Non, Barbare, je vois ce que tu t'es promis:
%# Tu le souhaites trop pour me le faire croire.
\livretPers Zoroastre
%# Vous connoîtrez l'erreur dont vos sens sont surpris.
\livretPers Ensemble
%# Brisez, brisez les nœuds d'une fatale chaîne.
\livretVerse#8 {
  \line { \raise#1 \column { Tremblez, Non, non, } \right-brace#20 }
  Votre esperance est vaine
}
\livretVerse#12 {
  Je \line { \left-brace#20 \raise#1 \column {
      \line { prévois des malheurs qui me vangent de vous }
      \line { crains peu des malheurs qui m’arrachent à vous } } }
}
\livretVerse#8 {
  Le Ciel gronde,
  \line {
    \left-brace#20
    \raise#1 \column { craignez j’attens }
    \right-brace#20 }
  ses coups.
}

\livretScene SCENE DEUXIÉME
\livretDescAtt\wordwrap-center {
  \smallCaps { L'Ordonnateur, }
  Peuples du Babylone, qui viennent rendre hommage au Tombeau de Ninus.
}
\livretPers L'Ordonnateur
\livretRef #'EBAordoChoeur
%# Au plus grand de nos Rois adressons nôtre *hommage
%# Remplissons de son nom & la Terre & les Airs.
%# Dieux immortels, vous dont il fut l'image,
%# Ecoutez nos concerts.
\livretPers Chœur
%#12 Au plus grand de nos Rois, &c.
\livretRef #'EBBair
\livretDidasPPage On danse.
\livretPers L'Ordonnateur
\livretRef #'EBCordonnateur
%# Fille de la Valeur, immortelle Victoire,
%# Vole devant nos pas, reconnoi nos Drapeaux.
%# D'un Roi fameux nous chantons les travaux.
%# Par son auguste Nom fai briller nôtre gloire;
%# Etend nos loix & sa memoire:
%# Nos succès sont pour lui des tri=omphes nouveaux.
%# Fille de la Valeur, immortelle Victoire,
%# Vole devant nos pas, reconnoi nos Drapeaux.
\livretRef #'EBDair
\livretDidasPPage On danse.

\livretScene SCENE TROISIÉME
\livretDescAtt\wordwrap-center {
  \smallCaps { Semiramis ; }
  Et les Acteurs de la Scene précédente.
}
\livretPers Semiramis
\livretRef #'ECArecit
%# Cessez. Ninus reçoit vos vœux & vôtre zele.
%# Les celestes décrets seront bien-tôt remplis.
%# La Victime paroît. D'où vient que je fremis!
%# Pour la premiere fois, je m'attendris pour elle...

\livretScene SCENE QUATRIÉME
\livretDescAtt\wordwrap-center {
  \smallCaps { Amestris, Semiramis, }
  \line { Et les Acteurs de la Scene précédente. }
}
\livretPers Amestris
\livretRef #'EDAamestris
%# Peuples, qui de Ninus honorez la memoire,
%# Je viens consommer ses bienfaits.
%# Tous ses jours ont coulé pour vous combler de gloire,
%# Le dernier de mes jours va vous donner la paix.
\livretDidasPPage\line { Elle prend le Fer des Sacrifices. }
%# O Ciel, défend mon cœur d'un souvenir trop tendre!
%# Mânes de mes a=yeux, faites place à ma cendre.

\livretScene SCENE CINQUIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Arsane, Amestris, Semiramis, Chœurs.
}
\livretPersDidas Arsane lui arrachant le Fer.
\livretRef #'EEArecitArsane
%#- Ah! Princesse, arrêtez...
\livretPers Amestris
%#= Seigneur, que faites-vous?
%#- Vous irritez les Dieux.
\livretPers Arsane
%#= Je brave leur couroux.
%# Quoi! la vertu perit! Quelle aveugle vangeance!
%# Est-ce donc à ces traits qu'ils marquent leur puissance?
%# Perfides, n'osez-vous défendre l'innocence?
\livretPers Semiramis
%# Ministres des Autels vangez les droits des Dieux.
\livretPers Arsane
%# Fu=yez, tremblez, Esclaves odi=eux
%# D'une Reine cru=elle.
%# Où suis-je? quels transports? C'est l'Enfer qui m'appelle,
%# Je vous répons... quelle épaisse vapeur...
%# Je vois devant mes pas le flambeau des Furies:
%# Je vous suis... Epuisez toutes vos barbaries.
%# Versons des flots de sang... Répandons la terreur:
%# Je sens tout l'Enfer dans mon cœur.
\livretRef #'EEBchoeur
\livretDidasPPage\wordwrap {
  Arsane renverse l’Autel, & se jette sur le Peuple qui fuit devant luy.
}
\livretPers Chœur
%# Secourez-nous, ô Dieux! frappez qui vous offense.
\livretDidasPPage\wordwrap {
  Semiramis sort à la tête des Prêtres & des Soldats ;
  Arsane les pousuit.
}

\livretScene SCENE SIXIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Amestris, & le Chœur.
}
\livretPers Amestris
\livretRef #'EFAchoeur
%# Dieux, prenez sa deffense.
\livretPers Chœur
%# Secourez-nous, ô Dieux! frappez qui vous offense.
\livretPers Amestris
%# Ciel implacable, Ciel jaloux,
%# Dois-je vous implorer? il combat contre vous.

\livretScene SCENE SEPTIÉME
\livretDescAtt\wordwrap-center\smallCaps {
  Arsane, Amestris.
}
\livretPers Arsane
\livretRef #'EGArecit
%# Vous vivrez, ma Princesse, & le Ciel par mes coups
%# A voulu sauver tant de charmes:
%# Un Dieu me conduisoit, un Dieu guidoit mes armes.
%# Je ne vois point la Reine, allons à ses genoux
%# Expi=er mon audace, & calmer ses allarmes.

\livretScene SCENE DERNIERE
\livretDescAtt\wordwrap-center\smallCaps {
  Arsane, Semiramis, Zoroastre, Amestris, Chœur.
}
\livretPers Arsane
\livretRef #'EHArecit
%# Grands Dieux! elle paroît, que mon cœur est glacé!
%#- Je vois couler son sang.
\livretPers Zoroastre
%#= C'est toi qui l'a versé.
\livretPers Arsane
%# La Reine par mon bras, a perdu la lumiere!
\livretPers Zoroastre
%# Ton sort est plus affreux, Arsane, c'est ta Mere.
\livretPers Semiramis
%# Vous, mon Fils! quoi je meurs par la main de mon Fils!
%# Dieux inhumains, vous me l'aviez promis.
%# Ce jour termine enfin mes malheurs & mes crimes.
\livretPers Arsane
%# Terre, pour m'engloutir, ouvre-moi tes abîmes.
\livretPers Semiramis
%# Amestris, calmez ses fureurs.
%# Je vous laisse en mourant la suprême puissance,
%# Le Soleil désormais luira sur l'innocence.
%# De l'éternelle nuit j'entrevois les horreurs.
%# Ninus approchez-vous… je m'affoiblis… je meurs.
\sep
