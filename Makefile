OUTPUT_DIR=out
#DELIVERY_DIR=delivery
DELIVERY_DIR=/Users/nicolas/Dropbox/LesOmbres/Semiramis/complet/

NENUVAR_LIB_PATH=$$(pwd)/../nenuvar-lib
LILYPOND_CMD=lilypond -I$$(pwd) -I$(NENUVAR_LIB_PATH) \
  --loglevel=WARN -ddelete-intermediate-files \
  -dno-protected-scheme-parsing
PROJECT=Destouches_Semiramis

# Conducteur
conducteur:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT) main.ly
.PHONY: conducteur
# Dessus
dessus:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-dessus -dpart=dessus part.ly
.PHONY: dessus
# Dessus & Hautes-contre
dessus-hc:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-dessus-hc -dpart=dessus-hc part.ly
.PHONY: dessus-hc
# Hautes-contre
haute-contre:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-haute-contre -dpart=haute-contre part.ly
.PHONY: haute-contre
# Tailles
taille:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-taille -dpart=taille part.ly
.PHONY: taille
# Basses
basse:
	$(LILYPOND_CMD) -o $(OUTPUT_DIR)/$(PROJECT)-basse -dpart=basse part.ly
.PHONY: basse
# Basse continue
basse-continue:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-basse-continue -dpart=basse-continue part.ly
.PHONY: basse-continue
# Trompettes
trompette:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-trompette -dpart=trompette part-tt.ly
.PHONY: trompette
# Timbales
timbales:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-timbales -dpart=timbales part-tt.ly
.PHONY: timbales
# Chant
bc-chant:
	$(LILYPOND_CMD) \
	-o $(OUTPUT_DIR)/$(PROJECT)-bc-chant -dpart=chant part.ly
.PHONY: bc-chant


delivery:
	@mkdir -p $(DELIVERY_DIR)/
	@if [ -e $(OUTPUT_DIR)/$(PROJECT).pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT).pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-dessus.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-dessus.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-dessus-hc.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-dessus-hc.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-haute-contre.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-haute-contre.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-taille.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-taille.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-basse.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-basse.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-basse-continue.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-basse-continue.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-trompette.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-trompette.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-timbales.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-timbales.pdf $(DELIVERY_DIR)/; fi
	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-bc-chant.pdf ]; then mv -fv $(OUTPUT_DIR)/$(PROJECT)-bc-chant.pdf $(DELIVERY_DIR)/; fi
#	@if [ -e $(OUTPUT_DIR)/$(PROJECT)-1.midi ]; then tar zcf $(DELIVERY_DIR)/$(PROJECT)-midi.tar.gz $(OUTPUT_DIR)/$(PROJECT).midi $(OUTPUT_DIR)/$(PROJECT)-[0-9]*.midi; fi
#	git archive --prefix=$(PROJECT)/ HEAD . | gzip > $(DELIVERY_DIR)/$(PROJECT).tar.gz
.PHONY: delivery

clean:
	@rm -f $(OUTPUT_DIR)/$(PROJECT)-* $(OUTPUT_DIR)/$(PROJECT).*
.PHONY: clean

parts: dessus dessus-hc haute-contre taille basse basse-continue trompette timbales bc-chant
.PHONY: parts

all: check parts conducteur delivery clean

.PHONY: all

check:
	@if [ ! -d $(NENUVAR_LIB_PATH) ]; then \
	  echo "Please install nenuvar-lib in parent directory:"; \
	  echo " cd .. && git clone https://github.com/nsceaux/nenuvar-lib.git"; \
	  false; \
	fi
.PHONY: check
