\header {
  copyrightYear = "2015"
  composer = "André Cardinal Destouches"
  poet = "Pierre-Charles Roy"
  date = "1718"
}

#(ly:set-option 'original-layout (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'use-ancient-clef (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'show-ancient-clef (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'use-rehearsal-numbers #t)

%% Staff size
#(set-global-staff-size
  (cond ((not (symbol? (ly:get-option 'part))) 16)
        ((memq (ly:get-option 'part) '(basse-continue chant)) 16)
        (else 18)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking
     (if (or (eqv? (ly:get-option 'part) 'chant)
             (not (symbol? (ly:get-option 'part))))
         ly:optimal-breaking
         ly:page-turn-breaking))
}

\language "italiano"
\include "nenuvar-lib.ily"
\setPath "ly"
\opusTitle "Semiramis"

\layout {
  indent = \smallindent
  short-indent = #0
  ragged-last = ##f
}

\header {
  maintainer = \markup {
    Nicolas Sceaux,
    \with-url #"http://www.les-ombres.fr" Les Ombres
  }
  license = "Creative Commons Attribution-ShareAlike 4.0 License"
}

\opusPartSpecs
#`((dessus
    "Violons, Flûtes, Hautbois" ()
    (#:notes "dessus"
             #:tag-notes dessus
             #:clef "treble"))
   (dessus-hc
    "Dessus & haute-contre" ((dessus #f))
    (#:score-template "score-dessus-hc"))
   (dessus2-hc
    "Haute-contre & dessus II" ((haute-contre #f))
    (#:notes "dessus2"
             #:tag-notes dessus2
             #:clef "treble"))
   (trompette
    "Trompettes" ()
    (#:notes "dessus" #:tag-notes trompette #:clef "treble"))
   (timbales
    "Timbales" ()
    (#:notes "timbales" #:tag-notes timbales #:clef "basse"))
   (haute-contre
    "Hautes-contre" ()
    (#:notes "haute-contre"
             #:clef ,(if (eqv? (ly:get-option 'part) 'dessus2-hc) "treble" "alto")))
   (taille
    "Tailles" ()
    (#:notes "taille"
             #:clef "alto"))
   (basse
    "Basses" ()
    (#:notes "basse" #:clef "basse" #:tag-notes basse))
   (basse-continue
    "Basse continue" ()
    (;
     #:notes "basse"
     #:clef "basse"
     #:tag-notes basse-continue
     #:score-template "score-basse-continue"))
   (chant "Chant" ((basse-continue #f)) (#:score "score-chant")))

%%%

ouverture =
#(define-music-function (parser location title) (string?)
   (let ((rehearsal (rehearsal-number)))
    (add-toc-item parser 'tocActMarkup #{ \markup\sep #})
    (add-toc-item parser 'tocPieceMarkup title rehearsal)
    (add-even-page-header-text parser (string-upper-case (*opus-title*)) #f)
    (add-odd-page-header-text parser (string-upper-case title) #f)
    (add-toplevel-markup parser (markup #:act (string-upper-case title)))
    (add-no-page-break parser)
    (if (eqv? #t (ly:get-option 'use-rehearsal-numbers))
        (begin
         (add-toplevel-markup parser (markup #:rehearsal-number rehearsal))
         (add-no-page-break parser))))
  (make-music 'Music 'void #t))

semiramisMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Semiramis"))

semiramisMarkText =
#(define-music-function (parser location text) (markup?)
  (make-character-mark-text "vbas-dessus" "Semiramis" text))

amestrisMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbas-dessus" "Amestris"))

amestrisMarkText =
#(define-music-function (parser location text) (markup?)
  (make-character-mark-text "vbas-dessus" "Amestris" text))

arsaneMark =
#(define-music-function (parser location) ()
  (make-character-mark "vhaute-contre" "Arsane"))

arsaneMarkText =
#(define-music-function (parser location text) (markup?)
  (make-character-mark-text "vhaute-contre" "Arsane" text))

zoroastreMark =
#(define-music-function (parser location) ()
  (make-character-mark "vbasse" "Zoroastre"))

zoroastreMarkText =
#(define-music-function (parser location text) (markup?)
  (make-character-mark-text "vbasse" "Zoroastre" text))

#(define-markup-command (triangle-up layout props a b c) (markup? markup? markup?)
   (let ((base (interpret-markup
                layout props
                #{ \markup\fontsize #-2 { \number $b \number $c } #}))
         (top (interpret-markup
               layout props #{ \markup\fontsize #-2 \number $a #})))
     (let* ((base-width (interval-length (ly:stencil-extent base X)))
            (top-width (interval-length (ly:stencil-extent top X)))
            (top-left-padding (/ (- base-width top-width) 2.0)))
       (stack-lines
        DOWN 0.0 2
        (list (stack-stencil-line
               0
               (list (ly:make-stencil "" `(0 . ,top-left-padding) '(0 . 0))
                     top))
              base)))))

#(define-markup-command (triangle-down layout props a b c) (markup? markup? markup?)
   (let ((base (interpret-markup
                layout props #{ \markup\fontsize #-2 { \number $a \number $b } #}))
         (bottom (interpret-markup
                  layout props #{ \markup\fontsize #-2 \number $c #})))
     (let* ((base-width (interval-length (ly:stencil-extent base X)))
            (bottom-width (interval-length (ly:stencil-extent bottom X)))
            (bottom-left-padding (/ (- base-width bottom-width) 2.0)))
       (stack-lines DOWN 0.0 2
                    (list
                     base
                     (stack-stencil-line
                      0 (list (ly:make-stencil ""
                                               `(0 . ,bottom-left-padding)
                                               '(0 . 0))
                              bottom)))))))
