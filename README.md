# Destouches : Semiramis
Matériel complet de Semiramis de Destouches, créé pour les Ombres (2016).

Pour construire le matériel :
```
$ make all
```
