\notesSection "Notes"
\markuplist\with-line-width-ratio #0.7 \column-lines {
  \livretAct NOTES
  \livretParagraph {
    Cette édition de \italic Semiramis de André Cardinal Destouches,
    réalisée pour l’ensemble \italic { Les Ombres, } est basée sur les
    sources suivantes :
  }
  \null
  %% édition imprimée
  \indented-lines#5 {
    \line\bold { [édition imprimée] }
    \line {
      \italic { Sémiramis, tragédie en musique par Mr Destouches. }
      Destouches, André Cardinal (1672-1749).
    }
    \line { Paris : J.B.C. Ballard, 1718 }
    \wordwrap-lines {
      Bibliothèque nationale de France, VM2-270.
      \with-url #"http://gallica.bnf.fr/ark:/12148/btv1b9058697j"
      \smaller\typewriter "http://gallica.bnf.fr/ark:/12148/btv1b9058697j"
    }
    \line { Édition réduite imprimée. }
  }
  \fill-line {
    \epsfile #X #70
    #"images/Semiramis-edition-imprimee.eps"
  }
  \null
  %% parition manuscrite
  \indented-lines#5 {
    \line\bold { [partition manuscrite] }
    \line {
      \italic { Sémiramis, tragédie. }
      Destouches, André Cardinal (1672-1749).
    }
    \line { Partition manuscrite complète, côte A.100.b. }
  }
  \fill-line {
    \epsfile #X #60
    #"images/Semiramis-manuscrit.eps"
  }
  \null
  %% livret
  \indented-lines#5 {
    \fill-line {
      \general-align #Y #DOWN \column {
        \line\bold { [livret imprimé] }
        \italic {
          \line { Sémiramis, Tragédie, }
          \line { Réprésentée Par l’Academie Royale de Musique, }
          \line { l’An 1718. }
        }
        \line { Roy, Pierre-Charles (1683-1764). }
        \line { Texte imprimé. }
      }
      \general-align #Y #CENTER \epsfile #X #35
      #"images/Semiramis-livret.eps"
    }
  }
  \livretParagraph {
    L’édition imprimée a été considérée comme canonique : elle a été
    copiée, puis complétée grâce à la partition manuscrite (parties
    manquantes, signes d’interprétation, chiffrages). Les clés sont
    modernisées, les clés d’origines étant rappelées en début de
    pièce. Les altérations sont également modernisées (usage de
    bécarres), hormis dans la basse chiffrée ; les altérations
    accidentelles qui ne sont pas explicites dans les sources sont
    suggérées entre parenthèses. L’orthographe utilisée dans la
    partition est celle du livret imprimé, lequel est restitué en
    préambule. 
    Pour l’heure, le prologue n’est pas retranscrit dans cette édition.
  }
  \livretParagraph {
    Autant que possible, les signes musicaux (agréments, rythmes, etc.)
    utilisés sur les sources sont retranscrits de même
    dans cette édition, à deux exceptions près.
    Les coulés, exprimés au moyen des lignes incurvées, comme dans
    cet exemple du premier air des Prétresses (page \page-refIII #'DEHrondeau ),
    ont été changés en appoggiatures, considérant que le rendu avec
    des signes de liaisons prêtait à confusion :
  }
  \fill-line {
    \null
    \vcenter\epsfile #X #30 #"images/coules.eps"
    \null
    \vcenter\score {
      { \clef "french" \time 3/4
        sol''4 \appoggiatura fa''8 mi''4 \appoggiatura re''8 dod''4 |
        re''8 mi'' mi''2\trill |
        re''4 }
      \layout { \quoteLayout }
    }
    \null
  }
  \livretParagraph {
    Les notes pointées au delà d’une barre de mesure, rencontrées notamment
    dans les hémioles, sont changées en notes liées, en raison d’une difficulté
    technique à les rendre à l’identiques. Un exemple dans la chaconne
    (page \page-refII #'AFEchaconne ) :
  }
  \fill-line {
    \null
    \vcenter\epsfile #X #30 #"images/pointe.eps"
    \null
    \vcenter\score {
      { \clef "french" \time 3/4
        fa''4 mi'' re''\trill~ |
        re''8 do'' do''2\trill | }
      \layout { \quoteLayout }
    }
    \null
  }
  \null
  \paragraph {
    Le matériel d'orchestre est constitué des parties
    séparées suivantes : dessus (violons, flûtes, hautbois),
    hautes-contre, tailles, basses, basse continue, trompettes,
    timbales.
  }
  \null
  \paragraph {
    Cette édition est distribuée selon les termes de la licence
    Creative Commons Attribution-ShareAlike 4.0.  Il est donc permis,
    et encouragé, de jouer cette partition, la distribuer,
    l’imprimer.
  }
}
