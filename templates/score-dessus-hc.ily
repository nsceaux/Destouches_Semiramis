\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "d." } << 
      \keepWithTag #(*tag-global*) \global
      \keepWithTag #(*tag-notes*) \includeNotes "dessus"
      $(or (*score-extra-music*) (make-music 'Music))
    >>
    \new Staff \with { instrumentName = "h-c." } <<
      \keepWithTag #(*tag-global*) \global
      \keepWithTag #(*tag-notes*) \includeNotes "haute-contre"
      \clef "treble"
    >>
  >>
  \layout {
    system-count = #(*system-count*)
    indent = #(if (*instrument-name*)
                  largeindent
                  (or (*score-indent*) smallindent))
    ragged-last = #(*score-ragged*)
  }
}
