\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \includeNotes "vdessus"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "vhaute-contre"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "vtaille"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "vbasse"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    \new Staff \with { \haraKiri } \withLyrics <<
      \global \includeNotes "voix"
    >> \keepWithTag #'amestris \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*4\break
        s4.*6\break s4.*2 s2. s4.\pageBreak
        s2.*5\break s2.*3\break s4.*6\pageBreak
        s4.*8\break s4.*7\pageBreak
        s4.*6\pageBreak
        s4.*7\break
      }
      \modVersion { s2.*4 s4.*8 s2. s4.\break s2.*8\break }
    >>
  >>
  \layout { }
  \midi { }
}
