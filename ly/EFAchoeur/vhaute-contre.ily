\clef "vhaute-contre" R2.*4 |
la'8 la' la' |
la'4 la'8 |
la'4 mi'8 |
fa'4. |
mi'8 mi' mi' |
fad'4 fad'8 |
R4.*2 R2. R4. R2.*8 |
sol'8 sol' sol' |
sol'4 sol'8 |
sol'4 sol'8 |
mi'4. |
sol'8 sol' sol' |
sol'4 sol'8 |
R4.*9 |
sol'8 sol' sol' |
la'4 la'8 |
sol'4. |
mi'8 mi' mi' |
fa'4 fa'8 |
sol'4. |
fa'8 fa' fa' |
fa'4 fa'8 |
mi'4 la'8 |
la'4. |
sol'8 sol' sol' |
sol' sol' sol' |
mi'4. |
la'8 la' sol' |
sol'4 sol'8 |
R4.*9 |
R1 |
