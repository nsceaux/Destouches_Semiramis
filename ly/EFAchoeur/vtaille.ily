\clef "vtaille" R2.*4 |
mi'8 mi' mi' |
fa'4 fa'8 |
mi'4 mi'8 |
la4. |
la8 la la |
la4 la8 |
R4.*2 R2. R4. R2.*8 |
si8 si si |
do'4 do'8 |
si4 si8 |
do'4. |
re'8 re' re' |
do'4 do'8 |
R4.*9 |
do'8 do' do' |
fa'4 fa'8 |
do'4. |
sol8 sol do' |
la4 re'8 |
re'4. |
do'8 do' re' |
re'4 re'8 |
do'4 fa'8 |
fa'4. |
mi'8 do' mi' |
re' re' re' |
do'4. |
do'8 do' si |
do'4 do'8 |
R4.*9 |
R1 |
