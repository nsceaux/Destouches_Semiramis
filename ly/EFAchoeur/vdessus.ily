\clef "vdessus" R2.*4 |
dod''8 dod'' dod'' |
re''4 re''8 |
dod''4 dod''8 |
re''4. |
re''8 re'' dod'' |
re''4 re''8 |
R4.*2 R2. R4. R2.*8 |
re''8 re'' re'' |
mi''4 mi''8 |
re''4 sol''8 |
sol''4. |
fa''8 fa'' fa'' |
mi''4 mi''8 |
R4.*9 |
mi''8 mi'' mi'' |
fa''4 fa''8 |
mi''4. |
do''8 do'' do'' |
do''4 sib'8 |
sib'4. |
la'8 la' la' |
si'4 si'8 |
do''4 do''8 |
re''4. |
mi''8 fa''16[ mi''] re''[ do''] |
si'8 si' sol'' |
do''4. |
fa''8 fa'' re'' |
mi''4 mi''8 |
R4.*9 |
R1 |
