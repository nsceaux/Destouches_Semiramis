\tag #'(amestris basse) {
  Dieux, Dieux, pre -- nez sa def -- fen -- se.
}
\tag #'choeur {
  Se -- cou -- rez- nous, ô Dieux ! frap -- pez qui vous of -- fen -- se.
}
\tag #'(amestris basse) {
  Ciel im -- pla -- ca -- ble, Ciel ja -- loux,
  Dois- je vous im -- plo -- rer ? il com -- bat con -- tre vous.
}
\tag #'choeur {
  Se -- cou -- rez- nous, ô Dieux ! frap -- pez qui vous of -- fen -- se.
  Se -- cou -- rez- nous, ô Dieux !
  Se -- cou -- rez- nous, ô Dieux !
  Se -- cou -- rez- nous, ô Dieux ! frap -- pez qui vous of -- fen -- se,
  frap -- pez qui vous of -- fen -- se.
}
