\clef "vbasse" R2.*4 |
la8 la la |
la4 la8 |
la4 la8 |
fa4. |
la8 la la |
re4 re8 |
R4.*2 R2. R4. R2.*8 |
sol8 sol sol |
sol4 sol8 |
sol4 sol8 |
la4. |
si8 si si |
do'4 do'8 |
R4.*9 |
do'8 do' do' |
do'4 do'8 |
do'4. |
do8 do do |
re4 re8 |
mi4. |
fa8 fa fa |
sol4 sol8 |
la4 la8 |
si4. |
do'8 do' do |
sol8 sol mi |
la4. |
fa8 fa sol |
do4 do8 |
R4.*9 |
R1 |
