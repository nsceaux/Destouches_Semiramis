\clef "haute-contre" R2.*4 |
la'8 la' la' |
la'4 la'8 |
la'4 mi'8 |
fa'4. |
mi'8 mi' mi' |
fad'4 fad'8 |
R4. |
mi''8 mi'' mi'' |
re''4 mi'' re''8. do''16 |
si'4.\trill |
R2.*8 |
sol'8 sol' sol' |
sol'4 sol'8 |
sol'4 sol'8 |
mi'4. |
sol'8 sol' sol' |
sol'4 sol'8 |
R4. |
do''8 do'' re'' |
si' mi' sold' |
la'16 si' sold'8.\trill la'16 |
la'4. |
mi''8 re'' re'' |
re''4 sold'8 |
la' do'' do'' |
re''16 do'' si'8.\trill do''16 |
do''8 sol' sol' |
la'4 la'8 |
sol'4. |
mi'8 mi' mi' |
fa'4 fa'8 |
sol'4. |
fa'8 fa' fa' |
fa'4 fa'8 |
mi'4 la'8 |
la'4. |
sol'8 sol' sol' |
sol' sol' sol' |
mi'4. |
la'8 la' sol' |
sol'4 sol'8 |
do'' sol' do'' |
re''4 re''8 |
do''4 do''8 |
do''4 la'8 |
la'4 sol'8 |
sol'4 si'8 |
do''4 do''8 |
do''4 do''8 |
do''8 si'8.\trill do''16 |
do''2.
