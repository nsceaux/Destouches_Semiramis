\amestrisMark re''2 r4 |
mi''2 r8 mi'' |
la'2 la'8 si' |
sold'2 sold'4 |
R4.*8 |
R2. |
R4. |
<>^\markup\character Amestris mi''2 mi''8 do'' |
la'2\trill la'4 |
fa''2 re''4 |
si'2\trill sol''4~ |
sol''4 sol''8 fa'' fa'' mi'' |
mi''2\trill re''8 mi'' |
do''2\prall si'8 do'' |
\appoggiatura do''8 re''2. |
R4.*39 |
R1 |
