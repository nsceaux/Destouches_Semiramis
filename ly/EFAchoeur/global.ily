\key la \minor \midiTempo#120
\digitTime\time 3/4 s2.*4
\time 3/8 s4.*8
\once\override Staff.TimeSignature.stencil = ##f \digitTime\time 3/4 s2.
\once\override Staff.TimeSignature.stencil = ##f \time 3/8 s4.
\digitTime\time 3/4 s2.*8
\time 3/8 s4.*39
\time 2/2 s1 \bar "|."
