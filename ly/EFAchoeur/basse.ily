\clef "basse"
<<
  \tag #'basse { R2.*4 }
  \tag #'basse-continue {
    si,2. |
    do |
    fa2 re4 |
    mi2. |
  }
>>
la,8 la, la, |
la,4 la,8 |
la,4 la8 |
fa4. |
la8 la la |
re4 re8 |
R4. |
la,16 si, do re mi fad |
sol4 do re8 re, |
<< { sol,16 fad, sol, la, si, sol, | }
  \\ \new CueVoice { \voiceTwo sol,4._"[manuscrit]" } >>
<<
  \tag #'basse { R2.*8 }
  \tag #'basse-continue {
    do2. |
    fa8 sol la sol fa mi |
    re2. |
    sol8 fa sol la si sol |
    la2 si4 |
    do'2 si4 |
    la2. |
    sol4. fa16[ mi re do si, la,] |
  }
>>
sol,8 sol, sol, |
sol,4 sol,8 |
sol,4 sol8 |
la16 sol la si do' la |
si8 si si |
do'4 do'8 |
R4. |
la8 fa re |
mi sold mi |
la16 re mi8 mi, |
la,4. |
mi16 fa sol la si do' |
re'4 re'8 |
la mi fa |
re sol sol, |
do do' do' |
do'4 do'8 |
do' do do |
do do do |
re4 re8 |
mi16 re mi fa sol mi |
fa8 fa fa |
sol16 fa sol la si sol |
la sol la si do' la |
si la si do' re' si |
do'8 do' do |
sol8 sol mi |
la4. |
fa8 fa sol |
do4 do8 |
do'4 do'8 |
si4 si8 |
la4 la8 |
fa4 fa8 |
re4 sol8 |
si4 sol8 |
do'4 do8 |
fa4 fa8 |
re sol sol, |
<<
  \tag #'basse { do2. }
  \tag #'basse-continue { \custosNote do4 \stopStaff }
>>
