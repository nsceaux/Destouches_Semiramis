\clef "dessus" R2.*4 |
dod''8 dod'' dod'' |
re''4 re''8 |
dod''16 re'' mi'' fa'' sol'' mi'' |
la''8 fa'' re'' |
re'' re'' dod'' |
re''4 re''8 |
re''16 mi'' fad'' sol'' la'' si'' |
do'''4 si''16 la'' |
si''8. si''16 si'' la'' sol'' la'' la''8.\trill sol''16 |
sol''4. |
R2.*8 |
re''8 re'' re'' |
mi''4 mi''8 |
re''4 sol''8 |
sol''4. |
fa''8 fa'' fa'' |
mi''4 mi''8 |
sol'' mi'' do'' |
do'''16 si'' la'' sold'' la'' si'' |
sold'' fad'' mi'' re'' do'' si' |
do'' re'' si'8.\trill la'16 |
la' si' do'' re'' mi'' fa'' |
sol''4. |
fa''16 mi'' fa'' sol'' la'' si'' |
do''' si'' la'' sol'' la'' sol'' |
fa'' mi'' re''8.\trill do''16 |
do''8 mi'' mi'' |
fa''4 fa''8 |
mi''16 fa'' sol'' fa'' mi'' re'' |
do''8 do'' do'' |
do''4 sib'8 |
sib'4. |
la'8 la' la' |
si'4 si'8 |
do''4 do''8 |
re''16 do'' re'' mi'' fa'' re'' |
mi''8 fa''16 mi'' re'' do'' |
si'8 si' sol'' |
do''16 si' do'' re'' mi'' do'' |
fa''8 fa'' re'' |
mi''4 mi''8 |
mi''16 re'' do'' re'' mi'' fa'' |
sol'' fa'' sol'' la'' si'' sol'' |
do'''16 si'' la'' sol'' fa'' mi'' |
la'' sol'' fa'' mi'' re'' do'' |
fa'' mi'' re'' do'' si' la' |
sol'8 sol'' sol'' |
mi'' do''' do''' |
la''16 si'' do''' si'' la'' sol'' |
fa'' mi'' re''8.\trill do''16 |
do''2.
