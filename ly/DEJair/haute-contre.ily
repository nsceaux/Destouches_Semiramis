\clef "haute-contre" la'2 |
la' la'4 sol' |
fa' mi' re' dod' |
re'2 la'4 la'8 sol' |
la'4 la' la' dod'' |
re''2 do''4 sol' |
fa' sol' sol' mi' |
fa'2 re'' |
re'' re''4 do''8 re'' |
si'4 do'' si'4.\trill do''8 |
do''2 sold' |
sold'? la'4 mi' |
fa' do' re' mi' |
mi'2 r |
mi'4 re' mi' la' |
sold' mi' la' la' |
la' la' sold'4. la'8 |
la'2 la' |
la' la'4 dod'' |
re'' la' la' mi' |
fa'2 r |
la'4 sol' la' sol'8 fa' |
mi'2 la'4 la' |
la' mi' fa' la' |
la' mi' fa' fa' |
mi'4. re'8 dod'4. re'8 |
re'2
