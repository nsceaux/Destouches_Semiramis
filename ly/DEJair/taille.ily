\clef "taille" fa'2 |
fa' fa'4 do' |
do' sib la la |
la2 fa'4 mi'8 re' |
mi'4 mi' fa' sol' |
la'2 sol'4 sol' |
do' re' re' do' |
do'2 la' |
la' la'4 la' |
sol' sol' sol'4. sol'8 |
mi'2 sold' |
sold'?4 mi' la do' |
re' mi' si do' |
si2 r |
do'4 re' do' mi' |
mi' si do' mi' |
fa' fa' mi'4. re'8 |
dod'2 mi' |
mi' fa'4 sol' |
la'4. sib'8 dod'4 dod' |
re'2 r |
fa'4 sol' fa' mi' |
la2 la4 re' |
dod' dod' re' re' |
dod' dod' re'2 |
re'4 re' la4. mi8 |
fa2
