\clef "dessus" <>^"Violons" re''2 |
re'' re''4 do''8\trill sib' |
la'4 sol' fa'\trill mi' |
re'4. la'8 re''4 mi'' |
dod''4\trill si'8 la' re''4 mi''\trill |
fa''2 mi''8\trill re'' do'' sib' |
la'4\trill sol' sib' sol'\trill |
fa'2 fa'' |
fa'' fa''4 mi''8 fa'' |
re''4 mi''8 fa'' re''4.\trill do''8 |
do''2 si' |
si' re''4 do'' |
si' la' sold' la' |
mi'2 do''4 si' |
la' sold' la'8 si' do'' la' |
si'4 mi' mi''8 re'' do''\trill si' |
la'4 si'8 do'' si'4.\trill la'8 |
la'2 dod'' |
dod'' re''4 mi'' |
fa'' mi''8 re'' mi''4 mi'' |
la'2 fa''4 mi'' |
re'' dod'' re'' dod''16 si'8 dod''16 |
dod''2 re''4 fa'' |
mi'' la' re''8 mi'' fa'' re'' |
mi''4 la' re''8 do'' sib' la' |
sol'4 la'8 sib' mi'4.\trill re'8 |
re'2
