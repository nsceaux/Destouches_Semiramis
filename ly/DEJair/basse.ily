\clef "basse" re2 |
re re4 mi |
fa sol la la, |
re2 re4 do8 sib, |
la,4 sol, fa, mi, |
re,4. re8 mi4 mi |
fa sib, sol, do |
fa,2 re |
re re4 re |
sol do sol,2 |
do mi |
mi fa4 mi |
re do si, la, |
sold,2 r |
do4 si, do la, |
mi re do la, |
fa re mi mi, |
la,2 la |
sol fa4 mi |
re do8 sib, la,4 sol, |
fa,2 r |
fa4 mi fa sol |
la sol fa re |
la sol fa re |
la la,8 sol, fa,2 |
sol, la, |
re,
