\clef "vdessus" <>^\markup\character Chœur fa''4 fa'' |
mi''8[\melisma fa'' mi'' re''] dod''[ si' la' sol']( |
fa'2)\melismaEnd re''4 re'' |
\setMusic #'reprise {
  mi''2 mi''4.\trill mi''8 |
  fa''2 fa''4. fa''8 |
  mi''8\trill[\melisma re'' mi'' fa''] mi''[ sol'' fa'' mi''] |
  re''[\trill dod'' re'' mi''] re''[ fa'' mi'' re'']( |
  dod''2\trill)\melismaEnd dod''4. dod''8 |
  re''8[\melisma dod'' re'' mi''] re''[ fa'' mi'' re'']( |
  sol''2)\melismaEnd sol''4 fa'' |
  mi''2 mi''4 mi'' |
  mi''2 mi''4 fa'' |
  mi''2.\trill fa''4 |
  re'' re'' mi'' fa'' |
  mi''\trill mi'' fa'' fa'' |
  mi''8[\melisma fa'' mi'' re''] dod''[ si' la' sol']( |
  fa'2)\melismaEnd re''4 re'' |
  mi''2 mi''4.\trill mi''8 |
  fa''2 fa''4. fa''8 |
  mi''8[\trill\melisma re'' mi'' fa''] mi''[ sol'' fa'' mi'']( |
  re''8)\trill[ dod'' re'' mi''] re''[ fa'' mi'' re'']( |
  dod''2)\melismaEnd dod''4. dod''8 |
  re''8[\melisma dod'' re'' mi''] re''[ fa'' mi'' re''] |
  sol''2\melismaEnd sol''4 fa'' |
  mi''2 mi''4 mi'' |
  fa''2 mi''4 re'' |
  mi''2.\trill re''4 |
  dod'' re'' mi'' dod'' |
  re'' re'' mi'' mi'' |
  fa''2 mi''4 re'' |
  mi''2.\trill fa''4 |
  mi'' re'' mi''4. fa''8 |
  fa''2( mi'')\trill |
  re''2
}
\keepWithTag #'() \reprise
r2 |
R1*11 |
r2 r4 fa'' |
fa'' mi'' re'' do'' |
si'2\trill re''4. mi''8 |
\appoggiatura re''16 do''4.( re''8) re''4.\trill do''8 |
do''2 mi''4 mi'' |
re''8[\melisma mi'' re'' do''] si'[ la' sold' fad']( |
mi'2)\melismaEnd mi''4 mi'' |
mi''2 re''4.\trill mi''8 |
mi''2 mi''4. mi''8 |
re''8[\trill\melisma do'' re'' mi''] re''[ fa'' mi'' re''] |
do''[\trill si' do'' re''] do''[ mi'' re'' do'']( |
si'2)\trill\melismaEnd si'4 si' |
mi''2 mi''4. mi''8 |
re''8[\trill\melisma do'' re'' mi''] re''[ mi'' do'' re'']( |
si'2)\trill\melismaEnd si'4 mi'' |
dod''2 mi''4 mi'' |
fad''2 fad''4 sol'' |
fad''2. re''4 |
re'' mi'' fad'' sol'' |
fad'' fad'' sol'' sol'' |
mi''8[\melisma fa'' mi'' re''] do''[ sib' la' sib']( |
sol'2\trill)\melismaEnd fa''4 fa'' |
sol''2 sol''4.\trill sol''8 |
la''2 do''4. do''8 |
sib'8[\trill\melisma la' sib' do''] sib'[ re'' do'' sib'] |
la'\trill[ sol' la' sib'] la'[ do'' sib' la']( |
re''2)\melismaEnd re''4 re'' |
do''2 do''4. do''8 |
sib'8[\melisma la' sib' do''] sib'[ do'' la' sib']( |
sol'2\trill)\melismaEnd sol'4 do'' |
la'2\trill r |
R1*7 |
r2 r4 fa'' |
mi'' re'' dod'' re'' |
la'2 sol'4. la'8 |
\appoggiatura sol'8 fa'4.( sol'8) sol'4.\trill la'8 |
la'4 r r2 |
R1 |
r2 r4 fa'' |
mi'' re'' dod'' re'' |
la'2 sol'4. la'8 |
\appoggiatura sol'16 fa'4.( sol'8) sol'4.\trill la'8 |
la'2. fa''4 |
mi'' re'' dod'' si' |
dod''2.\trill re''4 |
dod'' re'' mi''4. fa''8 |
fa''2\trill mi''4 fa'' |
mi'' re'' dod'' si' |
dod''2.\trill fa''4 |
mi'' dod'' re''4. mi''8 |
fa''2( mi''2)\trill |
re'' la'4 la' |
fa'8[\melisma sol' la' sib' la' sol' fa' mi']( |
fa'2)\melismaEnd r |
R1 |
r2 la'4 la' |
fa'8[\melisma sol' la' sib'] la'[ sol' fa' mi']( |
fa'2)\melismaEnd re''4 re'' |
\modVersion\reprise
