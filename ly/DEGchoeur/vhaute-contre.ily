\clef "vhaute-contre" la'4 la' |
sol'8[\melisma la' sol' fa'] mi'[ re' dod' si]( |
la2)\melismaEnd la'4 la' |
\setMusic #'reprise {
  la'2 sol'4.\trill la'8 |
  la'2 la'4. la'8 |
  sol'8[\melisma fa' sol' la'] sol'[ sib' la' sol'] |
  fa'[ mi' fa' sol'] fa'[ la' sol' fa']( |
  sol'2)\melismaEnd sol'4. sol'8 |
  la'2 la'4 la' |
  la'2 la'4 la' |
  la'2 sol'4 sol' |
  sol'2 sol'4 la' |
  sol'2.\trill la'4 |
  fa' fa' sol' la' |
  sol'\trill sol' la' la' |
  sol'8[\melisma la' sol' fa'] mi'[ re' dod' si]( |
  la2)\melismaEnd la'4 la' |
  la'2 la'4. la'8 |
  la'2 la'4. la'8 |
  sol'8\trill[\melisma fa' sol' la'] sol'[ sib' la' sol']( |
  fa'8)\trill[ mi' fa' sol'] fa'[ la' sol' fa']( |
  sol'2)\melismaEnd sol'4 sol' |
  la'2 la'4 la' |
  la'2 la'4 la' |
  la'2 la'4 la' |
  la'2 sol'4 fa' |
  sol'2. fa'4 |
  sol'4 la' la' la' |
  la' la' la' la' |
  la'2 sol'4 fa' |
  sol'2. fa'4 |
  sol' la' sol' la' |
  la'2.( mi'4) |
  fa'2
}
\keepWithTag #'() \reprise
r2 |
R1*11 |
r2 r4 re' |
re' do' do' fa' |
re'2 re'4. re'8 |
sol'2 fa'4. fa'8 |
mi'2 r |
R1 |
r2 do''4 do'' |
si'8[\melisma do'' si' la'] sold'[ mi' fad' sold']( |
la'2)\melismaEnd mi'4 mi' |
fa'2 fa'4. fa'8 |
mi'2 la'4 la' |
la'2 sold'4. la'8 |
la'2 mi'4. mi'8 |
mi'2 mi'4 mi' |
mi'2 mi'4 mi' |
mi'2 mi'4 la' |
la'2 la'4 sib' |
la'2. la'4 |
la' sib' do'' sib' |
la' la' sib' sib' |
sol'8[\melisma la' sol' fa'] mi'[ sol' fa' sol']( |
mi'2)\melismaEnd la'4 la' |
sol'2 mi'4. mi'8 |
fa'2 la'4. la'8 |
sol'8\trill[\melisma fa' sol' la'] sol'[ sib' la' sol']( |
fa'2)\melismaEnd fa'4 fa' |
sol'2 sol'4.\trill sol'8 |
la'2 fa'4. fa'8 |
fa'2 mi'4 fa' |
fa'2 fa'4 mi' |
fa'2 r |
R1*7 |
r2 r4 re' |
dod' re' mi' fa' |
mi'2 mi'4. mi'8 |
re'2 re'4. mi'8 |
dod'4 r r2 |
R1 |
r2 r4 re' |
dod' re' mi' fa' |
mi'2 mi'4. mi'8 |
re'2 re'4. mi'8 |
dod'2. re'4 |
mi' fa' sol' fa' |
mi'2. re'4 |
mi' fa' mi'4. dod'8 |
re'2 dod'4 re' |
mi' fa' sol' fa' |
mi'2. re'4 |
mi' sol' sol'4. sib'8 |
la'2.( mi'4) |
fa'2 fa'4 fa' |
re'8[ mi' fa' sol' fa' mi' re' dod']( |
re'2) r |
R1 |
r2 fa'4 fa' |
re'8[\melisma mi' fa' sol'] fa'[ mi' re' dod']( |
re'2)\melismaEnd la'4 la' |
\modVersion\reprise
