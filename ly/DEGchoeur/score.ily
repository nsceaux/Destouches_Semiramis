\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \keepWithTag #'dessus \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \includeNotes "vdessus"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "vhaute-contre"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "vtaille"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "vbasse"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s1*5\pageBreak
        s1*6\pageBreak s1*6\pageBreak
        s1*6\pageBreak s1*5\pageBreak
        s1*7\break s1*5\pageBreak s1*4\break s1*5\pageBreak
        s1*6\pageBreak s1*6\pageBreak
        s1*5\pageBreak s1*6\pageBreak
        s1*7\pageBreak s1*7\pageBreak
        s1*6\pageBreak s1*5\pageBreak
        s1*6\pageBreak
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
