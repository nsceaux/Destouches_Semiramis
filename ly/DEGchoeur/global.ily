\set Score.currentBarNumber = 66
\key la \minor \midiTempo#144 \tempo "Legerement"
\digitTime\time 2/2 \partial 2
<<
  \origVersion {
    s2 s1*2 \bar "||" \segnoMark
    s1*30 s2 \bar "|." \fineMark
    s2 s1*75 \bar "|." \endMark "[Dal Segno] jusqu’au mot fin."
  }
  \modVersion {
    s2 s1*138 s2 \bar "|."
  }
>>
