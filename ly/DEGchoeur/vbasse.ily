\clef "vbasse" r2 |
R1 |
r2 re'4 re' |
\setMusic #'reprise {
  dod'8[\melisma re' dod' si] la[ sol fa mi]( |
  re2)\melismaEnd re'4 re' |
  re'2 dod'4. dod'8 |
  re'2 la4. la8 |
  sol8\trill[\melisma fa sol la] sol[ sib la sol] |
  fa[\trill mi fa sol] fa[ la sol fa]( |
  mi2)\melismaEnd mi4 re |
  la2 do'4 do' |
  do'2 do'4 fa |
  do2. fa4 |
  sib la sol fa |
  do'2 fa |
  R1 |
  r2 re'4 re' |
  dod'8[\melisma re' dod' si] la[ sol fa mi]( |
  re2)\melismaEnd re'4 re' |
  re'2 dod'4. dod'8 |
  re'2 la4. la8 |
  sol8\trill[\melisma fa sol la] sol[ sib la sol] |
  fa\trill[ mi fa sol] fa[ la sol fa]( |
  mi2)\trill\melismaEnd mi4 re |
  la2 la4 la |
  la2 la4 re |
  la,2. la4 |
  sol fa mi la |
  fa re la la |
  la2 la4 re |
  la,2. la4 |
  sol fa mi re |
  la2( la,) |
  re2
}
\keepWithTag #'() \reprise
r2 |
R1*11 |
r2 r4 re |
re mi fa re |
sol2 fa4. sol8 |
mi2 fa4. sol8 |
\appoggiatura sol8 do2 r |
R1 |
r2 la4 la |
sold8[\melisma la sold fad] mi[ re do si,]( |
la,2)\melismaEnd la4 la |
la2 sold4. la8 |
la2 mi4. mi8 |
re[\trill\melisma do re mi] re[ fa mi re] |
do[ si, do re] do[ mi re do]( |
si,2\trill)\melismaEnd si,4 la, |
mi2 mi4 mi |
la2 la4 la |
re'2 re'4 sol |
re2. re'4 |
do' sib la sol |
re' re' sol sol |
do'2 do'4. do'8 |
do'2 fa4 fa |
mi8[\melisma fa mi re] do[ sib, la, sol,]( |
fa,2)\melismaEnd fa4 fa |
fa2 mi4.\trill fa8 |
fa2 do'4. do'8 |
sib8\trill[\melisma la sib do'] sib[ re' do' sib] |
la[\trill sol la sib] la[ do' sib la]( |
sol2)\trill\melismaEnd sol4 fa |
do'2 do'4 do |
fa2 r |
R1*7 |
r2 r4 la |
sol fa mi re |
dod2\trill dod4. dod8 |
re2 sib,4.\trill la,8 |
la,4 r r2 |
R1 |
r2 r4 la |
sol fa mi re |
dod2 dod4. dod8 |
re2 sib,4.\trill la,8 |
la,2. la4 |
sol fa mi re |
sol2. fa4 |
mi re dod4. la,8 |
re2 la,4 la |
sol fa mi re |
sol2. fa4 |
sol la sib4. sol8 |
la2( la,) |
re2 r |
R1*5 |
r2 re'4 re' |
\modVersion\reprise