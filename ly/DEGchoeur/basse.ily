\clef "basse" r2 |
R1 |
r2 re'4 re' |
\setMusic #'reprise {
  dod'8 re' dod' si la sol fa mi |
  re2 re'4 re' |
  re'2 dod'4. dod'8 |
  re'2 la4. la8 |
  sol8\trill fa sol la sol sib la sol |
  fa mi fa sol fa la sol fa |
  mi2 mi4 re |
  la2 do'4 do' |
  do'2 do'4 fa |
  do2. fa4 |
  sib la sol fa |
  do'2 fa |
  sol la4 la, |
  re2 re'4 re' |
  dod'8 re' dod' si la sol fa mi |
  re2 re'4 re' |
  re'2 dod'4. dod'8 |
  re'2 la4. la8 |
  sol8\trill fa sol la sol sib la sol |
  fa mi fa sol fa la sol fa |
  mi2 mi4 re |
  la2 la4 la |
  la2 la4 re |
  la,2. la4 |
  sol fa mi la |
  fa re la la |
  la2 la4 re |
  la,2. la4 |
  sol fa mi re |
  la2 la, |
  re2
}
\keepWithTag #'() \reprise
re4 re |
mi2 mi4 mi |
fa re re' do' |
sib2~ sib |
la re'4 re' |
re'2 dod'4. re'8 |
re'2 la4. la8 |
sol8 fa sol la sol sib la sol |
fa mi fa sol fa la sol fa |
mi2 mi4 re |
dod2. re4 |
la re la,2 |
re2. re4 |
re mi fa re |
sol2 fa4. sol8 |
mi2 fa4. sol8 |
do2 do4 do |
re2 mi4 mi, |
<<
  \new CueVoice {
    \voiceOne <>^"[Manuscrit]"
    la,2 la4 la4 |
    sold8 la sold fad mi re do si, |
  }
  { \voiceTwo la,1~ | la,~ | \oneVoice la,2 }
>> la4 la |
la2 sold4. la8 |
la2 mi4. mi8 |
re do re mi re fa mi re |
do si, do re do mi re do |
si,2 si,4 la, |
mi2 mi4 mi |
la2 la4 la |
re'2 re'4 sol |
re2. re'4 |
do' sib la sol |
re'4 re' sol sol |
do'2 do'4. do'8 |
do'2 fa4 fa |
mi8 fa mi re do sib, la, sol, |
fa,2 fa4 fa |
fa2 mi4. fa8 |
fa2 do'4. do'8 |
sib la sib do' sib re' do' sib |
la sol la sib la do' sib la |
sol2\trill sol4 fa |
do'2 do'4 do |
fa2 fa4 fa |
sol2 sol4 sol |
la8 sol la si dod' si dod' la |
re'4 re re re' |
do'2 sib |
la sol |
fa4 sol8 la sib4 sib |
fa sol la la, |
re2. la4 |
sol fa mi re |
dod2 dod4. dod8 |
re2 sib,4.\trill la,8 |
la,2 r |
la, r |
re2. la4 |
sol fa mi re |
dod2 dod4. dod8 |
re2 sib,4.\trill la,8 |
la,2. la4 |
sol fa mi re |
sol2. fa4 |
mi re dod4. la,8 |
re2 la,4 la |
sol fa mi re |
sol2. fa4 |
sol la sib4. sol8 |
la2 la, |
re2 r |
re r |
re re4 re |
sol2 sold4 mi |
la2 r |
re r |
re re'4 re' |
\modVersion\reprise
