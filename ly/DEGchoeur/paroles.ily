Tri -- om -- phez, __ Tri -- om -- phez, Dieu puis -- sant, qui re -- gnez __
\tag #'vhaute-contre { qui re -- gnez, qui re -- gnez }
\tag #'vdessus { qui re -- gnez __ }
\tag #'vtaille { qui re -- gnez }
sur les Dieux,
Qu’on vous ren -- de par tout un é -- ter -- nel hom -- ma -- ge,
Tri -- om -- phez, __ Tri -- om -- phez, Dieu puis -- sant, qui re -- gnez __
\tag #'vhaute-contre { qui re -- gnez, qui re -- gnez }
\tag #'vdessus { qui re -- gnez __ }
\tag #'vtaille { qui re -- gnez }
sur les Dieux,
Qu’on vous ren -- de par tout un é -- ter -- nel hom -- ma -- ge,
Qu’on vous ren -- de par tout un é -- ter -- nel hom -- ma -- ge.

Ver -- sez sur ces cli -- mats mil -- le dons pré -- ci -- eux,
Tri -- om -- phez, __ Tri -- om -- phez, Dieu puis -- sant,
\tag #'vdessus {
  qui re -- gnez __ sur les Dieux, qui re -- gnez __
}
\tag #'vhaute-contre {
  Tri -- om -- phez, Dieu puis -- sant, qui re -- gnez qui re -- gnez
}
\tag #'vtaille { qui re -- gnez __ qui re -- gnez __ qui re -- gnez }
\tag #'vbasse { qui re -- gnez __ qui re -- gnez }
sur les Dieux,
Qu’on vous ren -- de par tout un é -- ter -- nel hom -- ma -- ge,

\tag #'vdessus {
  Tri -- om -- phez, __
  Tri -- om -- phez, Dieu puis -- sant, qui re -- gnez __ sur les Dieux,
  qui re -- gnez __ sur les Dieux.
}
\tag #'vhaute-contre {
  Tri -- om -- phez, __
  Tri -- om -- phez, Dieu puis -- sant, qui re -- gnez __
  Tri -- om -- phez, Dieu puis -- sant, qui re -- gnez
  qui re -- gnez sur les Dieux.
}
\tag #'vtaille {
  Tri -- om -- phez, Dieu puis -- sant,
  Tri -- om -- phez, Dieu puis -- sant, qui re -- gnez sur les Dieux,
  Tri -- om -- phez, Dieu puis -- sant, qui re -- gnez
  qui re -- gnez sur les Dieux.
}
\tag #'vbasse {
  Tri -- om -- phez, Dieu puis -- sant,
  Tri -- om -- phez, __
  Tri -- om -- phez, Dieu puis -- sant, qui re -- gnez __
  qui re -- gnez sur les Dieux.
}

Ver -- sez sur ces cli -- mats mil -- le dons pré -- ci -- eux,
Ver -- sez sur ces cli -- mats mil -- le dons pré -- ci -- eux,
Loin de ces tris -- tes lieux lais -- sez gron -- dez l’o -- ra -- ge,
Loin de ces tris -- tes lieux lais -- sez gron -- dez l’o -- ra -- ge,

\tag #'(vdessus vhaute-contre vtaille) {
  Tri -- om -- phez, __
}
\origVersion {
  \tag #'(vdessus vhaute-contre vtaille) { Tri -- om -- phez, __ }
  Tri -- om -
}
\modVersion {
  \tag #'vtaille { Tri -- om -- phez, __ }
  Tri -- om -- phez, __ Tri -- om -- phez, Dieu puis -- sant, qui re -- gnez __
  \tag #'vhaute-contre { qui re -- gnez, qui re -- gnez }
  \tag #'vdessus { qui re -- gnez __ }
  \tag #'vtaille { qui re -- gnez }
  sur les Dieux,
  Qu’on vous ren -- de par tout un é -- ter -- nel hom -- ma -- ge,
  Tri -- om -- phez, __ Tri -- om -- phez, Dieu puis -- sant, qui re -- gnez __
  \tag #'vhaute-contre { qui re -- gnez, qui re -- gnez }
  \tag #'vdessus { qui re -- gnez __ }
  \tag #'vtaille { qui re -- gnez }
  sur les Dieux,
  Qu’on vous ren -- de par tout un é -- ter -- nel hom -- ma -- ge,
  Qu’on vous ren -- de par tout un é -- ter -- nel hom -- ma -- ge.
}
