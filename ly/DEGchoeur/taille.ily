\clef "taille" r2 |
R1 |
r2 fa'4 fa' |
\setMusic #'reprise {
  mi'8 fa' mi' re' dod' la si dod' |
  re'2 re'4 re' |
  mi'2 mi'4.\trill mi'8 |
  fa'2 fa'4. fa'8 |
  mi'8\trill re' mi' fa' mi' sol' fa' mi' |
  re'2 re'4 re' |
  dod'2 dod'4 re' |
  dod'2 do'4 do' |
  do'2 do'4 do' |
  do'2. do'4 |
  sib4 do' sib do' |
  do'2 do' |
  R1 |
  r2 fa'4 fa' |
  mi'8 fa' mi' re' dod' la si dod' |
  re'2 re'4 re' |
  mi'2 mi'4.\trill mi'8 |
  fa'2 fa'4. fa'8 |
  mi'8\trill re' mi' fa' mi' sol' fa' mi' |
  re'2 re'4 re' |
  dod'2 dod'4 re' |
  dod'2 dod'4 re' |
  re'2 dod'4 si |
  dod'2. re'4 |
  mi'4 fa' sol' mi' |
  fa' fa' dod' dod' |
  re'2 dod'4 si |
  dod'2. re'4 |
  dod' re' dod' re' |
  re'2 dod' |
  re'2
}
\keepWithTag #'() \reprise
fa'4 fa' |
mi' mi' dod' dod' |
re'2 fa'4 mi' |
re'2 re'4. mi'8 |
dod'2 fa'4. fa'8 |
sib'2 la'4. la'8 |
la'2 la'4. la'8 |
la'2 dod'4. dod'8 |
re'2 re'4 re' |
mi'2 sol'4 fa' |
la'2. fa'4 |
mi' fa'8 sol' la'4. mi'8 |
fa'2. la4 |
la sol la la |
sol2 si4. si8 |
do'2 la4. si8 |
do'2 do'4 do' |
si8 do' si la sold fad mi re |
do2 la4 la |
si2 si4.\trill si8 |
do'2 do'4. do'8 |
si8\trill la si do' si re' do' si |
la8\trill sold la si la do' si la |
re'2 si4. si8 |
la8 sold la si la do' si la |
sold2 sold4 la |
la2 la4 sold |
la2 dod'4 dod' |
re'2 re'4 re' |
re'2. fad'4 |
fad' sol' do' re' |
re' re' re' re' |
do'2 do'4. do'8 |
do'2 do'4 do' |
do'2 do'4. do'8 |
do'2 do'4 do' |
re'2 do'4 do' |
do'2 do'4 fa' |
fa'2 mi'4.\trill fa'8 |
fa'2 do'4. do'8 |
re'2 do'4 do' |
do'2 do'4 do' |
do'2 fa'4 fa' |
mi'2 mi'4 mi' |
la dod' mi' mi' |
re'2 la'4 fa' |
sol' do' fa' sib' |
mi' la' re' sol' |
la' sol'8 fa' sib' la' sol' sib' |
la'4 sib' la'4. mi'8 |
fa'2. la4 |
la la sol la |
la2 la4. la8 |
la2 sol4. sol8 |
mi4 r r2 |
R1 |
r2 r4 la |
la la sol la |
la2 la4. la8 |
la2 sol4. sol8 |
mi2.\trill la4 |
dod' re' la la |
la2. la4 |
sol la la4. la8 |
la2 la4 la |
dod' re' la la |
la2. la4 |
sib la re'4. re'8 |
re'2( dod') |
re' fa'4 fa' |
re'8 mi' fa' sol' fa' mi' re' dod' |
re'2 la'4 la' |
sol'2 mi'4 mi' |
mi'2 fa'4 fa' |
re'8 mi' fa' sol' fa' mi' re' dod' |
re'2 fa'4 fa' |
\modVersion\reprise
