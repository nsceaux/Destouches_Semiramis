\clef "dessus" fa''4 fa'' |
mi''8 fa'' mi'' re'' dod'' si' la' sol' |
fa'2 re''4 re'' |
\setMusic #'reprise {
  mi''2 mi''4.\trill mi''8 |
  fa''2 fa''4. fa''8 |
  mi''8\trill re'' mi'' fa'' mi'' sol'' fa'' mi'' |
  re''\trill dod'' re'' mi'' re'' fa'' mi'' re'' |
  dod''2 dod''4. dod''8 |
  re''8 dod'' re'' mi'' re'' fa'' mi'' re'' |
  sol''2 sol''4 fa'' |
  mi''2 mi''4 mi'' |
  mi''2 mi''4 fa'' |
  mi''2. fa''4 |
  re'' re'' mi'' fa'' |
  mi'' mi'' fa'' fa'' |
  mi''8 fa'' mi'' re'' dod'' si' la' sol' |
  fa'2 re''4 re'' |
  mi''2 mi''4.\trill mi''8 |
  fa''2 fa''4. fa''8 |
  mi''8\trill re'' mi'' fa'' mi'' sol'' fa'' mi'' |
  re''\trill dod'' re'' mi'' re'' fa'' mi'' re'' |
  dod''2 dod''4. dod''8 |
  re'' dod'' re'' mi'' re'' fa'' mi'' re'' |
  sol''2 sol''4 fa'' |
  mi''2 mi''4 mi'' |
  fa''2 mi''4 re'' |
  mi''2.\trill re''4 |
  dod'' re'' mi'' dod'' |
  re'' re'' mi'' mi'' |
  fa''2 mi''4 re'' |
  mi''2.\trill fa''4 |
  mi'' re'' mi''4. fa''8 |
  fa''2( mi'')\trill |
  re''2
}
\keepWithTag #'() \reprise
la''4 la'' |
sol''8 la'' sol'' fa'' mi'' re'' dod'' si' |
la'2 la''4 la'' |
la''2 sol''4.\trill la''8 |
la''2 la''4. la''8 |
sol'' fa'' sol'' la'' sol'' sib'' la'' sol'' |
fa'' mi'' fa'' sol'' fa'' la'' sol'' fa'' |
mi''2 mi''4. mi''8 |
la'2 re''4 mi'' |
dod''2 mi''4 fa'' |
sol''8 la'' sol'' fa'' mi'' fa'' re'' mi'' |
dod''4 re''8 mi'' mi''4.\trill re''8 |
re''2. fa''4 |
fa'' mi'' re'' do'' |
si'2 re''4. mi''8 |
do''4. re''8 re''4.\trill do''8 |
do''2 mi''4 mi'' |
re''8 mi'' re'' do'' si' la' sold' fad' |
mi'2 mi''4 mi'' |
mi''2 re''4. mi''8 |
mi''2 mi''4. mi''8 |
re''8 do'' re'' mi'' re'' fa'' mi'' re'' |
do''\trill si' do'' re'' do'' mi'' re'' do'' |
si'2 si'4 si' |
mi''2 mi''4. mi''8 |
re''8\trill do'' re'' mi'' re'' mi'' do'' re'' |
si'2 si'4 mi'' |
dod''2 mi''4 mi'' |
fad''2 fad''4 sol'' |
fad''2. re''4 |
re'' mi'' fad'' sol'' |
fad'' fad'' sol'' sol'' |
mi''8 fa'' mi'' re'' do'' sib' la' sib' |
sol'2 fa''4 fa'' |
sol''2 sol''4.\trill sol''8 |
la''2 do''4. do''8 |
sib'8\trill la' sib' do'' sib' re'' do'' sib' |
la'\trill sol' la' sib' la' do'' sib' la' |
re''2 re''4 re'' |
do''2 do''4. do''8 |
sib'8 la' sib' do'' sib' do'' la' sib' |
sol'2\trill sol'4 do'' |
la'2\trill la''4 la'' |
sol''8 la'' sol'' fa'' mi'' fa'' re'' mi'' |
dod''4 la' la'' la'' |
fa'' re'' fa'' sib'' |
sib'' la'' la'' sol'' |
sol'' fa'' fa'' mi''8 fa'' |
re'' fa'' mi'' fa'' sol'' fa'' mi'' re'' |
la''4 sol''8 fa'' mi''4.\trill re''8 |
re''2. fa''4 |
mi'' re'' dod'' re'' |
la'2 sol'4. la'8 |
fa'4.( sol'8) sol'4.\trill la'8 |
la'4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { la''4 sol'' fa'' |
    mi'' mi''8( fa''16) sol'' fa''4\trill mi'' |
    fa''8 sol'' fa'' mi'' re''4 }
  { fa''4 mi'' re'' |
    dod'' dod''8( re''16) mi'' re''4\trill dod'' |
    re''2 la'4 }
>> fa''4 |
mi'' re'' dod'' re'' |
la'2 sol'4. la'8 |
fa'4.( sol'8) sol'4.\trill la'8 |
la'2. fa''4 |
mi'' re'' dod'' si' |
dod''2.\trill re''4 |
dod'' re'' mi''4. fa''8 |
fa''2\trill mi''4 fa'' |
mi'' re'' dod'' si' |
dod''2.\trill fa''4 |
mi'' dod'' re''4. mi''8 |
fa''2( mi'')\trill |
re'' la'4 la' |
fa'8 sol' la' sib' la' sol' fa' mi' |
fa'2 fa''4 fa'' |
re''8 mi'' fa'' mi'' re'' do'' si' mi'' |
dod''4\trill la' la' la' |
fa'8 sol' la' sib' la' sol' fa' mi' |
fa'2 re''4 re'' |
\modVersion\reprise
