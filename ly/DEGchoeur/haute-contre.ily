\clef "haute-contre" la'4 la' |
sol'8 la' sol' fa' mi' re' dod' si |
la2 la'4 la' |
\setMusic #'reprise {
  la'2 sol'4.\trill la'8 |
  la'2 la'4. la'8 |
  sol'8 fa' sol' la' sol' sib' la' sol' |
  fa' mi' fa' sol' fa' la' sol' fa' |
  sol'2 sol'4. sol'8 |
  la'2 la'4 la' |
  la'2 la'4 la' |
  la'2 sol'4 sol' |
  sol'2 sol'4 la' |
  sol'2.\trill la'4 |
  fa' fa' sol' la' |
  sol'\trill sol' la' la' |
  sol'8 la' sol' fa' mi' re' dod' si |
  la2 la'4 la' |
  la'2 la'4. la'8 |
  la'2 la'4. la'8 |
  sol'8\trill fa' sol' la' sol' sib' la' sol' |
  fa'8\trill mi' fa' sol' fa' la' sol' fa' |
  sol'2 sol'4 sol' |
  la'2 la'4 la' |
  la'2 la'4 la' |
  la'2 la'4 la' |
  la'2 sol'4 fa' |
  sol'2. fa'4 |
  sol'4 la' la' la' |
  la' la' la' la' |
  la'2 sol'4 fa' |
  sol'2. fa'4 |
  sol' la' sol' la' |
  la'2. la'4 |
  la'2
}
\keepWithTag #'() \reprise
re''4 re'' |
dod'' mi'' mi' sol' |
fa'2 la'4 do'' |
fa'2 sib'4. sib'8 |
mi'2 re''4. re''8 |
mi''2 mi''4. mi''8 |
la'2 re''4. re''8 |
dod''2 la'4. mi'8 |
fa'2 la'4 la' |
sol'2 dod''4 re'' |
mi''2. la'4 |
la' re'' dod''4. re''8 |
re''2. re'4 |
re' do' do' fa' |
re'2 re'4. re'8 |
sol'2 fa'4. fa'8 |
mi'2 r |
R1 |
r2 do''4 do'' |
si'8 do'' si' la' sold' mi' fad' sold' |
la'2 mi'4 mi' |
fa'2 fa'4. fa'8 |
mi'2 la'4 la' |
la'2 sold'4. la'8 |
la'2 mi'4. mi'8 |
mi'2 mi'4 mi' |
mi'2 mi'4 mi' |
mi'2 mi'4 la' |
la'2 la'4 sib' |
la'2. la'4 |
la' sib' do'' sib' |
la' la' sib' sib' |
sol'8 la' sol' fa' mi' sol' fa' sol' |
mi'2 la'4 la' |
sol'2 mi'4. mi'8 |
fa'2 la'4. la'8 |
sol'8\trill fa' sol' la' sol' sib' la' sol' |
fa'2 fa'4 fa' |
sol'2 sol'4.\trill sol'8 |
la'2 fa'4. fa'8 |
fa'2 mi'4 fa' |
fa'2 fa'4 mi' |
fa'2 do''4 re'' |
sib'8 do'' sib' la' sol' la' fa' sol' |
mi'2 la'4 la' |
la'2 re''4 re'' |
mi''2 re'' |
dod''4 re'' sib'2 |
la'4 re'' re'' re'' |
re'' mi''8 re'' dod''4. re''8 |
re''2. re'4 |
dod' re' mi' fa' |
mi'2 mi'4. mi'8 |
re'2 re'4. mi'8 |
dod'4 r r2 |
R1 |
r2 r4 re' |
dod' re' mi' fa' |
mi'2 mi'4. mi'8 |
re'2 re'4. mi'8 |
dod'2. re'4 |
mi' fa' sol' fa' |
mi'2. re'4 |
mi' fa' mi'4. dod'8 |
re'2 dod'4 re' |
mi' fa' sol' fa' |
mi'2. re'4 |
mi' mi' sol'4. sib'8 |
la'2. la'4 |
la'2 fa'4 fa' |
re'8 mi' fa' sol' fa' mi' re' dod' |
re'2 re''4 re'' |
si'8 do'' re'' do'' si' la' sold' si' |
la'2 fa'4 fa' |
re'8 mi' fa' sol' fa' mi' re' dod' |
re'2 la'4 la' |
\modVersion\reprise
