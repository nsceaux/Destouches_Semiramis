Com -- mande à l’Em -- pi -- re
Te -- ne -- breux.
Tout cons -- pi -- re
Pour tes vœux.
Qu’on res -- pi -- re
Mil -- le feux.
Com - feux.

Sou -- flons la guer -- re.
Cou -- vrons la ter -- re
De sang & de morts.
Fai -- sons des ef -- forts
E -- gaux au ton -- ner -- re.
Souf -- flons la guer -- re
Peu -- plons les som -- bres bords.

Cou -- vrons la ter -- re
De sang & de morts.
Fai -- sons des ef -- forts
E -- gaux au ton -- ner -- re.
Fai -- sons des ef -- forts
E -- gaux au ton -- ner -- re.
