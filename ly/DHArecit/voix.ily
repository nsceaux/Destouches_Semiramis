<<
  %% Amestris
  \tag #'(amestris basse) {
    \amestrisMark R1*5 |
    r2 r4 r8 do'' |
    do''4. sib'8 sib'4. do''8 |
    \appoggiatura sib'16 lab'2 sol'4. sol'8 |
    sol'2 fa'4 sol' |
    \appoggiatura fa'16 mib'2 mib' |
    r4 r8 sol' lab'4. sib'8 |
    sib'4. sol'8 do''4 sib' lab' sol' |
    fa'2\trill fa' |
    r4 sib' re'' mib'' |
    lab'2.( sol'8) lab'8 |
    sol'2\trill sol' |
    r4 sib'8 sib' si'4\trill si'8 do'' |
    do''2 r4 do''8 do'' |
    mi'2 mi'4 r8 sol' |
    re''2 r4 mib''8 fa'' |
    sol''2. do''8 re'' |
    si'2\trill <<
      \tag #'basse { s2 s1*4 s2 \amestrisMark }
      \tag #'amestris { r2 R1*4 r2 }
    >> mib''8 mib''16 re'' do''8 sol' |
    \appoggiatura sol'16 la'4 si'8 do'' si'4 si'8 do'' |
    \appoggiatura do''16 re''4 re'' <<
      \tag #'basse { s1*2 s4. \amestrisMark }
      \tag #'amestris { r2 r R1 r4 r8 }
    >> lab'8 lab'4 sol'8 sol' do''4 re''8 mib'' |
    re''2 sol'4 <<
      \tag #'basse { s2. s1 s2 \amestrisMark }
      \tag #'amestris { r4 r2 R1 r2 }
    >> re''8 do'' sib' la' sol'4\trill\prall sol'8 fad' |
    sol'4 sol' mib'' |
    lab'4. lab'8 fa' lab' |
    re'2 sol'8 sol' |
    re''2 do''8 re'' |
    \appoggiatura re''16 mib''2 do''8 do'' |
    sol'2 la'8 sib' |
    la'2\trill r4 la'8 la' sib'4 sib'8 do'' |
    \appoggiatura do''16 re''4. sib'8 do'' re'' |
    do''4.\trill\prall sib'8 la' sib' |
    fad'4\trill fad' la' la'8 sib' do''4 re'' |
    si'2 do''8 do''16 do'' re''8 mib'' |
    re''2 re''4 |
    sol''2 mi''8 mi'' |
    fa''2 fa''8 sol'' |
    mi''2 fa''4 fa''( mib''8)\trill re'' do'' sib' |
    la'2\trill si'4 do''8 re'' sol'4. mib''8 |
    mib''2( re''1)\trill |
    do''2 <<
      \tag #'basse { s1 s1*2 s1. s1 s2. s1*5 s1. s1 s4 \amestrisMark }
      \tag #'amestris { r2 r R1*2 R1. R1 R2. R1*5 R1. R1 r4 }
    >> sol'8 sol' lab'4 lab'8 sib' |
    \appoggiatura lab'8 sol'4. sol'8 do'' re'' mib'' fa'' |
    \appoggiatura fa'' sol''2 re''4 re''8 re'' \appoggiatura re''8 mib''4. mib''8 |
    do''4. do''8 lab'\trill\prall lab' lab' sol' |
    sol'4\trill sol'8 <<
      \tag #'basse { s8 s2 s2. s2 \amestrisMarkText "vivement" }
      \tag #'amestris { r8 r2 R2. r2 <>^\markup\italic vivement }
    >> si'4 si'8 do'' |
    do''4 do''8 do'' sib'4 sib'8 do'' |
    la'2\trill la'4 la'8 sib' |
    sib'4 sib'8 sib' lab'4 lab'8 sib' |
    sol'2\trill sol'4 sol'8 lab' |
    lab'4 lab'8 sol' fa'4\trill fa'8 sol' |
    mib'2 mib''8 mib''16 re'' do''8 sib' |
    la'2\trill fa''8 fa''16 mib'' re''8 do'' |
    si'2. si'8 si' |
    do''2. do''8 re'' |
    si'2 r4 do''8 do'' |
    do''4.( si'8) si'4.\trill do''8 |
    do''2 mib''4 fa''8 sol'' re''4. mib''8 |
    si'4 sib'8 sib' sib'4 sib'8 la' |
    la'4\trill la' <>^\markup\italic tendrement lab'4 lab'8 lab' lab'4. sib'8 |
    \appoggiatura lab'8 sol'2 sol'4 sol' |
    \afterGrace sol'2.( fa'8) mib' fa' |
    sol'2 sol'4 r8 lab' |
    re'2 r |
    r <>^\markup\italic tendrement sol'4 sol'8 sol' |
    sib'4. sib'8 do''4. do''8 re''4. mib''8 |
    re''4.\trill re''8 re''4. fa''8 mib''4.\trill\prall re''8 |
    \appoggiatura re''8 mib''2 do''4 do''8 sib' lab'4.\trill\prall sol'8 |
    \appoggiatura sol'8 lab'2 lab'4 |
    lab'4. sol'8 sol' sol' |
    do''2 do''8 do'' |
    fa'2 fa'8 sol' |
    mi'2 mi'4 |
    lab'8 lab' la'4\trill\prall sol'8 la' |
    \appoggiatura la'8 sib'2 si'8 si' |
    do''2 do''8 re'' |
    si'2 mib''8 mib'' |
    \appoggiatura fa''8 mi''4. fa''8 sol''4 |
    re''2 mib''8 re'' |
    do''2\trill\prall do''8 si' |
    do''2
    \tag #'amestris { r2 R1*6 }
  }
  %% Arsane
  \tag #'(arsane basse) {
    <<
      \tag #'basse { s1*11 s1. s1*9 s2 \arsaneMarkText "vivement" }
      \tag #'arsane { \arsaneMark R1*11 R1. R1*9 r2 <>^\markup\italic vivement }
    >> re'4 re'16 mib' fa' sol' |
    mi'4.\trill do'8 sol' sol' fa' sol' |
    \appoggiatura sol'16 lab'4 lab'8 fa' re' re' mib' fa' |
    sib4. mib'8 do'4.\trill fa'8 |
    re'2\trill mib'4 mib'8 re' |
    \appoggiatura re'16 mib'2 <<
      \tag #'basse { s2 s1 s2 \arsaneMarkText "vivement" }
      \tag #'arsane { r2 R1 r2 <>^\markup\italic vivement }
    >> sol'2 si8 si si do' |
    do'4 re'8 mib' fa'4 mib'8 re' |
    \appoggiatura re'16 mib'4. <<
      \tag #'basse { s8 s1 s2. \arsaneMarkText "vivement" }
      \tag #'arsane { r8 r2 r r2 r4 <>^\markup\italic vivement }
    >> re'8 mi' fa'4 fa'8 sol' |
    mi'4 mi'8 mi' mi'4 fad'8 sol' |
    fad'2 <<
      \tag #'basse {
        s1 s2.*6 s1. s2.*2 s1. s1 s2.*5 s1.*2 s2 \arsaneMarkText "vivement"
      }
      \tag #'arsane {
        r2 r R2.*6 R1. R2.*2 R1. R1 R2.*5 R1.*2 r2 <>^\markup\italic vivement
      }
    >> sol'8 fa' mib' re' mi'4 mi'8 fa' |
    fa'4 lab'8 lab' re'4 re'8 sol' |
    do'4 re'8 mib' si4 do'8 re' |
    sol2 do'8 re' mib' fa' sol'4 sol'8 do'8 |
    si4\trill si8 sol'16 fa' mib'4 mib'8 re' |
    do' do' re' mib' mib' re' |
    \appoggiatura re'16 mib'8. mib'16 mib'8 sol' mib' re' do' sib |
    la\trill la re' sol' do' do'16 sib la8 re' |
    sol4. sol'8 re'\trill re' re' mi' |
    \appoggiatura mi'? fa'4 fa'8 fa' fa'4 sol'8 re' |
    \appoggiatura re' mib'4 mib'8 mib' la4. fa'8 |
    re'2\trill sol'4 re'8 mib' fa'4. sol'8 |
    si8. do'16 re'8 mib' re'2\trill |
    do'4 <<
      \tag #'basse { s2. s1 s1. s1 s4. \arsaneMarkText "vivement" }
      \tag #'arsane { r4 r2 R1 R1. R1 r4 r8 <>^\markup\italic vivement }
    >> sol'16 fa' mi'4 re'8 do' |
    fa'4 fa'8 fa'16 sol' lab'8 fa' |
    re'4\trill re' <<
      \tag #'basse {
        s2 s1*11 s1. s1 s1. s1*5 s1.*3 s2.*12 s2 \arsaneMarkText "vivement"
      }
      \tag #'arsane {
        r2 |
        r mi'4 mi'8 fa' |
        fa'4 fa'8 fa' mib'4 mib'8 fa' |
        re'2\trill re'4 re'8 mib' |
        mib'4 mib'8 mib' reb'4 reb'8 mib' |
        do'4\trill do'8 do' re'4\trill re'8 mib' |
        mib'2 sol'8 sol'16 fa' mib'8 re' |
        do'2\trill lab'8 lab'16 sol' fa'8 mib' |
        re'2.\trill sol'8 sol' |
        \afterGrace sol'2.( fa'8)\trill mib' fa' |
        \appoggiatura fa' sol'2 r4 fa'8 lab' |
        re'2 re'4. mib'8 |
        do'2 r r |
        R1 R1. R1*5 R1.*3 R2.*12 r2 <>^\markup\italic vivement
      }
    >> mib'2 |
    do'8 re' mib' fa' sol' sol' fa' mib' |
    re'4\trill re' mib' fa'8 sol' |
    do'\trill do' do' re' mi'4 mi'8 fa' |
    fa'4 r16 re' re' re' sol4. re'8 |
    mib'4 fa'8
    << \new CueVoice { \voiceOne lab'^"[manuscrit]" }
      { \voiceTwo sol' \oneVoice } >>
    si4 si8 do' |
    do'1 |
  }
>>
