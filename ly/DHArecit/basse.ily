\clef "basse" do2 mib |
re4 do si, sol, |
mib2 re4 do |
fa sol lab fa |
sol fa sol sol, |
do1 |
re2 mi |
fa sol |
la si |
do'1~ |
do' |
sol2 lab4 sol fa mib |
sib1 |
r4 sol fa mib |
sib,1 |
mib |
r2 re |
do r |
sib, r |
lab, r |
mib,2. fa,4 |
sol,2 si, |
do mi |
fa sib4 lab |
sol2 lab |
sib mib4 sib, |
mib2. mi4 |
fa2 sol4 do |
sol1 fa2 |
mib2 re |
do sib, lab,4 fa, |
sol,2 sol si, |
do dod |
re sib, mib8 do re re, |
sol,2 do4 |
fa2. |
sol |
si, |
do |
mi |
fa2 mib re4 do |
sib,2. |
do |
re1 fad,2 |
sol,4 sol8 fa mib4 re8 do |
sol2. |
sib |
lab2 sib4 |
do'2 fa4 |
lab2 mi4 |
fa1 mib4 do |
sol2 sol,1 |
do2 do' sib2 |
lab4 fa sib4. mib8 |
lab4 fa sol fa |
mib16 re mib fa sol fa mib re do2 si,4 do |
sol2 do'4 sol |
lab8 sol fa mib sib sib, |
mib2 do |
re4 sib, la, re |
mib2 sib |
la si |
do'4 do fa2 |
sol2. fa8 mib re4 do |
fa,2 sol, |
do1~ |
do4 sib, lab,2 |
sol, sol do'4 sol |
lab2 fa4 sib8 sib, |
mib2 do4 sib, |
lab,2 fa,4 |
sol,2 sol4 sol, |
do2 do'4 do |
fa2 fa4 fa, |
sib,2 sib4 sib, |
mib2 mib'4 mib |
lab2 sib4 sib, |
mib1 | \allowPageTurn
fa |
sol |
lab |
sol4 fa mib fa |
sol2 sol, |
do1 fa2 |
sol mi |
fa re1 |
mib2 do4 sib, |
lab,1 |
sol,2. do4 |
si,2 r |
r mib |
re do1 |
sib,2 si,1 |
do2 r mi |
fa2. |
sib2 mib4 |
la,2. |
si, |
do |
fa4 mib2 |
re sol4 |
lab2. |
sol2 do'4 |
sib2 sol4 |
lab2 mib8 fa |
sol4 sol,2 |
do1 |
lab2 mib4 fa |
sol1 |
lab2 sol |
fa mib4 si, |
do fa, sol,2 |
do1 |
