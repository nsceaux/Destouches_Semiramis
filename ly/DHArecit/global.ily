\key sol \minor \tempo "Lentement"
\time 2/2 \midiTempo#160 s1*11
\time 3/2 s1.
\time 2/2 s1*9 \midiTempo#80 s1
\time 2/2 s1*6
\time 3/2 \grace s16 \midiTempo#160 s1.
\time 2/2 s1
\time 3/2 \grace s16 \midiTempo#160 s1.*2
\time 2/2 s1
\time 3/2 s1.
\digitTime\time 3/4 s2.*6
\time 3/2 s1.
\digitTime\time 3/4 \grace s16 s2.*2
\time 3/2 s1.
\time 2/2 \midiTempo#80 s1
\digitTime\time 3/4 \midiTempo#160 s2.*5
\time 3/2 s1.*3
\time 2/2 s1*2
\time 3/2 \midiTempo#80 s1.
\time 2/2 s1
\digitTime\time 3/4 s2.
\time 2/2 \grace s16 s1*5
\time 3/2 \midiTempo#160 s1.
\time 2/2 \midiTempo#80 s1*3
\time 3/2 \midiTempo#160 \grace s8 s1.
\time 2/2 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\digitTime\time 2/2 \midiTempo#160 s1*12
\time 3/2 s1.
\time 2/2 s1
\time 3/2 s1.
\time 2/2 \grace s8 s1*5
\time 3/2 s1.*3
\digitTime\time 3/4 \grace s8 s2.*12
\digitTime\time 2/2 \midiTempo#80 s1*7 \bar "|."
