\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'amestris \includeNotes "voix"
    >> \keepWithTag #'amestris \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'arsane \includeNotes "voix"
    >> \keepWithTag #'arsane \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*7\break \grace s8 s1*4 s2 \bar "" \break s1*5\break s1*4\pageBreak
        s1*2 s2 \bar "" \break s2 s1*2\break s1*2 s2 \bar "" \break s2 s1. s2 \bar "" \break s2 s1. s1 \bar "" \break s2 s1*2 \bar "" \pageBreak
        s2 s2.*4\break \grace s8 s2.*2 s1.\break \grace s8 s2.*2 s1.\break s1 s2.*3\break s2.*2 s1.*2\break s1. s1\pageBreak
        s1 s1.\break s1 s2.\break \grace s8 s1 s2 \bar "" \break s2 s1 s2 \bar "" \break s2 s1 s1.\break s1*2 s2 \bar "" \pageBreak
        s2 s1. s1\break s1 s2. s1\break s1*2 s2 \bar "" \break s2 s1*2\pageBreak
        s1*2\break s1*4\break s1. s1 s2 \bar "" \break s1*4\break s1*2 s1. s2 \bar "" \pageBreak
        s1 s1. s2.\break s2.*4\break s2.*4\break \grace s8 s2.*3 s1\break s1*2\break s1*2\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
