\tag #'(amestris basse) {
  Je vais su -- bir la loi que le sort me dis -- pen -- se ;
  De tant d’hon -- neurs pro -- mis à ma nais -- san -- ce,
  Un seul tom -- beau me res -- te. Et ce jour que je vois,
  Cet -- te Ter -- re, ces Cieux, tout va fuir de -- vant moi.
}
\tag #'(arsane basse) {
  Eh ! vous y con -- sen -- tez, Grands Dieux, le puis- je croi -- re !
  Non, non, fuy -- ez ces lieux, ve -- nez, ve -- nez, sau -- vez vos jours.
}
\tag #'(amestris basse) {
  Dois- je les con -- ser -- ver, aux dé -- pens de ma gloi -- re !
}
\tag #'(arsane basse) {
  Quoi ! vous me ha -- ïs -- sez jus -- qu’à fuir mon se -- cours !
}
\tag #'(amestris basse) {
  Sei -- gneur, je ne puis que vous plain -- dre.
}
\tag #'(arsane basse) {
  Dans quel tris -- te mo -- ment, plai -- gnez- vous mon mal -- heur ?
}
\tag #'(amestris basse) {
  Lors -- que je n’ai plus à vous crain -- dre.
  Aux por -- tes du tré -- pas je vous ou -- vre mon cœur.
  J’ai con -- nu vos ver -- tus ; & ma fein -- te ri -- gueur
  Ne m’a que trop cau -- sé d’al -- lar -- mes ;
  Que ne pou -- vois-je, he -- las ! fai -- re vô -- tre bon -- heur !
  Le Ciel a vû seul ma dou -- leur,
  Il sçait com -- bien pour vous, j’ai dé -- vo -- ré de lar -- mes.
}
\tag #'(arsane basse) {
  Eh ! je vais vous perdre à ja -- mais,
  Est-ce à vous d’ex -- pi -- er de cou -- pa -- bles for -- faits !
  Que le Ciel s’em -- brase & qu’il ton -- ne,
  Que la guer -- re s’al -- lume en -- tre les E -- le -- mens.
  De la su -- per -- be Ba -- by -- lo -- ne
  Qu’ils ren -- ver -- sent les fon -- de -- mens :
  Qu’im -- por -- te quel tré -- pas leur fu -- reur nous ap -- prê -- te,
  Vi -- vez, vi -- vez, & que sur nous re -- tom -- be la tem -- pê -- te.
}
\tag #'(amestris basse) {
  Un Mor -- tel o -- se- t’il bra -- ver les Dieux van -- geurs ?
  A leurs su -- prê -- mes loix, il faut que tout se ren -- de.
}
\tag #'(arsane basse) {
  Ah ! qu’ils cher -- chent ail -- leurs leur sa -- cri -- lege of -- fran -- de.
}
\tag #'(amestris basse) {
  Ciel e -- qui -- table, ou -- bli -- ez ses fu -- reurs.
  Ciel e -- qui -- table, ou -- bli -- ez ses fu -- reurs.
  Ciel e -- qui -- table, ou -- bli -- ez ses fu -- reurs.
  Tom -- bent sur moi vos coups.
  Tom -- bent sur moi vos coups.
  ou -- bli -- ez ses fu -- reurs.
  ou -- bli -- ez ses fu -- reurs.
}
\tag #'arsane {
  Ciel im -- pla -- cable, é -- pui -- sez vos ri -- gueurs.
  Ciel im -- pla -- cable, é -- pui -- sez vos ri -- gueurs.
  é -- pui -- sez vos ri -- gueurs.
  Tom -- bent sur moi vos coups.
  Tom -- bent sur moi vos coups.
  é -- pui -- sez vos ri -- gueurs.
  é -- pui -- sez vos ri -- gueurs.
}
\tag #'(amestris basse) {
  Non, lais -- sez- moi mou -- rir in -- no -- cen -- te vic -- ti -- me ;
  Mais n’ou -- bli -- ez ja -- mais le beau feu qui m’a -- ni -- me.
  A -- dieu. Puis -- se le Ciel vous voir d’un œil plus doux !
  Ren -- dez ce Peuple heu -- reux, qu’il me re -- trouve en vous :
  Qu’il rende à vos ver -- tus un tri -- but lé -- gi -- ti -- me ;
  Que de vô -- tre bon -- heur rien ne bor -- ne le cours :
  Qu’ils vous don -- nent ces Dieux, ce qu’ils m’ô -- tent de jours.
}
\tag #'arsane {
  Moi ! je se -- rois com -- pli -- ce de leur cri -- me !
  Rei -- ne bar -- ba -- re, in -- fi -- del -- les Sol -- dats :
  Je vous at -- tens, o -- sez l’ar -- ra -- cher de mes bras.
}
