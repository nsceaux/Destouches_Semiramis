\clef "basse"
<<
  \tag #'basse { r4 | R2.*3 | R1. R1*3 | }
  \tag #'basse-continue {
    re4\repeatTie~ |
    re2. |
    sol |
    dod |
    re1 do2 |
    sib,2. sol,4 |
    la,1 |
    re |
  }
>>
R1*2 |
<>^"Basses de violes" sib2 do'4 mib' |
re' do' re' sib |
<>^"Basses de violons" fa2 sol4 sib |
la sol la fa |
<>^"Tous et la contre-basse" sib,2 do4 mib |
re do re sib, |
sol, la, si, sol, |
do2 reb |
do4 re mi do |
fa,2 sol,4 sib, |
la, sol, la, fa, |
sib,2 do4 mib |
re do re sib, |
mib2 r |
re r |
do4 fa fa fa |
sib, mib, fa,2 |
sib,4 sib, sib, sib |
sol sol sol sib |
mib mib mib mib |
\sugNotes { sib, sib, sib, sib, | }
sib, sib, sib, sib, |
do do do do |
do do do do' |
la la la sib |
sol2~ sol\trill |
fa4 fa, fa, fa, |
fa,2 fa4 fa |
lab lab lab lab |
mib mib mib mib |
sol sol sol fa |
mib2~ mib\trill |
re4 re8 re re4 re |
re'4 re'8 re' sib4 sol |
do do8 do do4 do' |
fa4. fa8 fa4 do |
fa fa8 fa mib4 mib |
re mib fa fa, |
sib,2*3/4~ \hideNotes sib,8
