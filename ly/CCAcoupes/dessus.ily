\clef "dessus2" r4 |
R2.*3 |
R1. |
R1*3 |
<>^"Tous"_"Détachez" fa'2 sol'4 sib' |
la' sol' la' fa' |
sib'2 sib'4( la') |
sib'( la') sib' re'' |
do'' fa' fa' mib'8\trill re' |
mib'4. mib'8 mib'4.\trill re'8 |
re'4. mib'8 mib'4.\trill( re'16 mib') |
fa'4 mib' fa' re' |
sib' la' sol' fa' |
mi' do'' do'' sib' |
sib' lab' sol' la'8 sib' |
la'4.( sib'8) sib'4.\trill( la'16 sib') |
do''4. do''8 do''4.(\trill sib'16 do'') |
re''4 sib' sib' lab'8\trill sol' |
lab'4. lab'8 lab'4.\trill sol'8 |
sol'4 do'' do'' do'' |
fa' sib' sib' sib' |
mib'2. fa'8 mib' |
re'4. do'8 do'4.\trill sib8 |
sib2. fa'4\doux |
sib'2 sib'4 re'' |
sol'2 sol'4 mib' |
\sugNotes { fa'4 sol' fa' mib' | }
fa' fa' fa' sib' |
sib' sib' sib' la' |
sol' sol' sol' sol' |
do'' do'' do'' re'' |
sib'4. sib'8 sib'4.\trill la'8 |
la'2 fa'4 fa' |
lab'2 lab'4 lab' |
mib'2 mib'4 mib' |
sol'2 sol'4 sol' |
re'2 re'4 re' |
re'2( do')\trill |
fad'4 fad'8 fad' fad'4 sol' |
fad'4 fad'8 fad' sol'4 sol' |
mi'4 mi'8 mi' mi'4 mi' |
la'4. la'8 la'4 sol' |
la'2 la'4 sib'8 do'' |
re''4. do''8 do''4.\trill sib'8 |
sib'2
