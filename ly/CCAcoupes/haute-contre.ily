\clef "haute-contre" r4 |
R2.*3 |
R1. |
R1*5 |
sib2 do'4 mib' |
re' do' re' sib |
fa'2 sib4 re' |
do' sib do' la |
sib2 sol4 sol |
sib2 sib4 re' |
re'2 re'4 si |
do'4 mi' fa'2 |
mi'4 fa' sol' fa'8 mi' |
fa'2. fa'4 |
mib'2. mib'4 |
re'2 mib'4 sol' |
fa' mib' fa' re' |
mib'2 r |
la4 r r2 |
sib4 la do' la |
sib4. la8 la4.\trill sib8 |
sib2. re'4\douxSug |
re'2 sol'4 fa' |
mib'2 mib'4 mib' |
\sugNotes { re' mib' re' do' | }
re' re' re' sol' |
sol' sol' sol' fa' |
mi' mi' mi' mi' |
la' la' fa' fa' |
fa'2 mi'4. fa'8 |
fa'2 do'4 do' |
fa'2 fa'4 fa' |
do'2 do'4 do' |
mib'2 mib'4 mib' |
sib2 sib4 re' |
sib2 sol |
re'4 re'8 re' re'4 re' |
la'4 la'8 la' re'4 sib |
sib2. sib4 |
la4. do'8 do'4 mi' |
fa'2 fa'4 fa' |
fa' sol' fa'4. mib'8 |
re'2
