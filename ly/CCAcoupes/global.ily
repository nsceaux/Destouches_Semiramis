\key la \minor \midiTempo#160
\digitTime\time 3/4 \partial 4 s4 s2.*3
\time 3/2 s1.
\digitTime\time 2/2 s1*3 \bar "|."
\beginMark "Prélude"
\key sib \major \time 2/2 \midiTempo#120 s1*40 s2 \bar "|."
