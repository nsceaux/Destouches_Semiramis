\clef "taille" r4 |
R2.*3 |
R1. |
R1*5 |
sib2 do'4 mib' |
re' do' re' sib |
fa'2 re'4 sib |
do'2 fa |
fa4 sib sib2 |
fa sib |
sib4 do' re'2 |
sol4 do' lab reb' |
sol fa do' do' |
do'2 sib4 re' |
fa2. fa4 |
fa sib sol sol |
sib2 sib4 sib |
sib4 r r2 |
do'4 r r2 |
sol4 la la fa |
fa sol fa4. fa8 |
fa2. sib4\douxSug |
sib2 re'4 sib |
sib2 sib4 sib |
\sugNotes { re' mib' re' do' | }
sib4 sib sib re' |
mi' mi' mi' do' |
do' do' do' mi' |
fa' fa' fa' re' |
re'2 sol' |
do' la4 la |
do'2 do'4 do' |
do'2 lab4 lab |
sib2 sib4 sib |
sib2 sol4 la |
sol1 |
la4 la8 la la4 sib |
la4 re'8 re' sol4 sol |
sol2. sol'4 |
fa'4. fa'8 do'4 do' |
do'2 do'4 do' |
sib sib fa4. fa8 |
fa2
