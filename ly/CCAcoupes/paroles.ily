Pour tant de maux souf -- ferts, quels maux dois- je lui ren -- dre ?
Et com -- ment me van -- ger ? mon art va me l’ap -- pren -- dre.
Ter -- ri -- ble ram -- part des En -- fers
Styx af -- freux, dont les flots en -- vi -- ron -- nent les om -- bres
E -- le -- vez jus -- qu’à moi vos va -- peurs les plus som -- bres.
Que le So -- leil vain -- cu se ca -- che dans les Mers,
Que ce jour manque à l’U -- ni -- vers.
