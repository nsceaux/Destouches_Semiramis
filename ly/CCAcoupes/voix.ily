\clef "vbasse" la4 |
sib8 do' sib4.\trill\prall la8 |
\appoggiatura la8 sib2 sib4 |
sol4.\trill\prall fa8 sol la |
fa2\trill re4 la8 sib do'4 do'8 la |
re'2 r4 sib |
mi2 la4 la8 la |
re2 re |
R1*19 |
r2 r4 sib |
sol2 sol4 sib |
mib2 mib4 mib |
\appoggiatura mib8 \sugNotes { sib,1~ | }
sib,2 sib4 sib |
mi2 mi4 fa |
do2 do'4. do'8 |
\appoggiatura sib8 la2 la4 sib |
\appoggiatura la8 sol2~ sol\trill |
fa r |
r fa4. fa8 |
lab2 lab4. lab8 |
mib2 mib4. mib8 |
sol2 sol4 fa |
\appoggiatura fa8 mib2~ mib\trill |
re1 |
re'4 re'8 re' sib4 sol |
do2. do'4 |
fa4. fa8 fa4 do |
fa2 fa4 sol8 la |
sib2 sib4 sib8 la |
sib2
