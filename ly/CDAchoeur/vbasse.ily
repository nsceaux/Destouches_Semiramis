\clef "vbasse" r4 r8 sib fa4 |
sib2 sib4 sol la2 |
sib sib4 sib mib2 |
sib, sib4 sib sib sol |
do' do' fa fa fa do |
fa fa fa sib, do2 |
fa4. r8 r4 r r8 sib fa4 |
fa2. r4 r8 do re4 |
mib2 mib4 do re2 |
mib mib4 sol2 fa4 |
mib4 mib mib fa sol2 |
do4. r8 r4 r r8 sol la4 |
sib2 sib4 la sol2 |
re re4 sol sol re |
sol sol sol sib sol re |
sol sol sib, do re2 |
sol,4. r8 r4 r r8 re mi4 |
fa2 fa4 re mi2 |
fa fa4 sib sib fa |
sib sib sib sib sib fa |
sol sol re mib fa2 |
sib,4. r8 r4 r r8 do re4 |
\once\override Staff.TimeSignature.stencil = ##f
sib,2.*2/3~ sib, |
R1. R1*4
