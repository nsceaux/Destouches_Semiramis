\clef "taille" r4 r8 sib la4 |
sib2 sib4 sib do'2 |
sib2 sib4 sib sib2 |
sib sib4 sib sib re' |
do' do' do' do' do' do' |
do' la la sib sol4. la8 |
la4. re'8 do'4 sib4. sib8 la4 |
la4. sol8 fa4 sol4. sol8 fa4 |
mib2 sol4 lab fa2 |
mib sol4 sol2 lab4 |
sib sib sib lab sol2 |
sol4. do'8 do'4 sib4. re'8 do'4 |
re'2 re'4 do' sib2 |
fad2 re'4 re' sib re' |
re' sib sol sol sib re' |
re' sib re' do' la4. sib8 |
si4. sol8 sol4 fa4. la8 sol4 |
fa2 la4 sib sol2 |
fa2 la4 sib sib la |
sib sib sib sib sib la |
sib sib sib sol fa2 |
fa4. fa8 fa4 sol4. sol8 fa4 |
fa4. sib8 sib4 sib |
re'4. re'8 do'4. do'8 do'4 do' |
do'2 do'4 do'8 do' |
sib2 sib4 re' |
re' do'8 do' do'4 do' |
re'1 |
