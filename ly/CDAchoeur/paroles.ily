\tag #'choeur {
  L’u -- ni -- vers
  Por -- te nos fers,
  Le Dieux des mers
  Pour nous fait la guer -- re.
  Par nous le Ton -- ner -- re
  Trou -- ble les airs.
  L’u -- ni -   airs.

  A nos voix Trem -- blent les Rois.
  Tou -- te la Ter -- re
  Cede à nos loix.
  Tout mor -- tel nous doit ses vœux.
  En -- trons en par -- ta -- ge
  D’en -- cens & d’hom -- ma -- ge
  A -- vec les Dieux.
  Sur ces bords,
  Tous nos ef -- forts
  Vont van -- ger ta gloi -- re :
  Pre -- voi ta vic -- toi -- re
  Dans nos trans -- ports.
  A nos  - ports.
}
\tag #'(zoroastre basse) {
  Du Dieu du Styx Mi -- nis -- tres in -- fle -- xi -- bles,
  Com -- men -- cez a -- vec moi nos Mys -- te -- res ter -- ri -- bles.
}
