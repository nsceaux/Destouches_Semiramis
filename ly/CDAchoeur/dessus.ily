\clef "dessus2" <>^"Violons" r4 r8 fa' fa'4 |
fa'2 fa'4 sib' fa'2 |
fa' fa'4 fa' sol'2 |
fa' fa'4 fa' fa' sib' |
sol' sol' la' la' la' sol' |
la' fa' fa' sol' mi'2 |
fa'4. sib8 do'4 re'4. fa'8 fa'4 |
fa'4. do'8 re'4 mib'4. mib'8 fa'4 |
sol'2 sol'4 mib' fa'2 |
sol' sol'4 sib'2 lab'4 |
sol'4 sol' sol' fa' re'4.( mi'8) |
mi'4.\trill sol'8 la'4 sib'4. sib'8 la'4 |
sol'2 sol'4 fad' sol'2 |
la' la'4 sib' sol' la' |
sib' sol' sib' sol' sol' la' |
sib' sol' sol' la' fad'2 |
sol'4. re'8 mib'4 fa'4. fa'8 sol'4 |
la'2 la'4 fa' sol'2 |
la' fa'4 fa' fa' fa' |
fa' re' fa' fa' fa' mib' |
re' re' fa' mib' do'2 |
re'4. do'8 re'4 mib'4. mib'8 fa'4 |
\clef "dessus" re'4. re''8 re''4 sib' |
<<
  { sib'4. fa''8 mi''4. mi''8 mi''4 mi'' |
    la''2 la''4 } \\
  \new CueVoice {
    \voiceTwo sib'4._"[manuscrit]" sib'8 sol'4. sol'8 sol'4 sol' |
    la'2 la'4
  }
>> la'8 la' |
re''2 re''4 fa'' |
sib'4 sib'8 sib' sib'4 la' |
sib'1 |
