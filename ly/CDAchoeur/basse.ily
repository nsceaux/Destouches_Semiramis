\clef "basse" r4 r8 sib fa4 |
sib2 sib4 sol la2 |
sib sib4 sib mib2 |
sib, sib4 sib sib sol |
do' do' fa fa fa do |
fa fa fa sib, do2 |
fa,4. sol,8 la,4 sib,4. sib8 fa4 |
fa4. mib8 re4 do4. do8 re4 |
mib2 mib4 do re2 |
mib mib4 sol2 fa4 |
mib4 mib mib fa sol2 |
do4. sib,8 la,4 sol,4. sol8 la4 |
sib2 sib4 la sol2 |
re re4 sol sol re |
sol sol sol sib sol re |
sol sol sib, do re2 |
sol,4. fa,8 mib,4 re,4. re8 mi4 |
fa2 fa4 re mi2 |
fa fa4 sib sib fa |
sib sib sib sib sib fa |
sol sol re mib fa2 |
sib,4. fa8 fa4 do4. do8 re4 |
sib,4. sib8 sib4 re' |
sol4. sol8 do'4. do'8 do'4 do |
fa2 fa4 fa8 fa |
sib2 sib4 re |
mib mib8 sol fa4 fa8 fa |
sib,1 |
