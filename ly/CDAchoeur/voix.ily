\clef "vbasse" r2*3/2 |
R1.*21 |
r4 r8 <>^\markup\character Zoroastre sib sib4 re' |
sol4. sol8 do'4. do'8 do'4 do |
fa2 fa4 fa8 fa |
sib2 sib4 re |
mib4 mib8 sol fa4 fa8 fa |
\appoggiatura fa16 sib,2 sib, |
