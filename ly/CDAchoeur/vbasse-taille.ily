\clef "vbasse-taille" r4 r8 sib8 la4 |
sib2 sib4 sib do'2 |
sib sib4 sib sib2 |
sib sib4 sib sib re' |
do' do' do' do' do' do' |
do' la la sib sol4.( la8) |
la4.\trill r8 r4 r r8 sib la4 |
la2. r4 r8 sol fa4 |
mib2 sol4 lab fa2 |
mib sol4 sol2 lab4 |
sib sib sib lab sol2 |
sol4. r8 r4 r r8 re' do'4 |
re'2 re'4 do' sib2 |
fad2 re'4 re' sib re' |
re' sib sol sol sib re' |
re' sib re' do' la4.( sib8) |
si4. r8 r4 r r8 la sol4 |
fa2 la4 sib sol2 |
fa la4 sib sib la |
sib sib sib sib sib la |
sib sib sib sol fa2 |
fa4. r8 r4 r r8 sol fa4 |
\once\override Staff.TimeSignature.stencil = ##f
fa2.*2/3~ fa |
R1. R1*4
