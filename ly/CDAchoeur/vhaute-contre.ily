\clef "vhaute-contre" r4 r8 fa' fa'4 |
fa'2 fa'4 sib' fa'2 |
fa' fa'4 fa' sol'2 |
fa' fa'4 fa' fa' sib' |
sol' sol' la' la' la' sol' |
la' fa' fa' sol' mi'2 |
fa'4. r8 r4 r r8 fa' fa'4 |
fa'2. r4 r8 mib' fa'4 |
sol'2 sol'4 mib' fa'2 |
sol' sol'4 sib'2 lab'4 |
sol'4 sol' sol' fa' re'4.( mi'8) |
mi'4.\trill r8 r4 r r8 sib' la'4 |
sol'2 sol'4 fad' sol'2 |
la' la'4 sib' sol' la' |
sib' sol' sib' sol' sol' la' |
sib' sol' sol' la' fad'2 |
sol'4. r8 r4 r r8 fa' sol'4 |
la'2 la'4 fa' sol'2 |
la' fa'4 fa' fa' fa' |
fa' re' fa' fa' fa' mib' |
re' re' fa' mib' do'4.( re'8) |
re'4. r8 r4 r r8 mib' fa'4 |
\once\override Staff.TimeSignature.stencil = ##f
re'2.*2/3~ re' |
R1. R1*4
