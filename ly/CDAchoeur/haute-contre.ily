\clef "haute-contre" r4 r8 re' do'4 |
re'2 re'4 mib' mib'2\trill |
re'2 re'4 re' mib'2 |
re' re'4 re' re' sol' |
mi'4 mi' fa' fa' fa' mi' |
fa' do' do' re' do'2 |
do' fa'4 fa'4. re'8 do'4 |
do'2 si4 do'4. do'8 sib4 |
sib2 sib4 do' sib2 |
sib mib'4 mib'2 re'4 |
mib' mib' do' re' si2 |
do'4. mi'8 fad'4 sol'4. sol'8 fad'4 |
sol'2 sol'4 re' re'2 |
re' fad'4 sol' re' fad' |
sol' re' re' re' re' fad' |
sol' re' re' mib' re'2 |
re'4. re'8 do'4 re'4. re'8 do'4 |
do'2 do'4 re' do'2 |
do'2 do'4 re' re' do' |
re' sib re' re' re' do' |
sib sib sib do' la2\trill |
sib4. do'8 do'4 do'4. do'8 sib4 |
sib4. fa'8 fa'4 fa' |
fa'4. fa'8 mi'4. mi'8 mi'4 mi' |
fa'2 fa'4 fa'8 fa' |
fa'2 fa'4 fa' |
sol'4 sol'8 mib' fa'4 fa' |
fa'1 |
