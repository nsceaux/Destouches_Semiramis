\key sib \major \midiTempo#160
\time 6/4 \partial 2. s2.
\bar ".!:" s1.*5 \alternatives s1. s1.
\bar ".!:" s1.*13 \alternatives s1. { \time 2/2 s1 }
\time 3/2 s1.
\time 2/2 s1*4 \bar "|."
