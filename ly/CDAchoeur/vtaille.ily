\clef "vtaille" r4 r8 re' do'4 |
re'2 re'4 mib' mib'2\trill |
re' re'4 re' mib'2 |
re' re'4 re' re' sol' |
mi' mi' fa' fa' fa' mi' |
fa' do' do' re' do'2 |
do'4. r8 r4 r r8 re' do'4 |
do'2. r4 r8 do' sib4 |
sib2 sib4 do' sib2 |
sib mib'4 mib'2 re'4 |
mib' mib' do' re' si2 |
do'4. r8 r4 r r8 sol' fad'4 |
sol'2 sol'4 re' re'2 |
re' fad'4 sol' re' fad' |
sol' re' re' re' re' fad' |
sol' re' re' mib' re'2 |
re'4. r8 r4 r r8 re' do'4 |
do'2 do'4 re' do'2 |
do' do'4 re' re' do' |
re' sib re' re' re' do' |
sib sib sib do' la2\trill |
sib4. r8 r4 r r8 do' sib4 |
\once\override Staff.TimeSignature.stencil = ##f
sib2.*2/3~ sib |
R1. R1*4
