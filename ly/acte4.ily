\newBookPart #'()
\act "ACTE IV"
\sceneDescription\markup\wordwrap-center {
  Le Théâtre représente le Temple de Jupiter Belus,
  orné pour la Consecration \concat { d’ \smallCaps Amestris. }
}
\scene "Scene Premiere" "Scene I"
\sceneDescription\markup\wordwrap-center {
  \smallCaps Arsane, seul.
}
% 4-1
\pieceToc\markup\wordwrap {
  Arsane :
  \italic { Ou suis-je ? quelle horreur agite mes esprits ? }
}
\includeScore "DAAarsane"
\newBookPart #'(full-rehearsal)

\scene "Scene Deuxiéme" "Scene II"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Semiramis, Arsane.
}
% 4-2
\pieceToc\markup\wordwrap {
  Semiramis, Arsane :
  \italic { Arsane, quel spectacle ici vient me surprendre }
}
\includeScore "DBArecit"

\scene "Scene Troisiéme" "Scene III"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Arsane, Amestris, Semiramis.
}
% 4-3
\pieceToc\markup\wordwrap {
  Arsane, Amestris :
  \italic { Ah ! Princesse, faut-il que rien ne vous fléchisse ! }
}
\includeScore "DCArecit"

\scene "Scene Quatriéme" "Scene IV"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Semiramis, Amestris.
}
% 4-4
\pieceToc\markup\wordwrap {
  Semiramis, Amestris :
  \italic { Soutenez Dieux immortels }
}
\includeScore "DDAsemiramisAmestris"
\newBookPart #'(full-rehearsal)

\scene "Scene Cinquiéme" "Scene V"
\sceneDescription\markup\wordwrap-center {
  \smallCaps { Semiramis, Amestris, }
  Prestres & Prestresses de Jupiter, & le Peuple.
}
% 4-5
\pieceToc "[Entrée]"
\includeScore "DEAentree"
\newBookPart #'(full-rehearsal)
% 4-6
\pieceToc\markup\wordwrap {
  Chœur de prétresses : \italic { Fille de l’Innocence }
}
\includeScore "DEBchoeur"
\newBookPart #'(full-rehearsal)
% 4-7
\pieceToc\markup\wordwrap {
  Amestris : \italic { J’immole aux Dieux le printems des mes jours }
}
\includeScore "DECamestris"
% 4-8
\pieceToc\markup\wordwrap {
  Chœur des prestres et prestresses :
  \italic { Digne sang des Rois }
}
\includeScore "DEDchoeur"
% 4-9
\pieceToc\markup\wordwrap {
  Amestris : \italic { Recevez cet Encens, ces Couronnes de fleurs }
}
\includeScore "DEEamestris"
\newBookPart #'(full-rehearsal)
% 4-10
\pieceToc\markup\wordwrap {
  Semiramis, chœur :
  \italic { Triomphez, Dieu puissant, qui regnez sur les Dieux }
}
\includeScore "DEFsemiramis"
\newBookPart #'(full-rehearsal)
\includeScore "DEGchoeur"
\newBookPart #'(full-rehearsal)
% 4-11
\pieceToc "Premier air, pour les Prestresses"
\includeScore "DEHrondeau"
\newBookPart #'(full-rehearsal)
% 4-12
\pieceToc\markup\wordwrap {
  Une Prestresse, chœur : \italic { L’Amour verse des larmes }
}
\includeScore "DEIpretresseChoeur"
\newBookPart #'(full-rehearsal)
% 4-13
\pieceToc "Deuxiéme air, pour les Sacrificateurs"
\includeScore "DEJair"
% 4-14
\pieceToc\markup\wordwrap {
  La Prestresse :
  \italic { Beaux Lieux, soyez toûjours exempts d’allarmes }
}
\includeScore "DEKpretresse"
\newBookPart #'(full-rehearsal)
% 4-15
\pieceToc "Deuxiéme air, pour les Sacrificateurs"
\reIncludeScore "DEJair" "DELair"
% 4-16
\pieceToc "Troisiéme air, pour les Prestresses"
\includeScore "DEMair"
% 4-17
\pieceToc\markup\wordwrap {
  Semiramis, Amestris : \italic { Amestris, achevez ce noble sacrifice }
}
\includeScore "DENsemiramisAmestris"
\newBookPart #'(full-rehearsal)
% 4-18
\pieceToc\markup\wordwrap {
  Chœur : \italic { Quel bruit affreux nous fait trembler }
}
\includeScore "DEOchoeur"

\scene "Scene Sixiéme" "Scene VI"
\sceneDescription\markup\wordwrap-center {
  \smallCaps { L'Oracle, Amestris, Semiramis, }
  & les Acteurs de la Scene précédente.
}
% 4-19
\pieceToc\markup\wordwrap {
  L’Oracle : \italic { Pour apaiser mon couroux légitime }
}
\includeScore "DFAoracle"
% 4-20
\pieceToc\markup\wordwrap {
  Amestris : \italic { J’obéïrai, grands Dieux, je vais vous satisfaire }
}
\includeScore "DFBamestris"
% 4-21
\pieceToc\markup\wordwrap {
  Chœur : \italic { Non, non, non, Dieux cruels, gardez vôtre colere }
}
\includeScore "DFCchoeur"
\newBookPart #'(full-rehearsal)

\scene "Scene Septiéme" "Scene VII"
\sceneDescription\markup\wordwrap-center {
  \smallCaps { Arsane, Amestris, }
  Prestres, Prestresses, & le Peuple.
}
% 4-22
\pieceToc\markup\wordwrap {
  Arsane, Chœur, Amestris :
  \italic { Quoi ! tout me fuit, tout m'abandonne }
}
\includeScore "DGAaaChoeur"
\newBookPart #'(full-rehearsal)

\scene "Scene Huitiéme" "Scene VIII"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Amestris, Arsane.
}
% 4-23
\pieceToc\markup\wordwrap {
  Amestris, Arsane :
  \italic { Je vais subir la loi que le sort me dispense }
}
\includeScore "DHArecit"

\scene "Scene Neuviéme" "Scene IX"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Zoroastre, Arsane.
}
% 4-24
\pieceToc\markup\wordwrap {
  Zoroastre, Arsane :
  \italic { Arsane, où courez-vous }
}
\includeScore "DIArecit"

\scene "Scene Dixiéme" "Scene X"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Zoroastre.
}
% 4-25
\pieceToc\markup\wordwrap {
  Zoroastre : \italic { Va, malheureux Amant, & plus malheureux Fils }
}
\includeScore "DJAzoroastre"
% 4-26
\pieceToc "Entr’acte"
\reIncludeScore "CDBair" "DJBentracte"
