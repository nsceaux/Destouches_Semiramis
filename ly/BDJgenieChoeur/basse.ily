\clef "basse"
\setMusic #'rondeau {
  sol4 sib2 la4 |
  sol2 sol4 do4. re8 mib do |
  re4 re sib sib2 la4 |
  sol2 sib,4 do2 re4 |
  sol,2
}
<<
  \tag #'basse { r4 r2*3/2 | R1.*3 | r4 r }
  \tag #'basse-continue {
    sol4 sib2 la4 |
    sol2 sol4 do4. re8 mib do |
    re2 sib,4 sib,2 la,4 |
    sol,2 sib,4 do re re, |
    sol,2
  }
>>
\keepWithTag #'() \rondeau
<<
  \tag #'basse { r4 r2*3/2 | R1.*4 | r4 r }
  \tag #'basse-continue {
    r4 r r la |
    sib2 do'4 re' la sib |
    fa2 fa4 do2 fa4 |
    sib,2 fa4 sib la sol |
    fa mib re mib fa fa, |
    sib,2
  }
>>
\rondeau
<<
  \tag #'basse { r4 r2*3/2 | R1.*8 | r4 r }
  \tag #'basse-continue {
    r4 r r sol |
    lab2 sol4 mib( re) do |
    sol2 sol,4 do2 re4 |
    mib re mib fa sol sol, |
    do2 do'8 sib la2 sol4 |
    do'2 sib4 la2 sol4 |
    fad2 sib4 la2 sol4 |
    re'2 sib4 la2 sol4 |
    re'4 do' sib la sol2 |
    re
  }
>>
\rondeau
