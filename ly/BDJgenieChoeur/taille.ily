\clef "taille"
\setMusic #'rondeau {
  re'4 re'2 do'4 |
  re'2 sib4 do'2 mib'4 |
  re' re' re' re'2 do'4 |
  re'2 re'4 mib'2 re'4 |
  sib2
}
r4 r2*3/2 | R1.*3 | r4 r
\keepWithTag #'() \rondeau
r4 r2*3/2 | R1.*4 | r4 r
\rondeau
r4 r2*3/2 | R1.*8 | r4 r
\rondeau
