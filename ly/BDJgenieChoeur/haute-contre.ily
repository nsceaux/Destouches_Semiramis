\clef "haute-contre"
\setMusic #'rondeau {
  sib'4 sol'2 la'4 |
  sib'2 sol'4 sol'2 la'4 |
  fad' fad' sol' sol'2 la'4 |
  sib'2 sol'4 sol'2 fad'4 |
  sol'2
}
r4 r2*3/2 | R1.*3 | r4 r
\keepWithTag #'() \rondeau
r4 r2*3/2 | R1.*4 | r4 r 
\rondeau
r4 r2*3/2 | R1.*8 | r4 r
\rondeau
