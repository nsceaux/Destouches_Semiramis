<<
  %% Le Génie
  \tag #'(genie basse genie-hc) {
    \clef "vhaute-contre" <>^\markup\character "Le Genie"
    re'4 mi'2 fad'4 |
    sol'2 re'4 mib'4.( re'8) do'[ sib] |
    la4\trill la re' mi'2 fad'4 |
    sol'2 re'4 sib2\trill\prall la4 |
    sol2 <<
      \tag #'genie-hc { s1 s1.*3 s2 }
      \tag #'(genie basse) { r4 r2*3/2 | R1.*3 | r4 r }
    >> <>^\markup\character "Le Genie"
    la4 sib2 do'4 |
    re'2 mib'4 fa'( mib') re' |
    do'2\trill re'4 mib'2 do'4 |
    re' sib do' re'( mib'16[ re'8.]) do'16[ sib8.] |
    do'4 la sib4. do'8 do'2\trill |
    sib2 <<
      \tag #'genie-hc { s1 s1.*3 s2 }
      \tag #'(genie basse) { r4 r2*3/2 | R1.*3 | r4 r }
    >> <>^\markup\character-text "Le Genie" "à Semiramis"
    re'4 mib'2 re'4 |
    do'2 si4 do'2 mib'4 |
    re' sol re' mib'2 re'4 |
    do' si sol' mib'2\trill re'4 |
    do'2 mi'4 fad'2 sol'4 |
    fad'2 mi'4 fad'2 sol'4 |
    re'2 sol'4 la'2 sib'4 |
    fad' re' sol' la'2 sib'4 |
    fad' re' sol' fad'8.[ sol'16] sol'4.\trill( fad'16[ sol']) |
    la'2 <<
      \tag #'genie-hc { s1 s1.*3 s2 }
      \tag #'(genie basse) { r4 r2*3/2 | R1.*3 | r4 r }
    >>
  }
  %% Dessus
  \tag #'vdessus {
    \setMusic #'rondeau {
      <>^\markup\character Chœur re''4 mi''2 fad''4 |
      sol''2 re''4 mib''4.( re''8) do''[ sib'] |
      la'4\trill la' re'' mi''2 fad''4 |
      sol''2 re''4 sib'2\trill la'4 |
      sol'2
    }
    \clef "vdessus" r4 r2*3/2 | R1.*3 | r4 r
    \keepWithTag #'() \rondeau
    r4 r2*3/2 | R1.*4 | r4 r
    \rondeau
    r4 r2*3/2 | R1.*8 | r4 r
    \rondeau
  }
  %% Haute-contre
  \tag #'(vhaute-contre genie-hc) {
    \setMusic #'rondeau {
      sib'4 sol'2 la'4 |
      sib'2 sol'4 sol'2 la'4 |
      fad' fad' sol' sol'2 la'4 |
      sib'2 sol'4 sol'2 fad'4 |
      sol'2
    }
    \clef "vhaute-contre"
    <<
      \tag #'vhaute-contre { r4 r2*3/2 | R1.*3 | r4 r }
      \tag #'genie-hc { s1 s1.*3 s2 }
    >>
    \keepWithTag #'() \rondeau
    <<
      \tag #'vhaute-contre { r4 r2*3/2 | R1.*4 | r4 r }
      \tag #'genie-hc { s1 s1.*4 s2 }
    >>
    \rondeau
    <<
      \tag #'vhaute-contre { r4 r2*3/2 | R1.*8 | r4 r }
      \tag #'genie-hc { s1 s1.*8 s2 }
    >>
    \rondeau
  }
  %% Taille
  \tag #'vtaille {
    \setMusic #'rondeau {
      re'4 re'2 do'4 |
      re'2 sib4 do'2 mib'4 |
      re' re' re' re'2 do'4 |
      re'2 re'4 mib'2 re'4 |
      sib2
    }
    \clef "vtaille" r4 r2*3/2 | R1.*3 | r4 r
    \keepWithTag #'() \rondeau
    r4 r2*3/2 | R1.*4 | r4 r
    \rondeau
    r4 r2*3/2 | R1.*8 | r4 r
    \rondeau
  }
  %% Basse
  \tag #'vbasse {
    \setMusic #'rondeau {
      sol4 sib2 la4 |
      sol2 sol4 do4.( re8) mib[ do] |
      re4 re sib sib2 la4 |
      sol2 sib,4 do2 re4 |
      sol,2
    }
    \clef "vbasse" r4 r2*3/2 | R1.*3 | r4 r
    \keepWithTag #'() \rondeau
    r4 r2*3/2 | R1.*4 | r4 r
    \rondeau
    r4 r2*3/2 | R1.*8 | r4 r
    \rondeau
  }
>>
