\score {
  \new ChoirStaff <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'genie-hc \includeNotes "voix"
      >> \keepWithTag #'(genie choeur) \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1 s1.*3 s2 \bar "" \pageBreak
        s1 s1.*3 s2 \bar "" \break
        s1 s1.*4\break s1.*4 s2 \bar "" \pageBreak
        s1 s1.*3\break s1.*3 s2. \bar "" \pageBreak
        s2. s1.*2\break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
