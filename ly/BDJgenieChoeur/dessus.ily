\clef "dessus"
\setMusic #'rondeau {
  re''4 mi''2 fad''4 |
  sol''2 re''4 mib''4. re''8 do'' sib' |
  la'4 la' re'' mi''2 fad''4 |
  sol''2 re''4 sib'2\trill la'4 |
  sol'2
}
r4 r2*3/2 | R1.*3 | r4 r
\keepWithTag #'() \rondeau
r4 r2*3/2 | R1.*4 | r4 r
\rondeau
r4 r2*3/2 | R1.*8 | r4 r
\rondeau
