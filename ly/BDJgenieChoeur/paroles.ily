\tag #'(genie basse) {
  Au Dieux d’A -- mour il faut se ren -- dre,
  Lui seul ap -- prend l’art d’être heu -- reux.
}
\tag #'choeur {
  Au Dieux d’A -- mour il faut se ren -- dre,
  Lui seul ap -- prend l’art d’être heu -- reux.
}
\tag #'(genie basse) {
  Pour -- quoy se plain -- dre de ses feux,
  Que sert d’at -- ten -- dre,
  Craint- on __ de pren -- dre
  De si beaux nœuds ?
}
\tag #'choeur {
  Au Dieux d’A -- mour il faut se ren -- dre,
  Lui seul ap -- prend l’art d’être heu -- reux.
}
\tag #'(genie basse) {
  Ne per -- dez pas des jours ai -- ma -- bles,
  Mais moins du -- ra -- bles,
  Que les Ze -- phirs.
  Les soins ja -- loux & les sou -- pirs
  Sont- ils sans char -- mes ?
  Non, jus -- qu’aux lar -- mes,
  Tout est plai -- sirs.
}
\tag #'choeur {
  Au Dieux d’A -- mour il faut se ren -- dre,
  Lui seul ap -- prend l’art d’être heu -- reux.
}
