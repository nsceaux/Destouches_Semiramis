\clef "vbasse" R2. |
sol4 sol8 la sib sol |
do'2 do'4 |
la4. sib8 do' la |
re'4 re' re'8 do' |
sib8[ do' re' do' sib la]( |
sol4) sol sol |
fa2 fa4 |
fa8 sol mib4.\trill re8 |
re2 re'4 |
sol8[ fa sol la sib sol]( |
do'4) do' do' |
la la fa8 fa |
sib4. sol8 la4 |
sib4 fa4. fa8 |
sib,2 sib8 sib |
sib4 sib sib |
sib2 sib8 la |
sol4 sol sol |
la2.~ |
la~ |
la |
fa4. fa8 mi re |
sib4 sib sol |
la4( la,2) |
re2 r4 |
R2. |
r4 r re' |
do'8[ sib la sol fad mi]( |
fad4) re4 re' |
sib sol re8 re |
sol4. sol8 la4 |
sib la4.\trill sol8 |
re2 sib8 sib |
sib4 sib sib |
do'2 do'8 sib |
la4 la sol |
re'2.~ |
re'~ |
re' |
sib4. do'8 re' sib |
mib'4 mib' do' |
re'4( re2) |
sol2. |
R2.*17 |
fa4 fa8 sol la fa |
sib2 sib4 |
sol4. la8 sib sol |
do'4 do' do'8 sib |
la[ sib do' sib la sol]( |
fa4) fa fa |
sib2 sib8 la |
sol8[ la sib la sol fa]( |
mib4) mib re |
do2 fa4 |
fa8 mib fa4 fa |
sib,2 sib4 |
la8[ sol fa sol la fa]( |
sib4) sib sol8 sol |
do'4. sib8 la4 |
sib4 do'4. do'8 |
fa2 r4 |
R2. |
r4 r re' |
dod'8[ si la sol fa mi]( |
fa4) re re |
la4 la la8 la |
re'4. la8 sib4 |
sol la4. la8 |
re2 r4 |
R2. |
r4 r sol8 sol |
sol4 sol sol |
sol sol fa |
mib re do |
sol2. |
do'4. do'8 do' sol |
lab4 lab fa |
sol2. |
do2 do'8 do' |
do'4 do' do' |
do'2 la8 la |
re'4 re' re' |
re'2.~ |
re' |
do'4. do'8 do' re' |
sib4 sib sib |
la8[ sol la sib la sib]( |
sol2) sol4 |
fa4. fa8 fa sol |
mib4 mib mib |
re8[ do re mib re mib]( |
do2) do4 |
do'4. sib8 la sol |
mib'4 mib' do' |
re'4( re2) |
sol2. |
