\clef "vtaille" R2. |
re'4 re'8 re' re' re' |
do'2 mi'4 |
mi'4. mi'8 mi' do' |
re'4 re' re'8 re' |
re'[ mib' fa' mib' re' do']( |
re'4) re' sib |
do' r re' |
re'8 re' re'4 do' |
re'2 re'4 |
re' re' re' |
do' do' do' |
do' do' do'8 do' |
sib4. sib8 do'4 |
sib la4.\trill sib8 |
sib2 sib8 sib |
sib4 re' sib |
sib2 re'8 re' |
mi'4 mi' mi' |
dod'2 dod'8 re' |
dod'4 dod' re' |
la2. |
la4. re'8 dod' re' |
re'4 re' re' |
re'4( dod'2) |
re'2 r4 |
R2. |
r4 r re' |
re' re' re' |
re'8[ do' re' mi' fad' re']( |
sol'4) sol' fad'8 fad' |
re'4. re'8 do'4 |
sib8.[ do'16] do'4.\trill re'8 |
re'2 re'8 re' |
re'4 re' re' |
do'2 do'8 do' |
do'4 do' re' |
re' re' re' |
sol' sol' sol' |
re'2. |
sol'4. sol'8 re' sol' |
mib'4 mib' mib' |
re'2( la4) |
sib2. |
R2.*17 |
la4 la8 sib do' la |
sib2 sib4 |
re'4. re'8 re' sib |
do'4 do' do'8 do' |
do'[ re' mib' re' do' sib]( |
la4) la la |
sib2 sib8 sib |
sib4 sib sib8 sib |
sib4 sib sib |
sib2 la4 |
la8 sib sib4 la |
sib2 sib4 |
do' do' do' |
sib sib re'8 re' |
do'4. do'8 do'4 |
re' do'4. do'8 |
la2 r4 |
R2. |
r4 r re' |
mi' mi' mi' |
re'8[ dod' re' mi' fa' sol']( |
mi'4) dod' dod'8 dod' |
re'4. dod'8 re'4 |
re'4 dod'4. re'8 |
re'2 re'8 re' |
re'4 re' re' |
sol'2.~ |
sol'~ |
sol'4 sol sol |
do'4 si do' |
re'2. |
do'4. do'8 do' si |
do'4 do' do' |
do'4( si2) |
do'2 do'8 do' |
do'4 do' fa' |
do'2 dod'8 dod' |
re'4 re' sol' |
re' re' re' |
sol' sol' sol' |
re'2 re'8 re' |
re'4 re' re' |
mib' do' do' |
re' sib sib |
do' la8 la la la |
sib4 sol8 sol sol do' |
do'4 si si |
do'2 do'4 |
do'4. do'8 do'8 re' |
mib'4 mib' mib' |
re'2( la4) |
sib2. |
