Nous re -- ce -- vons un Roi des mains de la Vic -- toi -- re,
Qu’il ré -- pan -- de sur nous mil -- le nou -- veaux bien -- faits,
Qu’il re -- gne, \tag #'vtaille { Qu’il re -- gne, }
Qu’il re -- gne, qu’il nous donne une é -- ter -- nel -- le paix ;
Que les Dieux im -- mor -- tels
\tag #'vbasse { ne sé -- pa -- rent ja -- mais __ }
\tag #'(vdessus vhaute-contre vtaille) {
  Que les Dieux im -- mor -- tels ne sé -- pa -- rent ja -- mais
}
Et nô -- tre bon -- heur & sa gloi -- re.
\tag #'vdessus { Qu’il re -- gne, }
Qu’il re -- gne,
Qu’il re -- gne, qu’il nous donne une é -- ter -- nel -- le paix ;
Que les Dieux im -- mor -- tels
\tag #'vbasse { ne sé -- pa -- rent ja -- mais __ }
\tag #'(vdessus vhaute-contre vtaille) {
  Que les Dieux im -- mor -- tels ne sé -- pa -- rent ja -- mais
}
Et nô -- tre bon -- heur & sa gloi -- re.

Nous re -- ce -- vons un Roi des mains de la Vic -- toi -- re,
Qu’il ré -- pan -- de sur nous
\tag #'vtaille { Qu’il ré -- pan -- de, }
Qu’il ré -- pan -- de sur nous
\tag #'(vhaute-contre) { Qu’il ré -- pan -- de sur nous }
mil -- le nou -- veaux bien -- faits,
\tag #'(vdessus vhaute-contre vtaille) { Qu’il re -- gne, }
Qu’il re -- gne, qu’il nous donne une é -- ter -- nel -- le paix ;
\tag #'(vdessus vhaute-contre) { Qu’il re -- gne, }
Qu’il re -- gne,
Qu’il re -- gne, qu’il nous donne une é -- ter -- nel -- le paix ;
\tag #'(vdessus vhaute-contre) { Que les Dieux im -- mor -- tels, }
\tag #'(vdessus vhaute-contre vbasse) { Que les Dieux im -- mor -- tels }
\tag #'(vtaille) { Que les Dieux im -- mor -- tels __ }
ne sé -- pa -- rent ja -- mais
Et nô -- tre bon -- heur & sa gloi -- re.
\tag #'(vdessus vhaute-contre vtaille) {
  Que les Dieux im -- mor -- tels,
  Que les Dieux im -- mor -- tels ne sé -- pa -- rent ja -- mais
}
\tag #'vbasse {
  Que les Dieux im -- mor -- tels ne sé -- pa -- rent ja -- mais __
}
\tag #'(vhaute-contre vtaille) {
  Que les Dieux im -- mor -- tels ne sé -- pa -- rent ja -- mais
  Et nô -- tre bon -- heur,
}
\tag #'(vdessus vbasse) {
  Et nô -- tre bon -- heur & sa gloi -- re,
}
Et nô -- tre bon -- heur & sa gloi -- re.
Et nô -- tre bon -- heur & sa gloi -- re.
