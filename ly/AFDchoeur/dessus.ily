\clef "dessus" re''4 re''8 mi'' fad'' re'' |
sol''2 sol''4 |
mi''4. fa''8 sol'' mi'' |
la''4 la'' la''8 sol'' |
fad'' sol'' la'' sol'' fad'' mi'' |
re''4 re'' re'' |
sib'2 mib''4 |
mib''8 fa'' re''4. re''8 |
re''2 la''4 |
fad''8 mi'' fad'' sol'' la'' fad'' |
sol''4 sol'' sol'' |
mi''8 re'' mi'' fa'' sol'' mi'' |
fa''4 fa'' fa''8 mib'' |
re''4. re''8 mib''4 |
re'' do''4.\trill sib'8 |
sib'2 re''8 re'' |
re''4 re'' mib'' |
re''2 re''8 re'' |
sol''4 sol'' sol'' |
sol''2 sol''8 fa'' |
mi'' fa'' sol'' fa'' mi'' re'' |
dod'' sib' la' sol' fa' mi' |
re'4. re''8 mi'' fa'' |
re''4 mi''4. fa''8 |
fa''4( mi''2)\trill |
re''2 sol''4 |
fad''8 mi'' re'' do'' sib' la' |
sib'4 sol' sol'' |
fad'' fad'' re'' |
re''8 mi'' fad'' sol'' la'' fad'' |
sol''4 re'' re''8 re'' |
sib'4. sib'8 do''4 |
re'' do''4.\trill sib'8 |
la'2 re''8 re'' |
re''4 re'' re'' |
mi''2 mi''8 mi'' |
fad''4 fad'' sol'' |
fad'' fad'' fad'' |
sol'' sol'' fa''16 mi''8 fa''16 |
fad''2. |
re''4. mi''8 fad'' re'' |
sol''4 sol'' sol'' |
sol''4( fad''2) |
sol''2. |
sib'4 sib'8 do'' re'' mib'' |
fa''4. mib''8 fa'' re'' |
sol''4 sol''8 sib'' la'' sol'' |
fad'' mi'' re'' do'' sib' la' |
sib'4 sib'8 do'' re'' mib'' |
fa''4. mib''8 fa'' re'' |
sol''4 sol''8 sib'' la'' sol'' |
fad'' mi'' re'' do'' sib' la' |
sib'2 sib''8 la'' |
sol'' la'' sib'' la'' sol'' fa'' |
mib''4 mib'' mib'' |
do''2 fa''8 mib'' |
re'' mib'' fa'' mib'' re'' do'' |
sib'4 sib' sib' |
sol'8 la' sib' do'' re'' sib' |
mib'' re'' do''4.\trill sib'8 |
sib'4 sib'8 do'' re'' sib' |
fa''2 fa''4 |
re''4. mib''8 fa'' re'' |
sol''4 sol'' sol''8 fa'' |
mi'' fa'' sol'' fa'' mi'' re'' |
do''4 do'' do'' |
fa''2 fa''8 mib'' |
re'' mib'' fa'' mib'' re'' do'' |
sib'4 sib' sib' |
sol'2\trill sib'4 |
do''8 re'' mib''4 re'' |
do''2 fa''4 |
re''8 do'' sib' do'' re'' mib'' |
do''4 la' fa'' |
re'' re'' re''8 sol'' |
mi''4. mi''8 fa''4 |
fa'' mi''4.\trill fa''8 |
fa''2 la''4 |
sol''8 fa'' mi'' re'' dod'' si' |
dod''4 la' fa'' |
mi'' dod'' dod'' |
re''8 mi'' fa'' mi'' re'' mi'' |
dod''4 la' mi''8 la'' |
fa''4.\trill mi''8 re''4 |
sol''4 mi''4.\trill re''8 |
re''2 fad''8 fad'' |
fad''4 fad'' fad'' |
sol''2 si'8 si' |
si'4 si' do'' |
si' si' si' |
do'' re'' mib'' |
si'8 do'' re'' mib'' fa'' re'' |
mib''4. fa''8 mib'' re'' |
do''4 re''4. mib''8 |
mib''4( re''2)\trill |
do''2 mi''!8 mi'' |
mi''4 mi'' fa'' |
mi''2 mi''8 mi'' |
fad''4 fad'' sol'' |
fad''4 fad'' fad'' |
sol'' sol'' fa''16[ mi''8 fa''16] |
fad''2.\trill |
re''4. re''8 re'' mib'' |
do''4 do'' re'' |
sib'8 la' sib' re'' do'' sib' |
la'2 la'4 |
sol''4. sol''8 sol'' sol'' |
fa''4 \appoggiatura sol''8 fa''4 \appoggiatura mib''8 re''4 |
mib''4. re''8 do''4 |
mi''!4. mi''8 fad'' sol'' |
sol''4 sol'' sol'' |
sol''4( fad''2) |
sol''2. |
