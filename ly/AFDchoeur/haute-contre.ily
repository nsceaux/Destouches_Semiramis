\clef "haute-contre" fad'4 fad'8 sol' la' fad' |
sib'2 sib'4 |
sol'4. la'8 sib' sol' |
do''4 do'' do''8 sib' |
la' sib' do'' sib' la' sib' |
sol'4 sol' sol' |
sol'2 sol'4 |
la'8 la' la'4. la'8 |
la'2 do''4 |
la'8 sol' la' sib' do'' la' |
sib'4 sib' sib' |
sol'8 fa' sol' la' sib' sol' |
la'4 la' la'8 la' |
fa'4. fa'8 fa'4 |
fa' fa'4. fa'8 |
re'2\trill fa'8 fa' |
fa'4 fa' sol' |
fa'2 fa'8 fa' |
sib'4 sib' sib' |
mi'2 mi'8 fa' |
sol' la' sib' la' sol' fa' |
mi'2. |
fa'4. fa'8 sol' la' |
fa'4 sol' sib' |
la'2. |
fad'2 r4 |
R2. |
r4 r sib' |
la'8 sib' do'' sib' la' sol' |
la'4 la' la' |
sib' sib' la'8 la' |
sol'4. sol'8 fad'4 |
sol' fad'4. sol'8 |
fad'2 sol'8 sol' |
sol'4 sol' sol' |
sol'2 sol'8 sol' |
la'4 la' sib' |
la' la' la' |
sib' sib' la'16 sol'8 la'16 |
la'2.\trill |
sib'4. sib'8 la' sib' |
sol'4 la'4. sib'8 |
sib'4( la'2)\trill |
sol'2. |
sol'4 sol'8 la' sib' do'' |
re''2 re''4 |
sib'4. re''8 do'' sib' |
la'4 la' fad' |
sol' sol'8 la' sib' do'' |
re''2 re''4 |
sib'4 sib'8 re'' do'' sib' |
la'4 la' fad' |
sol'2 r4 |
r r sib' |
do'' do'' do'' |
fa'2 la'4 |
sib'2 fa'4 |
sol'2 sol'4 |
sol'4. la'8 sib'4 |
sib' la'4.\trill sib'8 |
sib'4 re'8 mib' fa' re' |
do'2 fa'4 |
fa'4. fa'8 fa' fa' |
sib'4 sib' sib'8 la' |
sol' la' sib' la' sol' mi' |
fa'4 fa' fa' |
do'2 fa'8 fa' |
fa'4 fa' fa' |
sol'2 re'8 re' |
mib'4 mib' fa' |
sol'2 fa'4 |
fa'8 sol' fa'4 fa' |
fa'2 fa'4 |
fa'4. fa'8 fa'4 |
fa' fa' sib'8 sib' |
sol'4. sol'8 la'4 |
sol'16 fa'8 sol'16 sol'4.\trill fa'8 |
fa'2 r4 |
R2. |
r4 r la' |
sol' sol' sol' |
fa' fa' la' |
la' la' la'8 la' |
la'4. sol'8 fa'4 |
sib'4 la'4. la'8 |
fad'2 la'8 la' |
la'4 la' la' |
si'2 re'8 re' |
re'4 re' mib' |
re' re' re' |
mib' fa' sol' |
sol'2. |
sol'4. lab'8 sol' fa' |
mib'4 mib' lab' |
sol'2. |
mi'2 sol'8 sol' |
sol'4 sol' la' |
sol'2 la'8 la' |
la'4 la' sib' |
la' la' la' |
sib' sib' la'16 sol'8 la'16 |
la'2\trill la'8 la' |
sol'4 sol' sol' |
sol' fa' fa' |
fa' mib' mib' |
mib' re'8 re' re' re' |
re'4 do'8 do' do' do' |
re'4 re' fa' |
sol'2 mib'4 |
sol'4. sol'8 la'8 sib' |
sol'4 la'4. sib'8 |
sib'4( la'2)\trill |
sol'2. |
