\clef "basse" R2.*3 |
sol8 fa sol la sol fa |
mi fa sol la sol fa |
mi re do re mi do |
fa2.~ |
fa~ |
fa4. mi8 re do |
sol4 re8^"égales" mi re do |
si, sol, do re mi do |
fa,2 fa4 |
mi8 fa sol4 sol, |
do2. |
