\clef "dessus" mi''8 re'' mi'' fa'' mi'' re'' |
do'' re'' mi'' fa'' mi'' re'' |
do'' si' la' si' do'' si'16 la' |
si'4 do''8.( re''16) re''8.(\trill do''32 re'') |
mi''2.~ |
mi'' |
do''8 sib' do'' re'' do'' sib' |
la' sib' do'' re'' do'' sib' |
la'4. la'8 si' do'' |
si'4 \appoggiatura { si'16[ dod''] } re''2~ |
re''4 do''2~ |
do''4 si'8 do'' re'' si' |
mi'' re''16 do'' si'4.\trill do''8 |
do''2. |
