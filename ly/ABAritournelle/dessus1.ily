\clef "dessus" sol''8 fa'' sol'' la'' sol'' fa'' |
mi'' fa'' sol'' la'' sol'' fa'' |
mi'' re'' do'' re'' mi'' re''16 do'' |
re''4 mi''8.( fa''16) fa''8.(\trill mi''32 fa'') |
sol''2.~ |
sol'' |
la''8 sol'' la'' sib'' la'' sol'' |
fa'' sol'' la'' sib'' la'' sol'' |
fa'' sol'' la'' sol'' fa'' mi'' |
re''4 \appoggiatura { re''16[ mi''] } fa''2~ |
fa''4 mi''2~ |
mi''4 re''8 mi'' fa'' re'' |
sol'' la'' re''4.\trill do''8 |
do''2. |
