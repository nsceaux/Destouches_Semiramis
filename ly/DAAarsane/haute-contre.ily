\clef "haute-contre" re''4 re'' re'' dod''8\trill si' |
do''!4 do'' do'' si'8\trill la' |
si'4 si' si' dod''8 re'' |
la'2 la'4 la' |
dod''4 dod'' dod'' re''8 mi'' |
re''4 re'' re'' dod''8 re'' |
mi''4 mi'' mi'' re''8 dod'' |
re''2 re''4 re'' |
re'' do'' do'' do'' |
do'' si' si' si' |
si' la' la' la' |
la' sol' sol' sol' |
sol' fad' mi' sold' |
la'2 la'4 la' |
dod'' dod'' la' la' |
la' la' la' sol'8 fad' |
sol'4 sol' sol' la' |
sol' si' mi'' mi'' |
mi'' dod'' dod'' re'' |
si' si' si' si' |
la' si'8 dod'' dod''4.\trill re''8 |
re''2 re''\douxSug~ |
re''4 do'' do'' do'' |
do'' si' si' si' |
si' la' la' la' |
sol' si' si' si' |
si' fad' sol' la' |
si'2 la'4 la' |
la'2 mi'4 mi' |
fad'2 fad'4 sol' |
la'2 la'4 la' |
mi' mi' mi' mi' |
mi'2 la'4 la' |
la' la' la' la' |
la' re'' re'' re'' |
si' la' si' si' |
si' mi'' la' la' |
la'2 r |
la'8\fortSug la' re'' re'' re'' si' re'' mi'' |
dod'' dod'' mi'' dod'' re'' la' la' la' |
si' sol' si' re'' mi'' mi'' si' si' |
la' la' dod'' mi'' mi''16 fad'' mi'' re'' dod''8. re''16 |
re''4 r re''\douxSug r |
re''4. la'8 la'2 |
la' si'4 si' |
si'2 mi''4 si' |
la'2 la'4 la' |
si' si' si' si' |
si'2 la'4. la'8 |
si'2 si'4. si'8 |
la'2 la'4 la' |
si'2 si'4. si'8 |
la'2 la'4 fad' |
sol' sol' sol' mi' |
la' sol'8 re'' la'4. sol'8 |
fad'2 r |
la'8\fortSug la' re'' re'' re'' si' re'' mi'' |
dod'' dod'' mi'' dod'' re'' la' la' la' |
si' sol' si' re'' mi'' mi'' si' si' |
la' la' dod'' mi'' mi''16 fad'' mi'' re'' dod''8.\trill re''16 |
re''1 |
