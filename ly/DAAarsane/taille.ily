\clef "taille" re''4 re'' re'' dod''8\trill si' |
do''4 do'' do'' si'8 la' |
si'4 si' si' dod''8 re'' |
mi'4 mi' mi' la |
la la la si8 dod' |
re'4 re' re' mi'8 fad' |
si4 si dod' re'8 mi' |
re'4 fad' fad' sol' |
mi' mi' mi' fad' |
re' re' re' mi' |
dod' dod' dod' re' |
si si si dod' |
la re' re' si |
la mi' mi' mi' |
la' la' mi' mi' |
re' fad' fad' fad' |
fad' mi' mi' re' |
re'2 si'4 si' |
dod'' la' la' la' |
la' sol' sol' sol' |
sol' fad' mi' la' |
la'2 sol'\douxSug |
sol'4 sol' la' la' |
fad' re' re' re' |
mi' do' do' la |
si mi' sol' sol' |
fad' fad' si' la' |
sol'2 mi'4 dod' |
mi'2 mi'4 dod' |
re'2 re'4 sol' |
fad' re' re' re' |
si si si si |
dod'2 mi'4 fad' |
mi' mi' dod' mi' |
re'2 re'4 fad' |
fad' fad' re' sol' |
sol' sol' sol' fad' |
mi'2 r |
re'8\fortSug re' re' fad' si' si' si' si' |
la' la' la' la' la' fad' fad' fad' |
sol' sol' sol' la' sol' sol' mi' mi' |
mi' mi' la' la' la' si' la' sol' |
fad'4 r la'\douxSug r |
la' sol'8 fad' mi'2 |
re' fad'4 fad' |
sol' re' mi'4. mi'8 |
mi'2 fad'4 fad' |
fad' fad' fad' fad' |
mi'2 mi'4. re'8 |
fad'2 mi'4. mi'8 |
mi'2 re'4 re' |
re'2 mi'4. mi'8 |
mi'2 fad'4 si |
si mi' mi' mi' |
re' si mi'4. la8 |
la2 r |
re'8\fortSug re' re' fad' si' si' si' si' |
la' la' la' la' la' fad' fad' fad' |
sol' sol' sol' la' sol' sol' mi' mi' |
mi' mi' la' la' la' si' la' sol' |
fad'1 |
