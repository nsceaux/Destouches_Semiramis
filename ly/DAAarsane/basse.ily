\clef "basse" R1*3 |
la4^"Basse de violons" la la sol8 fad |
sol4 sol sol fad8 mi |
fad4 fad fad mi8 re |
mi4 mi la,2 |
<>^"Tous" re4 re re dod8 si, |
do4 do do si,8 la, |
si,4 si, si, la,8 sol, |
la,4 la, la, sol,8 fad, |
sol,4 sol, sol, fad,8 mi, |
fad,4 fad, sold, mi, |
la la la sol8 fad |
sol4 sol sol fad8 mi |
fad4 fad fad mi8 re |
mi4 mi mi fad |
sol2 mi |
la,4 la, la, sol,8 fad, |
sol,4 sol, sol, fad,8 mi, fad,4 sol, la,2 |
<< { re4^"Basses de violons" re } \\ re,2 >> re4 dod8 si, |
do4 do do si,8 la, |
si,4 si, si, do8 si, |
la,4 la, la, la, |
mi mi mi8 fad sol la |
si4 la sol fad |
mi8 fad sol mi la4 la |
dod si, la, sol, |
fad, sol, fad, mi, |
re, re,8 mi, fad,4 re, |
mi, mi, mi, mi, |
la la la sol8 fad |
sol4 sol8 fad mi4 sol |
fad4 fad8 sol la4 fad |
si fad sol sol8 fad |
mi4 mi la re |
la,2 r |
re'16^"Tous" \fortSug dod' re' mi' re' mi' re' do' si8 sol si sol |
la16 sol la si la si la sol fad8 re re re |
sol sol sol fad mi mi mi mi |
la la la sol fad sol la la, |
re1^"Basses de violons" ~ |
re2 dod |
re16 dod re mi re mi re dod si,2 |
sol16 fad sol la sol la sol fad mi2 |
la16 sol la si la si la sol fad8 mi re dod |
si,4 si,8 dod red4 si, |
mi mi8 re dod4 re |
si,2 mi |
la16 sol la si la sol fad mi re2 |
sol16 fad sol la sol la sol fad mi2 |
la16 sol la si la si la sol fad4 si |
mi sol8 fad mi4 la |
re sol, la,2 |
re, r2 |
re'16_"Tous" ^"Marquez" dod' re' mi' re' mi' re' do' si8 sol si sol |
la16 sol la si la si la sol fad8 re re re |
sol sol sol fad mi mi mi mi |
la la la sol fad sol la la, |
<<
  \tag #'basse re1
  \tag #'basse-continue { \custosNote re4 \stopStaff }
>>
