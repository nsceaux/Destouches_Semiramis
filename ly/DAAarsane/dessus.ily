\clef "dessus" fad''4 fad'' fad'' mi''8\trill re'' |
mi''4 mi'' mi'' re''8\trill dod'' |
re''4 re'' re'' mi''8 fad'' |
dod''2 dod''8.( re''16) re''8.(\trill dod''32 re'') |
mi''1 |
la''4 la'' la'' sol''8\trill fad'' |
sol''4 sol'' sol'' la''8 sol'' |
fad''2 si''~ |
si''4 do'''8( si'') la''2~ |
la''4 sol''8(\trill fad'') sol''2~ |
sol'' fad''4 fad'' |
fad'' mi''8 re'' mi''4 mi'' |
mi'' re''8 dod'' si'4 dod''8 re'' |
dod''4.\trill si'8 la'2 |
mi''4 mi'' mi'' re''8 dod'' |
re''2 re''4 re'' |
re'' dod''8 si' do''4 re''8 do'' |
si'2 sol''4 sol'' |
sol'' fad''8 mi'' fad''4 fad'' |
fad''( mi''8) re'' mi''2~ |
mi''4 re''8( mi'') mi''4.\trill re''8 |
re''2 si''\doux~ |
si''4 la''8 sol'' la''4 la'' |
la'' sol''8 fad'' sol''4 sol'' |
sol'' fad''8 mi'' fad''4 fad'' |
sol'' sol''8 la'' si''4 si'' |
fad'' red''8 red'' mi''4 red'' |
mi''2 mi''4 mi'' |
mi''2 dod''4 dod''8 mi'' |
la'2 la'4 dod'' |
re'' re'' re'' si' |
sold' sold' sold' sold' |
la'2 dod''4 dod'' |
dod'' dod''8 re'' mi''4 dod'' |
re'' re''8 mi'' fad''4 la'' |
re''4 re''8 re'' re''4 re'' |
mi'' mi''8 mi'' mi''4 fad'' |
dod''2 la''16\fort sol'' la'' si'' la'' si'' la'' sol'' |
fad''8 re'' fad'' re'' sol''16 fad'' sol'' la'' sol'' la'' sol'' fad'' |
mi''8 mi'' sol'' mi'' la''16 si'' la'' sol'' fad'' mi'' re'' dod'' |
si' la' si' dod'' re'' mi'' fad'' re'' sol'' la'' si'' la'' sol'' fad'' mi'' re'' |
dod'' si' dod'' re'' mi'' fad'' sol'' mi'' la''8 sol''16 fad'' mi''8.\trill re''16 |
re''4 r fad''\doux r |
fad'' sol''8 la'' mi''4 la'' |
la'' la' re'' fad'' |
re''2 si''4. sol''8 mi''4. dod''8 re''4 fad'' |
fad'' fad''8 fad'' fad''4 red'' |
mi''2 la''4. la''8 |
la''2 la''4 sold'' |
la''2 la''4 fad'' |
re''2 si''4. sol''8 |
mi''2 mi''4 red'' |
mi'' dod''8 re'' mi''4 dod'' |
re'' mi''8 fad'' mi''4.\trill re''8 |
re''2 la''16\fort sol'' la'' si'' la'' si'' la'' sol'' |
fad''8 re'' fad'' re'' sol''16 fad'' sol'' la'' sol'' la'' sol'' fad'' |
mi''8 mi'' sol'' mi'' la''16 si'' la'' sol'' fad'' mi'' re'' dod'' |
si' la' si' dod'' re'' mi'' fad'' re'' sol'' la'' si'' la'' sol'' fad'' mi'' re'' |
dod'' si' dod'' re'' mi'' fad'' sol'' mi'' la''8 sol''16 fad'' mi''8.\trill re''16 |
re''1 |
