\beginMark Prélude
\tempo "Lentement" \midiTempo#120
\key re \major
\time 2/2 s1*37 s2
\tempo "Tres vîte" s2 s1*17 s2
\tempo "Vîte" s2 s1*5 \bar "|."
