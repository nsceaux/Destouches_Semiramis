\clef "vhaute-contre" R1*21 |
r2 r4 <>^\markup\character Arsane sol'4 |
mi'2 mi'8 r la' la' |
re'2. mi'4 |
\appoggiatura re'16 do'4. do'8 do'4 re' |
si2 sol'4 sol' |
red' si8 si mi'4 fad' |
sol'2 dod'4 dod'8 mi' |
\appoggiatura mi'8 la2 la4 la |
re' re' re' mi' |
\appoggiatura mi'8 fad'1 |
si4 si8 si mi'4 mi' |
dod'2. r8 la |
mi'4 mi'8 fad' sol'4 sol'8 mi' |
la'2 la' |
r4 re'8 do' si4 si8 re' |
sol4 sol'8 mi' dod'4 dod'8 re' |
\appoggiatura re' mi'2 r |
R1*4 |
r4 r8 la^"mesuré" re' re' re' fad' |
re'4 mi'8 fad' sol'4 fad'8 mi' |
fad'4\trill fad' fad' re' |
si\trill si sol'4. mi'8 |
dod'4. mi'8 la'4. la8 |
re'4 re'8 dod' si4\trill si8 la |
sold2 mi'4. fad'8 |
re'4\trill dod'8 si dod'4. re'8 |
dod'4\trill dod' fad' re' |
si\trill si sol'4. mi'8 |
dod'2 la' |
sol'8 fad' mi' re' sol'2 |
fad'4 mi'8 re' dod'4.\trill re'8 |
re'2 r2 |
R1*5 |
