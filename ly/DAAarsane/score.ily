\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*7\break s1*8\pageBreak
        s1*8\break s1*5\pageBreak \grace s8 s1*5\break s1*4\pageBreak
        \grace s8 s1*4\break s1*3\pageBreak s1*4\break s1*4\pageBreak
        s1*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
