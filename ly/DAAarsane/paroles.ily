Ou suis- je ? quelle hor -- reur a -- gi -- te mes es -- prits ?
C’est i -- ci que le Dieu du Ciel & de la Ter -- re
Pré -- sen -- te sa splen -- deur à nos re -- gards sur -- pris.
Y viens- je bra -- ver son Ton -- ner -- re,
Et du sein des Au -- tels ar -- ra -- cher A -- mes -- tris !
Qu’im -- por -- te que la foudre à mes yeux é -- tin -- cel -- le !
Dieu bar -- ba -- re, Dieu ja -- loux,
Frap -- pez : je vais au de -- vant de vos coups,
Trop heu -- reux de pe -- rir pour el -- le,
Dieu bar -- ba -- re, Dieu ja -- loux,
Non, tant que je res -- pire, el -- le n’est point à vous.
