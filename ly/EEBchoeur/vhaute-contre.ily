\clef "vhaute-contre" R4.*9 |
sol'8 sol' sol' |
la'4 la'8 |
sol'4 sol'8 |
la'4. |
la'8 la' la' |
sol'4 sol'8 |
R4.*7 |
la'8 la' la' |
sib'4 sib'8 |
la'4 la'8 |
sib'4. |
la'8 la' la' |
si'!4 si'8 |
