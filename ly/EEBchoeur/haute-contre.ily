\clef "haute-contre" do''4 do''8 |
re''4 re''8 |
do''4 do''8 |
do''4 la'8 |
la'4 sol'8 |
sol'4 si'8 |
do''4 do''8 |
do''4 do''8 |
do''8 si'8.\trill do''16 |
do''8 sol' sol' |
la'4 la'8 |
sol'4 sol'8 |
la'4. |
la'8 la' la' |
sol'4 sol'8 |
R4. |
sol'8 sol' sol' |
la'16 sib' do'' sib' la' sol' |
fa'4. |
sol'8 sol' sib' |
la'4 dod''8 |
re'' dod''8. re''16 |
re''8 la' la' |
sib'4 sib'8 |
la'4 la'8 |
sib'4. |
la'8 la' la' |
si'!4 si'8 |
