\clef "dessus" mi''16_"Tous" re'' do'' re'' mi'' fa'' |
sol'' fa'' sol'' la'' si'' sol'' |
do''' si'' la'' sol'' fa'' mi'' |
la'' sol'' fa'' mi'' re'' do'' |
fa'' mi'' re'' do'' si' la' |
sol'8 sol''8 sol'' |
mi'' do''' do''' |
la''16 si'' do''' si'' la'' sol'' |
fa'' mi'' re''8.\trill do''16 |
do''8 mi'' mi'' |
fa''4 fa''8 |
mi''16 fa'' sol'' fa'' mi'' re'' |
do'' si' do'' re'' mi'' do'' |
fa'' sol'' fa'' mi'' re'' do'' |
si'4 si'8 |
sol''16 fa'' sol'' la'' sol'' fa'' |
mi''8 do'' do'' |
fa''16 sol'' la'' sol'' fa'' mi'' |
re''4. |
sib''16 la'' sol'' fa'' mi'' re'' |
dod'' re'' mi'' fa'' sol'' mi'' |
fa''16 sol'' mi''8.\trill re''16 |
re''8 fad'' fad'' |
sol''4 sol''8 |
fad''16 sol'' la'' sib'' do''' la'' |
re'''8 sib'' sol'' |
sol'' sol'' fad'' |
sol''4 sol''8 |
