\clef "vbasse" R4.*9 |
do'8 do' do' |
do'4 do'8 |
do'4 do8 |
fa4. |
re8 re re |
sol4 sol8 |
R4.*7 |
re'8 re' re' |
re'4 re'8 |
re'4 re'8 |
sib4. |
re'8 re' re' |
sol4 sol8 |
