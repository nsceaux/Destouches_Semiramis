\clef "basse" do'4^"Tous" do'8 |
si4 si8 |
la4 la8 |
fa4 fa8 |
re4 sol8 |
si4 sol8 |
do'4 do8 |
fa4 fa8 |
re sol sol, |
do do do |
do4 do8 |
do4 do8 |
fa4. |
re8 re re |
sol4 sol8 |
R4. |
do'16 sib do' re' do' sib |
la8 fa fa |
sib16 do' re' do' sib la |
sol4 sol8 |
la4 la8 |
re16 sol, la,4 |
re8 re re |
re4 re8 |
re4 re'8 |
sib4. |
re'8 re' re' |
sol4 sol8 |
