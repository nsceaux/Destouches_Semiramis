\clef "taille" sol'4 sol'8 |
sol'4 sol'8 |
la'4 la'8 |
la'4 fa'8 |
fa'4 re'8 |
re'4 re'8 |
mi'4 sol'8 |
la'4 la'8 |
la' sol'8. fa'16 |
mi'8 do' do' |
fa'4 fa'8 |
do'4 do'8 |
do'4. |
re'8 re' fa' |
re'4\trill re'8 |
R4. |
do'8 mi' mi' |
do' fa' fa' |
fa'4. |
sol'8 re' sol' |
mi' mi' la' |
la'16 sib' la'8. sol'16 |
fad'8 re' re' |
sol'4 sol'8 |
re'4 re'8 |
re'4. |
re'8 re' re' |
re'4 re'8 |
