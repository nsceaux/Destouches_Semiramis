\clef "vdessus" R4.*9 |
<>^\markup\character Chœur
mi''8 mi'' mi'' |
fa''4 fa''8 |
mi''4 mi''8 |
do''4. |
fa''8 fa''16[ mi''] re''[ do''] |
si'4 si'8 |
R4.*7 |
fad''8 fad'' fad'' |
sol''4 sol''8 |
fad''4 fad''8 |
sol''4. |
sol''8 sol'' fad'' |
sol''4 sol''8 |
