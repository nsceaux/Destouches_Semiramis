\clef "vtaille" R4.*9 |
do'8 do' do' |
fa'4 fa'8 |
do'4 do'8 |
do'4. |
re'8 re' fa' |
re'4\trill re'8 |
R4.*7 |
re'8 re' re' |
sol'4 sol'8 |
re'4 re'8 |
re'4. |
re'8 re' re' |
re'4 re'8 |
