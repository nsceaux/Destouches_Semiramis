\clef "dessus" sol''4 re'' sib'4.\trill la'16 sol' |
re''4.*5/6 re''16 mi'' fad'' sol''4. sol''8 |
sol''4.*5/6 fad''16 sol'' la'' la''4.(\trill sol''16 la'') |
sib''2 la''4. la''8 |
la''4 re'' sol''4. sol''8 |
sol''4. fa''8 fa''4. sol''16 re'' |
mib''2 re''4.*11/12 mib''32 re'' do'' sib' la' |
sol'2 mib''4. mib''8 |
mib''2 re''4. re''8 |
la''4.*13/12 sib''32 fad'' sol'' sol''4.(\trill fad''16 sol'') |
la''1 |
la'' |
re''4. la'16 sol' la' sib' do'' la' |
sib'8[ sol'] sol''8.[ la''16] la''8.[\trill sol''16] |
fad''4 la'16 sib' do''8 do''8.\trill( sib'32 do'') |
re''8 sib''8. sib''16 sib''8 la''8. la''16 |
la''4 sol'' fad''8.(\trill mi''32 fad'') |
sol''8. fa''16 mib'' re'' mib''8 do''8.\trill do''16 |
do''8. sib'16 do'' la' re''4. |
sol''16 fa'' sol'' lab'' sol'' fa'' mib'' fa'' mib'' re'' do'' sib' |
la'4 fa'' do''8.\trill( sib'32 do'') |
re''8. do''16 sib' la' sib' do'' do''8.\trill sib'16 |
sib'8 sib''4 la''16 sib'' la'' sol'' fa'' mib'' |
re'' do'' re'' mi'' fa'' re'' sol''4. |
mi''16 re'' mi'' fa'' sol'' mi'' la''4. |
fa''16 mi'' fa'' sol'' la'' fa'' sib'' la'' sol'' fa'' mi'' re'' |
dod''8\trill[ si'16 la'] fa''8.[ mi''16] mi''8.[\trill re''16] |
re''8 fa''4 re''16 do'' re'' mib'' fa'' re'' |
mib''4. do''16 sib' do'' re'' mib'' do'' |
re''8. do''16 sib' la' sol'8 mib''4 |
re''8 sol'' sol'' sol'' fa'' fa'' |
fa'' sib'' sib'' sib'' la'' la'' |
la''16 sol'' la'' sib'' la'' sol'' fad''4. |
re''16 do'' re'' mi'' fa'' sol'' mi'' fa'' mi'' fa'' sol'' la'' |
fad'' sol'' fad'' sol'' la'' sib'' sol'' fad'' sol'' la'' sib'' sol'' |
do''' sib'' la'' sol'' fad'' mi'' re''4 r8 |
mi''16 re'' mi'' fa'' sol'' mi'' la'' sib'' do''' sib'' la'' sol'' |
fad''8[ mi''16 re''] sib''8.[ la''16] la''8.[\trill sol''16] |
sol''2 re''4.*5/6 do''16 sib' la' |
sol'4.*5/6 fad''16 sol'' la'' sib''4. sib''8 |
sib''2 la''4. la''8 |
la''4 sib''8 la'' sol''2~ |
sol''4 fa''8\trill mi'' fa''4. fa''8 |
fa''4.*5/6 mib''16 re'' mib'' mib''4.\trill re''8 |
re''2 sib''4.*5/6 la''16 sol'' fa'' |
mi''4 mi'' la''4.*5/6 sib''16 la'' sol'' |
fad''4 re'' sol''4.*5/6 fa''?16 mib'' re'' |
do''4 sib'8\trill la' sib'4. sib'8 |
sib'4. la'8 la'4.\trill sol'8 |
sol'1 |
sol' |
