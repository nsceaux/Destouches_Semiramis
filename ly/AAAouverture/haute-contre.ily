\clef "haute-contre" re''4 sib' sol'4. re'8 |
sib'4 la' sib'4. sib'8 |
sib'4. do''8 do''4.(\trill sib'16 do'') |
re''2 mib''4. mib''8 |
re''2 re''4. mib''8 |
do''2 re''4. si'8 |
do''2 sib'4. sol'8 |
mib'2 sol'4. do''8 |
do''2 sib'4 la' |
sol'4. do''8 do''4.\trill re''8 |
re''1 |
re'' |
R2. |
sol'4. do'16 sib do' re' mib' do' |
re'4. la16 sol la sib do' la |
sib8 sol sib mib' mib'8. mib'16 |
re'4. la'4 la'8 |
re'8 sib'8. sib'16 sib'8 la'8. la'16 |
la'4 la'16 la' sib'4. |
sib' do''16 re'' do'' sib' la' sol' |
fa'4 sib' la'8.\trill sol'32 la' |
sib'4 sib'8 sib' la'8. sib'16 |
sib'4 r8 r2*3/4 |
fa'16 mi' fa' sol' la' fa' sib'4. |
la' la'16 sol' la' sib' do'' la' |
re''4. sib'16 do'' sib' la' sol' fa' |
mi'4 la'8.[ re''16] dod''8.[ re''16] |
re''8 re''4 si'16 la' si' do'' re'' si' |
do''4. la'16 sol' la' sib' do'' la' |
sib'4 sol'8 mib' sol'4 |
sol'16 la' sib' do'' re'' sib' do'' sib' do'' re'' mib'' do'' |
re''8 sib' sib' sol' do'' la' |
fad'16 mi' fad' sol' fad' sol' la'4. |
sol'16 la' sib' do'' re'' sib' sol' la' sol' la' sib' do'' |
la'8 re'' re'' re''4. |
do''4 r8 sib'16 la' sib' do'' re'' sib' |
do''4. do''8 do''16 re'' do'' sib' |
la'4 sol'8.[ fad'16] fad'8.[ sol'16] |
sol'2 r |
r re''4. re''8 |
mib''2 do''4. do''8 |
re''2 sib'4. sib'8 |
do''2 la'4. la'8 |
sol'2 sol'4. sol'8 |
sol'2 r |
do''4. do''8 re''2 |
re''4. re''8 re''4 sib' |
sol' la' re'4. sol'8 |
sol'4. fad'8 fad'4. sol'8 |
sol'1 |
sol' |
