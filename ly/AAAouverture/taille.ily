\clef "taille" sib'4 sol' re'4. sib'8 |
sol'4 fad'8 la' sol'4. re'8 |
re'2 do'4 mib' |
re'2 do'4. do'8 |
do'2 sib4. sib'8 |
sib'4. la'8 sol'4. sol'8 |
sol'2 sol'4. sib8 |
sib2 do'4. do'8 |
la2 sib4 re' |
do'2 do'4 sol' |
fad'1 |
fad' |
R2. |
sol'4. do'16 sib do' re' mib' do' |
re'4. la16 sol la sib do' la |
sib8 sol sol sol do'8. la16 |
fad4 sib la8 do' |
sib8 re'8. sol'16 sol'8 mib'8. mib'16 |
mib'4. re'16 do' re' mib' fa' re' |
mib'4 sol'4. sol'8 |
do'4 re' do'8 fa' |
fa'4 sol'8 fa' fa'8. mib'16 |
re'4 r8 r2*3/4 |
fa'4. sol'16 la' sol' fa' mi' re' |
dod' si dod' re' mi' dod' re'4. |
re'16 do' re' mi' fa' re' sol'8 re'4 |
la la la'8. mi'16 |
fa'8 la'4 sol'4. |
mib'16 re' mib' fa' sol' mib' fa'4. |
fa'4 fa'8 sib do'4 |
sib8 re' re' do' fa' la' |
la' sol' re' mib' mib' do' |
la re'4 do'8 la fad |
sol sib4 sib8 la do' |
do'4. sib |
la4 r8 sol4 r8 |
sol'8. fa'16 mi'8 mi'4 mi'8 |
re'4 re' re'8. la16 |
sib2 r |
r sol'4. sol'8 |
sol'2 fa'4. fa'8 |
fa'2 mib'4. mib'8 |
mib'?2 re'4. re'8 |
re'2 do'4. do'8 |
sib2 r |
sol'4. do''8 la'2 |
la'4. la'8 sol'2 |
do'4. do'8 sib4. re'8 |
mib'2 re'4. la8 |
sib1 |
sib |
