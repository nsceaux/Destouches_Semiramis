\key re \minor
\tempo "Gravement" \midiTempo#120
\time 2/2 s1*10 \alternatives s1 s1
\time 6/8 \bar ".!:" s2.*26
\time 2/2 s2 \tempo "Gravement" s2 s1*10 \alternatives s1 s1
\bar "|."

