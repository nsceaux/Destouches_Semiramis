\clef "basse" sol,1 |
sol4 re sib,4.\trill la,16 sol, |
mib2 mib4 do |
sol2 do4 la, |
sib,2 sib,4 sol, |
la,2 si,4 sol, |
do2 sol4 sol, |
mib4.*11/12 fa32 sol fa mib re do4. do8 |
fad2 sol4 fa? |
mib2~ mib\trill |
re4. do16 sib, la,4 re |
re1 |
<<
  \tag #'basse R2.*3 |
  \tag #'basse-continue {
    \clef "dessus" re''4. la'16 sol' la' sib' do'' la' |
    \clef "taille" sol'4. do'16 sib do' re' mib' do' |
    re'4. la16 sol la sib do' la | \clef "basse"
  }
>>
sol4. do16 sib, do re mib do |
re4. la,16 sol, la, sib, do la, |
sib,8 sol, sol do16 sib, do re mib do |
fa4. sib,16 la, sib, do re sib, |
mib4. do16 sib, do re mib do |
fa4 re fa |
sib,4 mib8 re16 mib fa8 fa, |
sib,4. r2*3/4 |
sib4. sol16 fa sol la sib sol |
la4. fa16 mi fa sol la fa |
sib4. sol16 fa sol la sib sol |
la4 re la, |
re16 do re mi fa re sol4. |
do16 sib, do re mib do fa4. |
sib,16 la, sib, do re sib, mib8 do4 |
sol,16 fad, sol, la, sib, sol, la, sol, la, sib, do la, |
sib, la, sib, do re sib, do sib, do re mib do |
re4. la,16 sol, la, sib, do la, |
sib,8 sib, sib, do do do |
re re re mi4. |
fad4. sol16 fad sol la sib sol |
do'4. la16 sol la sib do' la |
re'4 sol re |
sol,2 r |
r sol4.*5/6 fa16 mib re |
do2 fa4.*5/6 mib16 re do |
sib,2 mib4.*5/6 re16 do sib, |
la,2 re4.*5/6 mib16 re do |
si,2 do |
sol,2 r |
do'4.*5/6 sib16 la sol fad4 re |
re'4.*5/6 do'16 sib la sib4 sib |
mi fad sol4.*5/6 fa?16 mib re |
do2 re4 re, |
sol,1 |
sol, |
