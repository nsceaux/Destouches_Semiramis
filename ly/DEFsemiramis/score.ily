\score {
  \new ChoirStaff <<
    \new GrandStaff <<
      \new Staff << \global \includeNotes "flute1" >>
      \new Staff << \global \includeNotes "flute2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "violon"
      \includeFigures "chiffres"
      \origLayout {
        s2 s1*3\break s1*4 s2 \bar "" \pageBreak
        s2 s1*5\break s1*5 s2 \bar "" \break s2 s1*5\pageBreak
        \grace s8 s1*5\break s1*5\break s1*5\pageBreak
        s1*6\break s1*5 s2 \bar "" \break s2 s1*4\pageBreak
        s1*4 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
