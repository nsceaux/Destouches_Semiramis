\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse-continue #:notes "violon" #:clef "dessus2")
   (chant)
   (silence #:on-the-fly-markup , #{ \markup\tacet#65 #}))
