\semiramisMark r2 |
R1*7 |
r2 re''4 re'' |
dod''8[\melisma re'' dod'' si'] la'[ sol' fa'\trill mi']( |
re'2)\melismaEnd re''4. re''8 |
re''2 dod''4. re''8 |
re''2 la'4. la'8 |
sol'8\trill[\melisma fa' sol' la'] sol'[ sib' la' sol']( |
fa')[ mi' fa' sol'] fa'[ la' sol' fa']( |
mi'2\trill)\melismaEnd mi'4 re' |
la'2 re''4. mi''8 |
fa''2 fa''4 do'' |
fa'2. fa'4 |
sib' la' sol' fa' |
do''2 fa' |
R1*2 |
r2 r4 fa'' |
mi''4. mi''8 mi''4. fa''8 |
\appoggiatura mi''8 re''2 re''4. mi''8 |
do''4.( re''8) re''4.\trill do''8 |
do''2 mi''4 mi'' |
re''8[\melisma mi'' re'' do''] si'[ la' sold' fad']( |
mi'2)\melismaEnd mi''4 mi'' |
mi''2 re''4.\trill mi''8 |
mi''2 mi''4. mi''8 |
re''8\trill[\melisma do'' re'' mi''] re''[ fa'' mi'' re'']( |
do'')[\trill si' do'' re''] do''[ mi'' re'' do'']( |
si'2\trill)\melismaEnd si'4 la' |
mi''2 mi''4 mi' |
la'2 la'4. la'8 |
re''2 re''4 sol' |
re'2. re''4 |
do'' sib' la' sol' |
re''2 sol' |
R1 |
r2 r4 fa'' |
mi'' re'' dod'' re'' |
la'2 sol'4. la'8 |
fa'4.( sol'8) sol'4.\trill la'8 |
la'2 r |
R1 |
r2 r4 fa'' |
mi'' re'' dod'' re'' |
la'2 sol'4. la'8 |
fa'4.( sol'8) sol'4.\trill la'8 |
la'2. re''4 |
do'' sib' la' sol' |
fad'2. sib'4 |
la' sol' fa'4.\trill mi'8 |
fa'4.(\melisma mi'8) re'[ mi' fa' sol'] |
la'[ sol' la' si'] dod''[ si' dod'' la']( |
re''2)\melismaEnd re''4 re'' |
do''2 sib' |
la'4. la'8 sol'4(\melisma la'8)[ sol'] |
fa'[ sol' la' sib']( la'2)\melismaEnd |
re'4. la'8 re''4 fa'' |
fa'4. sol'8 la'[ sib' sol' sib']( |
la'2)~ la'\trill |
re'2
