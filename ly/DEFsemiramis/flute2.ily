\clef "dessus" <>^\markup\whiteout "Flutes" fa''4 fa'' |
mi''8 fa'' mi'' re'' dod'' si' la' sol' |
fa'2 fa''4 mi'' |
re''4.( mi''16 fa'') mi''4.\trill re''8 |
dod''2\trill fa''4. fa''8 |
mi''8\trill re'' mi'' fa'' mi'' sol'' fa'' mi'' |
re''\trill dod'' re'' mi'' re'' fa'' mi'' re'' |
dod''4 re''8 mi'' dod''4.\trill re''8 |
re''2 fa''4 fa'' |
mi''2 dod''4.\trill dod''8 |
re''2 fa''4. fa''8 |
mi''8\trill re'' mi'' fa'' mi'' sol'' fa'' mi'' |
re''2 re''4 re'' |
re''2 dod''4.\trill re''8 |
re''2 re''4 re'' |
re''2 dod''4 re'' |
dod''2\trill r |
r fa''4 mi'' |
fa''4. mi''8 fa'' sol'' fa'' mi'' |
re''4 re'' mi'' fa'' |
mi''\trill mi'' fa'' sol''\trill |
la'' \appoggiatura sol''8 fa''4 sib''( la''8\trill sol'') |
fa'' la'' sol'' fa'' mi''4.\trill fa''8 |
fa''2. la''4 |
sol''4. sol''8 sol''4. la''8 |
fa''2 fa''4. mi''8 |
mi''4 la'' sol''8 la'' fa'' sol'' |
mi''2\trill r |
r sold''4 sold'' |
la''8 sold'' la'' si'' la'' sold'' la'' si'' |
la''4.( si''16 do''') si''4.\trill la''8 |
sold''2 la''4 la'' |
la''2 sold''4.\trill la''8 |
la''4. sold''8 la'' do''' si'' la'' |
sold''2 sold''4 la'' |
la''2 sold''4.\trill la''8 |
la''2 la''4 la'' |
fad''2 fad''4 sol'' |
fad''2. sol''4 |
re'' mi'' fad'' sol'' |
fad''4.(\trill mi''16 fad'') sol''4 sol'' |
fa'' mi'' re''\trill dod'' |
re''2 la'4 r |
r fa'' mi'' re'' |
dod'' re'' la' dod'' |
fa''4.( mi''8) mi''4.\trill re''8 |
dod''4 fa'' mi'' re'' |
dod'' dod''8( re''16 mi'') re''4\trill dod'' |
re''2 la'4 r |
r fa'' mi'' re'' |
dod'' re'' la' dod'' |
fa''4.( mi''8) mi''4.\trill re''8 |
dod''2. re''4 |
re'' mi'' fad'' sol'' |
re''2. re''4 |
do'' sib' la'4.\trill sol'8 |
la'4. la'8 re''4 re'' |
dod''8 si' dod'' re'' mi'' re'' mi'' dod'' |
re''2 la'4 fa'' |
mi''4 fa''8 mi'' re'' fa'' mi'' re'' |
dod''4 do'' si' dod'' |
re''2 re''4 dod'' |
re''4. r8 r2 |
r4 r8 re'' dod''4 re'' |
re''2 dod''4. re''8 |
re''2
