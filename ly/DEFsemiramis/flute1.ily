\clef "dessus" <>^"Flutes" la''4 la'' |
sol''8 la'' sol'' fa'' mi'' re'' dod'' si' |
la'2 la''4 la'' |
la''2 sol''4.\trill la''8 |
la''2 la''4. la''8 |
sol''8\trill fa'' sol'' la'' sol'' sib'' la'' sol'' |
fa''\trill mi'' fa'' sol'' fa'' la'' sol'' fa'' |
mi''4 fa''8 sol'' mi''4.\trill re''8 |
re''2 la''4 la'' |
la''2 sol''4.\trill la''8 |
fa''2 la''4. la''8 |
sol''8\trill fa'' sol'' la'' sol'' sib'' la'' sol'' |
fa''\trill mi'' fa'' sol'' fa'' la'' sol'' fa'' |
mi''2 mi''4 mi'' |
la''2 la''4. la''8 |
sol''8\trill fa'' sol'' la'' sol'' la'' fa'' sol'' |
mi''2\trill r |
r la''4 sol'' |
la''4. sol''8 la'' sib'' la'' sol'' |
fa''4 fa'' sol'' la'' |
sol''\trill sol'' la'' sib''\trill |
do''' \appoggiatura sib''8 la''4 re'''( do'''8\trill sib'') |
la'' do''' sib'' la'' sol''4.\trill fa''8 |
fa''2. do'''4 |
dod'''4. dod'''8 dod'''4. re'''8 |
re'''2 si''4. si''8 |
do'''2 si''4.\trill do'''8 |
do'''2 r |
r si''4 si'' |
do'''8 si'' do''' re''' do''' si'' do''' re''' |
do'''2 re'''4 re''' |
si''2\trill do'''4. do'''8 |
si'' la'' si'' do''' si''4 si'' |
mi'''2 mi'''4. mi'''8 |
re'''8 do''' re''' mi''' re''' mi''' do''' re''' |
si''4 do'''8 re''' si''4.\trill la''8 |
la''2 do'''4 do''' |
do'''2 do'''4 sib'' |
la''2. sib''4 |
fad'' sol'' la'' sib'' |
la''2\trill sol''4 sib'' |
la'' sol'' fa''\trill mi'' |
fa''8 sol'' fa'' mi'' re''4 r |
r la'' sol'' fa'' |
mi'' fa'' dod'' mi'' |
la''4.( sol''8) sol''4.\trill fa''8 |
mi''4 la'' sol'' fa'' |
mi'' mi''8( fa''16 sol'') fa''4\trill mi'' |
fa''8 sol'' fa'' mi'' re''4 r |
r la'' sol'' fa'' |
mi'' fa'' dod'' mi'' |
la''4.( sol''8) sol''4.\trill fa''8 |
mi''2. fa''4 |
fad'' sol'' la'' sib'' |
la''2. sol''4 |
fa'' mi'' re''4.\trill dod''8 |
re''4. mi''8 fa'' sol'' la'' sib'' |
mi''4 la'' la''4( sol''8) la'' |
fa'' mi'' fa'' sol'' fa''4 sib'' |
sib'' la'' la'' sol'' |
sol'' fa'' fa'' mi'' |
la''4. mi''8 mi''4.\trill re''8 |
re''4. r8 r4 r8 re'' |
la''4 sol''8 fa'' mi''4 mi'' |
mi''8 fa'' sol'' fa'' mi''4.\trill re''8 |
re''2
