\clef "dessus2" <>^\markup\whiteout "Violons" r2 |
R1 |
r2 re''4\doux do'' |
sib'2( sib')\trill |
la'2 re''4 re'' |
re''2 dod''4. re''8 |
re''2 re'4 sol' |
la' re' la2 |
re' re''4 re'' |
dod''8 re'' dod'' si' la' sol' fa'\trill mi' |
re'2 re''4. re''8 |
re''2 dod''4. re''8 |
re''2 la'4. la'8 |
sol'\trill fa' sol' la' sol' sib' la' sol' |
fa' mi' fa' sol' fa' la' sol' fa' |
mi'2 mi'4 re' |
la'2 re''4. mi''8 |
fa''2 fa''4 do'' |
fa'2. fa'4 |
sib' la' sol' fa' |
do''2 fa' |
fa''2 sib'4 do'' |
re'' mi''8 fa'' do''4 do' |
fa'2. fa''4 |
mi''4. mi''8 mi''4. fa''8 |
re''4 re' sol'2 |
la'4 fa' sol'2 |
do'2 r |
r mi'4 re' |
do'2 do'4 do' |
fa'2~ fa'\trill |
mi' mi''4. mi''8 |
re''\trill do'' re'' mi'' re'' fa'' mi'' re'' |
do''\trill si' do'' re'' do'' mi'' re'' do'' |
si'2 si'4 la' |
mi''2 mi''4 mi' |
la'2 la'4. la'8 |
re''2 re''4 sol' |
re'2. re''4 |
do'' sib' la' sol' |
re''2 sol' |
R1 |
r2 r4 fa'' |
mi'' re'' dod'' re'' |
la'2 sol'4. la'8 |
fa'4.( sol'8) sol'4.\trill la'8 |
la'2 r |
la' r |
re' r4 fa'' |
mi'' re'' dod'' re'' |
la'2 sol'4. la'8 |
fa'4.( sol'8) sol'4.\trill la'8 |
la'2. re''4 |
do'' sib' la' sol' |
fad'2. sib'4 |
la' sol' fa'4.\trill mi'8 |
fa'4. mi'8 re' mi' fa' sol' |
la' sol' la' si' dod'' si' dod'' la' |
re''2 re'4 re'' |
do''2 sib' |
la'4. la'8 sol'4 la'8 sol' |
fa' sol' la' sib' la'2 |
re'4. la'8 re''4 fa'' |
fa'4. sol'8 la' sib' sol' sib' |
la'2~ la'\trill |
re'2
