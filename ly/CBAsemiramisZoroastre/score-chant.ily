\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \keepWithTag #'complet \global
      \keepWithTag #'semiramis \includeNotes "voix"
      { s4.*11 \noHaraKiri }
    >> \keepWithTag #'semiramis \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \keepWithTag #'complet \global
      \keepWithTag #'zoroastre \includeNotes "voix"
      { s4.*11 \noHaraKiri }
    >> \keepWithTag #'zoroastre \includeLyrics "paroles"
    \new Staff \with { \consists "Horizontal_bracket_engraver" } <<
      \keepWithTag #'complet \global
      \keepWithTag #'complet \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
