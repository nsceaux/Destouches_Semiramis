\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { \haraKiriFirst } <<
        \keepWithTag #'complet \global
        \keepWithTag #'complet \includeNotes "dessus"
      >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \keepWithTag #'complet \global
      \keepWithTag #'semiramis \includeNotes "voix"
      { s4.*11 \noHaraKiri }
    >> \keepWithTag #'semiramis \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \keepWithTag #'complet \global
      \keepWithTag #'zoroastre \includeNotes "voix"
      { s4.*11 \noHaraKiri }
    >> \keepWithTag #'zoroastre \includeLyrics "paroles"
    \new Staff \with { \consists "Horizontal_bracket_engraver" } <<
      \keepWithTag #'complet \global
      \keepWithTag #'complet \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4.*4\break s4.*6\pageBreak
        s2.*2 s1 s2.\break s2.*4\break s1*2 s4 \bar "" \break
        s2 s2. s2 \bar "" \break s4 s2.*2 s1\break s1*2 s2 \bar "" \pageBreak
        s2 s2.*3\break s2.*3 s1 \bar "" \break s2 s2.*2\break
        s2.*5\break s2. s1 s2.\break \grace s8 s1*3 s2 \bar "" \pageBreak
        s2 s1 s1.*2\break s1 s2.*2 s2 \bar "" \break s2 s1. s1 s1.\break
        s1. s2.*3\break \grace s8 s2.*3 s1. s2 \bar "" \break
        s4 s2. s1 s2.*2\pageBreak
        \grace s8 s2. s1*3\break s1*4\break s1.*2 s1\break
        s1 s1. s1*2\break \grace s8 s1*3 s1. s2 \bar "" \break s1*3\pageBreak
        \grace s8 s1. s1*2\break \grace s8 s1*3 s1.\break
        s1 s2. s1\break \grace s8 s1 s2. s1\break
        s1 s1.\break s1*2 s2 \bar "" \pageBreak
        s2 s2.*2 s1\break s1*2\break s1. s4 \bar "" \break
        s8 s4.*6\break s4.*6\pageBreak
        s4.*6\break s4.*6\break s4.*6\break s4.*6\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
