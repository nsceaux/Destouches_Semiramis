\piecePartSpecs
#(let ((music #{ s1*119 <>_\markup\right-align\italic\line {
  Craignez Zoroastre en fureur.
} #}))
       `((dessus #:tag-notes part #:tag-global part #:music ,music)
         (dessus2-hc #:notes "dessus" #:tag-notes part #:tag-global part #:music ,music)
         (basse #:tag-notes part #:tag-global part #:music ,music)
         (basse-continue #:tag-notes complet #:tag-global complet
                         #:score-template "score-basse-continue-voix")
         (chant)
         (silence #:on-the-fly-markup , #{ \markup\tacet#173 #})))
