\key la \minor
<<
  \tag #'part {
    \once\override Staff.TimeSignature.stencil = ##f \time 2/2 s1*119
  }
  \tag #'complet {
    \beginMark "Prélude" \tempo "Viste"
    \time 3/8 \midiTempo#120 s4.*10
    \digitTime\time 3/4 \midiTempo#80 s2.*2
    \digitTime\time 2/2 \midiTempo#160 s1
    \digitTime\time 3/4 \midiTempo#80 s2.*5
    \digitTime\time 2/2 \midiTempo#160 s1*2
    \digitTime\time 3/4 \midiTempo#80 s2.*5
    \digitTime\time 2/2 \midiTempo#160 \grace s8 s1*4
    \digitTime\time 3/4 \midiTempo#80 s2.*6
    \time 3/2 \midiTempo#160 \grace s8 s1.
    \digitTime\time 3/4 \midiTempo#80 s2.*8
    \time 2/2 \midiTempo#160 s1
    \digitTime\time 3/4 \midiTempo#80 s2.
    \time 2/2  \midiTempo#160 \grace s8 s1*5
    \time 3/2 s1.*2
    \time 2/2 s1
    \digitTime\time 3/4 \midiTempo#80 s2.*2
    \time 2/2 \midiTempo#160 \grace s8 s1
    \time 3/2 s1.
    \time 2/2 s1
    \time 3/2 s1.*2
    \digitTime\time 3/4 \midiTempo#80 s2.*6
    \time 3/2 \midiTempo#160 s1.
    \digitTime\time 3/4 \midiTempo#80 \grace s8 s2.*2
    \digitTime\time 2/2 \midiTempo#160 \grace s8 s1
    \digitTime\time 3/4 \midiTempo#80 s2.*3
    \time 2/2 \midiTempo#160 s1*7
    \time 3/2 s1.*2
    \digitTime\time 2/2 s1*2
    \time 3/2 s1.
    \time 2/2 s1*5
    \time 3/2 s1.*2
    \digitTime\time 2/2 s1*2
    \time 3/2 \grace s8 s1.
    \time 2/2 s1*5
    \time 3/2 s1.
    \time 2/2 s1
    \digitTime\time 3/4 \midiTempo#80 \grace s8 s2.
    \time 2/2 \midiTempo#160 s1*2
    \digitTime\time 3/4 \midiTempo#80 s2.
    \time 2/2 \midiTempo#160 s1*2
    \time 3/2 s1.
    \time 2/2 s1*3
    \digitTime\time 3/4 \midiTempo#80 s2.*2
    \time 2/2 \midiTempo#160 s1*3
    \time 3/2 s1.
  }
>>
\time 3/8 \midiTempo#80 s4 \tempo "Vivement" s8 s4.*28
\bar ".!:" s4.*12 \alternatives s4. { \digitTime\time 3/4 s2 }
