<<
  %% Semiramis
  \tag #'(semiramis basse) {
    <<
      \tag #'basse { s4.*10 s2.*2 s1 s2 \semiramisMark }
      \tag #'semiramis {
        \clef "vbas-dessus" R4.*10 R2.*2 R1 r4 r
        <>^\markup\character-text Semiramis fierement
      }
    >> re''4 |
    la'4. la'8 la' sib' |
    sib'2 sib'8 re'' |
    sib'2\trill sib'8 la' |
    <<
      \tag #'basse { la'8.\trill s16 s2 s1 s4. \semiramisMark }
      \tag #'semiramis { la'4\trill r r R1 r4 r8 <>^\markup\italic vivement }
    >> sol'8 sol' la' sib' do'' |
    re''4 mi''8 mi''16 mi'' fa''8 sol'' |
    dod''4 <<
      \tag #'basse { s2 s2. s4. \semiramisMark }
      \tag #'semiramis { r4 r R2. r4 r8 <>^\markup\italic vivement }
    >> la'8 la' sib' |
    do'' do'' do'' do'' do'' re'' |
    \appoggiatura do'' sib'2 sib'4 la' |
    re''4 re''8 mi'' mi''4\trill mi''8 fa'' |
    fa''4. <<
      \tag #'basse { s8 s2 s1 s2.*6 s1. s2.*4 s2 \semiramisMark }
      \tag #'semiramis { r8 r2 R1 R2.*6 R1. R2.*4 r4 r <>^\markup\italic fierement }
    >> r8 la' |
    do''4. do''8 do'' si' |
    si'2\trill si'8 dod'' |
    dod''4.\trill dod''8 re''4 |
    re'' <<
      \tag #'basse {
        s2. s2. s1*5 s1.*2 s1 s2.*2 s1 s1. s1 s1.*2 s2.*6 s4. \semiramisMark
      }
      \tag #'semiramis {
        r4 r2 R2. R1*5 R1.*2 R1 R2.*2 R1 R1. R1 R1.*2 R2.*6 r4 r8
      }
    >> fa''8 \appoggiatura mi''8 re''2 do''4 re''8 la' |
    \appoggiatura la'8 sib'2 la'8. do''16 |
    sol'2\trill fa'8 sol' |
    \appoggiatura sol'8 la'2 si'4 dod''8 re'' |
    dod''2 la'8 la' |
    mi''2 re''8 mi'' |
    \appoggiatura mi''8 fa''2 r8 sib' |
    sol'4\trill r8 do'' la'4\trill r8 re'' |
    si'4\trill\prall si'8 dod''16 re'' mi''4. fa''8 |
    mi''4\trill mi'' r la'8 la' |
    fa'2 r4 sib'8 sib' |
    sol'4.\trill\prall sol'8 do'' sol' la' sib' |
    la'2\trill fa''4 mi'' |
    re''2 do''8 sib' la' sol' |
    fa'2\trill re'4 la'8 si' do''4 sib'8 la' |
    re''4 mi''8 fa'' fa''2( mi''8)\trill re''[ dod''] re'' |
    dod''2 mi'' |
    fa'' mi''8 re'' do'' sib' |
    la'4.\trill re''8 \appoggiatura do''8 sib'4.\prall la'8 sol'4. fa'8 |
    mi'2 mi'4 re'' |
    do'' sib' la' sol' |
    \appoggiatura sol'8 fad'2 si'4 dod''8 re'' |
    sol'2. fa'4 |
    fa'2( mi')\trill |
    re'2. <<
      \tag #'basse { s2. s1. s2 \semiramisMark }
      \tag #'semiramis { r4 r2 R1. r2 }
    >> r8 la' la' si' |
    do''4 do''8 do'' do''4\trill do''8 si' |
    \appoggiatura si'8 do''4. do''8 sol'4 sol'8 la' sib'4 sib'8 sol' |
    re''2 re'' |
    re''4 re''8 do'' sib'4.\trill la'8 |
    \appoggiatura la'8 sib'4 sib'8 re'' sol'4 la'8 sib' |
    la'2 fa'4 fa'' |
    fa''4. mi''8 mi''4( fa''8) sol'' |
    si'4. dod''8 re''4. re''8 mi''4. fa''8 |
    dod''2 do''4 do''8 re'' |
    \appoggiatura do''8 sib'4 la'8 la'16 la' re''8 sol' |
    mi'2\trill mi'4 <<
      \tag #'basse { s4 s1 s2. s1*2 s2. \semiramisMark }
      \tag #'semiramis { r4 R1 R2. R1*2 r2 r4 <>^\markup\italic vivement }
    >> la'8 si' do''4 do''8 re'' |
    si'4 do''8 re'' re''4\trill re''8 mi'' |
    mi''4 mi''8 mi'' si'4 si'8 re'' |
    sold'4. la'8 la'8\trill la' la' sold' |
    la'4 la' r8 mi'' |
    fa''4. fa''8 fa'' fa'' |
    dod''4 <<
      \tag #'basse { s2. s1 s8 \semiramisMark }
      \tag #'semiramis { r4 r2 R1 r8 <>^\markup\italic vivement }
    >> re''8 mi'' fa'' mi''\trill re'' mi'' do'' |
    fa''4. <<
      \tag #'basse { s8 s1 s4 \semiramisMark }
      \tag #'semiramis { r8 r2 r | r4 }
    >> r16 re'' |
    la'[ si' dod'' re'' mi'' dod'']( |
    fa''4.) |
    re''8 re'' re'' |
    mi''4 r16 mi'' |
    dod''[\melisma re'' mi'' re'' dod'' si'] |
    la'[ sib' do'' sib' la' sol']( |
    fa'4)\melismaEnd r16 fa'' |
    fa''4 fa''8 |
    re''16[ mib'' re'' do''] sib'[ la'] |
    sol'8\trill sol' sol' |
    do''8. re''16 do'' sib' |
    la'4\trill fa''8 |
    re''8.\trill do''16 si' la' |
    sold'4 mi''8 |
    si'8. si'16 dod'' re'' |
    dod''4 r16 re'' |
    la'[ si' dod'' re'' mi'' dod'']( |
    fa''4.) |
    re''8 re'' re'' |
    mi''4 r16 mi'' |
    dod''[ re'' mi'' re'' dod'' si']( |
    la'16)[ sib' do'' sib' la' sol']( |
    fa'4) r16 fa'' |
    fa''4 r16 fa'' |
    re''[ mib'' re'' do''] sib'[ la'] |
    sol'8\trill sol' do'' |
    re''16[ do'' sib' la'] sib'[ sol'] |
    la'8\trill la' la'16 la' |
    re''4 r16 re'' |
    si'4\trill si'16 si' |
    mi''4 r16 mi'' |
    dod''4 dod''16 dod'' |
    fa''8 fa''16 mi'' re''8 |
    re''16 do'' sib'[ la'] sol'[ fa'] |
    mi'8\trill mi' la'16 si' |
    dod''8 re'' mi'' |
    la'4 la'16 re'' |
    si'8 dod'' re'' |
    dod'' re'' mi'' |
    fa'' mi''8.\trill re''16 |
    re''4 la'16 la'
    re''2
  }
  %% Zoroastre
  \tag #'(zoroastre basse) {
    \clef "vbasse" R4.*10 |
    <>^\markup\character-text Zoroastre vivement re'2 la4 |
    fad4 fad8 r la la |
    sib4 sib8 sib sol4\trill sol8 la |
    fa4\trill re <<
      \tag #'basse { s4 s2.*3 s8. \zoroastreMark }
      \tag #'zoroastre { r4 R2.*3 r8 r16 <>^\markup\italic vivement }
    >> la16 la8 sol fa la |
    re4 re8 re sol4 sol8 sol |
    \appoggiatura sol8 do4 do8 <<
      \tag #'basse { s8 s2 s2. s4 \zoroastreMark }
      \tag #'zoroastre { r8 r2 R2. r4 <>^\markup\italic vivement }
    >> la8 la16 la si8 dod' |
    re'4 sol8 sol16 fa fa8 mi |
    mi4.\trill <<
      \tag #'basse { s4. s2. s1*2 s4. \zoroastreMark }
      \tag #'zoroastre { r8 r4 R2. R1*2 r4 r8 <>^\markup\italic vivement }
    >> la8 re' do' sib la |
    sol4\trill sol8 sol do'4 do'8 do' |
    fa4 sib8 sib16 sib sib la sol fa |
    mi2\trill mi4 |
    la r la8 la |
    dod'4. dod'8 dod' la |
    re'4. la8 la sib |
    sol4.\trill sol8 la mi |
    \appoggiatura mi8 fa4 fa8 r \sugNotes r4 re'8 re' fad4 fad8 sol |
    sol4 mi8 mi16 mi mi8 sol |
    do8. fa16 fa8 sol la fa |
    sib4. r8 sib sib |
    mi4. r8 fa sol |
    dod4 dod <<
      \tag #'basse { s4 s2.*3 s4 \zoroastreMark }
      \tag #'zoroastre { r4 R2.*3 r4 <>^\markup\italic vivement et tendrement }
    >> re'4 sold8 sold la si |
    mi4 fa8 fa16 re \afterGrace mi8( re8) mi |
    \appoggiatura mi la,2 la4. fa8 |
    re2\trill sib4. sol8 |
    mi2\trill do'8 sol16 sol la8 sib |
    la4 fa r la8 la |
    \appoggiatura la8 sib2 sol4.\trill sol8 |
    sol2 fa4 fa8 sol sol4.\trill la8 |
    la2 r4 dod'8 mi' la4 sol8 la |
    fad4. la8 si si dod' re' |
    dod'4 la8 r la sol |
    fad2 sol8 la |
    \appoggiatura la8 sib4 sib8 la sol4\trill\prall sol8 la |
    mi2\trill r4 do'8 sib la4\trill sol8 fa |
    do'2 la4 sib8 do' |
    fa4. sol8 la2( sol)\trill |
    fa4. la8 la2 la4 la8 la |
    re4. mi8 fa4 |
    sol4.( fa8)\trill mi fa |
    mi4.\trill fa8 sol la |
    \appoggiatura re8 dod2 la8 sol |
    fa2\trill\prall mi8 fa |
    fa4( mi2)\trill |
    re4. <<
      \tag #'basse {
        s8 s2 s s2.*2 s1 s2.*3 s1*7 s1.*2 s1*2 s1. s1*5 s2. \zoroastreMark
      }
      \tag #'zoroastre {
        r8 r2 r R2.*2 R1 R2.*3 R1*7 R1.*2 R1*2 R1. R1*5 r2 r4
      }
    >> la8 sol fa4 mi8 re |
    la2 fa4 sol8 la sol4.\trill la8 |
    mi2\trill <<
      \tag #'basse { s2 s1 s1. s1*5 s1. s1 s2. s2.\zoroastreMark }
      \tag #'zoroastre { r2 R1 R1. R1*5 R1. R1 R2. r2 r4 }
    >> la4 |
    \appoggiatura sol8 fa4 fa <>^\markup\italic vivement la4 la8 sib |
    do' la re' do' sib la |
    sol4 mi8 sol do4 sol8 la |
    sib4 la8 la re'4 re'8 re |
    la2 la4 <<
      \tag #'basse { s2. s1*3 s2.*2 s4 \zoroastreMark }
      \tag #'zoroastre { r4 r2 R1*3 R2.*2 r4 }
    >> la8 la sib4 sib8 sib |
    mi4 mi8 r la4 fa8 la |
    re <<
      \tag #'basse { s4. s2 s4. \zoroastreMark }
      \tag #'zoroastre { r8 r4 r2 r4 r8 <>^\markup\italic vivement }
    >> la8 si4 dod'8 re' dod'4 si8 la |
    re'4
    \tag #'zoroastre {
      r8 |
      r8 r r16 la |
      re[ mi fa sol la fa]( |
      sib4.) |
      sol8 sol sol |
      la4 r16 la |
      fa[ sol la sol fa mi]( |
      re4) r16 re' |
      re'4 re'8 |
      sib16[ do' sib la] sol[ fa] |
      do'8 do' do' |
      la8. sib16 la sol |
      fa4 re'8 |
      si8. la16 sold la |
      mi4 do'8 |
      re8. re16 mi mi |
      la,4 r8 |
      r8 r r16 la |
      re[ mi fa sol la fa]( |
      sib4.) |
      sol8 sol sol |
      la4 r16 la |
      fa[ sol la sol fa mi]( |
      re4) r16 re' |
      re'4 r16 re' |
      sib[ do' sib la] sol[ fa] |
      do'8 do' la |
      sib do' do'16 do |
      fa8 fa r |
      r r re16 re |
      sol4 r16 sol |
      mi4 mi16 mi |
      la4 r16 la |
      fa8 fa16 fa sib8 |
      sib16 la sol[ fa] mi[ re] |
      dod8 dod fa16 fa |
      mi8 re dod |
      fa4 re16 re |
      sol8 la sib |
      la si dod' |
      re' la8. la16 |
      re4 r8 |
      re2
    }
  }
>>