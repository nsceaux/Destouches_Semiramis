\tag #'(zoroastre basse) {
  Ah ! Per -- fi -- de, o -- sez- vous soû -- te -- nir ma pré -- sen -- ce ?
}
\tag #'(semiramis basse) {
  Ces noms in -- ju -- ri -- eux me sont- ils a -- dres -- sez ?
}
\tag #'(zoroastre basse) {
  A -- vez- vous crû for -- cer mon dé -- pit au si -- len -- ce ?
}
\tag #'(semiramis basse) {
  Ou -- bli -- ez- vous mon rang, & qui vous of -- fen -- sez ?
}
\tag #'(zoroastre basse) {
  Ou -- bli -- ez- vous le mien, & qui vous tra -- his -- sez ?
}
\tag #'(semiramis basse) {
  Se -- mi -- ra -- mis ne con -- noît point de Maî -- tre,
  Le Ciel est seul ju -- ge des Rois.
}
\tag #'(zoroastre basse) {
  Le Ciel van -- ge sur eux le mé -- pris de ses Loix,
  Et vous l’é -- prou -- ve -- rez peut- ê -- tre.
  Quoi ! les ser -- mens les plus sa -- crez
  N’ont pû fi -- xer vôtre in -- cons -- tan -- ce !
  Eh ! quel est le Ri -- val que vous me pré -- fe -- rez ?
  Un é -- tran -- ger sans nom, sans é -- tats, sans nais -- san -- ce ?
}
\tag #'(semiramis basse) {
  J’i -- gno -- re ses A -- yeux, je con -- nois ses ver -- tus.
}
\tag #'(zoroastre basse) {
  In -- grate, il est donc vrai que vous ne m’ai -- mez plus ?
  Tant de soins, tant d’a -- mour, tant de per -- se -- ve -- ran -- ce,
  Mon es -- poir, mon bon -- heur sont pour ja -- mais per -- dus.
  Que ne puis-je é -- touf -- fer l’ar -- deur qui me dé -- vo -- re ;
  Que ne puis-je à mon tour ou -- bli -- er vos at -- traits,
  Ces per -- fi -- des at -- traits, que mal -- gré moi j’a -- do -- re.
  Faut- il, quand vô -- tre cœur m’a -- ban -- donne à ja -- mais,
  Que vos re -- gards me re -- tien -- nent en -- co -- re ?
}
\tag #'(semiramis basse) {
  Eh bien, dans mes re -- gards, li -- sez donc mes dou -- leurs.
  Pour vous van -- ger, joü -- is -- sez de mes pleurs.
  Je veux, je crains, j’es -- pere, & mon es -- poir me gê -- ne ;
  Je com -- bats, je ré -- siste, & ce -- de tour à tour :
  Un pen -- chant in -- con -- nu m’en -- traî -- ne,
  Plus puis -- sant mil -- le fois, & moins doux que l’a -- mour.
  Ah ! ah ! si vous con -- nois -- siez l’ex -- cès de mes al -- lar -- mes,
  Vous- même à mes mal -- heurs vous don -- ne -- riez des lar -- mes.
}
\tag #'(zoroastre basse) {
  N’a -- viez- vous à m’of -- frir que ce cru -- el se -- cours ?
}
\tag #'(semiramis basse) {
  E -- par -- gnons- nous d’i -- nu -- ti -- les dis -- cours,
  Sei -- gneur, res -- pec -- tons nô -- tre gloi -- re.
  D’un mal -- heu -- reux a -- mour é -- touf -- fez la me -- moi -- re,
  Lais -- sez mon tris -- te cœurs en proye à ses re -- mords.
  C’est mal -- gré moi, que je cou -- ronne Ar -- sa -- ne...
}
\tag #'(zoroastre basse) {
  Ar -- sa -- ne ! ah ! que ce nom re -- dou -- ble mes trans -- ports !
  C’en est fait, à pe -- rir vôtre a -- mour le con -- dam -- ne.
}
\tag #'(semiramis basse) {
  D’un a -- veu -- gle cou -- roux nous bra -- vons les ef -- forts.
  Mais vous- mê -- me trem -- blez d’en ê -- tre la vic -- ti -- me.
  Le Ciel se -- ra pour lui.
}
\tag #'(zoroastre basse) {
  Se -- ra- t’il pour le cri -- me,
  Il pe -- ri -- ra.
}
\tag #'(semiramis basse) {
  Crai -- gnez les Dieux & sa va -- leur.
}
\tag #'(zoroastre basse) {
  Crai -- gnez Zo -- ro -- astre en fu -- reur.
}
\tag #'(semiramis zoroastre basse) {
  Ton -- nez, __ Dieux im -- mor -- tels, ton -- nez, __ ton -- nez, lan -- cez __ la Fou -- dre,
  Per -- dez vos En -- ne -- mis,
  Per -- dez vos En -- ne -- mis, qu’ils tom -- bent sous vos coups,
  Ton -- nez, __ Dieux im -- mor -- tels, ton -- nez, __ ton -- nez, lan -- cez __ la Fou -- dre,
  \tag #'(semiramis basse) { lan -- cez __ la Fou -- dre, }
  \tag #'zoroastre { lan -- cez, lan -- cez la Fou -- dre, }
  Hâ -- tez- vous, Frap -- pez, hâ -- tez- vous, Frap -- pez,
  \tag #'(semiramis basse) { é -- cla -- tez, }
  Hâ -- tez- vous de re -- duire en pou -- dre
  \tag #'(semiramis basse) {
    Les su -- per -- bes Mor -- tels, Les su -- per -- bes Mor -- tels
  }
  \tag #'zoroastre {
    Les par -- ju -- res Mor -- tels, Les par -- ju -- res Mor -- tels
  }
  qui s’ar -- ment con -- tre vous.
  \tag #'(semiramis basse) { Hâ -- tez - }  vous.
}
