\clef "basse" <<
  \tag #'part { R1*119 | r4 }
  \tag #'complet {
    fa16_"Tous" mi re mi fa sol |
    la sol la si dod' la |
    re' mib' re' do' sib la |
    sib do' sib la sib sol |
    la sib la sol la la, |
    fa mi re mi fa sol |
    la sol la si dod' la |
    re' mib' re' do' sib la |
    sib do' sib la sib sol |
    la sib la sol la la, |
    re2.~ |
    re~ |
    re2 dod |
    re2. |
    fad |
    sol |
    mi |
    fa2 la,4 |
    sib,2 si, |
    do2. sol8 la |
    sib4 sol2 |
    la4 sol2 |
    fa4 dod re |
    la,2 la4 |
    fad2. |
    sol re'8 do' |
    sib4 sol do' do |
    fa2 sib, |
    do la, |
    sib,4 sol,2 |
    la,2.~ |
    la,2 la4 |
    sol2. |
    fa |
    mi |
    re1 do2 |
    si,4 sib,2 |
    la,2. |
    sol,~ |
    sol, |
    la,2 la8 sol |
    fad2. |
    sol |
    la4 la,2 |
    re1 |
    do4 re mi |
    la,1 |
    sib,2 sol, |
    do mi |
    fa fa, |
    sol,1 |
    la,2 re4 do sib,2 |
    la,1 dod2 |
    re sol |
    la la,4 |
    re4. do8 sib, la, |
    sol,2 si,! |
    do1 fa2 |
    la,1 |
    sib,2 do do, |
    fa,4 fa dod2 do |
    si,2. |
    dod2 re4 |
    la,2. |
    sol,2 fa,4 |
    sib,2 sol,4 |
    la,2. |
    re,4 re sib2 fad |
    sol re'8 do' |
    sib2. |
    la2 sold |
    la2. |
    dod |
    re |
    mi2 fa |
    sol sold |
    la2 la, |
    re1 |
    mi |
    fa~ |
    fa2. dod4 |
    re1 do2 |
    sib,2 sol,1 |
    la,2 dod |
    re mi |
    fa sol1 |
    la2. r4 |
    r re' do' sib |
    la re' sol8 fa mi re |
    dod2. re4 |
    la,1 |
    re1. | \allowPageTurn
    dod2 re sol, |
    la,4~ la,16 sib, la, sol, fa,4 fa |
    mi2 re |
    do1 sol2 |
    fad1~ |
    fad |
    sol2 mi |
    fa1 |
    sol |
    sold1. |
    la2 fad |
    sol4 re'8 do' sib4 |
    la2. la,4 |
    re1 | \allowPageTurn
    la,4 sib,2 |
    do2. sib,8 la, |
    sol,4 re8 do sib,2 |
    la,2. la8 sol fad2 |
    sol8 fa mi fa sol4 sol, |
    do2 re |
    mi fa8 re mi mi, |
    la,2.~ |
    la,~ |
    la,4 fa, sol,2 |
    la,1 |
    sib,4 si, do sib, |
    la,2 sold, sol, |
    fa,4
  }
>>
r8 |
r8 r r16 la |
re mi fa sol la fa |
sib4. |
sol8 sol sol |
la4 r16 la |
fa sol la sol fa mi |
re4 r16 re' |
re'4 re'8 |
sib16 do' sib la sol fa |
do'8 do' do' |
la8. sib16 la sol |
fa4 re'8 |
si8. la16 sold la |
mi4 do'8 |
re8. re16 mi mi |
la,4 r8 |
r8 r r16 la |
re mi fa sol la fa |
sib4. |
sol8 sol sol |
la4 r16 la |
fa sol la sol fa mi |
re4 r16 re' |
re'4 r16 re' |
sib do' sib la sol fa |
do'8 do' la |
sib do' do'16 do |
fa8 fa r |
r r re16 re |
sol4 r16 sol |
mi4 mi16 mi |
la4 r16 la |
fa8 fa16 fa sib8 |
sib16 la sol fa mi re |
dod8 dod fa16 fa |
mi8 re dod |
fa4 re16 re |
sol8 la sib |
la si dod' |
re' la8. la16 |
re4 r8 |
re2*7/8~ \hideNotes re16
