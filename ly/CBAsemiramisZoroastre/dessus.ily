\clef "dessus"
<<
  \tag #'complet {
    R4.*10 R2.*2 R1 R2.*5 R1*2 R2.*5 R1*4 R2.*6 R1. R2.*8 R1 R2. R1*5
    R1.*2 R1 R2.*2 R1 R1. R1 R1.*2 R2.*6 R1. R2.*2 R1 R2.*3 R1*7 R1.*2
    R1*2 R1. R1*5 R1.*2 R1*2 R1. R1*5 R1. R1 R2. R1*2 R2. R1*2 R1.
    R1*3 R2.*2 R1*3 R1. |
  }
  \tag #'part { R1*119 }
>>
r4 r8 |
R4. |
r8 r r16 la''\doux ^"Violons" |
fa'' mi'' fa'' sol'' la'' fa'' |
sib''4. |
mi''8 mi'' mi'' |
fa''4 r16 la'' |
la'' sol'' fa'' re'' la'' la'' |
la''4 r16 re'' |
fa''4 r16 fa'' |
mi'' re'' mi'' fa'' sol'' mi'' |
fa''4 fa'8 |
do''4 la'8 |
sold'8. la'16 si' do'' |
si'4 la'8 |
la'4 sold'8 |
la'4 r8 |
R4. |
r8 r r16 la'' |
fa'' mi'' fa'' sol'' la'' fa'' |
sib''4. |
mi''8 mi'' mi'' |
fa''4 r16 la'' |
la'' sol'' fa'' re'' la'' la'' |
la''4 r16 re'' |
fa''4 r16 fa'' |
mi'' fa'' mi'' re'' do''8 |
fa' fa' mi' |
fa'4 do''16 do'' |
fa''4 r16 fa'' |
re''4\trill re''16 re'' |
sol''4 r16 sol'' |
mi''4\trill mi''16 mi'' |
la''8 la''16 sol'' fa''8 |
fa'16 fa' sol'8 sol' |
la' la' re'16 re' |
sol'8 fa' mi' |
re'4 fa'16 fa' |
fa'8 mi' re' |
la' fa' mi' |
re' dod'8. re'16 |
re'4 do''16 do'' |
re''2

