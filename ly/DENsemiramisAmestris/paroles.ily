A -- mes -- tris, a -- che -- vez ce no -- ble sa -- cri -- fi -- ce.
Qu’il nous ren -- de le Ciel pro -- pi -- ce.
Au -- guste In -- ter -- pre -- te des Dieux,
Vous tien -- drez dans vos mains le bon -- heur de ces lieux.

Je quit -- te pour ja -- mais l’é -- clat qui m’en -- vi -- ron -- ne,
Maî -- tre des Im -- mor -- tels, rem -- plis -- sez tout mon cœur,
La pom -- pe, les plai -- sirs, la su -- prê -- me gran -- deur
N’ont plus de droits sur moi, je vous les a -- ban -- don -- ne.
Ren -- dez heu -- reux les jours que je vous don -- ne.
Ren -- dez heu -- reux les jours que je vous don -- ne.
