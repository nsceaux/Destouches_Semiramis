\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } << \global \includeNotes "dessus" >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*5\break s2.*4 s2 \bar "" \break s4 s2.*3\pageBreak
        s2.*6\break s2.*6\break s2.*6\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
