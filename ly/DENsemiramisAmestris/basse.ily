\clef "basse" re2. |
mi |
fa |
dod |
re |
sib,2 do4 |
fa2 re4 |
sol fa mi |
re do sib, |
la,2 la8 sol |
fa2. |
sol |
fa8 sol la4 la, |
re2.~ |
re |
sol2 re'8 do' |
sib2 sol4 |
re'2. |
do' |
sib |
sol |
la2 la,4 |
re2 sol4 |
do'2. |
sib |
la2 sib4 |
do' do2 |
fa2. |
sol |
la |
fa4 mi re |
dod2. |
re2 sib,4 |
sol, la,2 |
re2. |
fa4 mi re |
dod2. |
re2 sib,4 |
sol, la,2 |
re2. |
