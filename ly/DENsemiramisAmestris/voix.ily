\semiramisMark r4 r la'8 re'' |
re''2 dod''8 mi'' |
la'2~ la'8 sib' |
sol'4.\trill\prall fa'8 sol' la' |
fa'4\prall re' la'8 la' |
re''4 re''8 sol' la' sib' |
la'4 fa' la' |
\appoggiatura la'8 sib'4. la'8 sol'4 |
fa'4.\trill\prall mi'8 re'4 |
la'2 si'8 dod'' |
re''4. fa''8 \appoggiatura mi''8 re''4 |
si'2 si'8 dod'' |
re''2 re''8 dod'' |
re''2 \amestrisMarkText "la main sur l’Autel" r8 re'' | \noBreak
re''4. re''8 mi'' fa'' |
sib'2 la'4 |
re''4. dod''8 re'' mi'' |
\appoggiatura mi''8 fa''2 fa''4 |
la' la'8 si' do'' la' |
re''2 mi''8 fa'' |
mi''4.\trill fa''8 \appoggiatura mi''8 re''4 |
dod''2 la'4 |
\appoggiatura sol'8 fa'4 fa'8 r sib' sib' |
sol'4.\trill r8 do''8 re'' |
mi''4.\trill re''8 do''4 |
fa''4. mi''8 re'' do'' |
do''4.( sib'8)\trill[ la'] sib' |
la'2.\trill |
sib'4 sib'8 la' sol' fa' |
mi'2\trill mi'4 |
re'' dod''4. re''8 |
la'4 la'4.( sol'16[\trill fa']) |
fa'2 sol'4 |
la'8 sib' mi'2\trill |
re'2. |
re''4 dod''4. re''8 |
la'4 la'4.( sol'16[\trill fa']) |
fa'2 sol'4 |
la'8 sib' mi'2\trill |
re'2. |
