\clef "dessus" R2.*13 |
r4 r <>^"Flutes" r8 fa'' |
fa''4. fa''8 sol'' la'' |
mi''2 fa''4 |
fa''4. mi''8 fa'' sol'' |
\appoggiatura sol'' la''2 re''8 r |
mi''4 mi''4. la''8 |
la''2 sol''8 la'' |
sib'' la'' sol'' la'' \appoggiatura sol'' fa''4 |
mi''2 dod''4 |
re''4. r8 re'' sol'' |
mi''4. r8 mi'' fa'' |
sol''4. fa''8 mi''4 |
la''4. sol''8 fa'' fa'' |
fa''4( mi''4.)\trill fa''8 |
fa''2. |
mi''4 mi''8 fa'' mi'' re'' |
dod''2 r4 |
la'' sol'' fa'' |
mi'' mi''2 |
la' mi''4 |
fa''8 sol'' dod''4. re''8 |
re''2. |
la''4 sol'' fa'' |
mi'' mi''2 |
la'2 mi''4 |
fa''8 sol'' dod''4. re''8 |
re''2. |
