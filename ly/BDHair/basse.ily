\clef "basse" sol2 la4 |
sib2. si2 sol4 |
do' do sol <>^\markup\whiteout Bassons mib si,2 |
do8 si, do re mib do sol fa mib2 |
re8 do re mi fad re sol2 la4 |
re2. <>^\markup\whiteout Tous sib2 la4 |
sib2 la4 sol la sib |
fa8 mib fa sol fa mib <>^\markup\whiteout Bassons re2 re4 |
mib4. re8 do4 sib, fa fa, |
sib8 la sib do' sib la <>^\markup\whiteout [Tous] sol2 fad4 |
sol2 sol,4 <>^\markup\whiteout Bassons sol2 fad4 |
sol2 sol,4 <>^\markup\whiteout Tous mib2 mib4 |
la, re sol do re re, |
sol,2. sib2 la4 |
sol,2
