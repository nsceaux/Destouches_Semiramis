\clef "taille" sib'2 sib'4 |
fa'2. sol'2 si4 |
do'2 si4 r2*3/2 |
R1.*2 |
r2*3/2 fa'2 fa'4 |
fa'2. sol'4 fa' fa' |
fa'2 do'4 r2*3/2 |
R1. |
r2*3/2 sol'2 la'4 |
re'4. do'8 sib4 r2*3/2 |
r mib'2 mib'4 |
mib'? re' re' mib'! re'4. la8 |
sib2. fa'2 fa'4 |
sib2
