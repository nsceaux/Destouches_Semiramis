\clef "haute-contre" re''2 re''4 |
sib'2. re''2 re''4 |
sol'2. r2*3/2 |
R1.*2 |
r2*3/2 re''2 do''4 |
sib'2 fa'4 sib' do'' sib' |
la'2 fa'4 r2*3/2 |
R1. |
r2*3/2 sib'2 la'4 |
sol'2 re'4 r2*3/2 |
r sib'2 sol'4 |
sol' fad' sol' sol' fad'4. sol'8 |
sol'2. re''2 do''4 |
sol'2
