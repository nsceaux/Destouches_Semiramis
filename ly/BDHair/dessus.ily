\clef "dessus" <>^\markup\whiteout Tous sol''2 sol''4 |
re''2. sol''4. fa''8 sol''4 |
mib''2 re''4 <>^\markup\whiteout Hautbois sol''8 lab'' sol'' fa'' mib'' re'' |
mib''4. re''8 do''4 sib'8. do''16 do''4.\trill( sib'16) do'' |
re''2. sol''2 sol''4 |
re''2. <>^\markup\whiteout Tous fa''2 fa''4 |
re''2\trill do''4 mib''2 re''4 |
do''2\trill la'4 <>^\markup\whiteout Hautbois sib'8 do'' re'' do'' sib' la' |
sol'4. sol'8 mib''4 re'' do''2\trill |
sib'2. <>^\markup\whiteout [Tous] re''2 re''4 |
sib'4.\trill la'8 sol'4 <>^\markup\whiteout Hautbois re''8 do'' re'' mib'' re'' do'' |
sib'4. la'8 sol'4 <>^\markup\whiteout Tous sol''8 fa'' mib'' re'' do'' sib' |
do''4 la'\trill sib'4. do''8 la'4.\trill sol'8 |
sol'2. fa''2 fa''4 |
sol'2
