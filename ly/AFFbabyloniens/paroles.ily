\tag #'(vdessus basse) {
  Dieu char -- mant de Cy -- the -- re,
  Ré -- pand tes fa -- veurs ;
  Et du soin de te plai -- re
  Rem -- pli tous nos cœurs.
  De tes flâ -- mes
  Nos a -- mes
  Sen -- tent les dou -- ceurs.
  Plus de pei -- nes,
  Tes chaî -- nes
  Sont fai -- tes de fleurs.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Dieu char -- mant de Cy -- the -- re,
  Ré -- pand tes fa -- veurs ;
  Et du soin de te plai -- re
  Rem -- pli tous nos cœurs.
  De tes flâ -- mes
  Nos a -- mes
  Sen -- tent les dou -- ceurs.
  Plus de pei -- nes,
  Tes chaî -- nes
  Sont fai -- tes de fleurs.
}
\tag #'(vhaute-contre basse) {
  De la gran -- deur su -- prê -- me
  Les Dieux sont ja -- loux ;
  Mais l’A -- mour est le mê -- me
  Pour eux & pour nous.
  Trop ai -- ma -- ble Jeu -- nes -- se,
  Crai -- gnez moins ses coups :
  Si ce Dieu ne vous bles -- se,
  Quel bien goû -- tez- vous ?
  Trop ai -- ma -- ble Jeu -- nes -- se,
  Crai -- gnez moins ses coups :
  Si ce Dieu ne vous bles -- se,
  Quel bien goû -- tez- vous ?
}
\tag #'(vdessus vhaute-contre basse) {
  Doux Em -- pi -- re,
  Dont les loix sont nos de -- sirs,
  Quel mar -- ty -- re
  De ré -- sis -- ter aux plai -- sirs !
  Qu’on sou -- pi -- re !
  On ne res -- pi -- re
  Que du jour,
  Où l’A -- mour
  Nous ins -- pi -- re.
  Doux Em -- pi -- re,
  Dont les loix sont nos de -- sirs,
  Quel mar -- ty -- re
  De ré -- sis -- ter aux plai -- sirs !
}
\tag #'vdessus {
  Ce -- le -- brons tous tes char -- mes,
  Ra -- ni -- me nos voix,
}
\tag #'(vdessus vhaute-contre) {
  Loin de nous tes al -- lar -- mes.
  Chan -- tons mil -- le fois :
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Tendre A -- mour, de tes ar -- mes
  Lais -- se- nous le choix.
  Sans en -- nuis & sans lar -- mes
  Vi -- vons sous tes loix.
}
\tag #'vdessus {
  Quel tri -- om -- phe ! que d’heu -- reux ins -- tants !
  Quel -- le gloi -- re ! quels trans -- ports char -- mants !
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Quel tri -- om -- phe ! que d’heu -- reux ins -- tants !
}
\tag #'(vdessus vhaute-contre basse) {
  Pour prix de ta vic -- toi -- re
  Rend nos cœurs con -- tens.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Quel tri -- om -- phe ! que d’heu -- reux ins -- tants !
  Quel -- le gloi -- re ! quels trans -- ports char -- mants !
}
