\clef "vbasse" r4 r |
R2.*15 |
r4 sib sib |
la la la |
sol sol sol |
fa fa fa |
sib, sib sib |
la la la |
sol sol sol |
fa fa fa |
sib, sib fa |
sol sol mib |
fa fa re |
sol mib fa |
sib, sib fa |
sol sol mib |
fa fa re |
sol mib fa |
sib, r r |
R2.*23 |
r4 r r |
R2.*23 |
r4 r r |
R2.*23 |
r4 r r |
R2.*7 |
r4 do re |
mib re mi |
fa mib! re |
mib fa fa, |
sib, do re |
mib re mi |
fa mib! re |
mib fa fa, |
sib, r r |
R2.*7 |
r4 sib sib |
sib4.( do'8)[ sib lab]( |
sol4) mib sol |
lab fa sib |
mib r r |
R2.*3 |
r4 fa fa |
fa4.( sol8)[ fa mib]( |
re4) sib, re |
mib fa fa, |
sib, fa fa |
fa4.( sol8)[ fa mib]( |
re4) sib, re |
mib fa fa, |
sib,2
