\piecePartSpecs
#`((dessus #:score "score-dessus")
   (dessus2-hc #:score "score-dessus2-hc")
   (dessus-hc #:score "score-dessus-hc")
   (haute-contre)
   (taille)
   (basse)
   (basse-continue #:tag-notes basse-continue
                   #:score-template "score-basse-continue-voix")
   (chant)
   (silence #:on-the-fly-markup , #{ \markup\tacet#144 #}))
