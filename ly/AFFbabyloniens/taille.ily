\clef "taille" r4 r |
R2.*15 |
r4 sib mib' |
mib' la la |
re' sib sol |
la la la |
sib sib mib' |
mib' la la |
re' sib sol |
la la la |
sib re' do' |
sib sol do' |
la fa sib |
sib sol fa |
fa re' do' |
sib sol do' |
la fa sib |
sib sol fa |
fa r r |
R2.*23 |
r4 r r |
R2.*23 |
r4 r r |
R2.*23 |
r4 r r |
R2.*7 |
r4 do' do' |
sib re' re' |
re'\trill do' sib |
la16 sol8 la16 la4.\trill sib8 |
sib4 do' do' |
sib re' re' |
re'\trill do' sib |
la16 sol8 la16 la4.\trill sib8 |
sib4 r r |
R2.*7 |
r4 sib do' |
re'4. do'8 re' sib |
mib'4 mib' mib' |
do' do' sib |
sib r r |
R2.*3 |
r4 la sib |
do'4. sib8 do' la |
re'4 re' sib |
sib la4.\trill sib8 |
sib4 la sib |
do'4. sib8 do' la |
re'4 re' sib |
sib la4.\trill sib8 |
sib2
