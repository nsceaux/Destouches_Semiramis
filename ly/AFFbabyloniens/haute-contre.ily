\clef "haute-contre" r4 r |
R2.*15 |
r4 fa'4 sol' |
sol' fa' fa' |
fa' mib' sib |
do' do' fa' |
fa' fa' sol' |
sol' fa' fa' |
fa' mib' sib |
do' do' fa' |
fa' fa' fa' |
fa'\trill mib' mib' |
mib'\trill re' fa' |
re' mib' do' |
re' fa' fa' |
fa'\trill mib' mib' |
mib'\trill re' fa' |
re' mib' do' |
re' r r |
R2.*23 |
r4 r r |
R2.*23 |
r4 r r |
R2.*23 |
r4 r4 r |
R2.*3 |
r4 fa' sol' |
la' sol' la' |
sib' la' sol' |
fa'4. sol'8 mi'4 |
fa'4 fa' fa' |
sol' fa' sol' |
fa' fa' fa' |
mib'4 mib'4.\trill re'8 |
re'4 mib' fa' |
sol' fa' sol' |
fa' fa' fa' |
mib' mib'4.\trill re'8 |
re'4 r r |
R2.*8 |
r4 sib' sib' |
sib' sol' sib' |
lab' lab' fa' |
sol' r r |
R2.*4 |
r4 la' la' |
sib' fa' fa' |
mib' mib' do' |
re' r r |
r la' la' |
sib' fa' fa' |
mib' mib' do' |
re'2
