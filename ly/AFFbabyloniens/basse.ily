\clef "basse"
<<
  \tag #'basse { r4 r | R2.*15 | r4 }
  \tag #'basse-continue {
    sib4 sib |
    la la la |
    sol sol sol |
    fa fa fa, |
    sib, sib sib |
    la la la |
    sol sol sol |
    fa fa fa, |
    sib, sib fa |
    sol sol mib |
    fa fa re |
    sol mib fa |
    sib, sib fa |
    sol sol mib |
    fa fa re |
    sol mib fa |
    sib,
  }
>>
%%
sib sib |
la la la |
sol sol sol |
fa fa fa |
sib, sib sib |
la la la |
sol sol sol |
fa fa fa |
sib, sib fa |
sol sol mib |
fa fa re |
sol mib fa |
sib, sib fa |
sol sol mib |
fa fa re |
sol mib fa |
sib,
%%
<<
  \tag #'basse { r4 r | R2.*23 | r4 r }
  \tag #'basse-continue {
    sib4 sib |
    la2 la4 |
    sol re mib |
    fa fa fa, |
    sib, sib sib |
    la2 la4 |
    sol re mib |
    fa fa fa, |
    sib, r r |
    sib la sol |
    fa mib re |
    do la, sib, |
    fa do re |
    mib8 re do4 sib, |
    fa mib re |
    mib fa fa, |
    sib, r r |
    sib la sol |
    fa mib re |
    do la, sib, |
    fa do re |
    mib8 re do4 sib, |
    fa mib re |
    mib fa fa, |
    sib,4~ sib,
  }
>>
%%
sib4 |
sib2 mib4 |
sib,2 sib,4 |
fa2 fa,4 |
sib,2 sib4 |
sib2 mib4 |
sib,2 sib,4 |
fa2 fa,4 |
sib, r fa |
fa2 sib,4 |
fa,2 fa4 |
sib la sol |
fa mib8 re do sib, |
la,4 fa, fa |
sib2 fa4 |
sib mib fa |
sib, r fa |
fa2 sib,4 |
fa,2 fa4 |
sib la sol |
fa mib8 re do sib, |
la,4 fa, fa |
sib2 fa4 |
sib mib fa |
sib,
%%
<<
  \tag #'basse { r4 r | R2.*23 | r4 }
  \tag #'basse-continue {
    r4 r |
    r sib, fa |
    re2 mib4 |
    sib, la, sib, |
    fa r r |
    r sib, fa |
    re2 mib4 |
    sib, mib, fa, |
    sib, r r |
    r sib sib, |
    mib8 re do4 sib, |
    fa fa mib |
    re r r |
    mib fa sol8 la |
    sib la sol4 fa |
    do' do fa |
    sib, r r |
    r sib, fa |
    re2 mib4 |
    sib, la, sib, |
    fa4 r r |
    r sib, fa |
    re2 mib4 |
    sib, mib, fa, |
    sib,
  }
>>
%%
r4 r |
R2. |
r4 sib, do |
re do sib, |
fa r r |
fa r r |
sib,2 do4 |
re sib, do |
fa, do re |
mib re mi |
fa mib! re |
mib fa fa, |
sib, do re |
mib re mi |
fa mib! re |
mib fa fa, |
sib, r r |
r <>^"Basses de violons" sib sib |
sib sib, sib |
mib2 fa4 |
sib, r r |
r sib sib |
sib sib, sib |
mib2 fa4 |
sib, <>^"[Tous]" sib sib |
sib4. do'8 sib lab |
sol4 mib sol |
lab fa sib |
mib2 r4 |
mib2 fa8 mib |
re2 sol8 fa |
mib2 mib4 |
fa fa fa |
fa4. sol8 fa mib |
re4 sib, re |
mib fa fa, |
sib, fa fa |
fa4. sol8 fa mib |
re4 sib, re |
mib fa fa, |
sib,2
