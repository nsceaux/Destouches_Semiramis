\clef "dessus" r4 r |
R2.*15 |
r4 re'' sib' |
do'' re''16 do''8. sib'16 la'8. |
sib'4 sol' mib'' |
mib'' fa''16 mib''8. re''16 do''8. |
re''4 re'' sib' |
do'' re''16 do''8. sib'16 la'8. |
sib'4 sol' mib'' |
mib'' fa''16 mib''8. re''16 do''8. |
re''4 fa'' mib'' |
re'' sib' mib'' |
do'' la' re'' |
sib' do'' la' |
sib' fa'' mib'' |
re'' sib' mib'' |
do'' la' re'' |
sib' do'' la' |
sib' r r |
R2.*23 |
r4 <<
  \twoVoices #'(dessus1 dessus2 dessus) <<
    { re''8 mib'' fa'' sol'' |
      fa''4 re'' sol'' |
      fa'' sol''8 fa'' mib'' re'' |
      do'' sib' do'' re'' mib'' re''16 do'' |
      re''4 re''8 mib'' fa'' sol'' |
      fa''4 re'' sol'' |
      fa'' sol''8 fa'' mib'' re'' |
      do'' sib' do'' re'' mib'' re''16 do'' |
      re''4 do''8 re'' mib'' re'' |
      do''4 la' sib' |
      do'' la' do'' |
      re'' mib''8 re'' do'' sib' |
      fa''2.~ |
      fa''4 mib''8 re'' do'' mib'' |
      re''4 sib' do'' |
      re''4. mib''8 do''4\trill |
      sib'4 do''8 re'' mib'' re'' |
      do''4 la' sib' |
      do'' la' do'' |
      re'' mib''8 re'' do'' sib' |
      fa''2.~ |
      fa''4 mib''8 re'' do'' mib'' |
      re''4 sib' do'' |
      re''4. mib''8 do''4 |
      sib' }
    { sib'8 do'' re'' mib'' |
      re''4 sib' mib'' |
      re'' mib''8 re'' do'' sib' |
      la' sol' la' sib' do'' sib'16 la' |
      sib'4 sib'8 do'' re'' mib'' |
      re''4 sib' mib'' |
      re'' mib''8 re'' do'' sib' |
      la' sol' la' sib' do'' sib'16 la' |
      sib'4 la'8 sib' do'' sib' |
      la'4 fa' sol' |
      la' fa' la' |
      sib' fa' sib' |
      la' la'8 sib' do'' re'' |
      do''4 la' la' |
      sib' fa' la' |
      sib'4. do''8 la'4 |
      sib'4 la'8 sib' do'' sib' |
      la'4 fa' sol' |
      la' fa' la' |
      sib' fa' sib' |
      la' la'8 sib' do'' re'' |
      do''4 la' la' |
      sib' fa' la' |
      sib'4. do''8 la'4 |
      sib'4 }
  >>
>> r4 r |
R2.*23 |
r4 sib' do'' |
re''\trill do'' re'' |
mib'' re'' mib'' |
fa'' sol''8 fa'' mib'' re'' |
do''4\trill la' sib' |
do'' sib' do'' |
re'' do'' sib' |
la'4. sib'8 sol'4\trill |
fa' lab' lab' |
sol' sib' sib' |
sib' la' re'' |
do''16 sib'8 do''16 do''4.\trill sib'8 |
sib'4 lab' lab' |
sol' sib' sib' |
sib'\trill la' re'' |
do''16 sib'8 do''16 do''4.\trill sib'8 |
sib'4 r r |
R2.*7 |
r4 re'' mib'' |
fa''4. mib''8 fa'' re'' |
sol''4 sol'' sol'' |
mib'' fa'' re'' |
mib'' r r |
R2.*3 |
r4 do'' re'' |
mib''4. re''8 mib'' do'' |
fa''4 re'' fa'' |
sol'' do''4.\trill sib'8 |
sib'4 do'' re'' |
mib''4. re''8 mib'' do'' |
fa''4 re'' fa'' |
sol'' do''4.\trill sib'8 |
sib'2

