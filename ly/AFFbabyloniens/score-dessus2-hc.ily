\score {
  <<
    \new Staff \with { \haraKiriFirst } <<
      { \startHaraKiri s2 s2.*55 s4
        \stopHaraKiri s2 s2.*23 s4 \startHaraKiri }
      \global \keepWithTag #'dessus2 \includeNotes "dessus"
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \includeNotes "haute-contre" \clef "dessus"
      { s2 s2.*32\break s2.*48\break }
    >>
  >>
  \layout { }
}
