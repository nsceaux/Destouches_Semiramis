\key sib \major \midiTempo#160
\digitTime\time 3/4 \partial 2
\beginMark "Air" s2 s2.*15 s4 \bar "||"
\beginMark "Chœur" s2 s2.*15 s4 \bar "||"
\beginMark "Air" s2 s2.*23 s4 \bar "||"
\beginMark "Violons" s2 s2.*23 s4 \bar "||"
\beginMark "Air" s2 s2.*23 s4 \bar "||"
\beginMark "Chœur" s2 s2.*39 s2 \bar "|."
