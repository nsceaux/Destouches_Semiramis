\amestrisMark r4 r la' |
la'2 sib'4 |
sib'2 la'8 la' |
re''2 dod''8 re'' |
\appoggiatura re''8 mi''2 mi''4 |
la'4. sol'8 sol' la' |
fad'2 sol'8 la' |
la'2\trill sol'8 la' |
\appoggiatura la'8 sib'4 sib' r8 sib' |
\appoggiatura do'' si'! si' r si'16 si' dod''8 re'' |
dod''2 la'8 la' |
fa''2 fa''8 fa'' |
si'2 dod''8 re'' |
re''2 re''8 dod'' |
re''2 re''4 |
