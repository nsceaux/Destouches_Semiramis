\clef "basse" re2. |
re' |
do' |
sib |
la |
dod |
re |
fad, |
sol,2 sol4 |
fa4. fa8 mi re |
la2. |
re |
sol |
fa8 sol la4 la, |
re2. |
