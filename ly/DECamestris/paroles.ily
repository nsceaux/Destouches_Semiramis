J’im -- mole aux Dieux le prin -- tems des mes jours ;
A l’om -- bre des Au -- tels a -- vec vous je vais vi -- vre :
Heu -- reu -- se, si vô -- tre se -- cours
De mes trou -- bles se -- crets pour ja -- mais me dé -- li -- vre !
