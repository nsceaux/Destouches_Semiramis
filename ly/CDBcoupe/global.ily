\key sib \major
\tempo "Gravement" \midiTempo#120
\time 2/2 \partial 2 s2 \set Score.currentBarNumber = 2
\bar ".!:" s1*11 \alternatives s1 s1
\time 6/8 \tempo "Vîte" \bar ".!:" s2.*20 \alternatives s2. s2. \bar "|."
