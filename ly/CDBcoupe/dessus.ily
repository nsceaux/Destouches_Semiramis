\clef "dessus" re''4. re''8 |
re''4. do''16 re'' mib''2~ |
mib''4. re''8 re''4 mib''8 fa'' |
la'2\trill r4 fa''32 sol'' fa'' mib'' re'' do'' sib' la' |
sol'4~ sol'32 sol' la' sib' do'' re'' mib'' fa'' sol''4. sol''8 |
sol''4. fa''8 fa''4 mi''8 fa'' |
mi''4.*11/12 do''32 re'' mi'' fa'' sol'' lab''2~ |
lab''4.*13/12 lab''32 sol'' fa'' sol''2~ |
sol''4 lab''8 sol'' fa''2~ |
fa''4.*13/12 sol''32 re'' mib'' mi''4.\trill fa''8 |
fa''8 mib''32 re'' do'' sib' la' sol' fa' sol' la' sib' do'' re'' mib''2~ |
mib''4.*13/12 fa''32 do'' re'' re''4.\trill do''8 |
do''2 re''4. re''8 |
do''1 |
fa''8 do'' fa'' re'' mib''16 re'' do'' sib' |
la'4 la'16 la' sib'4 r8 |
do''16 sib' do'' re'' mib'' do'' re''8 sib' re''16 re'' |
mib''4 r8 fa''16 mib'' fa'' sol'' lab'' fa'' |
sol''8 mib'' sol'' sol'' fa''16 mib'' re'' do'' |
si'4 r8 do''4 r8 |
re''16 do'' re'' mib'' fa'' re'' mib'' re'' mib'' fa'' sol'' mib'' |
lab'' sol'' fa''8.\trill mib''16 re''4\trill do''8 |
do''16 sib' do'' re'' do'' sib' la'8 fa' la' |
sib' fa' fa'' re'' sol''4 |
mi''4 r8 fa''4 r8 |
sol''4 r8 la''16 sol'' fa'' sol'' la'' sib'' |
sol''4 fa''8 sib'' fa'' sib'' |
sol''8 lab''16 sol'' fa'' mib'' re'' mib'' mib''8.\trill re''16 |
do'' re'' re''8.\trill do''16 sib' la' sib' do'' re'' mib'' |
do''4\trill sib'8 do''4 r8 |
re''16 do'' sib' do'' re'' sib' mib'' re'' do'' re'' mib'' do'' |
fa'' mib'' re'' mib'' fa'' re'' sol''8. sol''16 la'' sib'' |
la''4.\trill( \grace { \stemDown sol''32 la'') } sib''16 la'' sol'' fa'' mib'' re'' |
sol'' fa'' mib'' re'' do'' la' sib' do'' do''8.\trill sib'16 |
sib'4.~ sib'4. |
sib'2. |
