\clef "haute-contre" sib'4. sib'8 |
sib'4. la'16 sib' do''2~ |
do''4. do''8 sib'2 |
mib' fa' |
mib'4. sib'8 re''2~ |
re''4 do'' si'4. do''8 |
do''2. do''4 |
re''2~ re''4.*5/6 re''16 do'' sib' |
do''2. re''8 do'' |
sib'4. sib'8 sol'4.\trill fa'8 |
fa'2 sol'4 sol' |
do''4.*13/12 do''16*1/2 la' sib' sib'4.\trill la'8 |
la'2 sib'4. sib'8 |
la'1 |
R2. |
fa'4 fa'16 fa' fa'4 r8 |
la'16 sol' la' sib' do'' la' sib'4 sib'16 sib' |
sib'4 r8 sib'4 sib'16 sib' |
sib'4 r8 la'4 r8 |
sol'4 r8 sol'4 r8 |
sol'4 r8 sol'4 do''8 |
do'' re''8. do''16 si'4 do''8 |
r2*3/4 fa'8 do' fa' |
fa'4 sib'8 sib' re''4 |
do''4 r8 do''4 r8 |
do''4 r8 do''16 sib' la' sib' do'' re'' |
do''4 do''8 re''4 r8 |
re''4 do''8 si'4 do''8 |
la'4 sib'8 sol'16 fa' sol' la' sib' do'' |
la'4 sib'8 fa'4 r8 |
fa'4 r8 sib'4 r8 |
la'4 r8 sol'4 r8 |
do''4 r8 sib'4 r8 |
sib'4 fa'8 sib' la'8.\trill sib'16 |
sib'4.~ sib' |
sib'2. |

