\clef "taille" fa'4. fa'8 |
fa'2 sol' |
fa' fa' |
do' sib |
sib4. sol'8 sol'4 fa' |
mib' lab' sol'4. sol'8 |
sol'2. fa'4 |
fa'2 sib4. mib'8 |
mib'2 lab'4. re'8 |
re'2 do'4. do'8 |
la4. do'8 do'4 do' |
fa'2 fa'4. fa'8 |
fa'2 fa'4. fa'8 |
fa'1 |
R2. |
do'4 do'16 do' re'4 r8 |
mib'4. re'8 fa'8. fa'16 |
sol'4 r8 lab'4 r8 |
sol'4 r8 do'4 fa'8 |
re'4 r8 mib'4 r8 |
fa'4 r8 mib'4 sol'8 |
sol' fa'8. lab'16 sol'4 sol'8 |
r2*3/4 do'8 do' do' |
re'4 re'8 re' sib sib' |
sol'4 r8 la'4 r8 |
sib'4 r8 la'4 r8 |
mi'4 la'8 fa'4 r8 |
sib'4 lab'8 sol'4 mib'8 |
mib'4 re'8 mib'4 fa'8 |
fa'4 fa'8 mib'4 r8 |
re'4 r8 sol'4 r8 |
do'4 r8 sib4 r8 |
mib'4 r8 re'4 r8 |
mib'4 fa'8 fa' fa'8. mib'16 |
re'4.~ re' |
re'2. |
