\newBookPart #'()
\act "ACTE V"
\sceneDescription\markup\wordwrap-center {
  Le Théâtre représente le Tombeau de \smallCaps Ninus,
  Roy de Babylone : il est au milieu d’une Forest.
}
\scene "Scene Premiere" "Scene I"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Zoroastre, Semiramis.
}
% 5-1
\pieceToc "Ritournelle"
\includeScore "EAAritournelle"
\newBookPart #'(full-rehearsal)
% 5-2
\pieceToc\markup\wordwrap {
  Zoroastre, Semiramis :
  \italic { Quoy ! la mort d’Amestris s'aprête dans ces lieux }
}
\includeScore "EABrecit"
\newBookPart #'(full-rehearsal)

\scene "Scene Deuxiéme" "Scene II"
\sceneDescription\markup\wordwrap-center {
  \smallCaps { L'Ordonnateur, }
  Peuples du Babylone, qui viennent rendre hommage au Tombeau de Ninus.
}
% 5-3
\pieceToc\markup\wordwrap {
   L’Ordonnateur, chœur :
  \italic { Au plus grand de nos Rois adressons nôtre hommage }
}
\includeScore "EBAordoChoeur"
\newBookPart #'(full-rehearsal)
% 5-4
\pieceToc "Premier air"
\includeScore "EBBair"
\newBookPart #'(full-rehearsal)
% 5-5
\pieceToc\markup\wordwrap {
   L’Ordonnateur :
  \italic { Fille de la Valeur, immortelle Victoire }
}
\includeScore "EBCordonnateur"
\newBookPart #'(full-rehearsal)
% 5-6
\pieceToc "Deuxiéme air"
\includeScore "EBDair"
\newBookPart #'(full-rehearsal)

\scene "Scene Troisiéme" "Scene III"
\sceneDescription\markup\wordwrap-center {
  \smallCaps { Semiramis ; }
  Et les Acteurs de la Scene précédente.
}
% 5-7
\pieceToc\markup\wordwrap {
   Semiramis : \italic { Cessez. Ninus reçoit vos vœux & vôtre zele }
}
\includeScore "ECArecit"

\scene "Scene Quatriéme" "Scene IV"
\sceneDescription\markup\wordwrap-center {
  \smallCaps { Amestris, Semiramis, }
  \line { Et les Acteurs de la Scene précédente. }
}
% 5-8
\pieceToc\markup\wordwrap {
   Amestris : \italic { Peuples, qui de Ninus honorez la memoire }
}
\includeScore "EDAamestris"
\newBookPart #'(full-rehearsal)

\scene "Scene Cinquiéme" "Scene V"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Arsane, Amestris, Semiramis, Chœurs.
}
% 5-9
\pieceToc\markup\wordwrap {
  Arsane, Amestris, Semiramis :
  \italic { Ah ! Princesse, arrêtez }
}
\includeScore "EEArecitArsane"
% 5-10
\pieceToc\markup\wordwrap {
  Chœur : \italic { Secourez-nous, ô Dieux ! frappez qui vous offense }
}
\includeScore "EEBchoeur"
\newBookPart #'(full-rehearsal)

\scene "Scene Sixiéme" "Scene VI"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Amestris, & le Chœur.
}
% 5-11
\pieceToc\markup\wordwrap {
  Amestris, Chœur : \italic { Dieux, prenez sa deffense }
}
\includeScore "EFAchoeur"
\newBookPart #'(full-rehearsal)

\scene "Scene Septiéme" "Scene VII"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Arsane, Amestris.
}
% 5-12
\pieceToc\markup\wordwrap {
  Arsane : \italic { Vous vivrez, ma Princesse }
}
\includeScore "EGArecit"

\scene "Scene Derniere" "Scene VIII"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Arsane, Semiramis, Zoroastre, Amestris, Chœur.
}
% 5-13
\pieceToc\markup\wordwrap {
  Arsane, Zoroastre, Semiramis :
  \italic { Grands Dieux ! elle paroît, que mon cœur est glacé }
}
\includeScore "EHArecit"
