\clef "basse" R2. |
do'2 do4 |
fa2 fa4 |
sib2 sib,4 |
mib2 mib4 |
lab fa2 |
sol4. fa8 mib\trill re |
mib4 fa sol |
do2 do4 |
fa2 fa4 |
sib2 sib,4 |
mib2 mib4 |
fa2 fa4 |
sol2 sol4 |
lab2 sol8 fa |
sol2 sol4 |
mib fa sol |
do2 fa8 mib |
re4 sol sol, |
do2 r4 |
do'2 do4 |
fa2 fa4 |
sib2 sib,4 |
mib2 mib4 |
lab2 re4 |
sol2 do4 |
fa2 sol4 |
si,2 do4 |
sol8 fa sol la si sol |
do'2 do'4 |
si2 sib4 |
la re' re |
sol2 sol4 |
fad2 fa4 |
mi la la, |
re2 sib,4 |
la, re re, |
sol,2 sol,4 |
do2 do4 |
si,2 si,4 |
do2 do4 |
re2 re4 |
mib2 mib4 |
fa2 fa4 |
sol2 do4 |
fa2 sol8 fa |
mib4 fa sol |
lab sol fa |
sol sol,2 |
do2. |
do'2 do'4 |
do'4 si8^"egales" sol do' sib? |
lab2. |
sol8 fa sol la si sol |
do si, do re mib do |
re do re mib re do |
sib,4 do re |
sol, sol8 la si sol |
do' sol lab sol fa mib |
sib,4 sib8 lab sol lab16 sol |
fa8 mib sib8 lab sib sib, |
mib fa sol lab sib lab |
sol fa do' sib lab sol |
fa sol lab sol fa mib |
re do si, la, si, sol, |
do fa, sol,2 |
do r4 |
sol8 lab sol fa mib re |
do2 do4 |
sib,2 sib,4 |
lab,2 fa,4 |
sol,2 sol4 |
lab sol fa |
mib mib, mib |
mib re mib |
sib8 lab sib do' re' sib |
mib'4 re' do' |
sib8 do' sib lab sol mib |
lab sol lab sib do' lab |
sib4 sib,2 |
mib mib4 |
re mi do |
fa8 sol fa mib re do |
fa, mib, fa, sol, lab, fa, |
sol, sol fa sol fa mib |
re do re mib fa sol |
mib\trill re do re mib fa |
sol lab sol fa mib fa |
sol4 sol,2 |
do do8 re mib do |
fa4 fa8 sol lab4 fa |
sol4 sol8 lab sol4 sol, |
la, si, do re |
mib4. lab16 sol fa4 mib |
sib sib, mib fa |
sol fa mib re |
do2 fa4 fa, |
sib,8 la, sib, sol, do4 sib, |
la, sol, re re'8 do' |
si la sol fa mib re mib do |
fa4 fa8 sol lab4 fa |
sol fa8 mib re4 mib8 fa |
sol lab sol fa mib re mib do |
fa2 sol4 sol, |
do2 si, sol, |
lab, la, |
sib, sib4 lab sol2 |
lab2 fa |
sib4. sol8 lab sol fa mib sib4 sib, |
mib2 si, sib,2 |
la,4. si,8 do4 do, |
sol,2 sol4 fa |
mib2 re4 do |
si,2 do4 fa8 mib re4 do |
sol2 si1 | \allowPageTurn
do'2 lab1 |
sib2 sib, |
fa reb4 mib fa fa, |
sib,4 sib8 lab sol2 |
lab8 sol fa mib sib4 sib, |
mib1 |
re2 do mi |
fa lab |
sol fa |
mib1 |
re2 do |
sib,1 |
mib1 |
lab2 mi |
fa2. |
do4 sib, lab, |
sol,2. |
fad, |
sol,4 sol sib2 |
lab2 fa |
sib sol |
lab8 sol fa mib sib4 sib, |
mib2 mi fa4 re |
mib2. do4 sol do |
sol,2 sol,16 fa, sol, lab, sol, lab, sol, fa, |
mib,2 mib |
sol, sol |
lab1 |
sol2 sib |
la sib |
la16[ sib lab sol] fa[ mib re do] sib,1 |
do2 re |
mib1 |
lab2. fa4 |
fa2 mib re4 do |
sol1 |
fad2 fa |
mib2. re4 |
do fa, sol,2 |
do do'1 |
si |
la |
sol2
