\tag #'(arsane basse) { Non, ne crai -- gnez }
\tag #'(amestris basse) {
  Non, je ne veux pas vous en -- ten -- dre,
  Non, non, je ne veux pas vous en -- ten -- dre,
  Non, non, je ne veux pas vous en -- ten -- dre :
}
\tag #'arsane {
  pas de m’en -- ten -- dre,
  Non, non, ne crai -- gnez pas de m’en -- ten -- dre,
  Non, non, ne crai -- gnez pas de m’en -- ten -- dre :
}
\tag #'(amestris basse) {
  Les Dieux sont en cou -- roux, son -- gez à les cal -- mer.
}
\tag #'(arsane basse) {
  C’est vous que je dois dé -- sar -- mer ;
  J’ai trop de gra -- ces à leur \tag #'arsane { ren -- dre. }
}
\tag #'(amestris basse) {
  Non, je ne veux pas vous en -- ten -- dre,
  Non, non, non,
  Non, je ne veux pas vous en -- ten -- dre,
  Non, non, non, non, je ne veux pas vous en -- ten -- dre.
}
\tag #'arsane {
  Non, ne crai -- gnez pas de m’en -- ten -- dre,
  Non, non, ne crai -- gnez pas de m’en -- ten -- dre,
  Non, non, non, ne crai -- gnez pas de m’en -- ten -- dre.
}
\tag #'(arsane basse) {
  Dois-je é -- prou -- ver en -- cor vôtre in -- jus -- te ri -- gueur,
  Quand le Ciel a -- vec moi pa -- roît d’in -- tel -- li -- gen -- ce ?
  Vou -- lez- vous ban -- nir l’es -- pe -- ran -- ce,
  Qu’il vient ra -- me -- ner dans mon cœur ?
  Vou -- lez- vous ban -- nir l’es -- pe -- ran -- ce,
  Qu’il vient ra -- me -- ner dans mon cœur ?
}
\tag #'(amestris basse) {
  Pou -- vez- vous per -- dre sans al -- lar -- mes,
  L’at -- ten -- te d’un sort é -- cla -- tant ?
  Pour chan -- ger vô -- tre cœur ne faut- il qu’un ins -- tant ?
  Et la gloi -- re pour vous n’a- t’el -- le plus de char -- mes ?
  Pour chan -- ger vô -- tre cœur ne faut- il qu’un ins -- tant ?
  Et la gloi -- re pour vous n’a- t’el -- le plus de char -- mes ?
}
\tag #'(arsane basse) {
  Non, mon cœur n’a ja -- mais chan -- gé :
  A ses pre -- miers dé -- sirs, il fut tou -- jours fi -- del -- le ;
  Vos yeux n’ont- ils pas vû ma con -- train -- te mor -- tel -- le,
  Et l’hor -- reur où j’é -- tois plon -- gé ?
  Non, mon cœur n’a ja -- mais chan -- gé !
  Non, non, non, mon cœur n’a ja -- mais chan -- gé !
  Mais, nous n’a -- vez rien vû, Cru -- el -- le que vous ê -- tes ?
  A -- mes -- tris, in -- sen -- sible à mes pei -- nes se -- cre -- tes,
  Crai -- gnoit d’en sus -- pen -- dre le cours :
  A -- mes -- tris, in -- sen -- sible à mes pei -- nes se -- cre -- tes,
  Dé -- tour -- noit des re -- gards que je cher -- chois toû -- jours...
  Eh ! vous me les ca -- chez en -- co -- re ?
}
\tag #'(amestris basse) {
  Un au -- gus -- te ser -- ment doit en -- ga -- ger ma foi.
}
\tag #'(arsane basse) {
  Quel est donc ce ser -- ment ;
}
\tag #'(amestris basse) {
  Je ne suis plus à moi.
}
\tag #'(arsane basse) {
  Ex -- pli -- quez- vous. Cal -- mez l’hor -- reur qui me dé -- vo -- re.
}
\tag #'(amestris basse) {
  Pour la der -- nie -- re fois re -- ce -- vez mes a -- dieux ;
  Ne sui -- vez plus mes pas, c’est un soin i -- nu -- ti -- le.
}
\tag #'(arsane basse) {
  Où fuy -- ez- vous ?
}
\tag #'(amestris basse) {
  Au Temple, & c’est là mon a -- zi -- le.
  Par des nœuds é -- ter -- nels, je vais m’u -- nir aux Dieux.
}
\tag #'(arsane basse) {
  Et moi je vous dis -- pute à ces Ri -- vaux ter -- ri -- bles ;
  Et vous me trou -- ve -- rez en -- tre l’Au -- tel & vous.
}
\tag #'(amestris basse) {
  Ah ! Sei -- gneur, é -- touf -- fez un im -- puis -- sant cou -- roux.
  Aux pro -- fa -- nes Mor -- tels ces lieux i -- nac -- ces -- si -- bles,
  Ont en dé -- pôt la fou -- dre, en -- ten -- dez ces é -- clats :
  Je la sus -- pens en -- cor par mon o -- bé -- is -- san -- ce :
  Crai -- gnez ces Dieux, trem -- blez & ne me for -- cez pas
  D’im -- plo -- rer con -- tre vous leur ter -- ri -- ble ven -- gean -- ce.
}
\tag #'(arsane basse) {
  Ah ! dûs -- sai-je y trou -- vez le plus cru -- el tré -- pas
  Je ne souf -- fri -- rai point...
}
