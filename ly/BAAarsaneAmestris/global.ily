\beginMark "Ritournelle" \key sol \minor
\tempo "Legerement" \midiTempo#120
\digitTime\time 3/4
\tag #'complet {
  s2.*89
  \digitTime\time 2/2 \midiTempo#160 s1*15
  \time 3/2 s1.
  \time 2/2 s1
  \time 3/2 s1.
  \time 2/2 \grace s8 s1
  \time 3/2 s1.*2
  \time 2/2 \grace s8 s1*3
  \time 3/2 s1.*3
  \time 2/2 s1
  \time 3/2 \grace s8 s1.
  \time 2/2 s1*3
  \time 3/2 s1.
  \time 2/2 s1*7
  \digitTime\time 3/4 \grace s8 s2.*4
  \time 2/2 s1*4
  \time 3/2 s1.*2
  \time 2/2 \grace s8 s1*6
  \time 3/2 s1.
  \time 2/2 s1*3
  \time 3/2 s1.
  \time 2/2 s1*4
  \key do \major \time 3/2 s1.
  \time 2/2 s1*2
  \time 3/2 s2 \bar ""
}
