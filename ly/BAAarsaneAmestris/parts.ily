\piecePartSpecs
#`((dessus #:score "score-dessus")
   (dessus2-hc #:tag-notes part #:tag-global part)
   (basse-continue #:score-template "score-basse-continue-voix")
   (chant)
   (silence #:on-the-fly-markup , #{ \markup\tacet#158 #}))
