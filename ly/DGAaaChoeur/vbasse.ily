\clef "vbasse" R1*2 R1. R1*3 R1.*2 R2. |
r4 r sol8 sol |
fa2 fa8 sol |
mib2 sol8 sol |
do'4. do'8 do4 |
sol2 sol4 |
R2. R1. R1*4 |
