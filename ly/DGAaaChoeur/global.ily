\key re \minor
\digitTime\time 2/2 \midiTempo#160 s1*2
\time 3/2 s1.
\digitTime\time 2/2 s1*3
\time 3/2 s1.*2
\digitTime\time 3/4 s2.*6 \bar "|."
\midiTempo#80 s2.
\time 3/2 \midiTempo#160 s1.
\time 2/2 \midiTempo#80 s1*4 \bar "|."
