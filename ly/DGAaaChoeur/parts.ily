\piecePartSpecs
#`((dessus)
   (dessus-hc)
   (haute-contre)
   (taille)
   (basse)
   (basse-continue #:score-template "score-basse-continue-voix")
   (chant)
   (silence #:on-the-fly-markup , #{ \markup\tacet#20 #}))
