\tag #'(arsane basse) {
  Quoi ! tout me fuit, tout m’a -- ban -- don -- ne !
  Quel pres -- tige a gla -- cé le cœur de mes Sol -- dats !
  Je les ex -- cite en -- vain à mar -- cher sur mes pas :
  Mais, quel trou -- ble nou -- veau, quelle hor -- reur m’en -- vi -- ron -- ne !
}
\tag #'choeur {
  A -- mes -- tris va pe -- rir : c’est le Ciel qui l’or -- don -- ne.
}
\tag #'(arsane basse) {
  Dé -- ro -- bez- moi les pleurs que vous m’of -- frez ;
  Peu -- ples, é -- loi -- gnez- vous : Ar -- sa -- ne, de -- meu -- rez.
}
