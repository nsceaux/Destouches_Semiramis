\clef "haute-contre" R1*2 R1. R1*3 R1.*2 R2. |
r4 r sol'8 sol' |
lab'2 lab'8 sol' |
sol'2 sol'8 sol' |
sol'4. sol'8 sol'4 |
sol'2 sol'4 |
R2. R1. R1*4 |
