\arsaneMarkText Vivement sol'2 r4 sib8 sol |
re'2 sol'4 mib'8 do' |
la4\trill la r sib8 do' re'4\trill re'8 mib' |
fa'4. fa'8 sol' sol' sol' sol' |
do'4. fa'8 fa' fa' do' re' |
mib'4 mib'8 sol' mib'4\prall mib'8 re' |
re'2\trill la4 r8 la re'4 re'8 re' |
sol2 r4 re'8 sol' mib'4 mib'8 mib' |
si2\trill si4 |
R2.*5 |
\amestrisMarkText "au Peuple" r4 r8 re'' mib'' fa'' |
mib''4.\trill\prall re''8 do''4 sib' la' sib' |
fad'2\trill la'8 la'16 la' la'8 la' |
re''2 r4 sib' |
sol'\trill\prall sol' r sol'8 fad' |
sol'1 |
