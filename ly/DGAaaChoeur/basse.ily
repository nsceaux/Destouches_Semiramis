\clef "basse"
<<
  \tag #'basse { R1*2 R1. R1*3 R1.*2 R2. | r4 r }
  \tag #'basse-continue {
    sol,1 |
    si,2. do4 |
    re2 sol1 |
    re2 mib |
    fa2~ fa8 fa mib re |
    do2 la, |
    sib, re1 |
    mib2 si, do |
    sol,2.~ |
    sol,2
  }
>>
sol8 sol |
fa2 fa8 sol |
mib2 sol8 sol |
do'4. do'8 do4 |
sol2 sol4 |
<<
  \tag #'basse { R2. R1. R1*4 | }
  \tag #'basse-continue {
    si,2. |
    do1. |
    re2 do |
    sib,1 |
    mib4 do re re, |
    sol,1 |
  }
>>