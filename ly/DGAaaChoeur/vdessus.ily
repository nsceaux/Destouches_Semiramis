\clef "vdessus" R1*2 R1. R1*3 R1.*2 R2. |
<>^\markup\character Chœur
r4 r4 re''8 re'' |
re''2 re''8 re'' |
sol''2 re''8 re'' |
mib''4. sol'8 la'4 |
sib'2 sib'4 |
R2. R1. R1*4 |
