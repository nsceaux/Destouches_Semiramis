\clef "vhaute-contre" r8 re' mi'! fad' sol'4. mib'8 |
la8 la re' do' sib4 la8 sol |
re'2 fad' re'8 re' re' re' |
si4 si8 si do'4 do'8 re' |
\appoggiatura re' mib' mib' mib' fa' re'4\trill re'8 fa' |
sib4 mib'8 sol' do'4 re'8 mib' |
re'4\trill re' r re'8 re' sol4 sib8 do' |
re'4 re'8 mi' fa'4 fa'8 sol' |
mi'2 fad'4 sol'8 la' |
re'4. mi'8 fad'4. sol'8 sol'4.( fad'8) |
sol'1 |
