\clef "basse" sol,2. sol4 |
fad2 sol4 fa8 mib |
re1. |
sol4 fa mib re |
do2 sol4 re |
mib2 fa4 fa, |
sib,1 mib2 |
sib, si, |
do1 |
sib,2 la,4. sol,8 re4 re, |
sol,1 |
