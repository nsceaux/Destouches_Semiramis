\key la \minor \midiTempo#144
\beginMark "Rondeau" \tempo "Doux & legerement"
\digitTime\time 3/4 \partial 2
s2 s2.*7 s4 \bar "|." \fineMark
s2 s2.*7 s4 \bar "|." \endMark "au Rondeau"
s2 s2.*11 s4 \bar "|." \endMark "au Rondeau, jusqu’au mot fin."