\clef "haute-contre" la'4 la' |
la' dod'' re'' |
dod'' re'' la' |
la'2 sol'4 |
la' la' la' |
la' dod'' re'' |
mi'' \appoggiatura re''8 dod''4 \appoggiatura si'8 la'4 |
la'8 re'' dod''2 |
re''4 re'' do'' |
do'' sib' sib' |
la' do'' do'' |
sib'2 la'4 |
sol' la' sib'8.(\trill la'32 sib') |
do''4 fa' sol' |
la' sib' la' |
re'' do''4. do''8 |
do''4 %{%} re'' la' |
sib' fa' mi' |
fa'2 sol'4 |
fa' fa' sol' |
la' la' la' |
sib' fa' mi' |
fa'2 sol'4 |
fa' fa' sol' |
la' re'' re'' |
re'' re'' do'' |
re''2 dod''4 |
mi'' re''4. mi''8 |
dod''4
