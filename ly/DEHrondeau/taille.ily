\clef "taille" re'4 mi' |
re' la' la' |
sol' la' sol' |
fa' la' sib' |
mi' re' mi' |
re' la' la' |
sol' la' la' |
la' la'4. mi'8 |
fa'4 la' sib' |
fa' fa' sol' |
fa' sib la |
do' do'4. do'8 |
do'4 do' mi' |
fa' fa' sib |
la do' do' |
fa' mi'4.\trill fa'8 |
fa'4 %{%} fa' do' |
fa' fa' sol' |
do'2 do'4 |
do' sib sib' |
fa' re' mi' |
fa' fa' sol' |
do'2 do'4 |
do' sib sib' |
la' fa' sol' |
la' sol' fad' |
sol' sib' la' |
la' la'4. la'8 |
la'4
