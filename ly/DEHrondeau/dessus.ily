\clef "dessus" <>_\markup\whiteout "Violons" fa''4 mi'' |
fa'' mi''\trill re'' |
sol'' fa''\trill mi'' |
\appoggiatura mi''8 fa''4. mi''8 re''4 |
dod'' fa'' mi'' |
fa'' mi''\trill re'' |
sol'' \appoggiatura fa''8 mi''4 \appoggiatura re''8 dod''4 |
re''8 mi'' mi''2\trill |
re''4
fa''4 sol'' |
la'' re'' mi''\trill |
fa'' sol'' la'' |
sol''4.\trill la''8 fa''4 |
mi'' fa''8.( sol''16) sol''8.(\trill fa''32 sol'') |
la''4 re'' mi''\trill |
fa'' sol'' la''~ |
la''8 sib'' sol''4.(\trill fa''16 sol'') |
la''4
fa''4 mi'' |
re'' do''\trill sib' |
la'2 sib'4 |
la' re'' mi''\trill |
fa'' fa'' mi'' |
re'' do''\trill sib' |
la'2 sib'4 |
la' re'' mi''\trill |
fa'' la'' sib'' |
fad'' sol''8.( la''16) la''8.(\trill sol''32 la'') |
sib''4 \appoggiatura la''8 sol''4 \appoggiatura fa''8 mi''4 |
mi''8( fa''16 sol'') fa''4.\trill mi''8 |
mi''4
