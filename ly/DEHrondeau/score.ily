\score {
  \new StaffGroup <<
    \new Staff <<
      { s2 s2.*7 s4^\markup\fontsize#1 \italic Fin. }
      \global \includeNotes "dessus"
    >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff = "basse" <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s2.*8\break s2.*7 s4\break s2 s2.*8\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
