\clef "basse" <>^\markup\whiteout "Basse de violons" re4 dod |
re sol fa |
mi re dod |
re do sib, |
la, re dod |
re sol fa |
mi la sol |
fa8 sol la4 la, |
re
re4 mi |
fa sib, sol, |
re mi fa |
mi fa fa, |
do8 sib, la,4 sol, |
fa, sib, sol, |
re mi fa |
sib, do do, |
fa,
re'4 do' |
sib la sol |
fa2 mi4 |
fa sib sol |
re' re' do' |
sib la sol |
fa2 mi4 |
fa sib sol |
re' re' re' |
do' sib la |
<<
  { \voiceOne \forceStemLength#2 { sol2 la4 } | dod \oneVoice }
  \new CueVoice { \voiceTwo sol8_"[manuscrit]" fa mi re dod si, | la,4 }
>> re4 re, |
la,
