\clef "haute-contre" R1 |
sib'4. do''8 la'2 |
sol'4. la'16 sib' la'4.\trill sol'8 |
fad'2 sol'4. fad'8 |
sol'2 fa' |
fa'4. sib'8 do''4. re''8 |
re''4. dod''8 re''4. mi''8 |
dod''2 re'' |
R1 |
re''4. mib''8 do''2\trill |
sib'2 do''4. sib'8 |
la'2\trill sib' |
R1 |
re'2 re' |
re' re'4. fad'8 |
sol'2 fad' |
R1 |
sib'2 sib' |
la'1 |
la'2 la' |
sib' do'' |
sib'2. r8 la' |
sib'2 do''4. sol'8 |
la'4. sol'16 fad' sol'4. la'8 |
fad'2 sol' |
r4 r8 si' do''4. sol'8 |
la'4. sol'16 fad' sol'4. la'8 |
fad'2 sol' |
