\clef "taille" R1 |
sol'2 re' |
re'4. fa'8 mib'2 |
re' re'4. re'8 |
re'2 do'\trill |
sib4. sol'8 la'4. sib'8 |
la'2 re'4. re'8 |
la2 la |
R1 |
fa'2 fa' |
fa'4. sol'8 fa'4. fa'8 |
fa'2 fa' |
R1 |
la2 fad |
sol do'4. sib16 la |
sol2 la |
R1 |
sol'2 sol' |
mi' mi' |
re' fad' |
re' fad'\trill |
sol'2. r8 la' |
sol'2 la'4. sib'8 |
fad'4. re'8 re'4. mib'8 |
re'2 re' |
r4 r8 re' do'4. sol'8 |
fad'4. re'8 re'4. mib'8 |
re'2 re' |
