\clef "dessus" sol''2 sol'' |
re''4. mib''8 re''4. do''8 |
sib'4.\trill( do''16 re'') do''4. sib'8 |
la'2\trill sol'4. la'8 |
sib'4.( do''8) do''4.\trill( sib'16 do'') |
re''4. mi''8 fad''4. sol''8 |
fad''4. sol''16 la'' sol''4.\trill fad''8 |
mi''2\trill re'' |
sib''2 sib'' |
fa''4. sol''8 fa''4. mib''8 |
re''4.\trill( mib''16 fa'') mib''4.\trill re''8 |
do''2\trill sib' |
la' sib' |
fad'4. sol'8 la'4. sib'8 |
sol'4. sib'8 la'4. sib'16 do'' |
sib'2\trill la' |
la'' la'' |
mi''2.\trill r16 mi'' fa'' sol'' |
la''2 la'' |
fad''2\trill re''4. fad''8 |
sol''4. la''8 la''4.(\trill sol''16 la'') |
sib''2. r8 re'' |
sol''4. fa''8 mib''4. re''8 |
do''4.\trill( sib'16 la') sib'4. do''8 |
la'2\trill sol'4. re''8 |
sol''4. fa''8 mib''4. re''8 |
do''4.\trill( sib'16 la') sib'4. do''8 |
la'2\trill sol' |
