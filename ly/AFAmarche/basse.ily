\clef "basse" R1 |
sol2 fad |
sol4. re8 mib4. do8 |
re2 sol,4. re8 |
sol4.( la8) la4.(\trill sol16 la) |
sib4. sib8 la4. sol8 |
re'4 la sib4. sol8 |
la2 re |
R1 |
sib2 la |
sib4. sol8 la4. sib8 |
fa2 sib, |
R1 |
re2 re |
mi4. sol8 fad4. re8 |
sol2 re |
R1 |
re'2 re' |
dod'2. r16 la si dod' |
re'2 re' |
sib la |
sol4. re8 sol4. fa8 |
mib4. re8 do4.\trill sib,8 |
la,4. re8 sol,4. do,8 |
re,2 sol, |
r4 r8 sol do'4. sib8 |
la4. re'8 sol4. do8 |
re2 sol, |
