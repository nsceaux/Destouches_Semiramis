\clef "vdessus" re''2 |
la' re''4 dod''8\trill si' |
la'4 sol' fa' sol' |
la' re'8 mi' fa'4 sol' |
la' si' dod'' re'' |
dod''2 re''4 mi'' |
fa'' si' do'' la' |
sold'2 do''4 re'' |
mi'' la' si' do'' |
re'' sold' la'4. si'8 |
\appoggiatura si'8 do''4. re''8 si'4.\trill la'8 |
la'2 fa'' |
do'' fa''4 mi''8\trill re'' |
do''4 \appoggiatura sib'8 la'4 sib' \appoggiatura la'8 sol'4 |
la' fa' la' sib'8 do'' |
re''4 re'' mi'' fa'' |
mi''2\trill la'4 sib' |
do'' sol' sol' la' |
sib' fa' fa'4. sol'8 |
\appoggiatura sol'8 la'4. sib'8 sol'4.\trill fa'8 |
fa'2 fa''4(\melisma mi''8\trill)[ re'']( |
mi''4) fa''8[ mi''] re''[ fa'' mi'' re'']( |
dod''4)\melismaEnd la'8 re'' dod''4 re'' |
la' sol' fa'\trill mi' |
fa'2 la'4 la'8 si' |
do''4 la' re'' si' |
mi'' dod'' re''8[ mi'' fa'' mi'']( |
re''4) la' dod''8[\melisma re'' mi'' fa''] |
sol''1~ |
sol''4. fa''8 mi''[ re'' dod'' si']( |
dod''4)\melismaEnd la'8 fa'' mi''4 re'' |
dod'' si' dod'' re'' |
la'2 mi''4 fa''8 sol'' |
dod''4. sol''8 fa''4.\trill mi''8 |
fa''2( mi'')\trill |
re'' mi''4 fa''8 sol'' |
sol''1~ |
sol''4. fa''8 dod''4. re''8 |
re''4.( mi''8 mi''2\trill) |
re''2
