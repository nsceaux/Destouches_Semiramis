\score {
  \new ChoirStaff <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s1*3\break s1*3\break s1*3\break \grace s8 s1*3 s2 \bar "" \pageBreak
        s2 s1*3\break s1*3 s2 \bar "" \break s2 s1*3\break s1*3\pageBreak
        s1*4\break s1*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
