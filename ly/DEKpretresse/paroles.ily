Beaux Lieux, soy -- ez toû -- jours ex -- empts d’al -- lar -- mes.
A -- mour, n’en trou -- ble point la paix.
Trop de pleurs sui -- vent tes traits,
Que tes ar -- mes,
Que tes char -- mes,
S’en é -- loi -- gnent pour ja -- mais.
Non, non, à des plai -- sirs purs & fa -- ci -- les
Ne mê -- le point des soins fâ -- cheux.
Ces a -- zi -- les
Si tran -- qui -- les,
Ne re -- dou -- tent point tes feux.
Vo -- le, des -- cens A -- mour, vien dans ces lieux,
Nos cœurs y bra -- vent ta vic -- toi -- re :
Vo -- le, vo -- le, des -- cends A -- mour, vien dans ces lieux,
Voi ta dé -- faite, & nô -- tre gloi -- re.
Voi ta dé -- faite, & nô -- tre gloi -- re.
