\clef "dessus" <>^"Un hautbois" fa''2 |
fa'' fa''4 mi''8\trill fa'' |
re''4 mi'' la' re'' |
dod'' re'' re'' dod'' |
re'' re'' mi'' fa'' |
mi''2 fa''4 dod'' |
re'' fa'' mi'' re''\trill |
mi''2 la'4 si' |
do'' fad' sold' la' |
si' mi' fad'4. sold'8 |
la'4. si'8 sold'4. la'8 |
la'2 la'' |
la'' la''4 mi'' |
fa'' fa'' fa'' mi'' |
fa'' do'' do''( sib'8)\trill la' |
sib'4 fa' sol' la' |
sol'2\trill do''4( sib'8)\trill la' |
sol'4 do'' do''8( sib') la'(\trill sol') |
fa'4 sib' sib'4( la'8)\trill sol' |
fa'4. sol'8 mi'4.\trill fa'8 |
fa'2 la''4( sol''8)\trill fa'' |
sol''4 la''8 sol'' fa'' la'' sol'' fa'' |
mi''4 dod''8 fa'' sol''4 fa'' |
mi'' dod'' re'' mi'' |
la'2 r |
r fa''4( mi''8)\trill re'' |
sol''4( fa''8)\trill mi'' fa'' sol'' la'' sol'' |
fa''2 mi''8( fa'') sol''( la'') |
sib''2 mi''8( fa'') mi''( re'') |
dod''4.( re''8) dod''( re'') mi''( fa'') |
mi''4 dod''8 la'' sol''4 fa'' |
mi'' re'' mi'' fa'' |
mi''2 sol''4( la''8) sib'' |
mi''4.( re''16\trill) dod'' re''4. re''8 |
re''2( dod'')\trill |
re''2 sol''4( la''8) sib'' |
dod''1~ |
dod''4. re''8 mi''4. fa''8 |
fa''4.( mi''16 re'') dod''2 |
re''
