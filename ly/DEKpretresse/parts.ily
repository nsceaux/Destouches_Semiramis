\piecePartSpecs
#`((dessus)
   (basse-continue #:score-template "score-basse-continue-voix")
   (chant)
   (silence #:on-the-fly-markup , #{ \markup\tacet#74 #}))
