\clef "basse" re2 |
re re4 mi |
fa dod re sib, |
la,8 sol, fa, mi, re,4 mi, |
fa, sol,8 fa, mi,4 re, |
la,4 la8 sol fa4 mi |
re re mi fa |
mi2 r |
do r |
si,4 re do si, |
la, re, mi,2 |
la, fa, |
fa, fa4 sol |
la re' sol do' |
fa fa, fa fa |
sib la sol fa |
do'2 fa |
mi mib |
re re4 mi |
fa sib, do do, |
fa,2 fa |
dod re |
la4 sol8 fa mi4 re |
dod la, si, dod |
re2 fa |
mi4 fa re sol |
mi la8 sol fa4 re |
re8 mi fa sol la2 |
mi8 fa sol la sib2 |
la1 |
la,2~ la,4. la8 |
sol4 fa mi re |
dod4 la, sib, sol, |
la, la re' sol |
la2 la, |
re r |
r mi4 fa8 sol |
la4. re8 la,4 sol, |
la,1 |
re2
