\clef "basse"
<<
  \tag #'basse {
    R1*2 R1.*2 R1 R2. R1*7 R1.*8 R1 R1.*2 R1 R1. R2.*3 R1*6 R1. R1 | r4 r
  }
  \tag #'basse-continue {
    sol,2 la, |
    sib, fad, |
    sol,1. |
    do2 sol,4 fa, mib,2 |
    re1 |
    fad2. |
    sol4 sol8 fa mib8 fa mib re |
    do2 do'8 do' sib la |
    sol2 la4 sib8 mib |
    fa2 mib |
    re4 mib fa fa, |
    sib,2 do2 |
    re sib,4 la,8 sol, |
    re2 fa sol |
    la1 <<
      { re'4 re | la2 sol1 | fa2 sol4 sol, }
      \\ \new CueVoice {
        \voiceTwo re8_"[manuscrit]" do sib,4 | la,2 sol,1 | fa,2 sol,
      }
    >> la,2 |
    re2 sol, mib |
    re1 sol4 sol, |
    re2 sol, mib4 re |
    do2 fa, sib,8 lab, sol, fa, |
    mib,4 sol mi!2 |
    fa4. re8 mib re do sib, fa4 fa, |
    sib,1 la,2 |
    sol,2 si, |
    do1. |
    sib,2 la,8 sol, |
    fad,2~ fad,8 sol, |
    re4 re,2 |
    sol,2. sol8 la |
    sib2 re |
    mib do |
    fa4 mib re do |
    sib,1 |
    do2 re4 mib |
    re1 dod2 |
    re1 | \allowPageTurn
    sol,2
}
>> sol4 sol fa4.\trill mib8 |
re4 re re sol4. fa8 sol4 |
do2 do4 fa fa8 sol la fa |
sib4 sib sib fad2\trill fad8 sol |
re2 re'4 sol sol sol |
do'2 do'4 la2\trill la8 do' |
fa4 fa fa sib4. sol8 la4 |
re2 re4 sol sol8 la sib do' |
re'4 re' sib sol4. do8 re4 |
sol,2 re4 sol sol8 la sib do' |
re'4 re' sib sol4. do8 re4 |
sol,2 sol4 sol2 fad4 |
sol sol, sol sol fa mib |
re2 re'4 sol la sib |
la2 fa4 sol la la, |
re2 re4 sol2 sol,4 |
do2 mib4 fa2 fa,4 |
sib,2 sol,4 do2 do4 |
re2 sib,4 do re re, |
sol,2 sol,4 do2 do4 |
re2 sib,4 do re re, |
<<
  \tag #'basse {
    sol2 r |
    R1 R1.*11 R1*5 R1. R1*9 R1. R1 R1. R1 R1.*2 R1*2 R1. R1*2 R1. R1*37 |
    r2 r4
  }
  \tag #'basse-continue {
    sol,2. sol4 |
    fad2 fa |
    mi mib re |
    sol,1. | \allowPageTurn
    re |
    mib2 do fa4 mib |
    re1. |
    sol1 fa2 |
    mib1 si,2 |
    do1. |
    re1 do2 |
    re re,1 |
    sol4.*13/12 lab32 sol fa mib1 |
    lab,2 sol,8 lab, sib,4 |
    mib2. mi4 |
    fa2 la, |
    sib, sib |
    fad1 |
    sol2 sol,1~ |
    sol, |
    fad,2. sol,4 |
    do1 |
    sib,2 la,8 sib, do4 |
    fa2 r |
    mi sol |
    fa1 |
    sol2 la |
    re2 r |
    re r r |
    sol si, |
    do r mi |
    fa fa, |
    sol,1. |
    mib2 r r |
    do fa4. re8 |
    sol4 mib fa fa, |
    sib,1 do2 |
    re fad |
    sol4 do re re, |
    sol,1 si,2 |
    do4 sol,8 fa, mib,4 do, |
    re,4. re8 sol4. sol8 |
    sib4 sol do' do |
    sol2 do |
    re4 re r sol |
    do2. fa4 |
    sib sib sol2 |
    mib4 mib8 mib fa4 fa |
    sib, sib, sib4. sib8 |
    la2 la4. sib8 |
    sol2 sol4. sol8 |
    do'2 do'4. do8 |
    fa2 r4 re |
    sol2 r4 mi |
    la la fa2 |
    sib4 sib8 sol la4 la, |
    re re re' re' |
    re'8 mib' re' do' sib la sol fa |
    mib4 mib8 mib fa4 sol |
    do2 do'4 do' |
    do'8 re' do' sib la sol fa mib |
    re4 re8 re mib4 fa |
    sib,2 sib4 sib |
    mib'8 fa' mib' re' do' sib la sol do' re' mib' re' do' sib la sol |
    re'4 re'8 sib do'4 re' |
    mib'2. r8 sol |
    do2 re |
    sol,1 |
  }
>>
