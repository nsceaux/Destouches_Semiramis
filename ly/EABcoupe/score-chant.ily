\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'semiramis \includeNotes "voix"
    >> \keepWithTag #'semiramis \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'zoroastre \includeNotes "voix"
    >> \keepWithTag #'zoroastre \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
