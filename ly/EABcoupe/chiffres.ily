s2 <6+> <6> <5/> s2 <7 _+>1 <_->2. <6>4 <7> <6>

<_+>1 <6 5/>2. s4. <4+>8 <6>4. <6+>8 <_->2 <_+>4\figExtOn <_+>8\figExtOff <6+>
s2 <7>4. <6 5>8 <"">2\figExtOn <"">\figExtOff <6>1
s2 <6 _-> <_+> <6>4 <6+> <_+>2 <6> <6 _-> <_+>1.

\new FiguredBass { <_+>2\figExtOn <_+>1\figExtOff <6>2 <7> } <5 4>4\figPosOn <5 3+>\figPosOff <_+>1 <7>4 <6>
<_+>1. <_+>1 <"">4\figExtOn <"">\figExtOff <7 _->2 <7-> s4 <6>8 <6 _->
s2 <5/> s4. <6>8 s4 <6 _-> <4> <3>4 s1 <6+>2

s2 <6 5/> s2 <6 4+ 2>1 <6>2 <6+>4 \new FiguredBass { <5/>2. <4>4 <3+>2 }
s1 s2 <6> s1 <"">4\figExtOn <"">\figExtOff
<6> <\markup\triangle-down 7 6 \figure-flat > s1 <6 _->2 <6>4 <6> <_+>1 <5/>2 <_+>1

s1 <6>4. <6 4>8 <6>2. <_+> <_->1. s2. <5/>
<_+>1. s2. <6> s s2 <_+>4 <_+>1.
<_+>2 <6>4 s4. <7 _->8 <_+>4 s2 <_+>4 s2. <_+>2 <6>4 s4. <7 _->8 <_+>4 s2

s4 <6- 4 2>2 <5/>4 s2. <6->4 <6> <6> <_+>2. <6>2\figExtOn <6>4\figExtOff <_+>2 <6>4 <6 5>2 <_+>4 <_+>2
<_->4 <7 _+>2. <_-> <5 4>2 <3>4 s2. <7 _->2 <6>4 <_+>2 <6>4 <6 5 _->2 \new FiguredBass <_+>4

s2. <7 _->2 <6>4 <_+>2 <6>4 <6 5 _->2 \new FiguredBass <_+>4 s1
<6>2\figExtOn <6>\figExtOff <7> <6 4 3> <_+> s1. <6>2 <5/>1 s1
<"">4\figExtOn <"">\figExtOff <6>1. <_+>1\figExtOn <_+>2\figExtOff <6>1 <5/>2 <_->1.

<_+>1\figExtOn <_+>2\figExtOff \new FiguredBass { <4>2 <3+>1 } s2 <5>1 <6 4>2 <6->
s2. <5/>4 s2 <5/> s1
<5/>1 s2 <6- 4>1 <5> <6>

s1 <6 4 2>2 <6> s1 <6+>2\figExtOn <6+>\figExtOff <6>1
<6>2 <_+> <_+>1\figExtOn <_+>1.\figExtOff s2 <6 5/> s1
<6 5/>2 s1 s1.*2 <_->2 s4. <6>8

s2 <5 4>4 <3> s1 <6 _->2 <_+>1 s2 <4>4 <3+>
s1 <5/>2 <_->4. <6>8 s4 \new FiguredBass <6>

<_+>1 <6>2 <_-> s <_-> <_+>1 <7 _->
s2 <6-> s1*2 <6+>1 s

<7>1 s <_+> <_+>2 <6> <7>4 <6 4> <7 _+>2 <_+>

s2 s <6> s <7->4 <_+> <_->1 s2 <6>

<6>2 <9 7> s1*3 <_+>2 <9- 7>4 <7 _+>

s1 <7 _->2 <7 _+>
