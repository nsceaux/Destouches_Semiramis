\key re \minor
\time 2/2 \midiTempo#120 s1*2
\time 3/2 s1.*2
\time 2/2 s1
\digitTime\time 3/4 s2.
\time 2/2 \grace s8 s1*7
\time 3/2 s1.*8
\time 2/2 \grace s8 s1
\time 3/2 s1.*2
\time 2/2 s1
\time 3/2 s1.
\digitTime\time 3/4 s2.*3
\time 2/2 s1*6
\time 3/2 s1.
\time 2/2 s1
\time 6/4 s1.*21
\time 2/2 s1*2
\time 3/2 s1.*11
\time 2/2 s1*5
\time 3/2 \grace s8 s1.
\time 2/2 s1*9
\time 3/2 s1.
\time 2/2 \grace s8 s1
\time 3/2 s1.
\time 2/2 s1
\time 3/2 \grace s8 s1.*2
\digitTime\time 2/2 s1*2
\time 3/2 s1.
\time 2/2 s1*2
\time 3/2 s1.
\time 2/2 \grace s8 s1

\time 2/2 \beginMark "[Duo]" s1*28 \bar "|."

