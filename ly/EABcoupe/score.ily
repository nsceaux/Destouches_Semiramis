\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { \haraKiriFirst } << \global \includeNotes "dessus" >>
      \new Staff \with { \haraKiriFirst } << \global \includeNotes "haute-contre" >>
      \new Staff \with { \haraKiriFirst } << \global \includeNotes "taille" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'semiramis \includeNotes "voix"
    >> \keepWithTag #'semiramis \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'zoroastre \includeNotes "voix"
    >> \keepWithTag #'zoroastre \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2 s1.\break \grace s8 s1. s1 s2. s2 \bar "" \break s2 s1*2 s2 \bar "" \break s2 s1*2\pageBreak
        s1 s1.*2\break s1.*2\break \grace s8 s1.*2 s2 \bar "" \break s1 s1. s2 \bar "" \break s2 s1.*2\break s1 s1. s2.\pageBreak
        s2.*2 s1*2\break s1*3\break s1 s1. s1\break s1.*3\pageBreak
        s1.*3 s2. \bar "" \break s2. s1.*2\pageBreak
        s1.*3\break s1.*3\break s1.*3\break s1.*3\pageBreak
        s1*2 s1.*2\break s1.*3 s1 \bar "" \break s2 s1.*3\break s1.*2 s1\break s1*2 s2 \bar "" \break s2 s1 s1.\pageBreak
        s1*3 s2 \bar "" \break s2 s1*3 s2 \bar "" \break s2 s1 s1. s1\break s1. s1 s1.\break s1. s1*2 s1 \bar "" \break s2 s1*2 s1.\pageBreak
        \grace s8 s1*3 s2 \bar "" \break s2 s1*4 s2 \bar "" \break s2 s1*5\pageBreak
        s1*5\break s1*5\break s1*5\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
