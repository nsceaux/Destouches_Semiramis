\clef "haute-contre" R1*2 R1.*2 R1 R2. R1*7 R1.*8 R1 R1.*2 R1 R1. R2.*3 R1*6
R1. R1 |
r4 r sib'\douxSug la' la'4. do''8 |
re''4 re'' re'' si'4. la'8 si'4 |
do'' sol' do'' la' fa' fa' |
fa' sib' sib' la'8 sol' la' sib' la' sol' |
fad'4 la' la' sib' sib'4. sib'8 |
sol' fa' sol' la' sib' sol' do''4 do''4. do''8 |
do''4 la' do'' re''4. re''8 dod''4 |
re''2 re''4 re'' re''8 do'' sib'4 |
la' la' sol' sol'4. la'8 fad'4 |
sol'2 re''4 re'' re''8 do'' sib'4 |
la' la' sol' sol'4. la'8 fad'4 |
sol'2 \footnoteHere #'(0 . 0) \markup\wordwrap {
  Manuscrit : \italic { “mettre des parties” }
} r4 r2*3/2 |
R1.*9 |
R1*2 R1.*11 R1*5 R1. R1*9 R1. R1 R1. R1 R1.*2 R1*2 R1. R1*2 R1. R1*29 |

