\clef "dessus" R1*2 R1.*2 R1 R2. R1*7 R1.*8 R1 R1.*2 R1 R1. R2.*3 R1*6
R1. R1 |
\footnoteHere #'(0 . 0) \markup\wordwrap {
  Manuscrit : mesure à 6/8.
}
r4 r re''4\doux ^"Violons" re'' re''4.\trill mib''8 |
fa''8 mib'' fa'' sol'' fa'' mib'' re'' do'' re'' mib'' fa'' re'' |
mib'' re'' mib'' fa'' mib'' re'' do'' sib' la' sib' do'' la' |
re'' do'' re'' mib'' re'' mib'' do'' sib' do'' re'' do'' sib' |
la'4 re''8 mi'' fad'' re'' sib'' do''' sib'' la'' sol'' fa''? |
mi'' re'' mi'' fa'' sol'' mi'' fa''4 do''4. mi''8 |
la'' sol'' la'' sib'' la'' sol'' fa''4. sol''8 mi''4 |
re''2 la''4 sib''8 do''' sib'' la'' sol'' la'' |
fad'' sol'' fad'' mi'' re'' do'' sib'4. do''8 la'4 |
sol'2 la''4 sib''8 do''' sib'' la'' sol'' la'' |
fad'' sol'' fad'' mi'' re'' do'' sib'4. do''8 la'4 |
sol'2 re'4\doux ^"Violons" mib'2 re'4 |
re'2 re'4 mib' re' sol' |
fad'2 la'4 sib' la' sol' |
mi'2 re'4 re'2 dod'4 |
re'2 fa'4 re'2 si'4 |
do''2 sib'4 sib'2 la'4 |
sib'2 sib'4 sib'2 la'4 |
fad'2 sol'4 sol'2 fad'4 |
sol'2 sib'4 sib'2 la'4 |
fad'2 sol'4 sol'2 fad'4 |
sol'2 r | \allowPageTurn
R1 R1.*11 R1*5 R1. R1*9 R1. R1 R1. R1 R1.*2 R1*2 R1. R1*2 R1. R1 |
R1 |
<>^"Violons" r4 r8 sol''\doux sol''4. sol''8 |
sol'4 sol' sol'2 |
re' r4 re'' |
mib''2. do''4 |
re'' re' mib'2 |
sol'4 sol'8 sol' la'4 la' |
sib' sib' r2 |
R1 |
r2 sib'4. sib'8 |
sib'2 sol'4.(\trill fa'16 sol') |
la'4 r r fa'' |
re''2 r4 sol'' |
mi'' mi'' la'' la' |
la' sol' sol' la' |
fad'2 r |
r sol''4 sol'' |
sol''8 fa'' sol'' la'' sol'' fa'' mib'' re'' |
mib''4 do'' r2 |
r fa''4 fa'' |
fa''8 mib'' fa'' sol'' fa'' mib'' re'' do'' |
re''4 sib' r2 |
r sol'4 sol' |
sol'2 la'4 la' |
fad' fad'8 sib' sib'4 la' |
sol'2 r4 r8 sol' |
sol'2 fad' |
sol'1 |
