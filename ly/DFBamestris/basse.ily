\clef "basse" sol4. fa8 mib re |
do2. |
fad, |
sol,2 sol4 |
fad2 fa4 |
mi2 mib4 |
re2. |
do |
mib |
re4. do8 sib, la, |
sol,2 sol4 |
dod2 do4 |
sib,2 sib,4 la, sol, re8 re, |
sol,2.
