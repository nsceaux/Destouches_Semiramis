\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*3\break \grace s8 s2.*4\break
        s2.*3 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
