J’o -- bé -- ï -- rai, grands Dieux, je vais vous sa -- tis -- fai -- re :
Je re -- çois u -- ne mort qui fi -- nit mes tour -- mens.
Reine, à vô -- tre re -- pos, je ne suis plus con -- trai -- re ;
Lais -- sez- moi m’oc -- cu -- per de mes der -- niers mo -- mens.
