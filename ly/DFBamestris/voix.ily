\amestrisMark r8 sol' sol' sol' do'' re'' |
mib''2 r8 do'' |
do''4. do''8 re'' la' |
\appoggiatura la'8 sib'4 sib' mib''8 mib'' |
mib''4.( re''8) re'' re'' |
sol'2 sol'8 lab' |
\appoggiatura sol'8 fa'2 fa'8 sol' |
mi'!2. |
la'4 la'8 la' sib' do'' |
\appoggiatura sol'8 fad'8. fad'16 fad'8 fad' sol' la' |
\appoggiatura la'8 sib'4 sib' sib'8 sib' |
sib'2 la'8 la' |
re'2 mi'4 fad' sol' sol'8 fad' |
sol'2.
