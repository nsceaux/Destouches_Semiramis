\tag #'(semiramis basse) {
  C’est as -- sez. Il est tems d’a -- che -- ver mon ou -- vra -- ge ;
  A -- mes -- tris, ap -- pro -- chez, fai -- tes vô -- tre de -- voir.
}
\tag #'(amestris basse) {
  Sei -- gneur, du su -- prê -- me pou -- voir
  C’est donc à moi de vous of -- frir le ga -- ge.
  Mon sang m’a -- voit don -- né des droits sur vos E -- tats ;
  Vi -- vez heu -- reux, re -- gnez, je n’en mur -- mu -- re pas.
  Joü -- is -- sez à ja -- mais de la fa -- veur ce -- les -- te ;
  Et re -- ce -- vez mes vœux, c’est tout ce qui me res -- te...
}
\tag #'(arsane basse) {
  Quel pré -- sent ! quel -- le main vient i -- ci me l’of -- frir !
  Ge -- ne -- reuse A -- mes -- tris... Non, dus -- sai- je pe -- rir...
}
