\clef "basse" sib4 |
la2. |
sol2 do'4 do |
fa2. |
sib, |
mib2 fa4 fa, |
sib,4. do8 sib, la, |
sol,2. |
do~ |
do |
sib, |
la, |
sol,2 sol4 |
fa2. |
mib2 mi1 |
fa2. |
mib |
re |
mib4 fa8 sib, fa,4 |
sib,2.~ |
sib, |
si,1. |
do1.~ |
do |
re1 do2 |
sib,2. do4 re mib |
re2 re,1 |
sol,2 sol4 |
fad2 fa4 |
mi4 fad sol sol, |
re2 si, |
do1. |
re2. |
