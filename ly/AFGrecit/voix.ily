<<
  %% Semiramis
  \tag #'(semiramis basse) {
    \tag #'basse \semiramisMark
    \tag #'semiramis \clef "vbas-dessus" re''8 mib'' |
    \appoggiatura re''8 do''2 fa'8 fa' |
    sib'4 sib'8 la' sol'4 la'8 sib' |
    la'4\trill la'8 r la' la' |
    re''2 re''8 re'' |
    \appoggiatura do''8 sib'4 sib'8 sib' sib'4\trill sib'8 la' |
    sib'2. |
    \tag #'semiramis { R2.*7 R1. R2.*6 R1.*6 R2.*2 R1*2 R1. R2. }
  }
  %% Amestris
  \tag #'(amestris basse) {
    <<
      \tag #'basse { s4 s2. s1 s2.*2 s1 s2. \amestrisMark }
      \tag #'amestris { \clef "vbas-dessus" r4 R2. R1 R2.*2 R1 R2. }
    >>
    r4 r r8 re'' |
    la'4. r8 la' sib' |
    do''2 sib'8 la' |
    re''4. sib'8 do'' re'' |
    fad'4 fad'8 fad'16 fad' sol'8 la' |
    \appoggiatura la'8 sib'4 sib'8 r sib'4 |
    si'4.\trill si'8 si' do'' |
    do''4. do''8 do''2 sib'4 sib'8 la' |
    la'4.\trill la'8 sib' do'' |
    fa'2 r8 do'' |
    fa''2. |
    r8 do'' la' sib' sib' la' |
    \appoggiatura la'8 sib'2 re''8 re'' |
    re''2 re''8 sib' |
    \appoggiatura la'8 sol'2 re''4 re''8 re'' re''4. fa''8 |
    \appoggiatura fa''8 mib''1 mib''2 |
    la'2 la'4 do'' la'4.\trill\prall sib'8 |
    fad'1\trill la'2 |
    re''2. la'4 fad' sol' |
    sib'2( la'1)\trill |
    sol'2
    \tag #'amestris { r4 R2. R1*2 R1. R2. }
  }
  %% Arsane
  \tag #'(arsane basse) {
    <<
      \tag #'basse {
        s4 s2. s1 s2.*2 s1 s2.*8 s1. s2.*6 s1.*6
        s2 \arsaneMarkText "Vivement"
      }
      \tag #'arsane {
        \clef "vhaute-contre" r4 R2. R1 R2.*2 R1 R2.*8 R1. R2.*6 R1.*6
        r4 r <>^\markup\italic Vivement
      }
    >> mib'8 mib' |
    la4 r re'8 re' |
    re'4 do'8 re' sib4\trill sib8 la |
    la4\trill re'8 mib' fa'4 mib'8 re' |
    \appoggiatura re'8 mib'2 <>^\markup\italic { à part } sol'4. do'8 la4\trill sib8 do' |
    fad2.\trill |
  }
>>