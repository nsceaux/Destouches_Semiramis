\key sol \minor
\digitTime\time 3/4 \midiTempo#160 \partial 4 s4 s2.
\time 2/2 s1
\digitTime\time 3/4 s2.*2
\time 2/2 \grace s8 s1
\digitTime\time 3/4 s2.
\key re \minor s2.*7
\time 3/2 s1.
\digitTime\time 3/4 s2.*6
\time 3/2 \grace s8 s1.*6
\digitTime\time 3/4 s2.*2
\time 2/2 s1*2
\time 3/2 \grace s8 s1.
\digitTime\time 3/4 s2. \bar "|."
