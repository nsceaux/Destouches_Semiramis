\clef "vtaille" r2 re'4 dod' |
re'2 sol' |
mi'2. mi'4 |
re'2 re'4 fa' |
sol'2 sol'4 fa'8 fa' |
re'2 do' |
do' do'4 do' |
do'2 do' |
re'2. re'4 |
re'2 re'4 fa' |
dod'2 la4 la |
dod'2 re'4. mi'8 |
la1~ |
la2 la4 re' |
dod'2 dod'4. dod'8 |
re'4 la dod'4 dod' |
re'2 mi' |
re'2. re'4 |
dod'2 re'4. mi'8 |
re'4 re' re' fa' |
mi'2 mi'\trill |
re'2. re'4 |
sol'2 sol'4 sol' |
do'2. do'4 |
re'2 re'4.\trill re'8 |
mi'4 mi' re' do' |
si2 mi' |
la la'4 la' |
dod'2. re'4 |
la la fa'4. mi'8 |
re'2 re' |
la'2 re'4 dod' |
