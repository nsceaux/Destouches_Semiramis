\clef "dessus" <>^"Violons" r2 fa''4 mi'' |
re''2 mi'' |
dod''2. mi''4 |
fa''2 fa''4 re'' |
mi''2 mi''4 fa''8 fa'' |
fa''2 mi'' |
fa'' fa''4 fa'' |
mi''2 fad'' |
sol''2. mi''4 |
fa''2 fa''4 re'' |
mi''2 dod''4 fa'' |
mi''2 re''4 dod'' |
re'' la' re'' re'' |
mi''2 fa'' |
mi''2. mi''4 |
la''2 la''4 la'' |
la''1~ |
la''2 la''4 fa'' |
mi''2 re''4. dod''8 |
re''4 la' re'' re'' |
si'4.( do''8) dod''2\trill |
re''2. \twoVoices #'(dessus1 dessus2 dessus) <<
  { fa''4 |
    fa''2 mi''4 re'' |
    mi''2.\trill mi''4 |
    fa''2 re''4. do''8 |
    si'4 si' si' do'' |
    re''2 si'\trill |
    la' mi''4 fa'' |
    sol''2. fa''4 |
    mi'' dod''! re''4. mi''8 |
    \appoggiatura mi'' fa''2 re''4.( mi''8) |
    mi''2\trill }
  { re''4 |
    re''2 do''4 si' |
    do''2. do''4 |
    do''2 si'4. la'8 |
    sold'4 sold' sold' la' |
    la'2 sold' |
    la' dod''4 re'' |
    mi''2. re''4 |
    dod'' la' si'4. dod''8 |
    \appoggiatura dod''8 re''2 si'4.( dod''8) |
    dod''2 }
>> fa''4 mi'' |
