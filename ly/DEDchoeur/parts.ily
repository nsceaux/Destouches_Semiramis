\piecePartSpecs
#`((dessus #:score "score-dessus")
   (dessus-hc)
   (haute-contre)
   (taille)
   (basse)
   (basse-continue)
   (chant)
   (silence #:on-the-fly-markup , #{ \markup\tacet#32 #}))
