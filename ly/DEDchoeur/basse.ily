\clef "basse" re2 re4 mi |
fa2 sol |
la2. la4 |
re'2 re'4 re' |
do'2 do'4 la8 la |
sib2 do' |
fa fa4 fa |
do'2 do' |
sol2. sol4 |
re'2 re'4 re |
la1~ |
la~ |
la~ |
la2 la4 la |
sol2 fa4 mi |
fa re la la |
si2 dod' |
re'2. la4 |
sol2 fa4 mi |
fa re fa fa |
sol2 la |
re2. <<
  \tag #'basse { r4 R1*9 r2 }
  \tag #'basse-continue {
    \clef "alto" re'4 |
    sol'2 sol'4 sol' |
    do'2. do'4 |
    re'2 re'4.\trill re'8 |
    mi'4 mi' re' do' |
    si2 mi' |
    la la'4 la' |
    dod'2. re'4 |
    la la fa'4. mi'8 |
    re'2 re' |
    la' \clef "basse"
  }
>> re4 mi |
