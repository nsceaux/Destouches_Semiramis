\clef "vbasse" r2 re4 mi |
fa2 sol |
la2. la4 |
re'2 re'4 re' |
do'2 do'4 la8 la |
sib2 do' |
fa2 fa4 fa |
do'2 do' |
sol2. sol4 |
re'2 re'4 re |
la1~ |
la~ |
la~ |
la2 la4 la |
sol2 fa4. mi8 |
fa4 re la4 la |
si2 dod' |
re'2. la4 |
sol2 fa4. mi8 |
fa4 re fa fa |
sol2 la |
re2. r4 |
R1*9 |
r2 re4 mi |
