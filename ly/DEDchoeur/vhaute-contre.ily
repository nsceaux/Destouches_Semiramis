\clef "vhaute-contre" r2 la'4 sol' |
la'2 sib' |
la'2. la'4 |
la'2 la'4 si' |
do''2 do''4 do''8 do'' |
sib'2 sol' |
la' la'4 la' |
sol'2 la' |
sib'2. sib'4 |
la'2 la'4 la' |
la'2 mi'4 la' |
sol'2 fa'4. mi'8 |
fa'4 re' fa' fa' |
sol'2 la' |
la'2. la'4 |
fa'2 mi'4. la'8 |
fa'4.( sol'8 sol'4.\trill fa'8) |
fa'2. la'4 |
la'2 la'4. sol'8 |
la'4 fa' la' la' |
sol'2 sol'\trill |
fa'2. re'4 |
sol'2 sol'4 sol' |
do'2. do'4 |
re'2 re'4.\trill re'8 |
mi'4 mi' re' do' |
si2 mi' |
la la'4 la' |
dod'2. re'4 |
la la fa'4. mi'8 |
re'2 re' |
la'2 la'4 sol' |
