\score {
  \new GrandStaff <<
    \new Staff <<
      \global \keepWithTag #'dessus1 \includeNotes "dessus"
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'dessus2 \includeNotes "dessus"
      { \startHaraKiri s1*21 s2.\break \stopHaraKiri }
    >>
  >>
  \layout { }
}
