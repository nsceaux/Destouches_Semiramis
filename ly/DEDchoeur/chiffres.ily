\simultaneous {
  \tag #'taille {
    s1*22
    <7>1 s <7>2 <6> <_+>2\figExtOn <_+>\figExtOff <7> <7 _+> <_+>1 <5/> <_+>2
    <6 4>4. <6+>8 s2 <6> <_+>
  }
  \tag #'basse {
    s2. <6+>4 s2 <6 _-> <_+>1 s2. <6>4 <5>2. <6>4 <6 5>2 <7-> s1 s2 <6 4>
    <_->1 s <_+> <7> <6 4> <7 _+>2 <6 4> <4+>2.\figExtOn <4+>4\figExtOff <6>2
    <_+>2 <7> <5/> s2. <6 4>4 <4+>2\figExtOn <4+>\figExtOff <6> <"">4\figExtOn <"">\figExtOff <6>2 <7 _+> s2.
  }
}
