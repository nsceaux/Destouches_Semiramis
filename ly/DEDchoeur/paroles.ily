Di -- gne sang des Rois,
Le Ciel vous ap -- pel -- le,
Soû -- te -- nez son choix ;
Di -- gne sang des Rois,
Le Ciel vous ap -- pel -- le,
\tag #'vtaille { Le Ciel vous ap -- pel -- le, }
Au Peu -- ple fi -- del -- le
Dis -- pen -- sez ses Loix.
\tag #'(vdessus vhaute-contre) { Le Ciel vous ap -- pel -- le, }
Au Peu -- ple fi -- del -- le
Dis -- pen -- sez ses Loix.

\tag #'(vdessus vhaute-contre) {
  Sen -- sible à vos vœux
  Le Dieu du Ton -- ner -- re
  E -- tein -- dra ses feux ;
  Et par vous, la Ter -- re
  Va s’u -- nir aux Cieux.
}

Di -- gne
