\clef "basse" do'1 |
mi'4. re'8 do' re' do' si la sol fa4 |
mi1 la4 sol |
fa2 dod |
re1~ |
re2 fad |
sol~ sol8 la sol fa |
mi1 re2 |
do1 |
