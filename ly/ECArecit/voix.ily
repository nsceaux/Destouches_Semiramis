\semiramisMark r2 r4 mi'' |
sold'4. mi'8 la' la' la' si' do'' do'' do'' re'' |
\appoggiatura re''8 mi''2 mi''4 mi''8 mi'' dod''4 dod''8 re'' |
re''4. re''8 mi'' mi'' mi'' mi'' |
la'4 la'8 la' sib'4 sib'8 sib' |
fad'4. re''8 re'' do'' do'' si' |
si'2\trill sol'8 sol'16 sol' la'8 si' |
\appoggiatura si'8 do''2 sol'4 sol'8 la' \appoggiatura sol'8 \afterGrace fa'4.( mi'8) fa' |
mi'2 mi' |
