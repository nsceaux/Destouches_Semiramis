\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s1 s1. s1 \bar "" \break s2 s1*2\break s1*2\break }
    >>
  >>
  \layout { }
  \midi { }
}
