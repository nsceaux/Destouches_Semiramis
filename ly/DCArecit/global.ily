\key re \major
\time 2/2 \partial 2 \midiTempo#160 s2
\time 3/2 s1.
\time 2/2 s1*5
\time 3/2 s1.*3
\time 2/2 s1
\digitTime\time 3/4 \grace s8 s2.*4
\time 3/2 \midiTempo#80 \grace s16 s1.
\time 2/2 \midiTempo#160 s1*2
\digitTime\time 3/4 s2.
