\clef "basse" la,2\repeatTie |
la sol4. fad8 mi4 la8 la, |
re2 re' |
do'1 |
si2 la |
sol2. sold4 |
la4. fad8 sol4 la |
re2 re' sold4 sol |
fad sold la2 re |
mi1. |
la4 sol fad8 sol fad mi |
re2. |
mi2 fad4 |
sol4. fad8 mi re |
la4 la,2 |
re~ re fad |
sol1 |
fad4 mi re sol, |
la,2. |
