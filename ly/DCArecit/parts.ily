\piecePartSpecs
#`((basse-continue #:score-template "score-basse-continue-voix")
   (chant #:score "score")
   (silence #:on-the-fly-markup , #{ \markup\tacet#18 #}))
