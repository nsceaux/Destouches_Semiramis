<<
  %% Amestris
  \tag #'(basse amestris) {
    <<
      \tag #'basse { s2 s1. s2. \amestrisMarkText "gravement" }
      \tag #'amestris {
        \clef "vbas-dessus" r2 R1. r2 r4 <>^\markup\italic "gravement"
      }
    >> la'4 |
    la' la'8 r la'4 la' |
    re''4 re''8 re'' fad'4 fad'8 sol' |
    sol'4. si'8 dod'' re'' mi'' fad'' |
    dod''4. re''8 \appoggiatura dod''16 si'8 si' la' sol' |
    fad'2 r4 la'8 la' mi''4 mi''8 fad'' |
    \appoggiatura mi''8 re''4. si'8 \appoggiatura si'16 do''4. do''8 si'4. la'8 |
    sold'2\trill sold'4 si'8 si' si'4 dod''8 re'' |
    dod''4. dod''8 re'' re'' re'' mi'' |
    \appoggiatura mi''8 fad''2 re''4 |
    re''4. do''8 do'' re'' |
    \appoggiatura do''16 si'2 dod''?8 re'' |
    re''2\trill re''8 dod'' |
    \appoggiatura dod''16 re''4 re''
    \tag #'amestris { r2 r R1*2 R2. }
  }
  %% Arsane
  \tag #'(basse arsane) {
    \arsaneMarkText "à Amestris" sol'4 fad' |
    mi' mi'8 fad' dod'4. re'8 mi' mi' fad' sol' |
    fad'2\trill fad'4
    <<
      \tag #'basse { s4 s1*4 s1.*3 s1 s2.*4 s2 \arsaneMarkText "vivement" }
      \tag #'arsane {
        r4 R1*4 R1.*3 R1 R2.*4 r2 <>^\markup\italic vivement
      }
    >> fad'8 fad'16 fad' sol'8 la' re'16 re' re' re' mi'8 fad' |
    si4. sol8 si si si dod' |
    \footnoteHere #'(0 . 0) \markup\wordwrap {
      Manuscrit : \raise#-6 \score {
        \new StaffGroup <<
          \new Staff \with { autoBeaming = ##f } {
            \tinyQuote \clef "vhaute-contre" \key re \major \time 2/2
            re'4 re'8 mi' re'4\trill re'8 dod' |
            \digitTime\time 3/4 re'2\trill re'4 |
          } \addlyrics { - dats ra -- ni -- mer le cou -- ra -- ge. }
          \new Staff {
            \tinyQuote \clef "basse" \key re \major
            fad4 sol la la, | re2. |
          }
          \new FiguredBass \figuremode { \tinyQuote <6>2 <4>4 <3> }
        >>
        \layout {
          \quoteLayout
          \context { \Staff \consists "Time_signature_engraver" }
        }
      }
    }
    re'4 re'8 mi' fad'4\trill fad'8 sol' |
    mi'2\trill mi'4 |
  }
>>