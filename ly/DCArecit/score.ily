\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s1.\break s1*3\break
        s1*2 s2 \bar "" \break s1 s1.\break s1. s1 s2.\break s2.*3\break \grace s8 s1. s1\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
