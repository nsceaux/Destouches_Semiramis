\tag #'(arsane basse) {
  Ah ! Prin -- ces -- se, faut- il que rien ne vous flé -- chis -- se !
}
\tag #'(amestris basse) {
  Ar -- sa -- ne, res -- pec -- tez A -- mes -- tris & les Dieux.
  Quels sont vos droits sur moi, pour -- quoi trou -- bler mes vœux ?
  De mes jours à nos Dieux, je fais un libre hom -- ma -- ge.
  J’ai cal -- mé les trans -- ports d’un Peuple au -- da -- ci -- eux.
  Lais -- sez à ma ver -- tu con -- som -- mer son ou -- vra -- ge.
}
\tag #'(arsane basse) {
  Non, lais -- sez- moi sor -- tir de ces fu -- nes -- tes lieux.
  Je vais de mes Sol -- dats ra -- ni -- mer le cou -- ra -- ge.
}
