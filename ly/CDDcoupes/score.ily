\score {
  \new ChoirStaff <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
    >>
    \new ChoirStaff <<
      \new Staff \with { \haraKiri } \withLyricsB <<
        \global \includeNotes "vhaute-contre"
      >>
      \keepWithTag #'couplet1 \includeLyrics "paroles"
      \keepWithTag #'couplet2 \includeLyrics "paroles"
      \new Staff \with { \haraKiri } \withLyricsB <<
        \global \includeNotes "vtaille"
      >>
      \keepWithTag #'couplet1 \includeLyrics "paroles"
      \keepWithTag #'couplet2 \includeLyrics "paroles"
      \new Staff \with { \haraKiri } \withLyricsB <<
        \global \includeNotes "vbasse-taille"
      >>
      \keepWithTag #'couplet1 \includeLyrics "paroles"
      \keepWithTag #'couplet2 \includeLyrics "paroles"
      \new Staff \withLyricsB <<
        \global \includeNotes "vbasse"
      >>
      \keepWithTag #'couplet1 \includeLyrics "paroles"
      \keepWithTag #'couplet2 \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*6\pageBreak s2.*8\pageBreak
        s2.*6\pageBreak s2.*7\pageBreak
        s2.*6\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
