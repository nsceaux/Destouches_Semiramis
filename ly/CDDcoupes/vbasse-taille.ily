\clef "vbasse-taille" la4 |
sib sib sib |
sib2 sib4 |
la2 la4 |
sib sib sib |
sib2 sib4 |
sib2 la4 |
sib4 fa fa |
fa2 la4 |
sol2 sol4 |
la2 la4 |
la2. |
sib4 sib sib |
sib2 sib4 |
lab lab lab |
lab lab lab |
sol sol si |
do'2 si4 |
do' do' fa' |
do'2 do'4 |
do' do' re' |
re' re' re' |
re'2 re'4 |
re' sib sol |
la2 fa4 |
sol2 sol4 |
fad2. |
sol4 sol sol |
sol sol sol |
sol sol do' |
do'2 la4 |
sib sib mib' |
sib2 sib4 |
sol sol fa |
fa fa la |
sib sib mib' |
sib2 sib4 |
sol sol fa |
fa fa
\set Staff.whichBar = "|."
