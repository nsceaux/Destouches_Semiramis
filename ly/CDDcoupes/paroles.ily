\tag #'couplet1 {
  Ver -- sons l’é -- pou -- van -- te
  Dans les cœurs ;
  Que l’at -- ten -- te
  Des mal -- heurs
  En aug -- men -- te
  Les hor -- reurs.
  Com- _
}
\tag #'couplet2 {
  _ - mande à l’Em -- pi -- re
  Te -- ne -- breux.
  Tout cons -- pi -- re
  Pour tes vœux.
  Qu’on res -- pi -- re
  Mil -- le _ _ feux.
}

\tag #'couplet1 {
  Sou -- flons la guer -- re.
  Cou -- vrons la ter -- re
  De sang & de morts.
  Fai -- sons des ef -- forts
  E -- gaux au ton -- ner -- re.
  Souf -- flons la guer -- re
  Peu -- plons les som -- bres bords.
  
  Cou -- vrons la ter -- re
  De sang & de morts.
  Fai -- sons des ef -- forts
  E -- gaux au ton -- ner -- re.
  Fai -- sons des ef -- forts
  E -- gaux au ton -- ner -- re.
}
