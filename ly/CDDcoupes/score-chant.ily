\score {
  <<
    \new ChoirStaff <<
      \new Staff \with { \haraKiri } \withLyricsB <<
        \global \includeNotes "vhaute-contre"
        { <>^\markup {
            \smallCaps Zoroastre & le \smallCaps Chœur, alternativement
          } }
      >>
      \keepWithTag #'couplet1 \includeLyrics "paroles"
      \keepWithTag #'couplet2 \includeLyrics "paroles"
      \new Staff \with { \haraKiri } \withLyricsB <<
        \global \includeNotes "vtaille"
      >>
      \keepWithTag #'couplet1 \includeLyrics "paroles"
      \keepWithTag #'couplet2 \includeLyrics "paroles"
      \new Staff \with { \haraKiri } \withLyricsB <<
        \global \includeNotes "vbasse-taille"
      >>
      \keepWithTag #'couplet1 \includeLyrics "paroles"
      \keepWithTag #'couplet2 \includeLyrics "paroles"
      \new Staff \withLyricsB <<
        \global \includeNotes "vbasse"
      >>
      \keepWithTag #'couplet1 \includeLyrics "paroles"
      \keepWithTag #'couplet2 \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
