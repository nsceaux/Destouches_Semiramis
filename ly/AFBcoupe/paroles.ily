\tag #'(semiramis basse) {
  En -- fin, voi -- ci l’ins -- tant si cher à mes sou -- haits !
  Ve -- nez, jeu -- ne He -- ros, ve -- nez, que mes su -- jets
  Vous pla -- cent sur le Trône, où vous au -- riez dû naî -- tre,
  Et dans leur Dé -- fen -- seur re -- con -- nois -- sent leur Maî -- tre.
}
\tag #'(arsane basse) {
  Croi -- rai- je qu’en ce jour ces Peu -- ples re -- dou -- tez,
  Aux loix d’un In -- con -- nu, sans mur -- mure o -- bé -- ïs -- sent ?
  Plus je vous vois pour moi pro -- di -- guer vos bon -- tez,
  Plus mes es -- prits sont a -- gi -- tez.
  Peut- ê -- tre les Dieux me pu -- nis -- sent
  D’u -- sur -- per des hon -- neurs que j’ai peu me -- ri -- tez.
}
\tag #'(semiramis basse) {
  Vô -- tre va -- leur ar -- dente à nous dé -- fen -- dre,
  Ré -- vele en vous le sang, ou des Rois, ou des Dieux.
  Et quand je vous é -- leve à ce rang glo -- ri -- eux
  Je crois vous le don -- ner bien moins que vous le ren -- dre.
  Et quand je vous é -- leve à ce rang glo -- ri -- eux
  Je crois vous le don -- ner bien moins que vous le ren -- dre.
  Chan -- tez, Peu -- ples, chan -- tez, chan -- tez, ré -- ü -- nis -- sez vos voix,
  Ce -- le -- brez ce He -- ros,
  Ce -- le -- brez ce He -- ros, a -- plau -- dis -- sez mon choix.
}
