\clef "basse" sol,4 |
sol2 do2 |
fa sib, do |
re re8 do sib, la, |
sol,2 sol8 fa mib re |
do2 fa8 mib re do |
sib,1 |
mib |
do2 fa4 fa, |
sib,2. |
sib |
la |
sib2~ sib8 la |
sol2 do4 |
fa2 sib,4 |
mib2. |
re |
do |
do'2 sib4 |
la2. |
sol |
fa1 |
mib2 re4 do |
sib,2 sib4 |
do'2. |
dod' |
re'2 sol4 |
do2 la,4 |
re2. |
sol2 sol,4 |
re sol,2 |
<>^"notes égales" sol8 fad sol la sib sol |
do' sib la sol fad re |
sol4 sol,4. re8 |
sol la sib2 |
la8 sol la fa sib la |
sol fa mib2\trill |
re2.\trill |
R2. |
sol4 sol8 fa sol la |
sib la sol fa mi re |
la sol la sib la sol |
fa mi re mi fa sol |
la sib la sol fa4 |
sol8 la sib sol la re |
la,2. |
re8 do re mi fad re |
sol lab sol fa mib re |
do re mib re do sib, |
la, sib, do sib, la, sol, |
re do re mib re do |
sib, sol, do sib, la, sol, |
re do re mi fad re |
sol fad sol re mib do |
re4 re,2 |
sol, r4 |
r r sol8 fa |
mi4. re8 do4 |
fa8 sol fa mib re4 |
mib fa fa, |
sib,8 la, sib, do re sib, |
mib re do re mib do |
re do re mi fad re |
sol lab sol fa mib re |
do re mib do sol4 |
sol8. fa16 mib2 |
re2.
