<<
  %% Semiramis
  \tag #'(semiramis basse) {
    \tag #'basse \semiramisMark \clef "vbas-dessus"
    re''4 |
    sib'4. sol'8 mib''4 mib'' |
    mib''4. re''8 re''4. do''8 do''4.\trill sib'8 |
    la'4.\trill re''8 fad' fad' sol' la' |
    \appoggiatura la'8 sib'4. re''8 si' sol' do'' re'' |
    \appoggiatura re'' mib''4. mib''8 do''\trill do'' re'' mib'' |
    \appoggiatura mib''? fa''4. mib''!8 re'' do'' sib' la' |
    sol'4\trill sol'8 sib' sib' do'' re'' sib' |
    mib''4 mib''8 re'' do''4\trill\prall re''8 mib'' |
    re''4\trill re'' <<
      \tag #'basse { s4 s2.*11 s1*2 s2.*7 s4 }
      \tag #'semiramis { r4 R2.*11 R1*2 R2.*7 | r4 }
    >> <>^"mesuré" \tag #'basse \semiramisMark sib'4 sib'8 sol' |
    re''2 re''4 |
    \appoggiatura re''8 mib''4. re''8 do'' re'' |
    sib'4\trill sol' r8 la' |
    sib' do'' re''4.\trill mib''8 |
    \appoggiatura mib'' fa''2 re''8 fa'' |
    sib'2 sib'8 do'' |
    \appoggiatura do''8 re''2. |
    la'4 la'8 la' si' do'' |
    si'2 si'8 dod'' |
    re''2 mi''8 fa'' |
    dod''2 r8 la' |
    la'4. sol'8 la' sib' |
    mi'2\trill la'4 |
    mi''4. fa''8 dod'' re'' |
    fa''4( mi''2)\trill |
    re''2. |
    si'4 si'8 si' do'' re'' |
    \appoggiatura re'' mib''2 mib''8 re'' |
    do''2\trill do''8 sib' |
    la'2\trill r8 re'' |
    sol'4. mi'8 la' sib' |
    fad'2\trill re''4 |
    sib'4.\trill\prall la'8 sol' do'' |
    sib'4( la'2)\trill |
    sol'2 re''8 do'' |
    si'4. la'8 sol'4 |
    do''2 do''4 |
    la'2\trill re''4 |
    do''8 sib' la'4.\trill sib'8 |
    sib'2 sib'8 la' |
    sol'[ la'16 sib'] la'4.\trill sol'8 |
    fad'2 la'8 re'' |
    si'2\trill do''8 re'' |
    \appoggiatura re''8 mib''2 r8 sib' |
    sib' do'' do''4.\trill re''8 |
    re''2.
  }
  %% Arsane
  \tag #'(arsane basse) {
    <<
      \tag #'basse { s4 s1 s1. s1*6 s2 \arsaneMark }
      \tag #'arsane { \clef "vhaute-contre" r4 | R1 R1. R1*6 | r4 r }
    >> r8 re' |
    re'4. re'8 re' sib |
    fa'2 fa'4 |
    re'4.\trill re'8 re' fa' |
    sib2 mib'4 |
    mib'4. re'8 re' re' |
    sol2 do'8 do' |
    do'2\trill do'8 si |
    \appoggiatura si8 do'2 do'4 |
    mi'4 mi'8 mi' mi' sol' |
    do'2 fa'8 fa' |
    fa'2 fa'8 mi' |
    fa'2 do'4 re'8 mib' |
    la2 sib4 sib8 la |
    sib2 re'4 |
    mi'2 mi'8 fad' |
    sol'2 sol'8 la' |
    fad'4 re' re'8 re' |
    \appoggiatura re'8 mib'2 do'8 do' |
    do'2 do'8 re' |
    sib2\trill sib8 la |
    la4\trill
    \tag #'arsane { r4 r | R2.*35 }
  }
>>
