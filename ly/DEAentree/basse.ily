\clef "basse" re'2 re4 |
sol2 re'4 |
dod' re' sib |
la4. sib8 la sol |
fa4 sol la |
re2 re4 |
sol2 sol4 |
la sol fa8 mi |
re sol, la,2 |
re la4 |
re'2 re4 |
sol2 sol4 |
do' fa sol |
do2 do'4 |
re'2 re'4 |
mi'2 do'4 |
re' mi' mi |
la2 la4 |
re'2 re4 |
sol2 sol4 |
do'2 do4 |
fa2 fa4 |
sol2 sol4 |
mi la8 sol fa4 |
dod re2 |
la8 sol la si dod' la |
re'2 re4 |
sol2 re'4 |
dod' re' sib |
la4. sib8 la sol |
fa4 sol la |
re2 re4 |
sol2 sol4 |
la sol fa8 mi |
re sol, la,2 |
re2. |
