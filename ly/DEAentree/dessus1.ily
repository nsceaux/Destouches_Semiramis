\clef "dessus" <>^"Flutes" fa''4. sol''8 la''4\trill |
sib''2 la''4 |
la''4. sib''8 sol''4\trill |
la''2 mi''4 |
la''4. sol''8 fa''\trill mi'' |
fa''4 re'' la'' |
sol''4.\trill la''8 fa''4 |
mi''2 fa''4~ |
fa''8 sol'' mi''4.\trill re''8 |
re''2 mi''4 |
fa''4. mi''8 fa''4 |
re''4.\trill do''8 re''4 |
mi''4. fa''8 re''( mi'') |
mi''2\trill do'''4 |
si''4. do'''8 la''4 |
sold''2 do'''4 |
si''4.\trill la''8 sold''4 |
la'' mi'' do''' |
la''4. sol''8 la''4 |
sib'' sol'' sib'' |
sol''4.\trill fa''8 sol''4 |
la'' fa'' la'' |
sib''8 la'' sol'' fa'' mi'' re'' |
sol''4 mi'' la'' |
sol'' fa''4.(\trill mi''8) |
mi''2.\trill |
fa''4. sol''8 la''4\trill |
sib''2 la''4 |
la''4. sib''8 sol''4\trill |
la''2 mi''4 |
la''4. sol''8 fa''\trill mi'' |
fa''4 re'' la'' |
sol''4.\trill la''8 fa''4 |
mi''2 fa''4~ |
fa''8 sol'' mi''4.\trill re''8 |
re''2. |
