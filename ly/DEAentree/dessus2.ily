\clef "dessus" <>^"Flutes" re''4. mi''8 fad''4 |
sol''2 fa''4 |
mi'' fa'' \appoggiatura mi''16 re''4 |
dod''2 dod''4 |
fa''4. mi''8 re''\trill dod'' |
re''4 la' fa'' |
mi''4.\trill fa''8 re''4 |
dod''2 re''4~ |
re''8 mi'' dod''4.\trill re''8 |
re''2 dod''4 |
re''4. do''8 re''4 |
si'4.\trill la'8 si'4 |
do''4. re''8 si'4 |
do''2 mi''4 |
re''4. mi''8 do''4 |
si'2 mi''4 |
re''4.\trill do''8 si'4 |
do''4 la' la'' |
fad''4. mi''8 fad''4 |
sol''4 re'' sol'' |
mi''4.\trill re''8 mi''4 |
fa'' do'' fa'' |
fa'' mi''8 re'' dod'' si' |
mi''4 dod'' fa'' |
mi''4 re''4.\trill( dod''8) |
dod''2.\trill |
re''4. mi''8 fad''4 |
sol''2 fa''4 |
mi'' fa'' \appoggiatura mi''16 re''4 |
dod''2\trill dod''4 |
fa''4. mi''8 re''\trill dod'' |
re''4 la' fa'' |
mi''4. fa''8 re''4 |
dod''2\trill re''4~ |
re''8 mi'' dod''4.\trill re''8 |
re''2. |
