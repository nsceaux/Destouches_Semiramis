\clef "dessus" sol''4. sol''8 |
sol''4. la''16 sol'' fad''4. mi''8 |
re''2~ re''4.*1/3 sol''16[ fa'' mib'' re'' do'' sib'] |
la'4. sib'16 do'' sib'4.\trill la'8 |
sol'4. re''8 sol''8. la''16 la''8.\trill( sol''32 la'') |
sib''4 sib'8 sib' mib''2~ |
mib''4 re''8 re'' sol''2~ |
sol''8 sib'16 la' sol' la' sib' do'' do''4.\trill re''8 |
re''2 sol''4. sol''8 |
re''4.*13/12 fad''32 sol'' la'' sib''8. la''16 sol''8. fa''?16 |
mi''8. mi''16 fa''8. fa''16 sol''8. sol''16 sol''8.\trill( fa''32 sol'') |
la''8. fa''16 sib'' la'' sol'' fa'' sol'' fa'' mib'' re'' do''8.\trill sib'16 |
sib'8 sib'16 do'' re''8 sib' fa''4 fa'' |
r8 do''16 re'' mib''8 do'' sol''4 sol'' |
r8 re''16 mi'' fa''8 fa''16 sol'' la''8 la'' la'' sol''16 la'' |
sib'' la'' sol'' fa'' sol'' fa'' mi'' re'' dod''8 re''16 mi'' mi''8.\trill re''16 |
re''4. r16 re''32 mi'' fa''2~ |
fa''4 mi''8\trill re'' mib''2~ |
mib''4 fa''8 mib'' re''2~ |
re''4 do''8\trill sib' do''2~ |
do''4.*5/6 re''16 la' sib' sib'4.\trill la'8 |
la'4 re'' re'' re'' |
la''2.( sol''8\trill) fad'' |
sol''4. la''8 la''4.\trill sol''8 |
sol''4.*13/12 fad''32 sol'' la'' sib''8. la''16 sol''8. fa''?16 |
sol''1 |
