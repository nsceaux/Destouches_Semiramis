\clef "haute-contre" re''4. re''8 |
do''2. do''4 |
sib'2. sol'4 |
sol'2 sol'4.\trill fad'8 |
sol'4. sib'8 sib'8. do''16 do''8.(\trill sib'32 do'') |
re''2 r4 sol'8 sol' |
la'4 sib'8 la' sol'2~ |
sol' sol'4. la'8 |
fad'2 re''4. re''8 |
fad'4. re''8 re''4 re''8. re''16 |
do''8. do''16 do''4 sib' sib'8. sib'16 |
la'8. la'16 re''8. re''16 sib'8. sib'16 la'8.\trill sib'16 |
sib'4 r r8 re''16 do'' sib'8 re'' |
la' si' do''4 r8 mib''16 re'' do''8 mib'' |
sib'4 la' re''8 re'' re''4 |
re''16 do'' sib' la' sib' la' sol' fa' mi'8 la' la'8. sol'16 |
fad'4 r r re'' |
si'2 r4 do'' |
do''2 r4 sib' |
sib'2 r4 la' |
la'2 sol'4. la'8 |
fad'2 r |
r4 re'' re'' re'' |
sib'4. mib''8 re''4. la'8 |
sib'4. re''8 re''4 re''8. re''16 |
sib'1 |
