\clef "basse" sol,2 |
la,1 |
sib, |
do2 re4 re, |
sol,4. sol8 mib4 do |
sol2 r4 do8 do |
fad4 sol8 la sib4 sib,8 sib, |
mib1\trill |
re2 sol, |
re4.*13/12 re32 mi fad sol8. la16 sib8. sol16 |
do'8 sib la4 sol do'8 do |
fa4 re mib fa8 fa, |
sib,4 r r8 sib,16 do re8 sib, |
fa8 fa, do4 r8 do16 re mib8 do |
sol sol, re re16 mi fa8 re sib4 |
sol4. sol8 la re la,4 |
re2 r4 re' |
sol2 r4 do' |
fa2 r4 sib |
mi2 r4 la |
re2 sol4 sol, |
re2 r |
r4 re' re' re' |
mib' do' re' re |
sol4.*13/12 re32 mi fad sol8. la16 sib8. sol16 |
sol,1 |
