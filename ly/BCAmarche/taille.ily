\clef "taille" sib'4. sib'8 |
sib'2 la' |
sol'2. re'4 |
mib'2 re'4. la8 |
sib4. sol'8 sol'4 sol' |
sol'2 r4 do'8 do' |
do'4 sib8 do' re'2~ |
re' do'4. mib'8 |
la2 sib'4. sib'8 |
la4. la'8 sol'4 sol'8. sol'16 |
sol'4 la' re' mi'8. mi'16 |
fa'4 fa' mib' mib'8. re'16 |
re'4 r r8 fa' fa'4 |
fa'4 mib' r8 mib'16 fa' sol'8 sol' |
sol'4 fa' fa'8 fa' fa' sol' |
sol'4. sib'8 la' fa' dod' re' |
re'2 r4 la'8 re' |
re'2 sol'4. sol'8 |
la'2 r4 fa' |
sol'2 r4 mi' |
fad' re' re'4. re'8 |
re'2 r |
r4 fad' fad' la' |
sol' sol' fad'4. sol'8 |
sol'4. la'8 sol'4 sol'8. sol'16 |
sol'1 |
