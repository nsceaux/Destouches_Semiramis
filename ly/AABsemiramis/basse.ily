\clef "basse" R2. |
fa4 lab fa |
si,2. |
mib4 sol mib |
la, si, sol, |
do2 r4 |
sib,2 r4 |
lab,2. |
sol,4. sol8 la si |
do'2 sol4 |
lab2 fa4 |
sol4. r8 r4 |
r4 r8 do re mi |
fa2 fa,4~ |
fa, sol,2 |
do2. |
R2. |
do4 mi do |
fa,2 r4 |
re fad re |
sol,2 r4 |
do2 r4 |
sib,2 r4 |
lab,2 r4 |
sol,2 sol8 fa |
mib2 r4 |
fa2 r4 |
sol2 r4 |
R2. |
r4 r8 re mib fa |
sol4 sol,2 |
do4. r8 r4 |
r4 r8 mib fa sol |
lab2 fa4 |
sol sol,2 |
do2. |
r4 r r8 sol |
do4. do8 do do |
fa2 fa,4 |
sib,2. |
mib4. reb8 do sib, |
lab,2. |
sib, |
mib4. fa8 sol1 |
fad8 mi fa2 |
mi8 re mib2 |
re8 mib re do sib, la, |
sol,2. |
sol |
do |
re |
fad, |
sol, |
R |
do4 mi do |
fa,2 r4 |
re4 fad re |
sol,2 r4 |
do2 r4 |
sib,2 r4 |
lab,2 r4 |
sol,2 sol8 fa |
mib2 r4 |
fa2 r4 |
sol2. |
R2. |
r4 r8 re mib fa |
sol4 sol,2 |
do2 r4 |
r4 r8 do re mi |
fa2 fa,4~ |
fa, sol,2 |
do r4 |
fa,2 sol,4 |
lab,2 la,4 |
si,2. |
do2 r r4 lab8 sol |
fa2 fa, sol,4 lab, |
sib,2 r r4 fa8 sol |
lab2 la |
sib lab |
sol4 mib fa fa, |
sib,2. sib,4 |
do2. do4 |
re2. |
sol |
do |
re4 re,2 |
sol,2. |
do4 re2 |
mib2. |
lab2 sol |
fa1 sib,4 lab, |
sol,2 la,4 sib, |
do2 sol,4 lab, |
sib,2 lab, |
sol,16 lab, sol, lab, sib, lab, sol, fa, mib,4 mib |
fa2. r4 |
r2 fa4 re |
mib2 mi |
fa fad |
sol r |
R1 |
lab4 sol fa re |
sol fa mib do |
sol fa sol sol, |
do1 |
R1 |
fa4 mib re si, |
mib re do la, |
re do si, sol, |
do fa, sol,2 |
do,1 |
re,2 r4 |
mib,2 r4 |
fa,2 r4 |
fad,2 r4 |
sol,2 r4 |
la,2 r4 |
si,2 r4 |
do2 sib,4 |
lab,2. |
sol, |
