\clef "dessus" do''4 mib''4. do''8 |
fa''2. |
re''2 re''8 re'' |
sol''2. |
r4 r8 fa'' sol'' re'' |
mib''2 lab''4 |
lab''2 sol''4 |
sol''( fa''8)\trill mib'' re'' do'' |
si'2. |
r4 r8 mib'' fa'' sol'' |
do''4. fa''8 fa''\trill mib'' |
re''4.\trill sol''8 la'' si'' |
do'''2~ do'''8 sib''( |
lab'')( sol'') fa''( mib'') re''4~ |
re''8( mib''16 fa'') re''4.\trill do''8 |
do''2. |
\clef "dessus2" R2. |
mi'4 sol' mi' |
fa'2 r4 |
fad'4 la' fad' |
sol'2 r4 |
sol'4. r8 fa''8( mib'') |
re''2 mib''8( re'') |
do''4.( re''16) mib'' re''8.\trill do''16 |
si'4.\trill re''8 mib'' fa'' |
sol''2 sol''4 |
do''4. do''8 re'' mib'' |
re''2\trill r4 |
r4 r8 mib'' fa'' sol'' |
si'2 r8 mib'' |
mib''4( re''4.)\trill do''8 |
do''4. re''8 mib'' fa'' |
sol''2.~ |
sol''4( fa''8)\trill( mib'') re'' do'' |
do''4( si'4.\trill) do''8 |
do''2 r4 |
r r r8 si' |
do''4. do''8 do'' do'' |
la'2 la'8 sib' |
sib'2 r8 sib' |
sib'4. mib'8 fa' sol' |
lab'2 lab'8 lab' |
fa'2 fa'8 sib' |
sol'2 r sib'4 sib' |
re''2 re'4 |
re' do'2\trill |
re'4. re'8 mi' fad' |
sol'2 re'4 |
si'2 si'8 si' |
do''2( la'4) |
fad'2 fad'8 sol' |
la'2 la'4 |
re'2. |
R2. |
mi'4 sol' mi' |
fa'2 r4 |
fad'4 la' fad' |
sol'2 r4 |
sol'4. r8 fa''( mib'') |
re''2 mib''8( re'') |
do''4.( re''16) mib'' re''8.\trill do''16 |
si'4. re''8 mib'' fa'' |
sol''2 sol''4 |
do''4. do''8 re'' mib'' |
re''2. |
r4 r8 mib'' fa'' sol'' |
si'2 r8 mib'' |
mib''4( re''4.)\trill do''8 |
do''4. \clef "dessus" sol''8 la'' si'' |
do'''2~ do'''8( sib'') |
lab''( sol'') fa''( mib'') re''4~ |
re''8( mib''16 fa'') re''4.\trill do''8 |
do''4 \clef "dessus2" mib'4. mib'8 |
lab'2 sol'4 |
sol'2 fa'8 fa' |
re'2 sol'4 |
sol'2 r4 sol'8 fa' mib'4 mib' |
mib'1 mib'4 mib' |
re'2 r4 re'8 mi' fa'4 fa'8 mi' |
fa'4 lab'8 sib' do''4. do''8 |
fa'2 r4 fa'8 fa' |
sib'4 sol' fa' fa'8 fa' |
re'2. sib'4 |
la' la'8 sib' do''4 do''8 sib' |
la'2. |
sol'2 sol'4 |
sol'2 sol'4 |
fad'8 sol' fad'4. sol'8 |
sol'2. |
mib'4 fa'2 |
sib2 sol'4 |
sol' fa'8 fa' fa'4 mi' |
fa'2 r4 la'!8 la' sib'4 sib'8 sib' |
sib'4 sib'8 sib' mib'4 re' |
mib'2 mib'4 mib' |
mib'2 re' |
mib'2 r4 sol' |
la'2. r4 |
r2 si'4 si'8 si' |
do''2 r4 sol' |
do'2 r4 la' |
re'2 r |
R1 |
do''4 sib' lab' fa' |
re' re' mib'2~ |
mib'4. re'8 re'4.\trill do'8 |
do'1 |
\clef "dessus" do''4 do''8 do'' fa''4 sol'' |
lab''2. r4 |
sol''2. r4 |
fa''4. fa''8 fa''4 sol''8 re'' |
mib''4. re''8 re''4.\trill do''8 |
do''2 \clef "dessus2" r4 do' |
do'2 sib4 |
sib2 mib'4 |
mib'2 re'4 |
la'2 la'4 |
re'2 sol'4 |
sol'2 fa'4 |
fa'2 mib'8 re' |
mib'2 re'4 |
do'8.( re'16) re'4.\trill do'8 |
si2.\trill |
