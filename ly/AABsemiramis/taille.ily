\clef "taille" R2. |
do'4 do' fa' |
fa'2. |
mib'2 sol'4 |
sol'2 si4 |
do'2 r4 |
fa'2 r4 |
lab'2 r4 |
re'4. si8 do' re' |
mib'2 si4 |
do'2 re'8 do' |
si4. r8 r4 |
r4 r8 mib' fa' do' |
do'2 sol'4 |
lab' sol'4. re'8 |
mib'2. |
R2. |
sol2 sol4 |
fa2 r4 |
la2 la4 |
si2 r4 |
sol2 r4 |
sib2 r4 |
mib'4 r lab' |
re'2 r4 |
mib'2 r4 |
re'2 r4 |
re'2 r4 |
R2. |
r4 r8 re' do'4 |
sol'2~ sol'8 re' |
mib'2 r4 |
r r8 mib' mib' re' |
do'2 fa'4 |
mib'( re'4.)\trill do'8 |
do'2 r4 |
r r r8 re' |
do'2 do'4 |
do'2 do'4 |
sib2. |
sib2 mib'8 reb'? |
do'2. |
fa' |
mib'2 r re' |
re'4 la2 |
sib4 sol4. la8 |
la2 re'8 do' |
sib2. |
re' |
do' |
la |
re'2 la4 |
sol2. |
R2. |
sol2 sol4 |
fa2 r4 |
la2 la4 |
si2 r4 |
sol2 r4 |
sib2 r4 |
mib'4 r lab' |
re'2 r4 |
mib'2 r4 |
re'2 r4 |
re'2 r4 |
R2. |
r4 r8 re' do'4 |
sol'2~ sol'8 re' |
mib'2 r4 |
r r8 mib' fa' do' |
do'2 sol'4 |
lab' sol'4. re'8 |
mib'4 r r |
do'2 sol4 |
mib2 fa4 |
sol2. |
sol2 r r4 lab |
lab1 sol4 fa |
fa2 r r4 do'8 sib |
lab4 do' do'2 |
sib2 r4 sib |
sib2 \footnoteHere #'(0 . 0) \markup\wordwrap {
  Source : \raise #1 \score {
    { \tinyQuote \clef "mezzosoprano" \key sol \minor
      sib2 sol4. sol8 | sol2. sol4 |
      do'2. do'4 | \digitTime\time 3/4 sib2. | }
    \layout { \quoteLayout }
  }
}
\sugNotes { fa4. fa8 } | % sol4. sol8 |
%{sol2.%} \sugNotes { fa2. } sol4 |
do'2. do'4 |
\sugNotes { la2. } | % sib2. |
sib2 sib4 |
sib2 la4 |
la re'4. do'8 |
si2. |
do'4 fa2 |
sol2. |
lab2 sib |
do' r4 do' sib2 |
sib4 sol sol fa |
mib2 sol4 fa |
fa2 fa |
sol2 r4 sib |
fa2. r4 |
r2 re'4. re'8 |
do'2 r4 sol |
fa2 r4 la |
sol2 r |
R1 |
lab2 lab |
sol sol |
sol sol4. fa8 |
mib1 |
R1 |
re'2 r |
do' r |
si2 si4 sol' |
sol'4 lab' sol'4. re'8 |
mib'2 r4 mib |
fa2 r4 |
mib2 r4 |
do'2 r4 |
re'2 r4 |
sol2 r4 |
do'2 r4 |
sol2 r4 |
sol2 sib4 |
mib2 lab4 |
re2. |
