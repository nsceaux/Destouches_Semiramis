\clef "haute-contre" R2. |
lab'4 do'' lab' |
sol'2. |
do''2 do''4 |
do'' re''2 |
sol' r4 |
re''2 r4 |
do''2 r4 |
sol'2. |
r4 r8 sol' fa'4 |
mib'2 lab'4 |
sol'4. r8 r4 |
r4 r8 sol' fa' sol' |
lab'4 do'' si'~ |
si'8 do''16 re'' si'4. do''8 |
do''2. |
R2. |
do'2 do'4 |
do'2 r4 |
re'2 re'4 |
re'2 r4 |
do'2 r4 |
fa'2 r4 |
do''2 r4 |
sol'2 r4 |
do''2 r4 |
do''2 r4 |
sol'2 r4 |
R2. |
r4 r8 fa' sol' lab' |
sol'2~ sol'8 sol' |
sol'2 r4 |
r r8 sol' la' si' |
do''2 lab'4 |
sol'2~ sol'8 re' |
mib'2 r4 |
r r r8 sol' |
sol'2 sol'4 |
fa'2 fa'4 |
fa'2. |
mib'2 mib'4 |
mib'2. |
sib |
sib2 r sol' |
la'2 la4 |
sol2 do'4 |
la2 sib8 do' |
re'2. |
sol' |
sol' |
re' |
re' |
re' |
R |
do'2 do'4 |
do'2 r4 |
re'2 re'4 |
re'2 r4 |
do'2 r4 |
fa'2 r4 |
do''2 r4 |
sol'2 r4 |
do''2 r4 |
do''2 r4 |
sol'2 r4 |
R2. |
r4 r8 fa' sol' lab' |
sol'2~ sol'8 sol' |
sol'2 r4 |
r r8 sol' fa' sol' |
lab'4 do'' si'~ |
si'8( do''16 re'') si'4. do''8 |
do''4 do'4. do'8 |
fa'2 re'8 si |
do'2 do'4 |
re'2 fa'4 |
mib'2 r r4 do' |
do'1 fa'4 do' |
sib2 r r4 do' |
do'2 fa'4. fa'8 |
fa'2 r4 fa' |
mib'2 do'4.\trill sib8 |
sib2. sol'4 |
sol'2. sol'4 |
re'2. |
re'2 re'4 |
mib'2 mib'4 |
re'4 re'4. re'8 |
re'2. |
do'4 sib2 |
sib mib'4 |
mib'4. do'8 re'4. sib8 |
lab2 r4 fa' fa'2 |
sol'2 do'4 re' |
do'2 mib'4 do' |
sib2 sib |
sib r4 mib' |
do'2. r4 |
r2 sol'4. sol'8 |
sol'2 r4 do' |
do'2 r4 re' |
re'2 r |
R1 |
fa'2 re' |
re' do' |
sol sol4. sol8 |
sol1 |
R1 |
fa'2 fa'4 re' |
sol'2 mib'4 do' |
fa'2 re''4 si' |
do''4. si'8 si'4.\trill do''8 |
do''2 r4 do' |
lab2 r4 |
sol2 r4 |
do'2 r4 |
do'2 r4 |
re'2 r4 |
do'2 r4 |
re'2 r4 |
do'2 sol4 |
do'2. |
sol |
