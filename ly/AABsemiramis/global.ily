\key sol \minor \midiTempo#120
\digitTime\time 3/4 s2.*43
\time 3/2 s1.
\digitTime\time 3/4 s2.*28 s4 \tempo "Vivement" s2 s2.*3
\time 3/2 \grace s8 s1.*3
\time 2/2 s1*5
\time 3/4 s2.*7
\time 2/2 s1
\time 3/2 \grace s8 s1.
\time 2/2 s1*20
\time 3/4 s2.*10 \bar "|."
