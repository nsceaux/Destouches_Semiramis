\clef "vbas-dessus" R2.*16 |
sol'4 si'4. sol'8 |
do''2. |
la'2 la'8 la' |
re''2 re''4 |
r4 r8 si' do'' re'' |
\appoggiatura re''8 mib''4. r8 lab'4 |
lab'2 sol'4 |
sol'4.( fa'8)\trill[ mib'] fa'8 |
\appoggiatura fa'8 sol'2. |
r4 r8 sol' la' sib' |
la'4.\trill la'8 si' do'' |
si'2\trill si'4 |
r r8 do'' re'' mib'' |
fa''2( mib''8)\trill re''16[ do''] |
do''4( si'4.) do''8 |
do''2 r4 |
R2.*3 |
r4 r r8 do'' |
sol'4. sol'8 sol' sol' |
mib''2 mib''8 mib'' |
do''2\trill\prall do''8 fa'' |
re''4\trill re'' r8 sib' |
sol'4.\trill sol'8 lab' sib' |
\appoggiatura sib'8 do''2 do''8 re'' |
re''2\trill re''8 mib'' |
mib''2 r re''4 re'' |
la'2 la'8 re'' |
sol'2 r8 do'' |
fad'4. fad'8 sol' la' |
\appoggiatura la'8 sib'2 sol'4 |
re''2 mib''8 fa'' |
mib''2\prall \appoggiatura re''8 do''4 |
la'2\trill la'8 sib' |
do''4. do''8 re''4 |
si'2.\trill |
sol'4 si'4. sol'8 |
do''2 r4 |
la'2 la'8 la' |
re''2 re''4 |
r4 r8 si' do'' re'' |
\appoggiatura re''8 mib''4. r8 lab'4 |
lab'2 sol'4 |
sol'4.( fa'8)\trill[ mib'] fa' |
\appoggiatura fa'8 sol'2. |
r4 r8 sol' la' sib' |
la'4.\trill la'8 si' do'' |
si'2\trill si'4 |
r4 r8 do'' re'' mib'' |
fa''2( mib''8)\trill re''16[ do''] |
do''4( si'4.\trill) do''8 |
do''2 r4 |
R2.*3 |
r4 sol'4. sol'8 |
re''2 re''8 mib'' |
do''2 fa''8 fa'' |
fa''2 mib''8 re'' |
\appoggiatura re''8 mib''4 mib'' r mib''8 re'' do''4 do''8 mib'' |
lab'2 lab'4 lab'8 lab' sib'4 do'' |
fa'2 r4 fa'8 sol' lab'4 lab'8 sib' |
do''4 do''8 re'' mib''4 mib''8 fa'' |
re''4\trill re'' r re''8 re'' |
\appoggiatura re''8 mib''4 do'' la' la'8 sib' |
sib'2. re''4 |
do''\trill do''8 sib' la'4\trill sib'8 sol' |
fad'2\trill fad'4 |
sib'4 sib'8 sib' la' sol' |
mib''2 la'4 |
la'8 sib' la'4.\trill sol'8 |
sol'2. |
lab'4 lab'8 lab' lab' sib' |
\appoggiatura lab' sol'2 sol'4 |
do''4 do''8 reb'' sib'4\trill lab'8 sol' |
\appoggiatura sol'8 lab'4 lab' r fa''8 mib'' re''4\trill re''8 sib' |
mib''4 mib''8 re'' do''4 sib'8 lab' |
sol'4.\trill lab'8 sib'4. do''8 |
fa'2 sib'4 sib'8 sib' |
mib'4 mib' r mib'' |
mib''2 mib''4 r |
r2 re''4 re''8 re'' |
sol'2 r4 do''8 do'' |
la'2 r4 re''8 mib'' |
si'2 r |
sol'4 sol'8 sol' re''4\trill mi'' |
fa''1 |
si'4 si' do''2~ |
do'' do''4 si' |
do''1 |
R1*4 |
r2 r4 sol' |
mib'2 lab'4 lab'8 lab' |
fa'2\trill r8 sib' |
sol'4. sol'8 la' sib' |
la'4\trill la'8 r re''4 |
re''4. la'8 si' do'' |
si'2 si'4 |
do''4. do''8 re'' mib'' |
re''2 sol'8 sol' |
sol'4. lab'8 sol'4 |
sol'( fa'4.)\trill sol'8 |
sol'2. |
