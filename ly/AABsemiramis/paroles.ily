Pom -- peux A -- prêts, Fête é -- cla -- tan -- te,
Flam -- beaux sa -- crez, Au -- tels or -- nez __ de fleurs,
Hy -- men si cher à mon at -- ten -- te,
Que vous m’al -- lez __ cou -- ter __ de pleurs !

Ri -- va -- le des He -- ros, que de -- vient ma puis -- san -- ce ?
A -- vec un In -- con -- nu, j’en par -- ta -- ge l’é -- clat ;
Je la mets à ses pieds, ma gloi -- re s’en of -- fen -- se,
Et mon a -- mour en -- cor craint de faire un in -- grat.

Pom -- peux A -- prêts, Fête é -- cla -- tan -- te,
Flam -- beaux sa -- crez, Au -- tels or -- nez __ de fleurs,
Hy -- men si cher à mon at -- ten -- te,
Que vous m’al -- lez __ cou -- ter __ de pleurs !

Quels re -- pro -- ches Ni -- nus, n’as- tu point à me fai -- re ?
A pé -- rir en nais -- sant, j’ai con -- dam -- né mon Fils.
Pour é -- tein -- dre la race, & les droits de ton fre -- re,
Aux Au -- tels j’en -- chaîne A -- mes -- tris :
Et c’est u -- ne main é -- tran -- ge -- re,
Qui de mes at -- ten -- tats va re -- cueil -- lir le prix.
Tris -- te Se -- mi -- ra -- mis,
Faut- il que ton cœur te tra -- his -- se ?
Plus cru -- els que les dieux qui dé -- so -- lent ces bords,
L’A -- mour te guide au pre -- ci -- pi -- ce.
Ar -- rê -- te. Il n’est plus temps. quels com -- bats ! quels re -- mords ?
Jus -- ti -- fi -- ez, grands Dieux, ou cal -- mez __ mes trans -- ports.
On vient. C’est A -- mes -- tris... quelle est mon in -- jus -- ti -- ce !
Cap -- ti -- ve dès long- tems, quels maux elle a souf -- ferts !
Je ne fais que chan -- ger __ ses fers.
