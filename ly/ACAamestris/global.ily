\key do \major \tempo "Tendrement"
\digitTime\time 3/4 \midiTempo#120 s2.*51
\key sol \minor \tempo "Vivement" s2.*4
\time 3/2 \grace s8 s1.
\digitTime\time 3/4 s2.*2
\time 2/2 s1
\digitTime\time 3/4 s2.*8 s2 \bar "|."
