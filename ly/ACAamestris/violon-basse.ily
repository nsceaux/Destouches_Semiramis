\clef "dessus2" <>^"Violons" r4 r do'' |
mi'2 fa'4 |
re'2 re'4 |
sol'2. |
do''2 sol'4 |
la'2 fa'4 |
sol'2. |
do'2 r4 |
r r do'' |
fad'2 r4 |
fa'!2 r4 |
mi'2 r4 |
fad'2 r4 |
sol'2 r4 |
re'2 r4 |
fa'2 r4 |
mi'8 fa' sol'2 |
do'2 do''4 |
fa'' sol'' sol' |
do''2. |
sold'4 sol'2 |
fad'4 fa'2 |
mi'2 r4 |
do'2 r4 |
re'2 r4 |
mi'2 r4 |
la'2 r4 |
R2. |
la2 r4 |
si2 r4 |
do'2 r4 |
fa'2 re'4 |
sol'2. |
do''4. si'8 la'4 |
sol'2.~ |
sol'~ |
sol'~ |
sol'2 re'4 |
mi'2 mi'4 |
fa'2.~ |
fa'~ |
fa'~ |
fa'2 mi'8 do' |
sol'2. |
do'4 do''4.( si'16\trill la') |
sib'2~ sib'8( la'16\trill sol') |
la'2~ la'8 sol'16\trill fa' |
sol'2 r4 |
fad'2 r4 |
sol'2. |
do'2. |
%%%
<<
  \tag #'violon {
    \clef "dessus2" <>^"Violons" sol'2 r4 |
    sol'2 r4 |
    sol'2 fa'4 |
    fa'4. re'8 mib'8 si |
    do'4. re'8 mib'4 mib'8 mib' mib'4 re' |
    mib'2 sol'4 |
    fa'4. re'8 mib' fa' |
    sol'2 sol'4 fad' |
    sol'2 sol'4 |
    sol'2 sol'4 |
    do''2 lab'4 |
    sol'4. sol'8 lab' fa' |
    re'2 r8 si' |
    do''4 sol'4. sol'8 |
    mib'2 lab'8 do'' |
    do''2 sol'4 |
    sol'2
  }
  \tag #'basse {
    \clef "basse" do'2 r4 |
    sib2 r4 |
    lab2 r4 |
    sol4. fa8 mib re |
    do4. sib,8 lab,2 sib, |
    mib, mib4 |
    re sol8 fa mib re |
    do2 re4 do |
    si,2 do4 |
    sib,!2. |
    lab,2 fa,4 |
    do2 fa4 |
    sol2 sol,4 |
    do2. |
    lab2 fa4 |
    sol sol,2 |
    do2*7/8~ \hideNotes do16
  }
>>
