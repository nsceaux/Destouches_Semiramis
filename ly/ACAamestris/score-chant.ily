\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse \includeNotes "violon-basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
