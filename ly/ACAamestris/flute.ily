\clef "dessus" <>^"Flutes" r4 r sol'' |
sol''2 la''4 |
\appoggiatura { la''16[ sol''] } fa''2~ fa''8 fa'' |
fa''2. |
mi''4 fa''4. sol''8 |
do''2 r8 re'' |
mi''4( re''2)\trill |
do''2 sol''4 |
sol''2 la''4 |
la''2( si''8) do''' |
si''2. |
r8 do''' re'''4. mi'''8 |
la''2 la''4 |
re''8.( mi''16) mi''4.(\trill re''16 mi'') |
fa''2 sol''8 la'' |
re''2. |
sol''8( fa''16 mi'') re''4.\trill do''8 |
do''4. do'''8 re''' mi''' |
la''8.\trill( si''16) si''4.\trill do'''8 |
do'''2 do'''4 |
mi'''8.( si''16) si''4 r8 mi'' |
mi''8. re''16 re''4 r |
si''8.( mi''16) mi''4 sold''8 sold'' |
la''2 la''8 do''' |
do'''2 si''4 |
si''8. la''16 sold''4.\trill la''8 |
la''2. |
la''2( sol''8)\trill fa'' |
mi''2( re''8) do'' |
re''2 sol''4 |
sol''8.*5/6 mi''32 fa'' sol'' sol''4.\trill( fa''16 sol'') |
la''4. la''8 la''( si''16 do''') |
si''8.*5/6( sol''32 la'' si'') si''4.\trill( la''16 si'') |
do'''4.( sol''8) sol'' la''16( si'') |
si''4.\trill la''8( sol''4) |
do'''2 re'''8 mi''' |
re'''4.\trill\prall do'''8 si'' do''' |
\appoggiatura do'''8 re'''2 fa''4 |
sol''2 sol''4 |
do''2 do'''4 |
do'''2. re'''4 mi'''4. fa'''8 |
si''2 r8 do''' |
do'''4( si''4.)\trill do'''8 |
do'''2 la''4~ |
la''8( sol''16\trill fa'') sol''2~ |
sol''8( fa''16\trill mi'') fa''2~ |
fa''4 si''4.( la''16\trill sol'') |
do'''2 do'''4 |
do'''8.*5/6( si''32 la'' si'') si''4.\trill do'''8 |
do'''2. |
