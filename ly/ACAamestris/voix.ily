\clef "vbas-dessus" R2.*7 |
r4 r mi'' |
mi''2 fa''4 |
\appoggiatura mi''8 re''2~ re''8 re'' |
re''2. |
r8 mi'' fa''4. sol''8 |
do''2~ do''8 sib'16[ la'] |
\appoggiatura la'8 sib'2 sib'4 |
r re'' mi''8 fa'' |
si'2. |
do''2 do''8 si' |
do''2 r4 |
R2. |
r4 r mi'' |
si' mi'' mi''8 do'' |
la'4\trill la'8 si'16 do'' si'8.\trill la'16 |
sold'2 si'8 si' |
mi''2 re''8 mi'' |
\appoggiatura mi''8 fa''2 fa''4 |
re''4.\trill\prall do''8 si' do'' |
do''4.\trill si'8( la'4) |
R2. |
do''2 si'8 la' |
sol'4 sol' sol'8 la' |
mi'2\trill mi''4 |
\appoggiatura re''8 do''4. la'8 fa'' fa'' |
fa''2. |
mi''4\trill mi''8 mi'' mi'' fad'' |
\appoggiatura fad'' sol''2 re''4 |
mi''2 fa''8 sol'' |
fa''4.\trill\prall mi''8 re'' do'' |
si'2 sib'4 |
sib'2 do''4 |
\appoggiatura sib'8 la'2~ la'8 la' |
la'2. |
fa''4 sol''4. la''8 |
re''2 r8 mi'' |
mi''4( re''2)\trill |
do''2 r4 |
R2.*6 |
mib''4. fa''8 sol''4 |
re'' re'' r8 sol'' |
do''2 fa''4 |
si'4. si'8 do'' re'' |
\appoggiatura re''8 mib''4. sol'8 do''4 do''8 lab' fa'4 sol'8 lab' |
sol'4\trill mib' sib' |
\appoggiatura do''8 si'4. si'8 do'' re'' |
mib''4 mib''8 do'' la'4 la'8 re'' |
sol'2 mib''4 |
\appoggiatura fa''8 mi''4. mi''8 mi'' fa'' |
fa''2 re''4 |
mib''4. re''8 do'' re'' |
si'4\trill si' r8 re'' |
mib''4. mib''8 fa'' sol'' |
do''4\trill do'' fa''8 mib'' |
re''4.\trill do''8 si'4 |
do''2
