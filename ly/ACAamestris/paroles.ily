Mes yeux, mes tris -- tes yeux, lais -- sez cou -- ler vos lar -- mes,
Foi -- ble se -- cours des mal -- heu -- reux.
Faut- il que d’un a -- mour, dont j’ai bra -- vé les feux,
L’im -- por -- tun sou -- ve -- nir me cau -- se tant d’al -- lar -- mes ?
Con -- tre des maux si ri -- gou -- reux
Faut- il que mes sou -- pirs soient mes u -- ni -- ques ar -- mes ?
Foi -- ble se -- cours des mal -- heu -- reux,
Mes yeux, mes tris -- tes yeux, lais -- sez cou -- ler vos lar -- mes.
Rei -- ne bar -- ba -- re, non, non, mes fers, ny ta ri -- gueur,
N’ont point é -- bran -- lé ma cons -- tan -- ce :
Ar -- sa -- ne, c’est pour toi que j’ai craint sa fu -- reur.
Ar -- sa -- ne, c’est à toi, qu’in -- gratte en ap -- pa -- ren -- ce,
Je vais don -- ner l’Em -- pi -- re au dé -- faut de mon cœur.
