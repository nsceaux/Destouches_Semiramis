\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse-continue #:notes "violon-basse"
                   #:tag-notes basse
                   #:clef "dessus2"
                   #:score-template "score-basse-continue-voix")
   (chant)
   (silence #:on-the-fly-markup , #{ \markup\tacet#68 #}))
