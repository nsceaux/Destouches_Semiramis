\clef "dessus" <>^\markup\whiteout Violons
sol''8 la'' sib'' la'' |
sol''4 re'' sol''8 la'' sib'' la'' |
sol''4 re'' sol'' la'' |
sib'' la''8 sol'' fad''4 sol'' |
la''2\trill re''8 mib'' fa'' sol'' |
fa''4 mib''8 re'' mib''4 re'' |
do'' la' fa''8 sol'' fa'' mib'' |
re''4 do''8 sib' do''4 la' |
sib'4. do''8 re''4 re'' |
re''8 do'' re'' mib'' re''4 sol' |
re''2 re''8 mi'' fa'' sol'' |
mi'' fa'' mi'' fa'' sol'' la'' sol'' la'' |
fad''4\trill re'' re'' re'' |
re''8 do'' re'' mib'' re''4 sol' |
re''4. mi''8 fad'' sol'' la'' fad'' |
sib'' la'' sol'' la'' fad''4.\trill sol''8 |
sol''2
