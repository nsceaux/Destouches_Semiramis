\piecePartSpecs
#`((dessus)
   (dessus-hc)
   (haute-contre)
   (taille)
   (basse)
   (basse-continue #:music , #{ s2 s1*7\break #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#32 #}))
