\clef "taille" r2 |
re' r |
re' sib4 do' |
re'2 la4 re' |
re'2 fa' |
fa' do'4 fa' |
fa'2 fa'4 fa' |
sol'2 sol'4 do' |
sib2. re'4 |
re'2 sib4 do' |
la2 re' |
do' mi' |
la4 re' re' sol' |
re'2 mib' |
la re' |
re'4 mib' re' la |
sib2
