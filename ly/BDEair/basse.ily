\clef "basse" r2 |
sol r |
sol mib4 do |
sol la8 sib la4 sol |
re'2 sib |
re do4 sib, |
fa8 sol fa mib re2 |
sol4 re mib fa |
sib,2. sol4 |
fa2 mib |
re sib, |
do la, |
re sol |
fa mib |
re do |
sib,4 do re re, |
sol,2
