\clef "haute-contre" r2 |
sib' r |
sib' sib'4 mib'' |
re'' do''8 sib' do''4 re'' |
fad'2 re'' |
re''4 do''8 sib' la'4 sib' |
la' fa' sib' sib' |
sib'2 sib'4 fa' |
re'2. sib'4 |
la'2 sol' |
fad' sib' |
sol'4 do'' dod'' dod'' |
re''4 la' sib'2 |
la' sol' |
fad'4. sol'8 la' sib' do'' la' |
re'' do'' sib' do'' la'4.\trill sol'8 |
sol'2
