\score {
  \new StaffGroup <<
    \new GrandStaff <<
      \new Staff <<
        \global \keepWithTag #'dessus1 \includeNotes "dessus"
      >>
      \new Staff <<
        \global \keepWithTag #'dessus2 \includeNotes "dessus"
      >>
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \includeNotes "haute-contre"
      \clef "treble"
    >>
  >>
  \layout { }
}
