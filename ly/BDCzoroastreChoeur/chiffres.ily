s4 s2 <_+>4 s <6 _->2 <_+>4 \new FiguredBass { <6 4> <_+> } s2. <6>2 <6+>4 <6> <6+>2 <_+>2\figExtOn <_+>4\figExtOff
<6>2 <6+>4 <5/>2. <_+> s2.*2 <6>2\figExtOn <6>4\figExtOff

s2 <6>4 s2. <_-> <"">2\figExtOn <"">4\figExtOff s2.*2
s2 <6+>4 <6>2. s <7 _+>2. s4 <_+>2 s2.

s2 <6+>4 <6>4\figExtOn <6>4.\figExtOff <7 _+>8 s2 <6>4 <7>2 <5 4> <_+> s

s4 <6>2 <6+>4 <6> <6+>2 <6>\figExtOn <6>4\figExtOff s2 <6>4 s2 <6 _->4 <_+>2.

s2.*3 <6>2\figExtOn <6>4\figExtOff s2.*3

<6 5>4 <3>2 s2. s2 <6 4+>4 <_->2 s8 <6> s2 s8 <6+> <6>2. s4 <6>2

<_+>2. s2 <6+>4 <6>8*5\figExtOn <6>8\figExtOff s2. <_+>1 <_+>2 s2. <6+>2\figExtOn <6+>4\figExtOff

s2. <_+> s4 <6>2 s2. <_-> s4 <6>2 <6 5>4 <4> <3> s4
s2 s2.*3 s4 <6 _-> <7 _+> <6 4>2 <7 _+>4
s2.*2 <_+>4 <6>\figExtOn <6>\figExtOff <_+>4 <6>4.\figExtOn <6>8\figExtOff s4 <_+>2 s

s4 <6>2 <6+>4 <6> <6+>2 <_+>2. <_+> <_+> s2

s4 <6>2 <6+>4 <6> <6+>2 <_+>2. s2.*3

<"">4\figExtOn <"">\figExtOff <6> <_+>2. <6>4. <6+> <_+>2. s <6>4 <6 4> <3+> s2.

s2.*2 <"">4\figExtOn <"">4\figExtOff <6> <_+>2. <6>4. <6+> <_+>2. <"">4.\figExtOn <"">8\figExtOff <6+>4

<6>2. <6>2 <6+>4 s <_+>2 <_+>2. s <_+> <_+>

s2.*4 <6 5/>2\figExtOn <6>4\figExtOff s2.

s2.*2 <_+>4 <_+>2 <_+>2. <6> s2 <_+>4

<_->2. s2.*3 s2 <6 4+>4 <_->2 s8 <6> s2

s8 <6+> <6>2. s4 <6>2 <_+>2. s2 <6+>4 <6>4\figExtOn <6>2\figExtOff s2.

<7 _+>1 <_+>2
