\clef "taille" r4 |
R2.*28 R1. |
r4 r re'4 |
re'2 re'8 re' |
re'4 re' re' |
re'2 fad4 |
sol2 la8 sol |
sib4. sib8 do'4 |
la2\trill la4 |
R2.*2 |
do'4 do' mi' |
do'4. do'8 do'4 |
re'4. re'8 re'4 |
re'2 sib4 |
sib4. sib8 sib4 |
do'4 do'4.\trill sib8 |
sib2. |
la4 la si |
do'4 sol4. la8 |
sib4 sib re'8 re' |
re'2. |
mi'4 mi'4.\trill mi'8 |
re'2. |
re'4 re' do' |
sib re'4. re'8 |
do'4 do' do'8 do' |
re'2 re' re'4. re'8 |
re'2 sib4 |
la do' re' |
re'2 re'4 |
re'2 re'4 |
sib mib'2 |
re' sol'4 |
sol'2 sol'4 |
fa' fa'2 |
sol'4 fa'4. mib'8 |
re'4 r r |
R2.*5 |
re'2 re'4 |
do' mi' mi' |
re' re' re' |
re' re'2 |
mib'4 re'4. la8 |
sib2 sib4 |
re'2 re'8 re' |
re'4 re' re' |
re'4 r r |
R2.*2 |
r4 r sib |
re'2 re'8 re' |
re'4 re' re' |
re'2 re'4 |
re'2 re'8 re' |
do'4. do'8 do'4 |
do'2 do'4 |
R2.*10 |
la4 re' dod' |
re'4. dod'8 re'4 |
dod'4. re'8 mi'4 |
re'2 mi'4 |
la4 la la |
re'4. re'8 dod'4 |
re' dod'4.\trill re'8 |
re'2. |
R2. |
la4 la re' |
re'4. do'8 si4 |
do'4. re'8 mi'4 |
fa'2. |
la4 la la |
sib4. sib8 sib4 |
do'4. sib8 la4 |
sib2 re'4 |
sib8[ la sib do' re' sib]( |
do'4.) do'8 do'4 |
dod'4 re'4. re'8 |
re'2 re'4 |
re'8[ mib' fa' mib' re' do']( |
re'4) re' si |
do'4 do' do' |
do'4. do'8 sib4 |
sib la4.\trill sib8 |
sib2. |
la4 la si |
do'4 do'4. la8 |
sib4 sib sib8 do' |
re'2. |
mi'4 mi'4. mi'8 |
re'2. |
re'4 re' do' |
sib4 re'4. re'8 |
do'4 do' do'8 do' |
re'2 re' re'4. re'8 |
sib2. |
