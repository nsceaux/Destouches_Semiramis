\clef "vbasse" r4 |
R2.*3 |
r4 r <>^\markup\character Zoroastre sol |
sib2 la8 sol |
sib4 la sol |
re'2 re'4 |
sib2\trill\prall la8 sol |
fad4 fad sol |
re2 re4 |
sol sol sol |
do'4. re'8 do'4 |
la4.\trill sol8 fa4 |
sib2 re4 |
mib8[ fa sol fa mib\trill re]( |
do)[ sib, do re mib do]( |
fa4.) sol8 la4~ |
la8 sib fa4. fa8 |
sib,2. |
sol4 sol la |
sib4 sib4. sol8 |
do'4 do' la8 la |
re'2. |
sol4 re4. sol8 |
sol2. |
sol4 sol la |
sib4 do'4. re'8 |
mib'4 mib' sib8 sib |
do'2 re' re4. sol8 |
sol2 r4 |
R2.*24 R1. R2.*79 R1. R2. |
