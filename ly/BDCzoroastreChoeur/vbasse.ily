\clef "vbasse" r4 |
R2.*28 R1. |
r4 r sol4 |
sib2 la8 sol |
sib4 la sol |
fad2\trill re4 |
sol2 fa8 sol |
mib4.\trill re8 do4 |
re2 re4 |
R2. |
sol4 sol sol |
do'4. do'8 do'4 |
la4.\trill sol8 fa4 |
sib2 sib4 |
sol8[ la sib la sol fa]( |
mib4.) fa8 sol4 |
mib4 fa4. fa8 |
sib,2. |
fa4 fa fa |
do do4. do8 |
sol4 sol sol8 la |
sib2. |
do'4 do'4.\trill do'8 |
re'2. |
sol4 sol la |
sib4 sib4. sol8 |
do'4 do' la8 la |
re'2 sol re4. sol8 |
sol2 r4 |
R2.*19 |
r4 r sol |
sib2 la8 sol |
sib4 la sol |
re'4 r r |
R2.*2 |
r4 r sol |
sib2 la8 sol |
sib4 la sol |
re'2 re'4 |
sol2 sol8 sol |
do'4. do'8 do4 |
fa2 fa4 |
re4 fa sol |
la4. sib8 la4 |
fa4.\trill\prall mi8 re4 |
la2 r4 |
R2.*5 |
re4 fa sol |
la4. sib8 la4 |
fa4.\trill\prall mi8 re4 |
la2 la4 |
sib8[ la sol fa mi re]( |
dod8)[ re mi fa sol mi]( |
fa4.) fa8 mi4 |
re la4. la8 |
re2. |
R2. |
re4 re re |
sol4. sol8 sol4 |
mi4.\trill re8 do4 |
fa2. |
fa4 fa fa |
sib4. sib8 sib4 |
la4.\trill sol8 fa4 |
sib2 sib4 |
sol8[ fa sol la sib sol]( |
do'4.) do'8 do'4 |
la4 re'4. re'8 |
re'2 re'4 |
sib8[ do' re' do' sib la]( |
sol4) sol sol |
do'8[ re' do' sib la sol]( |
fa4.) fa8 sol4 |
mib fa4. fa8 |
sib,2. |
fa4 fa fa |
do do4. do8 |
sol4 sol sol8 la |
sib2. |
do'4 do'4.\trill do'8 |
re'2. |
sol4 sol la |
sib4 sib4. sol8 |
do'4 do' la8 la |
re'2 sol re4. sol8 |
sol2. |
