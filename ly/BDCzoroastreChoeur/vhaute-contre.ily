\clef "vhaute-contre" r4 |
R2.*28 R1. |
r4 r re'4 |
sol'2 fad'8 sol' |
sol'4 fad' sol' |
re'2 re'4 |
re'2 re'8 re' |
mib'4. mib'8 mib'4 |
re'2 re'4 |
R2.*2 |
sol'4 sol' sol' |
la'4. sib'8 la'4 |
fa'4. fa'8 fa'4 |
sol'2 sol'4 |
sib'4. la'8 sol'4 |
sol'4 fa'4. fa'8 |
re'2. |
do'4 fa' fa' |
sol' sol'4. sol'8 |
sol'4 sol' sol'8 fad' |
sol'2. |
sol'4 la'4. sib'8 |
la'2.\trill |
sol'4 sol' fad' |
sol'4 sol'4. sib'8 |
sol'4 sol' la'8 la' |
la'2 sib' la'4.\trill sol'8 |
sol'2 r4 |
R2.*19 |
r4 r sol'4 |
sol'2 la'8 sib' |
sol'4 fad' sol' |
fad'\trill\prall r r |
R2.*2 |
r4 r sol' |
sol'2 la'8 sib' |
sol'4 fad' sol' |
fad'2 fad'4 |
sol'2 sol'8 sol' |
mi'4. mi'8 mi'4 |
fa'2 fa'4 |
R2.*10 |
mi'4 fa' sol' |
la'4. sib'8 la'4 |
la'4. la'8 la'4 |
fa'2 sol'4 |
sol'4 sol' sol' |
fa'4. fa'8 sol'4 |
la'4 la'4. la'8 |
la'2. |
R2. |
fad'4 fad' fad' |
sol'4. sol'8 sol'4 |
sol'4. sol'8 sol'4 |
la'2. |
do'4 do' fa' |
fa'4. fa'8 fa'4 |
fa'4. fa'8 fa'4 |
fa'2 fa'4 |
sol'4 re' sol' |
sol'4. mi'8 mi'4 |
mi' fad'4. sol'8 |
la'2 la'4 |
sib'4 sol' sol' |
sib' sib' sol' |
sol' sol' mib' |
mib'4. mib'8 re'4 |
sol' fa'4. fa'8 |
fa'2. |
fa'4 fa' fa' |
sol' sol'4. sol'8 |
sol'4 sol' sol'8 fad' |
sol'2. |
sol'4 la'4. sib'8 |
la'2. |
sol'4 sol' fad' |
sol'4 sol'4. sib'8 |
sol'4 sol' la'8 la' |
la'2 sib' la'4.\trill sol'8 |
sol'2. |
