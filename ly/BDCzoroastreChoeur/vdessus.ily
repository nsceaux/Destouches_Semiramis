\clef "vdessus" r4 |
R2.*28 R1. |
r4 r sib' |
re''2 do''8 sib' |
re''4 do'' sib' |
la'2\trill la'4 |
sib'2 la'8 sib' |
sol'4.\trill sol'8 la'4 |
fad'2\trill fad'4 |
re''4 re'' re'' |
sol''4. sol''8 sol''4 |
mi''4.\trill re''8 do''4 |
fa''2 fa''4 |
re''8[ mib'' fa'' mib'' re'' do'']( |
sib')[ do'' re'' mib'' fa'' re'']( |
sol''4.) fa''8 mib''[ re''] |
do''[ sib'] la'4.\trill sib'8 |
sib'2. |
do''4 do'' re'' |
mib'' mib''4. fa''8 |
re''4\trill re'' re''8 re'' |
sol''2. |
mi''4 fad''4. sol''8 |
fad''2.\trill |
sib'4 sib' do'' |
re'' re''4. sol''8 |
mi''4 mi'' mi''8 mi'' |
fad''2 sol'' fad''4.\trill sol''8 |
sol''2 r4 |
R2.*19 |
r4 r re'' |
sol''2 fad''8 sol'' |
re''4 do'' sib' |
la' r r |
R2.*2 |
r4 r re'' |
sol''2 fad''8 sol'' |
re''4 do'' sib' |
la'2\trill la'4 |
sib'2 sib'8 sib' |
sol'4. la'8 sib'4 |
la'2\trill la'4 |
R2.*6 |
la'4 re'' mi'' |
fa''4. sol''8 fa''4 |
mi''4.\trill re''8 dod''4 |
fa''2. |
dod''4 re'' mi'' |
fa''4. sol''8 fa''4 |
mi''4.\trill re''8 dod''4 |
re''2 dod''4 |
mi''8[ fa'' mi'' re'' dod'' sib']( |
la'4.) re''8 mi''4 |
fa'' mi''4. la''8 |
fad''2. |
la'4 la' la' |
re''4. mib''8 re''4 |
si'4. la'8 sol'4 |
do''2. |
do''4 do'' do'' |
fa''4. fa''8 fa''4 |
re''4.\trill do''8 sib'4 |
fa''2 fa''4 |
re''8[ do'' re'' mib'' fa'' re'']( |
sol''4.) sol''8 sol''4 |
mi''4 la''4. la''8 |
la''2 la''4 |
fad''8[ sol'' la'' sol'' fad'' mi'']( |
re''4) sib' re'' |
sol''8[ la'' sol'' fa'' mib'' re'']( |
mib''8)[ fa'' mib'' re'' do'' sib']( |
la'4.) la'8 sib'4~ |
sib'8 do'' do''4.\trill sib'8 |
sib'2. |
do''4 do'' re'' |
mib'' mib''4. fa''8 |
re''4 re'' re''8 re'' |
sol''2. |
mi''4 fad''4. sol''8 |
fad''2.\trill |
sib'4 sib' do'' |
re'' re''4. sol''8 |
mi''4 mi'' mi''8 mi'' |
fad''2 sol'' fad''4.\trill sol''8 |
sol''2. |
