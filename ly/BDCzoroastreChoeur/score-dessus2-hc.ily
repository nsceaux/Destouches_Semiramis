\score {
  <<
    \new Staff \with { \haraKiri } <<
      \global \keepWithTag #'dessus2 \includeNotes "dessus"
      { s4 s2.*28 s1. s2 \startHaraKiri\break }
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \includeNotes "haute-contre" \clef "dessus"
      { s4 s2.*28 s1. s2 \noHaraKiri }
    >>
  >>
  \layout { }
}
