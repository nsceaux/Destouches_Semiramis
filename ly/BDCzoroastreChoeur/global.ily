\key re \minor \midiTempo#160
\tempo "Legerement"
\digitTime\time 3/4 \partial 4 s4 s2.*28
\time 3/2 s1.
\digitTime\time 3/4 s2 \bar "||"
\beginMark "Chœur" \tempo "Gay" s4 s2.*24
\time 3/2 s1.
\digitTime\time 3/4 s2.*79
\time 3/2 s1.
\digitTime\time 3/4 s2. \bar "|."
