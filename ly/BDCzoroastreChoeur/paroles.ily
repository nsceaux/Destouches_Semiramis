\tag #'zoroastre {
  For -- mez les plus ten -- dres con -- certs.
  Chan -- tez u -- ne Rei -- ne char -- man -- te :
  Que de son Nom re -- ten -- tis -- sent les airs,
  Qu’il vole __ en cent cli -- mats di -- vers :
  A cet -- te Fête é -- cla -- tan -- te,
  Ap -- pel -- lez tout l’U -- ni -- vers.
  A cet -- te Fête é -- cla -- tan -- te,
  Ap -- pel -- lez tout l’U -- ni -- vers.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  For -- mons les plus ten -- dres con -- certs.
  Chan -- tons u -- ne Rei -- ne char -- man -- te.
  Que de son Nom re -- ten -- tis -- sent les airs,
  \tag #'(vdessus vbasse) {
    Qu’il vole __ en cent cli -- mats di -- vers.
  }
  \tag #'(vhaute-contre vtaille) {
    Qu’il vole en cent cli -- mats di -- vers.
  }
  A cet -- te Fête é -- cla -- tan -- te,
  Ap -- pel -- lons tout l’U -- ni -- vers.
  A cet -- te Fête é -- cla -- tan -- te,
  Ap -- pel -- lons tout l’U -- ni -- vers.
}

\tag #'(vdessus vhaute-contre vtaille vbasse) {
  For -- mons les plus ten -- dres con -- certs.
  For -- mons les plus ten -- dres con -- certs.
  Chan -- tons u -- ne Rei -- ne char -- man -- te.
  \tag #'vdessus { Que de son Nom re -- ten -- tis -- sent les airs, }
  \tag #'(vtaille vbasse) {
    Que de son Nom re -- ten -- tis -- sent les airs,
  }
  Que de son Nom re -- ten -- tis -- sent les airs,
  \tag #'(vdessus vbasse) {
    Qu’il vole __ en cent cli -- mats di -- vers.
  }
  \tag #'(vhaute-contre vtaille) {
    Qu’il vo -- le, Qu’il vole en cent cli -- mats di -- vers.
  }
  Que de son Nom re -- ten -- tis -- sent les airs,
  Que de son Nom re -- ten -- tis -- sent les airs,
  \tag #'(vdessus vbasse) {
    Qu’il vole __ en cent cli -- mats di -- vers.
    Qu’il vo -- le, Qu’il vole __ en cent cli -- mats di -- vers.
  }
  \tag #'vhaute-contre {
    Qu’il vo -- le, Qu’il vole en cent cli -- mats di -- vers.
    Qu’il vo -- le, Qu’il vo -- le, Qu’il vo -- le,
    Qu’il vole en cent cli -- mats di -- vers.
  }
  \tag #'vtaille {
    Qu’il vole __ en cent cli -- mats di -- vers.
    Qu’il vo -- le, Qu’il vo -- le, Qu’il vole en cent cli -- mats di -- vers.
  }
  A cet -- te Fête é -- cla -- tan -- te,
  Ap -- pel -- lons tout l’U -- ni -- vers.
  A cet -- te Fête é -- cla -- tan -- te,
  Ap -- pel -- lons tout l’U -- ni -- vers.
}