\clef "dessus" <>^\markup\whiteout "Violons"
\twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''4 |
    sib'' la''8 sol'' fad'' mi'' |
    re''4 mib''8 re'' do'' sib' |
    la'4 re''8 do'' sib' la' |
    sib'2 re''4 |
    sol''2 la''8 sib'' |
    sol''4 la'' sib'' |
    fad''8\trill mi'' re'' do'' sib'\trill la' |
    re''2 do''8\trill sib' |
    do''4. re''8 sib'4 |
    la'8 sol' la' sib' do'' la' |
    sib'2 r4 |
    sol' sol' sol' |
    do''4. do''8 fa'' mib'' |
    re''4. mib''8 fa''4 |
    sib'2 sib'4 |
    mib''8 re'' mib'' fa'' sol'' mib'' |
    do''4 fa''2 |
    mib''8 re'' do''4.(\trill sib'16) do'' |
    re''2. |
    re''4 sol'' fad'' |
    sol'' sol''8 la'' sol'' fa'' |
    mi'' fa'' sol'' mi'' la'' la'' |
    fad'' mi'' fad'' sol'' la'' fad'' |
    sol''4 fad''4. sol''8 |
    sol''4. fad''8 sol'' la'' |
    sib''4 sib'' la'' |
    sol'' sol''4. fad''8 |
    sol''4 sol'' sib''8 sib'' |
    sib''2 la'' la''4.\trill sol''8 |
    sol''2 }
  { re''4 |
    sol'' fad''8 mi'' re'' do'' |
    sib'4 do''8 sib' la' sol' |
    fad'4 sib'8 la' sol' fad' |
    sol'2 sib'4 |
    re''2 do''8\trill sib' |
    re''4 do''\trill sib' |
    la' sib'8 la' sol'\trill fad' |
    sol'2 fad'8 sol' |
    la'4. sib'8 sol'4 |
    fad'8 mi' fad' sol' la' fad' |
    sol'2 r4 |
    mi'4 mi' mi' |
    fa'4. sol'8 la'4 |
    sib'4. do''8 re''4 |
    sol'2 sol'4 |
    do'' sol' sol' |
    la'4.\trill sib'8 do''4~ |
    do''8 sib' la'4.\trill sib'8 |
    sib'2. |
    sib'4 sib' do'' |
    re'' re''4. sib'8 |
    sol' la' sib' sol' do'' do'' |
    la' sol' la' sib' do'' la' |
    sib'4 la'4.\trill sol'8 |
    sol'4. la'8 sib' do'' |
    re''4 re'' do'' |
    re'' re''4.\trill do''8 |
    sib'4 sib' sol''8 sol'' |
    sol''2 sol'' fad''4.\trill sol''8 |
    sol''2 }
>>
<>^\markup\whiteout "Violons" sib'4 |
re''2 do''8 sib' |
re''4 do'' sib' |
la'8 sol' la' sib' do'' la' |
sib'2 la'8 sib' |
sol'4.\trill sol'8 la'4 |
fad'8 sol' la' sib' do'' la' |
re''4 re'' re'' |
sol''4. sol''8 sol''4 |
mi''4.\trill re''8 do''4 |
fa''2 fa''4 |
re''8 mib'' fa'' mib'' re'' do'' |
sib' do'' re'' mib'' fa'' re'' |
sol''4. fa''8 mib'' re'' |
do'' sib' la'4.\trill sib'8 |
sib' la' sib' do'' re'' mib'' |
do''4 do'' re'' |
mib'' mib''4. fa''8 |
re''4\trill re'' re''8 re'' |
sol'' la'' sib'' la'' sol'' fa'' |
mi''4 fad''4. sol''8 |
fad''8 mi'' re'' do'' sib'\trill la' |
sib'4 sib' do'' |
re'' re''4. sol''8 |
mi''4 mi'' mi''8 mi'' |
fad''2 sol'' fad''4.\trill sol''8 |
sol''2 <>^\markup\whiteout "Violons" sol''4 |
fad''8 mi'' re'' do'' sib' la' |
sib'4 sol' sib' |
la'\trill\prall( fad') re'' |
sol''8.( la''16) la''4.(\trill sol''16 la'') |
sib''8 la'' sol'' fa'' mib'' re'' |
mib'' fa'' sol'' fa'' mib'' re'' |
do''4 fa''8 mib'' re'' do'' |
sib' do'' do''4.\trill sib'8 |
sib'4
<>^\markup\whiteout "Petites flutes"
\twoVoices #'(dessus1 dessus2 dessus) <<
  { re''8( mib'') fa''( mib'') |
    re''( mib'') fa''( sol'') fa''( mib'') |
    re'' sol'' fa''4.\trill mib''8 |
    re'' sol'' fa''4.\trill mib''8 |
    re''4 mib''8 re'' do'' sib' |
    do'' re'' do'' sib' la'4 | }
  { \slurDashed sib'8( do'') re''( do'') |
    sib'( do'') re''( mib'') re''( do'') | \slurSolid
    sib' mib'' re''4.\trill do''8 |
    sib' mib'' re''4.\trill do''8 |
    sib'4 la'8 sib' la' sol' |
    la' sib' la' sol' fad'4 | }
>>
<>^"Tous" sol''8 la'' sib'' la'' sol'' fa'' |
mi'' fa'' mi'' fa'' sol'' la'' |
fad''8.(\trill sol''16) sol''4.(\trill fad''16 sol'') |
la''4 re''2 |
sol''8.( la''16) la''4.\trill sol''8 |
sol''2 re''4 |
sol''2 fad''8 sol'' |
re''4 do'' sib' |
la'
<>^\markup\whiteout "Petites flutes"
\twoVoices #'(dessus1 dessus2 dessus) <<
  { la'8( sib') do''( sib') |
    la'( sib') do''( re'') do''( sib') |
    la'( sib'16 do'') re''8( do'') sib'\trill( la') |
    sib'4 sol' }
  { fad'8( sol') la'( sol') |
    fad'( sol') la'( sib') la'( sol') |
    fad'( sol'16 la') sib'8( la') sol'( fad') |
    sol'4 re' }
>>
<>^\markup\whiteout "Tous" re''4 |
sol''2 fad''8 sol'' |
re''4 do'' sib' |
la'8 sol' la' sib' do'' la' |
sib'2 sib'8 sib' |
sol'4. la'8 sib'4 |
la'2\trill la'4 |
R2.*3 | \allowPageTurn
\twoVoices #'(dessus1 dessus2 dessus) <<
  { mi''4 fa''8.( sol''16) sol''8.(\trill fa''32 sol'') |
    la''4. sib''8 la''4 |
    sol''4 fa''\trill mi'' |
    fa''4 fa''8.( sol''16) sol''8.(\trill\prall fa''32 sol'') |
    la''4. sib''8 la''4 |
    sol''4.\trill fa''8 mi''4\trill |
    re''2. | }
  { dod''4 re''8.( mi''16) mi''8.(\trill re''32 mi'') |
    fa''4. sol''8 fa''4 |
    mi'' re''\trill dod'' |
    re''4 re''8.( mi''16) mi''8.(\trill re''32 mi'') |
    fa''4. sol''8 fa''4 |
    mi''4.\trill re''8 dod''4 |
    fa''2. | }
>>
dod''4 re'' mi'' |
fa''4. sol''8 fa''4 |
mi''4.\trill re''8 dod''4 |
re''2 dod''4 |
mi''8 fa'' mi'' re'' dod''8. sib'16 |
la'4. re''8 mi''4 |
fa''4 mi''4.\trill r8 |
re''4 re'' re'' |
fad''4. sol''8 la''4 |
la''8 sib'' la'' sol'' fad'' mi'' |
re''2. |
mi''4 mi'' mi'' |
la''4. sib''8 do'''4 |
do'''8 re''' do''' sib'' la'' sol'' |
fa''2 fa''4 |
do''8 sib' do'' re'' mib'' do'' |
re'' do'' re'' mib'' fa'' re'' |
sol''4. sol''8 sol''4 |
mi'' la''4. la''8 |
la''2 la''4 |
fad''8 sol'' la'' sol'' fad'' mi'' |
re''4 sib' re'' |
sol''8 la'' sol'' fa'' mib'' re'' |
mib'' fa'' mib'' re'' do'' sib' |
la'4. la'8 sib'4~ |
sib'8 do'' do''4.\trill sib'8 |
sib' la' sib' do'' re'' mib'' |
do''4 do'' re'' |
mib'' mib''8 re'' mib'' fa'' |
re''4 re'' re''8 re'' |
sol'' la'' sib'' la'' sol'' fa'' |
mi''4 fad''4. sol''8 |
fad''8 mi'' re'' do'' sib' la' |
sib'4 sib' do'' |
re'' re''4. sol''8 |
mi''4 mi'' mi''8 mi'' |
fad''2 sol'' fad''4. sol''8 |
sol''2. |
