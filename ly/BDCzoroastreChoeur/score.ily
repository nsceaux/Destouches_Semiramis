\score {
  \new ChoirStaff <<
    \new GrandStaff <<
      \new Staff \with { \haraKiri } <<
        \global \keepWithTag #'dessus1 \includeNotes "dessus"
        { s4 s2.*28 s1. s2 \startHaraKiri\break }
      >>
      \new Staff \with { \haraKiri } <<
        \global \keepWithTag #'dessus2 \includeNotes "dessus"
        { s4 s2.*28 s1. s2 \startHaraKiri }
      >>
    >>
    \new StaffGroup <<
      \new Staff \with { \haraKiriFirst } <<
        \global \keepWithTag #'dessus \includeNotes "dessus"
        { \startHaraKiri s4 s2.*28 s1. s2
          \stopHaraKiri s4 s2.*24 s1. s2.*9
          \footnoteHere #'(0 . 0) \markup\wordwrap {
            Manuscrit : mesures 65 et 79, les indications
            \italic Petites flutes et \italic Basses de violons
            sont rayées et remplacées par \italic Hautbois et
            \italic Bassons.
          }
        }
      >>
      \new Staff \with { \haraKiriFirst } <<
        \global \includeNotes "haute-contre"
        { s4 s2.*28 s1. s2 \noHaraKiri }
      >>
      \new Staff \with { \haraKiriFirst } <<
        \global \includeNotes "taille"
        { s4 s2.*28 s1. s2 \noHaraKiri }
      >>
    >>
    \new ChoirStaff <<
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \includeNotes "vdessus"
        { s4 s2.*28 s1. s2 \noHaraKiri }
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \includeNotes "vhaute-contre"
        { s4 s2.*28 s1. s2 \noHaraKiri }
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \includeNotes "vtaille"
        { s4 s2.*28 s1. s2 \noHaraKiri }
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        \global \includeNotes "vbasse"
        { s4 s2.*28 s1. s2 \noHaraKiri }
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \with { \haraKiri } \withLyrics <<
      \global \includeNotes "voix"
    >> \keepWithTag #'zoroastre \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*6\break s2.*6\pageBreak
        s2.*7\break s2.*6\break s2.*3 s1. s2 \pageBreak
        s4 s2.*5\pageBreak s2.*6\pageBreak
        s2.*6\pageBreak s2.*6\pageBreak
        s2.*8\break s2.*7\pageBreak
        s2.*8\break s2.*7\pageBreak
        s2.*6\pageBreak s2.*7\pageBreak
        s2.*6\pageBreak s2.*7\pageBreak
        s2.*6\pageBreak s2.*7\pageBreak
        s2.*6\pageBreak s2.*5\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
