\clef "taille" r4 |
R2. |
r4 r re' |
sib sib la |
sib2 sib4 |
fa'2 fa'4 |
fa'2 mib'8 fa' |
fa'2. |
fa' |
fa' |
sol' |
sib' |
fa'2 fa'4 |
fa'4 mi' sol' |
fa'2 fa'4 |
fa' mi'4.\trill fa'8 |
fa'4 mi' sol' |
re'8 fa' mi'4. fa'8 |
fa'2 fa'4 |
sol'2 sol'4 |
sol'2 sol'4 |
mib' fa'2 |
sol'4 lab' do'' |
sol'2 sol'4 |
sol' sol' fa' |
fa' sol'4. fa'8 |
mi'4 mi'8 re' mi'4 |
fa'2 fa'4 |
fa'2 fa'4 |
sol'2 sol'4 |
sol' la'2 |
la'4 re' re' |
re'2 do'4 |
do' re'4. do'8 |
si4 si8 la si4 |
do' sol mi |
fa la la |
sib4 do' mib' |
sib4 sib2 |
do'4 r r |
re' r r |
do' r r |
sib r r |
fa' r r |
sol' r r |
do' do' do' |
re' do' fa' |
sol'2. |
fa'4 fa' sol' |
mib' mib' fa' |
re' mib' mib' |
sol' fa'4. mib'8 |
re'2
