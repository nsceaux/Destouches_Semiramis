\clef "dessus" <>^"Violons" fa'4 |
sib'4 sib'8 do'' re''4 |
sib' re'' sib' |
fa' sib' fa' |
sib'8 la' sib' do'' re'' mib'' |
fa''4 la'' fa'' |
sib'' la''8 sol'' fa'' mib'' |
re''2.\trill |
do''8 sib' do'' re'' mib'' do'' |
fa''2. |
re''8 do'' re'' mib'' fa'' re'' |
sol''2. |
re''8 mib'' fa'' sol'' la'' fa'' |
sib'' la'' sol'' fa'' mi'' re'' |
do''4 fa''2 |
sol''4 sol''4.\trill( fa''16 sol'') |
la''4. sol''8 fa'' mi'' |
fa'' sol'' sol''4.\trill fa''8 |
fa''2 do''4 |
mib'' mib''8 fa'' sol''4 |
mib'' sol'' mib'' |
do'' lab''2 |
sol''4 do'''2 |
si''8 la'' sol'' fa'' mib'' re'' |
mib'' fa'' sol'' mib'' lab'' sol'' |
fa'' mib'' re''4.\trill do''8 |
do''2 do''4 |
fa''4 fa''8 mib'' fa''4 |
re'' sib' fa'' |
sib'' sib''8 la'' sib''4 |
sol''8 fad'' sol'' sib'' la'' sol'' |
fad'' mib'' re'' do'' sib' la' |
sib' do'' re'' sib' mib'' re'' do'' sib' la'4.\trill sol'8 |
sol'2 sol'4 |
do'' do''8 sib' do''4 |
la'4 fa' do'' |
fa'' fa''8 mib'' fa''4 |
re'' sol''2 |
r4 fa''2 |
r4 mib''2 |
r4 re'' re'' |
re'' do'' do'' |
do'' sib'8 do'' re'' sib' |
mib'' fa'' mib'' re'' do'' sib' |
la' sib' do'' sib' la' sol' |
fa' sol' la' sib' do'' re'' |
mib''2. |
re''4 re'' mib'' |
do''8 sib' do'' mib'' re'' do'' |
sib' la' sib' do'' re'' sib' |
mib'' re'' do''4.\trill sib'8 |
sib'2
