\newBookPart #'()
\act "ACTE II"
\sceneDescription\markup\wordwrap-center {
  Le Théâtre représente l’avant-cour du Palais de Semiramis.
  On voit un Temple dans l’éloignement d’un des côtez.
}
\scene "Scene Premiere" "Scene I"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Arsane, Amestris.
}
% 2-1
\pieceToc\markup\wordwrap {
  Arsane, Amestris : \italic { Non, ne craignez point de m’entendre }
}
\includeScore "BAAarsaneAmestris"
\newBookPart #'(full-rehearsal)

\scene "Scene Deuxiéme" "Scene II"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Semiramis, Arsane.
}
%% 2-2
\pieceToc\markup\wordwrap {
  Semiramis, Arsane : \italic { Ou courez-vous Arsane ? }
}
\includeScore "BBArecit"
\newBookPart #'(full-rehearsal)

\scene "Scene Troisiéme" "Scene III"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Zoroastre, Semiramis.
}
%% 2-3
\pieceToc "Marche"
\includeScore "BCAmarche"
\newBookPart #'(basse-continue)
%% 2-4
\pieceToc\markup\wordwrap {
  Zoroastre, Semiramis : \italic {
    Belle Semiramis, l’amour & l’esperance
  }
}
\includeScore "BCBzoroastreSemiramis"
\newBookPart #'(full-rehearsal)

\scene "Scene Quatriéme" "Scene IV"
\sceneDescription\markup\center-column {
  \wordwrap-center {
    Le Théâtre change, & représente les célébres Jardins de Semiramis.
  }
  \wordwrap-center {
    \smallCaps { Zoroastre, Semiramis, }
    Troupes de Genies Elementaires, & de Peuples.
  }
}
%% 2-5
\pieceToc "Air"
\includeScore "BDArondeau"
\newBookPart #'(full-rehearsal)
%% 2-6
\pieceToc\markup\wordwrap {
  Air. Un Genie : \italic { L’Art plus prompt que la Nature }
}
\includeScore "BDBair"
%% 2-7
\pieceToc\markup\wordwrap {
  Zoroastre, chœur : \italic { Formez les plus tendres concerts }
}
\includeScore "BDCzoroastreChoeur"
\newBookPart #'(full-rehearsal)
%% 2-8
\pieceToc "Premier air"
\includeScore "BDDair"
%% 2-9
\pieceToc "Deuxiéme air"
\includeScore "BDEair"
\newBookPart #'(full-rehearsal)
%% 2-10
\pieceToc\markup\wordwrap {
  Le Genie, chœur : \italic { Paroissez jeunes Zephirs }
}
\includeScore "BDFgenieChoeur"
%% 2-11
\pieceToc "Deuxiéme air"
\reIncludeScore "BDEair" "BDGair"
\newBookPart #'(full-rehearsal)
%% 2-12
\pieceToc "Troisiéme air"
\includeScore "BDHair"
%% 2-13
\pieceToc "Quatriéme air"
\includeScore "BDIair"
\newBookPart #'(full-rehearsal)
%% 2-14
\pieceToc\markup\wordwrap {
  Le Genie, chœur : \italic { Au Dieux d’Amour il faut se rendre }
}
\includeScore "BDJgenieChoeur"
\newBookPart #'(full-rehearsal)
%% 2-15
\pieceToc "Troisiéme air"
\reIncludeScore "BDHair" "BDKair"
%% 2-16
\pieceToc "Quatriéme air"
\reIncludeScore "BDIair" "BDLair"
%% 2-17
\pieceToc\markup\wordwrap {
  Zoroastre, Semiramis :
  \italic { De vos nouveaux Sujets voyez quelle est l’ardeur }
}
\includeScore "BDMrecit"
\newBookPart #'(full-rehearsal)

\scene "Scene Cinquiéme" "Scene V"
\sceneDescription\markup\wordwrap-center\smallCaps { Zoroastre. }
%% 2-18
\pieceToc\markup\wordwrap {
  Zoroastre :
  \italic { Qu’ai-je entendu ? quel soupçon ! quel effroi }
}
\includeScore "BEArecit"
%% 2-19
\pieceToc "Entr’acte"
\reIncludeScore "BDHair" "BEBair"
\includeScore "BECair"
