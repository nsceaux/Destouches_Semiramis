s1 <6> <7>2 s1.

<6>4\figExtOn <6 4>2\figExtOff <6>2\figExtOn <6>\figExtOff s1
<6>1. \new FiguredBass { <_+>2\figExtOn <_+>2\figExtOff } s1
s2 <6> <5/> s1. <4+> <6>2

<_+>4\figExtOn <_+> <6> <6> <""> <"">\figExtOff \new FiguredBass <6>2 s \new FiguredBass { <6> <_+> } <_+>
s <4+> <6> <6> s <4+>
<6>1 <7>4 <6+> s1*3 <6>2.

s2 <6>4 <4> <3>2 s1 <6 4 3>2 <_+>2 <7> s2
s4 <4+>8 <6+> s2 <5/> <"">\figExtOn <"">\figExtOff <6> <7>4 <6>
s2.*4 <6>2. <6>4\figExtOn <6>2\figExtOff <_+> <5>4

<"">2\figExtOn <"">4\figExtOff <6>2. <6 5>4. <4>4 <3>2 s2.
s <6> <7>4\figExtOn <7>\figExtOff <7> s2 <6+>4 s <7> <6> <_+>2.

<6>2. <_+>2 <_->4 <5/>2. <_->4. <6> <6>4 <6 _->2 <"">2\figExtOn <"">4\figExtOff
s2 <6->4 s2 <6 _->4 <6 5>2. s <6 _-> <_+>4\figExtOn <_+>\figExtOff <6>

<6>2. <_+>4\figExtOn <_+>\figExtOff s8 <6> <7>4 <7 _+>2 s <6>4 <7> <_+>2 s2.
s1 <7>2 <6+> <6>1 <6 _->2 \new FiguredBass { <6 4>4 <_+> } s1 <5/>2 s8 <6> <7> <6> <_+>2

<5/>2 <_->4. <6>8 s4 <6 5/>4 s1 s2 <6>4 <6>
<_+>1 s4 \new FiguredBass { <4>8 <3+> } <_+>2 <5/> <_-> <"">4\figExtOn <"">\figExtOff
<6>1 s4 <4> <6>4 <4>8 <3> s2 <6>4\figExtOn <6>8\figExtOff <6+>

s4 <5/>2. s1 <_->2 <5/>
s2. <"">8\figExtOn <"">\figExtOff <6+>4 <4>8 \new FiguredBass <_+>

