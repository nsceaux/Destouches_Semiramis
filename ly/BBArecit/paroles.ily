\tag #'(semiramis basse) {
  Ou cou -- rez- vous Ar -- sa -- ne ?
  Quel trouble a -- gi -- te vos es -- prits ?
}
\tag #'(arsane basse) {
  Rei -- ne, qu’ai-je en -- ten -- du ! que de -- vient A -- mes -- tris ?
  El -- le fuit de ces lieux. Eh ! qui donc l’y con -- dam -- ne ?
}
\tag #'(semiramis basse) {
  Les Dieux, ses vo -- lon -- tez, la Paix de mes E -- tats.
}
\tag #'(arsane basse) {
  Eh quoi ! vous n’y ré -- sis -- tez pas !
}
\tag #'(semiramis basse) {
  J’ai fait pe -- rir mon Fils pour con -- ser -- ver l’Em -- pi -- re.
  Les Dieux me me -- na -- çoient de pe -- rir par son bras.
  A -- mes -- tris est d’un sang qu’il est tems de pros -- cri -- re.
}
\tag #'(arsane basse) {
  De quoy l’ac -- cu -- sez- vous ? quels sont ses at -- ten -- tats ?
}
\tag #'(semiramis basse) {
  Et vous, quel in -- té -- rêt ?...
}
\tag #'(arsane basse) {
  Ce -- lui de vô -- tre gloi -- re,
  Le re -- pos de vos jours.
}
\tag #'(semiramis basse) {
  Du moins j’aime à le croi -- re.
  Le tems dé -- voi -- le -- ra ce mys -- tere à mes yeux.
  Mais loin de s’ap -- pai -- ser, que de -- man -- dent les Dieux ?
  Quel obs -- ta -- cle nou -- veau font- ils i -- ci re -- naî -- tre ?
  Zo -- ro -- astre est prêt d’y pa -- roî -- tre.
  Son char aus -- si bril -- lant, que le flam -- beau du jour,
  Plus prompt que les é -- clairs, vole __ & fend les nu -- a -- ges.
  Mon Peuple ad -- mire, & trem -- ble tour à tour,
  Et l’en -- cens à la main, l’at -- tend sur ces ri -- va -- ges ;
  Ain -- si, vô -- tre Ri -- val m’ap -- por -- te ses hom -- ma -- ges,
  Dans l’ins -- tant où pour vous je tra -- his son a -- mour.
  Ain -- si, vô -- tre Ri -- val m’ap -- por -- te ses hom -- ma -- ges,
  Dans l’ins -- tant où pour vous je tra -- his son a -- mour.
  On vient, fai -- sons- nous vi -- o -- len -- ce.
}
\tag #'(arsane basse) {
  Croy -- ez- vous l’a -- bu -- ser, ou bra -- ver sa ven -- gean -- ce ?
}
\tag #'(semiramis basse) {
  Ar -- sa -- ne, s’il n’a pas con -- sul -- té les En -- fers,
  Il i -- gnore en -- cor mon of -- fen -- se.
}
\tag #'(arsane basse) {
  Tout va l’en é -- clair -- cir.
}
\tag #'(semiramis basse) {
  Pré -- pa -- rez ma dé -- fen -- se,
  As -- sem -- blez mes Sol -- dats, les mo -- mens nous sont chers.
}
\tag #'(arsane basse) {
  Je sui -- vrai des de -- voirs dont rien ne me dis -- pen -- se.
  Si par le Ciel mes vœux sont se -- con -- dez,
  Je fe -- rai plus pour vous que vous ne de -- man -- dez.
}
