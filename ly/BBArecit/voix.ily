<<
  %% Semiramis
  \tag #'(semiramis basse) {
    \tag #'basse \semiramisMark
    \tag #'semiramis \clef "vbas-dessus"
    re''4 re''8 re'' re''4 mi'' |
    \appoggiatura re''8 do''4 do'' r8 do''8 re'' mi'' fa'' mi'' re'' do'' |
    si'2 <<
      \tag #'basse { s1 s2. s1*2 s4. \semiramisMark }
      \tag #'semiramis { r2 r | R2. R1*2 | r4 r8 }
    >> si'8 mi''4 r do'' re''8 mi'' |
    la'4 r8 re'' fad' fad' fad' sol' |
    sol'4. <<
      \tag #'basse { s8 s2 s \semiramisMark }
      \tag #'semiramis { r8 r2 | r2 }
    >> r4 r8 la' sol' fa' mi' la' |
    \appoggiatura sol'8 fa'2 la'4 la'8 la' re''4 re'' |
    fad'2 fad'4 r8 la' la' la' si' do'' |
    re''4 re''8 mi'' la'4 la'8 re'' |
    sol'2 r4 si'8 si' do''4 do''8 do'' |
    la'4\trill la'8 la' re''4 re''8 re'' |
    sold'4 sold'8 <<
      \tag #'basse { s8 s2 s1 s4. \semiramisMark }
      \tag #'semiramis { r8 r2 | R1 | r4 r8 }
    >> re''8 mi'' mi'' mi'' mi'' |
    la'4. <<
      \tag #'basse { s8 s2 s1. s2. \semiramisMark }
      \tag #'semiramis { r8 r2 | R1. | r2 r4 }
    >> r8 re'' |
    re''2 r8 re'' mi'' fa'' |
    re''2\trill re''4 r8 re'' |
    sol'4. sol'8 la' sib' |
    la'2\trill si'8 do'' |
    do''2\trill do''8 si' |
    do''2 mi''4. r8 mi'' mi'' mi'' mi'' |
    dod''4 re''8 mi'' mi''4\trill re''8 mi'' |
    \appoggiatura mi''8 fa''4 re''8 re'' la'4\trill la'8 si' |
    \appoggiatura si'8 do''4. do''8 do'' do'' do'' si' |
    si'4\trill si' r sol'8 sol' |
    do''4. do''8 re''4 mi''8 fa'' |
    mi''2 mi''4 |
    R2. |
    r4 r do'' |
    si'4. la'8 si' sol' |
    do''2 r8 do'' |
    re'' mib'' re''4.\trill do''8 |
    si'2\trill mib''4 |
    re''4.\trill do''8 re'' sib' |
    mib''4. mib''16[\melisma fa'' sol'' fa'' mib'' re''] do''[ reb'' do'' sib' lab' sol']( |
    fa'8.\trill)\melismaEnd fa'16 sib'4 sib'8 sib' |
    sol'2\trill sol'4 |
    r4 r8 mib' sol' la' |
    sib'2 sib'4 |
    mib''4. mib''8 do'' fa'' |
    re''4.\trill re''8 do''4 |
    sib'2 la'8 sol' |
    fad'2\trill la'4 |
    re''4. la'8 si' do'' |
    si'4\trill sol'8 r sol'4 |
    sib'4. sib'8 do'' sol' |
    \appoggiatura sol'8 lab'4. sol'8 do''4 |
    sib' do''8[ sib'] lab'[ sol'] |
    fa'4\trill fa' sib'8 lab' |
    sol'2\trill lab'8 sib' |
    \appoggiatura sib'8 do''2 re''8 mib'' |
    re''16[ do''8 re''16] re''4.\trill mib''8 |
    mib''2 mib''4 |
    re''4.\trill\prall do''8 si' la' |
    si'4.\trill sol'8 do'' do'' |
    re''8. mib''16 mib''4.\trill( re''8) |
    re''2 re''8 mib'' |
    fa''4 \appoggiatura mib''16 re''4 \appoggiatura do''16 si'4 |
    mib''2 do''8 mib'' |
    la'8.[ sib'16] si'4.\trill do''8 |
    do''2. |
    R1*4 |
    r2 r4 re'' |
    la' la'8 la' sib'4 sib'8 sol' |
    re'' re'' <<
      \tag #'basse { s2. s1 s4. \semiramisMark }
      \tag #'semiramis { r4 r2 | R1 | r4 r8 }
    >> la'8 re'' re''16 r re''8 do'' |
    sib'4 sib'8 la' sol'4 sol'8 sib' |
    mi'2 la'8 la' si' dod'' re''4 re''8 dod'' |
    re''4 re''8 <<
      \tag #'basse { s8 s2 s4 \semiramisMark }
      \tag #'semiramis { r8 r2 | r4 }
    >> do''8 do'' la'4\trill la'8 do'' |
    fa' fa' sib' sib' fa'4 sol'8 lab' |
    sol'4\trill sol'8 la' sib'4 sib'8 la' |
    sib'4
    \tag #'semiramis { r4 r2 | R1*3 R2.*2 | r2 }
  }
  %% Arsane
  \tag #'(arsane basse) {
    <<
      \tag #'basse { s1 s1. s2 \arsaneMarkText "Vivement" }
      \tag #'arsane {
        \clef "vhaute-contre" r2 r | R1. | r2 <>^\markup\italic Vivement
      }
    >>
    mi'4 mi' r8 mi' mi' do' |
    sol'4. mi'16 mi' mi'8 mi'16 sol' |
    do'4 do'8 do' fa'4 fa'8 fa' |
    re'4 re'8 re' mi'4 re'8 do' |
    \appoggiatura do'8 re'4 re'8 <<
      \tag #'basse { s8 s1 s1 s4. \arsaneMarkText "Vivement" }
      \tag #'arsane { r8 r2 r | R1 | r4 r8 <>^\markup\italic Vivement }
    >> sol'8 mi'8.\trill mi'16 mi' re' do' si |
    la2\trill <<
      \tag #'basse { s1 s1.*2 s1 s1. s1 s4. \arsaneMark }
      \tag #'arsane { r2 r | R1.*2 R1 R1. R1 | r4 r8 }
    >> mi'8 si si si do' |
    do'4 r8 do' dod' dod' dod' re' |
    re'4. <<
      \tag #'basse { s8 s2 s4. \arsaneMark }
      \tag #'arsane { r8 r2 | r4 r8 }
    >> fa'8 fad' fad' fad' sol' |
    sol'4 sol' r sol'8 sol' do'4 do'8 re' |
    si2. <<
      \tag #'basse {
        s4 s1*2 s2.*3 s1. s1*5 s2.*8 s8*9 s2.*26 s1*6 s4 \arsaneMark
      }
      \tag #'arsane { r4 | R1*2 R2.*3 R1. R1*5 R2.*8 R8*9 R2.*26 R1*6 | r4 }
    >> re'8 mi' fa'4 fa'8 sol' |
    \appoggiatura fa'8 mib'4 re'8 re' sol'4 sol'8 do' |
    la4\trill la8 <<
      \tag #'basse { s8 s2 s1 s1. s4. \arsaneMark }
      \tag #'arsane { r8 r2 | R1 R1. | r4 r8 }
    >> re'8 fa' fa' mib' re' |
    mib'4 <<
      \tag #'basse { s2. s1*2 s4 \arsaneMark }
      \tag #'arsane { r4 r2 | R1*2 | r4 }
    >> re'8 re' sol4 sol8 la |
    sib4 do' re'8 mib' fa' re' |
    sol'4 sol' r8 sol sol sol |
    mib'4. sol'8 do' do' re' mib' |
    re'4\trill re'8 re'16 re' mi'8 fa' |
    mi' mi' fad' sol' sol' fad' |
    sol'2
  }
>>