\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \keepWithTag #'complet \global
      \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new Staff <<
      \keepWithTag #'complet \global
      \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
