\clef "basse"
<<
  \tag #'complet {
    r2 r |
    R1.*2 R2. R1*2 R1. R1*2 R1.*3 R1 R1. R1*5 R1. R1*3 R2.*3 R1. R1*5 |
  }
  \tag #'part { R1*31 <>^"Violons" }
>>
R2.*8 R8*9 R2.*26 |
\tag #'part <>_\markup\small\right-align\line {
  je trahis son amour.
}
sol,1 |
la, |
sib, |
do2 re4 re, |
sol,2 r |
<<
  \tag #'complet { R1*5 R1. R1*8 R2.*2 | r2 }
  \tag #'part { R1*16 }
>>
