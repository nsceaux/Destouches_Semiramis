\key do \major
<<
  \tag #'part {
    \once\override Staff.TimeSignature.stencil = ##f
    \time 2/2 s1*31
  }
  \tag #'complet {
    \time 3/2 \midiTempo#160 \partial 1 s1 s1.*2
    \digitTime\time 3/4 \midiTempo#80 s2.
    \digitTime\time 2/2 \midiTempo#160 s1*2
    \time 3/2 \grace s8 s1.
    \time 2/2 \midiTempo#80 s1*2
    \time 3/2 \midiTempo#160 s1.*3
    \time 2/2 s1
    \time 3/2 s1.
    \time 2/2 s1*5
    \time 3/2 s1.
    \time 2/2 s1*3
    \digitTime\time 3/4 s2.*3
    \time 3/2 s1.
    \time 2/2 s1*5
  }
>>
\digitTime\time 3/4 \midiTempo#120 s2. \bar "||" \key sol \minor s2.*7
\time 9/8 s8*9
\digitTime\time 3/4 s2.*26 \bar "||"
\key re \minor \time 2/2 \midiTempo#80 s1*5
<<
  \tag #'part {
    s1*16
  }
  \tag #'complet {
    s1*5
    \time 3/2 \midiTempo#160 s1.
    \time 2/2 s1*8
    \digitTime\time 3/4 \midiTempo#80 s2.*2
    \time 2/2 s2 \bar ""
  }
>>
