\clef "basse" fa1 |
mi re2 |
sol do'1 |
si4 sib2 |
la re |
sol do |
si, do1 |
re2 re, |
sol, do |
fa dod1 |
re1. |
do |
si,4 do re do |
si, la, sol, fa, mi,2 |
fa, re, |
mi, mi |
la sol |
fa dod |
re do |
si,1 la,2 |
sol,1~ |
sol,~ | \allowPageTurn
sol,2. sol8 fa |
mi2. |
fa4. mi8 re do |
sol4 sol,2 |
do do' sib |
la la, |
re re'4 do'8 si |
la2 fad |
sol fa |
mi re |
do2. | \allowPageTurn
R2.*2 |
r4 r sol |
mib4. re8 mib do |
lab sol fa4.\trill( mib16 fa) |
sol2 mib4 |
sib2 lab4 |
sol4.~ sol lab |
sib4 sib,2 |
mib2.~ |
mib |
re |
do4 fa fa, |
sib,4. sib8 la4 |
sol4 do2 |
re2. |
fad,2. |
sol,2 sol8 fa |
mi2. |
fa4. sol8 lab4 |
sol fa mib |
sib8 lab sib do' re' sib |
mib' fa' mib' reb' do' sib |
lab sib lab sol fa mib |
lab4 sib sib, |
mib2 mib4 |
fa2 fa4 |
sol fa mib |
si,8 sol, do4 do, |
sol, sol fa8 mib |
re4 sol sol, |
do8 si, do re mib4 |
fa sol sol, |
do2. |
sol,1 |
la, |
sib, |
do2 re4 re, |
sol,2. sol4 |
fad2 sol8 fa? mib4 |
re2 si, |
do4 sol8 fa mib4 mi |
fa2 sib |
sol4 sol8 la sib4 sol |
la1 sib8 sol la la, |
re2 si, |
do fa4 mib |
re1 |
mib2 re8 mib fa fa, |
sib,2 sib,8 do sib, la, |
sol,4 la, sib,2 |
mib1 |
do2 la, |
sib,2. |
do8 sib, la, sol, re re, |
sol,2
