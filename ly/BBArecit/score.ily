\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { \haraKiriFirst } <<
        \keepWithTag #'complet \global
        \keepWithTag #'complet \includeNotes "dessus"
      >>
      \new Staff \with { \haraKiriFirst } <<
        \keepWithTag #'complet \global
        \keepWithTag #'complet \includeNotes "haute-contre"
      >>
      \new Staff \with { \haraKiriFirst } <<
        \keepWithTag #'complet \global
        \keepWithTag #'complet \includeNotes "taille"
        { s2 s s1.*2 s2. s1*2 s1. s1*2 s1.*3 s1 s1. s1*5 s1. s1*3 s2.*3
          s1. s1*5 | s2.*8 s8*9 s2.*26 \break }
      >>
    >>
    \new Staff \withLyrics <<
      \keepWithTag #'complet \global
      \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new Staff <<
      \keepWithTag #'complet \global
      \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1 s1. s2 \bar "" \break s1 s2. s1\break s1 s1. s1\break s1 s1.\break
        \grace s8 s1.*2 s2 \bar "" \break s2 s1. s1 s2 \bar "" \pageBreak
        s2 s1*2\break s1 s1. s1\break s1*2 s2.*2\break
        s2. s1. s1\break \grace s8 s1*2 s2 \bar "" \break s2 s1\pageBreak
        s2.*7\break s2. s8*9 s2.*3\break s2.*6\break s2.*5\pageBreak
        s2.*6\break s2.*6\break s1*5\break s1*2 s2 \bar "" \pageBreak
        s2 s1*2\break s1. s1\break s1*2\break s1*2 s2 \bar "" \break
        s2 s1*2\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
