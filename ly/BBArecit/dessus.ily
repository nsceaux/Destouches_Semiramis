\clef "dessus"
<<
  \tag #'complet {
    r2 r |
    R1.*2 R2. R1*2 R1. R1*2 R1.*3 R1 R1. R1*5 R1. R1*3 R2.*3 R1. R1*5 |
  }
  \tag #'part {
    R1*31 <>_\markup\small\center-align\center-column {
      \line { Zoroastre est prêt d’y paroître. }
    }
  }
>>
r4 r <>^"Violons" sol'' |
mib''4.\trill re''8 mib'' do'' |
sol''2 mib''4\doux |
re''4. do''8 re'' si' |
mib''2 r8 mib'' |
fa'' sol'' lab'' sol'' fa'' mib'' |
re''2 sol''4 |
fa''4.\trill mib''8 fa'' re'' |
sol''4. sol''16 lab'' sib'' lab'' sol'' fa'' mib''4 mib''8 |
mib''8. re''16 re''4.\trill mib''8 |
mib''2 r4 |
r r8 sol' sib' do'' |
fa'2 sib'4 |
sib'2 la'4 |
sib'4. fa''8 fad''4 |
sol''8 fa'' mib'' re'' do'' sib' |
la' sib' la' sol' fad'( sol') |
la'2 re''4 |
re''8 mib'' re'' do'' si'8. la'16 |
sol'2 sol''4 |
do''4. mib''8 mib''4 |
mib'' re'' mib'' |
re''8 do'' re'' mib'' fa'' re'' |
mib''4. mib''8 fa'' sol'' |
lab''4. sib''8 lab'' sol'' |
fa''4 fa''4.\trill mib''8 |
mib''2 sol''4 |
fa''4. mib''8 re'' mib'' |
re''4. re''8 mib'' fa'' |
sol''8 si'16( do'') do''4.\trill( si'8) |
si'2 si'8 do'' |
do''4 si'8( do'') re''4 |
sol'2 sol''8 sol'' |
do''8.( re''16) re''4.\trill do''8 |
do''2. |
<>^"Violons" r2 sol''4. sol''8 |
sol''4. la''16 sol'' fad''4.\trill mi''8 |
re''2~ re''4.*1/3 sol''16[ fa'' mib'' re'' do'' sib'] |
la'4. sib'16 do'' sib'4.\trill la'8 |
sol'2 r |
<<
  \tag #'complet { R1*5 R1. R1*8 R2.*2 | r2 }
  \tag #'part { R1*16 }
>>
