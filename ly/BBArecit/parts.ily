\piecePartSpecs
#`((dessus #:tag-notes part #:tag-global part)
   (dessus-hc #:tag-notes part #:tag-global part)
   (haute-contre #:tag-notes part #:tag-global part)
   (taille #:tag-notes part #:tag-global part)
   (basse #:notes "basse-violon" #:tag-notes part #:tag-global part)
   (basse-continue #:score-template "score-basse-continue-voix"
                   #:tag-notes basse-continue)
   (chant)
   (silence #:on-the-fly-markup , #{ \markup\tacet#88 #}))
