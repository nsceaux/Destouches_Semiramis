\clef "taille"
<<
  \tag #'complet {
    r2 r |
    R1.*2 R2. R1*2 R1. R1*2 R1.*3 R1 R1. R1*5 R1. R1*3 R2.*3 R1. R1*5 |
  }
  \tag #'part { R1*31 <>^"Violons" }
>>
R2.*8 R8*9 R2.*26 |
\tag #'part <>_\markup\small\right-align\line {
  je trahis son amour.
}
r2 sib'4. sib'8 |
sib'2 la' |
sol'2. re'4 |
mib'2 re'4. la8 |
sib2 r |
<<
  \tag #'complet { R1*5 R1. R1*8 R2.*2 | r2 }
  \tag #'part { R1*16 }
>>
