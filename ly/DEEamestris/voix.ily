\amestrisMark re''8 fa'' |
mi''4.\trill\prall re''8 mi''4 |
la'2 fa''8 mi'' |
re''2\trill\prall dod''8 re'' |
dod''2 mi''4 |
re''4.\trill\prall do''8 si' la' |
re''2 mi''8 fa'' |
sold'2\trill si'8 do'' |
do''4( si'2)\trill |
la'2 re''8 fa'' |
la'2. |
la'4 sib' do'' |
do''2 \appoggiatura sib'8 la'4 |
\appoggiatura la'8 sib'2 sib'8 la' |
sol'2\trill\prall fa'8 sol' |
\appoggiatura sol'8 la'4 fa' r8 la' |
re''4( do''8)\trill si' do'' re'' |
si'2\trill mi''8 fa'' |
re''4. mi''8 dod''4 |
re''2. |
re''2
