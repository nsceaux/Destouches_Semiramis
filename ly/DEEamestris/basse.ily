\clef "basse" <>^"B-C." re4 |
la2 sol4 |
fa2 fa4 |
sib8 la sol4.(\trill fa16) sol |
la2 la,4 |
re2 mi4 |
fa2 re4 |
mi2 re4 |
mi mi,2 |
la,2 re4 |
la,8 sol, la, si, dod la, |
re4 re' do'8 sib |
la4 re' re |
sol2 sol4 |
do'2 do4 |
fa2 fa4 |
fad2. |
sol8 la sol fa mi re |
sib4 sol la |
re2. |
re2
