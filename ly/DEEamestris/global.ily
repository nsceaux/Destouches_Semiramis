\key la \minor \midiTempo#144 \beginMark "Air"
\digitTime\time 3/4 \partial 4
s4 \bar ".!:" s2.*8 \alternatives s2. s2.
\bar ".!:" s2.*8 \alternatives s2. { \digitTime\time 2/2 s2 }
