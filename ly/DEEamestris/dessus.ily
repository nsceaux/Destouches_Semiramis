\clef "dessus" <>^"Flutes" la'8 re'' |
dod''4. si'8 dod''4 |
re''2 la''8 sol'' |
fa''( sol'') sol''4.\trill fa''8 |
mi''2 do'''4 |
si''4.\prall la''8 sold'' la'' |
si''2 do'''8 re''' |
re''' do''' si'' la'' sold'' la'' |
la''4( sold''4.)\trill la''8 |
la''2 la'8 re'' |
la''2. |
fa''4 sol'' la'' |
la''2 fad''4 |
sol''2 sol''8 fa'' |
fa''4( mi''4.)\trill fa''8 |
fa'' sol'' la''4. la''8 |
la''2 la''4 |
re''2 sol''8 la'' |
fa''4. sol''8 mi''4\trill |
re''2. |
re''2
