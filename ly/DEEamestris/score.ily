\score {
  \new ChoirStaff <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*5\break
        s2.*7\break \grace s8 s2.*5\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
