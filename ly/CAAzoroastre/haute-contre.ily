\clef "haute-contre" r2 fad'4. fad'8 |
mi'2 mi'4. mi'8 |
la2 dod'4.*13/12 dod'16*1/2 re' mi' |
re'4.*13/12 fad'16*1/2 sol' la' si'2 |
sold'4.*13/12 sold'16*1/2 la' si' la'4. la'8 |
fa'2 mi'4. si8 |
do'2 la'4. la'8 |
la'2 sold'4. la'8 |
la'2 re''4. re''8 |
re''2 mi''4. mi''8 |
dod''2 re''4. re''8 |
si'2 mi'4. mi'8 |
fad'2 re'4. re'8 |
mi'2 mi'4. mi'8 |
dod'2 re'4. re'8 |
re'4. dod'8 dod'4.\trill re'8 |
re'2 re'4.\douxSug fad8 |
sol2 si'4.\fortSug si'8 |
mi'2 dod'4\douxSug dod' |
fad'2 la'4.\fortSug la'8 |
re''2 la'\douxSug |
sol' si'4.*5/6\fortSug dod''16 si' la' |
sol'2 mi'\douxSug |
fad'4. re'8 re'4. si8 |
la2 mi'4.\fortSug fad'8 |
sol'2 la'4.\douxSug la'8 |
mi'2. mi'4 |
dod'2. sold'4 |
fad'1 |
fad'2 mi' |
mi'1~ mi'4. mi'8 |
mi'2. |
re''2 la'4 |
sol'2. |
mi''2 si'4 |
la' dod'' la' |
fad'2 fad'4 |
si'2 si'4 |
la'2 re'4 |
mi'2 mi'4 |
fad'2 re'4 |
re'2 mi'4 |
re'2 si'4 |
la'2 la'4 |
si'2 si'4 |
si'2 si'4 |
si'2 si'4 |
si'2 mi'8 sold' |
la'2 la'4 |
la'2 fad'4 |
fad'2 fad'4 |
fad'2 re'4 |
mi'2 mi'4 |
re'2 r4 |
r r la' |
sol'2. |
si'2 si'4 |
la'2 la'4 |
re''2 re''4 |
re''2 mi''4 |
dod''4. re''8 dod''8.\trill re''16 |
re''2. |
si'4 la'4. la'8 |
la'2. |
sol'4 mi'2\trill |
re'2. |
R2. |
fad'2 fad'4 |
mi' fad'2 |
fad'4. fad'8 si' la' |
sol'2 mi'4 |
mi'2 mi'4 |
mi'4 fad'4. mi'8 |
re'2 fad'4 |
mi'2 mi'4 |
dod'2 sold'4 |
fad'2 lad'4 |
si'2. |
si'2 sol'4 |
fad'2 fad'4 |
fad'2. |
r4 r r8 fad' |
re'2 re'4 |
fad'2 la' |
la' r4 la' la'4. si'8 |
si'2 la' fad'4 mi' |
mi'2. |
fad'2 fad'4 |
si'2 si'4 |
la'2 re'4 |
mi'2 mi'4 |
fad'2 re'4 |
re'2 mi'4 |
re'2 si'4 |
la'2 la'4 |
si'2 si'4 |
si'2 si'4 |
si'2 si'4 |
si'2 mi'8 sold' |
la'2 la'4 |
la'2 fad'4 |
fad'2 fad'4 |
fad'2 re'4 |
mi'2 mi'4 |
re'2. |
