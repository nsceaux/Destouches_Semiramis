\clef "basse" re1~ |
re |
dod8 dod16 re mi re dod si, la,4.*13/12 la,32 si, dod |
re8 re16 mi fad mi re dod si,4.*13/12 si,32 dod re |
mi4.*13/12 mi32 fad sold la4.*13/12 la,32 si, do |
re1 |
do2. la,32*8/5 si, do re mi |
fa2 mi4 mi, |
la,2 fad16 sol la sol fad mi re dod |
si,2 mi16 fad sol fad mi re dod si, |
la,2 re16 mi fad mi re dod si, la, |
sol,1 |
fad,1 |
mi, |
la,16 si, dod si, la, sol, fad, mi, re,2 |
sol, la, |
re,1 |
<>^"B[asses] de violons" sol,~ |
sol, |
fad,~ |
fad, |
sol,~ |
sol,2 la, |
si, sold, |
la,1 |
mi2 red |
mi1 |
mid |
fad4 sold lad fad |
si,2 re |
dod1 si,2 |
la,~ la,16 la,^"Tous" si, dod |
re2~ re16 re mi fad |
sol fad sol la sol fad sol la si la sol fad |
mi2~ mi16 mi fad sol |
la4 la, la, |
re4. re8 fad re |
si,4. si,8 re si, |
fad,4. re,8 fad, re, |
sol,4. sol,16 fad, mi,8 sol, |
fad,4. re,8 fad, re, |
sol,4. fad,8 sol, la, |
si,4. sol,8 si, sol, |
la,4. la,8 la, la, |
mi,4. mi,8 mi, fad, |
sol,4. sol,8 sol, mi, |
si,4. re16 dod si,8 re |
sold,4. mi,8 sold, mi, |
la,4. la,8 dod la, |
re4. re8 fad re |
fad,4. re8 fad re |
si,4. sol,16 la, si,8 sol, |
la,4. la,8 la, la, |
re,2 r4 | \allowPageTurn
r r r16 re mi fad |
sol fad sol la sol fad sol la si la sol fad |
mi2~ mi16 mi fad sol |
la8 la la la16 sol la si la sol |
fad2~ fad16 fad sol la |
si2 sol4 |
la la,2 |
re,2. |
<>^"B[asses] de violons" re4 dod la, |
si,2. |
mi,4 la,2 |
re,2. |
R2. |
fad4 mi re |
dod8 si, lad,2 |
si,4. la,8 sol, fad, |
mi,2 mi4 |
re2. |
dod4 re4. dod8 |
si,2. |
mi |
mid |
fad2 mi4 |
red2. |
mi2 mi,4 |
fad,2. |
si,~ |
si, |
re |
fad,2 fad,4 fad,8 re, |
la,2 re1 |
mi2 dod re4 mi |
la,4. la,16^"Tous" si, dod8 la, |
re4. re8 fad re |
si,4. si,8 re si, |
fad,4. re,8 fad, re, |
sol,4. sol,16 fad, mi,8 sol, |
fad,4. re,8 fad, re, |
sol,4. fad,8 sol, la, |
si,4. sol,8 si, sol, |
la,4. la,8 la, la, |
mi,4. mi,8 mi, fad, |
sol,4. sol,8 sol, mi, |
si,4. re16 dod si,8 re |
sold,4. mi,8 sold, mi, |
la,4. la,8 dod la, |
re4. re8 fad re |
fad,4. re8 fad re |
si,4. sol,16 la, si,8 sol, |
la,4. la,8 la, la, |
re,2. |
