\clef "vbasse" R1*16 |
<>^\markup\character Zoroastre \stopHaraKiri
r2 la4. la8 |
mi2 r |
r la4 la |
re2 r |
r4 la8 la re'4 re'8 la |
\appoggiatura la8 si2 si4 r |
re'4 dod'8 si la4. sol8 |
fad4\trill fad8 sol mi4\trill mi8 fad |
dod1 |
sol4 sol8 mi si4. si8 |
si2. sold8 sold |
dod'2 dod'4 dod' |
dod'2 dod'4 fad |
re' re'8 si sold4 la8 si |
mi4 mi mi fad8 sold sold sold sold la |
la2. |
R2.*4 |
fad2 fad8 r |
re4 fad4. re8 |
la2 la8 la |
dod'2 dod'8 la |
re'4 re'8 r re'4 |
si2\trill la8 sol |
fad2\trill fad8 sol |
mi2 mi8 fad |
sol2 sol8 la |
\appoggiatura la8 si2 si8 dod' |
re'2 re'8 si |
mi'2 mi'4 |
r4 la4. sol8 |
fad2 fad8 la |
re2 la4 |
re'2.~ |
re'4. re'8 re' dod' |
\appoggiatura dod' re'2. |
R2.*7 |
fad4 r sol |
\appoggiatura fad8*1/2 mi2 mi8 la |
re2 mi8 fad |
sol4. fad8 mi4 |
fad4.\trill mi8( re4) |
re'4 dod' si |
lad2 si4 |
lad8 si \appoggiatura si16 fad4 fad8 r16 <>^"Vivement" fad |
red4. red8 mi fad |
\appoggiatura fad8 sol2 sol8 sol |
sold2\trill sold8 la |
la4 r8 fad si dod' |
re'2 re'4 |
\appoggiatura la16 sold2 sold8 lad |
si4. dod'8 re'4 |
lad4.\trill\prall sold8( fad4) |
si4 si8 la sol\trill fad |
\appoggiatura fad8 sol2 r8 sol |
dod4. dod8 re mi |
re4.\trill dod8( si,4) |
r4 r r8 <>^"Vivement" si |
fad4. fad8 sol la |
re4 fad8 re la4 la8 re' |
dod'2\trill r4 re'8 dod' si4 dod'8 re' |
sold4 r8 mi la4 la8 r re4 mi |
\appoggiatura mi16 la,2. |
fad2 fad8 r |
re4 fad4. re8 |
la2 la8 la |
dod'2 dod'8 la |
re'4 re'8 r re'4 |
si2\trill la8 sol |
fad2 fad8 sol |
mi2 mi8 fad |
sol2 sol8 la |
\appoggiatura la8 si2 si8 dod' |
re'2 re'8 si |
mi'2 mi'4 |
r4 la4. sol8 |
fad2 fad8 la |
re2 la4 |
re'2.~ |
re'4. re'8 re' dod' |
re'2. |
