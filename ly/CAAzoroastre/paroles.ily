Qu’ai-je ap -- pris ! quels for -- faits ! quelle in -- ju -- re mor -- tel -- le !
C’est pour un in -- con -- nu qu’on me man -- que de foi.
O Ma -- jes -- té des Rois ! O puis -- sance é -- ter -- nel -- le
Des Dieux, qu’a -- tes -- toit l’In -- fi -- del -- le,
On vous ou -- tra -- ge, com -- me moi.
Hai -- ne, trans -- ports ja -- loux, im -- pla -- ca -- ble co -- le -- re,
Bar -- ba -- res en -- fans de l’A -- mour,
E -- tei -- gnez son flam -- beau, que le vô -- tre m’é -- clai -- re ;
Ar -- mez- vous con -- tre lui, re -- gnez __ à vô -- tre tour.
Mais, quel tris -- te se -- cours me pro -- met ma van -- gean -- ce !
C’est par mon cœur qu’el -- le com -- men -- ce.
De vains ge -- mis -- se -- mens, d’i -- nu -- ti -- les re -- grets,
Des cris per -- dus, des pleurs dont fre -- mit ma cons -- tan -- ce,
Sont du plus tendre A -- mour l’u -- ni -- que ré -- com -- pen -- se.
Non, non, de ma fu -- reur dé -- ploy -- ons tous les traits ;
Ac -- ca -- blons mon Ri -- val, la Rei -- ne, ses Su -- jets.
Hai -- ne, trans -- ports ja -- loux, im -- pla -- ca -- ble co -- le -- re,
Bar -- ba -- res en -- fans de l’A -- mour,
E -- tei -- gnez son flam -- beau, que le vô -- tre m’é -- clai -- re ;
Ar -- mez- vous con -- tre lui, re -- gnez __ à vô -- tre tour.