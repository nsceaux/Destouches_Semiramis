\clef "taille" r2 la4. la8 |
si2 si4. sol8 |
sol2 sol4. la8 |
la2 fad'4. fad'8 |
si4. mi'8 mi'4. mi'8 |
la2 si4. si8 |
la2 do'4. do'8 |
do'4. fa'8 si4. mi'8 |
mi'2 fad'4. fad'8 |
fad'2 si4. si8 |
mi'4. la8 la2 |
re'2 sol4. sol8 |
re'2 la4. la8 |
si2 si4. si8 |
la2 la4. la8 |
si2 la4. la8 |
la2 la4.\douxSug la8 |
sol2 sol'4.\fortSug sol8 |
la2 la4\douxSug la |
la2 la4.\fortSug re'8 |
fad'2 re'4.\douxSug re'8 |
re'1 |
re'2 dod'\douxSug |
si2 si4. mi8 |
mi2 dod'4.\fortSug dod'8 |
si2 si4.\douxSug si8 |
si1 |
si2. dod'4 |
dod'1 |
re'2 si |
dod'1 re'2 |
la2. |
la'2 la'4 |
re'2. |
si'2 si'4 |
mi' la' dod' |
re'2 re'4 |
fad'2 fad'4 |
fad'2 la4 |
la2. |
la2 la4 |
la2 la4 |
la2 mi'4 |
mi'2 mi'4 |
mi'2 si'8 la' |
sol'2 sol'4 |
re'2 re'4 |
mi'2 mi'4 |
mi'2 mi'4 |
fad'2 re'4 |
re'2 re'4 |
re'2. |
la2 la4 |
la2 r4 |
r r re' |
re'2. |
mi'2 mi'4 |
mi'2 mi'4 |
fad'2 fad'4 |
fad'2 si'4 |
la'4 la'4. sol'8 |
fad'4 r re' |
sol' la' mi' |
re'2. |
mi'4 la2 |
la2. |
R2. |
dod'2 si4 |
dod' dod'2 |
si si4 |
si2 si4 |
si2 si4 |
dod'4 la8 re' re' mi' |
fad'2 fad'4 |
si2. |
sold2 dod'4 |
dod'2 dod'4 |
fad'2. |
mi'2 dod'4 |
dod'2. |
si2. |
r4 r r8 si |
la2. |
re'2 re' |
mi' re' fad' |
mi'1 re'4 si |
dod'2. |
re'2 re'4 |
fad'2 fad'4 |
fad'2 la4 |
la2. |
la2 la4 |
la2 la4 |
la2 mi'4 |
mi'2 mi'4 |
mi'2 si'8 la' |
sol'2 sol'4 |
re'2 re'4 |
mi'2 mi'4 |
mi'2 mi'4 |
fad'2 re'4 |
re'2 re'4 |
re'2. |
la2 la4 |
la2. |
