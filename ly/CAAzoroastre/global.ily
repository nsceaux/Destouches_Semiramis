\key re \major \midiTempo#132 \tempo "Viste"
\beginMark PRÉLUDE
\digitTime\time 2/2 s1*30
\time 3/2 s1.
\digitTime\time 3/4 \tempo "Trés Vîte" s2.*22 s2 \tempo "Vîte" s4 s2.*29
\digitTime\time 2/2 s1
\time 3/2 s1.*2
\digitTime\time 3/4 \grace s16 \tempo "Trés Vîte" s2.*19 \bar "|."
