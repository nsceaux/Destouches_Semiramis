\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*5\break s1*4\break s1*4\pageBreak
        s1*5\break s1*5\pageBreak
        s1*4\break s1*4 \bar "" \pageBreak
        s2 s2.*4\break s2.*5\pageBreak
        s2.*6\break s2.*6\pageBreak
        s2.*5\break s2.*4\pageBreak
        s2.*6\break s2.*5\pageBreak
        s2.*6\break \grace s8 s2.*5 s2 \bar "" \pageBreak
        s2 s1.*2\break \grace s16 s2.*5\pageBreak
        s2.*7\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
