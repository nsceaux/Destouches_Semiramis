\clef "dessus" <>_"Violons" r2 re''4. re''8 |
sol'2~ sol'8 sol'16 la' si' la' sol' fad' |
mi'4.*13/12 mi'32 fad' sol' la'4~ la'8*1/2 la'16*1/2 si' do'' si' la' sol' |
fad'4.*13/12 la'32 si' dod'' re''2~ |
re''4.*13/12 re''32 do'' si' do''!2~ |
do''8 si'16 do'' re'' do'' si' la' sold' la' sold' la' si' la' sold' fad' |
mi'4~ mi'32 mi' fad' sold' la' si' dod'' re'' mi''2~ |
mi''4. re''8 re''4 dod''8 re'' |
dod''4\trill~ dod''32 la' si' dod'' re'' mi'' fad'' sol'' la''2~ |
la''4~ la''32 sol' la' si' dod'' re'' mi'' fad'' sol''2~ |
sol''4~ sol''32 fad' sol' la' si' dod'' re'' mi'' fad''2~ |
fad''8 mi''16 fad'' sol'' fad'' mi'' re'' dod'' re'' dod'' re'' mi'' re'' dod'' si' |
la'2 re''16 mi'' fad'' mi'' re'' dod'' si' la' |
sol' la' sol' la' si' do'' si' la' sol' la' sol' la' si' la' sol' fad' |
mi'2 fad'4. fad'8 |
fad'4. mi'8 mi'4.\trill re'8 |
re'2 fad'4.\doux fad'8 |
dod'2 mi''16\fort fad'' mi'' fad'' sol'' fad'' mi'' re'' |
dod''2 mi'4\doux mi' |
la'2 re''16\fort dod'' re'' mi'' fad'' re'' la'' la'' |
la''2 la''\doux |
re''2 re''16\fort mi'' re'' mi'' fad'' mi'' re'' dod'' |
si'2 dod''\doux |
re''4 re''8 sol' si'2 |
mi' la'16\fort si' la' si' dod'' re'' dod'' red'' |
mi''4. sol''8\doux fad''4. fad''8 |
sold''2. si'8 si' |
sold'2 sold'4 si' |
lad' si' dod'' lad' |
si'2 si'4 la'8 sold' |
la'2 la' re''8 re'' re'' mi'' |
dod''2~ dod''16 dod'' re'' mi'' |
fad'' mi'' fad'' sol'' la'' si'' la'' sol'' fad'' mi'' re'' dod'' |
si'2~ si'16 red'' mi'' fad'' |
sol'' fad'' sol'' la'' si'' do''' si'' la'' sol'' fad'' mi'' re'' |
dod''8 dod''16 re'' mi'' fad'' sol'' fad'' mi'' re'' dod'' si' |
la'4. la'8 la' la' |
re''4. re''8 re'' fad'' |
re''4. fad'16 sol' la'8 fad' |
mi'4. mi'8 sol' mi' |
la'4. fad'16 sol' la'8 fad' |
sol'4. re''8 re'' dod'' |
re''4. re''8 re'' mi'' |
dod''4. dod''8 dod'' red'' |
mi''4. mi''8 mi'' red'' |
mi''4. mi''8 mi'' sol'' |
fad''4. fad''16 fad'' fad''8 fad'' |
si'4. sold'16 la' si'8 sold' |
dod''4. dod''8 mi'' dod'' |
la'4. la'8 la' la' |
la'4. la'8 la' la' |
la'4. si'16 la' sol'8 si' |
mi'4. la'8 la' la' |
fad'2~ fad'16 dod'' re'' mi'' |
fad'' mi'' fad'' sol'' la'' si'' la'' sol'' fad'' mi'' re'' dod'' |
si'2~ si'16 red'' mi'' fad'' |
sol'' fad'' sol'' la'' si'' do''' si'' la'' sol'' fad'' mi'' re'' |
dod''2~ dod''16 mi'' fad'' sol'' |
la''8 la'' la'' la''16 si'' la'' sol'' fad'' mi'' |
re'' dod'' re'' mi'' fad'' mi'' fad'' re'' sol'' la'' sol'' fad'' |
mi'' fad'' sol'' fad'' mi''8 fad''16 sol'' mi''8.\trill re''16 |
re''2 si''4\doux |
\appoggiatura { si''32[ la''] } sol''2( fad''8) sol'' |
fad''2\trill mi''8 re'' |
re''4 dod''4.(\trill si'16 dod'') |
re''2 la'4 |
fad''4 mi'' re'' |
dod''2 re''4 |
mi''8 re'' dod''4. r16 fad'' |
fad''4. fad''8 sol'' red'' |
mi''2 si'8 si' |
si'2 si'4 |
mi''8.( la'16) la'4 re''8 lad' |
si'2 si'4 |
si'2 si'8 dod'' |
sold''2 sold''4 |
dod''2 fad''4 |
fad''2 fad''4 |
si'2 si'4 |
si'2 lad'4 |
si'2. |
r4 r r8 re'' |
la'2 la'4 |
la'4 la'8 re'' re''4 re''8 fad'' |
mi''2 r4 fad''8 mi'' re''4 mi''8 fad'' |
si'4. mi''8 mi''4. la'8 la'4 sold' |
la'8\fort la'16 si' dod''8 dod''16 re'' mi'' re'' dod'' si' |
la'4. la'8 la' la' |
re''4. re''8 re'' fad'' |
re''4. fad'16 sol' la'8 fad' |
mi'4. mi'8 sol' mi' |
la'4. fad'16 sol' la'8 fad' |
sol'4. re''8 re'' dod'' |
re''4. re''8 re'' mi'' |
dod''4. dod''8 dod'' red'' |
mi''4. mi''8 mi'' red'' |
mi''4. mi''8 mi'' sol'' |
fad''4. fad''16 fad'' fad''8 fad'' |
si'4. sold'16 la' si'8 sold' |
dod''4. dod''8 mi'' dod'' |
la'4. la'8 la' la' |
la'4. la'8 la' la' |
la'4. si'16 la' sol'8 si' |
mi'4. la'8 la' la' |
fad'2. |
