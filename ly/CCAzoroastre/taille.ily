\clef "taille" r4 |
R2.*3 |
R1. |
R1*5 |
sib2 do'4 mib' |
re' do' re' sib |
fa'2 re'4 sib |
do'2 fa |
fa4 sib sib2 |
fa sib |
sib4 do' re'2 |
sol4 do' lab reb' |
sol fa do' do' |
do'2 sib4 re' |
fa2. fa4 |
fa sib sol sol |
sib2 sib4 sib |
sib4 r r2 |
do'4 r r2 |
sol4 la la fa |
fa sol fa4. fa8 |
fa2. sib4\douxSug |
sib2 re'4 sib |
sib2 sib4 sib |
\sugNotes { re' mib' re' do' | }
sib4 sib sib re' |
mi' mi' mi' do' |
do' do' do' mi' |
fa' fa' fa' re' |
re'2 sol' |
do' la4 la |
do'2 do'4 do' |
do'2 lab4 lab |
sib2 sib4 sib |
sib2 sol4 la |
sol1 |
la4 la8 la la4 sib |
la4 re'8 re' sol4 sol |
sol2. sol'4 |
fa'4. fa'8 do'4 do' |
do'2 do'4 do' |
sib sib fa4. fa8 |
fa2 r |
r r8 re' re' re' |
fa' fa' fa' fa' fa' fa' fa' fa' |
sol' sol' sol' sol' sol' sol' sol' sol' |
sol' sol' sol' mib' do' re'16 mib' fa'8. mib'16 |
re'2 r4 re'\douxSug |
sib2 do'4 sol |
re'2. re'4 |
re'2 do'8 la sib4 |
sib sib fa fa'8 re' |
do'2 sol4 sol8 sol |
la2. fa4 |
sol sib sib sol |
la2 la4 la |
re'2 re'4 re' |
mib'2 mib'8 mib' sib do' |
do'2 r |
r r8 re' re' re' |
fa' fa' fa' fa' fa' fa' fa' fa' |
sol' sol' sol' sol' sol' sol' sol' sol' |
sol' sol' sol' mib' do' re'16 mib' fa'8. mib'16 |
re'2 re'4\douxSug re'8 do' |
sib4 re' mib'8 fa' do' do' |
sib1 |
fa'2 do' |
sib sib4 mib' |
mib'2 do'4 do' |
sol2 do'4 do' |
do'2 do'4 do' |
sol2 si4 si |
do'2 r |
fa'2 sib4. sib8 |
sib2 do'4. mib'8 |
la4 do'8 sib re'4. la8 |
sib2 sib4. fad8 |
sol4 sol sol8 fa' mib' re' |
do'2 do'4 do' |
re'2 mib'4 mib' |
mib'2 mib'4 mib' |
do'2 do'4 fa' |
re'2.

