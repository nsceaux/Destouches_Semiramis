\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { \haraKiriFirst } <<
        { s4 s2.*3 s1. s1*25
          \footnoteHere #'(0 . 0) \markup\wordwrap {
            Mesure ajoutée sur le manuscrit, ne figurant pas dans
            l'édition imprimée.
          }
        }
        \global \includeNotes "dessus"
      >>
      \new Staff \with { \haraKiriFirst } <<
        \global \includeNotes "haute-contre"
      >>
      \new Staff  \with { \haraKiriFirst } <<
        \global \includeNotes "taille"
      >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*3 s1 \bar "" \break s2 s1*3\break s1*7\pageBreak
        s1*8\break s1*8\pageBreak
        s1*6\break s1*6\pageBreak
        s1*5\break s1*4\pageBreak
        s1*4\break s1*3\pageBreak
        s1*4\break \grace s8 s1*4\pageBreak
        s1*3 s2 \bar "" \break s2 s1*4\pageBreak
        s1*4\break s1*3\pageBreak
        s1*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
