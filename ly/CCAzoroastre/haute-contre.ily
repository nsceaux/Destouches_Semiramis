\clef "haute-contre" r4 |
R2.*3 |
R1. |
R1*5 |
sib2 do'4 mib' |
re' do' re' sib |
fa'2 sib4 re' |
do' sib do' la |
sib2 sol4 sol |
sib2 sib4 re' |
re'2 re'4 si |
do'4 mi' fa'2 |
mi'4 fa' sol' fa'8 mi' |
fa'2. fa'4 |
mib'2. mib'4 |
re'2 mib'4 sol' |
fa' mib' fa' re' |
mib'2 r |
la4 r r2 |
sib4 la do' la |
sib4. la8 la4.\trill sib8 |
sib2. re'4\douxSug |
re'2 sol'4 fa' |
mib'2 mib'4 mib' |
\sugNotes { re' mib' re' do' | }
re' re' re' sol' |
sol' sol' sol' fa' |
mi' mi' mi' mi' |
la' la' fa' fa' |
fa'2 mi'4. fa'8 |
fa'2 do'4 do' |
fa'2 fa'4 fa' |
do'2 do'4 do' |
mib'2 mib'4 mib' |
sib2 sib4 re' |
sib2 sol |
re'4 re'8 re' re'4 re' |
la'4 la'8 la' re'4 sib |
sib2. sib4 |
la4. do'8 do'4 mi' |
fa'2 fa'4 fa' |
fa' sol' fa'4. mib'8 |
re'2 r |
r r8 sib' sib' sib' |
sib' sib' sib' sib' la' la' la' la' |
do'' do'' do'' do'' sib' sib' sib' sib' |
sib' sib' do'' sol' fa' sib' la'8. sib'16 |
sib'2 r4 sib'\douxSug |
sib'2 sol'4 sol' |
la'2. fad'4 |
sol'2 la'4 sol' |
sol' sol'8 sol' la'4 fa'8 fa' |
fa'2 do'4 do'8 do' |
do'2. do'4 |
sol' sol' sol' sol'8 mi' |
fa'2 fa'4 fa' |
fa'2 fa'4 sib' |
sol'2 sol'8 mib' mib' sol' |
fa'2 r |
r r8 sib' sib' sib' |
sib' sib' sib' sib' la' la' la' la' |
do'' do'' do'' do'' sib' sib' sib' sib' |
sib' sib' do'' sol' fa' sib' la'8. sib'16 |
sib'2 sib'4\douxSug sib'8 fad' |
sol'2 sol'8 sib' fa'8. fa'16 |
fa'1 |
fa'2 mib' |
sib' sib'4 sib' |
mib'2 mib'4 do' |
re'2 mi'4 mi' |
mi' fa' re' re' |
re'2 re'4 re' |
do'2 r |
do'2 fa'4 fa' |
sol'2 sol'4 sol' |
re'4. re'8 re'4. re'8 |
re'2 re'4. la'8 |
sib'4 sib' mib' mib'8 fa' |
fa'2 fa'4 fa' |
fa'2 sol'4 sib' |
do''2 do''4 sol' |
fa'2 fa'4 fa' |
fa'2.
