\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "flute" >>
    \new Staff << \global \includeNotes "violon" >>
  >>
  \layout { }
}
