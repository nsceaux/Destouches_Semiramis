\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "flute" >>
    \new Staff <<
      \global \includeNotes "violon"
      \includeFigures "chiffres"
      \origLayout { s2.*8\break s2.*7\break }
    >>
  >>
  \layout { }
  \midi { }
}
