\clef "dessus" <>^"Violons" R2. |
la'8^\doux sol' la' si' dod'' la' |
re''2 re'4 |
la'8 sib' la' sol' fa' mi' |
fa'2 r4 |
sib'4 sol'2 |
re''8 do'' sib'2 |
la'2. |
R2. |
sib'8 do'' re'' do'' sib' la' |
sol'2 r4 |
do''8 sib' do'' re'' do'' sib' |
la'2 r4 |
re''2 re''4 |
sib' do''2 |
fa'8 sol' la' sol' fa' mi' |
re'2 r4 |
sol'2 r4 |
mi'2 r4 |
la'8 sib' la' sol' fa' mi' |
fa'2 r4 |
sol'2 r4 |
sib'8 sol' la'4 la |
re'2. |
