\clef "dessus" <>^"Flutes" re''8 dod'' re'' mi'' fa'' sol'' |
la''2 mi''4 |
fa''8 sol'' fa'' mi'' re'' mi'' |
dod''2 dod''4 |
la''8 sol'' la'' si'' do''' la'' |
re''' do''' sib'' la'' sol'' la'' |
fa'' sol'' sol''4.(\trill fa''16 sol'') |
la''2. |
la''8 sib'' do''' sib'' la'' sol'' |
fa''4.\trill mi''8 re''4 |
sib''8 do''' re''' do''' sib'' la'' |
sol''4. fa''8 mi''4 |
do'''8 re''' do''' sib'' la'' sol'' |
fa'' mi'' fa'' sol'' la'' fa'' |
sib'' la'' sol''4.\trill fa''8 |
fa''2. |
la''2 r4 |
si''2 r4 |
si''8 la'' si'' dod''' re''' mi''' |
dod'''4. si''8 la''4 |
re'''8 do''' sib'' la'' sol'' fa'' |
sib'' la'' sol'' fa'' mi'' re'' |
sol'' fa'' mi''4.\trill re''8 |
re''2. |
