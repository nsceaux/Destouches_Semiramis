\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse-continue #:notes "violon" #:clef "dessus")
   (silence #:on-the-fly-markup , #{ \markup\tacet#48 #}))
