\newBookPart #'()
\act "ACTE III"
\sceneDescription\markup\wordwrap-center {
  Le Théâtre représente une vestibule orné de Statues des Rois de Babylone.
}
\scene "Scene Premiere" "Scene I"
\sceneDescription\markup\wordwrap-center {
  \smallCaps Zoroastre seul.
}
% 3-1
\pieceToc\markup\wordwrap {
  Zoroastre :
  \italic { Qu’ai-je appris ! quels forfaits ! quelle injure mortelle }
}
\includeScore "CAAzoroastre"
\newBookPart #'(full-rehearsal)

\scene "Scene Deuxiéme" "Scene II"
\sceneDescription\markup\wordwrap-center\smallCaps {
  Zoroastre, Semiramis.
}
% 3-2
\pieceToc\markup\wordwrap {
  Zoroastre, Semiarmis :
  \italic { Ah ! Perfide, osez-vous soûtenir ma présence }
}
\includeScore "CBAsemiramisZoroastre"
\newBookPart #'(full-rehearsal)

\scene "Scene Troisiéme" "Scene III"
\sceneDescription\markup\wordwrap-center {
  \smallCaps Zoroastre, seul.
}
% 3-3
\pieceToc\markup\wordwrap {
  Zoroastre :
  \italic { Pour tant de maux soufferts, quels maux dois-je lui rendre ? }
}
\includeScore "CCAzoroastre"
\newBookPart #'(full-rehearsal)

\scene "Scene Quatriéme" "Scene IV"
\sceneDescription\markup\wordwrap-center {
  \smallCaps Zoroastre, Troupe de Demons, de Magiciens & de Magiciennes.
}
% 3-4
\pieceToc\markup\wordwrap {
  Chœur : \italic { L'univers porte nos fers }
}
\includeScore "CDAchoeur"
\newBookPart #'(full-rehearsal)
% 3-5
\pieceToc "Premier air pour les Magiciens"
\includeScore "CDBair"
% 3-6
\pieceToc\markup\wordwrap {
  Zoroastre : \italic { Semiramis a trahi mon ardeur }
}
\includeScore "CDCzoroastre"
\newBookPart #'(full-rehearsal)
% 3-7
\pieceToc\markup\wordwrap {
  Zoroastre, chœur : \italic { Versons l’épouvante dans les cœurs }
}
\includeScore "CDDchoeur"
\newBookPart #'(full-rehearsal)
% 3-8
\pieceToc "Deuxiéme air"
\includeScore "CDEair"
\newBookPart #'(full-rehearsal)
% 3-9
\pieceToc\markup\wordwrap {
  Zoroastre, chœur : \italic { Commande à l'Empire tenebreux }
}
\includeScore "CDFchoeur"
\newBookPart #'(full-rehearsal)
% 3-10
\pieceToc "Deuxiéme air"
\reIncludeScore "CDEair" "CDGair"
\newBookPart #'(full-rehearsal)
% 3-11
\pieceToc\markup\wordwrap {
  Zoroastre : \italic { Arrêtez. Les Enfers sont prêts à m'inspirer }
}
\includeScore "CDHzoroastre"
\newBookPart #'(full-rehearsal)
% 3-12
\pieceToc "Entr'acte"
\reIncludeScore "CDEair" "CDIair"
