\clef "basse"
<<
  \tag #'basse { r4 | R2.*3 | R1. R1*3 | }
  \tag #'basse-continue {
    re4\repeatTie~ |
    re2. |
    sol |
    dod |
    re1 do2 |
    sib,2. sol,4 |
    la,1 |
    re |
  }
>>
R1*2 |
<>^"Basses de violes" sib2 do'4 mib' |
re' do' re' sib |
<>^"Basses de violons" fa2 sol4 sib |
la sol la fa |
<>^"Tous et la contre-basse" sib,2 do4 mib |
re do re sib, |
sol, la, si, sol, |
do2 reb |
do4 re mi do |
fa,2 sol,4 sib, |
la, sol, la, fa, |
sib,2 do4 mib |
re do re sib, |
mib2 r |
re r |
do4 fa fa fa |
sib, mib, fa,2 |
sib,4 sib, sib, sib |
sol sol sol sib |
mib mib mib mib |
\sugNotes { sib, sib, sib, sib, | }
sib, sib, sib, sib, |
do do do do |
do do do do' |
la la la sib |
sol2~ sol\trill |
fa4 fa, fa, fa, |
fa,2 fa4 fa |
lab lab lab lab |
mib mib mib mib |
sol sol sol fa |
mib2~ mib\trill |
re4 re8 re re4 re |
re'4 re'8 re' sib4 sol |
do do8 do do4 do' |
fa4. fa8 fa4 do |
fa fa8 fa mib4 mib |
re mib fa fa, |
sib,2 r | \allowPageTurn
r r16 sib la sol fa mib re do |
sib,8 sib,16 do re8 re16 mib fa8 fa fa fa |
do do16 re mib8 mib16 fa sol8 sib mib sol |
mib sol do mib fa sib, fa,4 |
sib,8^"Tous" sib,16 la, sib,8^"croches égales" do sib, do re sib, |
mib mib16 re mib8 fa mib re do mib |
re re16 do re8 mi fad mi fad re |
sol sol16 fad sol8 sib la do' sib sib, |
mib do16 re mi8 do fa fa16 mi fa8 sib, |
fa, fa,16 sol, la,8 fa, do re do sib, |
la, la,16 sol, la,8 sib, la, sib, la, sib,16 la, |
sol,8 sol,16 la, sol,8 fa, mi, mi,16 re, mi,8 do, |
fa, fa,16 mi, fa,8 sol, la, fa, la, fa, |
sib, sib,16 la, sib,8 do re do re sib, |
mib mib16 fa mib8 re do mib,16 fa, sol,8 mib, |
fa,2 r | \allowPageTurn
r r16 sib la sol fa mib re do |
sib,8 sib,16 do re8 re16 mib fa8 fa fa fa |
do do16 re mib8 mib16 fa sol8 sib mib sol |
mib sol do mib fa sib, fa,4 |
sib,8 sib,16 la, sib,8 do sib, do sib, la, |
sol, sol,16 la, sib,8 la, mib8 re16 mib fa8 fa, |
<< { \voiceThree sib,8 sib,16 la, sib,8 do re sib, sib,,4 | }
  \\ \new CueVoice { \voiceOne sib,1_"[manuscrit]" } >>
sib,2 do |
re mib |
lab lab4 lab |
sol fa mi do |
fa fa, fa, fa, |
sol, sol, sol, sol, |
do8 do16 si, do8 re mi re mi do |
fa8 fa16 sol fa8 mib re mib re sib, |
mib mib16 fa mib8 re do re mib do |
re do16 sib, la,8 sol, re do re re, |
sol, sol,16 fad, sol,8 la, sol, fad, sol, re, |
sol, sol,16 la, sib,8 sol, mib re do sib, |
fa fa16 mib fa8 sol fa sol fa mib |
re re16 do re8 sib, mib fa mib re |
do do16 sib, do8 re mib re mib do |
fa fa16 mib fa8 sol fa mib fa fa, |
sib,2*3/4~ \hideNotes sib,16
