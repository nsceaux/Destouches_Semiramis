\clef "vbasse" la4 |
sib8 do' sib4.\trill\prall la8 |
\appoggiatura la8 sib2 sib4 |
sol4.\trill\prall fa8 sol la |
fa2\trill re4 la8 sib do'4 do'8 la |
re'2 r4 sib |
mi2 la4 la8 la |
re2 re |
R1*19 |
r2 r4 sib |
sol2 sol4 sib |
mib2 mib4 mib |
\appoggiatura mib8 \sugNotes { sib,1~ | }
sib,2 sib4 sib |
mi2 mi4 fa |
do2 do'4. do'8 |
\appoggiatura sib8 la2 la4 sib |
\appoggiatura la8 sol2~ sol\trill |
fa r |
r fa4. fa8 |
lab2 lab4. lab8 |
mib2 mib4. mib8 |
sol2 sol4 fa |
\appoggiatura fa8 mib2~ mib\trill |
re1 |
re'4 re'8 re' sib4 sol |
do2. do'4 |
fa4. fa8 fa4 do |
fa2 fa4 sol8 la |
sib2 sib4 sib8 la |
sib2 <>^\markup\italic { Le Théatre s’obscurcit } r2 |
R1*4 |
re'2.^"Mesuré" sib4 |
sol\trill\prall sol8 sol la4 sib |
fad4. mi8( re2) |
sib4 sib8 sib do'4 re' |
sol4 sol8 do' la4\trill la8 sib |
\appoggiatura sib do'2 mi4 mi8 sol |
do2. do'4 |
mi4 mi8 fa sol4 la8 sib |
la4. sol8( fa2) |
r4 fa8 fa sib4 sib8 fa |
\appoggiatura fa8 sol2 mib8 mib16 mib mib8 sol |
do2 <>^\markup\italic\override #'(baseline-skip . 0) \column {
  \line { Le Théâtre change, & représente un Palais magique, }
  \line { orné de Statuës qui portent des flambeaux. }
}
r2 | \noBreak
R1*4 |
r4 r8 fa sol4. la8 |
sib4. re'8 sol8 sol fa8. mib16 |
re4.\trill do8( sib,2) |
lab2 lab4 lab |
lab2 sol4 sol |
do'2 do'4 do' |
si4 si8 si do'4 sol |
lab2 lab4 fa |
\appoggiatura mib8 re2 sol4 sol8 sol |
sol2. r8 do' |
la2\trill sib4 do'8 re' |
sol4 sol8 sol do' sib la sol |
fad sol la sib la4.\trill sol8 |
sol2 sib4 re' |
sib sib sol\trill la8 sib |
\appoggiatura sib do'2 do'4 r8 fa |
sib4. lab8 sol4. sib8 |
\appoggiatura sib8 mib2 sol4 la |
la2\trill la4 sib |
sib2
