Pour tant de maux souf -- ferts, quels maux dois- je lui ren -- dre ?
Et com -- ment me van -- ger ? mon art va me l’ap -- pren -- dre.
Ter -- ri -- ble ram -- part des En -- fers
Styx af -- freux, dont les flots en -- vi -- ron -- nent les om -- bres
E -- le -- vez jus -- qu’à moi vos va -- peurs les plus som -- bres.
Que le So -- leil vain -- cu se ca -- che dans les Mers,
Que ce jour manque à l’U -- ni -- vers.
Lieux té -- moins de mon in -- for -- tu -- ne,
Lieux que je fais en -- vain re -- ten -- tir de mes cris,
Dis -- pa -- rois -- sez, tom -- bez, vôtre as -- pect m’im -- por -- tu -- ne.
Qu’un ma -- gi -- que Pa -- lais nais -- se de vos dé -- bris.

Quà ma fu -- reur tout prête i -- ci des ar -- mes.
Tris -- tes Ob -- jets qui por -- tez ces flam -- beaux,
Ar -- ra -- chez du sein des tom -- beaux ;
A -- ni -- mez- vous. De -- mons, van -- gez mes lar -- mes,
C’est moi qui le pre -- mier vous ai don -- né la Loi.
Vous Mor -- tels ins -- truits à mes char -- mes,
Ve -- nez de tou -- tes parts, se -- con -- dez vô -- tre Roi.
