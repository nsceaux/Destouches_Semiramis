\key re \major
\time 2/2 \midiTempo#160 s1
\time 3/2 s1.*2
\time 2/2 s1
\time 3/2 \grace s8 s1.
\time 2/2 s1*2
\time 3/2 \grace s16 s1.
\time 2/2 \midiTempo#80 s1 \midiTempo#160 s1
\time 3/2 s1.
\time 2/2 s1*4
\time 3/2 s1.
\time 2/2 s1*2
\time 3/2 s1.
\time 2/2 s1
\time 3/2 s1.
\time 2/2 s1
\time 3/2 s1.
\time 2/2 s1*5
\time 3/2 s1.*2
\time 2/2 s1 \midiTempo#80 s1*2 \midiTempo#160
\digitTime\time 3/4 s2.*18
\time 2/2 s1*2
\digitTime\time 3/4 s2.*2
\time 2/2 s1*6
\time 3/2 s1.
\time 2/2 \midiTempo#80 s1*2 \midiTempo#160
\time 3/2 s1.
\time 2/2 \grace s16 s1*5 \midiTempo#80 s1 \midiTempo#160
\time 3/2 s1.
\digitTime\time 3/4 s2.*6 \midiTempo#80 s2. \midiTempo#160 s2.*2
\time 3/2 \grace s16 s1.
\digitTime\time 3/4 s2.*5
\time 2/2 s1*2
\digitTime\time 3/4 \grace s8 s2.*5
\time 3/2 s1.*5 \midiTempo#80 s1. \midiTempo#160
\time 2/2 \grace s16 s1*6
\time 3/2 s1.
\time 2/2 s1*4
\time 3/2 s1. \midiTempo#80 s1. \midiTempo#160
\time 2/2 s1*2
\time 3/2 s1.*2
\time 2/2 \midiTempo#80 s1 \midiTempo#160 s1 s2 \bar ""
