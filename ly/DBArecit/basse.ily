\clef "basse" re1~ |
re2 fad sol4 la |
si2 fad1 |
sol2 la4 re |
la,2 la fad |
sol red |
mi1 |
sold,1. |
la,2 la |
fad1 |
sol fad2 |
mi red |
mi~ mi8 mi re dod |
si,4 si,8 dod re4 sol, |
la,1 |
re2. mi4 fad mi |
re1 | \allowPageTurn
mi |
fad4.*13/12 sol32 fad mi re2 re' |
dod' si |
la si1 |
dod'2 re' |
dod' sold4 fad mid2 |
fad red |
mi la4 la, |
re2 la4 re |
la,2 re |
sol mi |
fad si, red |
mi sol mi |
fad fad, |
si, si |
mi4 si8 la sol4 mi | <>^"égales"
<<
  \new CueVoice { \voiceOne fad8 sold lad fad si4 | }
  { \voiceTwo fad8 mi re dod si,4 | \oneVoice }
>>
si4. re'8 dod' si |
lad4. sold8 lad fad |
si la sol fad mi4 |
fad8 mi fad sold lad fad |
si2 fad4 |
sol2 sol4 |
fad sol8 fad mi re |
la4 la,2 |
re4 do2 |
si,4 la,8 sol, fad, la, |
sol, fad, sol, mi, la,4 |
si,8 la, si, do si, si,, |
mi fad sol mi si4 |
dod'8 re' lad4\trill si |
fad8 mi fad sold lad fad |
si4 mi8 fad sol mi |
fad4 fad,2 |
si,4 si dod'2 |
re' dod'4 si |
la2. |
si2 la4 |
sol la si si, |
mi2 la4 la, |
re4 lad, si, fad, |
sol,2 sol |
mi la4. re8 |
sol2. mi4 |
fad2 lad, si, |
dod4 la, si,8 la, sold, fad, |
dod2 re8 si, dod4 |
fad,2 lad,1 |
si,2 dod |
re sol |
re red |
mi1 |
sol2 mi |
fad re |
mi1 fad4 fad, |
si,2 si4 |
la2. |
sol |
fad4. mi8 re dod |
si,2. |
red |
mi4 sold,2 |
la,2. |
lad, |
si,1. | \allowPageTurn
dod2 fad4 |
sold2. |
la |
si |
dod' |
la2 mid~ |
mid1 |
fad2. |
mi |
re |
dod |
re |
sol2 mi1 |
fad si4 la |
sol1 red2 |
mi1. |
mid2 fad8 mid re dod si,2 |
fad si4 la sol8 la sol fad |
mi2~ mi8 mi re dod |
si,4 la, sol,2 |
fad,1 |
sol,2 sold, |
la, lad, |
si, dod |
re1 sol2 |
la sol |
fad4 mi re2 |
mi la4 la, |
mi2 re |
dod re mi4 mi, |
la,2 la re' |
dod' do' |
si4 fad sol fad8 mi |
re1 sol2 |
fad2. mi8 re la4 la, |
re2 sol |
mi4 la re dod8 si, |
la,2*7/8~ \hideNotes la,16
