<<
  %% Semiramis
  \tag #'(basse semiramis) {
    \semiramisMark r4 re'' la' la' |
    r4 la'8 la' re''4. re''8 si' si' la' sol' |
    fad'4\trill fad' la' la'8 la' si'4. do''8 |
    si'4\trill dod''8 re'' dod''4\trill dod''8 re'' |
    \appoggiatura re''8 mi''2 r4 mi''8 fad'' re''4 re''8 fad'' |
    si'4. si'8 si' la' la' sold' |
    sold'4. si'8 si' si' dod'' re'' |
    \appoggiatura re''16 mi''2 mi''4 r8 mi'' si' si' dod'' re'' |
    dod''4 dod'' <<
      \tag #'basse { s2 s1 s1. s1*4 s2 \semiramisMarkText "Vivement" }
      \tag #'semiramis { r2 R1 R1. R1*4 r2 <>^\markup\italic Vivement }
    >> r8 fad'' dod''4 lad'\trill lad'8 dod'' |
    fad'4. fad''8 re'' dod'' si' la' |
    sold'4. mi''8 dod''\trill\prall si' si' dod'' |
    lad'4 lad' r4 re'' fad'8 fad' fad' sol' |
    la'4. la'8 si' si' dod'' re'' |
    dod''4.\trill dod''8 red''2 r8 red'' mid'' fad'' |
    mid''4 dod''8 si' la'4 sold'8 fad' |
    dod''2 mid''4 fad'' si' si'8 dod'' |
    lad'4 lad'8 fad' la' la' si' fad' |
    \appoggiatura fad' sol'4 sol'8 mi'' dod'' si' la' sol' |
    fad'4 la'8 si' dod''4 dod''8 re'' |
    \appoggiatura re''8 mi''4. dod''8 fad'' mi'' re'' dod'' |
    si'4 mi''8 re'' dod''4\trill re''8 mi'' |
    lad'2 re''4 dod'' si' la'?8 si' |
    \appoggiatura la'8 sold'4. mi''8 mi''4.( re''8)\trill dod''4 dod''8 re'' |
    re''2( dod''2)\trill |
    si'4 <<
      \tag #'basse { s2. s1 s2.*18 s4. \semiramisMark }
      \tag #'semiramis { r4 r2 R1 R2.*18 r4 r8 }
    >> re''8 dod''\trill\prall si' la' sol' |
    fad'4\trill fad'8 sol' la'4 la'8 re'' |
    dod''2 dod''8 re'' |
    red''2 si'8 si' |
    mi''4. mi''8 mi''\trill mi'' mi'' red'' |
    \appoggiatura red''8 mi''4 <<
      \tag #'basse {
        s2. s1*4 s1. s1*2 s1. s1*6 s1. s2
        \semiramisMarkText "douloureusement"
      }
      \tag #'semiramis {
        r4 r2 R1*4 R1. R1*2 R1. R1*6 R1. r4 r
        <>^"Douloureusement"
      }
    >> r8 sol' |
    sol'4 fad' fad'8 fad' |
    si'2 dod''8 re'' |
    lad'2\trill\prall si'8 dod'' |
    \appoggiatura dod''8 re''4 re''8 <>^\markup\italic { plus vivement } fad'' re''4 |
    si'2\trill si'8 dod'' |
    sold'4 si'8 si'16 si' dod''8 re'' |
    dod''2 <>^\markup\italic tendrement dod''8 dod'' |
    dod''2 dod''8 mi'' |
    \appoggiatura mi''16 re''4. dod''8 si'4 dod'' si' la' |
    sold' sold' r8 re'' |
    re''4. re''8 re'' mi'' |
    \appoggiatura re''16 dod''2 red''8 mi'' |
    red''2 mid''8 fad'' |
    mid''4.\trill red''8( dod''4) |
    <>^\markup\italic vivement fad''4. dod''8 si'4\trill la'8 sold' |
    dod''4 dod''8 re'' si'4\trill dod''8 sold' |
    \appoggiatura sold'8 la'4. r8 fad''4 |
    lad'4. lad'8 lad' si' |
    si'2 r8 <>^\markup\italic tendrement re'' |
    sol'4. sol'8 sol' la' |
    fad'4 fad' re''8 dod'' |
    si'2 dod''4. re''8 dod''4. si'8 |
    lad'4 lad' r8 <>^\markup\override #'(baseline-skip . 2) \italic\column { vivement à part. } dod''8 dod'' red'' red''4. mi''8 |
    mi''2. si'8 do'' la'4\trill\prall si'8 fad' |
    \appoggiatura fad'8 sol'4. <>^\markup\italic à Arsane. sol''8 mi''2\trill dod''4 re''8 mi'' |
    sold'2 lad'4 si'8 dod'' \appoggiatura dod''8 re''4. mi''8 |
    dod''4 <<
      \tag #'basse { s4 s1 s1*6 s2 \semiramisMark }
      \tag #'semiramis { r4 r2 r R1*6 r2 }
    >> re''4 mi''8 fad'' mi''4.\trill fad''8 |
    dod''4 mi''8 re'' dod''4\trill si'8 la' |
    re''4 re''8 mi'' fad''4 fad''8 re'' |
    si'4\trill si'8 re'' do''4 si'8 la' |
    sold'4 si'8 dod'' re''4 dod''8 si' |
    mi''4. dod''8 fad''4. si'8 dod''4. re''8 |
    dod''2 la'4 la'8 sol' fad'\trill fad'16 mi' fad'8 re' |
    la'8 la' la' la' mi''4 mi''8 fad'' |
    \appoggiatura mi''8 re''4 re''8 dod'' si'4 la'8 sol' |
    fad'2\trill fad''4. mi''8 re''4 dod''8 si' |
    la'2 re''4 mi''8 fad'' mi''4.\trill re''8 |
    re''4.
    \tag #'semiramis { r8 r2 R1 r2 }
  }
  %% Arsane
  \tag #'(arsane basse) {
    <<
      \tag #'basse { s1 s1.*2 s1 s1. s1*2 s1. s2 \arsaneMarkText "vivement" }
      \tag #'arsane { \arsaneMark R1 R1.*2 R1 R1. R1*2 R1. r2 <>^"Vivement" }
    >> dod'8 dod'16 dod' dod'8 la |
    re'4 re'8 fad' re'4\trill re'8 re' |
    si4\trill si re' re'8 re' red'4. mi'8 |
    mi'4 mi'8 mi' fad'4 fad'8 fad' |
    si4. sol'8 si si si dod' |
    re'4 re'8 mi' fad'4 fad'8 sol' |
    mi'4 mi'8 la dod' dod' dod' re' |
    re'2 <<
      \tag #'basse {
        s2 s s1*2 s1. s1 s1. s1 s1. s1*5 s1.*2 s1 s4 \arsaneMark
      }
      \tag #'arsane { r2 r R1*2 R1. R1 R1. R1 R1. R1*5 R1.*2 R1 r4 }
    >> si4 re'8 re' mi' fad' |
    \appoggiatura fad' sol'4 re'8 mi'16 fad' mi'4.\trill\prall re'8 |
    dod'2\trill <>^\markup { Air, mesuré } si8 si |
    sol'4. fad'8 mi'4 |
    \appoggiatura mi'8 fad'2 mi'8 fad' |
    \appoggiatura mi'8 re'2 mi'8 fad' |
    dod'2 dod'4 |
    re'4. re'8 dod'\trill re' |
    si2\trill si8 dod' |
    re'2 mi'8 fad' |
    fad'4( mi'2)\trill |
    re' fad'8 sol' |
    red'4 red' si |
    mi'2 fad'8 sol' |
    fad'2\trill mi'8 fad' |
    \appoggiatura fad'16 sol'2 re'4 |
    mi'8 fad' dod'4\trill re' |
    lad2\trill dod'8 fad' |
    re'2\trill dod'8 re' |
    re'4( dod'2)\trill |
    si4. <<
      \tag #'basse { s8 s2 s1 s2.*2 s1 s4 \arsaneMarkText "vivement" }
      \tag #'arsane { r8 r2 R1 R2.*2 R1 r4 <>^"Vivement" }
    >> sol'8 fad' mi'4 re'8 dod' |
    fad'4. dod'8 re' re' re' fad' |
    si4 si re' re'8 si |
    sol'4. sol'8 sol'4. fad'8 |
    fad'2 mi'4 mi'8 sol' |
    dod'2 fad'4 r8 dod' re'4 re'8 si |
    sold4 dod'8 fad' red'4 mid'8 fad' |
    mid' mid' red' mid' fad' fad'16 fad' fad'8 mid' |
    fad'2 dod'4 dod'8 dod' dod'4. fad'8 |
    \appoggiatura mi'16 re'4. re'8 mi' mi' fad' sol' |
    fad'4\trill fad'8 fad' si dod' re' mi' |
    fad'4. sol'8 la' la' sol' fad' |
    \appoggiatura fad'16 sol'4 sol'8 fad' mi' re' dod' si |
    mi'4 mi'8 re' dod'4 re'8 mi' |
    lad2 fad'8 mi'16 re' dod'8 si |
    sol'2~ sol'8 fad' mi' re' dod'4. re'8 |
    si2 <<
      \tag #'basse { s4 s2.*8 s1. s2.*5 s1*2 s2.*5 s1.*5 s4 \arsaneMark }
      \tag #'arsane { r4 R2.*8 R1. R2.*5 R1*2 R2.*5 R1.*5 r4 }
    >> fad'8 sol' red'4\trill\prall si16 si dod' red' mi'8 mi' mi' fad' |
    \appoggiatura fad'16 sol'4 mi'8 sol' si si si dod' |
    \appoggiatura dod'16 re'4. re'8 re'\trill re' re' mi' |
    \appoggiatura mi'16 fad'4. dod'8 dod' dod' re' re' |
    si4\trill si8 mi' re'4\trill\prall dod'8 re' |
    dod'4\trill sol'8 fad' mi'4\trill re'8 dod' |
    \appoggiatura dod'16 re'4 re'8 re' mi'4 fad'8 sol' |
    fad'4\trill fad'
    <<
      \tag #'basse { s1 s1*4 s1.*2 s1*2 s1.*2 s4. \arsaneMark }
      \tag #'arsane { r2 r R1*4 R1.*2 R1*2 R1.*2 r4 r8 }
    >> fad'8 si8. re'16 mi'8 fad' |
    sol'4 fad'8 mi' fad'4 mi'8 re' |
    \appoggiatura re'8 mi'4 mi'
  }
>>