\tag #'(semiramis basse) {
  Ar -- sa -- ne, quel spec -- tacle i -- ci vient me sur -- pren -- dre ?
  Prés de ces murs sa -- crez, on ar -- rê -- te mes pas.
  Eh ! qui donc con -- tre moi re -- vol -- te mes Sol -- dats ?
  Se -- roit- ce Zo -- ro -- as -- tre ? à qui je dois m’en pren -- dre ?
}
\tag #'(arsane basse) {
  Con -- tre tous ses ef -- forts je sçau -- rois vous dé -- fen -- dre :
  Mais un pe -- ril plus grand doit cau -- ser vôtre ef -- froi :
  Le Peuple aime A -- mes -- tris, il peut tout en -- tre -- pren -- dre,
  Il s’ar -- me con -- tre vous.
}
\tag #'(semiramis basse) {
  Non, Per -- fi -- de, c’est toi ;
  C’est toi qui me tra -- his, mes maux sont ton ou -- vra -- ge.
  Mais, tu n’as pas long- tems jou -- i de mon er -- reur,
  J’ai lû, je lis en -- cor dans le fond de ton cœur.
  Cœur in -- di -- gne du Trô -- ne, & fait pour l’es -- cla -- va -- ge ;
  J’y vois la tra -- hi -- son, le mé -- pris des bien -- faits,
  J’y vois con -- tre mes jours tes bar -- ba -- res pro -- jets,
  Et tes lâ -- ches sou -- pirs pour cel -- le qui m’ou -- tra -- ge.
}
\tag #'(arsane basse) {
  Ar -- sa -- ne ne sçait point dis -- si -- mu -- ler ses feux.
  L’en -- sen -- sible A -- mes -- tris oc -- cu -- poit tous mes vœux,
  A -- vant que vô -- tre main vint m’of -- frir tant de gloi -- re ;
  Je l’a -- do -- re mal -- gré ses mé -- pris ri -- gou -- reux,
  C’est de l’A -- mour sur moi la pre -- mie -- re vic -- toi -- re.
}
\tag #'(semiramis basse) {
  Re -- dou -- ble cet A -- mour, il me vange en -- cor mieux.
  Tu la perds : de leur choix de -- man -- de compte aux Dieux.
}
\tag #'(arsane basse) {
  O -- sez- vous at -- tes -- ter des noms si re -- dou -- ta -- bles ?
  Ont- ils par -- lé ces Dieux, sçait- on leurs vo -- lon -- tez ?
  Non, la soif de re -- gner, des fu -- reurs im -- pla -- ca -- bles,
  Sont les Dieux que vous con -- sul -- tez.
  Ah ! ne de -- man -- dez plus, d’où nais -- sent les pré -- sa -- ges ;
  Quel crime at -- tire i -- ci la foudre & les o -- ra -- ges ?
  Vous at -- ten -- tez aux droits dont le Ciel est ja -- loux,
  Et la jus -- tice é -- clate à se van -- ger de vous.
}
\tag #'(semiramis basse) {
  Je fais pour le flé -- chir, un ef -- fort i -- nu -- ti -- le.
  Mais, Bar -- bare, est-ce à toi de me le re -- pro -- cher ?
  J’es -- pe -- rois a -- vec toi goû -- ter un sort tran -- quil -- le ;
  Au -- près de tes ver -- tus je cher -- chois un a -- zi -- le.
  Non, ta hai -- ne pour moi ne sçau -- roit se ca -- cher ;
  Aug -- men -- te tes mé -- pris, tri -- om -- phe de mes lar -- mes,
  Con -- tre toi prê -- te- moi des ar -- mes.
  Quel as -- cen -- dant fa -- tal m’a sou -- mise à sa Loi !
  In -- grat, que m’as- tu fait pour m’at -- ten -- drir pour toi ?
}
\tag #'(arsane basse) {
  A -- che -- vez, & bri -- sez les fers de la Prin -- ces -- se :
  Sau -- vez de tant de Rois le res -- te pré -- ci -- eux :
  Je ne de -- man -- de point de l’u -- nir à mes vœux ;
  Je nour -- ris dans mon cœur u -- ne vai -- ne ten -- dres -- se.
}
\tag #'(semiramis basse) {
  Eh ! pour -- quoi donc l’ai -- mer a -- vec tant de trans -- port !
  Tu par -- ta -- ges en -- fin les ri -- gueurs de mon sort :
  Tu con -- nois le tour -- ment d’ai -- mer qui nous ab -- hor -- re ;
  Que n’y puis-je a -- joû -- ter en -- co -- re
  De la ren -- dre sen -- sible, & per -- fi -- de pour toi.
  Va, de -- vien, s’il se peut, plus mal -- heu -- reux que moi.
}
\tag #'(arsane basse) {
  On vient. Voi -- ci l’ins -- tant du cru -- el sa -- cri -- fi -- ce.
}
