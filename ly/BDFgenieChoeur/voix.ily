<<
  %% Le Génie ordonnateur
  \tag #'(genie genie-hc basse) {
    \clef "vhaute-contre" <>^\markup\character "Le Genie Ordonnateur"
    sol'4. sol'8 |
    fa'4 mib'16[ re'8 mib'16] mib'4.\trill re'8 |
    re'2 re'4. mib'8 |
    fa'4 mib'8[ re'] mib'4 do' |
    re' sib <<
      \tag #'genie-hc { s2 s1*3 s2 }
      \tag #'(genie basse) { r2 R1*3 r2 }
    >> <>^\markup\character "Le Genie"
    re'4 mi' |
    fa' mi' fad'4. sol'8 |
    fad'2\trill\prall sol'4. la'8 |
    dod'4\trill re' sol'4. fa'8 |
    mi'4\trill re' <<
      \tag #'genie-hc { s2 s1*3 s2 }
      \tag #'(genie basse) { r2 R1*3 r2 }
    >> <>^\markup\character "Le Genie"
    sol'4. fa'8 |
    mib'16[ re'8.] do'16[ sib8.] do'4. re'8 |
    la2\trill re'4. do'8 |
    sib4 re' do'4.\trill sib8 |
    la4\trill sol <<
      \tag #'genie-hc { s2 s1*3 s2 }
      \tag #'(genie basse) { r2 R1*3 r2 }
    >> <>^\markup\character "Le Genie"
    sol'4. fa'8 |
    mi'4\trill re' sol'4. fa'8 |
    mi'4\trill re' dod'4. re'8 |
    la4 fa' mi'4.\trill re'8 |
    re'2 <<
      \tag #'genie-hc { s2 s1*3 s2 }
      \tag #'(genie basse) { r2 R1*3 r2 }
    >> <>^\markup\character "Le Genie"
    fa'4 re' |
    \appoggiatura re'16 mib'4. mib'8 mib'4.\trill re'8 |
    do'2\trill fa'4. re'8 |
    \appoggiatura do'16 sib4. sol8 mib'4. re'8 |
    do'4\trill sib re'4. mi'8 |
    fa'4 mi' fad'4. sol'8 |
    fad'4 re' sol'4. la'8 |
    \appoggiatura la'16 sib'4. la'16[ sol'] fad'4.\trill\prall sol'8 |
    sol'2 <<
      \tag #'genie-hc { s2 s1*3 s2 }
      \tag #'(genie basse) { r2 R1*7 r2 }
    >>
  }
  %% Dessus
  \tag #'vdessus {
    \clef "vdessus" r2 R1*3 r2 <>^\markup\character Chœur
    sol''4. sol''8 |
    fa''4 mib''16[ re''8 mib''16] mib''4.\trill re''8 |
    re''2 re''4. mib''8 |
    fa''4 mib''8[ re''] mib''4 do'' |
    re'' sib'
    r2 R1*3 r2 <>^\markup\character Chœur
    re''4 mi'' |
    fa'' mi'' fad''4. sol''8 |
    fad''2 sol''4. la''8 |
    dod''4 re'' sol''4. fa''8 |
    mi''4\trill re''
    r2 R1*3 r2 <>^\markup\character Chœur
    sol''4. fa''8 |
    mib''16[ re''8.] do''16[ sib'8.] do''4. re''8 |
    la'2\trill re''4. do''8 |
    sib'4 re'' do''4.\trill sib'8 |
    la'4\trill sol'
    r2 R1*3 r2 <>^\markup\character Chœur
    sol''4. fa''8 |
    mi''4\trill re'' sol''4. fa''8 |
    mi''4\trill re'' dod''4. re''8 |
    la'4 fa'' mi''4.\trill re''8 |
    re''2
    r2 R1*7 r2 <>^\markup\character Chœur
    fa''4 re'' |
    mib''4. mib''8 mib''4. re''8 |
    do''2\trill fa''4 \appoggiatura mib''16 re''4 |
    \appoggiatura do''16 sib'4. sol'8 mib''4. re''8 |
    do''4\trill sib' re''4. mi''8 |
    fa''4 mi'' fad''4. sol''8 |
    fad''4 re'' do''4. re''8 |
    sib'4.\trill la'8 la'4.\trill sol'8 |
    sol'2
  }
  %% Haute-contre
  \tag #'(vhaute-contre genie-hc) {
    <<
      \tag #'genie-hc { s2 s1*3 s2 }
      \tag #'vhaute-contre { \clef "vhaute-contre" r2 R1*3 r2 }
    >>
    re'4. re'8 |
    fa'4 sol' sol'4. sol'8 |
    sol'2 sol'4. la'8 |
    sib'4 sol' sol' fa' |
    fa' fa'
    <<
      \tag #'genie-hc { s2 s1*3 s2 }
      \tag #'vhaute-contre { r2 R1*3 r2 }
    >>
    fa'4 sol' |
    la' sol' la'4. sib'8 |
    la'2 sol'4. fa'8 |
    mi'4 re' mi'4. la'8 |
    la'4 fad'
    <<
      \tag #'genie-hc { s2 s1*3 s2 }
      \tag #'vhaute-contre { r2 R1*3 r2 }
    >>
    re'4. sol'8 |
    fa'4. sol'8 fad'4. re'8 |
    re'2 re'4. fad'8 |
    sol'4 sol' fad'4. sol'8 |
    fad'4 sol'
    <<
      \tag #'genie-hc { s2 s1*3 s2 }
      \tag #'vhaute-contre { r2 R1*3 r2 }
    >>
    sol'4. la'8 |
    sib'4 la' sol'4. la'8 |
    sib'4 la' sol'4. fa'8 |
    mi'4. re'8 dod'4. re'8 |
    re'2
    <<
      \tag #'genie-hc { s2 s1*7 s2 }
      \tag #'vhaute-contre { r2 R1*7 r2 }
    >>
    sol'4 sol' |
    sol'4. sol'8 fa'4. fa'8 |
    fa'2 fa'4 fa' |
    sol'4 mib' mib'4. fa'8 |
    fa'4 re' fa'4. sol'8 |
    sol'4 sol' la'4. sib'8 |
    la'4 fad' re'4. re'8 |
    re'4. mib'8 re'4. la8 |
    sib2
  }
  %% Taille
  \tag #'vtaille {
    \clef "vtaille"
    r2 R1*3 r2
    sib4. sib8 |
    do'4 re' do' do' |
    sib2 sib4. do'8 |
    re'4 sib sib la |
    sib sib
    r2 R1*3 r2
    re'4 re' |
    do' do' do'4.\trill re'8 |
    re'2 re'4. re'8 |
    mi'4 fa' dod'4. re'8 |
    dod'4 re'
    r2 R1*3 r2
    sib4. do'8 |
    re'4. re'8 do'4. sib8 |
    do'2 sib4. do'8 |
    re'4 sib do'4. re'8 |
    do'4\trill sib
    r2 R1*3 r2
    re'4. do'8 |
    sib4 re' dod'4. re'8 |
    dod'4 la la4. la8 |
    la4 la la4. la8 |
    fad2
    r2 R1*7 r2
    si4 si |
    do'4. do'8 do'4.\trill sib8 |
    la2 sib4 sib |
    sib sib la4. sib8 |
    la4 sib sib4. sib8 |
    do'4 do' do'4. re'8 |
    re'4 re' la4. fad8 |
    sol4. sol8 fad4. sol8 |
    sol2
  }
  %% Basse
  \tag #'vbasse {
    \clef "vbasse"
    r2 R1*3 r2
    sol4. sol8 |
    la4 si do' do |
    sol2 sol4. sol8 |
    re4 sol do fa |
    sib, sib,
    r2 R1*3 r2
    sib4 sib |
    la sib la4. sol8 |
    re'2 sib4. la8 |
    sol4 fa mi4. re8 |
    la4 re
    r2 R1*3 r2
    sol4. la8 |
    sib4. sib8 la4. sol8 |
    fad2 sib4. la8 |
    sol4 sib la4. sol8 |
    re4 sol,
    r2 R1*3 r2
    sib4. la8 |
    sol4 fa mi4. re8 |
    sol4 fa mi re |
    dod4. re8 la4. la8 |
    re2
    r2 R1*7 r2
    sol4 sol |
    do'4. do'8 la4. sib8 |
    fa2 re4 sib, |
    mib4 re do4. sib,8 |
    fa4 sib, sib4. sib8 |
    la4 sib la4. sol8 |
    re'4 re' fad4. re8 |
    sol4 do re4. re8 |
    sol,2
  }
>>
