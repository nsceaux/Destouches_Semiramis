\clef "taille"
r2 R1*3 r2
sib4. sib8 |
do'4 re' do' do' |
sib2 sib4. do'8 |
re'4 sib sib la |
sib sib
r2 R1*3 r2
re'4 re' |
do' do' do'4.\trill re'8 |
re'2 re'4. re'8 |
mi'4 fa' dod'4. re'8 |
dod'4 re'
r2 R1*3 r2
sib4. do'8 |
re'4. re'8 do'4. sib8 |
do'2 sib4. do'8 |
re'4 sib do'4. re'8 |
do'4\trill sib
r2 R1*3 r2
re'4. do'8 |
sib4 re' dod'4. re'8 |
dod'4 la la4. la8 |
la4 la la4. la8 |
fad2
r2 R1*7 r2
si4 si |
do'4. do'8 do'4.\trill sib8 |
la2 sib4 sib |
sib sib la4. sib8 |
la4 sib sib4. sib8 |
do'4 do' do'4. re'8 |
re'4 re' la4. fad8 |
sol4. sol8 fad4. sol8 |
sol2