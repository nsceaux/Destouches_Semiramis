\score {
  \new ChoirStaff <<
    \new StaffGroup <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'genie-hc \includeNotes "voix"
      >> \keepWithTag #'(genie choeur) \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s1*3 s2 \bar "" \break s2 s1*3 s2 \bar "" \break
        s2 s1*3 s2 \bar "" \pageBreak
        s2 s1*3 s2 \bar "" \break
        s2 s1*3 s2 \bar "" \pageBreak
        s2 s1*3 s2 \bar "" \break
        s2 s1*3 s2 \bar "" \pageBreak
        s2 s1*3 s2 \bar "" \break
        s2 s1*3 s2 \bar "" \break s2 s1*3 s2 \bar "" \pageBreak
        s2 s1*4\pageBreak
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
