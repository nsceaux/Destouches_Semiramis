\tag #'(genie basse) {
  Pa -- rois -- sez jeu -- nes Ze -- phirs,
  Ex -- ci -- tez, a -- ni -- mez Flo -- re.
}
\tag #'choeur {
  Pa -- rois -- sez jeu -- nes Ze -- phirs,
  Ex -- ci -- tez, a -- ni -- mez Flo -- re.
}
\tag #'(genie basse) {
  Que l’ar -- deur de vos sou -- pirs
  Hâ -- te ses pré -- sens d’é -- clo -- re.
}
\tag #'choeur {
  Que l’ar -- deur de vos sou -- pirs
  Hâ -- te ses pré -- sens d’é -- clo -- re.
}
\tag #'(genie basse) {
  Qu’on les doive à vos plai -- sirs,
  Plu -- tôt qu’aux pleurs de l’Au -- ro -- re.
}
\tag #'choeur {
  Qu’on les doive à vos plai -- sirs,
  Plu -- tôt qu’aux pleurs de l’Au -- ro -- re.
}
\tag #'(genie basse) {
  Que Ve -- nus sur ce ri -- va -- ge
  Fi -- xe sa bril -- lan -- te Cour,
}
\tag #'choeur {
  Que Ve -- nus sur ce ri -- va -- ge
  Fi -- xe sa bril -- lan -- te Cour,
}
\tag #'(genie basse) {
  Qu’on en -- ten -- de nuit & jour
  Des Oy -- seaux le doux ra -- ma -- ge,
  Des a -- mans le tendre hom -- ma -- ge,
  Et l’é -- lo -- ge de l’A -- mour.
}
\tag #'choeur {
  Qu’on en -- ten -- de nuit & jour
  Des Oy -- seaux le doux ra -- ma -- ge,
  Des a -- mans le tendre hom -- ma -- ge,
  Et l’é -- lo -- ge de l’A -- mour.
}
