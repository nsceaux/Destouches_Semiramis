\clef "haute-contre" r2 R1*3 r2
re'4. re'8 |
fa'4 sol' sol'4. sol'8 |
sol'2 sol'4. la'8 |
sib'4 sol' sol' fa' |
fa' fa'
r2 R1*3 r2
fa'4 sol' |
la' sol' la'4. sib'8 |
la'2 sol'4. fa'8 |
mi'4 re' mi'4. la'8 |
la'4 fad'
r2 R1*3 r2
re'4. sol'8 |
fa'4. sol'8 fad'4. re'8 |
re'2 re'4. fad'8 |
sol'4 sol' fad'4. sol'8 |
fad'4 sol'
r2 R1*3 r2
sol'4. la'8 |
sib'4 la' sol'4. la'8 |
sib'4 la' sol'4. fa'8 |
mi'4. re'8 dod'4. re'8 |
re'2
r2 R1*7 r2
sol'4 sol' |
sol'4. sol'8 fa'4. fa'8 |
fa'2 fa'4 fa' |
sol'4 mib' mib'4. fa'8 |
fa'4 re' fa'4. sol'8 |
sol'4 sol' la'4. sib'8 |
la'4 fad' re'4. re'8 |
re'4. mib'8 re'4. la8 |
sib2
