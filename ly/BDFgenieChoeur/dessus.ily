\clef "dessus" r2 R1*3 r2
sol''4. sol''8 |
fa''4 mib''16 re''8 mib''16 mib''4.\trill re''8 |
re''2 re''4. mib''8 |
fa''4 mib''8 re'' mib''4 do'' |
re'' sib'
r2 R1*3 r2
re''4 mi'' |
fa'' mi'' fad''4. sol''8 |
fad''2 sol''4. la''8 |
dod''4 re'' sol''4. fa''8 |
mi''4\trill re''
r2 R1*3 r2
sol''4. fa''8 |
mib''16 re''8. do''16 sib'8. do''4. re''8 |
la'2 re''4. do''8 |
sib'4 re'' do''4. sib'8 |
la'4\trill sol'
r2 R1*3 r2
sol''4. fa''8 |
mi''4\trill re'' sol''4. fa''8 |
mi''4 re'' dod''4. re''8 |
la'4. fa''8 mi''4.\trill re''8 |
re''2
r2 R1*7 r2
fa''4 re'' |
mib''4. mib''8 mib''4. re''8 |
do''2 fa''4 re'' |
sib'4. sol'8 mib''4. re''8 |
do''4 sib' re''4. mi''8 |
fa''4 mi'' fad''4. sol''8 |
fad''4 re'' do''4. re''8 |
sib'4.\trill la'8 la'4.\trill sol'8 |
sol'2
