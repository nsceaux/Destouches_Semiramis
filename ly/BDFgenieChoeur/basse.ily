\clef "basse"
<<
  \tag #'basse { r2 R1*3 r2 }
  \tag #'basse-continue {
    sol4. sol8 |
    la4 si do' do |
    sol sol, sol fa8 mib |
    re4 sol do fa |
    sib,2
  }
>>
sol4. sol8 |
la4 si do' do |
sol sol, sol4. sol8 |
re4 sol do fa |
sib, sib,
<<
  \tag #'basse { r2 R1*3 r2 }
  \tag #'basse-continue {
    sib4 sib |
    la sib la sol |
    re'2 sib4. la8 |
    sol4 fa mi re |
    la re
  }
>>
sib4 sib |
la sib la4 sol |
re'2 sib4. la8 |
sol4 fa mi4 re |
la4 re
<<
  \tag #'basse { r2 R1*3 r2 }
  \tag #'basse-continue {
    sol4 la |
    sib4. sib8 la4. sol8 |
    fad8 mi re do sib,4. la,8 |
    sol,4 sib, la,4. sol,8 |
    re4 sol,
  }
>>
sol4. la8 |
sib4. sib8 la4. sol8 |
fad2 sib4. la8 |
sol4 sib la4. sol8 |
re4 sol,
<<
  \tag #'basse { r2 R1*3 r2 }
  \tag #'basse-continue {
    sib4. la8 |
    sol4 fa mi4. re8 |
    sol4 fa mi re |
    dod re la la, |
    re2
  }
>>
sib4. la8 |
sol4 fa mi4. re8 |
sol4 fa mi re |
dod4. re8 la4 la, |
<<
  \tag #'basse { re2 r R1*7 r2 }
  \tag #'basse-continue {
    re4 re'8 do' si4 si |
    do'4 do'8 sib la4 sib |
    fa4 fa8 mib re4 sib, |
    mib re do4. sib,8 |
    fa4 sib, sib sib |
    la sib la sol |
    re' do' sib la |
    sol do re re, |
    sol,2
  }
>>
sol4 sol |
do'4 do' la4 sib |
fa2 re4 sib, |
mib4 re do4 sib, |
fa4 sib, sib4 sib |
la4 sib la4 sol |
re'4 re' fad4 re |
sol4 do re4. re8 |
sol,2
