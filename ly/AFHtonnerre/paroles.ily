\tag #'(semiramis arsane) {
  Quel tour -- bil -- lon de feux s’é -- leve & nous sé -- pa -- re ?
  Quelle hor -- reur ! quels mu -- gis -- se -- mens !
  Quelle hor -- reur ! quels mu -- gis -- se -- mens !
  La ter -- re trem -- ble,
  La ter -- re trem -- ble, s’ou -- vre & mon -- tre le Te -- na -- re,
  Le Ciel con -- fond les E -- le -- mens.
  Le Ciel con -- fond les E -- le -- mens.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Quels dé -- lu -- ges brû -- lans tom -- bent de tou -- tes parts ?
  Quels dé -- lu -- ges brû -- lans tom -- bent de tou -- tes parts ?
  Tu fuis So -- leil, tu fuis ! quel est le cri -- me
  Qui te dé -- robe à nos re -- gards ?
  Tu fuis So -- leil, tu fuis ! quel est le cri -- me
  Qui te dé -- robe à nos re -- gards ?
  Ciel, Ciel, qui vou -- lez- vous pour vic -- ti -- me ?
  Ciel, Ciel, qui vou -- lez- vous pour vic -- ti -- me ?
  Ciel, Ciel, qui vou -- lez- vous pour vic -- ti -- me ?
}
\tag #'semiramis {
  Tout l’O -- lympe en cou -- roux s’ar -- me- t’il con -- tre moi ?
  Dieux, Dieux, me pu -- nis -- sez- vous d’a -- voir tra -- hi ma foi ?
}
