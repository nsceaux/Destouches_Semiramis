\clef "basse" sol,2 do4 |
fa sib,2 |
mib4 sib, do |
re mib8 do re re, |
sol,16 sol, sol, sol, sol, sol, sol, sol, sol, sol, sol, sol, |
re^\douxSug re re re re re re re re re re re |
si, si, si, si, si, si, si, si, si, si, si, si, |
do^\fortSug do do do do do do do do do do do |
do do do do do do do do do do do do |
sol, sol, sol, sol, sol, sol, sol, sol, sol, sol, sol, sol, |
sol, sol, sol, sol, sol, sol, sol, sol, sol, sol, sol, sol, |
do do do do do do do do do do do do |
do do do do do do do do do do do do |
do do do do do do do do do do do do |
do do do do do do do do do do do do |
fa, fa, fa, fa, fa, fa, fa, fa, fa, fa, fa, fa, |
sib, sib, sib, sib, sib, sib, sib, sib, sib, sib, sib, sib, |
mib4~ mib8. fa16 sol fa mib re |
do do do do do do do do do do do do |
fa4~ fa8. sol16 la sol fa mib |
re16 re re re re re re re mib mib mib mib |
fa fa fa fa fa fa fa fa fa fa fa fa |
sib,8 sib,16 do re8 re16 do sib,8 sib,16 la, |
sol,4 do16 do do do do do do do |
mib mib mib mib mib mib mib mib mib mib mib mib |
re re re re re re re re re re re re |
sib, sib, sib, sib, do do do do do do do do |
re re re re re re re re re re re re |
sol, sol, sol, sol, sol, sol, sol, sol, sol, sol, sol, sol, |
sol,2 do4 |
fa sib,2 |
mib4 sib, do |
re mib8 do re re, |
sol,4 sol( sol) |
sib( sib sib) |
sib( sib sib) |
mib mib8 mib mib mib |
sib,4( sib, sib,) |
sib,( sib, sib,) |
fa( fa fa) |
fa( fa fa) |
do do8 do do do |
sol,4( sol, sol,) |
sol,( sol, sol,) |
sol,( sol, sol,) |
sol,( sol, sol,) |
do do do |
do( do do) |
do( do do) |
fa fa fa |
do4. do8 do do |
fa,4( fa, fa,) |
fa( fa fa) |
sib( sib sib) |
mi( mi mi) |
la( la la) |
la( la la) |
la( la la) |
fa fa re |
la4. la8 la la |
re4( re re) |
sol( sol sol) |
la( la la) |
sib( sib sib) |
do'( do' do') |
re'( re' re') |
sib( sib sib) |
mib'( mib' mib') |
do'( do' do') |
re' re' re |
sol( sol sol) |
sib( sib sib) |
mib'( mib' mib') |
do'( do' do') |
re' re' re |
sol4. sol8 mib do |
fa4. re8 sib, sol, |
mib4. do8 la, la, |
re4. sol,8 si, sol, |
do4. la,8 dod la, |
re4. re8 re re |
sol,4. mib8 mib mib |
re do sib, do re re, |
sol,16 sol, sol, sol, sol, sol, sol, sol, sol, sol, sol, sol, |
sol\douxSug sol sol sol sol sol sol sol sol sol sol sol |
re re re re re re re re re re re re |
re re re re re re re re re re re re |
do do do do do do do do do do do do |
sib, sib, sib, sib, sib, sib, sib, sib, sib, sib, sib, sib, |
do do do do do do do do do do do do |
re re re re re re re re re re re re |
re, re, re, re, re, re, re, re, re, re, re, re, |
sol,4. sol,8^\fortSug si, sol, |
do4. la,8 dod la, |
re4. re8 re re |
sol,4. mib8 mib mib |
re do sib, do re re, |
sol,2. |
