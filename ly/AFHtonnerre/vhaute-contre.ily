\clef "vhaute-contre" R2.*33 |
r4 sol' sol' |
fa' fa' sol' |
fa'2. |
mib'4 mib'8 mib' mib' mib' |
fa'2. |
r4 fa' fa' |
fa'4 fa' sib' |
la'2. |
sol'4 sol'8 sol' sol' sol' |
sol'2. |
sol'4 sol' sol' |
sol'2 sol'4 |
sol'2. |
sol'4 sol' lab' |
sib'2.~ |
sib'2 sib'4 |
lab'4 lab' lab' |
sol'4. sol'8 sol' sol' |
la'!2. |
fa'4 fa' fa' |
fa'2 fa'4 |
sib'2. |
mi'4 mi' fa' |
sol'2.~ |
sol'2 sol'4 |
fa'4 fa' fa' |
mi'4. mi'8 mi' mi' |
fad'2. |
sol'2 r4 |
fad'2 r4 |
sol'4 sol' sib' |
sol' sol' do'' |
la'2 la'4 |
sib'2 r4 |
sib'2 r4 |
sib'4 sib' la' |
la' la' la' |
sib'2 sib'4 |
sib'2 r4 |
sib'2 r4 |
sib' sib' la' |
la' la' la' |
sib'2 sib'4 |
R2.*22 |
