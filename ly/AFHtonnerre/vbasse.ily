\clef "vbasse" R2.*33 |
r4 sol sol |
sib sib sib |
sib2. |
mib4 mib8 mib mib mib |
sib,2. |
r4 sib, sib, |
fa fa fa |
fa2. |
do4 do8 do do do |
sol,2. |
sol4 sol sol |
si2 si4 |
si2. |
do'4 do' do' |
do'2.~ |
do'2 do'4 |
fa4 fa fa |
do4. do8 do do |
fa2. |
fa4 fa fa |
sib2 sib4 |
mi2. |
la4 la la |
la2.~ |
la2 la4 |
fa fa re |
la4. la8 la la |
re2. |
sol2 r4 |
la2 r4 |
sib4 sib sib |
do' do' do' |
re'2 re'4 |
sib2 r4 |
mib'2 r4 |
do'4 do' do' |
re' re' re |
sol2 sol4 |
sib2 r4 |
mib'2 r4 |
do' do' do' |
re' re' re |
sol2 sol4 |
R2.*22 |
