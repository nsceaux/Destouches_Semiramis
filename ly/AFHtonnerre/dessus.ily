\clef "dessus" <>_"Violons" sol''16 fad'' sol'' la'' sib'' la'' sol'' fa''? mib'' re'' do'' sib' |
la'4 re''16 do'' re'' mib'' re'' do'' sib' la' |
sol' la' sib' do'' re'' mi'' fa'' sol'' mi'' fad''? sol'' la'' |
fad'' sol'' la'' sib'' sol'' fad'' sol'' la'' la''8.\trill sol''16 |
sol''4 r r |
r8 fad''16\doux sol'' la''8 la'' la'' la'' |
sol''4 r r |
r8 mib''16\fort fa'' sol''8 sol'' sol''16.*2/3 fa''32 mib'' re'' do'' sib' la' |
sol'2 r4 |
fa'8( fa') fa'( fa') fa'( fa') |
mib'( mib') re'( re') re'( re') |
do'2 r4 |
sib8( sib) sib( sib) sib( sib) |
lab( lab) lab( lab) lab( lab) |
do'8 mi'16 fa' sol'8( sol') sol'( sol') |
fa' la'16 sib' do''8( do'') do''( do'') |
sib' re'16 mib' fa'8( fa') fa'( fa') |
sol' mib'16 fa' sol'8( sol') sol'( sol') |
sol'( sol') sol'( sol') do''( do'') |
do'' fa'16 sol' la'8 la'16 sib' do'' sib' la' sol' |
fa'8 re''16 mib'' fa''8( fa'') mib''( mib'') |
mib'' mib''16 mib'' mib''8 mib'' do'' fa'' |
fa'' re''16 mib'' fa''8 fa''16 mib'' re''8 re''16 do'' |
sib'8 sib'16 la' sol'8 mib''16 fa'' sol''8 sol'' |
sol'' mib''16 fa'' sol''8 sol'' sol'' sol'' |
la'' fad''16 sol'' la''8 fad''16 sol'' la'' sol'' fad'' mi'' |
re''8 sib'' sib'' la'' la'' la'' |
la''16 sol'' fad'' mi'' re''8 re''16 mib'' re'' do'' sib' la' |
sol'2. |
%%
sol''16 fad'' sol'' la'' sib'' la'' sol'' fa''? mib'' re'' do'' sib' |
la'4 re''16 do'' re'' mib'' re'' do'' sib' la' |
sol' la' sib' do'' re'' mi'' fa'' sol'' mi'' fad'' sol'' la'' |
fad'' sol'' la'' sib'' sol'' fad'' sol'' la'' la''8.\trill sol''16 |
sol''4 sib' sib' |
re'' re'' mib'' |
re''8 re''16 mib'' fa''8 fa'' fa'' fa'' |
sib'' mib''16 fa'' sol''8 sol'' sol'' sol'' |
fa'' re''16 mib'' fa''8 fa'' fa''16 mib'' re'' do'' |
sib'4 re'' re'' |
do'' do'' re'' |
do''8 fa'16 sol' la'8 la'16 si' do''8 do''16 re'' |
mib''8 mib''16 fa'' sol''8 sol'' sol'' la'' |
sib'' sib'16 do'' re''8 re''16 mib'' fa'' mib'' re'' do'' |
si'8 si'16 do'' re''8 re'' re'' re'' |
sol''8 si'16 do'' re''8 re'' re''16 do'' si' la' |
sol'8 re''16 mi'' fa''8 fa'' fa'' fa'' |
mi''4 mi'' fa'' |
sol''8 sol''16 lab'' sib''8 sib'' sib''16 lab'' sol'' fa'' |
mi''8 mi''16 fa'' sol''8 sol'' sol'' sol'' |
lab''8 fa''16 sol'' lab''8 lab'' lab'' fa'' |
sol'' sol''16 la'' sib''8 sib'' sib'' sib'' |
la''! la''16 sib'' do'''8 do''' do'''8.*1/3 sib''32 la'' sol'' fa'' mi'' re'' |
do''4. fa'16 sol' la' sib' do'' la' |
re''4. sib'16 do'' re'' mib'' fa'' re'' |
sol''8 la''16 sib'' sib''8 sib''16 la'' sol'' fa'' mi'' re'' |
dod''4 dod'' re'' |
mi''8 mi''16 fa'' sol''8 sol''16 fa'' mi'' re'' dod'' si' |
la'8 la'16 si' dod''8 dod'' dod'' la' |
re'' fa''16 sol'' la''8 re'' fa'' re'' |
mi''8 mi''16 fa'' sol''8 sol'' sol'' la'' |
fad'' fad''16 sol'' la''8 la''16 sol'' fad'' mi'' re'' do'' |
sib'8 sib'16 do'' re''8 re'' re'' re'' |
do'' do''16 re'' mib''8 mib'' mib'' mib'' |
re'' re''16 mi'' fa''8 fa'' fa'' fa'' |
mi'' mi''16 fa'' sol''8 sol'' sol'' sol'' |
fad'' fad''16 sol'' la''8 la'' la''16 sol'' fad'' mi'' |
re''8 sol''16 la'' sib''8 sib'' sib'' sib'' |
sol'' mib''16 fa'' sol''8 sol'' sol'' sol'' |
do'''16 do''' do''' do''' do''' do''' do''' do''' do''' do''' do''' do''' |
do'''4. re''16 mi'' fad'' sol'' la'' fad'' |
sib''8 sol''16 sol'' sol''8 re'' sib' re'' |
sol' sol''16 la'' sib''8 sib'' sib'' sib'' |
sol'' mib''16 fa'' sol''8 sol'' sol'' sol'' |
do'''16 do''' do''' do''' do''' do''' do''' do''' do''' do''' do''' do''' |
do'''4. re''16 mi'' fad'' sol'' la'' fad'' |
sib''4. sib''16 la'' sol'' fa'' mib'' re'' |
do''4. fa''16 mib'' re'' do'' sib' la' |
sol'4. mib''16 re'' do'' sib' la' sol' |
fad'8 re''16 mi'' fa''8 fa'' fa'' fa'' |
mi''8 mi''16 fa'' sol''8 sol'' sol'' sol'' |
fad''8 fad''16 sol'' la''8 la'' la'' fad'' |
sib''4. la''16 sib'' do''' sib'' la'' sol'' |
fad''8 re'' sol''8. la''16 la''8.\trill sol''16 |
sol''2 r4 |
sib'2\doux r4 |
sib'2 fa'4 |
fa'2 r4 |
la'2 re''4 |
sol'2 sib'4 |
sib'2 la'4 |
la'2 r8 sib' |
la' sol' fad'4. sol'8 |
sol'8 re''16\fort mi'' fa''8 fa'' fa'' fa'' |
mi''8 mi''16 fad'' sol''8 sol'' sol'' sol'' |
fad''8 fad''16 sol'' la''8 la'' la'' fad'' |
sib''4. la''16 sib'' do''' sib'' la'' sol'' |
fad''8 re'' sol''8. la''16 la''8.\trill sol''16 |
sol''2. |
