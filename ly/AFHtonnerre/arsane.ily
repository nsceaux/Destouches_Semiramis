\clef "vhaute-contre" R2.*4 |
<>^\markup\character Arsane sib4 sib8 do' re' sol' |
fad'2 re'4 |
fa'4. fa'8 fa' sol' |
mib'2 mib'4 |
r4 r mib'8 fa' |
re'2 r4 |
R2. |
mi'4. mi'8 mi' fa' |
\appoggiatura fa'8 mi'2. |
r4 r do'8 do' |
sol4 mi'8 mi' mi' fa' |
fa'4. fa'8 fa' fa' |
re'4~ re'8.[\melisma mib'16] fa'[ mib' re' do']( |
sib4)\melismaEnd sib8 sol' sol' sol' |
mib'4~ mib'8.[\melisma fa'16] sol'[ fa' mib' re']( |
do'2)\melismaEnd do'4 |
fa'4 fa' r8 sol' |
do'4. do'8 do' do' |
re'2 re'4 |
sol' sol' mib' |
do'4. do'8 re' mib' |
la2. |
sol'4 sol' mib' |
la4. re'8 re' re' |
re'2. |
R2.*69 |
