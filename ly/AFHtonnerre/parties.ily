<<
  \tag #'haute-contre {
    \clef "haute-contre"
    sib'4 sib' sol' |
    fa' sib' fa' |
    mib' sib' do'' |
    la'8 fad' sol'4 fad'8. sol'16 |
    sol'4
  }
  \tag #'taille {
    \clef "taille"
    re'4 re' mib' |
    do' sib sib |
    sib sib la |
    la8 re' sib mib' re'8. la16 |
    sib4
  }
>> r4 r |
r8 la'16\douxSug la' re''8 re'' re'' re'' |
re''4 r r |
r8 do''16\fortSug do'' do''8 do'' do'' sol' |
mib'?2 r4 |
re'8( re') re'( re') re'( re') |
do'( do') si( si) si( si) |
do'2 r4 |
sol8( sol) sol( sol) sol( sol) |
fa( fa) fa( fa) do'( do') |
sib( sib) sib( sib) sol( do') |
do'( do') do'( do') do'( do') |
re'( re') re'( re') re'( re') |
mib' mib'16 mib' mib'8( mib') mib'( mib') |
mib'( mib') mib'( mib') mib' sol' |
fa' fa'16 fa' fa'8( fa') fa'( fa') |
fa'( fa') fa'( fa') sib' sol' |
fa'( fa') fa'( fa') fa'( fa') |
fa' sib'16 sib' sib'8 sib' fa' fa' |
sol' sol'16 sol' sol'8 do'' do'' do'' |
do''( do'') do''( do'') do''( sol') |
re' la'16 la' la'8( la') la'( la') |
sib'( sib') sol'( sol') sol'( mib') |
re'( re') re'( re') re'( re') |
re'2. |
<<
  \tag #'haute-contre {
    sib'4 sib'4. sol'8 |
    fa'4 fa' fa' |
    mib' sib' do'' |
    la'8 fad' sol'4 fad'8. sol'16 |
    sol'4 sol' sol' |
    fa' fa' sol' |
    fa'( fa' fa') |
    mib' mib'8 mib' mib' mib' |
    fa'4( fa' fa') |
    fa'( fa' fa') |
    fa' fa' sib' |
    la'( la' la') |
    sol' sol'8 sol' sol' sol' |
    sol'4( sol' sol') |
    sol'( sol' sol') |
    sol'( sol' sol') |
    sol'( sol' sol') |
    sol' sol' lab' |
    sib'( sib' sib') |
    sib'( sib' sib') |
    lab'( lab' lab') |
    sol'4. sol'8 sol' sol' |
    la'!4( la' la') |
    fa'( fa' fa') |
    fa'( fa' fa') |
    sib'( sib' sib') |
    mi' mi' fa' |
    sol'( sol' sol') |
    sol'( sol' sol') |
    fa'( fa' fa') |
    mi'4. mi'8 mi' mi' |
    fad'4( fad' fad') |
    sol'( sol' sol') |
    fad'( fad' fad') |
    sol' sol' sib' |
    sol' sol' do'' |
    la'( la' la') |
    sib'( sib' sib') |
    sib'( sib' sib') |
    sib' sib' la' |
    la' la' la' |
    sib'( sib' sib') |
    sib'( sib' sib') |
    sib'( sib' sib') |
    sib' sib' la' |
    la' la' la' |
    sol' sib' sib' |
    la'4. fa'8 sib'4 |
    mib'4. mib'8 mib' mib' |
    re'4. si'8 re'' si' |
    do''4. dod''8 mi'' dod'' |
    re''4. la'8 la' la' |
    sib'4. do''8 do'' do'' |
    re''4 re''8 mib'' re''8. la'16 |
    sib'2 r4 |
    sol'2\douxSug r4 |
    fa'2 fa'4 |
    re'2 r4 |
    re'2 r4 |
    re'2 sol'4 |
    sol'2. |
    re'2 r4 |
    re'4 re'4. re'8 |
    re'4. si'8\fortSug re'' si' |
    do''4. dod''8 mi'' dod'' |
    re''4. re''8 re'' re'' |
    re''4. do''8 do'' do'' |
    la'4 sib'8. mib''16 re''8. la'16 |
    sib'2. |
  }
  \tag #'taille {
    re'4 re' mib' |
    do' sib sib |
    sib sib la |
    la8 re' sib mib' re'8. la16 |
    sib4 re' re' |
    re' re' do' |
    sib( sib sib) |
    sib sib8 sib sib do' |
    re'4( re' re') |
    re' sib sib |
    la la sib |
    do'( do' do') |
    do' do'8 do' do' do' |
    sib4( sib sib) |
    si( si si) |
    re'( re' re') |
    re'( re' re') |
    do' do' fa' |
    mi'( mi' mi') |
    mi'( mi' mi') |
    fa' do' do' |
    do'4. do'8 do' do' |
    do'4( do' do') |
    la la la |
    sib( sib sib) |
    sib( sib sib) |
    la la re' |
    dod'( dod' dod') |
    dod'( dod' dod') |
    la la la |
    la4. la8 la la |
    la4( la la) |
    sib( sib sib) |
    do'( do' do') |
    sib sib re' |
    do' do' mi' |
    re'( re' re') |
    sol'( sol' sol') |
    mib'( mib' mib') |
    mib'( mib' mib') |
    re' re' re' |
    re'( re' re') |
    sol'( sol' sol') |
    mib'( mib' mib') |
    mib'( mib' mib') |
    re' re' re' |
    re'4. re'8 mib' mib' |
    la4. sib8 sib sib |
    sib4. do'8 do' do' |
    la4. re'8 re' re' |
    sol4. la8 la la |
    la4. la8 re' re' |
    re'4. sol'8 sol' la' |
    la'4 sib'8 la'16 sol' fad'8. sol'16 |
    sol'2 r4 |
    sol2\douxSug r4 |
    sib2 r4 |
    sib2 r4 |
    la2 r4 |
    sib2. |
    sol2 do'4 |
    la2 r4 |
    la4 re'4. la8 |
    sib4. re'8\fortSug sol' sol' |
    sol'4. la'8 la' la' |
    la'4. fad'8 fad' la' |
    sol'4. sol'8 sol' sol' |
    re'4 re'8. sol'16 fad'8. sol'16 |
    sol'2. |
  }
>>
