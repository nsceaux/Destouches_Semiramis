\key re \minor \midiTempo#120 \tempo "Tres vîte"
\beginMark "Prélude" \digitTime\time 3/4 s2.*29 \bar "||"
\beginMark "Prélude" \tempo "Tres vite" s2.*4
\beginMark "Chœur" s2.*50 \bar "||"
\tempo "Vite" s2.*15 \bar "|."
