\clef "vdessus" R2.*33 |
r4 sib' sib' |
re'' re'' mib'' |
re''2. |
sol'4 sol'8 sol' sol' la' |
sib'2. |
r4 re'' re'' |
do'' do'' re'' |
mib''2. |
mib''4 mib''8 mib'' mib'' mib'' |
re''2. |
sol''4 sol'' fa'' |
fa''2 fa''4 |
fa''2. |
mi''4 mi'' fa'' |
sol''2.~ |
sol''2 sol''4 |
fa''4 fa'' fa'' |
fa''4. fa''8 fa'' mi'' |
fa''2. |
mib''4 mib'' mib'' |
re''2 re''4 |
sol''2. |
dod''4 dod'' re'' |
mi''2.~ |
mi''2 mi''4 |
re''4 re'' re'' |
re''4. re''8 re'' dod'' |
re''2. |
re''2 r4 |
mib''2 r4 |
re''4 re'' re'' |
mi'' mi'' mi'' |
fad''2 fad''4 |
re''2 r4 |
sol''2 r4 |
sol'' sol'' sol'' |
sol'' sol'' fad'' |
sol''2 sol''4 |
re''2 r4 |
sol''2 r4 |
sol'' sol'' sol'' |
sol'' sol'' fad'' |
sol''2 sol''4 |
R2.*22 |
