\piecePartSpecs
#`((dessus)
   (dessus-hc #:score "score-dessus-hc")
   (haute-contre #:notes "parties" #:tag-notes haute-contre)
   (taille #:notes "parties" #:tag-notes taille)
   (basse)
   (basse-continue)
   (chant)
   (silence #:on-the-fly-markup , #{ \markup\tacet#98 #}))
