\clef "vtaille" R2.*33 |
r4 re' re' |
\footnoteHere #'(0 . 0) \markup\wordwrap {
  Manuscrit : \score {
    { \tinyQuote \time 3/4 \clef "tenor" \key re \minor
      r4 re' re' | re' re' do' | sib2. }
    \layout { \quoteLayout }
  }
}
sib4 sib mib' |
sib2. |
sib4 sib8 sib sib do' |
re'2. r4 sib sib |
la la sib |
do'2. |
do'4 do'8 do' do' do' |
sib2. si4 si si |
re'2 re'4 |
re'2. |
do'4 do' fa' |
mi'2.~ |
mi'2 mi'4 |
fa'4 do' do' |
do'4. do'8 do' do' |
do'2. |
la4 la la |
sib2 sib4 |
sib2. |
la4 la re' |
dod'2.~ |
dod'2 dod'?4 |
la4 la la |
la4. la8 la la |
la2. |
sib2 r4 |
do'2 r4 |
sib4 sib re' |
do' do' mi' |
re'2 re'4 |
sol'2 r4 |
mib'2 r4 |
mib'4 mib' mib' |
re' re' re' |
re'2 re'4 |
sol'2 r4 |
mib'2 r4 |
mib' mib' mib' |
re' re' re' |
re'2 re'4 |
R2.*22 |
