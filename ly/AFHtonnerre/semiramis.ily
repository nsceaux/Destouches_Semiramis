\clef "vdessus" R2.*4 |
<>^\markup\character Semiramis sol'4 sol'8 la' sib' do'' |
la'2 la'4 |
re''4. re''8 re'' re'' |
sol'2 sol'4 |
r4 r sol'8 sol' |
si'2 r4 |
R2. |
sol'4. sol'8 sol' lab' |
\appoggiatura lab'8 sol'2. |
r4 r lab'8 lab' |
mi'4 do''8 do'' do'' sol' |
la'4.\trill la'8 la' la' |
sib'4~ sib'8.[\melisma do''16] re''[ do'' sib' la']( |
sol'4)\melismaEnd sol'8 sib' sib' sib' |
do''4~ do''8.[\melisma re''16] mib''[ re'' do'' sib']( |
la'2)\melismaEnd la'4 |
re''4 re'' r8 sib' |
sib'4.\trill sib'8 sib' la' |
sib'2 sib'4 |
sib' mib'' do'' |
la'4. la'8 sib' do'' |
fad'2. |
sib'4 mib'' do'' |
fad'4. fad'8 fad' fad' |
sib'2. |
R2.*40 \clef "vbas-dessus" R2.*14 |
<>^\markup\character Semiramis
r4 r sib'8 do'' |
re''2 re''8 mib'' |
fa''2 re''8 fa'' |
sib'2 sib'8 re'' |
fad'2. |
re'' |
mi''4 mi''8 mi'' fad'' sol'' |
fad''2 r8 re'' |
do'' sib' la'4.\trill sib'8 |
sol'2 r4 |
R2.*5 |
