\score {
  \new StaffGroup <<
    \new Staff \with { instrumentName = "d." } <<
      \global \includeNotes "dessus"
    >>
    \new Staff \with { instrumentName = "h-c." } <<
      \global \keepWithTag #'haute-contre \includeNotes "parties"
      \clef "treble"
    >>
  >>
  \layout { }
}
