\clef "haute-contre" r4 r sib' |
do''2 r4 |
sib'2 r4 |
do''4. sib'8 la'4 |
sib'2 r4 |
do''2 r4 |
sib'2. |
mib''8 re'' do''4.\trill sib'8 |
sib'4 re'' do''8\trill sib' |
do''4 fa' do'' |
sib' fa' sib' |
la' la'4.\trill( sol'16 la') |
sib'4 re'' do''8\trill sib' |
do''4 fa' do'' |
sib' fa' sib' |
la' la'4.(\trill sol'16 la') |
sib'4 r r |
R2.*7 |
r4 re'' re'' |
re'' do'' do'' |
do'' sib' sib' |
sib' la'4.\trill sib'8 |
sib'4 re'' re'' |
re'' do'' do'' |
do'' sib' sib' |
sib' la'4.\trill sib'8 |
sib'4 mib'' sib' |
do'' do'' fa' |
fa' mib' r |
fa' fa'4. fa'8 |
fa'4 mib'' sib' |
do'' do'' fa' |
fa' mib' r |
fa' fa'4. fa'8 |
fa'4 r r |
R2.*3 |
r4 re'4. re'8 |
fa'4 fa'4. sib'8 |
sib'2 sib'4 |
do''8 sib' la'4.\trill sib'8 |
sib'4 %{%} sib'2\douxSug |
do''4 do''2 |
sib'4 sib' sib' |
sib' sib' la' |
sib' sib'2 |
do''4 do''2 |
sib'4 sib' sib' |
sib' sib' la' |
sib'2 fa'4 |
fa'2 mib'4 |
reb'2 solb'4 |
fa' sib'2 |
la'2 fa'4 |
fa'2 mib'4 |
reb'2 solb'4 |
fa' sib'2 |
la'4 sib' sib' |
sib' do'' reb'' |
do''2 sib'4 |
sib'2 sib'4 |
fa' sib' sib' |
sib' do'' reb'' |
do''2 sib'4 |
sib' la'4. sib'8 |
sib'4 reb'' do'' |
sib' do'' sib' |
la'2 sib'4 |
do'' sib' la' |
sib' reb'' do'' |
sib' do'' sib' |
la'2 sib'4 |
do'' sib' la' |
sib' reb'' do'' |
reb'' do'' sib' |
la'2 sib'4 |
sib' la'4. sib'8 |
sib'4 reb'' do'' |
reb'' do'' sib' |
la'2 sib'4 |
sib' la'4. sib'8 |
sib'4 %{%} sib'2\fortSug |
r4 do'' sib' |
la' sib' fa' |
fa' sol' fa' |
fa' re''2 |
r4 do'' sib' |
la' sib' fa' |
fa' sol' fa' |
fa' r r |
mib'' sib' la' |
sib' fad' sol' |
mib' re' re' |
re' r r |
mib'' sib' la' |
sib' fad' sol' |
mib' re' re' |
re' r r |
R2.*11 | \allowPageTurn
r4 r8 sib' sib' sib' |
re''4. la'8 la' fa' |
sol'4. sol'8 sol' sol' |
la'4. la'8 la' la' |
sib'4. sib'8 sib' sib' |
re''4. la'8 la' fa' |
sol'4. sol'8 sol' sol' |
la'4. la'8 la' la' |
sib'4. re'8 sol'4 |
r4 r8 fa' la'4 |
r r8 sol' sol'4 |
r4 r8 fad' fad' fad' |
sol'4. re'8 sol'4 |
r4 r8 fa' la'4 |
r4 r8 sol' sol'4 |
r4 r8 fad' fad' fad' |
sol'4 r r8 sol' |
fa' la' sib' sib' fa' sol'16 fa' |
mib'8 sol' do'' do' do' do' |
re' la' sib' sib'16 la' sib'8 fad' |
sol'4 r r8 sol' |
fa' la' sib' sib' fa' sol'16 fa' |
mib'8 sol' do'' do' do' do' |
re' la' sib' sib'16 la' sib'8 fad' |
sol'4 sol'4. sol'8 |
do''4 do''4. la'8 |
la'4.\trill( sol'16 la') sib'4~ |
sib'8 la'16 sol' fad'4. sol'8 |
sol'4 sol'4. sol'8 |
do''4 do''4. la'8 |
la'4.(\trill sol'16 la') sib'4~ |
sib'8 la'16 sol' fad'4. sol'8 |
sol'4. si'8 re''4 |
r4 r8 dod'' mi''4 |
re'' do'' mib'' |
la' re'' re''8. la'16 |
sib'4. si'8 re''4 |
r4 r8 dod'' mi''4 |
re''4 do'' mib'' |
la' re'' re''8. la'16 |
sib'4 r r |
r sib' sib' |
sib' re'' re'' |
do'' do'' la' |
sib' r r |
r sib' sib' |
sib' re'' re'' |
do'' do'' la' |
sib' r r |
R2.*7 |
re'4. mib'8 re' mib' |
do'2 r4 |
re' sib r |
do' do' do' |
re'4. mib'8 re' mib' |
do'2 r4 |
re' sib r |
do' do' do' |
fa'2 r4 |
fa'2 r4 |
fa'2 mib'4 |
mib'2\trill re'4 |
fa'2 r4 |
fa'2 r4 |
fa'2 mib'4 |
sib' la'4.\trill sib'8 |
sib'4
