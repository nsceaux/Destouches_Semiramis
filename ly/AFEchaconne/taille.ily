\clef "taille" r4 r fa' |
fa'2 r4 |
fa'2 mi'4 |
fa' do' fa' |
fa'2 r4 |
fa'2 r4 |
fa'2 mib'4 |
sol'4 fa'4. mib'8 |
re'2 fa'4 |
fa'2 do'4 |
re'2 sib4 |
do' do'4.\trill sib8 |
sib2 fa'4 |
fa'2 do'4 |
re'2 sib4 |
do' do'4.\trill sib8 |
sib4 r r |
R2.*7 |
r4 fa' fa' |
mib' mib' fa' |
fa' mib' mib' |
mib'8 re' fa'4. mib'8 |
re'4 fa' fa' |
mib' mib' fa' |
fa' mib' mib' |
mib'8 re' fa'4. mib'8 |
re'4 sib' sol' |
sol' fa' fa' |
re' sib r |
do'8 re' mib' re' do' mib' |
re'4 sib' sol' |
sol' fa' fa' |
re' sib r |
do'8 re' mib' re' do' mib' |
re'4 r r |
R2.*3 |
r4 sib4. sib8 |
do'4 fa'2 |
sol'2 sol'4 |
sol'4 fa'4. mib'8 |
re'4 %{%} fa'2\douxSug |
fa'4 mib'2 |
reb'4 mib' fa' |
solb' fa' fa' |
fa' fa'2 |
fa'4 mib'2 |
reb'4 mib' fa' |
solb' fa' fa' |
fa'2 do'4 |
sib2 fa'4 |
fa'2 mib'4 |
reb' sib2 |
do'2 do'4 |
sib2 fa'4 |
fa'2 mib'4 |
reb' sib2 |
do'4 reb' fa' |
mib' mib' reb' |
fa'2 fa'4 |
solb' fa' mib' |
do' reb' fa' |
mib' mib' reb' |
fa'2 fa'4 |
solb' fa' mib' |
reb' reb'2 |
reb'4 do' fa' |
fa'2 fa'4 |
solb' fa' fa' |
fa' reb'2 |
reb'4 do' fa' |
fa'2 fa'4 |
solb' fa' fa' |
fa' sib do' |
sib do' do' |
do'2 reb'4 |
solb' fa'4. do'8 |
reb'4 sib do' |
sib do' do' |
do'2 reb'4 |
solb' fa'4. do'8 |
re'!4 %{%} fa'2\fortSug~ |
fa'4 fa' fa' |
fa'2 mib'4 |
re' fa' mib' |
re' sib'2~ |
sib'4 fa' fa' |
fa'2 mib'4 |
re' fa' mib' |
re' r r |
sib sib do' |
sol la sib |
do' sib la |
sib r r |
sib sib do' |
sol la sib |
do' sib la |
sib r r |
R2.*11 | \allowPageTurn
r4 r8 sol' sol' sol' |
la'4. la'8 re' re' |
re'4. re'8 re' do' |
do'4. do'8 sib la |
sol4. sol'8 sol' sol' |
la'4. la'8 re' re' |
re'4. re'8 re' do' |
do'4. do'8 sib la |
sol4. sib8 sib4 |
r4 r8 do' fa'4 |
r4 r8 sib mib'4 |
r4 r8 re' re' re' |
re'4. sib8 sib4 |
r4 r8 do' fa'4 |
r4 r8 sib mib'4 |
r4 r8 re' re' re' |
re'4 r r8 mib' |
do' do' re' re' re' re' |
sib mib' mib' mib' mib' do' |
la re' re' mib' re' re' |
re'4 r r8 mib' |
do' do' re' re' re' re' |
sib mib' mib' mib' mib' do' |
la re' re' mib' re' re' |
re'4 re'4. re'8 |
do'4 sol'4. re'8 |
re'4 re'4. re'8 |
mib'4 re'4. la8 |
sib4 re'4. re'8 |
do'4 sol'4. re'8 |
re'4 re'4. re'8 |
mib'4 re'4. la8 |
sib4. re'8 re'4 |
r4 r8 mi'8 mi'4 |
fad' sol' sol' |
re' re'8. sol'16 fad'8. sol'16 |
sol'4. re'8 re'4 |
r r8 mi' mi'4 |
fad' sol' sol' |
re' re'8. sol'16 fad'8. sol'16 |
sol'4 r r |
r sol' sol' |
fa' fa' fa' |
fa' fa' fa' |
re' r r |
r sol' sol' |
fa' fa' fa' |
fa' fa' fa' |
re' r r |
R2.*7 |
sib2 r4 |
do'2 r4 |
sib2 r4 |
la la la |
sib2 r4 |
do'2 r4 |
sib2 r4 |
la la la |
sib2 r4 |
do'2 r4 |
sib2 r4 |
la2 sib4 |
sib2 r4 |
do'2 r4 |
sib2 r4 |
sib fa'4. mib'8 |
re'4
