\clef "dessus" r4 r8 sib' re'' mib'' |
fa''4 fa''4.(\trill mib''16 fa'') |
sol''4. sol''8 la'' sib'' |
la''4.\trill sol''8 fa'' mib'' |
re''4.\trill sib'8 re'' mib'' |
fa''4 fa''4.(\trill mib''16 fa'') |
sol''4. sol''8 la'' sib'' |
sol'' la'' la''4.\trill sib''8 |
sib''4 sib'' la''8\trill sol'' |
fa''4 do'' fa'' |
fa'' sib' mib'' |
mib'' do'' fa'' |
fa'' sib'' la''8\trill sol'' |
fa''4 do'' fa'' |
fa'' sib' mib'' |
mib'' do'' fa'' |
fa''4. <>^"Hautbois" re''8 mib'' re'' |
do''2\trill r4 |
sib'2 r4 |
sib'8.( do''16) do''4.(\trill sib'16 do'') |
re''4. re''8 mib'' re'' |
do''2\trill r4 |
sib'2 r4 |
sib'8.( do''16) do''4.\trill sib'8 |
sib'4 <>^"Tous" sib'' sib'' |
sib''4 la''8\trill sol'' fa'' mib'' |
re''4 sol''8 fa'' mib''\trill re'' |
do'' fa'' re''4.\trill do''8 |
sib'4 sib'' sib'' |
sib'' la''8\trill sol'' fa'' mib'' |
re''4 sol''8 fa'' mib''\trill re'' |
do'' fa'' re''4.\trill do''8 |
sib'4 sol'' mib'' |
do'' fa'' re'' |
sib' mib'' do'' |
la'8 sib' do'' sib' la' sol' |
fa'4 sol'' mib'' |
do'' fa'' re'' |
sib' mib'' do'' |
la'8 sib' do'' sib' la' sol' |
fa'4 <>^"Hautbois" fa'4. fa'8 |
do''( re'') re''4.\trill( do''16 re'') |
mib''4. re''8 mib'' fa'' |
mib''\trill re'' do''4.\trill sib'8 |
sib'4 <>^"Tous" fa'4. fa'8 |
do''8( re'') re''4.(\trill do''16 re'') |
mib''4. re''8 mib'' fa'' |
mib'' re'' do''4.\trill sib'8 |
sib'4 %{%} <>^"Violons" reb''2\doux |
do''4 fa''2 |
sib'4 solb'' fa'' |
mib'' reb''\trill do'' |
sib' reb''2 |
do''4 fa''2 |
sib'4 solb'' fa'' |
mib'' reb''\trill do'' |
sib'2 do''4 |
reb''2 do''4\trill |
sib'2 do''4 |
reb''8.( mib''16) mib''4.(\trill reb''16 mib'') |
fa''2 do''4 |
reb''2 do''4\trill |
sib'2 do''4 |
reb''8.( mib''16) mib''4.(\trill reb''16 mib'') |
fa''4 sib'' lab'' |
sol'' la'' sib'' |
fa'' mib'' reb'' |
do'' reb'' sib' |
la' sib'' lab''? |
sol'' la'' sib'' |
fa'' mib'' reb''\trill~ |
reb''8 do'' do''2\trill |
sib'4 fa''2 |
r4 mib'' reb'' |
do''2 fa''4 |
mib'' reb''\trill do'' |
reb'' fa''2 |
r4 mib'' reb'' |
do''2 fa''4 |
mib'' reb''\trill do'' |
reb'' solb'' fa'' |
solb'' fa'' mib'' |
fa''2. |
sib'8 do'' do''4.\trill sib'8 |
sib'4 solb'' fa'' |
solb'' fa'' mib'' |
fa''2. |
sib'8 do'' do''4.\trill sib'8 |
%{%} sib'4 <>^"Tous" re''2\fort |
r4 mib'' re'' |
do''\trill sib' do'' |
re'' sib'8.( do''16) do''8.(\trill sib'32 do'') |
re''4 fa''2 |
r4 mib'' re'' |
do''\trill sib' do'' |
re'' sib'8.( do''16) do''8.(\trill sib'32 do'') |
re''4 sib'' la'' |
sol'' fa'' mib'' |
re'' do'' sib' |
la' sol' fad' |
sol' sib'' la'' |
sol'' fa'' mib'' |
re'' do'' sib' |
la' sol' fad' |
sol'4. <>^"Hautbois" la'8 sib' do'' |
re''4 sol' la'\trill |
sib'4. la'8 sib' do'' |
re''4 sol' la'\trill |
sib' re'' mi'' |
fa'' sol''8( fa'' mi'' fa'') |
fad''4.(\trill mi''16 fad'') sol''4~ |
sol''8 la'' fad''4.\trill( mi''16 fad'') |
sol''4 re'' mi'' |
fa''4 sol''8( fa'' mi'' fa'') |
fad''4.(\trill mi''16 fad'') sol''4~ |
sol''8 la'' fad''4.\trill( mi''16 fad'') |
sol''8 <>^"Tous" re'' sol'' la'' sib''4 |
la''8 re'' re'' re''16 mib'' re'' do'' sib' la' |
sol'8 sol'16 la' sib'8 la' sib' do'' |
re''8 re''16 mi'' fad''8 mi''16 re'' mi''8 fad'' |
sol'' re'' sol'' la'' sib''4 |
la''8 re'' re'' re''16 mib'' re'' do'' sib' la' |
sol'8 sol'16 la' sib'8 la' sib' do'' |
re''8 re''16 mi'' fad''8 mi''16 re'' mi''8 fad'' |
sol'' sol' sib' sol' mib''4 |
r8 la' do'' la' re''4 |
r8 sol' sib' sol' do''4 |
r8 fad'16 sol' la'8 la'16 sib' do''8 re'' |
sib' sol' sib' sol' mib''4 |
r8 la' do'' la' re''4 |
r8 sol' sib' sol' do''4 |
r8 fad'16 sol' la'8 la'16 sib' do''8 re'' |
sib' re'' sol'' sol''16 fa'' mib'' re'' do'' sib' |
la'8 do'' fa'' fa''16 mib'' re'' do'' sib' la' |
sol'8 sib' mib'' mib''16 re'' do'' sib' la' sol' |
fad'8 re'' sol'' sol''16 fad'' sol''8 la'' |
sib'' re'' sol'' sol''16 fa'' mib'' re'' do'' sib' |
la'8 do'' fa'' fa''16 mib'' re'' do'' sib' la' |
sol'8 sib' mib'' mib''16 re'' do'' sib' la' sol' |
fad'8 re'' sol'' sol''16 fad'' sol''8 la'' |
sib''4 re''4. sol''8 |
mi''4 mi''4. la''8 |
fad''4.\trill( mi''16 fad'') sol''4~ |
sol''8. sib'16 la'4.\trill sol'8 |
sol'4 re''4. sol''8 |
mi''4 mi''4. la''8 |
fad''4.\trill( mi''16 fad'') sol''4~ |
sol''8. sib'16 la'4.\trill sol'8 |
sol' re'' fa'' re'' sol''4 |
r8 mi'' sol'' mi'' la''4 |
r8 sib''16 la'' sol''8 la''16 sib'' do''' sib'' la'' sol'' |
fad''8 mib''16 re'' sib''8. la''16 la''8.\trill sol''16 |
sol''8 re'' fa'' re'' sol''4 |
r8 mi'' sol'' mi'' la''4 |
r8 sib''16 la'' sol''8 la''16 sib'' do''' sib'' la'' sol'' |
fad''8 mib''16 re'' sib''8. la''16 la''8.\trill sol''16 |
sol''4 sib'' sib'' |
fa'' sol''8 la'' sib''4 |
fa'' sib'' sib'' |
la''8 sol'' fa'' mib'' re'' do'' |
sib'4 sib'' sib'' |
fa'' sol''8 la'' sib''4 |
fa'' sib'' sib'' |
la''8 sol'' fa'' mib'' re'' do'' |
sib'2 r4 |
<>^"Hautbois" do''2 r4 |
re''8( mib''16 fa'') mib''4.\trill re''8 |
do''2 r4 |
sib'2 r4 |
do''2 r4 |
re''8( mib''16 fa'') mib''4.\trill re''8 |
do''2\trill sib'4 |
<>^"Tous" fa'4. sol'8 fa' sol' |
fa'4. sol'8 fa' sol' |
fa'4 mi'8 fa' sol' mi' |
fa' sol' la' sib' do''4 |
fa'4. sol'8 fa' sol' |
fa'4. sol'8 fa' sol' |
fa'4 mi'8 fa' sol' mi' |
fa' sol' la' sib' do''4 |
re''4. mib''8 re'' mib'' |
do''4. re''8 do'' re'' |
sib' la' sib' re'' do'' sib' |
la'4.(\trill sol'16 la') sib'4 |
re''4. mib''8 re'' mib'' |
do''4. re''8 do'' re'' |
sib' la' sib' do'' re'' mib'' |
fa'' sol'' do''4.\trill sib'8 |
sib'4
