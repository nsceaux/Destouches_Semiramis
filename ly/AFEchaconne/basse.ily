\clef "basse" sib2 r4 |
la2 r4 |
sol2 r4 |
fa2 fa,4 |
sib,2 r4 |
la,2 r4 |
sol,2 r4 |
mib, fa,2 |
sib,4 sib sib |
la la, la |
sol sol, sol |
fa fa, fa, |
sib, sib sib |
la la, la |
sol sol, sol |
fa fa, fa, |
sib, \once\override TextScript.outside-staff-priority = #999 <>^"Bassons" sib sib |
<>^"Notes égales" la8 sol fa sol lab fa |
sol fa mib fa sol mib |
fa4 fa, fa, |
sib, sib sib |
la8 sol fa sol lab fa |
sol fa mib fa sol mib |
fa4 fa, fa, |
sib, <>^"Tous" sib, sib, |
do2 la,4 |
sib, mib do |
fa4 fa, fa, |
sib, sib, sib, |
do2 la,4 |
sib, mib do |
fa fa, fa, |
sib, sib2 |
la r4 |
sol2 r4 |
fa fa, fa, |
sib, sib2 |
la r4 |
sol2 r4 |
fa fa, fa, |
sib, <>^\markup\whiteout Bassons sib2 |
la4 lab2 |
sol mib4~ |
mib fa fa, |
sib, <>^"Tous" sib2 |
la4 lab2 |
sol mib4~ |
mib fa fa, |
sib, %{%} <>^\markup\whiteout { Basses de violons } sib2\douxSug |
la4 fa2 |
solb4 mib reb |
do fa fa, |
sib, sib2 |
la4 fa2 |
solb4 mib reb |
do fa fa, |
sib,2 la4 |
sib2 fa4 |
solb2 mib4 |
sib8.( lab16) solb2\trill |
fa la4 |
sib2 fa4 |
solb2 mib4 |
sib8.( lab16) solb2\trill |
fa4 reb reb |
mib8 reb do4 sib, |
la,2 reb4 |
mib fa solb |
fa reb reb |
mib8 reb do4 sib, |
la,2 sib,4 |
mib fa fa, |
sib, sib lab |
sol la sib |
fa mib reb |
do fa fa, |
sib, sib lab |
sol la sib |
fa mib reb |
do fa fa, |
sib, sib la |
sib lab! solb |
fa8 solb fa mib reb4 |
mib fa fa, |
sib, sib la |
sib lab! solb |
fa8 solb fa mib reb4 |
mib4 fa fa, |
%{%} sib,4 <>^"Tous" sib\fortSug la |
sol la sib |
fa sol la |
sib sol la |
sib sib la |
sol la sib |
fa sol la |
sib sol la |
sib r r |
mib re do |
sib, la, sol, |
do re re, |
sol, r r |
mib re do |
sib, la, sol, |
do re re, |
sol, r r |
<>^"Bassons" sol, r r |
sol, r r |
sol, r r |
sol, sib sib |
la si do' |
re' do' sib |
do' re' re |
sol sib sib |
la si do' |
re' do' sib |
do' re' re |
sol4. <>^"Tous" sol8 sol sol |
fa4. fa8 fa fa |
mib4. mib8 mib mib |
re4. re8 re re |
sol,4. sol8 sol sol |
fa4. fa8 fa fa |
mib4. mib8 mib mib |
re4. re8 re re |
sol,4. sol8 sol sol |
fa4. fa8 fa fa |
mib4. mib8 mib mib |
re4. re8 re re |
sol,4. sol8 sol sol |
fa4. fa8 fa fa |
mib4. mib8 mib mib |
re4. re8 re re |
sol,4 r r8 do |
fa fa16 mib re mib re do sib,8 sib, |
mib mib16 re do re do sib, la,8 la, |
re re16 do sib,8 do re re, |
sol,4 r r8 do |
fa fa16 mib re mib re do sib,8 sib, |
mib mib16 re do re do sib, la,8 la, |
re re16 do sib,8 do re re, |
sol, sol sib sib16 la sol fa mib re |
do8 sol do' do'16 sib la sol fad mi |
re8 la re' re'16 do' sib la sol fa |
mib8 do re do re re, |
sol, sol sib sib16 la sol fa mib re |
do8 sol do' do'16 sib la sol fad mi |
re8 la re' re'16 do' sib la sol fa |
mib8 do re do re re, |
sol,4. sol8 si sol |
do'4 r8 la dod' la |
re'4 mib' do' |
re' sol re |
sol,4. sol8 si sol |
do'4 r8 la dod' la |
re'4 mib' do' |
re' sol re |
sol, r r |
r mib mib |
re re8 do re mib |
fa4 fa fa, |
sib,4 r r |
r mib mib |
re re8 do re mib |
fa4 fa fa, |
<>^"Notes égales" ^"Bassons" sib,8 la, sib, do sib, do |
la, sol, fa, sol, la, fa, |
sib,4 la, sib, |
fa8 mib fa sol la fa |
sol fa sol la sib sol |
la sol fa sol la fa |
sib4 la sib |
fa fa, sib, |
<>^"Tous" sib,2 r4 |
la,2 r4 |
sol,2 r4 |
fa,4 fa,8 sol, la, fa, |
sib,2 r4 |
la,2 r4 |
sol,2 r4 |
fa,4 fa,8 sol, la, fa, |
sib2 r4 |
la2 r4 |
sol2 r4 |
fa4 fa, sib, |
sib2 r4 |
la2 r4 |
sol2 r4 |
re8 mib fa4 fa, |
sib,
