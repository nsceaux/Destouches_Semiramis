\clef "dessus" R2.*17 |
r4 r8 sol'8 sib'8.( do''16) do''8.(\trill sib'32 do'') |
re''2~ re''8 mib''16 re'' do'' sib' la' sol' |
fad'4. sib'16( la') sol'4.\trill fad'8 |
sol'4 re' r2 |
r4 r8 fad' la'8.( sib'16) sib'8.(\trill la'32 sib') |
do''4. do''8 do''4 sib'8 la' |
sib'4 sol' sib'4. sib'8 |
do''4 do''8 la' sib' la' sib' do'' |
re''4 do''8 sib' la'4.\trill sib'8 |
sib'4 r8 sol' sib'8.( do''16) do''8.(\trill sib'32 do'') |
re''4 r r2 |
sib'2 re''4. re''8 |
re''8 do'' sib' la' sol'4 do'' |
re''8 do'' sib' la' sol'4.\trill fa'8 |
fa'4. r8 r4 r8 fa'' |
re''4 re''8 re'' mi''4 mi''8 mi'' |
dod'' re'' mi'' dod'' fa''4 fa''8 mi'' |
re''4 re''8 re'' re''4( dod'') |
re''2 r |
r sib' |
si' do''4. do''8 |
do''4. si'8 si'4.(\trill la'16 si') |
do''4 sib'8 do'' re''4 re''8 mib'' |
la'2 r |
r sib' |
la' sol'4. fad'8 |
sol'1 |
re'4 re''8 do'' sib'4.\trill la'8 |
sol'2 la'4 fad' |
sol'1 |
R2.*27 |
R1*2 |
R2.*2 |
r4 r re'' |
re''2. |
do''4 do''8 mib'' re'' do'' |
sib'4 sib' sib' |
la' la' sib' |
sol'2 sol''8 sol'' |
fa''4 fa'' sol'' |
mib''8 re'' mib'' fa'' re'' mib'' |
do'' sib' do'' re'' mib'' do'' |
re''2. |
re''2 mi''8 re'' |
dod'' re'' mi'' fa'' re'' fa'' |
mi''4 mi''8 re'' mi'' dod'' |
re''2 la'4 |
r r r8 sol'' |
fad''4. mi''8 fad''4 |
sol''4. do''8 sib'4 |
la'8 sib' do'' sib' la' do'' |
sib' la' sol' la' sib' do'' |
la'\trill sol' fad'4.\trill sol'8 |
sol'2. sol''8 sol'' |
sol''4 fa''8 mib'' re'' do'' sib' do'' |
la'4. sol'8 la' sib' do'' la' |
sib'4 re'' sib'8 sib' sib' sib' |
do''4 do''8 sib' sib'4( la') |
sib'2. re''4 |
re''4. re''8 re''4 re'' |
mib''4. mib''8 mib''4 mib'' |
re''2. re''4 |
fa''2 fa''4 fa''8 fa'' |
mib''2 mib''4 mib'' |
sol''2 sib'4 sol'8 sib' |
la'2 la'4. sol'8 |
fad'4. mi'8 fad' sol' la' fad' |
sol'2 sib'4 sib'8 sib' |
sol'2 do''4 do'' |
do''2 sib'4 sib' |
sib'4. la'16 sol' fad'4. sol'8 |
sol'2 r |
R1*6 R1. R1*7 |
