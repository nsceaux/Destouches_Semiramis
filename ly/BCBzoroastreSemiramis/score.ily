\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new GrandStaff <<
        \new Staff \with {
          \haraKiriFirst
          \consists "Horizontal_bracket_engraver"
        } <<
          \global \includeNotes "dessus1"
          { s2.*17 s1*9 s4 \footnoteHere #'(0 . 0) \markup\wordwrap {
              L’édition imprimée ne contient pas cette mesure,
              ajoutée sur la partition manuscrite.
            }
            \override Staff.HorizontalBracket.direction = #UP
            \override Staff.HorizontalBracket.padding = #0.1
            s2.\startGroup s4\stopGroup }
        >>
        \new Staff \with { \haraKiriFirst } <<
          \global \includeNotes "dessus2"
          \modVersion {
            s2.*17 s1*30\break s2.*27 s1*2 s2.*22 s1*18\break \startHaraKiri
          }
        >>
      >>
      \new Staff \with { \haraKiriFirst } <<
        \global \includeNotes "haute-contre"
      >>
      \new Staff \with { \haraKiriFirst } <<
        \global \includeNotes "taille"
      >>
    >>
    \new Staff \withLyrics <<
      \global
      \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new Staff \with { \consists "Horizontal_bracket_engraver" } <<
      \global
      \keepWithTag #'basse-continue \includeNotes "basse"
      { s2.*17 s1*9 s4 s2.\startGroup s4\stopGroup }
      \includeFigures "chiffres"
      \origLayout {
        s2.*4 s2 \bar "" \break s4 s2.*4\break
        \grace s8 s2.*5\break \grace s8 s2.*3\pageBreak
        s1*5 s2 \bar "" \break s2 s1*4\break s1*4 s2 \bar "" \pageBreak
        s2 s1*4\break s1*6\break s1*5\pageBreak
        s2.*6\break s2.*6\break s2.*5\break
        s2.*5\break s2.*5\break s1*2 s2.\pageBreak
        s2.*6\break s2.*6\break s2.*6\pageBreak
        \grace s8 s2.*3 s1*3\break s1*4\break s1*5\pageBreak
        s1*6\break s1*6\pageBreak
        s1 s1. s1*2\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
