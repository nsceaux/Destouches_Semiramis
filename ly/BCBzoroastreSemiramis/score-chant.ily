\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global
      \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new Staff \with { \consists "Horizontal_bracket_engraver" } <<
      \global
      \keepWithTag #'basse-continue \includeNotes "basse"
      { s2.*17 s1*9 s4 s2.\startGroup s4\stopGroup }
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
