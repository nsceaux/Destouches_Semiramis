\key re \minor
\digitTime\time 3/4 \midiTempo#120 s2.*17
\beginMark "Air" \time 2/2 s1*4 \bar ".!:" s1*6 \alternatives s1 s1
s1*18
%% Semiramis
\digitTime\time 3/4 \bar ".!:" s2.*9 \alternatives s2. s2.
s2.*16
%% Zoroastre
\digitTime\time 2/2 s1*2
\digitTime\time 3/4 s2. \beginMark "Air" s2.*21
\digitTime\time 2/2 s1*25
\time 3/2 s1.
\digitTime\time 2/2 s1*7 \bar "|."
