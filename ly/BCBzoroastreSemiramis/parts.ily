\piecePartSpecs
#`((dessus #:score "score-dessus")
   (dessus-hc #:score "score-dessus-hc")
   (dessus2-hc #:score "score-dessus2-hc")
   (haute-contre)
   (taille)
   (basse #:tag-notes basse)
   (basse-continue #:score-template "score-basse-continue-voix"
                   #:tag-notes basse-continue)
   (chant)
   (silence #:on-the-fly-markup , #{ \markup\tacet#131 #}))
