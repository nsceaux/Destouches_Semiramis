\clef "dessus" R2.*17 |
r4 r8 <>_"Violons" re'' sol''8.( la''16) la''8.\trill( sol''32 la'') |
sib''2~ sib''8 sol''16 fa'' mib'' re'' do'' sib' |
la'4. re''16( do'') sib'4.\trill la'8 |
sib'4 sol' r2 |
r4 r8 re'' fad''8.( sol''16) sol''8.(\trill fad''32 sol'') |
la''4. la''8 la''4. la''8 |
re''4 sib' sol''4. sol''8 |
sol''4 fa''8\trill mib'' re'' do'' re'' mib'' |
fa''4 fa''8 sol'' do''4.\trill sib'8 |
sib'4 r8 re'' sol''8.( la''16) la''8.(\trill sol''32 la'') |
sib''4 r r2 |
sib'2 fa''4 fa''8 fa'' |
sib'' la'' sol'' fa'' mi''4 fa'' |
fa''2 fa''4 mi'' |
fa''4. r8 r4 r8 la'' |
fa''4 fa''8 fa'' sib''4 sib''8 sol'' |
mi'' fa'' sol'' mi'' la''4 la''8 sol'' |
fa'' sol'' la'' sib'' mi''4.\trill re''8 |
re''2 sol'' |
fa'' mib'' |
re'' sol''4. fa''8 |
fa''2.( mib''8\trill) re'' |
mib''4 re''8 fad'' sol''4 sol''8 la'' |
fad''2 sib'' |
la'' sol'' |
fad'' re''4. do''8 |
re''2.( do''8)\trill sib' |
la'4. re''8 re''4.\trill do''8 |
sib'2 do''4 la' |
sib'1 |
R2.*27 |
R1*2 | \allowPageTurn
R2.*2 |
r4_\markup\whiteout Violons r fad'' |
sol''2 sol''4 |
sol'' fa'' fa'' |
fa'' mib'' mib'' |
mib'' re'' re'' |
re'' do'' do'' |
do'' sib' sib' |
sib' la' sib' |
la'8 sol' la' sib' do'' la' |
sib'2. |
sib''4. la''8 sol'' fa'' |
mi''4 la''4. la''8 |
la''4 sol''8 fa'' sol'' mi'' |
fa'' sol'' fa'' mi'' re'' fa'' |
fad''4.\trill sol''8 la'' sib'' |
la''4. sol''8 la''4 |
sib''4. la''8 sol''4 |
fad''8 sol'' la'' sol'' fad'' mi'' |
re'' do'' sib' do'' re'' mib'' |
do''\trill sib' la'4.\trill sol'8 |
sol'2. sib''8 sib'' |
sib''4 la''8 sol'' fa'' mib'' re'' mib'' |
do''4. sib'8 do'' re'' mib'' do'' |
fa''4 fa'' re''8 re'' sol'' sol'' |
fa''4 mib''8 re'' do''4.\trill sib'8 |
sib'4. do''8 re'' mib'' fa'' re'' |
sib''4. sib''8 sib''4 fa'' |
sol''4. sol''8 sol''4 sol'' |
fa''2. fa''4 |
lab''2 lab''4 lab''8 lab'' |
sol''2 sol''4 sol'' |
sib''2 re''4 re''8 sol'' |
fad''2 re''4 do''8 sib' |
la'4. sol'8 la' sib' do'' la' |
re''2 re''4 re''8 re'' |
do''2 mib''4 mib'' |
mib''2 re''4 re'' |
re''4. do''16 sib' la'4.\trill sol'8 |
<< sol'2 \\ sol' >> <>^\markup\italic { Plus legerement } re''4 re''8 mib'' |
do''4( la') sib' la'16( sol'8 la'16) |
la'4\trill la' sib'8.( do''16) do''8.\trill( sib'32 do'') |
re''4. re''8 sol''4 sol''8 la'' |
fad''2 sib'4\doux do'' |
do'' sib' sib' la' |
sib' fa' re'' mi'' |
dod'' dod'' re''2 re''4 dod'' |
re''4. mi''8 fad'' sol'' la'' fad'' |
sib''4. re''8 mi''8.( fa''16) fad''8.(\trill mi''32 fad'') |
sol''4. sib'8 la'4\trill sol' |
re'' do''8\trill sib' la'4 re'' |
re'' mi''8 fad'' sol''4 sol'' |
sol''2 sol''4 fad'' |
sol''1 |
