<<
  %% Semiramis
  \tag #'(semiramis basse) {
    <<
      \tag #'basse { s2.*17 s1*30 \semiramisMarkText "Legerement" }
      \tag #'semiramis {
        \clef "vbas-dessus" R2.*17 R1*30 <>^\markup\italic "Legerement"
      }
    >>
    r4 sol' la' |
    sib' do'' re'' |
    \appoggiatura re''8 mib''2 mib''4 |
    do''4.\trill\prall sib'8 do'' re'' |
    sib'4\trill sol' r8 la' |
    sib'2. |
    do''2\trill sib'8 do'' |
    \appoggiatura do''8 re''2 la'8 do'' |
    sib'4 la' sol' |
    la'2.\trill |
    la'4\trill re'' do'' |
    si' la' si' |
    do'' re'' mib'' |
    fa''4 sol''8[ fa''] mib''[ re''] |
    mib''2. |
    sib'4 do''4. re''8 |
    sol'4. la'8 sib' do'' |
    re''4( do''2\trill) |
    sib'4 re'' do'' |
    sib' la' sol' |
    do'' sib' la' |
    sib' do''8[ sib'] la'[ sol'] |
    fad'2. |
    re''4 do''4. re''8 |
    la'4. sib'8 fad' sol' |
    sol'[ la']~ la'2\trill |
    sol'2. |
    \tag #'semiramis { R1*2 R2.*22 R1*25 R1. R1*7 }
  }
  %% Zoroastre
  \tag #'(zoroastre basse) {
    \tag #'basse \zoroastreMarkText "Gracieusement"
    \tag #'zoroastre { \clef "vbasse" <>^\markup\italic "Gracieusement" }
    sol4 sol8 sol la sib |
    fad2 r8 sol |
    la4. sib8 sol la |
    \appoggiatura la8 sib4. la8( sol4) |
    sib4 la8 sol fa mib |
    re2\trill re4 |
    sol4. sol8 la sib |
    la2\trill r8 re |
    sol sol16 lab fa4\trill mib8 re |
    \appoggiatura re16 mib4 do fa8 mib |
    re4\trill do4. sib,8 |
    fa2 fa4 |
    sol4. la8 sib sol |
    do'4 la\trill re' |
    \appoggiatura la8 sol4. mi8 fad sol |
    fad2\trill sol8 la |
    \appoggiatura la8 sib do' sib4( la\trill) |
    sol r4 r2 |
    R1*2 |
    r4 r8 sol sib4 do' |
    re'2~ re'8[\melisma mib'16 re'] do'[ sib la sol]( |
    fad4.)\melismaEnd fad8 fad4. fad8 |
    sol4 sol sol4. sol8 |
    la4 la8 fa sib2 |
    re4\trill re8 mib fa4. fa8 |
    sib,4. r8 r2 |
    r4 r8 sol8 sib4 do' |
    sib,2 sib4 sib8 sib |
    sol4 sol do' la |
    sib2 do'4 do'8 do |
    fa4. fa8 re4 re8 re |
    sib4 sib8 la sol16[ la sib la sol fa mi re]( |
    la4) la8 la fa4 sol8 la |
    sib4 fa8 sol la2 |
    re r |
    r sol |
    fa mib4. re8 |
    re1\trill |
    do4 sol8 la sib4 sib8 do' |
    \appoggiatura do'8 re'2 r |
    r re' |
    do' sib4. la8 |
    sib2.( la8)\trill[ sol] |
    fad2 sol4. re8 |
    mib2 do4 re |
    sol,1 |
    <<
      \tag #'basse { s2.*27 \zoroastreMark }
      \tag #'zoroastre { R2.*27 }
    >>
    sol4. sol8 sol4 sol8 re |
    sol4 sol8 la sib4 la8 sol |
    la2\trill re'4 |
    re'2. |
    do'4 do'4. re'8 |
    sib4 sib sib |
    la la sib |
    \appoggiatura la8 sol2 sol8 sol |
    fa4 fa sol |
    mib8[ re mib sol fa mib]( |
    re4)\trill re mib |
    do do sib, |
    fa2 fa4 |
    sib2 sib8 la |
    sol8[ fa sol la sib sol]( |
    la4) la si |
    dod' dod' la |
    re'2 r8 re' |
    do'4.\trill\prall sib8 la sol |
    do'4. sib8 la4 |
    sol4.\trill fad8 sol4 |
    \appoggiatura sol8 re2 re4 |
    R2.*2 |
    r2 r4 sol8 sol |
    la4. la8 sib4 sib8 sib, |
    fa2. fa4 |
    re8\trill re re re sol4 sol8 sol |
    la4 la8 sib fa2 |
    sib,2. sib4 |
    \appoggiatura la8 sol4. sol8 sol4 sib |
    mib2. mib4 |
    sib2 sib4 sib8 sib |
    fa2 fa4 fa |
    do'2 do'4 do'8 do' |
    sol2 sol4 sib |
    re'2 fad4. sol8 |
    \appoggiatura sol8 re2. re'4 |
    \appoggiatura do'8 sib2 sib4 sib8 sol |
    mib'2 do'4 mib' |
    fad2 sol4 sib |
    \appoggiatura sib8 do2 re4. re8 |
    sol,2 r |
    R1*3 |
    r2 sol4 sol8 lab |
    fa4 re mib fa |
    re sib, sib sol |
    mi4.\trill mi8 la2 mi4 fa |
    re1 |
    sol4. fad8 sol4 la |
    sib2 re4 sol |
    fad2. fad8 fad |
    sol4 sol8 la sib4 do' |
    re'4. re'8 re'4 re |
    sol2 sol |
  }
>>