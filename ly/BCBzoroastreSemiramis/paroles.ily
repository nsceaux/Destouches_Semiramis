\tag #'(zoroastre basse) {
  Bel -- le Se -- mi -- ra -- mis, l’a -- mour & l’es -- pe -- ran -- ce
  Par des che -- mins nou -- veaux m’a -- me -- nent dans ces lieux.
  Le char -- me de vô -- tre pré -- sen -- ce
  A dé -- ja ré -- pa -- ré l’ex -- trê -- me vi -- o -- len -- ce
  Des maux que j’ai souf -- ferts, é -- loi -- gné de vos __ yeux.
  Le Dieu qui lan -- ce le Ton -- ner -- re
  M’a re -- mis un pou -- voir peu dif -- fe -- rent du sien ;
  Le Dieu qui   sien ;
  Il m’a ren -- du des Rois l’Ar -- bitre & le Soû -- tien.
  J’é -- teins & j’al -- lu -- me la guer -- re,
  Je fais le des -- tin de la ter -- re,
  Et c’est dans vos beaux yeux que je cher -- che le mien.
  Et c’est dans vos beaux yeux que je cher -- che le mien.
}
\tag #'(semiramis basse) {
  Vos plus fiers En -- ne -- mis vous ce -- dent la vic -- toi -- re.
  Vôtre Art, vô -- tre Va -- leur peu -- vent tout sur -- mon -- ter :
  ter :
  Un cœur tel que le mien pou -- roit- il se flat -- ter,
  De man -- quer seul à vô -- tre gloi -- re ?
  Un cœur tel que le mien pou -- roit- il se flat -- ter,
  De man -- quer seul à vô -- tre gloi -- re ?
}
\tag #'(zoroastre basse) {
  Peu -- ples des E -- le -- mens, pa -- rois -- sez à mes yeux.
  Es -- prits, qui par l’A -- mour dis -- per -- sez en tous lieux,
  Sur la Terre & sur l’Onde __ é -- ten -- dez son Em -- pi -- re ;
  Vous qui vo -- lez __ a -- vec lui dans les Cieux,
  Joi -- gnez tous vos trans -- ports à l’ar -- deur qui m’ins -- pi -- re.
  De ce jour cé -- le -- bre pour nous,
  Ren -- dez à l’a -- ve -- nir la me -- moi -- re du -- ra -- ble.
  L’ou -- vra -- ge des Mor -- tels comme eux est pe -- ris -- sa -- ble,
  Dres -- sez un mo -- nu -- ment im -- mor -- tel com -- me vous.
  Dres -- sez un mo -- nu -- ment im -- mor -- tel, im -- mor -- tel com -- me vous.
  Qu’u -- ne nou -- vel -- le Flore ex -- ha -- le
  Des par -- fums du ciel des -- cen -- dus.
  Et que ces jar -- dins sus -- pen -- dus
  De la terre & des cieux rem -- plis -- sent l’in -- ter -- val -- le.
}
