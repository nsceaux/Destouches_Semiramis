\clef "basse"
<<
  \tag #'basse { R2.*17 R1*30 R2.*27 R1*2 R2.*22 R1*18 | r2 }
  \tag #'basse-continue {
    sol,2. |
    la,2 sib,4 |
    do re re, |
    sol,2.~ |
    sol,2 la,4 |
    sib,2 sol,4 |
    mib8 re do sib, la, sol, |
    re4. mib8 re do |
    si,2. |
    do4. sib,8 la, fa, |
    sib,4 la, sol, |
    fa, fa8 mib re sib, |
    mib2. |
    do4 fa sib, |
    mib8 re do2 |
    re8 mib re do sib, la, |
    sol, do, re,2 |
    sol,4 r r2 |
    r4 r8 sol, sib,4 do |
    re4. re8 re4 re, |
    sol,4. sol8 sib4 do' |
    re'2~ re'8 mib'16 re' do' sib la sol |
    fad4. fad8 fad4. fad8 |
    sol4 sol sol4. sol8 |
    la4 la8 fa sib2 |
    re4 re8 mib fa4. fa8 |
    sib,4. r8 r2 |
    r4 r8 sol8 sib4 do' |
    sib,2 sib4 sib8 sib |
    sol4 sol do' la |
    sib2 do'4 do'8 do |
    fa4. fa8 re4 re8 re |
    sib4 sib8 la sol16 la sib la sol fa mi re |
    la4 la8 la fa4 sol8 la |
    sib4 fa8 sol la2 |
    re r | \allowPageTurn
    r sol |
    fa mib4. re8 |
    re1 |
    do4 sol8 la sib4 sib8 do' |
    re'2 r |
    r re' |
    do' sib4. la8 |
    sib2.( la8)\trill sol |
    fad2 sol4. re8 |
    mib2 do4 re |
    sol,1 |
    sol,4 sol re |
    sol8 fa mib4 re |
    do do'8 re' do' sib |
    la4 re' re |
    sol8 fa mib re mib do |
    sol4 sol, sol |
    la la, la |
    sib do' re' |
    sol fa mib |
    re8 mib re do sib, la, |
    re4. mi8 fad re |
    sol4 sol fa |
    mib re do |
    si,2 sol,4 |
    do8 si, do re mib do |
    sol2 re4 |
    mib2 re8 mib |
    fa4 fa,2 |
    sib,4 r r |
    re' do' sib |
    la sol fad |
    sol fa mib |
    re do re |
    sib, la, sol, |
    do8 re do sib, la, sol, |
    re4 re,2 |
    sol,2. | \allowPageTurn
    sol,1 |
    mib4 do sol sol, |
    re2 re'4 |
    re'2. |
    do'4 do'4. re'8 |
    sib4 sib sib |
    la la sib |
    sol2 sol8 sol |
    fa4 fa sol |
    mib8 re mib sol fa mib |
    re4 re mib |
    do do sib, |
    fa2 fa4 |
    sib2 sib8 la |
    sol fa sol la sib sol |
    la4 la si |
    dod' dod' la |
    re'2 r8 re' |
    do'4.\trill sib8 la sol |
    do'4. sib8 la4 |
    sol4.\trill fad8 sol4 |
    re2. |
    sol2 sol,4 |
    do4 re re, |
    sol,2. sol8 sol |
    la4. la8 sib4 sib8 sib, |
    fa2. fa4 |
    re8 re re re sol4 sol8 sol |
    la4 la8 sib fa2 |
    sib,2. sib4 |
    sol4. sol8 sol4 sib |
    mib2. mib4 |
    sib2 sib4 sib8 sib |
    fa2 fa4 fa |
    do'2 do'4 do'8 do' |
    sol2 sol4 sib |
    re'2 fad4. sol8 |
    re2. re'4 |
    sib2 sib4 sib8 sol |
    mib'2 do'4 mib' |
    fad2 sol4 sib |
    do2 re4. re8 |
    sol,2
  }
>> sol2 |
sol4 fad sol sol, |
re8 mi fad re sol2 |
fa mib |
re mib |
re do |
sib, sol,8 la, sib, sol, |
la,4 sol, fa,2 sol,4 la, |
re,2 re8 mib re do |
sib,4. do8 sib,4 la, |
sol,2 fa,4 mib, |
re, re8 re re4 do |
sib,8 do sib, la, sol,4 mib |
re4. re8 re4 re |
sol,1 |
