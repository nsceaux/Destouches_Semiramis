\score {
  <<
    \new Staff \with { \haraKiriFirst } <<
      \global \includeNotes "dessus2"
      \modVersion {
        s2.*17 s1*30\break s2.*27 s1*2 s2.*22 s1*18 s2 \bar "" \break \startHaraKiri
      }
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \includeNotes "haute-contre" \clef "dessus"
    >>
  >>
  \layout { }
}
