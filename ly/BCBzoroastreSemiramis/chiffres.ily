s2. <6+>2 <6>4 <6 _-> <7 _+> \new FiguredBass { <4>8 <3+> } s2. s2 <6>8 <5/>

s2. s2 <6+>4 <_+>2\figExtOn <_+>4\figExtOff <6>2 <5->4 <_->4.\figExtOn <_->8\figExtOff
<6>8 <7-> s4 <6> <6> s2 \new FiguredBass <6>4 s2. <_->4 <7->2 <"">4\figExtOn <"">4\figExtOff <6> <_+>2
<6>8 <6+> s \new FiguredBass { <7 _-> <6 4>4\figPosOn <6 3+>\figPosOff } s

s2. s2 <6>4 <7 _-> <_+>1 s2 <6>4 <7> <_+>1
<5/>2 <5/> s1 <7>4 <6>8 <7-> s2 <6>2 <3> s1 s2 <6>4 <7 _->

s1 <7>2. <6>4 <6 5>2 <5 4>4. <3>8 s1 s2 <6>
<_+> <6>4.\figExtOn <6>8\figExtOff s4 <6> <5 4>\figPosOn <5 3+>\figPosOff <_+>1 s2 <6-> <4+> <6> <7> <6+>

<_->4. <6+>8 <6>4. <6 5>8 <_+>1 s2 <6 4> <4+> <6>4. <6+>8 <6>1 <6>2 s4. <7 _+>8 s2 <6 _->4 <7 _+> s1
s2 <7 _+>4 <"">8\figExtOn <"">\figExtOff <6>4 <6+> <_->2. <7>4 <_+>\figExtOn <_+>\figExtOff s2 s8 <6> s2. <7>4\figExtOn <7>\figExtOff <6+> <6>4 <9 7>

<7 _+>4 s <6> <6> <_+>2. <_+> <_+>4 <4 2> <4+> <6> <6+> <_-> <5/>2\figExtOn <5/>4\figExtOff
<_->2. s2 <6>4 s2 <6>8 <6 5> <4>4 <3>2 s2. <6 4>4 <4+> <6> <6+>2 <5/>4
s4 <6> <6> <_+>2\figExtOn <_+>4\figExtOff <6> <6+>2 <6 _->4. <6>8 <6+>4 <6 4> \new FiguredBass <3+>2 s2.

s1 s4 <6>2. <_+>2\figExtOn <_+>4\figExtOff
<_+>2. s <6> <7>4 <6>2 <7>4 <6->2 <7->4 <6>2 <7>4 <6>2 <7>4 <6>2

<7>4 <6>2 s2.*2 <7>2 <6 4>4 <_+>2 <7>4 <6> <5/>\figExtOn <5/>\figExtOff
<_->2. <4+>4. <6>8 <6+>4 <4+>4. <6>8 <6+>4 s4. <5/> <_+>2. s

<6 _->4 <_+>2 s1 <7>4 <6> s2 s1 <6>
<6>4 <5/> <5 4>4\figPosOn <5 3>\figPosOff s1*4 <_->1

<_->1 s2 <"">4\figExtOn <"">\figExtOff <_+>2 <5/> <_+>1 <6> <6>2 <_->4\figExtOn <_->\figExtOff <7->2
<"">4\figExtOn <"">\figExtOff <9 7>4 <8 7> <_+>2 s1 <6- 4 2>2\figExtOn <6->\figExtOff <_+>2. <6- 4>4 <6>2 <7>4 <6> <_+>2

<5>4 <6> <7> <6> <\markup\triangle-down 7 6 \figure-flat >2 s2. <4>4 <_+>\figExtOn <_+>\figExtOff <6>2 <6 5>4 <7 _+> \new FiguredBass <_+>1 <6>4. <6 4+ 2>8
<6 4>4 <6+> s2 \new FiguredBass { <6>4 <6> <_+>2\figExtOn <_+>2\figExtOff } <6>4 <4>8 <6+> s4 <6> <5 4>2. <_+>4
