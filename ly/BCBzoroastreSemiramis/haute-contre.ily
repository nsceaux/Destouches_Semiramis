\clef "haute-contre" R2.*17 R1*30 R2.*27 R1*2 R2.*22 R1*18 |
r2 sib'4. do''8 |
la'2 sol'4. la'8 |
fad'2 sol'4. sol'8 |
la'4. re''8 re''4. do''8 |
re''2 sol'4\douxSug sol' |
lab' fa' sol' mib' |
fa'2 sib'4 sib' |
la'2 la' sol'4 sol' |
fad'2 la'4 la' |
re''4. la'8 sib'4 do'' |
re''2 re'4 re'8 do' |
re'4 re' fad' la' |
sol'2 sol'4 do'' |
la'4. la'8 la'4.\trill sol'8 |
sol'1 |
