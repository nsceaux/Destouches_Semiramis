\clef "taille" R2.*17 R1*30 R2.*27 R1*2 R2.*22 R1*18 |
r2 sol'2 |
mib'4 do' sib re' |
re'2 re'4. mib'8 |
fa'2 sib'4. sol'8 |
la'2 sol'4\douxSug mib' |
fa'2 mib' |
sib re'4 re'8 mi' |
mi'2 fa' mi' |
re' re'4 re' |
re'2 sol'4 do' |
re'2 la |
la re'4 la |
sib2 re'4 sol |
re'2 re'4 la |
sib1 |
