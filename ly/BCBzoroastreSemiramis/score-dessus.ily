\score {
  \new GrandStaff <<
    \new Staff << \global \includeNotes "dessus1" >>
    \new Staff \with { \haraKiriFirst } <<
      \global \includeNotes "dessus2"
      \modVersion {
        s2.*17 s1*30 s2.*27 s1*2 s2.*22 s1*18\break \startHaraKiri
      }
    >>
  >>
  \layout { }
}
