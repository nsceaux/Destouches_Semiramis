\clef "taille"
\setMusic #'rondeau {
  r4 r r re'4 |
  re'2 do'4 sib la re' |
  re'2. r4 r re' |
  re'2 do'4 sib la re' |
  sib2
}
r4 r2*3/2 | R1.*3 | r4 r
\keepWithTag #'() \rondeau
r4 r2*3/2 | R1.*7 | r4 r
\rondeau
r4 r2*3/2 | R1.*8 | r4 r
\rondeau
