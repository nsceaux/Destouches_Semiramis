\setMusic #'rondeau {
  s2. <5/>4 s2 <6+>4 <6> <4> \new FiguredBass <3+> s4*5 <5/>4 s2 <6+>4 <6> <4> \new FiguredBass <3+> s2
}
\keepWithTag #'() \rondeau
\rondeau
s2. <7 _+>4 s2 <6+>4 <6>2 <6+>4 s4*5 <7 _+>4 s2 <6+>4 <6>2 <6+>4
s1. s2 <6>4 <6>2 <6>4 <6 _->2. <6 _->2\figExtOn <6>4 <_+> <_+>\figExtOff <6> <6+>2. <_+>2
\rondeau
s4 <5/>2 <_->4 <_+>2\figExtOn <_+>4\figExtOff <6>2 <6+>4 s4*5 <_->4 <_+>2\figExtOn <_+>4\figExtOff <6>2 <6+>4 <_->2 <6>4 <6>2 <6>4 <6 _->2 <6>4 <5/>2.
s2. <6 4 2>2 <5/>4 s2. <6 4 2>2 <5/>4 s2 <6>4 <7> <_+>2 <_+>
\rondeau
