\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1 s1.*2 s2. \bar "" \break s2. s1.*5\pageBreak
        s1.*5\break s1.*7\break s1.*7\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
