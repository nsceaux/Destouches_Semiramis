\beginMark "Ritournelle" \key sol \minor
\tempo "Legerement" \midiTempo#120
\digitTime\time 3/4
s2.*49
\override Score.BarNumber.break-visibility = #end-of-line-invisible
\override Score.BarNumber.self-alignment-X = #LEFT
s2.
\set Score.currentBarNumber = 1
\set Score.barNumberVisibility = #(every-nth-bar-number-visible 2)
\set Score.barNumberFormatter =
#(lambda (bar-number pos alt-number context)
   #{\markup\concat { "50-" $(number->string bar-number) } #})
s2.*8
\set Score.currentBarNumber = 51
\set Score.barNumberVisibility = #(every-nth-bar-number-visible 1)
\set Score.barNumberFormatter = #robust-bar-number-function
s2.
\set Score.barNumberVisibility = #first-bar-number-invisible-and-no-parenthesized-bar-numbers
\revert Score.BarNumber.self-alignment-X
\revert Score.BarNumber.break-visibility
\tag #'complet {
  s2.*38
  \digitTime\time 2/2 \midiTempo#160 s1*15
  \time 3/2 s1.
  \time 2/2 s1
  \time 3/2 s1.
  \time 2/2 \grace s8 s1
  \time 3/2 s1.*2
  \time 2/2 \grace s8 s1*3
  \time 3/2 s1.*3
  \time 2/2 s1
  \time 3/2 \grace s8 s1.
  \time 2/2 s1*3
  \time 3/2 s1.
  \time 2/2 s1*7
  \digitTime\time 3/4 \grace s8 s2.*4
  \time 2/2 s1*4
  \time 3/2 s1.*2
  \time 2/2 \grace s8 s1*6
  \time 3/2 s1.
  \time 2/2 s1*3
  \time 3/2 s1.
  \time 2/2 s1*4
  \key do \major \time 3/2 s1.
  \time 2/2 s1*2
  \time 3/2 s2 \bar ""
}
