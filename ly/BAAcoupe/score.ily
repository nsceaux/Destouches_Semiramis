\score {
  \new StaffGroupNoBar <<
    \new GrandStaff <<
      \new Staff \with { \haraKiri } <<
        \global
        \keepWithTag #'complet \includeNotes "dessus1"
      >>
      \new Staff \with { \haraKiri } <<
        \global
        \keepWithTag #'complet \includeNotes "dessus2"
      >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'amestris \includeNotes "voix"
      \modVersion { s1*20 \noHaraKiri }
    >> \keepWithTag #'amestris \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'arsane \includeNotes "voix"
      \modVersion { s1*20 \noHaraKiri }
    >> \keepWithTag #'arsane \includeLyrics "paroles"
    \new Staff <<
      \global
      \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*8\break s2.*8\break s2.*5\pageBreak
        s2.*5\break s2.*5\break s2.*5\break s2.*5\pageBreak
        s2.*5\break s2.*5\break \grace s8 s2.*5\break s2.*4\break s2.*4\pageBreak
        s2.*4\break \grace s8 s2.*6\break s2.*6 s2 \bar "" \break s4 s2.*4\break s2.*4 s1\break s1*4\pageBreak
        s1*4\break s1*4\break s1*2 s1.\break s1 s1. s2 \bar "" \break s2 s1.*2\break \grace s8 s1*3\break s1.*2 s2 \bar "" \pageBreak
        s1*2 s1. s1*2\break s1 s1. s1 s2 \bar "" \break s2 s1*2\break s1*3 s2.\break s2.*3 s1\break s1*2 s2 \bar "" \pageBreak
        s2 s1.*2\break \grace s8 s1*3\break \grace s8 s1*3 s1. \break s1*3 s1. \break s1*4 s1. \break 
      }
      \modVersion { s2.*38\pageBreak s2.*23\pageBreak \grace s8 }
    >>
  >>
  \layout { }
  \midi { }
}
