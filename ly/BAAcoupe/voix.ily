<<
  %% Amestris
  \tag #'(amestris basse) {
    <<
      \tag #'basse { s2.*20 \amestrisMark }
      \tag #'amestris {
        \clef "vbas-dessus" R2.*19
        <>^\markup\character Amestris R2.
      }
    >>
    do''4. sib'8 lab' sol' |
    lab'4. sol'8 la'4 |
    sib'4 fa' r8 sib' |
    sol'4.\trill sol'8 mib'' re'' |
    do''4. do''8 do''4 |
    do''8.[ sib'16] sib'4 r8 do'' |
    la'4. la'8 si' do'' |
    re''4. mib''8 do''4 |
    si'4 si' r8 sol''^\markup\italic "Mesuré" |
    mib''4.\trill mib''8 re'' do'' |
    sol''2 re''4 |
    do''4.\trill\prall do''8 re'' la' |
    sib'2 <<
      \tag #'basse { s4 s2.*4 \amestrisMark }
      \tag #'amestris { r4 | R2.*4 }
    >>
    sol''4. fa''8 mib'' re'' |
    mib''4. do''8 mib''4 |
    re'' re'' r8 re'' |
    sol'4 r do''~ |
    do'' r sib'~ |
    sib'4. mib''8 mib'' mib'' |
    \appoggiatura re''8*1/2 do''4 fa'' \appoggiatura mib''8*1/2 re''4 |
    si'\trill si' r8 do'' |
    la'4 si'2 |
    mib''4. re''8 do'' si' |
    re''4.( do''8)\trill si' do'' |
    do''2( si'4) |
    do''2. |
    <<
      \tag #'basse { s2.*8 s2.*16 s4. <>^\markup\italic "Mesuré" \amestrisMark }
      \tag #'amestris { R2.*8 R2.*16 | r4 r8 <>^\markup\italic "Mesuré" }
    >> do''8 re'' mib'' |
    re''4.\trill\prall si'8 do'' re'' |
    \appoggiatura re''8 mib''4 do'' r8 lab' |
    lab'8.[ sol'16] sol'4 sol' |
    do''2 re''8 mib'' |
    re''2\trill re''8 mib'' |
    do''4 sib'4.\trill lab'8 |
    sol'2\trill sol'8 fa' |
    lab'4. sib'8 sol'4 |
    fa'2\trill sib'8 lab' |
    sol'4.\trill fa'8 mib'4 |
    sib'2 r8 mib'' |
    do''4. sib'8 lab'16[ sol'] fa'[ mib'] |
    sol'4( fa'2)\trill |
    mib'2 sol'8 la' |
    \appoggiatura la'8 sib'2 sib'8 do'' |
    la'2 si'8 do'' |
    re''4. mib''8 do''4 |
    si'2\trill re''8 mib'' |
    fa''4. mib''8 re''4 |
    sol''2 r8 fa'' |
    re''4.\trill si'8 do'' re'' |
    mib''4( re''2\trill) |
    do''2 <<
      \tag #'basse {
        s2 s1*14 s1. s1 s1. s1 s1.*2 s1*3 s1.*2
        s2 \amestrisMarkText "Gravement"
      }
      \tag #'amestris {
        r2 | R1*14 R1. R1 R1. R1 R1.*2 R1*3 R1.*2 |
        r2 <>^\markup\italic "Gravement"
      }
    >> r2 do''4 do'' |
    reb''2 reb''4 reb'' |
    \appoggiatura sib'8 la'2 fa''4 mib''8 reb'' do''4.\trill reb''8 |
    sib'4 <<
      \tag #'basse { s2. s8. \amestrisMark }
      \tag #'amestris { r4 r2 | r8 r16 }
    >> do''16 re''8 mib'' re''4.\trill mib''8 |
    mib''2 <<
      \tag #'basse { s2 s1. s2 \amestrisMark }
      \tag #'amestris { r2 | R1. | r2 }
    >> fa''4 fa''16 mib'' re'' do'' |
    sib'4 sib'8 do'' lab'4\trill\prall lab'8 sol' |
    sol'2\trill sol'8 sol'16 sol' lab'8 sib' |
    fa'4 sib'8 sib' mib''4 mib''8 fa'' |
    re''4\trill re'' <<
      \tag #'basse { s2 s \amestrisMark }
      \tag #'amestris { r2 | r2 }
    >> r4 mib'' |
    \appoggiatura re''8 do''4 do''8 reb'' sib'4\trill lab'8 sol' |
    \appoggiatura sol'8 lab'4 lab' do''8 re'' |
    mib''4. mib''8 fa''4 |
    sol''2 r8 sol' |
    la'4. si'8 do'' re'' |
    si'4 <<
      \tag #'basse { s2. s1*3 s2 \amestrisMark }
      \tag #'amestris { r4 r2 | R1*3 | r2 }
    >> sib'4. do''8 la'4 lab'8 sib' |
    \appoggiatura lab'8 sol'4. sol'8 sol'4. do''8 si'4. do''8 |
    \appoggiatura do''8 re''2 r |
    r4 sib'8 lab' sol'4\trill fa'8 mib' |
    sib'4. sib'8 mib'' mib'' mib'' sib' |
    \appoggiatura sib'8 do''4 do'' lab'8 lab'16 sib' do''8 re'' |
    mi''4 mi''4 r do''8 do'' |
    fa''2 fa''4. fa''8 |
    fa''2. r8 mib'' re'' do'' sib' lab' |
    sol'2\trill fa'8 fa'16 fa' sol'8 lab' |
    sol'4\trill sol' r8 mib'' fa'' sol'' |
    do''2. lab''4 |
    si'2 do''4 do''8 do'' re''4. mib''8 |
    re''2\trill mib''4. mib''8 |
    mib''2 re''4. re''8 |
    sol'2 la'4 si' |
    do'' re''8 mib'' re''2\trill |
    do''2
    \tag #'amestris { r2 r | R1*2 | r2 }
  }
  %% Arsane
  \tag #'(arsane basse) {
    \clef "vhaute-contre" R2.*19 |
    <>^\markup\character Arsane
    sol'4. fa'8 mib' re' |
    <<
      \tag #'basse { s2.*12 s2 \arsaneMark }
      \tag #'arsane {
        mib'4. re'8 mi'4 |
        fa' do' r8 fa' |
        re'4.\trill re'8 re' re' |
        mib'4. fa'8 sol'4 |
        mib'4 do' fa'~ |
        fa' r mib'~ |
        mib' r8 re' re' mib' |
        fa'4. sol'8 mib'4 |
        re' re' r |
        R2.*3 |
        r4 r
      }
    >> mib'4 |
    mib'4.( re'8) re' re' |
    sol'4. fa'8 mi'4 |
    fad'4. re'8 re' mib'! |
    do'4.\trill si8 do' re' |
    <<
      \tag #'basse { s2.*13 <>^\markup\italic Mesuré \arsaneMark }
      \tag #'arsane {
        si4 si r |
        R2. |
        sol'4. fa'8 mib' re' |
        mib'4. mib'8 lab'4 |
        fa'4 fa' r8 sib' |
        sol'4.\trill sol'8 sol' sol' |
        \appoggiatura sol'16 lab'4 lab' \appoggiatura sol'16 fa'4 |
        re' re' mib'~ |
        mib' re'2 |
        sol'4. fa'8 mib' re' |
        fa'4.( mib'8)\trill re' mib' |
        mib'4( re'2)\trill |
        do'2. |
      }
    >>
    R2.*8 | <>^\markup\italic Mesuré
    do'4 do'8 re' mib' re'16[ do'] |
    \appoggiatura do' re'2 mib'8 sol' |
    sol'4.( fa'8)\trill mib' fa' |
    \appoggiatura fa'8 sol'2 re'8 fa' |
    mib'4. mib'8 do'4 |
    la2\trill~ la8 la |
    re'4. mib'8 re' do' |
    si4\trill si re'8 re' |
    mib' re' do'4 fa'8 sol' |
    re'4\trill sib8 re' mib'4 |
    fa'8 sol' fa'4\trill\prall mib'8 re' |
    \appoggiatura re'16 mib'2 re'8 re' |
    sol' lab' mi'4\trill fa'8 sol' |
    \appoggiatura sol' lab'4 fa' re' |
    fa'2 sol'8 re' |
    \appoggiatura re'8 mib'8.[ fa'16] re'4.\trill do'8 |
    do'4. <<
      \tag #'basse { s4. s2.*22 s2 \arsaneMarkText "Vivement" }
      \tag #'arsane { r8 r4 | R2.*22 | r2 <>^\markup\italic Vivement }
    >> sol'4 do' |
    la re'8 mib' re'4 do' |
    si2\trill re'4 mib'8 fa' |
    fa'4( mib'8)\trill re' \appoggiatura re'16 mib'4 fa' |
    sol'4. fa'8 lab'4. sol'8 |
    fa'2\trill mib'4 r8 re' |
    re'4.\trill\prall si8 do'4 re' |
    \appoggiatura re'16 mib'4 mib'8 re' do'4 re'8 mib' |
    re'4 sib mib' sol' |
    do' do'8 sib la4 re' |
    sol2 sol'4 do' |
    la4 re'8 mib' re'4 do' |
    si2\trill fa'4( mib'8\trill[ re']) |
    re'2\trill sol'4 do' |
    la4 re'8 mib' si4.\trill do'8 |
    do'2 re'4 r re'8 mib' fa' sol' |
    do'4. do'8 fa' mib' mib' re' |
    re'4\trill re' r re'8 re' mib'4 re'8 mib' |
    \appoggiatura re'8 do'4 do'8 do' lab'4 lab'8 fa' |
    re'4\trill re'8 sib do'4 re'8 mib' mib'4 mib'8 re' |
    mib'4 mib'8 fa' sol'2 re'4 mi' |
    \appoggiatura mi'?8 fa'4 do'8 re' mib'?4 mib'8 re'16[ do'] |
    \appoggiatura do'16 re'4 re' mib' re' |
    do'2 si4. do'8 |
    sol2 mib'4 re'8 do' fa'4. sol'8 |
    re'2\trill sol'4 fa'8 mib' re' mi' fa' sol' |
    mi'4\trill mi' <<
      \tag #'basse { s1 s1 s1. s4 \arsaneMarkText "Vivement" }
      \tag #'arsane { r2 r | R1 R1. | r4 ^\markup\italic Vivement }
    >> re'8 re' mib'4 fa'8 sol' |
    <<
      \tag #'basse { do'8. s16 s2. | s2 \arsaneMark }
      \tag #'arsane { do'4 r r2 | r2 }
    >> sol'4 sol'8 sol' |
    si4. re'8 mib'4 sol' do'8 sib? sib do' |
    la4\trill la <<
      \tag #'basse { s2 s1*3 s2 \arsaneMark }
      \tag #'arsane { r2 | R1*3 | r2 }
    >> re'4 re'8 re' |
    sol'2 <<
      \tag #'basse { s2 s1 s2.*4 s4 \arsaneMarkText Vivement }
      \tag #'arsane { r2 | R1 R2.*4 | r4 <>^\markup\italic Vivement }
    >> r8 sol' fa'\trill mib' re' mi' |
    fa' fa' fa' sol' lab'4 fa' |
    re'4\trill re' sol'8 sol'16 fa' mib'8 re' |
    do' do' re' mib' re'4.\trill mib'8 |
    mib'2 <<
      \tag #'basse {
        s1 s1. s1*6 s1. s1*3 s1. s1*4 s2 \arsaneMarkText "Vivement"
      }
      \tag #'arsane {
        r2 r | R1. R1*6 R1. R1*3 R1. R1*4 |
        r2 ^\markup\italic Vivement
      }
    >> mi'4. fa'8 sol'4 sol'8 sol' |
    re' re' re' re' sol'4. sol'8 |
    do'4. do'8 do' re' mi' fad' |
    sol'2
  }
>>