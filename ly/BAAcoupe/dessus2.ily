\clef "dessus" R2. |
do''4. sib'8 lab' sol' |
lab'4. sol'8 la'4 |
sib'4 fa'4. sib'8 |
sol'4\trill mib''4. re''8 |
do''8. re''16 re''4.\trill do''8 |
si'2.\trill |
sol''4. fa''8 mib''\trill re'' |
mib''4. re''8 mi''4 |
fa''4 do''4. fa''8 |
re''4\trill re''4. re''8 |
mib''4 mib''8 fa'' mib'' re'' |
do''4\trill fa''8( mib'') re''( do'') |
si'4 mib''2~ |
mib''4 re''2~ |
re'' do''8\trill si' |
mib''4. re''8 do''\trill si' |
do''4. sib'8 la'4 |
re'' si'4.\trill do''8 |
do''2 r4 |
R2.*29 |
r4 mib''8 fa'' mib'' re'' |
do''4\trill fa''8( mib'') re''( do'') |
si'4 mib''2~ |
mib''4 re''2~ |
re'' do''8\trill si' |
mib''4. re''8 do''\trill si' |
do''4. sib'8 la'4 |
re''4 si'4.\trill do''8 |
do''2. |
<<
  \tag #'complet {
    R2.*39 R1*15 R1. R1 R1. R1 R1.*2 R1*3 R1.*3 R1 R1. R1*3 R1. R1*7
    R2.*4 R1*4 R1.*2 R1*6 R1. R1*3 R1. R1*4 R1. R1*2 |
  }
  \tag #'part {
    R2.*107
  }
>>
