\clef "dessus" sol''4. fa''8 mib''\trill re'' |
mib''4. re''8 mi''4 |
fa'' do''4. fa''8 |
re''4\trill sib''2~ |
sib''4. lab''8 sol''\trill fa'' |
mib''8. fa''16 fa''4.\trill mib''8 |
re''2.\trill |
R2. |
do'''4. sib''8 lab''\trill sol'' |
lab''4. sol''8 la''4 |
sib''4 fa''4. sib''8 |
sol''4.\trill sol''8 sol'' sol'' |
lab''4~ lab''8( sol'') fa''( mib'') |
re''4 sol''2~ |
sol''4 fa''2~ |
fa'' mib''8\trill re'' |
sol''4. fa''8 mib''\trill re'' |
mib''4. re''8 do''4 |
fa''4 re''4.\trill do''8 |
do''2 r4 |
R2.*29 |
r4 r8 sol'' sol'' sol'' |
lab''4~ lab''8( sol'') fa''( mib'') |
re''4 sol''2~ |
sol''4 fa''2~ |
fa'' mib''8\trill re'' |
sol''4. fa''8 mib''\trill re'' |
mib''4. re''8 do''4 |
fa''4 re''4.\trill do''8 |
do''2. |
<<
  \tag #'complet {
    R2.*39 R1*15 R1. R1 R1. R1 R1.*2 R1*3 R1.*3 R1 R1. R1*3 R1. R1*7
    R2.*4 R1*4 R1.*2 R1*6 R1. R1*3 R1. R1*4 R1. R1*2
  }
  \tag #'part {
    R2.*107
  }
>>
