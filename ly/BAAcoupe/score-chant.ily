\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'amestris \includeNotes "voix"
      \modVersion { s1*20 \noHaraKiri }
    >> \keepWithTag #'amestris \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'arsane \includeNotes "voix"
      \modVersion { s1*20 \noHaraKiri }
    >> \keepWithTag #'arsane \includeLyrics "paroles"
    \new Staff <<
      \global
      \includeNotes "basse"
      \includeFigures "chiffres"
    >>
  >>
  \layout { }
}
