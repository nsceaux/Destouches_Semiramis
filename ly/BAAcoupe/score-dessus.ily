\score {
  \new GrandStaff <<
    \new Staff <<
      \keepWithTag #'part \global
      \keepWithTag #'part \includeNotes "dessus1"
    >>
    \new Staff <<
      \keepWithTag #'part \global
      \keepWithTag #'part \includeNotes "dessus2"
    >>
  >>
  \layout { }
}
