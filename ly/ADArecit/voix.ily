<<
  %% Amestris
  \tag #'(amestris basse) {
    <<
      \tag #'basse { s4 s2.*3 s2 \amestrisMark } 
      \tag #'amestris { \clef "vbas-dessus" r4 | R2.*3 | r4 r }
    >> sol'8 la' |
    si'2 si'8 sol' |
    do''2 re''8 mib'' |
    re''2\trill\prall do''8 re'' |
    \appoggiatura re''8 mib''4 <<
      \tag #'basse { s2. s1 s2 \amestrisMark }
      \tag #'amestris { r4 r2 | R1 | r4 r }
    >> sib'8 sib' |
    mib''2 mib''8 sib' |
    \appoggiatura sib'8 do''2 sib'4 sib'8 do'' lab'4\trill( sol'8) lab' |
    sol'4 <<
      \tag #'basse { s2. s1 s2 \amestrisMark }
      \tag #'amestris { r4 r2 | R1 | r4 r }
    >> r8 sib' |
    sib'4. sib'8 do'' sol' |
    \appoggiatura sol'8 lab'2 do''8 lab' |
    fa'4 fa' sib' |
    sol' mib' sib'8 sib' |
    si'2 do''8 re'' |
    \appoggiatura re''8 mib''4. do''8 sib'4 |
    la'2\trill la'8 sol' |
    re''2 la'4 |
    do''4. sib'8 la' sib' |
    fad'2 la'4 |
    la'8 sib' la'4.\trill sol'8 |
    sol'2 <<
      \tag #'basse { s4 s2.*22 s4. \amestrisMark }
      \tag #'amestris { r4 | R2.*22 | r4 r8 }
    >> sol'8 sol' lab' |
    lab'2 <<
      \tag #'basse { s4 s2. s4. \amestrisMark }
      \tag #'amestris { r4 | R2. | r4 r8 }
    >> sol'8 sol' lab' |
    lab'2 sol'8 sol' |
    do''2 re''8 mib'' |
    si'4. <<
      \tag #'basse { s4. s1 s2. \amestrisMark }
      \tag #'amestris { r8 r4 | R1 | r2 r4 }
    >> do''4 |
    lab'2 do''4 re''8 mib'' mi''4.\trill fa''8 |
    fa''2 fa''4 <<
      \tag #'basse { s2 \amestrisMark }
      \tag #'amestris { r4 | r4 }
    >> mib''8 re'' do''4 do''8 sib' |
    la'4\trill la'8 si' do''4 do''8 si' |
    \appoggiatura si'8 do''2 do''4 r |
  }
  %% Arsane
  \tag #'(arsane basse) {
    \arsaneMark do'8 sol' |
    mib'2 fa'8 sol' |
    re'2\trill sol'4 |
    sol'4( fa'8)\trill mib' re' do' |
    si4\trill si <<
      \tag #'basse { s4 | s2.*3 | s4 \arsaneMark }
      \tag #'arsane { r4 | R2.*3 | r4 }
    >> sol'8 fa' mi'4 mi'8 do' |
    fa'4 fa'8 mib' re'4\trill re'8 fa' |
    sib4 sib <<
      \tag #'basse { s4 s2. s1. s4 \arsaneMark }
      \tag #'arsane { r4 | R2. R1. | r4 }
    >> sol' do'8\trill do' do' re' |
    \appoggiatura re'8 mib'4 fa'8 sol' fa'4\trill mib'8 fa' |
    \appoggiatura fa' sol'2 <<
      \tag #'basse { s4 s2.*11 s2 \arsaneMark }
      \tag #'arsane { r4 | R2.*11 | r4 r }
    >> sol'4 |
    sol'4. fa'8 fa' fa' |
    fa'2 sol'4 |
    mib'4.\trill mib'8 mib' re' |
    re'4\trill re' mib' |
    fa'8 sol' re'4.\trill\prall mib'8 |
    si2\trill re'8 re' |
    \appoggiatura re'8 mib'4 mi'\trill( re'8) mi' |
    \appoggiatura mi'? fa'2 lab'8 sol' |
    fa'2\trill\prall fa'8 fa' |
    re'2\trill mib'8 re' |
    do'4. sib8 lab4 |
    sol4\trill sol do'8 do' |
    re' mib' re'4\trill\prall do'8 re' |
    \appoggiatura re'8 mib'2 sol'4 |
    sol'4( fa'8)\trill mib' re' do' |
    fa'2 re'8 re' |
    mib'2 fa'8 sol' |
    do'2 lab'8 sol' |
    fa'2\trill mib'8 re' |
    sol'2 re'4 |
    mib'8 fa' mib'4.\trill re'8 |
    mib'4( re'2)\trill |
    do'4. <<
      \tag #'basse { s4. | s2 \arsaneMark }
      \tag #'arsane { r8 r4 | r4 r }
    >> do'4 |
    fa' sol'8 sol'16 fa' mib'8\trill re' |
    \appoggiatura re'8 mib'4. <<
      \tag #'basse { s4. s2.*2 s4. \arsaneMark }
      \tag #'arsane { s8 s4 | R2.*2 | r4 r8 }
    >> re'8 re' sol' |
    mi'4 mi'8 fa' sol'4 sol'8 sol' |
    do'2 do'4 <<
      \tag #'basse { s4 s1. s2. \arsaneMark }
      \tag #'arsane { r4 | R1. | r2 r4 }
    >> fa'8 lab' |
    re'4 \tag #'arsane { r2 r4 | R1*2 }
  }
>>