\tag #'(arsane basse) {
  Vous, Prin -- cesse, en ces lieux ! quel sort vous y ra -- me -- ne ?
}
\tag #'(amestris basse) {
  J’y viens ê -- tre té -- moin du beau jour qui vous luit.
}
\tag #'(arsane basse) {
  Que l’é -- clat de ce jour & me trouble & me gê -- ne !
}
\tag #'(amestris basse) {
  De vos no -- bles tra -- vaux vous re -- ce -- vez le fruit.
}
\tag #'(arsane basse) {
  In -- grate, i -- gno -- rez- vous quel ef -- fort m’y re -- duit ?
}
\tag #'(amestris basse) {
  L’Hy -- men qui d’u -- ne main vous pré -- pa -- re sa chaî -- ne,
  Vous pré -- sen -- te de l’autre un Em -- pire é -- cla -- tant
  Com -- blé de tant d’hon -- neurs, n’ê -- tes- vous pas con -- tent ?
}
\tag #'(arsane basse) {
  Mon cœur ne l’eût é -- té qu’à vain -- cre vô -- tre hai -- ne.
  Je n’a -- do -- rois que vous ; tant de soins, tant de pleurs,
  De si ten -- dres sou -- pirs, une ar -- deur si sin -- ce -- re,
  Rien n’a pû flé -- chir vos ri -- gueurs :
  C’est vous qui m’en -- chaî -- nez à ces tris -- tes hon -- neurs,
  Et je vais me pu -- nir de n’a -- voir pu vous plai -- re.
}
\tag #'(amestris basse) {
  A -- dieu, Sei -- gneur...
}
\tag #'(arsane basse) {
  Mon cœur plus que ja -- mais é -- pris...
}
\tag #'(amestris basse) {
  A -- dieu, Sei -- gneur, ou -- bli -- ez A -- mes -- tris.
}
\tag #'(arsane basse) {
  Vous me fuy -- ez... ar -- rê -- tez In -- hu -- mai -- ne.
}
\tag #'(amestris basse) {
  Non, non, d’au -- tres des -- tins m’ap -- pel -- lent.
}
\tag #'(arsane basse) {
  Quel mé -- pris.
}
\tag #'(amestris basse) {
  Je n’é -- cou -- te plus rien, je vais sui -- vre la Rei -- ne.
}
