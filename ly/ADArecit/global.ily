\key sol \minor \midiTempo#120 \tempo "Vivement"
\digitTime\time 3/4 \partial 4 s4 s2.*7
\time 2/2 \grace s8 s4 \tempo "Vivement" s2. s1
\digitTime\time 3/4 s2.*2
\time 3/2 \grace s8 s1.
\time 2/2 s1*2
\digitTime\time 3/4 \grace s8 s2.*12 s2 \tempo "Vivement" s4 s2.*29
\time 2/2 s1*2
\time 3/2 s1.
\time 2/2 s1*4
