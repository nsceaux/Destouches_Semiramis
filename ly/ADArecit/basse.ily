\clef "basse" do4\repeatTie |
do'2. |
si2 sol4 |
lab2. |
sol |
fa |
mib2 fa4 |
sol2 sol,4 |
do2 do'4 sib |
la2 sib4 lab |
sol2.~ |
sol |
lab2 sol fa |
mib lab4 fa |
do'4. sib8 lab2 |
sol2. |
mi |
fa |
sib2 sib,4 |
mib2 mib4 |
re sol8 fa mib re |
do2 re4 |
mib2. |
re~ |
re~ |
re2 do4 |
do re re, |
sol,2 sol4 |
la2 la4 |
si2 sol4 |
do'2 do4 |
sol2 do'8 sib |
lab sol fa2 |
<>^"notes égales" sol8( fa) sol( la) si( sol) |
do'4 sol8( lab) sib( sol) |
lab( sib) lab( sol) fa( sol) |
la!( sib) la( sol) la( fa) |
sib( do') sib( lab) sol( mib) |
lab4 sol fa |
mib8( fa) sol( mib) lab( sol) |
fa mib sib4 sib, |
mib8( fa) mib( re) mib( do) |
fa4~ fa8( sol8) fa( mib) |
re( do) si,( la,) si,( sol,) |
do4 do' sol |
lab8( sib) lab( sol) fa( sol) |
lab2. |
mib8( re) mib( fa) sol4 |
do8( si,) do( sol,) lab,( fa,) |
sol,2. |
do |
fa,2 fa4 |
si,2. |
do |
sib, |
lab, |
sol,2 sol4 |
do'2 sib |
lab1~ |
lab sol2 |
fa1 |
sol4 do8 re mib4 mi |
fa2 mib8 fa sol sol, |
do2~ do4.*5/6 do16 sib, la, |
