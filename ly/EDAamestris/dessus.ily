\clef "dessus"
\twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''1 lab''4( sol'') |
    fa''1 sol''4( fa'') |
    mib''1 fa''4( mib'') |
    re''4( mib''8) fa'' mib''2.\trill re''4 |
    mib''2. re''4 do''2 | }
  { mib''1 fa''4( mib'') |
    re''1 mib''4( re'') |
    do''1 re''4( do'') |
    si'4( do''8) re'' do''2.\trill si'4 |
    do''1 sol'2 | }
>>
do''2 si'1 |
do'' lab'2 |
sol'1 do''4 si' |
do''1 mib'4 fa' |
re'1 mib'4 re' |
do'1 fa'2 |
sol' r r |
mib' r r |
mib' r << { fa'4 sol'4 | fa'1 }
  \\ \new CueVoice { \voiceTwo fa'4_\markup\whiteout "[manuscrit]" mib'4 | re'1 }
>> r2 |
re''1 fa'4 re' |
mib'1 mib'2 |
mib' re'2.(\trill do'8) re' |
mib'1 r2 |
sol'1 r2 |
sol'1 mi'2 |
fa' sol'1 |
do'1. |
fa'1. |
sib1 mib'2 |
lab' sol' sol' |
sol'2.
