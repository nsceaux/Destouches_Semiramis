\clef "haute-contre" do''1. |
sib'1. |
do''1. |
sol'1. |
sol'1. |
lab'2 sol'1 |
mib'1 fa'2 |
re'1 mib'4 fa' |
sol'1. |
re'1. |
fa'1. |
re'2 r r |
mib' r r |
lab r do' |
re'1 r2 |
sib1. |
sib |
do'1 sib2 |
sib1 r2 |
do'1 r2 |
do'1. |
do'1. |
do'1. |
sib |
sib |
fa'2 re' re' |
mib'2.
