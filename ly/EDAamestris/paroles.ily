Peu -- ples, qui de Ni -- nus ho -- no -- rez la me -- moi -- re,
Je viens con -- som -- mer ses bien -- faits.
Tous ses jours ont cou -- lé pour vous com -- bler de gloi -- re,
Le der -- nier de mes jours va vous don -- ner la paix.

O Ciel, O Ciel, dé -- fend mon cœur d’un sou -- ve -- nir trop ten -- dre !
Mâ -- nes de mes a -- yeux, fai -- tes place à ma cen -- dre.
