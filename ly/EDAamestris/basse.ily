\clef "basse" do'1 lab2 |
sib4( lab) sol1 |
lab4( sol) fa1 |
sol2 sol, sol, |
do1. |
fa2 sol1 |
lab fa2 |
sol2. fa4 mib re |
do1 r2 |
sib,1 r2 |
lab,1 r2 |
sol,2 sol4 fa mib2 |
sol,1 r2 |
lab,1. |
sib,1 sib2 |
lab1. |
sol |
fa |
mib1 r2 |
do1 do'2 |
sib1. |
lab2 mi1 |
fa1. |
re1. |
mib1 r2 |
fa sol sol, |
<<
  \tag #'basse { do2. }
  \tag #'basse-continue { do2.*11/12~ \hideNotes do16 }
>>
