\amestrisMark R1.*4 |
r2 r mib'' |
mib''4 r re''2 re''4 mib'' |
do''1 re''4 mib'' |
si'1\prall do''4 re'' |
\appoggiatura re''8 mib''2 mib'' r4 lab' |
lab'1 sol'4 sol' |
re''2. mib''4 do''2 |
si'1\trill sol'4 lab' |
sib'1 sib'4 mib'' |
\appoggiatura re''8 do''2 do''4 do''8 sib' lab'4 sol' |
fa'2\trill fa' re''4 mib'' |
fa''1 re''4 fa'' |
sib'1 sib'4 sib'8 do'' |
lab'1( sol'4) lab' |
sol'2\trill r sol' |
mib''1 mib''2 |
\appoggiatura fa''8 mi''2. mi''4 fa'' sol'' |
do''2 do''4 do''8 sib' sib'4 do'' |
la'1\trill la'2 |
lab'2 lab'4 lab' lab' sib' |
\appoggiatura lab'8 sol'1 do''4 do'' |
do''1\trill do''4 si' |
\appoggiatura si'?8 do''2 do''4
