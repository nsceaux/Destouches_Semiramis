\clef "taille" mib'1. |
fa'1.  |
lab'1. |
re' |
do' |
do'2 sol'1 |
do'1 do'2 |
sol1 sol2 |
sol1. |
sib |
do' |
sol2 r r |
sol r r |
mib r sol |
sol1 r2 |
fa1. |
sol |
lab |
mib1 r2 |
sol1 r2 |
sol'1. |
fa'2 do'1 |
do'1. |
fa1. |
sol |
do'2 sol re' |
re'2.
