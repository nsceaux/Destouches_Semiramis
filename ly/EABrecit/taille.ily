\clef "taille" R1*2 R1.*2 R1 R2. R1*7 R1.*8 R1 R1.*2 R1 R1. R2.*3 R1*6
R1. R1 |
r4 r sol'\douxSug re' re'4. la8 |
sib4 sib sib sol2 sol'4 |
sol'2 sol'4 fa' do' do' |
sib fa' fa' re'2 re'8 re' |
re'2 re'4 re' re'4. sol'8 |
sol'2 sol'4 la'2 la'8 sol' |
fa'4 do' fa' fa'4. sib'8 la'4 |
fad'2 fad'4 sol' sol' sol' |
re' re' re' re'4. mib'8 re'4 |
sib2 fad'4 sol' sol' sol' |
re' re' re' re'4. mib'8 re'4 |
sib2 r4 r2*3/2 |
R1.*9 |
R1*2 R1.*11 R1*5 R1. R1*9 R1. R1 R1. R1 R1.*2 R1*2 R1. R1*2 R1. R1*37 |
r2 r4

