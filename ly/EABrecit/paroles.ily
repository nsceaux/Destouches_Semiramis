\tag #'(zoroastre basse) {
  Quoy ! la mort d’A -- mes -- tris s’a -- prê -- te dans ces lieux,
  Et ce Tom -- beau sa -- cré va re -- ce -- voir sa cen -- dre ?
}
\tag #'(semiramis basse) {
  Tran -- quil -- le sur son sort, elle or -- don -- ne les Jeux,
  Les fu -- ne -- bres hon -- neurs, qu’à Ni -- nus on va ren -- dre ;
  C’est son der -- nier hom -- mage à ce Roi glo -- ri -- eux.
}
\tag #'(zoroastre basse) {
  Sou -- tien -- drez- vous ce spec -- tacle o -- di -- eux ?
}
\tag #'(semiramis basse) {
  Con -- tre l’Ar -- rêt du Ciel qui pour -- roit la dé -- fen -- dre ?
}
\tag #'(zoroastre basse) {
  Vous, si vos yeux s’ou -- vroient aux mal -- heurs que je crains :
  Je de -- vrois vous ha -- ïr, mal -- gré moi je vous plains.
  Voi -- ci ce mo -- ment re -- dou -- ta -- ble ;
  Le des -- tin d’A -- mes -- tris at -- ten -- drit tous les cœurs.
  Sa mort trou -- ve -- ra des van -- geurs,
  Croy -- ez- en mon ef -- froi, le trou -- ble qui m’ac -- ca -- ble,
  Triste & der -- nier ef -- fort d’un a -- mour dé -- plo -- ra -- ble.
}
\tag #'(semiramis basse) {
  Re -- dou -- tez moins un Peuple es -- cla -- ve de mes vœux :
  Ba -- by -- lo -- ne crain -- dra mon cou -- rage & nos Dieux.
}
\tag #'(zoroastre basse) {
  Est- ce donc sur vos Dieux que vôtre es -- poir se fon -- de ?
  U -- ne nuit pro -- fon -- de
  Vous ca -- che leurs coups.
  Des Maî -- tres du mon -- de
  Le Ciel est ja -- loux :
  Où fuir son cou -- roux ?
  Sa hai -- ne fe -- con -- de
  Nous fait suc -- com -- ber :
  La fou -- dre qui gron -- de
  Est prête à tom -- ber.
  La fou -- dre qui gron -- de
  Est prête à tom -- ber.
}
\tag #'(semiramis basse) {
  Tout ac -- croit ma ra -- ge,
  J’i -- mi -- te les Dieux.
  Pe -- risse à mes yeux
  L’Ob -- jet qui m’ou -- tra -- ge ;
  Que mes En -- ne -- mis
  Soient re -- duits en pou -- dre :
  Un coup de la fou -- dre
  M’est cher à ce prix.
  Un coup de la fou -- dre
  M’est cher à ce prix.
  Que dis- je ? d’A -- mes -- tris le sort me fait en -- vi -- e.
  Ar -- sa -- ne la plain -- dra : Dieux, quels trou -- bles se -- crets !
  In -- grat, don -- ne- moi tes re -- grets :
  J’a -- che -- te -- rois tes pleurs, aux dé -- pens de ma vi -- e.
}
\tag #'(zoroastre basse) {
  Il est tems d’é -- touf -- fer de cou -- pa -- bles a -- mours.
  Con -- nois -- sez- vous Ar -- sa -- ne ?
}
\tag #'(semiramis basse) {
  Où ten -- dent ces dis -- cours ?
}
\tag #'(zoroastre basse) {
  Je vais le dé -- cou -- vrir ce fu -- nes -- te mys -- te -- re.
  Vos feux ont fait pâ -- lir l’as -- tre qui nous é -- clai -- re,
  La terre en a trem -- blé, moi- mê -- me j’en fre -- mis.
}
\tag #'(semiramis basse) {
  Quel coup vient me frap -- per !
}
\tag #'(zoroastre basse) {
  Ar -- sane est vô -- tre Fils.
}
\tag #'(semiramis basse) {
  Mon Fils... Et j’ai brû -- lé d’u -- ne flâ -- me si noi -- re ?
  Qui vous l’a ré -- vé -- lé ?...
}
\tag #'(zoroastre basse) {
  Les En -- fers.
}
\tag #'(semiramis basse) {
  Lui, mon Fils !...
  Non, Bar -- ba -- re, je vois ce que tu t’es pro -- mis :
  Tu le sou -- hai -- tes trop pour me le fai -- re croi -- re.
}
\tag #'(zoroastre basse) {
  Vous con -- noî -- trez l’er -- reur dont vos sens sont sur -- pris.
}
\tag #'semiramis {
  Bri -- sez, bri -- sez les nœuds d’u -- ne fa -- ta -- le chaî -- ne.
  Non, non, non,
  Non, non, votre es -- pe -- rance est vai -- ne,
  Je crains peu des mal -- heurs qui m’ar -- ra -- chent à vous,
  Non, non,
  Non, non, non, non, non, non, votre es -- pe -- rance est vai -- ne,
  J’at -- tens ses coups.
  J’at -- tens ses coups.
  Le Ciel gron -- de, j’at -- tens ses coups.
  J’at -- tens ses coups.
}
\tag #'zoroastre {
  Bri -- sez, bri -- sez les nœuds d’u -- ne fa -- ta -- le chaî -- ne.
  Trem -- blez, trem -- blez,
  Trem -- blez, votre es -- pe -- rance est vai -- ne,
  Je pré -- vois des mal -- heurs qui me van -- gent de vous,
  Trem -- blez,
  Trem -- blez, trem -- blez, votre es -- pe -- rance est vai -- ne,
  Le Ciel gron -- de,
  Le Ciel gron -- de,
  Le Ciel gron -- de, crai -- gnez ses coups.
  Crai -- gnez ses coups.
}
\tag #'(zoroastre basse) {
  On vient... je ne puis voir cet -- te Fête in -- hu -- mai -- ne.
}
