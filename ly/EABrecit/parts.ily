\piecePartSpecs
#(let ((indications
        #{
s1*2 s1.*2 s1 s2. s1*7 s1.*8 s1 s1.*2 s1 s1. s2.*3 s1*6
s1. s1 |
s4 s_\markup\right-align\italic\line { Que vôtre espoir se fonde ? }
s1 s1.*20 s1*2 s1.*11 s1*5 s1. s1*9 s1. s1 s1. s1 s1.*2 s1*2 s1. s1*2 s1. s1
s4_\markup\right-align\italic\right-column {
  Vous connoîtrez l’erreur dont vos sens sont surpris }
     #})
        (indications2
         #{
s1*2 s1.*2 s1 s2. s1*7 s1.*8 s1 s1.*2 s1 s1. s2.*3 s1*6
s1. s1 |
s4 s_\markup\right-align\italic\line { Que vôtre espoir se fonde ? }
     #}))
        `((dessus #:music ,indications)
          (dessus-hc #:music ,indications)
          (haute-contre #:music ,indications2)
          (taille #:music ,indications2)
          (basse #:music ,indications2)
          (basse-continue #:score-template "score-basse-continue-voix")
          (chant)
          (timbales #:on-the-fly-music , #{
\clef "basse" \once\override Staff.TimeSignature.stencil = ##f
\key re \minor R1*127 \bar "|."
\time 2/2 \key do \major
do2 do |
do4. do16 do do4 sol, |
do4. do16 do do4 sol, |
do4. do16 do do4 do |
sol, do sol,4. do8 |
do1 | #})
(silence #:on-the-fly-markup , #{ \markup\tacet#136 #})))
