<<
  %% Semiramis
  \tag #'(semiramis basse) {
    <<
      \tag #'basse { s1*2 s1.*2 s2. \semiramisMark }
      \tag #'semiramis { \semiramisMark R1*2 R1.*2 r2 r4 }
    >> la'4 |
    la'4. la'8 la' do'' |
    \appoggiatura do''8 sib'4 sol'8 sol' do''4 do''8 re'' |
    \appoggiatura re''8 mib''4 do''8 do'' sol'4 sol'8 la' |
    \appoggiatura la'8 sib'4 sib'8 sib' do''4 re''8 mib'' |
    do''4\trill do'' la'8 la'16 la' sib'8 do'' |
    fa'4 sol'8 la' la'4\trill la'8 sib' |
    sib'4. <<
      \tag #'basse { s8 s2 s1 s2 \semiramisMark }
      \tag #'semiramis { r8 r2 R1 r2 }
    >> la'4 la'8 la' mi''4. mi''8 |
    mi''2. re''8 mi'' fa''4 mi''8 re'' |
    dod''4 dod'' <<
      \tag #'basse { s1 s1.*5 s1 s1.*2 s1 s1. s2.*3 s4. \semiramisMark }
      \tag #'semiramis { r2 r R1.*5 R1 R1.*2 R1 R1. R2.*3 r4 r8 }
    >> sol'8 sol' la' sib' do'' |
    re''4 mib'' fa''8 fa'' fa'' fa'' |
    sib'4 sib'8 sib' mib''4 mib''8 do'' |
    la'4 la'8 sib' sib'4\trill sib'8 la' |
    sib'2 <<
      \tag #'basse { s2 s1 s1. s1 s1.*11 s2 \semiramisMark }
      \tag #'semiramis { r2 R1 R1. R1 R1.*11 r2 }
    >> re''4 la' sib'4. do''8 |
    sib'4\trill sol' r8 sib' do''4.\trill sib'8 do''4 |
    re''2 re''4 mi''4. fa''8 sol''4 |
    dod''2 re''4 mi''4. fad''8 sol''4 |
    fad''4 re'' re'' si' do'' re'' |
    mib''2 sol''4 do'' re'' mib'' |
    re''\trill sib' re'' mib'' mib''8[ re''] do''[ sib'] |
    la'4\trill la' re'' la'2\trill la'8 sib' |
    sol'2 re''4 mib'' mib''8[ re''] do''[ sib'] |
    la'4\trill la' re'' la'2\trill la'8 sib' |
    sol'2. re''4 |
    la'4 la' r la'8 sib' |
    sol'4. sol'8 do''4. do''8 sib'4.\prall la'8 |
    \appoggiatura la'8 sib'2 sib'4 r r sib' |
    sib'2. lab'4 lab' sol' |
    sol'2\trill mib''4. mib''8 la'4 la'8 do'' |
    fa'1 fa''2 |
    si'2. re''8 re'' re''4 re''8 re'' |
    sol'2 r8 sol' sol' sol' re''4. fa''8 |
    \appoggiatura fa''8 mib''1 do''4. re''8 |
    \appoggiatura sol'8 fad'1 la'4 sib' |
    sib'2( la'1)\trill |
    sol'2 <<
      \tag #'basse { s1 s1*2 s4. \semiramisMark }
      \tag #'semiramis { r2 r R1*2 r4 r8 }
    >> do''8 mib'' mib'' mib'' re'' |
    re''4.\trill <<
      \tag #'basse {
        s8 s2 s1 s1. s1*4 s2 \semiramisMarkText "Lentement et a part"
      }
      \tag #'semiramis {
        r8 r2 R1 R1. R1*4 r2 <>^\markup\italic { Lentement et a part }
      }
    >> r4 la' |
    dod''2 mi''4 mi''8 mi'' |
    la'2. <<
      \tag #'basse { s4 s1 s2 \semiramisMarkText "Lentement et a part" }
      \tag #'semiramis { r4 R1 r2 <>^\markup\italic { Lentement et a part } }
    >> r4 re'' |
    fad'2 r la'4 la'8 re'' |
    \appoggiatura do''8 sib'4 sib'8 sib' sol'4\trill\prall sol'8 sol' |
    mi'2\trill mi'4 r <>^\markup\italic { a luy } do''8 do''16 sib' sib'8 la' |
    la'2\trill <<
      \tag #'basse { s2 s \semiramisMarkText "Lentement et a part" }
      \tag #'semiramis { r2 r <>^\markup\italic { Lentement et a part } }
    >> r2 re''4. sib'8 |
    sol'2\trill r r |
    <>^\markup\italic vivement mib''4 do'' la' la'8 fa'' |
    sib' sib' do'' re'' do''4.\trill re''8 |
    sib'2 r4 re'' do''8 sib' la' sib' |
    fad'2 la'4 la'8 la' |
    sib'4. do''8 la'2\trill |
    sol'
    \tag #'semiramis {
      r2 r |
      R1 |
      r4 r8 <>^\markup\italic vivement re'' sib'4. sol'8 |
      re''8[ mib''] fa''[ re''] mib''2 |
      re''4 do''8 re'' mib''[ re''] do''[ sib'] |
      la'4\trill la' sib'2~ |
      sib'4 do'' la'2 |
      r4 re'' sib'2 |
      mib''4 mib''8 mib'' do''4 do'' |
      re'' re'' re'' re'' |
      do''2 do''4. re''8 |
      \appoggiatura do'' sib'2 re''4. mi''8 |
      mi''2\trill mi''4. fa''8 |
      fa''4 la' re''2 |
      r4 si' mi'' mi'' |
      dod'' dod'' fa''2 |
      re''4 mi''8 fa'' mi''4 dod'' |
      re'' re'' r2 |
      R1 |
      r4 r8 sol' la'4 si' |
      do''2 r |
      R1 |
      r4 r8 fa' sol'4 la' |
      sib'2 re''4 re'' |
      sol''8[\melisma lab'' sol'' fa'' mib'' re'' do'' sib'] |
      mib''[ fa'' sol'' fa'' mib'' re'' do'' sib']( |
      la'4)\melismaEnd la'8 re'' mi''4 fad'' |
      sol''2. r8 sib' |
      sib'2 la'\trill |
      sol'1 |
      R1*8 |
      r2 r4
    }
  }
  %% Zoroastre
  \tag #'(zoroastre basse) {
    \zoroastreMark sib4. re'8 fad4 fad8 sol sol4. sol8 la la la la |
    re2 fa4 fa8 fa fa4. sol8 |
    \appoggiatura fa8 mib2 re4 re8 re sol4. la8 |
    fad2 fad4 <<
      \tag #'basse { s4 s2. s1*5 s4. \zoroastreMark }
      \tag #'zoroastre { r4 R2. R1*5 r4 r8 }
    >> re'8 la4. sib8 |
    fad4 fad8 fad sol4 la8 sib |
    la2\trill <<
      \tag #'basse { s1 s1. s2 \zoroastreMark }
      \tag #'zoroastre { r2 r R1. r2 }
    >> la2 la8 la si dod' |
    re'2 sib4. sol8 \appoggiatura fa mi4 mi8 la |
    \appoggiatura la16 re2 r4 sib8 la sol4 la8 sib |
    fad2. la8 do' sib4\trill\prall la8 sib |
    la4.\trill re8 sib4 sib8 re' sol4 sol8 sib |
    \appoggiatura sib8 mib2 mib4 fa8 mib re4\trill mib8 fa |
    \appoggiatura fa8 sol4 sol8 la sib4 sol8 do' |
    la4.\trill re'8 sol4 la8 sib fa4 fa8 fa |
    \appoggiatura fa8 sib,2. fa8 fa fad4 fad8 sol |
    sol4. sol8 sol fa fa mi |
    mi4\trill mi la2 la8 la la la |
    re2 la8 sib |
    do'4.( sib8) la sib |
    sib4( la2)\trill |
    sol4. <<
      \tag #'basse { s8 s2 s1*3 s2 \zoroastreMark }
      \tag #'zoroastre { r8 r2 R1*3 r2 }
    >> sib4. do'8 |
    la2\trill sib4 sol |
    re'2 la4 la8 sib \appoggiatura la16 sol4. la8 |
    fad2\trill fad4 r |
    r2 sol4 sol fa4.\trill mib8 |
    re4 re re sol4. fa8 sol4 |
    do2 do4 fa fa8[ sol] la[ fa] |
    sib4 sib sib fad2\trill fad8 sol |
    re2 re'4 sol sol4. sol8 |
    do'2 do'4 la2\trill la8 do' |
    fa4 fa fa sib4. sol8 la4 |
    re2 re4 sol sol8[ la] sib[ do'] |
    re'4 re' sib sol4. do8 re4 |
    sol,2 re4 sol sol8[ la] sib[ do'] |
    re'4 re' sib sol4. do8 re4 |
    sol,2 <<
      \tag #'basse { s4 s2. s1.*9 s1*2 s1.*10 s2 \zoroastreMark }
      \tag #'zoroastre { r4 r2*3/2 R1.*9 R1*2 R1.*10 r2 }
    >> r4 sib8 lab sol4 lab8 sib |
    fa4 sol8 lab sib4 sib8 sib, |
    mib4. sol8 sol sol la sib |
    la4\trill la8 <<
      \tag #'basse { s8 s2 s4. \zoroastreMark }
      \tag #'zoroastre { r8 r2 r4 r8 }
    >> sib8 re' re' re' re' |
    la4 la8 sib do'4 sib8 la |
    \appoggiatura la8 sib4 sib8 sib mib2 mib4 mib8 sol |
    re2 sol8 sol16 la sib8 do' |
    \appoggiatura do'8 re'2 re'4 r8 sib |
    mi!4. mi8 mi4 sol |
    do4. sol8 do' do' do' do |
    fa2 <<
      \tag #'basse { s2 s1 s2. \zoroastreMark }
      \tag #'zoroastre { r2 R1 r2 r4 }
    >> re'4 |
    sol2 la4 la8 la |
    re2 <<
      \tag #'basse { s2 s1. s1 s1. s2 \zoroastreMark }
      \tag #'zoroastre { r2 R1. R1 R1. r2 }
    >> fa4 fa |
    \appoggiatura fa8 sib,2 <<
      \tag #'basse { s1 s1. s1*2 s1. s1*2 s2 \zoroastreMark }
      \tag #'zoroastre { r2 r R1. R1*2 R1. R1*2 r2 }
    >> sol4 sol8 sol fa4. sol8 |
    \appoggiatura fa8 mib4 re8 re sol4 la8 sib |
    la4.\trill <<
      \tag #'basse { r8 r2 R1*27 R1*5 \zoroastreMark }
      \tag #'zoroastre {
        re8 sol4. sol8 |
        sib4 sol do'2 |
        sib4 la8 sib do'[ sib] la[ sol] |
        fad4 fad r sol |
        do2. fa4 sib4 sib sol2 |
        mib4 mib8 mib fa4 fa |
        sib, sib, sib4. sib8 |
        la2 la4. sib8 |
        \appoggiatura la8 sol2 sol4. sol8 |
        do'2 do'4. do8 |
        fa2 r4 re |
        sol2 r4 mi |
        la la fa2 |
        sib4 sib8 sol la4 la, |
        re re re' re' |
        re'8[ mib' re' do' sib la sol fa]( |
        mib4) mib r2 |
        r do'4 do' do'8[ re' do' sib la sol fa mib]( |
        re4) re r2 |
        r sib4 sib |
        mib'8[\melisma fa' mib' re' do' sib la sol] |
        do'[ re' mib' re' do' sib la sol]( |
        re'4)\melismaEnd re'8 sib do'4 re' |
        mib'2. r8 sol |
        do2 re |
        sol,1 |
        R1*5 |
      }
    >>
    r2 r4 do' |
    sol2 r8 sol sol do' |
    \appoggiatura si8 la4 sol8 la fa4\trill\prall mi8 fa |
    mi2\trill mi4
  }
>>