\clef "basse" do4 |
do sol, do |
sol,2 sol,4 |
do2 do4 |
sol,2 sol,4 |
do sol, do |
sol,2 sol,4 |
do do do |
do2 sol,4 |
do2 do4 |
sol, sol, sol, |
sol, sol, sol, |
sol,2 do4 |
sol,2 sol,4 |
do do do |
do2 do4 |
sol,2 sol,4 |
sol, sol, sol, |
do2 do4 |
do sol,2 |
do
