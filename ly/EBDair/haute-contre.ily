\clef "haute-contre" do''4 |
do'' sol' fad' |
sol'2 sol'4 |
sol'2 sol'4 |
sol'2 si'4 |
do'' sol' fad' |
sol'2 sol'4 |
sol'2 sol'4 |
sol'2 si'4 |
do'' re''8 do'' si' la' |
si'4 re' sol' |
si' re' sol' |
si'2\trill do''4 |
re'' mi''8 re'' do'' si' |
do''4 sol' do'' |
mi'' re'' do'' |
si'2 si'4 |
re'' si' re'' |
mi'' sol' do'' |
re''8 do'' si'4.\trill do''8 |
do''2
