\clef "basse" do4 |
do si, la, |
sol, sol8 la sol fa |
mi2 do4 |
si,8 do si, la, sol,4 |
do si, la, |
sol, sol fa |
mi8 fa sol fa mi re |
do2 sol4 |
do'2 do4 |
sol sol, sol |
sol sol, sol |
sol2 do'4 |
do'2 si4 |
do' do do' |
do'2 do4 |
sol2 sol,4 |
si,2 sol,4 |
do2 mi,4 |
fa, sol,2 |
do2
