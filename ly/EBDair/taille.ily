\clef "taille" mi'4 |
mi' re' do' |
si2 re'4 |
mi'2 mi'4 |
re'2 re'4 |
mi' re' do' |
si2 si4 |
mi'2 mi'4 |
mi'2 sol'4 |
sol'2 sol'4 |
re' si si |
sol si si sol2 sol'4 |
la'2 sol'4 |
sol' mi' mi' |
mi'2 sol'4 |
sol'2 sol'4 |
sol'2 si4 |
do'2 do'4 |
la' sol'4. fa'8 |
mi'2
