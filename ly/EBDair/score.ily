\score {
  \new StaffGroup <<
    \new Staff << \global \keepWithTag #'dessus \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff <<
      <>^\markup\whiteout "Tymballes"
      \global \includeNotes "timbales"
    >>
    \new Staff <<
      <>^\markup\whiteout "Basse continue"
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s4 s2.*9\break }
    >>
  >>
  \layout { }
  \midi { }
}
