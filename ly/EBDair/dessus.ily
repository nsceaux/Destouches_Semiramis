\clef "dessus" \tag #'dessus <>^"Trompettes et violons"
sol''4 |
sol'' fa''8 mi'' re'' do'' |
sol''4 sol'' sol' |
do'' sol' do'' |
re'' sol' sol'' |
sol'' fa''8 mi'' re'' do'' |
sol''4 sol'' sol' |
do'' sol' do'' |
do''2\trill re''4 |
mi'' fa''8 mi'' re'' do'' |
re''4 sol' re'' |
re'' sol' re'' |
re''2\trill mi''4 |
fa'' sol''8 fa'' mi'' re'' |
mi''4 do'' sol'' |
sol'' la''8 sol'' fa'' mi'' |
re''2 sol''4 |
sol'' re'' sol'' |
sol'' do'' sol'' |
fa''8 mi'' re''4.\trill do''8 |
do''2
