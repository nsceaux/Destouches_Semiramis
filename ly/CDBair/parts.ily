\piecePartSpecs
#`((dessus)
   (dessus-hc)
   (haute-contre)
   (taille)
   (basse)
   (basse-continue)
   (silence #:on-the-fly-markup , #{ \markup\tacet#70 #}))
