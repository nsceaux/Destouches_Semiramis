\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*5\break s1*5\pageBreak
        s1*4 s2. s4. \bar "" \break s4. s2.*4\break s2.*4\pageBreak
        s2.*4 s4. \bar "" \break s4. s2.*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
