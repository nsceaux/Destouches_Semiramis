\clef "basse" sib,1~ |
sib, |
la,2 sib, |
do re |
mib si, |
do4 fa sol sol, |
do2. fa4 |
sib,2~ sib,4.*5/6 sib,16 lab, sol, |
lab,2. sib,8 lab, |
sol,2 do4 do, |
fa,2 do4 sib, |
la, fa, sib,2 |
fa, sib, |
fa,1 |
R2. |
fa8 do fa re mib16 re do sib, |
la,4. sib8 fa sib |
sol lab16 sol fa mib re4 re16 re |
mib4 r8 fa4 r8 |
sol8 re sol mib fa16 mib re do |
si,4 r8 do4 r8 |
fa16 mib fa sol lab fa sol4 do8 |
r2*3/4 fa16 mib fa sol fa mib |
re8 sib,4 sib8 sol4 |
do'8 sol do' la sib16 la sol fa |
mi re do re mi do fa8 la fa |
do' do fa re4 r8 |
mib4 fa8 sol4 do8 |
fa4 sib,8 mib4 sib,8 |
fa fa, sib, la,16 sol, fa, sol, la, fa, |
sib,4 r8 do4 r8 |
re4 r8 mib4 r8 |
fa16 mib fa sol la fa sol4. |
mib4 r8 re16 mib fa8 fa, |
sib,4.~ sib, |
<<
  \tag #'basse sib,2.
  \tag #'basse-continue \custosNote sib,2*3/2
>>
