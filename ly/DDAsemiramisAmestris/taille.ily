\clef "taille" R4.*2 |
la'8 la' la' |
fad'16 mi' fad' sol' la'8 |
la' mi' mi'16 fad' |
sol'4. |
fad'8 si' si' |
la' fad' la' |
re'4 la'8 |
si' la'8. sol'16 |
fad'4. |
R4.*5 |
dod'4\fortSug dod'16 re' |
mi'4. |
re'4 mi'8 |
mi' la'8. fad'16 |
sol'4. |
R4.*7 |
la'8 la' la' |
fad'16 mi' fad' sol' la'8 |
la'8 mi' mi'16 fad' |
sol'4. |
fad'8 si' si' |
la' fad' la' |
re'4 la'8 |
si' la'8. sol'16 |
fad'4. |
