\clef "haute-contre" R4.*2 |
re''8 re'' re'' |
re''4. |
dod''8 dod'' red'' |
mi''4. re''8 re'' re'' |
la' la'4 |
sol'16 la' si' dod'' re''8 |
re''8 dod''8.\trill re''16 |
re''4. |
R4.*5 |
mi'4\fortSug mi'8 |
si'4. |
re''4 do''8 |
mi'' red''8. mi''16 |
mi''4. |
R4.*7 |
re''8 re'' re'' |
re''4. |
dod''8 dod'' red'' |
mi''4. |
re''8 re'' re'' |
la' la'4 |
sol'16 la' si' dod'' re''8 |
re'' dod''8.\trill re''16 |
re''4. |
