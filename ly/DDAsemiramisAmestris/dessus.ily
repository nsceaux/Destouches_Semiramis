\clef "dessus" <>_"Violons" fad''16 sol'' la'' sol'' fad'' mi'' |
re'' dod'' re'' mi'' fad'' sol'' |
la''8 la'' la'' |
la''4. |
mi''16 re'' mi'' fad'' sol'' la'' |
si''4. |
fad''16 sol'' la'' sol'' fad'' mi'' |
re'' mi'' fad'' mi'' re'' dod'' |
si' dod'' re'' mi'' fad'' re'' |
sol'' fad'' mi''8.\trill re''16 |
re''4. |
R4.*5 |
la'16\fort si' dod'' re'' mi'' fad'' |
sol''4. |
fad''16 sol'' la'' sol'' fad'' mi'' |
la'' sol'' fad''8.\trill mi''16 |
mi''4. |
R4.*5 |
<>^"Tous" fad''16\fort sol'' la'' sol'' fad'' mi'' |
re'' dod'' re'' mi'' fad'' sol'' |
la''8 la'' la'' |
la''4. |
mi''16 re'' mi'' fad'' sol'' la'' |
si''4. |
fad''16 sol'' la'' sol'' fad'' mi'' |
re'' mi'' fad'' mi'' re'' dod'' |
si' dod'' re'' mi'' fad'' re'' |
sol'' fad'' mi''8.\trill re''16 |
re''4. |
