\clef "basse" R4.*2 |
fad16 sol la sol fad mi |
re dod re mi fad sol |
la8 la la |
mi16 re mi fad sol la |
si4. |
fad8 re4 |
sol4 fad8 |
mi la la, |
<>^"B-C." re16 dod re mi fad sol |
la4. |
mi16 red mi fad sol la |
si4. |
mi'16 re' mi' fad' mi' re' |
dod'8 re' mi' |
la4. |
mi16^"Tous" re mi fad sol la |
si4 do'8 |
la si si, |
mi16^"[B-C.]" re mi fad sol mi |
la4. |
re16 dod re mi fad re |
sol fad sol la sol fad |
mi re mi fad sol la |
si sol la8 la, |
re4. |
R4. |
<>^"Tous" fad16 sol la sol fad mi |
re dod re mi fad sol |
la8 la la |
mi16 re mi fad sol la |
si4. |
fad8 re4 |
sol fad8 |
mi la la, |
re4. |
