\clef "vbas-dessus" R4.*9 |
\tag #'amestris <>^\markup\character Amestris
\tag #'semiramis <>^\markup\character Semiramis
r8 r <<
  \tag #'(amestris basse) {
    mi''16 mi'' |
    fad''4. |
    mi''8 mi'' fad'' |
    sol''4. |
    fad''8 fad'' fad'' |
    si'4. |
    mi''8 si'8. mi''16 |
    dod''4. |
  }
  \tag #'semiramis {
    dod''16 dod'' |
    re''4. |
    dod''8 dod'' red'' |
    mi''4. |
    re''8 re'' re'' |
    sold'4. |
    la'8 la' sold' |
    la'4. |
  }
>>
R4.*2 |
r8 r <<
  \tag #'(amestris basse) {
    fad''16 fad'' |
    sol''4. |
    mi''8 mi'' mi'' |
    fad''4. |
    re''8 re'' re'' |
    sol''4. |
    fad''8 mi''8.\trill re''16 |
    re''4. |
  }
  \tag #'semiramis {
    red''16 red'' |
    mi''4. |
    dod''8 dod'' dod'' |
    re''4. |
    si'8 si' si' |
    mi''4. |
    re''8 dod''8.\trill re''16 |
    re''4. |
  }
>>
R4.*10 |
