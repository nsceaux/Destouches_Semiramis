\clef "haute-contre" r4 r2 |
R1 R1. R1 R1. R1*4 R1. R1 R1. R1 R1. R1*2 |
sol'1 r2 |
do'' r r |
fa' fa' |
la' sol' |
sol'2 re' |
mib'1 |
r2 sol' |
fa'1 |
mib'2 sol' |
fa'2. re'4 |
re'2 r |
mib'2. sol'4 |
fa'2 fa' |
sol'4 sib' sib'2 |
sib'2. sib'4 |
lab'2 sol'4 sol' |
sol'1 |
