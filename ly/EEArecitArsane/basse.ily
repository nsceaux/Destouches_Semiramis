\clef "basse"
<<
  \tag #'basse { r4 r2 | R1 R1. R1 R1. R1*4 R1. R1 R1. R1 R1. R1*2 | }
  \tag #'basse-continue {
    do4\repeatTie~ do2~ |
    do re |
    mib1 do2 |
    lab2 sib4 sib, |
    mib1 sol4 fa |
    mib2 do |
    lab la |
    si do'8 sib? lab4 |
    sol1 |
    fad2 sol8 la sib sol do' sib la sol |
    re'2 sib |
    do'4 sib la sol re2 |
    sol r4 mib |
    lab2 r4 fa do' sib |
    lab2 mi |
    fa1 |
  }
>>
sol8.*2/3 fa32*2 mib re do si, la, sol,2 r |
lab,8 lab,16 sib, do sib, lab, sol, fa,2 r |
la,2 sib, |
la, si,8 si,16 do re do si, la, |
sol,2 sol,8 sol,16 lab, sol, fa, mib, re, |
do,1~ |
do, |
fa, |
do2 sib, |
lab,2. fa,4 |
sol,2 r |
mib2. mi4 |
fa2 sib |
mib2 sib4 mib |
sib,2. mib4 |
lab fa sol sol, |
do1 |
