\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { \haraKiriFirst } <<
        \global \keepWithTag #'dessus \includeNotes "dessus"
      >>
      \new Staff \with { \haraKiriFirst } <<
        \global \includeNotes "haute-contre"
      >>
      \new Staff \with { \haraKiriFirst } <<
        \global \includeNotes "taille"
      >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2. s1 s1 \bar "" \break
        s2 s1 s1.\break s1*2 s2 \bar "" \break s2 s1 s1.\break s1 s1.\break s1 s1.\break s1*2\pageBreak
        s1.*2 s1*2\break s1*4\pageBreak
        s1*3\break s1*2\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
