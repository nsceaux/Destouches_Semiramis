\clef "dessus" r4 r2 |
R1 R1. R1 R1. R1*4 R1. R1 R1. R1 R1. R1*2 |
re''2 r sol'' |
mib'' r r |
do'' re'' |
do'' re'' |
si' sol' |
sol'1 |
r2 mib'' |
do'' lab' |
sol' sol' |
do''2. re''4 |
si'2 r |
do''2. do''4 |
do''2 sib' |
sib'4 sol'' fa'' sol'' |
re''2. mib''4 |
mib'' fa''8 lab'' re''4 re''8 mib'' |
do''1 |
