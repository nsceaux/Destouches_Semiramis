\tag #'(arsane basse) {
  Ah ! Prin -- cesse, ar -- rê -- tez...
}
\tag #'(amestris basse) {
  Sei -- gneur, que fai -- tes- vous ?
  Vous ir -- ri -- tez les Dieux.
}
\tag #'(arsane basse) {
  Je bra -- ve leur cou -- roux.
  Quoi ! la ver -- tu pe -- rit ! Quelle a -- veu -- gle van -- gean -- ce !
  Est- ce donc à ces traits qu’ils mar -- quent leur puis -- san -- ce ?
  Per -- fi -- des, n’o -- sez- vous dé -- fen -- dre l’in -- no -- cen -- ce ?
}
\tag #'(semiramis basse) {
  Mi -- nis -- tres des Au -- tels van -- gez les droits des Dieux.
}
\tag #'(arsane basse) {
  Fu -- yez, trem -- blez, Es -- cla -- ves o -- di -- eux
  D’u -- ne Rei -- ne cru -- el -- le.
  Où suis- je ? quels trans -- ports ? C’est l’En -- fer qui m’ap -- pel -- le,
  Je vous ré -- pons... quelle é -- pais -- se va -- peur...
  Je vois de -- vant mes pas le flam -- beau des Fu -- ri -- es :
  Je vous suis... E -- pui -- sez tou -- tes vos bar -- ba -- ri -- es.
  Ver -- sons des flots de sang... Ré -- pan -- dons la ter -- reur :
  Je sens tout l’En -- fer dans mon cœur.
}
