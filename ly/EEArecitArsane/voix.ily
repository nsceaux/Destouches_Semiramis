<<
  %% Amestris
  \tag #'(amestris basse) {
    <<
      \tag #'basse { s2. s4. \amestrisMark }
      \tag #'amestris { \amestrisMark r4 r2 r4 r8 }
    >> do''8 do'' sib' sib' do'' |
    sol'2\trill sol'4 sol'8 sol' mib''4. mib''8 |
    do''4\trill
    \tag #'amestris { r4 r2 | R1. R1*4 R1. R1 R1. R1 R1. R1*2 R1.*2 R1.*15 }
  }
  %% Semiramis
  \tag #'(semiramis basse) {
    <<
      \tag #'basse { s2. s1 s1. s1 s1. s1*4 s1. s4. \semiramisMark }
      \tag #'semiramis { \semiramisMark r4 r2 | R1 R1. R1 R1. R1*4 R1. r4 r8 }
    >> re''8 re'' re'' re'' mib'' |
    mib''4 re'' do'' sib' la' re'' |
    si'4.\trill
    \tag #'semiramis { r8 r2 | R1. R1*2 R1.*2 R1*15 }
  }
  %% Arsane
  \tag #'(arsane basse) {
    \arsaneMarkText "lui arrachant le Fer"
    mib'8 fa' sol'4 sol'8 sol' |
    do'4. <<
      \tag #'basse { s8 s2 s1. s4 \arsaneMarkText "vivement" }
      \tag #'arsane { r8 R2 | R1. | r4 <>^\markup\italic vivement }
    >> r8 fa' re'\trill re' re' mib' |
    mib'2 sol' si8 si do' re' |
    sol4 sol'8 fa' mib'4 mib'8 re' |
    do'\trill do' lab' sol' fa'4 fa'8 mib' |
    re'4.\trill re'8 mib' mib' mib' fa' |
    \appoggiatura fa'8 sol'2 sol'4 <>^\markup\italic { Au Peuple } re'4 |
    la la sib8 do' re' re' mi' mi' fad' sol' |
    fad'4\trill\prall fad'8
     <<
      \tag #'basse { s8 s2 s1. s4. \arsaneMark }
      \tag #'arsane { r8 r2 R1. r4 r8 }
    >> re'8 sol'2 |
    r4 do' lab'4. lab'8 mi' mi' mi' sol' |
    do'4 do'8 do' sib4 sib8 do' |
    la4\trill la r fa' |
    si si r2 mib'4 mib' |
    \appoggiatura re'8 do'2 r fa'4 fa' |
    fa'2 fa'4 fa' |
    fa'1~ |
    fa'4 fa' r8 si si si |
    mib'1 |
    <>^\markup\italic vivement r4 sol'8 sol' do'4 do'8 mib' |
    lab4. lab8 do' do' do' re' |
    mib'4 mib'8 mib' mi'4 mi'8 fa' |
    fa'2 fa'4 fa'8 lab' |
    re'2. re'8 re' |
    sol'2 mib'8 mib'16 re' do'8 sib |
    la4\trill la8 fa' re'\trill do' sib lab |
    sol4\trill sib8 do' re'4\trill re'8 mib' |
    \appoggiatura mib'8 fa'2. sol'4 |
    do' lab'8 fa' si4 si8 do' |
    do'1 |
  }
>>