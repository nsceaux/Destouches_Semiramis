\key sol \minor
\time 3/2 \partial 2. \midiTempo#120 s2.
\time 2/2 s1
\time 3/2 s1.
\time 2/2 s1
\time 3/2 s1.
\time 2/2 s1*4
\time 3/2 s1.
\time 2/2 s1
\time 3/2 s1.
\time 2/2 s1
\time 3/2 s1.
\time 2/2 s1*2
\time 3/2 s1.*2
\time 2/2 s1*15 \bar "|."
