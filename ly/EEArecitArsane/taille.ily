\clef "taille" r4 r2 |
R1 R1. R1 R1. R1*4 R1. R1 R1. R1 R1. R1*2 |
re'1 r2 |
do' r r |
do' sib |
do' sol |
re'1 |
do' |
r2 do' |
do'1 |
do' |
do' |
sol2 r |
sol2. do'4 |
do'2 re' |
mib' sib4 sib |
sib2. sib4 |
do'2 sol'4 re' |
mib'1 |
