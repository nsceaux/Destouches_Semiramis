\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*8\pageBreak
        s2.*8\break s2.*8\break s2.*8\pageBreak
        s2.*8\break s2.*8\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
