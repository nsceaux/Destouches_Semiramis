\clef "basse" r4 |
R2. |
r4 r sib, |
re re8 mib fa4 |
re re8 do sib,4 |
la,2 la,4 |
sol,2 la,4 |
sib,8 la, sib, do re mib |
fa2. |
re8 do re mib fa re |
sol2. |
mib8 re mib fa sol la |
sib2 la4 |
sol do' sib |
la2 la4 |
sol do' do |
fa do2 |
re8 sib, do4 do, |
fa,2 fa4 |
do2 si,4 |
do mib do |
lab fa2 |
do'8 sib lab2 |
sol sol,4 |
do2 fa4 |
re sol sol, |
do do8 sib, do4 |
la, fa, fa |
sib sib8 la sib4 |
sol2 sol4 |
mib' do'2 |
re' re4 |
sol2 do4 |
la, re re |
sol sol8 fa sol4 |
mi do do |
fa fa8 mib fa4 |
re la,2 |
sib,4 sib sib |
la la la |
sol sol sol |
fa2 r4 |
mib2 r4 |
re2 r4 |
do2 r4 |
fa fa mib |
re fa fa |
do8 re mib fa sol la |
sib4 sib sol |
lab lab fa |
sol sol sol |
mib fa fa, |
sib,2
