\clef "haute-contre" r4 |
R2. |
r4 r fa' |
fa'2 fa'4 |
fa' sib'8 la' sib'4 |
do''2 do''4 |
re''2 do''4 |
sib'2. |
la'8 sol' la' sib' do'' la' |
sib'2. |
sib'8 la' sib' do'' re'' sib' |
mib''2. |
sib'2 do''4 |
re'' do'' sol' |
la' do''2 |
re''4 do''4. do''8 |
do''4 do''2 |
sib'8 re'' do''4.\trill sib'8 |
la'2 la'4 |
do''2 re''4 |
do''2 do''4 |
do'' do''2 |
mib''4 do''2\trill |
re'' sol'4 |
sol' do'' do'' |
do''4 si'4.\trill do''8 |
do''2 do''4 |
do'' la' do'' |
sib' fa' re'' |
re'' re''8 do'' re''4 |
sib'8 la' sib' re'' do'' sib' |
la'4 fad' fad' |
sol'2 sol'4 |
la'8 sol' fad'4.\trill sol'8 |
sol'2 sol'4 |
sol' mi' sol' |
fa' do' fa' |
fa' do'' la' |
fa' sib' re'' |
do''4 r r |
sib' r r |
la' r r |
sol' r r |
la' r r |
sib' r r |
fa' fa' fa' |
fa' fa'8 sol' la' si' |
do''2. |
sib'4 sib' sib' |
lab' lab' lab' |
sol' sol' sol' |
do''8 sib' la'4.\trill sib'8 |
sib'2
