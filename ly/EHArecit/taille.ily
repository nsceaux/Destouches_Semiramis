\clef "taille" R1*5 R2. R1*7 |
do'2 r4 |
do'2 r4 |
fa2 r4 |
do'2 fa'4 |
do'2 r4 |
do'2 r4 |
do'2 sib |
sib2 r4 |
sib2 r4 |
do'2 r4 |
re'2 r4 |
sol1 r2 |
R1*3 |
do'2. sib4 |
lab2 do'4 |
sol2 sib4 |
lab2 sib4 |
sib2 fa4 |
mib2 r4 |
R2. |
r4 r sol |
re2 r4 |
do'2 r4 |
sol2 r4 |
R2. |
re2 r4 |
mib2 r4 |
mib2 r4 |
re2 r4 |
mib2 r4 |
mib2 r4 |
R2. |
la4 r sol |
mi2. |
