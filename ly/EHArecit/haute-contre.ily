\clef "haute-contre" R1*5 R2. R1*7 |
mib'2 r4 |
do'2 r4 |
re'2 r4 |
mib'2 fa'4 |
mib'2 r4 |
sol'2 r4 |
fa'2 sib |
sib2 r4 |
mib'2 r4 |
mib'2 r4 |
sol2 r4 |
sol1 r2 |
R1*3 |
mib'2 do' |
do' mib'4 |
re'2 sib4 |
do'2 fa'8 sib |
sib2. |
sib2 r4 |
R2. |
r4 r do' |
la2 r4 |
re'2 r4 |
re'2 r4 |
R2. |
lab2 r4 |
sol2 r4 |
lab2 r4 |
sol2 r4 |
sol2 r4 |
sol2 r4 |
R2. |
do'4 r sol |
sol2. |
