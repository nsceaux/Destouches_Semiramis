<<
  %% Semiramis
  \tag #'(semiramis basse) {
    <<
      \tag #'basse { s1*5 s2. s1*7 \semiramisMarkText "lentement" }
      \tag #'semiramis { \semiramisMark R1*5 R2. R1*7 <>^\markup\italic lentement }
    >>
    mib''2 do''4 |
    la'2\trill re''8 fa'' |
    \appoggiatura do''16 si'2 si'8 si' |
    do''4. do''8 re''4 |
    \appoggiatura re''16 mib''2 r4 |
    mi''2 mi''8 fa'' |
    fa''4. mib''8 re'' do'' sib' lab' |
    sol'2 r8 sol' |
    sib'4. sib'8 sib' sol' |
    do''2 do''8 do'' |
    fa'2 fa'8 sol' |
    \appoggiatura fa'16 mib'2 mib' <<
      \tag #'basse { s2 s1*2 \semiramisMarkText "lentement" }
      \tag #'semiramis { r2 R1*2 <>^\markup\italic lentement }
    >>
    r2 r4 sol'8 sol' |
    do''4. reb''8 sib'4\trill\prall lab'8 sol' |
    \appoggiatura sol'8 lab'2 sol'8 sol' |
    re''2 re''8 mib'' |
    \appoggiatura re''16 do''2 re''8 mib'' |
    mib''2 mib''8 re'' |
    \appoggiatura re''8 mib''2 mib''4 |
    r4 mib''4. re''8 |
    do''2 la'8 sib' |
    fad'2 re''4 |
    re''2 do''8 do''16 re'' |
    si'2 si'4 |
    r mib'' mib''8 mib'' |
    re''2 re''4 |
    sol'2 sol'8 sol' |
    do''2 do''8 lab' |
    re'2 r8 sol' |
    sol'4 r8 sol' lab' sib' |
    \appoggiatura lab'8 sol'2 r4 |
    r4 r8 do'' re'' mib'' |
    \appoggiatura sib'16 la'2 r8 si' |
    \appoggiatura si' do''2. |
  }
  %% Arsane
  \tag #'(arsane basse) {
    \arsaneMark r2 r4 sol |
    mib'4. r8 sol'4 sol'8 sol' |
    do'4 re'8 mib' fa'4 sol'8 lab' |
    re'4. re'8 sol' fa' mib' re' |
    \appoggiatura re'16 mib'2
    <<
      \tag #'basse { s2 s2. s4. \arsaneMarkText "vivement" }
      \tag #'arsane { r2 R2. r4 r8 <>^\markup\italic vivement }
    >> fa'8 do'\trill do' do' re' |
    mib'4 mib'8 mib' mib'4 mib'8 fa' |
    \appoggiatura fa'8 sol'2 sol'4 <<
      \tag #'basse { s s1*4 s2.*6 s1 s2.*4 s1 \arsaneMarkText "vivement" }
      \tag #'arsane { r4 R1*4 R2.*6 R1 R2.*4 r2 r <>^\markup\italic vivement }
    >> mib'8 mib'16 mib' fa'8 sol' |
    do'4 re'8 mib' fa'4 sol'8 lab' |
    si2 si |
    \tag #'arsane { R1*2 R2.*20 }
  }
  %% Zoroastre
  \tag #'(zoroastre basse) {
    <<
      \tag #'basse { s1*4 s2 \zoroastreMarkText "lentement" }
      \tag #'zoroastre { \zoroastreMark R1*4 r2 <>^\markup\italic lentement }
    >> r4 do' |
    sol4. sol8 sol lab |
    lab4.
    <<
      \tag #'basse { s8 s2 s1 s2. \zoroastreMark }
      \tag #'zoroastre { r8 r2 R1 r2 r4 }
    >> sol4 |
    do' sib la sol |
    fad2. sib4 |
    \appoggiatura la16 sol4 sol r sol8 fad |
    sol2 sol4 r |
    \tag #'zoroastre { R2.*6 R1*2 R2.*4 R1. R1*4 R2.*20 }
  }
>>