\clef "basse"
<<
  \tag #'basse { R1*5 R2. R1*7 | }
  \tag #'basse-continue {
    R1 |
    do |
    lab2 fa |
    sol si, |
    do1 |
    mi2. |
    fa1 |
    do'4 sib lab2 |
    sol2 sol, |
    mib4 re do sib, |
    la,2. sol,4 |
    mib do re re, |
    sol,2. r4 |
  }
>>
do2 r4 |
fa2 r4 |
sol2~ sol8 fa |
mib2 re4 |
do2 r4 |
sib,2 r4 |
lab,2 sib, |
mib2.~ |
mib |
la,2 r4 |
si,2 r4 |
do1. |
lab2 fa |
sol1 |
R1 |
mib2 mi |
fa2 do4 |
sib,2 sol,4 |
lab,4. sol,8 fa, mib, |
sib,2. |
mib,2 r4 |
R2. |
r4 r do |
re2 r4 |
fad,2 r4 |
sol,2 r4 |
R2. |
fa,2 r4 |
mib,2 r4 |
lab,2 fa,4 |
sol,2 r4 |
mib,2 r4 |
mib,2 r4 |
R2. |
fa4 r sol |
do2. |
