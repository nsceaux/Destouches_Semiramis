\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { \haraKiriFirst } << \global \includeNotes "dessus" >>
      \new Staff \with { \haraKiriFirst } << \global \includeNotes "haute-contre" >>
      \new Staff \with { \haraKiriFirst } << \global \includeNotes "taille" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3 s2 \bar "" \break
        s2 s1 s2.\break s1*3\break s1*4\break s2.*5\pageBreak
        s2. s1 s2.\break s2.*3 s1.\pageBreak
        s1*4\break \grace s8 s2.*4\pageBreak
        \grace s8 s2.*5\break s2.*5\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
