\tag #'(arsane basse) {
  Grands Dieux ! el -- le pa -- roît, que mon cœur est gla -- cé !
  Je vois cou -- ler son sang.
}
\tag #'(zoroastre basse) {
  C’est toi qui l’a ver -- sé.
}
\tag #'(arsane basse) {
  La Rei -- ne par mon bras, a per -- du la lu -- mie -- re !
}
\tag #'(zoroastre basse) {
  Ton sort est plus af -- freux, Ar -- sa -- ne, c’est ta Me -- re.
}
\tag #'(semiramis basse) {
  Vous, mon Fils ! quoi je meurs par la main de mon Fils !
  Dieux in -- hu -- mains, vous me l’a -- viez pro -- mis.
  Ce jour ter -- mine en -- fin mes mal -- heurs & mes cri -- mes.
}
\tag #'(arsane basse) {
  Ter -- re, pour m’en -- glou -- tir, ou -- vre- moi tes a -- bî -- mes.
}
\tag #'(semiramis basse) {
  A -- mes -- tris, cal -- mez ses fu -- reurs.
  Je vous laisse en mou -- rant la su -- prê -- me puis -- san -- ce,
  Le So -- leil dé -- sor -- mais lui -- ra sur l’in -- no -- cen -- ce.
  De l’é -- ter -- nel -- le nuit j’en -- tre -- vois les hor -- reurs.
  Ni -- nus ap -- pro -- chez- vous... je m’af -- foi -- blis... je meurs.
}
