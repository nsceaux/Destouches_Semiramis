\key sol \minor
\time 2/2 \midiTempo#120 s1*5
\digitTime\time 3/4 s2.
\time 2/2 s1*7
\digitTime\time 3/4 s2.*6
\time 2/2 s1
\digitTime\time 3/4 s2.*4
\time 3/2 \grace s16 s1.
\time 2/2 s1*4
\digitTime\time 3/4 \grace s8 s2.*20 \bar "|."
