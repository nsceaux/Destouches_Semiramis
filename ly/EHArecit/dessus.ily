\clef "dessus2" R1*5 R2. R1*7 |
<>^"Violons" sol'2 r4 |
fa'2 r4 |
re'2 r4 |
sol'4. la'8 si'4 |
do''2 r4 |
sol'2 r4 |
lab'2 fa' |
mib'2 r4 |
sol'2 r4 |
sol'2 fa'4 |
re'2 r4 |
do'1 r2 |
R1*3 |
sol'2 sol' |
fa' mib'8 sol' |
sol'2 sol'8 sol' |
mib'2 fa'8 sol' |
fa'2 sib'4 |
sol'2 r4 |
R2. |
r4 r mib' |
re'2 r4 |
la'2 r4 |
re'2 r4 |
R2. |
si2 r4 |
do'2 mib'4 |
mib'2 lab'8 fa' |
si2 r4 |
sib!2 r4 |
sib2 r4 |
R2. |
mib'4 r re' |
do'2. |
