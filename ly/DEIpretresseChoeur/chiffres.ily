s4 <6+> <6>2 <6 5>4 <_+>2\figExtOn <_+>4\figExtOff <6> <6+>2 <_+>2 <6+>4 <6>4\figExtOn <6>\figExtOff <6 5>4 <_+>2 <6 4>4 <_-> <5 4> <_+> s

\setMusic #'rondeau {
  s4 <6+> <6>4\figExtOn <6>\figExtOff <6 5>4 <_+>2\figExtOn <_+>4\figExtOff <6>4 <6+>2 <_+>2 <6+>4
  <6>2 <6 5>4 <_+>2 <6 4>4 <6-> <_+>\figExtOn <_+>\figExtOff s4
}
\keepWithTag #'() \rondeau

s2 <6>4 <6> <6 4+> <6> <6+>2 <5/> <6>4 <_+>2. <6 4> s4. <6>8 <6+>4 s <4+> <6> <6+> <_+> <6> <9 7> <5 4> <3+> <_+>

\modVersion\rondeau

<6->4 <6> <6-> <6> <6> <_+> <6> <6> <6+>2 <5/>4 s <6-> <6>
s <6> <6> <_+> <6> <6> <6+>2 <5/>4 s2 <4+>4 <6>4\figExtOn <6>\figExtOff <6 _->4
<_+>4\figExtOn <_+>\figExtOff <6> <6 _-> <_+>2 s <4+>4
<6>\figExtOn <6>\figExtOff <6 _-> <_+>4\figExtOn <_+>\figExtOff <6>4 <6 5 _-> <_+>2 s4

\modVersion\rondeau
