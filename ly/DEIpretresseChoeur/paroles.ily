\tag #'(pretresse basse) {
  L’A -- mour ver -- se des lar -- mes,
  Vous cau -- sez ses pleurs ;
  Il de -- voit par vos char -- mes
  Vain -- cre tous les cœurs.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  L’A -- mour ver -- se des lar -- mes,
  Vous cau -- sez ses pleurs ;
  Il de -- voit par vos char -- mes
  Vain -- cre tous les cœurs.
}
\tag #'(pretresse basse) {
  Sa plus chere es -- pe -- ran -- ce
  S’é -- teint à ja -- mais !
  Eh ! quels yeux dé -- sor -- mais
  E -- ten -- dront sa puis -- san -- ce,
  Quels se -- ront ses traits ?
}
\modVersion\tag #'(vdessus vhaute-contre vtaille vbasse) {
  L’A -- mour ver -- se des lar -- mes,
  Vous cau -- sez ses pleurs ;
  Il de -- voit par vos char -- mes
  Vain -- cre tous les cœurs.
}
\tag #'(pretresse basse) {
  Le foible hon -- neur de plai -- re
  Coû -- te des tour -- mens.
  La vic -- toire est trop che -- re ;
  Fuy -- ons les A -- mans.
  Des mo -- mens plus tran -- quil -- les
  Vont cou -- ler pour vous.
  Goû -- tez dans nos a -- zi -- les
  Les biens les plus doux.
}
\modVersion\tag #'(vdessus vhaute-contre vtaille vbasse) {
  L’A -- mour ver -- se des lar -- mes,
  Vous cau -- sez ses pleurs ;
  Il de -- voit par vos char -- mes
  Vain -- cre tous les cœurs.
}
