\clef "dessus"
\setMusic #'rondeau {
  re''4 dod'' |
  re'' mi''16 re''8. do''16 si'8 do''16 |
  dod''4 la' la' |
  re'' mi'' fa'' |
  mi'' re''4. dod''8 |
  re''4 mi''16 re''8. do''16 si'8 do''16 |
  dod''4 la' sol''( |
  fa''8)\trill mi'' mi''4.\trill re''8 |
  re''4
}
%% Prestresse
r4 r R2.*7 r4
%% Chœur
\keepWithTag #'() \rondeau
%% Prestresse
r4 r R2.*9 r4
%% Chœur
\modVersion\rondeau
%% Prestresse
r4 r R2.*15 r4
%% Chœur
\modVersion\rondeau
