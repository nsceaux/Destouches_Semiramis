\clef "vdessus"
\setMusic #'rondeau {
  re''4 dod'' |
  re'' mi''16[ re''8.] do''16[ si'8 do''16] |
  dod''4 la' la' |
  re'' mi'' fa'' |
  mi''4\trill re''4. dod''8 |
  re''4 mi''16[ re''8.] do''16[ si'8 do''16] |
  dod''4 la' sol''( |
  fa''8)\trill mi'' mi''4.\trill re''8 |
  re''4
}
%% Prestresse
<<
  \tag #'(pretresse-vdessus pretresse basse) {
    <>^\markup\character-text "Une Pretresse" "à Amestris"
    \keepWithTag #'() \rondeau
  }
  \tag #'vdessus { r4 r R2.*7 r4 }
>>
%% Chœur
<<
  \tag #'(basse pretresse) { r4 r R2.*7 r4 }
  \tag #'(pretresse-vdessus vdessus) {
    <>^\markup\character Chœur \rondeau
  }
>>
%% Pretresse
<<
  \tag #'(pretresse-vdessus basse pretresse) {
    <>^\markup\character "La Pretresse"
    fa''4 mi'' |
    re'' do'' si' |
    mi'' re'' do'' |
    si'4.\trill\prall do''8 la'4 |
    sold'4. do''8 si'4 |
    la'4.\trill\prall sold'8 la'4 |
    mi'4. mi''8 re''4 |
    do'' si' la' |
    re'' si' mi''~ |
    mi''8 fa'' si'4.\trill la'8 |
    la'4
  }
  \tag #'vdessus { r4 r R2.*9 r4 }
>>
%% Chœur
\modVersion <<
  \tag #'(basse pretresse) { r4 r R2.*7 r4 }
  \tag #'(pretresse-vdessus vdessus) {
    <>^\markup\character Chœur \rondeau
  }
>>
%% Pretresse
<<
  \tag #'(pretresse-vdessus basse pretresse) {
    <>^\markup\character "La Pretresse"
    sib'4 la' |
    sib' la' sol' |
    la' re' re'' |
    dod'' re'' mi'' |
    fa'' sib' la' |
    sib' la' sol' |
    la' re' re'' |
    dod'' re'' mi'' |
    fa'' mi''4. fad''8 |
    sol''4 sol''8[ fa''] mi''[ re''] |
    dod''4 la' fa'' |
    mi''8[ re''] dod''4. re''8 |
    re''4 mi'' fad'' |
    sol'' sol''8[ fa''] mi''[ re''] |
    dod''4 la' fa'' |
    mi''16[ re''8 mi''16] mi''4.\trill re''8 |
    re''4
  }
  \tag #'vdessus { r4 r R2.*15 r4 }
>>
%% Chœur
\modVersion <<
  \tag #'(basse pretresse) { r4 r R2.*7 r4 }
  \tag #'(pretresse-vdessus vdessus) {
    <>^\markup\character Chœur \rondeau
  }
>>
