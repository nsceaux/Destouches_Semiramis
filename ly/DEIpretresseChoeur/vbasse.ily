\clef "vbasse"
\setMusic #'rondeau {
  re4 mi |
  fa fa sol |
  la la sol4 |
  fa mi re |
  la, re mi |
  fa fa sol |
  la la sib4 |
  sol la la, |
  re
}
%% Prestresse
r4 r R2.*7 r4
%% Chœur
\keepWithTag #'() \rondeau
%% Prestresse
r4 r R2.*9 r4
%% Chœur
\modVersion\rondeau
%% Prestresse
r4 r R2.*15 r4
%% Chœur
\modVersion\rondeau
