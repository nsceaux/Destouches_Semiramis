\clef "basse"
\setMusic #'rondeau {
  re4 mi |
  fa fa sol |
  la la sol4 |
  fa mi re |
  la, re mi |
  fa fa sol |
  la la sib4 |
  sol la la, |
  re
}
%% Prestresse
<<
  \tag #'basse { r4 r R2.*7 r4 }
  \tag #'basse-continue {
    re4 mi |
    fa fa sol |
    la2 sol4 |
    fa mi re |
    la, re mi |
    fa fa sol |
    la2 sib4 |
    sol la la, |
    re
  }
>>
%% Chœur
\keepWithTag #'() \rondeau
%% Prestresse
<<
  \tag #'basse { r4 r R2.*9 r4 }
  \tag #'basse-continue {
    r4 r |
    fa' mi' re' |
    do' si la |
    sold la fa |
    mi2.~ |
    mi~ |
    mi4. do'8 si4 |
    la re' do' |
    si mi'8 re' do'4 |
    re' mi' mi |
    la
  }
>>
%% Chœur
\modVersion\rondeau
%% Prestresse
<<
  \tag #'basse { r4 r R2.*15 r4 }
  \tag #'basse-continue {
    re'4 dod' |
    re' do' sib |
    la8^"Egales" sol fa re fa4 |
    mi re dod |
    re re' dod' |
    re' do' sib |
    la8^"Egales" sol fa re fa4 |
    mi re dod |
    re la do' |
    sib la sol |
    la sol fa |
    sol la la, |
    re la do' |
    sib la sol |
    la sol fa |
    sol la la, |
    re
  }
>>
%% Chœur
\modVersion\rondeau
