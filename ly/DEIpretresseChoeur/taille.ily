\clef "taille"
\setMusic #'rondeau {
  fa'4 mi' |
  re' re' re' |
  la la dod' |
  la dod' re' |
  dod' fa' mi' |
  re' re' re' |
  la la re' |
  re' dod'4. re'8 |
  re'4
}
%% Prestresse
r4 r R2.*7 r4
%% Chœur
\keepWithTag #'() \rondeau
%% Prestresse
r4 r R2.*9 r4
%% Chœur
\modVersion\rondeau
%% Prestresse
r4 r R2.*15 r4
%% Chœur
\modVersion\rondeau
