\clef "vhaute-contre"
\setMusic #'rondeau {
  la'4 sol' |
  la' la' sol' |
  mi' dod' mi' |
  fa' sol' la' |
  la' la' sol' |
  la' la' sol' |
  mi' dod' mi' |
  sol' sol' mi' |
  fa'4
}
%% Prestresse
r4 r R2.*7 r4
%% Chœur
\keepWithTag #'() \rondeau
%% Prestresse
r4 r R2.*9 r4
%% Chœur
\modVersion\rondeau
%% Prestresse
r4 r R2.*15 r4
%% Chœur
\modVersion\rondeau
