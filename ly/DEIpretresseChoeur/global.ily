\key la \minor \midiTempo#144
\digitTime\time 3/4 \partial 2
s2 s2.*7 s4
\origVersion\segnoMark s2 s2.*7 s4 \origVersion\fineMark
s2 s2.*9 s4 \origVersion\bar "|;|"
\modVersion { s2 s2.*7 s4 }
s2 s2.*15 s4 \origVersion\bar "|;|"
\modVersion { s2 s2.*7 s4 \bar "|." }
