<<
  \tag #'basse { \clef "basse" re2. \clef "dessus2" <>^"Violons" }
  \tag #'violon { \clef "dessus2" <>^"Violons" R2. | }
>>
sol'4 sol'8 la' sib' sol' |
do''2 do''4 |
la'2 la'4 |
re''8 sol' re'2 |
sol'4 sol'8 la' sib' sol' |
do''2 do''4 |
la'4. sib'8 do'' la' |
re''4 re'' re''8 do'' |
sib' do'' re'' do'' sib' la' |
sol'4 sol'4. sol'8 |
\appoggiatura sol'8 fa'2 fa'4 |
fa'8 sol' mib'4.\trill re'8 |
re'2 re''4 |
sol'8 fa' sol' la' sib' sol' |
do''4 do'' do''8 sib' |
la'4. la'8 la'4 |
sib' do''4. do''8 |
fa'2 sib'8 sib' |
sib'4 sib' sib' |
sib'2 sib'8 la' |
sol'2\trill sol'8 sol' |
\appoggiatura sol'8 la'2.~ |
la' |
re''4. re''8 mi'' fa'' |
dod''4 dod'' re'' |
la'2. |
re' |
sol'4 sol'8 la' sib' sol' |
do''2 la'4 |
re''4. re''8 do'' re'' |
sib'4~ sib'4.\trill la'8 |
sol'2 sib'4 |
mib''8 re'' do'' sib' la'4 |
re''8 do'' sib'\trill la' sol'8 la'16 sib' |
do''8 re'' do'' sib' la' sol' |
re''4 re'' re'8 re' |
sol'4. sol'8 la'4 |
sib' fa'4. sib'8 |
sib'2 mib''8 mib'' |
mib''4 mib'' mib'' |
mib''2 mib''8 re'' |
do''2\trill\prall do''8 sib' |
la'2.\trill |
re''4. do''8 sib' la' |
sol'2 sol'8 sol' |
la' sib' la'( sol') la'( fa') |
sib' do'' sib'( la') sib'( sol') |
do''4~ do''4.\trill( sib'16 do'') |
re''2.~ |
re''4. mib''16 re'' do'' sib' la' sol' |
fad'8. mi'16 re'2 |
sol'4. la'8 sib' sol' |
mib''4 mib'' do'' |
re'' re'2 |
sol'2. |
