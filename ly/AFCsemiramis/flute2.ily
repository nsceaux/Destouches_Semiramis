\clef "dessus" <>^"Flutes" re''4 re''8 mi'' fad'' re'' |
sol''2 sol''4 |
mi''8 re'' mi'' fa'' sol'' mi'' |
la'' sol'' la'' sib'' la'' sol'' |
fad'' sol'' fad''4. sol''8 |
sol''2 r4 |
mi''4 mi''8 fa'' sol'' mi'' |
la''2 la''8 sol'' |
fad'' sol'' la'' sol'' fad'' la'' |
re''4. re''8 mi'' fad'' |
sol''2 re''8 mib'' |
mib''4 re'' re'' |
re''2 la''4 |
fad''8 mi'' fad'' sol'' la'' fad'' |
sol''4 sol'' sol'' |
mi''8 re'' mi'' fa'' sol'' mi'' |
fa''4. fa''8 fa''4 |
mib''16( re''8 mib''16) mi''4.\trill fa''8 |
fa''2 re''8 re'' |
re''4 re'' mib'' |
re''2 re''8 re'' |
sol'' la'' sol'' fa'' mi'' re'' |
dod''2 dod''8 re'' |
mi''4 mi'' dod'' |
re''4. fa''8 mi'' re'' |
mi''4 mi'' re'' |
re''( dod''2) |
re''2. |
re''4 re''8 re'' re'' sol'' |
mi''2 la''4 |
fad''4. sol''8 la'' fad'' |
sol'' re'' re''4.\trill do''8 |
sib'8 sib'' sib''4.(\trill la''8) |
sol''2 la''8( sol'') |
fad''2 sol''8( fad'') |
mi''4. mi''8 fad'' sol'' |
fad''2 fad''8 fad'' |
sol''4. fa''8 mib''4 |
re'' do''4.\trill sib'8 |
sib'2 sol''8 sol'' |
sol''4 sol'' lab'' |
sol''2 sol''8 sol'' |
la''4.( sib''8) la''[ sol''] |
fad''2. |
fad''4. re''8 mi'' fad'' |
sol''2 sib'8 sib' |
do''4 do''4.(\trill sib'16 do'') |
re''4 sol''4. sol''8 |
mi''4 mi''4. la''8 |
fad''8( sol'') sol''4.(\trill fad''16 sol'') |
la''2.~ |
la''2 sol''8\trill fad'' |
sol''2 sol''4 |
sol''2 sol''4 |
sol''( fad''4.)\trill sol''8 |
sol''2. |
