\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse-continue #:notes "violon-basse"
                   #:tag-notes basse)
   (chant)
   (silence #:on-the-fly-markup , #{ \markup\tacet#56 #}))
