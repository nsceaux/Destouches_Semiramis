\clef "dessus" <>^"Flutes" fad''4 fad''8 sol'' la'' fad'' |
sib''2 sib''4 |
sol''8 fa'' sol'' la'' sib'' sol'' |
do''' sib'' do''' re''' do''' sib'' |
la'' sib'' la''4.\trill sol''8 |
sol''2 r4 |
sol''4 sol''8 la'' sib'' sol'' |
do'''2 do'''8 sib'' |
la'' sib'' do''' sib'' la'' fad'' |
sol''4. fad''8 sol'' la'' |
sib''2 sib''8 do''' |
la''4 la'' la'' |
la''8 sib'' sol''4 do''' |
la''8 sol'' la'' sib'' do''' la'' |
sib''4 sib'' sib'' |
sol''8 fa'' sol'' la'' sib'' sol'' |
do'''4. do'''8 la''4 |
sol''16( fa''8 sol''16) sol''4.\trill fa''8 |
fa''2 fa''8 fa'' |
fa''4 fa'' sol'' |
fa''2 fa''8 fa'' |
sib'' do''' sib'' la'' sol'' fa'' |
mi''2 mi''8 fa'' |
sol''4 sol'' la'' |
fa'' la''4. la''8 |
sol''4 sol'' fa'' |
mi''8 fa'' mi'' fa'' sol'' la'' |
fad''4 fad''8 sol'' la'' fad'' |
sib''2 sib''4 |
sol''2 do'''4 |
la''4. sib''8 do''' la'' |
sib'' sol'' sol''4.\trill fad''8 |
sol'' re''' re'''4.\trill( do'''8) |
sib''2 do'''8( sib'') |
la''2 sib''8( la'') |
sol''4. sol''8 la'' sib'' |
la''2 la''8 la'' |
sib''4. sib''8 do'''4 |
sib'' la''4.\trill sib''8 |
sib''2 sib''8 sib'' |
sib''4 sib'' do''' |
sib''2 sib''8 sib'' |
do'''2 do'''8( re''' |
do'''4. re'''8 do''' sib'') |
la''4. fad''8 sol'' la'' |
sib''2 sol''8 sol'' |
sol''4 fa''4.\trill fa''8 |
fa''4 sib''4. sib''8 |
sib''4 la''4.\trill la''8 |
la''8( sib'') sib''4.(\trill la''16 sib'') |
do'''2.~ |
do'''2 sib''8\trill la'' |
sib''2 sib''4 |
sib''8.( la''16) la''4.( sib''16 do''') |
sib''4( la''4.\trill) sol''8 |
sol''2. |
