\setMusic #'rondeau {
  sib'4 la'8 sol' |
  la'4 la' sib' la' |
  la'2 re'4 re' |
  mib' mib' re' mib' |
  la2 re'4 fad'8 sol' |
  la'4 la' sib' la' |
  la'2 re'4 re' |
  re' mib' mib' re'8 la |
  sib2
}
\clef "taille" \keepWithTag #'() \rondeau
r2 | R1*7 | r2
%\modVersion\rondeau
r2 | R1*7 | r2
%\modVersion\rondeau
