\setMusic #'rondeau {
  re''4 do''8 re'' |
  fad'4 re'' re'' do''8 sib' |
  la'2 la'4 fad' |
  sol'2 sol'8. la'16 la'8.\trill sol'16 |
  fad'2 re''4 do''8 re'' |
  fad'4 re'' re'' do''8 sib' |
  la'4 fad' la' la' |
  sol' sol' sol' fad' |
  sol'2
}
\clef "haute-contre" \keepWithTag #'() \rondeau
r2 | R1*7 | r2
%\modVersion\rondeau
r2 | R1*7 | r2
%\modVersion\rondeau
