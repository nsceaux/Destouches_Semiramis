\piecePartSpecs
#(let ((breaks #{ s2 s1*7 s2\break s2 s1*7 s2\break #}))
       `((dessus #:music ,breaks)
         (dessus-hc)
         (haute-contre #:music , #{ s2 s1*7 s2\break #})
         (taille #:music , #{ s2 s1*7 s2\break #})
         (basse #:music ,breaks)
         (basse-continue #:music ,breaks)
         (silence #:on-the-fly-markup , #{ \markup\tacet#40 #})))
