\key re \minor \midiTempo#132
\time 2/2 \partial 2
\tempo "Doux & Tendrement"
\override Score.RehearsalMark.outside-staff-priority = #900
\origVersion {
  \beginMarkSmall "Rondeau" s2 \segnoMark s1*7 s2 \fineMark
  s2 s1*7 s2 \bar ":|."
  s2 s1*7 s2 \bar ":|."
}
\modVersion {
  \beginMarkSmall "Rondeau" s2 s1*7 s2 \bar "|." \fineMark
  s2 s1*7 s2 \bar "|." \endMarkSmall "[On reprend le rondeau]"
  %\beginMarkSmall "[Rondeau]" s2 s1*7 s2 \bar "||"
  s2 s1*7 s2 \bar "|." \endMarkSmall "[On reprend le rondeau]"
  %\beginMarkSmall "[Rondeau]" s2 s1*7 s2 \bar "|."
}