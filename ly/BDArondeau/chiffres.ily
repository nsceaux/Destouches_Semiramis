\setMusic #'rondeau {
  s2 <_+>4\figExtOn <_+>\figExtOff <6> <6> <_+>2 <6>4\figExtOn <6>\figExtOff s <6> s8 <6> <6>4 <_+>2 <6>4 <6+> <_+>\figExtOn <_+>\figExtOff
  <6> <6> <_+>2 <6> s4 <6 _-> <7> <7 _+> s2
}

\keepWithTag #'() \rondeau
<5>4 <6 5/> <"">\figExtOn <"">\figExtOff s <6 5/> s2. <6>4
s4 <6> s <7> s2 <_+>4 <6> <_+>\figExtOn <_+>\figExtOff s <6> <_+>4\figExtOn <_+>\figExtOff <6>2 s4 <6> <7> <7 _+> <_+>2
%\modVersion\rondeau
s4 <6 5/> s2. <6 5/>4 s2 <"">4\figExtOn <"">8\figExtOff <6> <6>4 <7> <7 _-> <7-> s2 <6>4 <6+> <6>2 <6>4 <5/>
s2. <6 5/>4 s <5/> s2 <_+>
%\modVersion\rondeau
