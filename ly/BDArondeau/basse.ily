\setMusic #'rondeau {
  <>^"Tous" sol4\doux sol |
  re' do' sib do' |
  re'2 fad4 re |
  mib do sol8 fa mib4 |
  re2 sib4 la8 sol |
  re'4 do' sib do' |
  re'2 fad |
  sol4 do la, re |
  sol,2
}
\clef "basse" \keepWithTag #'() \rondeau
<>^\doux^"Bassons" sib4 la |
sib4 sib, sib la |
sib sib, mib fa |
sol la sib do' |
fa2 la4 sib |
la la, la sib |
la8 sol fa mi fa2 |
sib4 sol mi la |
re2
%\modVersion\rondeau
<>\doux^"Bassons" sol4 fad |
sol4 sol, sol fad |
sol sol, sol fa8 mib |
re4 sol do fa |
sib, r sib la8 sol |
fad4 re sib fad |
sol sol, sol fad |
sol fad sol sol, |
re2 
%\modVersion\rondeau
