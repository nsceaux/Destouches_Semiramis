\setMusic #'rondeau {
  <>\doux^"Tous" sol''4 la''8 sib'' |
  la''4 fad'' sol'' mi''8.( fad''16) |
  fad''4( re''8) do'' re'' do'' sib'\trill la' |
  sol'4. la'8 sib'8.( do''16) do''8.(\trill sib'32 do'') |
  re''2 sol''4 la''8 sib'' |
  la''4 fad'' sol'' mi''8.( fad''16) |
  fad''4 re''8 do'' re'' mib'' re'' do'' |
  sib'4\trill la' do'' la'\trill |
  sol'2
}
\clef "dessus" \keepWithTag #'() \rondeau
<>\doux^"Hautbois" re''8( mib'') fa''4 |
re'' sib' fa''8( sol'') fa''( mib'') |
re''4 sib' sol''( fa''8)\trill mib'' |
re''4\trill do'' re''8.( mi''16) mi''8.(\trill re''32 mi'') |
fa''2 mi''4 re'' |
dod''4 la' mi''8( fa'') re''( mi'') |
dod''4 la' la''8 sol'' fa''\trill mi'' |
re''4 mi'' sol'' mi''\trill |
re''2
%\modVersion\rondeau
<>\doux^"Hautbois" sib'8( do'') re''4 |
sib'\trill sol' sib'8 do'' re'' mib'' |
re''4 sol' sol'' re''8 mib'' |
fa''4 re'' mib'' do''\trill |
sib'8.( do''16) do''8.\trill( sib'32 do'') re''2~ |
re''2 re''4 do''8 re'' |
sib'4\trill sol' re''8( mib'') re''( do'') |
sib'4\trill la' sib' la'16 sol'8 la'16 |
la'2\trill
%\modVersion\rondeau
