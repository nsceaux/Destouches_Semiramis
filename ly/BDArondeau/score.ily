\score {
  \new StaffGroup <<
    \new Staff <<
      \global \includeNotes "dessus"
      { s2 s1*7 s2^\markup\translate #'(1 . 0) \fontsize#1 \italic Fin. }
    >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff <<
      \global
      \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s1*6\break s1*7\break s1*5\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
