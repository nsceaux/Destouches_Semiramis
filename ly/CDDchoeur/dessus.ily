\clef "dessus2" <>^"Violons" fa'4 |
fa' fa' sib' |
fa'2 fa'4 |
mib'2 mib'4 |
re' re' mib' |
fa'2 fa'4 |
sol'2 mib'4 |
fa' re' re' |
do'2 fa'4 |
fa'2 mi'4 |
fa'2 fa'4 |
fa'2. |
fa'4 fa' fa' |
sol'2 sol'4 |
mib' mib' mib' |
fa' fa' fa' |
re' re' sol' |
sol'2 sol'4 |
sol' sol' lab' |
sol'2 sol'4 |
sol' la' sib' |
la' la' sib' |
sib'2 la'4 |
sib' sol' mib' |
mib'2 re'4 |
re'2 do'4 |
re'2. |
re'4 re' re' |
mi' mi' mi' |
sol' sol' sol' |
la'2 fa'4 |
fa' fa' sol' |
fa'2 fa'4 |
sol' mib' do' |
re' re' fa' |
fa' fa' sol' |
fa'2 fa'4 |
sol' mib' do' |
re' re'
