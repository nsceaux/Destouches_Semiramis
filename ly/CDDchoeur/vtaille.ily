\clef "vtaille" do'4 |
re' re' fa' |
re'2 re'4 |
do'2 do'4 |
sib sib do' |
re'2 re'4 |
mib'2 do'4 |
re' sib sib |
la2 do'4 |
do'2 do'4 |
do'2 do'4 |
do'2. |
re'4 re' re' |
mib'2 mib'4 |
do' do' do' |
re' re' re' |
si si re' |
mib'2 do'4 |
mi' mi' fa' |
mi'2 mi'4 |
mi' fad' sol' |
fad' fad' sol' |
sol'2 fad'4 |
sol' re' sib |
do'2 la4 |
sib2 sol4 |
la2. |
si4 si si |
do' do' do' |
do' do' mi' |
fa'2 do'4 |
re' re' mib' |
re'2 re'4 |
sib sib la |
sib sib do' |
re' re' mib' |
re'2 re'4 |
sib sib la |
sib sib
\set Staff.whichBar = "|."
