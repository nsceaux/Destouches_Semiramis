\clef "vbasse" fa4 |
sib sib re' |
sib2 sib4 |
fa2 fa4 |
sol sol sol |
re2 re4 |
mib2 mib4 |
sib,4 sib, sib, |
fa2 fa4 |
do2 do4 |
fa2 fa4 |
fa2. |
sib4 sib sib |
mib2 mib4 |
lab lab lab |
re re re |
sol sol sol |
do2 sol4 |
do' do' do' |
do'2 do'4 |
sib la sol |
re' re' sol |
sol2 re4 |
sol sol sol |
fa2 fa4 |
mib2 mib4 |
re2. |
sol4 sol sol |
do do do |
mi mi do |
fa2 fa4 |
sib sib sib |
sib2 re4 |
mib do fa |
re sib, fa |
sib sib sib |
sib2 re4 |
mib do fa |
re sib,
\set Staff.whichBar = "|."
