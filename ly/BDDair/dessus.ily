\clef "dessus" <>^\markup\whiteout Hautbois
sib'4 do'' |
re''( mib'') re''( do'') |
sib'\trill( la'8) sib' sol'4 sib' |
la' sib'8( do'') do''4.( sib'16\trill la') |
la'2\trill re''4 mib'' |
fa''( sol'') fa''( mib'') |
re''\trill do'' fa'' mib'' |
re''\trill do'' mib'' do''\trill |
sib'2 re''4( mib'') |
re'' sol' do''( sib'8)\trill la' |
sib'2 do''4( sib'8)\trill la' |
sol'4 do''8( sib') la'4.\trill sol'8 |
fad'4 sol' la' sib' |
do'' la' re''( do''8)\trill sib' |
do''2 sib'4( do''8) re'' |
fad'4 sol'8 la' la'2\trill |
sol'
