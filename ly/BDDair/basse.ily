\clef "basse" <>^\markup\whiteout Bassons
sol4 la |
sib( do') sib( la) |
sol re mi sol |
fad re sol sol, |
re2 sib,4 do |
re mib re do |
sib, mib re do |
sib, mib do fa |
sib,2 sib4 fad |
sol sol, sol fad |
sol sol, fad re |
mib do8 re mib4 do |
re mib8 re do4 sib, |
la, re8 do sib,4 la,8 sol, |
fad,4 re, sol,2 |
la,4 sib,8 do re4 re, |
sol,2
