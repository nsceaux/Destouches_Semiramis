\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s2 s1*7 s2\break \bar "" }
    >>
  >>
  \layout { }
  \midi { }
}