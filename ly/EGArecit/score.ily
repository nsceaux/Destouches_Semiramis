\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2 s2 \bar "" \break s1 s1.\break s1 s1.\break s1. s1 s2 \bar "" \break
      }
    >>
  >>
  \layout { }
  \midi { }
}
