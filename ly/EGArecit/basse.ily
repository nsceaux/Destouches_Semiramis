\clef "basse" do4 do' si sol |
la1 |
mi4. fa8 sol2 fad |
sol1 fa2 |
mi1 |
fa mi2 |
re1 dod2 |
re si, |
do re4 re, |
sol,1 |
