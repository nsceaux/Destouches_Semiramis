Vous vi -- vrez, ma Prin -- cesse, & le Ciel par mes coups
A vou -- lu sau -- ver tant de char -- mes :
Un Dieu me con -- dui -- soit, un Dieu gui -- doit mes ar -- mes.
Je ne vois point la Rei -- ne, al -- lons à ses ge -- noux
Ex -- pi -- er mon au -- dace, & cal -- mer ses al -- lar -- mes.
