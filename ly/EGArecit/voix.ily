\arsaneMark r4 mi'8 fa' re'4\trill\prall re'8 mi' |
\appoggiatura re'8 do'4 do'8 re' mi'4\trill mi'8 do' |
sol'2 re'8 do' re' fa' re'4\trill\prall mi'8 do' |
si2\trill si4 r8 re' re' si do' re' |
sol4. sol'8 mi'\trill re' do' si |
la4\trill la r8 do' do' do' dod'4. re'8 |
re'4 re' r r8 re' mi' mi' fad' sol' |
fad'4 re'8 re' sol'4 sol'8 re' |
\appoggiatura re'8 mi'4 do'8 mi' la4 si8 do' |
si2 si4 r |
