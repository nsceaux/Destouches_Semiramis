\key re \minor \midiTempo#80
\digitTime\time 3/4 s2.*4
\time 3/2 \grace s8 s1.
\time 3/8 \grace s16 s4.*14
\bar ".!:" s4.*6 \alternatives s4. s4. \bar ":|."
