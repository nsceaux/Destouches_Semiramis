\clef "vbasse" sib4 do'8 re' la\trill\prall sib |
sol4 do'8 do'16 sib la8 sol |
fad2 la4 |
re2 re'4 |
\appoggiatura do'8 sib4. sib8 sol sol sol mib fa4. fa8 |
\appoggiatura fa16 sib,4 r16 sib |
fa4 r16 fa |
do[ re mib fa sol la]( |
sib8.) sol16 sib sol |
do'4 do'16 sib |
la8 la sol |
re' re' re'16 do' |
sib8 do' re' |
mib'4 do'8 |
la8.\trill sol16 fad sol |
re8 re re'16 do' |
sib8 do' re' |
mib'4 do'8 |
re'8. re'16 re' re |
sol8 sol r |
R4.*6 |
