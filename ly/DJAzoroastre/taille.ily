\clef "taille" sol2 la8 re' |
re'4 mib' sol' |
re'2 la'4 |
sib'2 re'4 |
mib'2 sib fa' |
re'4 r16 fa' |
fa'4 r16 re' |
do'4 r16 la' |
fa'8. sol'16 sol' sol' |
sol'4 sol'16 sol' |
la'8 la' re' |
re' re' re'16 re' |
sib'4 la'8 |
sol'4 sol'8 |
la'4 la8 |
la4 re'8 |
re'4 re''8 |
sib'4 la'8 |
la' re' re' |
re'4 sol'8 |
fa'4 fa'8 |
mib'4 sol'8 |
sol'4 mi'8 |
mi' re' la' |
sib' fad'8. sol'16 |
sol'4. |
sol'4. |
