\clef "haute-contre" sib'2 re'8 fad' |
sol'2 la'4 |
la'2 re''4 |
re''2 sol'4 |
sol'2 sol' fa' |
fa'4 r16 sib' |
la'4 r16 si' |
do''4 r16 do'' |
sib'8. sib'16 sib' re'' |
do''4 do''16 do'' |
do''8 do'' sib' |
la'8 la' fad'16 fad' |
sol'8 do'' do'' |
sib'4 do''8 |
fad'8. sol'16 re'8 |
re' re' fad'16 fad' |
sib'4 la'8 |
sol'4 do''8 |
fad'8. fad'16 fad' sol' |
sol'4 sib'8 |
sib'4 sib'8 |
sib'4 re''8 |
do''4 sol'8 |
la'4 re''8 |
re''8 re''8. la'16 |
sib'4. |
sib'4. |
