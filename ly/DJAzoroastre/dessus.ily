\clef "dessus" re''2^"Violons" \doux do''8 re'' |
sib'4 mib''8 mib''16 re'' do''8 sib' |
la'2 fad''4 |
sol''2 sib'4 |
sol'4. re''8 sib'2 sib'4 la' |
sib'4 r16 re'' |
do''4 r16 re'' |
mib''4 r16 mib'' |
re''8. re''16 re'' sol'' |
mi''4 mi''16 mi'' |
fad''8 fad'' sol'' |
fad'' fad'' la'16 la' |
re''8 mi'' fad'' |
sol''4 mib''8 |
do''8. sib'16 la' sib' |
fad'8 fad' la'16 la' |
re''8 mi'' fad'' |
sol''4 mib''8 |
la'8. la'16 la' sib' |
sol'\fort la' sib' do'' re'' mi'' |
fa'' mib'' re'' mib'' fa'' re'' |
sol'' la'' sib'' la'' sol'' fa'' |
mi'' fa'' sol'' la'' sib'' sol'' |
do''' sib'' la'' sol'' fad'' la'' |
sol'' la''32 sib'' la''8.\trill sol''16 |
sol''4. |
sol''4. |
