\clef "basse" sol2 fad8 re |
mib4 do2 |
re do4 |
sib,2 sib,4 |
mib1 fa2 |
sib,4 r16 sib |
fa4 r16 fa |
do re mib fa sol la |
sib8. sol16 sib sol |
do'4 do'16 sib |
la8 la sol |
re' re' re'16 do' |
sib8 do' re' |
mib'4 do'8 |
la8. sol16 fad sol |
re8 re re'16 do' |
sib8 do' re' |
mib'4 do'8 |
re'8. re'16 re' re |
sol,4 sol8 |
re4 re8 |
mib4 si,8 |
do4 sib,8 |
la,4 re8 |
sib,16 do re8 re, |
sol,4.*5/6~ \hideNotes sol,16 | \unHideNotes
sol,4. |
