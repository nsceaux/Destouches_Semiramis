\clef "vbasse" sol4 |
sib sib do'4. do'8 |
re'2. sib4 |
sol2 la4 la8 la, |
re2 re4 r8 re |
sol4. sol8 mib4 do |
fa2. re4 |
mib2 fa4 fa8 fa |
sib4 sib fa fa |
do do' sol sol |
re sib do'4. do'8 |
re'2. sib4 |
do'2 re'4 re'8 re |
sol2 sol4 r |
