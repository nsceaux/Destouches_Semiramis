\clef "haute-contre" sol'4 |
sol' sol' la'4. la'8 |
fad'2. sol'4 |
sib'2 la'4 la'8 la' |
la'2 la'4 r8 fad' |
sol'4. sol'8 sol'4 sol' |
la'2. sib'4 |
sol'2 fa'4 fa'8 fa' |
fa'4 fa' fa' fa' |
sol' sol' sol' sib' |
la' sol' la'4. la'8 |
la'2. sib'4 |
sib'2 la'4 la'8 la' |
sib'2 sib'4 r |
