\clef "vdessus" sib'4 |
re'' re'' mib''8[ re''] do''[ sib'] |
la'2. re''4 |
mi''2 mi''4 fad''8 sol'' |
fad''2 fad''4 r8 re'' |
re''4. re''8 sol''[ fa''] mib''[ re''] |
do''2. fa''4 |
sol''2 do''4 do''8 fa'' |
re''4 re'' do'' re'' |
mib'' mib'' re'' sol'' |
fad'' re'' mib''8[ re''] do''[ sib'] |
la'2. re''4 |
mi''2 fad''4 fad''8 fad'' |
sol''2 sol''4 r |
