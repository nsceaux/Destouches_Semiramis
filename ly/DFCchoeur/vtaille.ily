\clef "vtaille" re'4 |
re' re' do'4. mib'8 |
re'2. re'4 |
re'2 re'4 re'8 dod' |
re'2 re'4 r8 la |
sib4. sib8 sib4 do' |
do'2. re'4 |
sib2 sib4 sib8 la |
sib4 sib la si |
do' do' sib re' |
re' sol' sol'4. sol'8 |
fad'2. sol'4 |
sol'2 re'4 re'8 re' |
re'2 re'4 r |
