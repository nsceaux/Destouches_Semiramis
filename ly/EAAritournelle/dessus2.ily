\clef "dessus" <>^\markup\whiteout "Violons" R1*2 |
r2 re'' |
do''4 sib' la' do'' |
sib' sol' mib''2 |
re'' re''\trill |
do''4 sib' la' sol' |
fad'2 re'4 mi' |
fad' sol' la' sol'8\trill fad' |
sol'2 sol'' |
fa''4 mib'' re'' fa'' |
mib'' do'' do'' re'' |
mi''! fa'' sol'' fa''8 mi'' |
fa''4 fa' sib' sib' |
sib'2 la' |
sib'4 fa' re''2 |
do'' do''\trill |
sib' re'' |
do''4 sib' la' do'' |
sib' la' sib' do'' |
la' fad' fad' sol' |
la' sib' do'' sib'8 la' |
sib'4 re''2 do''4~ |
do'' sib'2 la'4~ |
la' sol' sol' fad' |
sol'2 sol'4 la' |
si' do'' re'' do''8 si' |
do''2 r |
do''4 sib' la' sol' |
fad' sol'8 la' fad'4.\trill sol'8 |
sol'2 r |
do''4 sib' la' sol' |
fad' sol'8 la' fad'4.\trill sol'8 |
sol'1 |
