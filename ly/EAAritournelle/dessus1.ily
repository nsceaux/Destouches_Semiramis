\clef "dessus" <>^"Violons" r2 re'' |
do''4 sib' la' do'' |
sib' sol' sib'2 |
la' la'\trill |
sol' sol'' |
fa''4 mib'' re'' fa'' |
mib'' re'' do'' sib' |
la'2 fad'4 sol' |
la' sib' do''4 sib'8\trill la' |
sib'2 do''4 do'' |
do''2 si' |
do'' mi''!4 fa'' |
sol'' la'' sib'' la''8 sol'' |
la''2 fa'' |
mib''4 re'' do'' mib'' |
re'' sib' fa''2 |
mib'' mib''\trill |
re'' sib'' |
la''4 sol'' fad'' la'' |
sol'' fad'' sol'' la'' |
fad'' re'' re'' mi'' |
fad'' sol'' la'' sol''8\trill fad'' |
sol''2 sol''4 lab''8( sol'') |
fa''4( mib''8\trill re'') mib''2 |
re'' do''\trill |
sib' si'4 do'' |
re'' mib'' fa'' mib''8 re'' |
mib''2 r |
mib''4 re'' do'' sib' |
la' sib'8 do'' la'4.\trill sol'8 |
sol'2 r |
mib''4 re'' do'' sib' |
la' sib'8 do'' la'4.\trill sol'8 |
sol'1 |
