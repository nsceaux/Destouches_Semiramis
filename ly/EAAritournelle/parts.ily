\piecePartSpecs
#`((dessus #:score "score-dessus")
   (dessus2-hc)
   (basse-continue)
   (silence #:on-the-fly-markup , #{ \markup\tacet#34 #}))
