\clef "basse" R1*7 |
r2 re |
do4 sib, la, do |
sib, sol, mib2 |
re re\trill |
do do' |
sib4 la sol sib |
la fa re'2 |
do' do'\trill |
sib re4 mib |
fa sol la fa |
sib2 sib,4 sol, |
do2 re |
mib do |
re re' |
do'4 sib la do' |
sib sol mib'2 |
re' do' |
sib la |
sol sol |
fa4 mib re fa |
mib do mib re |
do re mib do |
re do re re, |
sol,2 mib4 re |
do re mib do |
re do re re, |
sol,1 |
