\clef "dessus"
\setMusic #'rondeau {
  sol''4 |
  mi''4.\trill re''8 do''4 mi'' |
  re'' sol'2 sol'4 |
  do'' do''8 re'' mi'' fa'' mi'' fa'' |
  sol''2 re''4 sol'' |
  mi''4. re''8 do''4 mi'' |
  re'' sol' sol'' sol'' |
  sol''8 fa'' mi'' fa'' re''4.\trill do''8 |
}
\keepWithTag #'() \rondeau do''2.
<<
  \tag #'trompette { r4 R1*7 r2 r4 }
  \tag #'dessus {
    <>^"Violons" mi''4 |
    do''4.\trill si'8 la'4 la'' |
    fad''4. mi''8 re''4 re'' |
    si'8 do'' si' la' sol'4 sol'' |
    mi''4.\trill re''8 do''4 do'' |
    fa''4.( sol''8) sol''4.(\trill fa''16 sol'') |
    la''4. sol''8 fa'' mi'' re'' si' |
    do''4 re''8 mi'' mi''4.\trill re''8 |
    re''2. <>^"Tous"
  }
>>
\rondeau do''2.
<<
  \tag #'trompette { r4 R1*7 r2 r4 }
  \tag #'dessus {
    <>^"Violons" mi''4 |
    fad''4. sol''8 fad'' sol'' la'' si'' |
    sol''4.\trill fad''8 mi'' fad'' sold'' mi'' |
    la''2 sold''4.(\trill fad''16 sold'') |
    la''4. sol''16 fa'' mi''4 fa'' |
    sol''4. fa''16 mi'' re''4 mi'' |
    fa''4 mi''8\trill re'' do'' re'' do'' re'' |
    mi'' fa'' mi'' fa'' fa''4.\trill mi''8 |
    re''4.\trill do''8 si'4 <>^"Tous"
  }
>>
\rondeau do''1
  