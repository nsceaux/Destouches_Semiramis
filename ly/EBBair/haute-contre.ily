\clef "haute-contre"
\setMusic #'rondeau {
  sol'4 |
  sol'2. sol'4 |
  sol'2 do''4 mi'' |
  re''4. do''8 re''4 si' |
  do''2 sol'4 do'' |
  sol'2 re''4 re'' |
  do'' do'' si'4.\trill do''8 |
}
r4 |
r2 r4 \keepWithTag #'() \rondeau |
do''2. sol'4 |
la'2 la'4 dod'' |
re''2 la'4 fad' |
sol'2 sol'4 si' |
do''2 sol'4 sol' |
la' do'' do''4. do''8 |
do''2 do''4 fa' |
sol'4 sol' sol'4. do''8 |
si'2. r4 |
r2 r4 \rondeau |
do''2. do''4 |
do''2. si'4 |
si'4. la'8 sold' la' si' sold' |
la'4. si'8 si'4.(\trill la'16 si') |
do''2 do''4. re''8 |
mi''4. re''16 do'' si'4 do'' |
re''4 do''8 si' la' si' la' si' |
do'' re'' do'' re'' re''4.\trill do''8 |
si'4. la'8 sol'4 si' |
do''2 sol'4 \rondeau |
do''1 |
