\setMusic #'rondeau {
  <6>2.\figExtOn <6>4\figExtOff <6>4.\figExtOn <6>8\figExtOff s2 <6>2.\figExtOn <6>4\figExtOff s1 <6>2.\figExtOn <6>4\figExtOff <6>1 s2.
}
s4 s1 \keepWithTag #'() \rondeau
s4 s2 <6> <_+>2 <6> s <6> <"">2.\figExtOn <"">4\figExtOff <6>1 s2. <6 4>4 <6> <6> s2
<"">2.\figExtOn <"">4\figExtOff s1 \rondeau
s4 <7 _+>2. <6>4 <_+>2\figExtOn <_+> <6>4 <6>\figExtOff <_+>2
s1*3 s2 <5/> s1*2 \rondeau
