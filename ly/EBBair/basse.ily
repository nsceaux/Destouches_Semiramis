\clef "basse"
\setMusic #'rondeau {
  do'4 |
  si4. la8 sol4 sol |
  mi8 fa mi re do4 do |
  si,4. la,8 si,4 sol, |
  do2. do'4 |
  si4. la8 sol4 sol |
  mi do sol sol, |
}
r4 | r2 r4 \keepWithTag #'() \rondeau do2.
do4 |
la,4. si,8 dod4 la, |
re re8 mi fad4 re |
sol4 sol8 la si4 sol |
do'2 do'4 sib |
la fa do' do |
fa2. fa4 |
mi si, do2 |
sol,4. fad,8 sol, la, si, sol, |
do2. \rondeau do2.
do4 |
re2. re4 |
mi2 re |
do4 re mi mi, |
la,2 la4. sol16 fa |
mi4 fa sol4. fa16 mi |
re4 mi fa mi8 re |
do2 si,4 do |
sol,8 fa, sol, la, si,4 sol, |
do2. \rondeau do1
