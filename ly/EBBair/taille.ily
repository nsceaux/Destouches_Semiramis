\clef "taille"
\setMusic #'rondeau {
  do'4 |
  re'2. re'4 |
  mi'2 mi'4 do' |
  sol'2 sol'4 re' |
  mi'2. sol'4 |
  sol'2 si4 si |
  mi' sol' sol'4. fa'8 |
}
r4 |
r2 r4 \keepWithTag #'() \rondeau |
mi'2. mi'4 |
mi'2 mi'4 la |
la2 la4 la |
si2 re'4 sol' |
sol'2 mi'4 mi' |
do'4 fa' mi'4.\trill fa'8 |
fa'2. re'4 |
mi' re' do'4. sol8 |
sol2. r4 |
r2 r4 \rondeau |
mi'2. la'4 |
la'2. la'4 |
mi'2 mi' |
mi'4 fa' mi'4. mi'8 |
mi'2 mi'4. re'8 |
do'4 re' re' la' |
la'2 fa'4. fa'8 |
sol'2 sol'4 sol' |
sol'2 sol'4 sol' |
sol'2. \rondeau |
mi'1 |
