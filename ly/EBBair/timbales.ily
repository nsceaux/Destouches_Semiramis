\clef "basse"
\setMusic #'rondeau {
  do4 |
  sol,4. sol,8 sol,4 sol, |
  do4. do8 do4 do |
  sol,2 sol,4 sol, |
  do2. do4 |
  sol,4. sol,8 sol,4 sol, |
  do4 do sol,4. do8 |
}
r4 | r2 r4 \keepWithTag #'() \rondeau do2.
r4 R1*8 r2 r4 \rondeau do2.
r4 R1*8 r2 r4 \rondeau do1
