\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff <<
        <>^"Trompette"
        \global \keepWithTag #'trompette \includeNotes "dessus"
      >>
      \new Staff <<
        <>^\markup\whiteout "Tymballes"
        \global \includeNotes "timbales"
      >>
    >>
    \new StaffGroupNoBracket <<
      \new Staff << \global \keepWithTag #'dessus \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff <<
        \global \includeNotes "basse"
        \includeFigures "chiffres"
        \origLayout {
          s4 s1*7\break s1*7\pageBreak
          s1*6\break s1*6\break s1*7\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
