\clef "basse" <>^"Basses de violons"
re'2 re'4 |
sol8 fa sol la sib sol |
re'2 re4 |
la8 sol la sib la sol |
fa4 sol la |
re2 re4 |
sol2 sol4 |
la8 sib la sol fa4 |
sol la la, |
re2 %{%} la4 |
re'2 re4 |
sol fa mi |
la fa sol |
do2 do4 |
re2 re4 |
mi2 do4 |
re mi mi, |
la,2 la4 |
re'2 re4 |
sol2 sol4 |
do'2 do4 |
fa2 fa4 |
sib8 do' sib la sol fa |
do'4 sib la |
sib do' do |
fa8 sol la sol fa re |
