\clef "vdessus"
<<
  \tag #'(vdessus1 basse) {
    <>^\markup\character { Chœur des Pretresses }
    fa''4 fa''8 fa'' mi'' fad'' |
    \appoggiatura fad''?8 sol''2 sol''4 |
    fa''4.\trill\prall mi''8 fa'' sol'' |
    mi''2.\trill |
    fa''4. mi''8 re'' dod'' |
    re''4 la' fa'' |
    mi''4.\trill fa''8 re''4 |
    dod''2\trill re''4~ |
    re''8 mi'' mi''4.\trill re''8 |
    re''2 %{%} mi''4 |
    fa''2 mi''8 fa'' |
    re''4 re'' mi'' |
    mi''4. fa''8 \appoggiatura mi''16 re''8.[ mi''16] |
    mi''2\trill mi''4 |
    re''4.\trill mi''8 do''4 |
    si'2 mi''4 |
    re''4.\trill\prall do''8 si'4 |
    do''4 la' mi''8 la'' |
    fad''2( mi''8) fad'' |
    sol''2 sol''4 |
    mi''4.\trill\prall re''8 mi''4 |
    fa'' do'' fa'' |
    re''2 mi''8 fa'' |
    mi''4 do'' fa''~ |
    fa''8 sol'' mi''4.\trill fa''8 |
    fa''2. |
  }
  \tag #'vdessus2 {
    re''4 re''8 do'' sib' la' |
    \appoggiatura la'8 sib'2 sib'4 |
    la'4. la'8 re'' mi'' |
    dod''2. |
    la'4. sol'8 fa' mi' |
    fa'4 re' la' |
    sol'4.\trill la'8 fa'4 |
    mi'2 la'4~ |
    la'8 si' dod''4.\trill re''8 |
    re''2 %{%} dod''4 |
    re''2 do''8 re'' |
    si'4 si' do'' |
    do''4. re''8 si'4 |
    do''2 do''4 |
    si'4.\trill do''8 la'4 |
    sold'2\trill do''4 |
    si'4.\trill\prall la'8 sold'4 |
    la' la' la'8 do'' |
    do''2( sib'8) la' |
    sib'2 sib'4 |
    sol'4.\trill\prall fa'8 sol'4 |
    la' fa' la' |
    fa'2 sol'8 la' |
    sol'4 mi' do''~ |
    do''8 re'' sol'4.\trill fa'8 |
    fa'2. |
  }
>>
