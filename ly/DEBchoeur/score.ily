\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      { s2.*9 s2.^\markup\translate #'(1.5 . 0) \fontsize#1 \italic Fin. }
      \global \keepWithTag #'vdessus1 \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vdessus2 \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*5\break s2.*6\break s2.*6\break s2.*5\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
