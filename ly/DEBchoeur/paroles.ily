Fil -- le de l’In -- no -- cen -- ce,
Me -- re de la Paix,
Douce in -- dif -- fe -- ren -- ce,
Nos cœurs sa -- tis -- faits
Goû -- tent vos at -- traits ;
Des jours sans nu -- a -- ge
S’é -- le -- vent sur nous.
Les biens les plus doux
Sont nô -- tre par -- ta -- ge.
Ce n’est pas à vous
Qu’on doit son hom -- ma -- ge,
For -- tu -- ne vo -- la -- ge,
Nous bra -- vons vos coups.
