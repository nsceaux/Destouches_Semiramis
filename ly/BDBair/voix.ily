\clef "vhaute-contre"
<>^\markup\translate #'(-1 . 1) \line {
  \characteri "Un Genie" Ordonnateur de la Fête.
}
sol'2 fad'4 |
sol'2. |
mib'4 re'4. do'8 |
sib2\trill la4 |
sib8 do' do'4.(\trill sib16[ do']) |
re'4. mi'8 fa'4 |
mi' fad'4. sol'8 |
fad'4. mi'8 fad'4 |
sol'4. fad'8 sol'4 |
re'2 sol'8 fa' |
mi'8[ fa'] sol'[ fa'] mi'[ re'] |
mi'2 dod'4 |
re'4. dod'8 re'4 |
mi'4.\trill fa'8 sol'4 |
fa'4.\trill sol'8 mi'4\trill |
re'2. |
re' |
sol'4 fad' sol' |
la'2 re'4 |
fa'2 sol'4 |
mi'\trill\prall do' r8 mib' |
re'4.\trill re'8 mib' fa' |
\appoggiatura fa'8 sol'2 sol'8 la' |
\appoggiatura la'8 sib'4. la'8 sol'8.[ la'16] |
la'4\trill fa' r8 sib' |
\appoggiatura la'8 sol'4. fa'8 mib'16[ re'8.] |
do'16[ sib8 do'16] do'4.\trill sib8 |
sib4. mi'8 fa'4 |
mi' fad'4. sol'8 |
fad'2 re'4 |
la'2. |
sib'8 mi' fad'4.\trill( mi'16[ fad']) |
sol'2. |
sol'2
