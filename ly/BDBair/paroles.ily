L’Art plus prompt que la Na -- tu -- re,
Dans ces beaux lieux ras -- semble en mê -- me tems,
Et des Fleurs & des Fruits la ri -- an -- te pa -- ru -- re ;
On voit l’Au -- tomne à cô -- té du Prin -- tems.
- tems.
Ai -- ma -- ble maî -- tre de nos a -- mes,
A -- mour, fe -- rois- tu moins en fa -- veur de nos flâ -- mes ?
Fai naître, & com -- ble nos dé -- sirs,
Ras -- semble en mê -- me tems l’es -- poir & les plai -- sirs.
- sirs.