\clef "basse" sol2 la4 |
sib8 do' sib la sib sol |
do'4 re' re |
sol sol, re |
sol8 la la4.(\trill sol16 la) |
sib2 la8 sib |
do' sib la4 sol |
re'2 do'4 |
sib4. la8 sol4 |
fad4 sib8 do' sib la |
sol4 la sib |
la8 sol la sib la sol |
fa4 mi re |
dod8 re dod si, la,4 |
re4 sol, la, |
re, re8 mi fad re |
re4. mi8 fad re |
sib4 la sol |
fad2 sol4 |
si,4. la,8 si, sol, |
do4. sib,8 la, fa, |
sib,4 sib8 lab sol fa |
mib2. |
mi! |
fa4. mib8 re sib, |
mib4. fa8 sol4 |
re8 mib fa4 fa, |
sib,4. sib8 la sib |
do' sib la4 sol |
re'8 mib' re' do' sib sol |
do' re' do' sib la re' |
sol do re4 re, |
sol, sol8 fad sol la |
sol,2
