\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*6\break s2.*5\break s2.*6\break s2.*6\pageBreak
        \grace s8 s2.*5\break
      }
    >>
  >>
  \layout { }
  \midi { }
}