Ar -- rê -- tez. Les En -- fers sont prêts à m’ins -- pi -- rer,
Qu’en ce jour sur vos soins je puis -- se m’as -- su -- rer.
Quel noir trans -- port suc -- cede à ma dou -- leur pro -- fon -- de !
Le Styx qui fait les loix & les cri -- mes des Dieux,
Le Styx se dé -- couvre à mes yeux.
Quels fu -- nes -- te se -- crets me re -- ve -- le son on -- de !
Mal -- heu -- reu -- se Se -- mi -- ra -- mis,
Trem -- ble ! de tes fu -- reurs que le Des -- tin con -- dam -- ne,
Ton Fils est é -- cha -- pé, je le vois, c’est Ar -- sa -- ne...
Mais ! quel spec -- tacle af -- freux trouble en -- cor mes es -- prits !
Le glaive est sus -- pen -- du, Quelle il -- lus -- tre vic -- ti -- me
Va se pré -- ci -- pi -- ter au te -- ne -- breux a -- bî -- me ?
Quel sang prêt à cou -- ler ? de quels lu -- gu -- bres cris
Les Au -- tels re -- ten -- tis -- sent ?
Le Peuple est cons -- ter -- né, les Dieux même en fre -- mis -- sent.
Et je sens que je m’at -- ten -- dris.
Eh ! qui donc va pe -- rir ? est- ce vous A -- mes -- tris ?
Tout fuit... Quel -- le con -- fuse i -- ma -- ge !
Je ne vois plus qu’à tra -- vers un nu -- a -- ge...
Je suis van -- gé. Je vois des mal -- heu -- reux...
De pleurs, de cris, de sang mar -- quons ce jour af -- freux.
