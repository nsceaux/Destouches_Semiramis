\clef "dessus" R1*3 R1. R1*2 |
<>^"Violons" r4 sol' sol' lab' |
sol'4.*13/12 sib'32 do'' re'' mib''2~ |
mib''4.*13/12 mib''32 re'' do'' re''2~ |
re''4.*13/12 re''32 do'' sib' do''2~ |
do'' r4 sib'32 do'' re'' do'' sib' lab' sol' fa' |
mib'4. mib'8 mib'4.\trill re'8 |
re'4 lab'\doux lab' lab' |
sol' sol' sol' sol' |
fa' fa' fa' re'' mib'' fa'' |
sib' sib' sib' do'' |
fa'' fa'' sol'' re'' |
mib'' mib''8 mib'' do''4 do''8 re'' |
la'4 la' la' la' |
do'' do'' do'' re''8 do'' |
sib'4 sib' sib' do'' |
la'2 r8 do''32\fort re'' mib'' re'' do'' sib' la' sib' do'' sib' la' sol' |
fa'2 fa''4\doux fa''8 fa'' |
sib'4 sib'8 mib'' mib''4 mib''8 re'' |
mib''4 r16 sol''\fort sol'' sol'' mib'' mib'' mib'' mib'' sib' sib' sib' sib' |
sol'2 fa'\doux |
mib'4.*9/12 sol'32 la' sib' do'' re'' mib'' fa'' sol''2~ sol''4.*9/12 sol''32 fa'' mib'' re'' do'' si' la' |
sol'4 r sol'\doux r mib'' mib'' |
la'2 fad' |
r16 sol'\fort sol' la' sib' sol' re'' re'' re''4 r |
r8 la''16 sib'' la'' sol'' fad'' mi'' re''4 r |
re'' r r16 re'' mib'' fa'' sol'' lab'' sol'' fa'' |
mib'' fa'' mib'' fa'' sol'' fa'' mib'' re'' do'' re'' do'' re'' mib'' re'' do'' sib' |
la'2 lab'\doux sol' sol'4 do'' |
do''2 sib'4 sib' fa' fa'8 sib' |
mi'2 r4 mi' |
fa'4. fa'8 fa'4 fa' |
mi'4~ mi'32\fort sol' la' sib' do'' re'' mi'' fa'' sol''1\doux |
do''2 r |
do''2 do''4 do''8 do'' fa'4 fa' |
sol'2. sol'4 |
sol'2 sol'4 sol'8 mi' |
fa'4.*9/12 fa'32\fort sol' la' sib' do'' re'' mib'' fa''2 do''\doux |
re'' sib' |
sol' lab' |
sol' mi' |
fa'2. la'4 |
sib' sib'8 sib' re''4 mib''8 fa'' |
sib'2 sol'4. sib'8 |
sib'2 la'4. sib'8 |
sib'2 mib''8\doux re'' do'' sib' |
la' sol' fad' la' sol'2 |
r la''8\doux sol'' fad'' mib'' |
re''2 la''4. la''8 |
re''2 mib''4.(\doux re''8) |
re''4. r8 \appoggiatura { do''32 re'' } mib''4.\doux( re''8) |
re''2 r4 sol'' |
sol''4.( fa''8) fa''2 |
si'4 si'8 do'' re''4 si' |
do''4 r16 sol''\fort sol'' la'' sib'' do''' sib'' la'' sol'' fa'' mi'' re'' |
do''2 r |
do''4.\doux do''8 fa''4 fa''8 sol'' |
la'2. r4 r re'' |
sol''2 r4 mib'' |
do''2 r4 fa'' |
sib''4. re''8 do''4. sib'8 la'4.\trill sib'8 |
sib'1 |
