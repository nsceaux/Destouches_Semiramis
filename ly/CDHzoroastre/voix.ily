\zoroastreMark r2 sib4. sib8 |
sib4. r8 la4 la |
re4. re8 sol fa fa sol |
mi2\trill r4 sol8 la sib4 sib8 sol |
do'4. fa8 fa mib mib re |
re1\trill |
R1*6 |
r4 sib sib sib |
sib2. do'4 |
\appoggiatura sib16 lab2 lab4 lab8 lab lab4. sib8 |
sol2\trill sol4 la |
sib4. re8 mib4 fa |
\appoggiatura fa16 sol4 sol8 la la4\trill la8 sib |
\appoggiatura sib16 do'2. fa4 |
fa2 fa4 fa |
fa2 fa4 mi |
\appoggiatura mi?16 fa2 r |
r4 fa8 fa sib4 sib8 re |
mib4 mib8 mib fa4 fa8 lab |
sol2 sol4 r |
r sib8 sib si4 si16 si si do' |
do'2 re' re'4 r |
r8 sol sol sol mib'4. re'8 do' sib la sol |
fad4 fad8 fad la la la la |
re2 r4 sol8 sol |
la2 r4 la8 la |
si2 si |
R1 |
r2 do' |
r4 do'8 do' do'4 do' |
fa2 fa4 fa sib sib8 reb' |
sol2 r4 sol |
lab4. lab8 lab4 lab |
sol2 r4 sol8 la sib4 sib8 do' |
la4 la do'8 do'16 sib la8 sol |
fa2 fa4 fa8 fa re4. fa8 |
\appoggiatura fa8 sib,2 sib,4 sib |
mi2 mi4 mi8 sol |
do2. r8 fa fa fa sol la |
sib2 sol4. sol8 |
do'2 do'4 do' |
do'2 do'4 r8 do' |
la4.\trill la8 la4 do' |
fa4 fa8 fa sib4 do'8 re' |
sol4\trill sol sib4. sol8 |
\appoggiatura fa8 mib2 mib8 mib mib re |
re2\trill r |
r r4 sib8. sib16 |
la4\trill sib8 do' fad2 |
r4 la8 sib do'4 do'8 re' |
si2 r |
r4 sol sol2 |
r4 sol sol8 sol sol sol |
do4 do r8 lab lab lab |
re4 re8 mi fa4 fa8 sol |
mi2 mi4 r |
r2 r8 sol la sib |
la4.\trill la8 sib do' re' mib' |
do'2\trill r4 fa sib2 |
r4 sol do'2 |
r4 la re'2 |
r4 r8 sib do'4. re'8 do'4.\trill sib8 |
sib1 |
