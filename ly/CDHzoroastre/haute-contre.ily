\clef "haute-contre" R1*3 R1. R1*2 |
r4 mib' mib' mib' |
mib'2 sol' |
la' sib'~ |
sib'4.*13/12 sib'16*1/2 la' sol' la'2~ |
la' fa' |
do' do'4.\trill sib8 |
sib4 re' re' re' |
mib' mib' mib' mib' |
fa'2 fa'2. fa'4 |
sol'2 sol'4 mib' |
fa'2 mib'4 lab' |
sol'2 fa'4. fa'8 |
fa'4 fa' fa' fa' |
fa'2 fa'4 fa' |
sol'4 sol' sol' sol' |
do'2. do'4\fortSug |
do'2 sib'4\douxSug sib'8 fa' |
sol'2 lab'4 lab'8 fa' |
sib'2. sol'4\fortSug |
sol2 lab\douxSug |
sol << { fa'2 mib'4 sol' } \\ re'1 >> |
re'4 r mib'\douxSug r sol' mib' |
re'2 re' |
re' r |
la' r |
sol'4 r r2 |
sol'2 r |
fa'1 |
sol'2\douxSug mib' |
fa'1 fa'2 |
do'2 r |
re'1 |
do'1 sol'2 |
fa' r |
la' do' re' |
re'2. re'4 |
do'1 |
do' fa'2\douxSug |
fa' mi' |
sol' fa' |
do'1 |
do'2. do'4 |
re'2. fa'4 |
mib'1 |
mib'2 fa' |
fa' mib'4\douxSug sol' |
re'2 sib |
r la'\douxSug |
la'2. la'4 |
sol'2 do''4.(\douxSug sol'8) |
sol'2 r |
sol' r4 re'' |
do''1 |
sol' |
sol'2 r |
sol' r |
fa'2\douxSug fa'4 fa' |
fa'2 r r4 sib' |
sib'2 r4 do'' |
la'2 r4 re'' |
sib'2 fa' fa'4. fa'8 |
fa'1 |
