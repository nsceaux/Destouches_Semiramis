\clef "taille" R1*3 R1. R1*2 |
r4 sib sib do' |
sib2 sib4. mib'8 |
do'2 fa |
sol mib' |
fa' re'4. fa8 |
sol2 la4. sib8 |
sib4 fa fa fa |
mib mib' do' do' |
sib2 sib2. sib4 |
sib2 mib' |
sib sib4. sib8 |
sib2 la4 re' |
do'2. do'4 |
do'2 lab4 lab |
sib4 re' re' sib |
do'2. la4\fortSug |
la2 sib4\douxSug sib8 sib |
sib2 do'4. lab8 |
mib'2. mib'4\fortSug |
mib2 fa\douxSug |
sol sol1 |
re'4 r do'\douxSug r sol la |
la2 la |
sib r |
re' r |
re' r |
mib' r |
do'1 |
do'\douxSug |
re'2 fa' sib |
do' r |
fa r |
sol1 do'2 |
do' r |
do'1 sib2 |
sib2. sib4 |
sol1 |
la1 do'2\douxSug |
sib1 sib2 lab |
do'1 |
do'2. fa4 |
fa1 |
sol |
sol2 do' |
sib sol\douxSug |
fad sol |
r re'\douxSug |
re'1 |
sol2 lab4.(\douxSug sol8) |
sol2 r |
re' r4 re' |
mib'2 do' |
re'1 |
do'2 r |
do' r |
do'\douxSug sib4 sib |
do'2 r2 r4 fa' |
mib'2 r4 sol' |
fa'2 r4 la' |
sol'2 fa' fa'4. mib'8 |
re'1 |
