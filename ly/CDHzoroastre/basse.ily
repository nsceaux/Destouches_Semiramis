\clef "basse"
<<
  \tag #'basse { R1*3 R1. R1*2 }
  \tag #'basse-continue {
    sib,1 |
    do2. la,4 |
    sib,2 si, |
    do1~ do4. sib,8 |
    la,1 |
    sib, |
  }
>>
mib,4 mib mib mib |
mib2~ mib4.*13/12 do32 re mib |
fa4. r8 r4 r8*5/4 fa32 mib re |
mib4. r8 r4 r8*5/4 mib32 re do |
re2. r4 |
do2 fa4 fa, |
sib,4 sib,^"Tous" sib, sib, |
do do do do |
re re re sib, do re |
mib mib mib mib |
re8 do sib, lab, sol,4 fa, |
mib, mib fa sib, |
fa, fa, fa, fa, |
lab, lab, lab, lab, |
sol, sol, sol, sol, |
fa,2 fa |
mib re8 do sib, lab, |
sol,2 fa, |
mib,1 |
mib2 re |
do si,1~ |
si,2 do1 |
re2 do |
sib,2 r |
fad, r |
sol,1 |
do |
fa |
mi2 mib |
re reb1 |
do2 r |
si,1 |
do mi,2 |
fa,1 |
la, sib,4 la, |
sol,2. sol,4 |
sib,1 |
la,1. |
sol,1 |
mi,2 fa, mi, do, |
fa, fa4 mib |
re1 |
mib |
do2 fa, |
sib, do^\doux |
re mib4 sib, |
do2 re |
fad,1 |
sol,2 do^\doux |
si, do |
si, sib, |
la, lab, |
sol,1 |
do2 r |
mi r |
fa4 mib re sib, |
fa1 r4 sib, |
mib2 r4 do |
fa2 r4 re |
sol4. sol8 la4. sib8 fa4 fa, |
sib,1 |
