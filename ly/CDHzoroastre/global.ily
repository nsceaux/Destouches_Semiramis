\key sib \major
\time 2/2 \midiTempo#160 s1*3
\time 3/2 s1.
\time 2/2 s1*2
\beginMark "Prélude" \time 2/2 \midiTempo#120 s1*8
\time 3/2 \grace s16 s1.
\time 2/2 s1*6 s2 \tempo "Vîte" s2 s1*4
\time 3/2 s1.*2
\time 2/2 s1*7
\time 3/2 s1.
\time 2/2 s1*2
\time 3/2 s1.
\time 2/2 s1
\time 3/2 s1.
\time 2/2 \grace s8 s1*2
\time 3/2 s1.
\time 2/2 s1*19
\time 3/2 s1.
\time 2/2 s1*2
\time 3/2 s1.
\digitTime\time 2/2 s1 \bar "|."
