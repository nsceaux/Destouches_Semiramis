\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff \with { \haraKiriFirst } <<
        \global \includeNotes "dessus"
      >>
      \new Staff \with { \haraKiriFirst } <<
        \global \includeNotes "haute-contre"
      >>
      \new Staff  \with { \haraKiriFirst } <<
        \global \includeNotes "taille"
      >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3\break s1. s1*2\break s1*6\pageBreak
        s1*2 s1. s1\break s1*4\pageBreak
        s1*3 s2 \bar "" \break s2 s1*2 s2 \bar "" \pageBreak
        s1 s1. s1\break s1*4\pageBreak
        s1*2 s1. s1\break s1 s1. s1\pageBreak
        s1. s1*2\break s1. s1*3\pageBreak
        s1*4\break s1*5\pageBreak
        s1*4\break s1*3\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
