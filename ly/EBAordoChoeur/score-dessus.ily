\score {
  \new StaffGroup <<
    \new Staff <<
      <>^"Trompette"
      \global \keepWithTag #'trompette \includeNotes "dessus"
    >>
    \new Staff << \global \keepWithTag #'dessus \includeNotes "dessus" >>
  >>
  \layout { }
}
