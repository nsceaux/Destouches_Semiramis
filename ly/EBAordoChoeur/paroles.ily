\tag #'ordonnateur {
  Au plus grand de nos Rois a -- dres -- sons nôtre hom -- ma -- ge
  Rem -- plis -- sons de son nom & la Terre & les Airs.
  Dieux im -- mor -- tels, vous dont il fut l’i -- ma -- ge,
  E -- cou -- tez, E -- cou -- tez nos con -- certs.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Au plus grand de nos Rois a -- dres -- sons nôtre hom -- ma -- ge
  Rem -- plis -- sons de son nom & la Terre \tag #'vtaille { & la Terre } & les Airs.
  Dieux im -- mor -- tels, vous dont il fut l’i -- ma -- ge,
  \tag #'(vbasse) { E -- cou -- tez, }
  E -- cou -- tez nos con -- certs.

  Au plus grand de nos Rois a -- dres -- sons nôtre hom -- ma -- ge
  Rem -- plis -- sons de son nom & la Terre \tag #'vtaille { & la Terre } & les Airs.
  Dieux im -- mor -- tels, vous dont il fut l’i -- ma -- ge,
  \tag #'(vbasse) { E -- cou -- tez, }
  E -- cou -- tez nos con -- certs.

  Au plus grand de nos Rois a -- dres -- sons nôtre hom -- ma -- ge
  Rem -- plis -- sons de son nom & la Terre \tag #'vtaille { & la Terre } & les Airs.
  Dieux im -- mor -- tels, vous dont il fut l’i -- ma -- ge,
  \tag #'(vdessus vhaute-contre vbasse) { E -- cou -- tez, }
  E -- cou -- tez nos con -- certs.
  \tag #'(vdessus vhaute-contre vtaille) {
    Dieux im -- mor -- tels, vous dont il fut l’i -- ma -- ge,
  }
  \tag #'vtaille { E -- cou -- tez, }
  Dieux im -- mor -- tels, vous dont il fut l’i -- ma -- ge,
  \tag #'vbasse { E -- cou -- tez, }
  E -- cou -- tez nos con -- certs.
  Dieux im -- mor -- tels, vous dont il fut l’i -- ma -- ge,
  \tag #'vbasse { E -- cou -- tez, }
  E -- cou -- tez nos con -- certs.
}
