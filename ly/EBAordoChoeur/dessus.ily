\clef "dessus"
\tag #'(dessus1 dessus2 dessus) <>^\markup\whiteout Tous
r8*5/4 re''32 mi'' fa'' |
sol''4. re''8 sol''4 fa'' |
mi''4.\trill fa''8 mi''4 re''\trill |
do'' sol' do''8 re'' mi'' do'' |
re''4 re'' sol'4.*13/12 re''32 mi'' fa'' |
sol''4. re''8 sol''4 fa'' |
mi''4.\trill fa''8 mi''4 re''\trill |
do'' fa'' mi'' re''\trill |
do''1 |
<<
  \tag #'trompette { R1*5 | }
  \twoVoices #'(dessus1 dessus2 dessus) <<
    { mi''2 re''4\trill do'' |
      fa''2 mi''4\trill re'' |
      sol''2 fa''4\trill mi'' |
      la'' sol'' fa'' mi'' |
      re'' do''8( re'') re''2\trill | }
    { do''2 si'4\trill la' |
      re''2 do''4\trill si' |
      mi''2 re''4\trill do'' |
      fa'' mi'' re'' do'' |
      si' la'8( si') si'2\trill | }
    { <>\doux }
  >>
>>
re''4.\fort mi''8 re''4 sol' |
re'' sol' re'' re'' |
re''2\trill r |
<<
  \tag #'trompette { R1*4 | r4 r8*5/4 }
  \twoVoices #'(dessus1 dessus2 dessus) <<
    { mi''2 re''4\trill do'' |
      si' re'' do''\trill si' |
      do'' fa'' mi''\trill re'' |
      mi'' la'8 si' si'2\trill |
      la'4.*13/12 }
    { do''2 si'4\trill la' |
      sold' si' la'\trill sold' |
      la' re'' do''\trill si' |
      do'' fad'8 sold' sold'2\trill |
      la'4.*13/12 }
    { <>\doux }
  >>
>> mi''32\fort fa'' sol'' la''4 la'' |
mi''4. re''8 mi''4 fa'' |
sol''2 sol'' |
<<
  \tag #'trompette { R1*2 | }
  \twoVoices #'(dessus1 dessus2 dessus) <<
    { re''4. dod''8 re''4 mi'' |
      fa''2 fa'' | }
    { si'4. la'8 si'4 dod''! |
      re''2 re'' | }
    { <>\doux s1*2 <>^"Tous" }
  >>
>>
do''4.\fort si'8 do''4 re'' |
mi'' sol'' fa'' mi'' |
re''8 do'' re'' mi'' re''4.*13/12 re''32 mi'' fa'' |
sol''4. re''8 sol''4 fa'' |
mi''4.\trill fa''8 mi''4 re''\trill |
do'' sol' do''8 re'' mi'' do'' |
re''4 re'' sol'4.*13/12 re''32 mi'' fa'' |
sol''4. re''8 sol''4 fa'' |
mi''4.\trill fa''8 mi''4 re''\trill |
do'' fa'' mi'' re''\trill |
do''2
%% Ordonnateur
<<
  \tag #'trompette { r2 | R1*14 | r2 }
  \tag #'(dessus1 dessus2 dessus) {
    r2 |
    r2 <>^\markup\whiteout "Violons" sol'4\doux sol' |
    si'2 si'4 sol' |
    do''2 sol''4 sol'' |
    mi''4. re''8 do'' re'' mi'' do'' |
    fa''4 fa'' fa'' fa'' |
    mi''4. re''8 mi'' fa'' sol'' mi'' |
    la''2 la''4 la'' |
    fad''2 sol''8.( la''16) la''8.(\trill sol''32 la'') |
    si''2 re'' |
    re'' mi'' |
    re''1 |
    sol''4 sol''8 fa'' mi''4 re'' |
    mi'' do'' sol''4. sol''8 |
    sol''4 fa''8 mi'' re'' mi'' fa'' sol'' |
    mi''4 do''
  }
>>
%% Chœur
<<
  \tag #'trompette {
    r2 |
    R1*2 |
    r2 sol''4 sol'' |
    do''' do'''8 do''' do'''4 do''' |
    la''4 la''8 la'' la''4 fa'' |
    sol''4. la''8 sol'' fa'' mi'' re'' |
    do''4.*13/12 mi''32 fa'' sol'' la''4 la'' |
    la''2 r |
    R1 |
    r2 sol'' |
    sol'' sol''4 sol''8 sol'' |
    sol''4 sol''8 sol'' sol''4 la'' |
    sol'' sol''8 sol''
  }
  \tag #'(dessus1 dessus2 dessus) {
    <>^\markup\whiteout "Violons" r2 |
    r2 sol'4 sol' |
    si'2 si'4 sol' |
    do''2 mi''4 mi'' |
    do''4. si'8 do'' re'' mi'' do'' |
    fa''4 fa'' fa'' fa'' |
    mi''4. re''8 mi'' fa'' sol'' mi'' |
    la''2 do''4 do'' |
    la'2 si'8.( do''16) do''8.(\trill si'32 do'') |
    re''2 re'' |
    re'' mi'' |
    re''1 |
    sol''4 sol''8 fa'' mi''4 re'' |
    mi'' do''
  }
>> sol''4. sol''8 |
sol''4 fa''8 mi'' re'' mi'' fa'' sol'' |
mi''4 do'' mi'' mi''8 fa'' |
sol'' fa'' sol'' la'' sol''4 sol'' |
do'''2 do''4 do''8 re'' |
mi''4 re''8 mi'' fa'' mi'' re'' do'' |
sol''2 sol''4 fa''8 mi'' |
re'' do'' re'' mi'' re'' do'' re'' mi'' |
re''4 sol' re'' re'' |
re'' sol' re'' re'' |
re''2\trill
<<
  \tag #'trompette { r2 R1*14 r2 }
  \tag #'(dessus1 dessus2 dessus) {
    r2 |
    r2 re''4 re'' |
    mi''4. re''8 mi'' fa'' sol'' mi'' |
    la''2 do''4 do'' |
    la'4. sol'8 la' si' do'' la' |
    re''4 re'' re'' re'' |
    si'2 do''8 si' do'' re'' |
    si'2 si'4 si' |
    dod''2 re''4 mi'' |
    fa''2 fad'' |
    fad'' sol'' |
    fad''1 |
    re''4 re''8 do'' sib'4 la' |
    sib' sol' sol''4. sol''8 |
    sol''2 sol''4 fad'' |
    sol''2
  }
>>
<<
  \tag #'(trompette dessus1 dessus) \new Voice {
    \tag #'dessus \voiceOne
    \tag #'(dessus1 dessus) <>^\markup\whiteout "Violons"
    re''4 re''8 mi'' |
    fa'' mi'' fa'' sol'' fa'' sol'' fa'' mi'' |
    re'' do'' re'' mi'' fa'' mi'' fa'' re'' |
    mi''4 do'' sol''2 |
    sol'' la'' |
    sol''1 |
    la''4 la''8 sol'' fa'' sol'' fa'' mi'' |
    re''4. mi''16 fa'' sol''4 sol'' |
    mi'' do'' mi''8 fa'' sol'' fa'' |
    mi'' re'' do'' re'' re''4.\trill do''8 |
    do''2
  }
  \tag #'(dessus2 dessus) \new Voice {
    \tag #'dessus \voiceTwo
    \tag #'(dessus2 dessus) <>-\markup\whiteout "Hautbois et violons"
    si'4 si'8 do'' |
    re'' do'' re'' mi'' re'' mi'' re'' do'' |
    si' la' si' do'' re'' do'' re'' si' |
    do''4 sol' mi''2 |
    mi'' fa'' |
    mi''1 |
    fa''4 fa''8 mi'' re'' mi'' re'' do'' |
    si'4. si'8 si'4.(\trill la'16 si') |
    do''4 sol' do''8 re'' mi'' re'' |
    do'' si' la' si' si'4.\trill do''8 |
    do''2
  }
>>
<<
  \tag #'trompette { r2 R1*20 \allowPageTurn r2 }
  \tag #'(dessus1 dessus2 dessus) {
    r2 |
    r2 sol'4 sol' |
    do''4. si'8 do'' re'' mi'' do'' |
    fa''2 fa''4 fa'' |
    mi''4.\trill re''8 mi'' fa'' sol'' mi'' |
    la''4 la'' do'' do'' |
    la'4. sol'8 la' si' do'' la' |
    re''2 re''4 re'' |
    si'2 do''4 re'' |
    mi''2 mi'' |
    mi'' fa'' |
    mi''1 |
    la''4 la''8 sol'' fa''4 mi'' |
    fa'' re'' re''4. re''8 |
    re''2 do''4. do''8 |
    do''2 do''4 si' |
    la'2\trill fad'' |
    fad'' sol'' |
    fad''1 |
    re''4 re''8 do'' si'4 la' |
    si' sol' re''2 |
    re''
  }
>>
<<
  \tag #'trompette {
    sol''2 |
    sol'' sol''4 sol''8 sol'' |
    sol''4 sol''8 sol'' sol''4 la'' |
    sol'' sol''8 sol''
  }
  \tag #'(dessus1 dessus2 dessus) {
    mi''2 |
    re''1 |
    sol''4 sol''8 fa'' mi''4 re'' |
    mi'' do''
  }
>> sol''4. sol''8 |
sol''4 fa''8 mi'' re'' mi'' fa'' sol'' |
mi''4 do'' r2 |
<<
  \tag #'trompette {
    R1 |
    r2 do'''2 |
    do''' do'''4 do'''8 do''' |
    do'''4 do'''8 do''' do'''4 do''' |
    sol''4 sol''8 sol'' sol''4 sol'' |
  }
  \tag #'(dessus1 dessus2 dessus) {
    r2 mi'' |
    mi'' fa'' |
    mi''1 |
    fa''4 fa''8 mi'' re''4 do'' |
    si'4 si' sol''4. sol''8 |
  }
>>
sol''4 fa''8 mi'' re'' mi'' fa'' sol'' |
mi''2.\trill
