\score {
  \new StaffGroupNoBar <<
    \new StaffGroupNoBracket <<
      \new Staff <<
        \global \keepWithTag #'trompette \includeNotes "dessus"
        { s4^"Trompette" s1*50 s2
          <>^"Trompette" s2 s1*37 s2
          <>^"Trompette" }
      >>
      \new Staff << \global \includeNotes "timbales" >>
    >>
    \new StaffGroupNoBracket <<
      \new Staff << \global \keepWithTag #'dessus \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \includeNotes "vdessus"
        { s4 s1*51 \noHaraKiri }
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "vhaute-contre"
        { s4 s1*51 \noHaraKiri }
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "vtaille"
        { s4 s1*51 \noHaraKiri }
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \includeNotes "vbasse"
        { s4 s1*51 \noHaraKiri }
      >> \keepWithTag #'(ordonnateur vbasse) \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s1*7\pageBreak
        s1*9\break s1*8\pageBreak
        s1*7\break s1*4 s2 \bar "" \pageBreak
        s2 s1*5\break s1*6\break s1*3 s2 \bar "" \pageBreak
        s2 s1*5\pageBreak
        s1*6\pageBreak s1*6\pageBreak
        s1*7\pageBreak s1*5 s2 \bar "" \pageBreak
        s2 s1*4 s2 \bar "" \pageBreak s2 s1*6\pageBreak
        s1*8\pageBreak s1*6\pageBreak
        s1*6\pageBreak s1*6\pageBreak
        s1*5\pageBreak s1*6\pageBreak
      }
      \modVersion {
        %% prélude
        s4 s1*18\pageBreak s1*17 s2 \pageBreak
        %% ordonnateur
        s2 s1*14 s2 \pageBreak
        %% chœur
        s2 s1*11\pageBreak
        s1*12\pageBreak
        s1*12\pageBreak
        s1*12\pageBreak
        s1*12\pageBreak
        s1*11\pageBreak
      }
    >>
  >>
  \layout {
    \context {
      \Score
      \override NonMusicalPaperColumn #'page-break-permission = ##f
    }
  }
  \midi { }
}
