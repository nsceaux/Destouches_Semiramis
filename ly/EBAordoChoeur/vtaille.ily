\clef "vtaille" r4 R1*50 |
r2 sol4 sol |
do'2 do'4 mi' |
re'2 re'4 re' |
do'2 do'4 do' |
do' do' do' do' |
do'2 do'4 do' |
do'2 do'4 do' |
mi'2 mi'4 mi' |
re'2 re'4 re' |
re'2 si |
si do' |
si1 |
do'4 do'8 do' do'4 fa' |
do' do' do'4. do'8 |
do'2 do'4 si |
do'2 r |
R1*7 |
r2 si4 si |
re'2 re'4 si |
do'2 do'4 do' |
mi'2 mi'4 mi' |
re' re' re' re' |
fad'2 fad'4 fad' |
mi'2 mi'4 mi' |
mi'2 mi'4 mi' |
mi'2 re'4 re' |
re'2 re' |
re' re' |
re'1 |
re'4 re'8 re' re'4 mib' |
re' sib re'4. re'8 |
mib'2 re'4 re' |
re'2 r |
R1*9 |
r2 do'4 do' |
si2 si4 mi' |
do'2 do'4 do' |
do'2 do'4 do' |
do' do' do' do' |
do'2 mi'4 mi' |
re'2 re'4 re' |
fad'2 fad'4 fad' |
mi'2 mi'4 mi' |
mi'2 dod' |
dod' re' |
dod'1 |
re'4 re'8 re' re'4 sol' |
re'4 re' fa'4. fa'8 |
fa'2 mi'4 mi' |
re'2 <>^\markup { Joignez 2 basses-tailles } re' |
re' re' |
re'1 |
re'4 re'8 do' si4 la |
si sol <>^\markup { Tailles seules } re'4. re'8 |
re'2 si |
si2 do' |
si1 |
do'4 do'8 do' do'4 fa' |
do' do' do'4. do'8 |
do'2 do'4 si |
do'2 r |
r do' |
do' do' |
do'1 |
do'4 do'8 do' do'4 fa' |
re' re' do'4. mi'8 |
mi'4( re'8)[ do'] si4.\trill do'8 |
do'2.
