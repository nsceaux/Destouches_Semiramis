\clef "haute-contre" r4 |
r2 r4 r8 sol' |
sol'4. re''8 do''4 si' |
do'' mi' sol' do'' |
si'2 si'4 si' |
do''2 r4 r8 si' |
do''4. re''8 do''4 si' |
do'' do'' do'' si' |
do''1 |
R1*5 |
si'4.\fortSug do''8 si'4 sol' |
sol' sol' si' si' |
si'2\trill r |
R1*4 |
r2 r4 r8 si'\fortSug |
do''4. si'8 do''4 la' |
si'2 si' |
R1*2 |
la'4.\fortSug sol'8 la'4 si' |
do''4 do'' si' do'' |
si'8 la' si' do'' si'2 |
r2 r4 r8 sol' |
sol'4. re''8 do''4 si' |
do'' mi' sol' do'' |
si'2 si'4 si' |
do''2 r4 r8 si' |
do''4. re''8 do''4 si' |
do''4 do'' do'' si' |
do''2 r |
r do'4 mi' |
re'2 re'4 re' |
mi'2 do''4 do'' |
do''2 mi'4 mi' |
la' la' la' la' |
sol'2 do''4 do'' |
do''2 do''4 do'' |
la'2 si'8.( do''16) do''8.(\trill si'32 do'') |
re''2 si' |
si' do'' |
si'1 |
do''2 do''4 si' |
do''2 mi''4. mi''8 |
mi''4 re''8 do'' si' do'' re'' si' |
do''4 sol' r2 |
r sol'4 sol' |
sol'2 sol'4 sol' |
sol'2 sol'4 sol' |
mi'2 mi'8 fa' sol' mi' |
la'4 la' la' la' |
sol'2 sol'4 sol' |
do''2 la'4 la' |
fad'2 sol'4 la' |
si'2 sol' |
sol' sol' |
sol'1 |
sol'4 sol'8 sol' sol'4 la' |
sol' mi' mi'4. mi'8 |
mi'2 re'4 sol' |
sol'2 r |
r mi''4 mi'' |
do''2 do''4 si' |
do''4 si'8 do'' re'' sol' la' do'' |
si'2 si'4 si' |
si'8 la' si' do'' si' la' si' do'' |
si'4 re' re' sol' |
re' re' re' sol' |
si'2\trill si'4 si' |
re''2 sol'4 sol' |
sol'2 sol'4 sol' |
do''2 la'4 la' |
fad'2 fad'4 fad' |
si' si' si' si' |
sold'2 la'4 la' |
la'2 sol'4 sol' |
sol'2 fa'4 sol' |
la'2 la' |
la' sib' |
la'1 |
sol'4 sol'8 sol' sol'4 fad' |
sol' sol' sol'4. sib'8 |
la'2 la'4 la' |
si'!2 sol'4 sol' |
sol' si' si' si' |
sol' sol' sol' sol' |
sol'2 do'' |
do'' do'' |
do''1 |
do''4 do''8 do'' do''4 do'' |
sol'4. sol'8 sol'4 sol' |
sol'2 sol' |
sol'4 la' sol'4. fa'8 |
mi'2 r |
r sol'4 sol' |
mi'2 mi'4 mi' |
la'2 la'4 la' |
sol'2 sol'4 sol' |
do'' do'' la' la' |
fad'2 fad'4 fad' |
si'2 si'4 si' |
sold'2 la'4 si' |
do''2 la' |
la' la' |
la'1 |
la'4 la'8 la' la'4 sib' |
la' fa' la'4. la'8 |
sib'2 la'4. la'8 |
fad'2 fad'4 sol' |
fad'2\trill la' |
la' sib' |
la'1 |
sol'4 sol'8 sol' sol'4 fad' |
sol' sol' sol'2 |
sol'2 sol' |
sol'1 |
sol'4 sol'8 sol' sol'4 la' |
sol' mi' mi'4. mi'8 |
mi'2 re'4 sol' |
sol'2 r |
r sol' |
sol' la' |
sol'1 |
fa'4 fa'8 sol' la'4 la' |
sol'4 sol' sol' sol' |
la'2 sol'4 sol' |
sol'2.
