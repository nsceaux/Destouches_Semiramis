\clef "vdessus" r4 R1*50 |
r2 r |
r sol'4 sol' |
si'2 si'4 sol' |
do''2 mi''4 mi'' |
do''2 do''8[ re''] mi''[ do''] |
fa''4 fa'' fa'' fa'' |
mi''2 mi''4 mi'' |
la''2 do''4 do'' |
la'2 si'4 do'' |
re''2 re'' |
re'' mi'' |
re''1 |
sol''4 sol''8 fa'' mi''4 re'' |
mi'' do'' sol''4. sol''8 |
sol''2 fa''4 re'' |
mi''2\trill r |
R1*8 |
r2 re''4 re'' |
mi''2 mi''4 mi'' |
la''2 do''4 do'' |
la'2 la'4 la' |
re'' re'' re'' re'' |
si'2 do''8[ si'] do''[ re''] |
si'2\trill si'4 si' |
dod''2 re''4 mi'' |
fa''2 fad'' |
fad'' sol'' |
fad''1 |
re''4 re''8 do'' sib'4 la' |
sib' sol' sol''4. sol''8 |
sol''2 sol''4 fad'' |
sol''2 r |
R1*10 |
r2 sol'4 sol' |
do''2 do''4 do'' |
fa''2 fa''4 fa'' |
mi''2\trill mi''4 mi'' |
la'' la'' do'' do'' |
la'2 la'4 la' |
re''2 re''4 re'' |
si'2 do''4 re'' |
mi''2 mi'' |
mi'' fa'' |
mi''1 |
la''4 la''8 sol'' fa''4 mi'' |
fa'' re'' re''4. re''8 |
re''2 do''4. do''8 |
do''2 do''4 si' |
la'2\trill fad'' |
fad'' sol'' |
fad''1 |
re''4 re''8 do'' si'4 la' |
si' sol' re''2 |
re'' mi'' |
re''1 |
sol''4 sol''8 fa'' mi''4 re'' |
mi'' do'' sol''4. sol''8 |
sol''2 fa''4 re'' |
mi''2\trill r |
r mi'' |
mi'' fa'' |
mi''1 |
fa''4 fa''8 mi'' re''4 do'' |
si'\trill si' sol''4. sol''8 |
sol''2 fa''4 re'' |
mi''2.\trill
