\clef "basse" <>^\markup\whiteout "Tymballes" r4 |
r2 r4 r8*5/4 sol,32 sol, sol, |
do4. sol,8 do4 sol, |
do4. do8 do4 do |
sol,2 r |
r r4 r8*5/4 sol,32 sol, sol, |
do4. sol,8 do4 sol, |
do do sol,4. do8 |
do1 |
R1*5 |
sol,4.\fort sol,8 sol,4 sol, |
sol, sol, sol, sol, |
sol,2 r |
R1*12 |
r2 r4 r8*5/4 sol,32 sol, sol, |
do4. sol,8 do4 sol, |
do4. do8 do4 do |
sol,2 r |
r2 r4 r8*5/4 sol,32 sol, sol, |
do4. sol,8 do4 sol, |
do do sol,4. do8 |
do2 r |
R1*14 |
r2 r |
R1*9 |
r2 sol, |
sol,2 r |
r do4 do8 do |
do4 do8 do do4. do8 |
do4 do sol,4. do8 |
do2 r |
R1 |
do4 do8 do do4 sol, |
do2 sol,4 do |
sol,2 sol, |
sol, sol,4 sol,8 sol, |
sol,4 sol,8 sol, sol,4 sol, |
sol, sol, sol, sol, |
sol,2 r |
R1*14 |
r2 sol, |
sol, sol, |
sol,1 |
do4 do8 do do2 |
do do |
do4 do8 do do4 do |
do do8 do do4 do |
sol, sol,8 sol, sol,4 sol, |
do do8 do do4 do |
do4 do sol,4. do8 |
do2 r |
R1*20 |
r2 sol, |
sol, r |
r do4 do8 do |
do4 do8 do do4. do8 |
do4 do8 do sol,4. do8 |
do2 r |
R1 |
r2 do |
do do4 do8 do |
do4 do do2 |
r do4 do8 do |
do4 do sol,4. do8 |
do2.
