\clef "vbasse" r4 R1*35 |
r2 <>^\markup\character L'Ordonnateur do4 do | \noBreak
mi2 mi4 do |
sol2 sol4 sol |
mi2 mi8[ fa] sol[ mi] |
la4 la la la |
fa2 fa4 fa |
do'2 do'4 do' |
la2 la4 la |
re'2 sol |
sol sol |
sol1 |
sol4 sol8 fa mi4 re |
mi do do'4. do'8 |
do'2 mi4. mi8 |
fa2 sol4 sol |
do2 %{%} do4 do |
mi2 mi4 do |
sol2 sol4 sol |
mi2 mi8[ fa] sol[ mi] |
la4 la la la |
fa2 fa4 fa |
do'2 do'4 do' |
la2 la4 la |
re'2 sol |
sol sol |
sol1 |
sol4 sol8 fa mi4 re |
mi4 do do'4. do'8 |
do'2 mi4. mi8 |
fa2 sol4 sol |
do2 r |
R1*7 |
r2 sol4 sol |
si2 si4 sol |
do'2 do'4 do' |
la2 la4 la |
re' re' re' re' |
si2 si4 si |
mi'2 la4 la |
mi2 mi4 mi |
la2 re' |
re' re' |
re'1 |
re'4 re'8 do' sib4 la |
sib sol sol4. sol8 |
sol2 sib4. sib8 |
do'2 re'4 re |
sol2 r |
R1*9 |
r2 do4 do |
sol2 sol4 mi |
la2 la4 la |
fa2 fa4 fa |
do' do' do' do' |
la2 la4 la |
re'2 re'4 re' |
si2 si4 si |
mi'2 la |
la la |
la1 |
la4 la8 sol fa4 mi |
fa re re'4. re'8 |
re'2 fa4. fa8 |
sol2 la4 la |
re2 r |
R1*3 |
r2 sol |
sol sol |
sol1 |
sol4 sol8 fa mi4 re |
mi do do'4. do'8 |
do'2 mi4. mi8 |
fa2 sol4 sol |
do2 do' |
do' do' |
do'1 |
do'4 do'8 sib la4 sol |
la fa fa4. fa8 |
sol2 mi4. mi8 |
fa2 sol4 sol |
do2.
