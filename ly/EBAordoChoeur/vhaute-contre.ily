\clef "vhaute-contre" r4 R1*50 |
r2 r |
r sol'4 sol' |
sol'2 sol'4 sol' |
sol'2 sol'4 sol' |
mi'2 mi'8[ fa'] sol'[ mi'] |
la'4 la' la' la' |
sol'2 sol'4 sol' |
do''2 la'4 la' |
fad'2 sol'4 la' |
si'2 sol' |
sol' sol' |
sol'1 |
sol'4 sol'8 sol' sol'4 la' |
sol' mi' mi'4. mi'8 |
mi'2 re'4 sol' |
sol'2 r |
R1*8 |
r2 sol'4 sol' |
sol'2 sol'4 sol' |
do''2 la'4 la' |
fad'2 fad'4 fad' |
si' si' si' si' |
sold'2 la'4 la' |
la'2 sol'4 sol' |
sol'2 fa'4 sol' |
la'2 la' |
la' sib' |
la'1 |
sol'4 sol'8 sol' sol'4 fad' |
sol' sol' sol'4. sib'8 |
la'2 la'4 la' |
si'!2 r |
R1*10 |
r2 sol'4 sol' |
mi'2 mi'4 mi' |
la'2 la'4 la' |
sol'2 sol'4 sol' |
do'' do'' la' la' |
fad'2 fad'4 fad' |
si'2 si'4 si' |
sold'2 la'4 si' |
do''2 la' |
la' la' |
la'1 |
la'4 la'8 la' la'4 sib' |
la' fa' la'4. la'8 |
sib'2 la'4. la'8 |
fad'2 fad'4 sol' |
fad'2\trill la' |
la' sib' |
la'1 |
sol'4 sol'8 sol' sol'4 fad' |
sol' sol' sol'2 |
sol'2 sol' |
sol'1 |
sol'4 sol'8 sol' sol'4 la' |
sol' mi' mi'4. mi'8 |
mi'2 re'4 sol' |
sol'2 r |
r sol' |
sol' la' |
sol'1 |
fa'4 fa'8 sol' la'4 la' |
sol' sol' sol'4. sol'8 |
la'2 sol'4 sol' |
sol'2.
