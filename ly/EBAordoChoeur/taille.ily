\clef "taille" r4 |
r2 r4 r8 re' |
do'4. si8 do'4 sol' |
mi'4. mi'8 mi' fa' sol'4 |
sol'2 sol' |
sol' r4 r8 sol' |
sol'4. sol'8 sol'4 fa' |
mi' la' sol'4. fa'8 |
mi'1 |
R1*5 |
sol'4.\fortSug sol'8 sol'4 sol' |
si si sol' sol' |
sol'2 r |
R1*4 |
r2 r4 r8 mi'\fortSug |
mi'4 la' la'2 |
sol' sol' |
R1*2 |
fa'2\fortSug fa' |
sol' sol' |
sol' sol' |
r r4 r8 re' |
do'4. si8 do'4 sol' |
mi'4. mi'8 mi' fa' sol'4 |
sol'2 sol' |
sol' r4 r8 sol' |
sol'4. sol'8 sol'4 fa' |
mi' la' sol'4. fa'8 |
mi'2 r |
r sol4 do' |
re'2 sol4 sol |
sol2 mi'4 mi' |
do'2 do'4 do' |
do'2 do'4 do' |
do'2 sol'4 mi' |
mi'2 mi'4 mi' |
re'2 re' |
re' sol' |
sol' sol' |
sol'1 |
sol'2 sol'4 fa' |
sol'2 do'4. do'8 |
do'2 sol'4 sol' |
sol' mi' sol sol |
do'2 do'4 mi' |
re'2 re'4 re' |
do'2 do'4 do' |
do' do' do' do' |
do'2 do'4 do' |
do'2 do'4 do' |
mi'2 mi'4 mi' |
re'2 re'4 re' |
re'2 si |
si do' |
si1 |
do'4 do'8 do' do'4 fa' |
do' do' do'4. do'8 |
do'2 do'4 si |
do'2 r |
r sol'4 sol' |
sol'8 fa' sol' la' sol'4 sol' |
sol'2 sol'4 fad' |
sol'2 sol' |
sol' sol' |
sol'4 si si si |
si si si re' |
sol'2 sol'4 sol' |
sol'2 re'4 si |
do'2 do'4 do' |
mi'2 mi'4 mi' |
re' re' re' re' |
fad'2 fad'4 fad' |
mi'2 mi'4 mi' |
mi'2 mi'4 mi' |
mi'2 re'4 re' |
re'2 re' |
re' re' |
re'1 |
re'4 re'8 re' re'4 mib' |
re' sib re'4. re'8 |
mib'2 re'4 re' |
re'2 re'4 re' |
re' sol' sol' sol' |
sol' re' re' re' |
do'2 sol |
sol do' |
sol'4 sol'8 sol' sol'4 sol' |
fa'2 fa'4 fa' |
fa'2 fa'4 re' |
do'2 do'4 do' |
do' do' sol4. sol8 |
sol2 do'4 do' |
si2 si4 mi' |
do'2 do'4 do' |
do'2 do'4 do' |
do' do' do' do' |
do'2 mi'4 mi' |
re'2 re'4 re' |
fad'2 fad'4 fad' |
mi'2 mi'4 mi' |
mi'2 dod' |
dod' re' |
dod'1 |
re'4 re'8 re' re'4 sol' |
re'4 re' fa'4. fa'8 |
fa'2 mi'4 mi' |
re'2 re' |
re' re' |
re'1 |
re'4 re'8 do' si4 la |
si sol re'4. re'8 |
re'2 si |
si2 do' |
si1 |
do'4 do'8 do' do'4 fa' |
do' do' do'4. do'8 |
do'2 do'4 si |
do'2 r |
r do' |
do' do' |
do'1 |
do'4 do'8 do' do'4 fa' |
re'4 re' do'4. mi'8 |
mi'4 re'8 do' si4.\trill do'8 |
do'2.
