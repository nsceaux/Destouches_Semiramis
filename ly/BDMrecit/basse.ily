\clef "basse" sol,4 re8 do sib, la, |
sib,4. la,8 sib, do |
re4. do8 sib, la, |
sol,2 sol4 fad fa2 |
mi4 mib2 |
do2. |
re |
dod2 re4 |
sol,4. la,8 sib, sol, |
la,2 fa,4 |
sol,4. fa,8 mi, re, |
sib,2. |
sol,4 la,2 |
re, re4 |
do2. |
sib,2 la,4 |
sol,8 fa, mi,2 |
fa,4 fa8 mib re4 |
mib si,2 |
do2. |
re2 sol,4 |
re,2. |
sol,2 sol4 |
fa2. |
mib2 si, |
do2. sib,8 la, |
sol,4 la, sib, mib, |
fa,2 fa4. mib8 |
re4. do8 sib,2 |
la,1 |
sib,2 re |
mib4 re do sib, fa fa, |
sib,1 |
