\tag #'(zoroastre basse) {
  De vos nou -- veaux Su -- jets voy -- ez quelle est l’ar -- deur,
  Ré -- pon -- dez à mes feux, ré -- pon -- dez à leur ze -- le ;
  Je veux de -- voir l’ins -- tant de mon bon -- heur
  Bien moins à vos ser -- mens, qu’à mon a -- mour fi -- de -- le.
  Je veux de -- voir l’ins -- tant de mon bon -- heur
  Bien moins à vos ser -- mens, qu’à mon a -- mour fi -- de -- le.
}
\tag #'(semiramis basse) {
  Sei -- gneur, il n’est pas tems d’ac -- com -- plir vos pro -- jets.
  A -- mes -- tris est en -- cor trop chere à mes su -- jets,
  Je veux con -- tr’elle as -- su -- rer ma puis -- san -- ce :
  Je ne puis vous of -- frir que ma re -- co -- nois -- san -- ce.
}
