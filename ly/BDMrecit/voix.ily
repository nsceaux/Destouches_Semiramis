<<
  %% Semiramis
  \tag #'(semiramis basse) {
    <<
      \tag #'basse { s2.*23 s2 \semiramisMark }
      \tag #'semiramis {
        \clef "vbas-dessus" R2.*23 |
        r4 r <>^\markup\character Semiramis
      }
    >> sib'4 |
    si'4. si'8 si' do'' |
    do''4 do''8 do'' re''4 re''8 re'' |
    sol'4 mib''8 mib'' sol'4 sol'8 la' |
    sib'4 do'' re''8 re'' re'' mib'' |
    do''2\trill r8 fa' sol' la' |
    sib'4 sib'8 do'' re''4\trill re''8 sib' |
    fa''2 fa'' |
    r4 sib'8 sib' fa'4 sol'8 lab' |
    sol'4\trill sol' la' sib' sib' la' |
    sib'2 sib'4 r |
  }
  %% Zoroastre
  \tag #'(zoroastre basse) {
    \zoroastreMarkText "à Semiramis" sol4 sol8 fad sol la |
    re2 r8 la |
    la4.\trill\prall fad8 sol la |
    \appoggiatura la8 sib2 sib8 do' |
    re'2 la8 sib |
    sol2\trill do'8 sib |
    la4.\trill\prall sib8 sol4 |
    fad4 fad8 la la la |
    mi2 fa4 |
    sol4. fa8 mi fa |
    dod2 la4 |
    si4. si8 dod' re' |
    sol4. sol8 la sib |
    mi fa mi2\trill |
    re r8 fa |
    fad2(\trill mi8) fad |
    \appoggiatura fad? sol2 la4 |
    sib4. sol8 do' re' |
    la2\trill re'4 |
    sol4. fa8 sol re |
    \appoggiatura re mib4. la8 sib do' |
    \appoggiatura sol fad2 r8 sib |
    sib4( la2)\trill |
    sol \tag #'zoroastre {
      r4 | R2. R1*7 R1. R1
    }
  }
>>