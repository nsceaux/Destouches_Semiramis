\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> \keepWithTag #'basse \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*4\break s2.*5\break s2.*4\pageBreak
        s2.*5\break s2.*5\break s2.*2 s1\break s1*3\break s1*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
