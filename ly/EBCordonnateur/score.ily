\score {
  \new ChoirStaff <<
    \new Staff <<
      <>^"Trompettes"
      \global \keepWithTag #'trompette \includeNotes "dessus"
    >>
    \new GrandStaff <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*5\break s1*5\pageBreak
        s1*7\break s1*6\pageBreak
        s1*6\break s1*5\pageBreak
        s1*4\break s1*5\break s1*5\pageBreak
        s1*5\break s1*5\pageBreak
        s1*6\break s1*5\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
