\clef "basse" <>^"Basses de violons"
R1 |
do4 do8 do do4. mi8 |
do2 si,4 la, |
sol,2 re4 r |
fa r la r |
do'4 do'8 la si4 do' |
re'2 sol4. sol8 |
sol2 re |
sol, r |
<>^"Tous" sol,2 sol, |
sol,2 r |
<>^"Basses de violons" sol, sol, |
do r |
<>^"Tous" do2 do |
do r |
<>^"Basses de violons" do r |
do4 r sol, sol8 fa |
mi4 do8 fa mi4 re |
do do8 do do4 do |
fa,2 sol, |
do r |
r <>^"Tous" sol8\fortSug la sol la |
si la si sol do'2 |
do'4 sol la sol8 fa |
mi4 fa sol sol, |
do2 r |
<>^"Basses de violons" do'4\doux sol la sol8 fa |
mi4 fa sol sol, |
do2 do' |
si la |
mi do |
re mi4 mi, |
la,2 la |
la4 sold la la, |
mi2 re |
do4 si, la, si, |
do2 re |
mi8 fad sold mi la4 la8 si |
do'4 do'8 re' mi'4 mi |
la2 r4 r8 fa |
fa2 do |
fa sol4 sol8 sol |
la4 la la la |
re'4. do'8 re' mi' re' do' |
si2 si4. si8 |
do' si do' re' do' re' do' si |
la2 re |
sol4 fa mi re |
do2 r |
do4 do8 do do4. mi8 |
do2 si,4 la, |
sol,2 re4 r |
fa r la r |
do' do'8 la si4 do' |
re'2 sol4. sol8 |
sol2 re |
sol, r |
<>^"Tous" sol,2 sol, |
sol, r |
<>^"Basses de violons" sol, sol, |
do r |
<>^"Tous" do do |
do r |
<>^"Basses de violons" do r |
do4 r sol, sol8 fa |
mi4 do8 fa mi4 re |
do do8 do do4 do |
fa,2 sol, |
do r |
R1*2 |
do4 do8 fa mi4 re |
do4 do8 do do4 do |
fa,2 sol, |
do
