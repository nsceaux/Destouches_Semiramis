\clef "dessus" \tag #'(dessus1 dessus2) <>^\markup\whiteout "Violons"
R1 |
<<
  \tag #'trompette {
    sol''4\doux sol''8 sol'' sol''4 sol'' |
    do'''2 r2 |
    R1*4 |
    re''4\doux re''8 re'' re''4 re'' |
    re''2\trill r |
    R1*3 |
    r2
  }
  \tag #'(dessus1 dessus2) {
    mi''4\doux mi''8 mi'' mi''4 mi'' |
    sol''2 r |
    <<
      \tag #'dessus1 {
        re''8\doux mi'' re'' mi'' fa''2 |
        R1 |
        sol''8\doux la'' sol'' la'' sol'' si'' la'' sol'' |
        fad'' mi'' re'' do'' si'4. si'8 |
        do'' re'' do'' si' la' si' do'' re'' |
        si'2\trill re''8\fort do'' re'' mi'' |
        re'' do'' re'' mi'' re'' mi'' re'' do'' |
        si'4 sol' r2 |
        re''2\doux re''4 si' |
        sol' mi'
      }
      \tag #'dessus2 {
        si'8\doux do'' si' dod'' re''2 |
        la'8\doux si' la' si' do'' re'' do'' re'' |
        mi'' fa'' mi'' fa'' re''4 do''8 si' |
        la'4 fad' sol'4. sol'8 |
        la' si' la' sol' fad' sol' la' fad' |
        sol'2 r |
        si'8\fort la' si' do'' si' do'' si' la' |
        sol'4 re' r2 |
        si'8\doux la' si' do'' si' do'' re'' si' |
        do''4 sol'
      }
    >>
  }
>>
<<
  \tag #'(trompette dessus1) {
    \tag #'dessus1 <>^"Violons"
    sol''8\fortSug fa'' sol'' la'' |
    sol'' fa'' sol'' la'' sol'' la'' sol'' fa'' |
    mi''4 do'' r2 |
    sol''8\doux fa'' sol'' la'' sol''4 mi'' |
  }
  \tag #'dessus2 {
    <>^\markup\whiteout "Hautbois & violons"
    r2 |
    mi''8\fort re'' mi'' fa'' mi'' fa'' mi'' re'' |
    do''4 sol' r2 |
    mi''8\doux re'' mi'' fa'' mi''4 do'' |
  }
>>
<<
  \tag #'trompette {
    R1 |
    sol''8\doux fa'' sol'' la'' sol'' la'' sol'' fa'' |
    sol''4 do'' sol'' sol'' |
    sol'' fa''8 mi'' re''4.\trill do''8 |
    do''2
  }
  \tag #'(dessus1 dessus2) {
    <>^"Violons" sol'8\tresdoux la' sol' la' si' do'' re'' si' |
    mi''4 mi''8 si' do''8.( re''16) re''8.(\trill do''32 re'') |
    mi''2 mi''4 mi'' |
    mi'' re''8 do'' si'4.\trill do''8 |
    do''2
  }
>>
<<
  \tag #'(trompette dessus1) {
    \tag #'dessus1 <>^"Violons"
    do''8\fort re'' do'' re'' |
    mi'' fa'' mi'' fa'' sol''2~ |
    sol'' sol''8 la'' sol'' la'' |
    sol'' la'' sol'' fa'' mi'' fa'' mi'' fa'' |
    sol''4 fa''8 mi'' re''4.\trill do''8 |
    do''2
  }
  \tag #'dessus2 {
    <>^\markup\whiteout "Hautbois & violons"
    r2 |
    r si'8\fortSug do'' si' do'' |
    re'' do'' re'' si' mi''2 |
    mi''8 fa'' mi'' re'' do'' re'' do'' re'' |
    mi''4 re''8 do'' si'4.\trill do''8 |
    do''2
  }
>>
<<
  \tag #'trompette { r2 R1*23 }
  \tag #'dessus1 {
    <>^\markup\whiteout "Violons & hautbois"
    sol''8\doux la'' sol'' la'' |
    sol'' la'' sol'' fa'' mi'' fa'' mi'' fa'' |
    sol''4 fa''8 mi'' re''4.\trill do''8 |
    do''2 r |
    R1 |
    r2 <>^\markup\whiteout "Violons" mi''4\fort fa''8 mi'' |
    re'' mi'' re'' do'' si' do'' re'' mi'' |
    do''4 la' r2 |
    r mi''4\doux mi''8 la'' |
    sold''8 fad'' sold'' la'' sold'' la'' si'' sold'' |
    la''4 r r la''8 sold'' |
    la''4 fa''8 mi'' re'' mi'' do'' re'' |
    si' la' si' re'' do''4 do''8 re'' |
    mi''4 mi''8 fa'' si'2 |
    la'2 r4 r8 do'' |
    do''2 sib' |
    la'4. la''8 sib'' la'' sol'' fa'' |
    mi''4 mi'' mi''4. la''8 |
    fad''4. mi''8 fad'' sol'' la'' fad'' |
    sol''8 fad'' sol'' la'' sol''4 sol'' |
    sol'' sol' sol'' sol'' |
    sol''2 fad''4.(\trill mi''16 fad'') |
    sol''2 r |
    R1 |
  }
  \tag #'dessus2 {
    <>^\markup\whiteout "Violons & hautbois"
    r2 |
    mi''8\doux fa'' mi'' re'' do'' re'' do'' re'' |
    mi''4 re''8 do'' si'4.\trill do''8 |
    do''2 r |
    R1 |
    r2 <>^\markup\whiteout "Violons" do''4\fort re''8 do'' |
    si' do'' si' la' sold' la' si' sold' |
    la'4 mi' mi''\doux fa''8 mi'' |
    re''4 mi''8 re'' do'' mi'' re'' do'' |
    si' la' si' do'' si' do'' re'' si' |
    do''4 do''8 re'' mi'' re'' do''\trill si' |
    la'4 la'8 sol' fa'2\trill |
    mi'4 r mi' la'8 sold' |
    la'4 la'8 la' la'4( sold') |
    la'2 r4 r8 la' |
    la'2 sol' |
    fa'4. fa''8 mi'' fa'' mi'' re'' |
    dod''4. si'8 dod'' re'' mi'' dod'' |
    re''2 la'4 la' |
    re''2 re''4. re''8 |
    mi''8 re'' mi'' fa'' mi'' fa'' mi'' re'' |
    do''4. re''8 do'' si' la' si' |
    si'2\trill r |
    R1 |
  }
>>
<<
  \tag #'trompette {
    sol''4\doux sol''8 sol'' sol''4 sol'' |
    do'''2 r |
    R1*4 |
    re''4\doux re''8 re'' re''4 re'' |
    re''2\trill r |
    R1*3 |
    r2
  }
  \tag #'(dessus1 dessus2) {
    <>^\markup\whiteout "Violons" mi''4\doux mi''8 mi'' mi''4 mi'' |
    sol''2 r |
    <<
      \tag #'dessus1 {
        re''8\doux mi'' re'' mi'' fa''2 |
        R1 |
        sol''8\doux la'' sol'' la'' sol'' si'' la'' sol'' |
        fad'' mi'' re'' do'' si'4. si'8 |
        do'' re'' do'' si' la' si' do'' re'' |
        si'2 re''8\fort do'' re'' mi'' |
        re'' do'' re'' mi'' re'' mi'' re'' do'' |
        si'4 sol' r2 |
        re''2\doux re''4 si' |
        sol' mi'
      }
      \tag #'dessus2 {
        si'8\doux do'' si' dod'' re''2 |
        la'8\doux si' la' si' do'' re'' do'' re'' |
        mi'' fa'' mi'' fa'' re''4 do''8 si' |
        la'4 fad' sol'4. sol'8 |
        la' si' la' sol' fad' sol' la' fad' |
        sol'2 r |
        si'8\fort la' si' do'' si' do'' si' la' |
        sol'4 re' r2 |
        si'8\doux la' si' do'' si' do'' re'' si' |
        do''4 sol'
      }
    >>
  }
>>
<<
  \tag #'(trompette dessus1) {
    \tag #'dessus1 <>^"Violons"
    sol''8\fort fa'' sol'' la'' |
    sol'' fa'' sol'' la'' sol'' la'' sol'' fa'' |
    mi''4 do'' r2 |
    sol''8\doux fa'' sol'' la'' sol''4 mi'' |
  }
  \tag #'dessus2 {
    <>^\markup\whiteout "Violons & hautbois" r2 |
    mi''8\fort  re'' mi'' fa'' mi'' fa'' mi'' re'' |
    do''4 sol' r2 |
    mi''8\doux re'' mi'' fa'' mi''4 do'' |
  }
>>
<<
  \tag #'trompette {
    R1 |
    sol''8\doux fa'' sol'' la'' sol'' la'' sol'' fa'' |
    sol''4 do'' sol'' sol'' |
    sol'' fa''8 mi'' re''4.\trill do''8 |
    do''2 r |
    R1*2 |
    sol''8\doux fa'' sol'' la'' sol'' la'' sol'' fa'' |
    sol''4 do''
  }
  \tag #'(dessus1 dessus2) {
    <>^"Violons" sol'8\tresdoux la' sol' la' si' do'' re'' si' |
    mi''4 mi''8 si' do''8.( re''16) re''8.(\trill do''32 re'') |
    mi''2 mi''4 mi'' |
    mi'' re''8 do'' si'4.\trill do''8 |
    do''2 r2 |
    mi'2\tresdoux ~ mi'4.*5/6 fa'32 sol' la' si' do'' re'' |
    re''2( re''4.\trill do''16 re'') |
    mi''4 mi''8 si' do''8.( re''16) re''8.\trill( do''32 re'') |
    mi''2
  }
>>
<<
  \tag #'trompette {
    sol''4 sol'' |
    sol'' fa''8 mi'' re''2\trill |
    do''
  }
  \tag #'(dessus1 dessus2) {
    mi''4 mi'' |
    mi'' re''8 do'' si'4.\trill la'16 si' |
    do''2
  }
>>
