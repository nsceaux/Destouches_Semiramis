\piecePartSpecs
#`((trompette)
   (dessus #:score "score-dessus")
   (basse)
   (basse-continue)
   (chant)
   (silence #:on-the-fly-markup , #{ \markup\tacet#75 #}))
