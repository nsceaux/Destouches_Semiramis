Fil -- le de la Va -- leur, im -- mor -- tel -- le Vic -- toi -- re,
Vo -- le de -- vant nos pas, re -- con -- noi nos Dra -- peaux.
Vo -- le, Vo -- le,
Vo -- le de -- vant nos pas, im -- mor -- tel -- le Vic -- toi -- re.

D’un Roi fa -- meux nous chan -- tons les tra -- vaux.
D’un Roi fa -- meux nous chan -- tons les tra -- vaux.
Par son au -- gus -- te Nom fai bril -- ler __
fai bril -- ler nô -- tre gloi -- re ;
E -- tend nos loix & sa me -- moi -- re :
Nos suc -- cès sont pour lui des tri -- om -- phes nou -- veaux.
Fil -- le de la Va -- leur, im -- mor -- tel -- le Vic -- toi -- re,
Vo -- le de -- vant nos pas, re -- con -- noi nos Dra -- peaux.
Vo -- le, Vo -- le,
Vo -- le de -- vant nos pas, im -- mor -- tel -- le Vic -- toi -- re.
Vo -- le de -- vant nos pas, im -- mor -- tel -- le Vic -- toi -- re.
