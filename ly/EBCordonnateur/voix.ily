\clef "vbasse" <>^\markup\character L'Ordonnateur
do'4 do'8 do' do'4. mi'8 |
do'2 do4 do |
mi2 fa8[ mi] re[ do] |
sol4 sol re8[\melisma mi re mi] |
fa[ sol fa sol] la[ si la si]( |
do'4)\melismaEnd do'8 la si4 do' |
re'2 sol4. sol8 |
sol2 re4. sol8 |
sol2 r |
R1 |
r2 sol8[\melisma fa sol la] |
sol[ fa sol la] sol[ la sol fa]( |
mi4)\melismaEnd do r2 |
R1 |
r2 do'8[ si do' re']( |
do'4) do do8[\melisma re do re] |
mi8[ fa mi fa] sol[ la si sol]( |
do'4)\melismaEnd do'8 sol la4 si |
do'2 mi4 mi |
la si8 do' sol2 |
do r |
R1*7 |
r2 do'4 do'8 do' |
si4 si8 si la4 la8 la |
mi2 r |
R1 |
r2 do'4 do'8 do' |
si4 si8 si la4 la8 la |
mi2 mi4 mi8 mi |
la4. si8 do'4 do'8 re' |
mi'4( re'8\trill)[ do']\melisma si[ do' la si]( |
sold2\trill)\melismaEnd la4. si8 |
do'4 do'8 re' mi'4( mi) |
la2 r4 r8 fa |
fa2 do |
fa sol4 sol8 sol |
la4 la la la |
re'2 re'4 do' |
si2 si4. si8 |
do'8[\melisma si do' re'] do'[ re' do' si]( |
la2\trill)\melismaEnd la4. re'8 |
sol2 r |
do'4 do'8 do' do'4. mi'8 |
do'2 do4 do |
mi2 fa8[ mi] re[ do] |
sol4 sol re8[\melisma mi re mi] |
fa[ sol fa sol] la[ si la si]( |
do'4)\melismaEnd do'8 la si4 do' |
re'2 sol4. sol8 |
sol2 re4. sol8 |
sol2 r |
R1 |
r2 sol8[\melisma fa sol la] |
sol[ fa sol la] sol[ la sol fa]( |
mi4)\melismaEnd do r2 |
R1 |
r2 do'8[ si do' re']( |
do'4) do do8[\melisma re do re] |
mi[ fa mi fa] sol[ la si sol]( |
do'4)\melismaEnd do'8 sol la4 si |
do'2 mi4 mi |
la si8 do' sol2 |
do r |
do2~ do4.*5/6\melisma re32[ mi fa sol la si] |
si2\trill( si4.\trill la16[ si] |
do'4)\melismaEnd do'8 sol la4 si |
do'2 mi4 mi |
la si8 do' sol2 |
do
