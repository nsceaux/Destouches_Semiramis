<<
  %% Arsane
  \tag #'(arsane basse) {
    <<
      \tag #'basse { s1 s4 \arsaneMark }
      \tag #'arsane { \arsaneMark R1 r4 }
    >> re'8 re' mib'4 mib'8 do' |
    la4. re'8 re'4 mi'8 fad' sol'4 sol'8 fad' |
    sol'2 sol'4
  }
  %% Zoroastre
  \tag #'(zoroastre basse) {
    \zoroastreMark r4 sol sib8 sib sib sol |
    re'4
    \tag #'zoroastre { r4 r2 R1. R2. }
  }
>>