\clef "basse"
\setMusic #'rondeau {
  r4 r r fad,4 |
  sol,2 la,4 sib,8 do re4 re, |
  sol,2 r4 r r fad, |
  sol,2 la,4 sib,8 do re4 re, |
  sol,2
}
<>^\markup\whiteout Bassons \keepWithTag #'() \rondeau
<>^\markup\whiteout Tous \rondeau
<>^\markup\whiteout Bassons r4 r r re |
sol2 la4 sib2 la4 |
sol2 r4 r r re |
sol2 la4 sib2 la4 |
sol2 r4 r2*3/2 |
r4 r re mib2 re4 |
do2 sib,4 do2 mib4 |
re do sib, la,2 sol,4 |
re2
<>^\markup\whiteout Tous \rondeau
<>^\markup\whiteout Bassons sol4 si2 do'4 |
sol2 fa4 mib2 re4 |
do2 r4 r r do' |
sol2 fa4 mib2 re4 |
do2 re4 mib2 re4 |
do2 sib,4 fad, sol,2 |
re2 re'4 re'2 dod'4 |
re' re re' re'2 dod'4 |
re' re sib sol la la, |
re2
<>^\markup\whiteout Tous \rondeau
