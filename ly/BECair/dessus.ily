\clef "dessus"
\setMusic #'rondeau {
  fad'4 sol'2 la'4 |
  sib'2 do''4 re''8 mib'' re'' do'' sib' la' |
  sib'4 sol' fad' sol'2 la'4 |
  sib'2 do''4 re''8 mib'' re'' do'' sib' la' |
  sol'2
}
<>^\markup\whiteout Hautbois \keepWithTag #'() \rondeau
<>^\markup\whiteout Tous \rondeau
<>^\markup\whiteout Hautbois re''4 sol''2 la''4 |
sib''2 la''4 sol''2\trill fad''4 |
sol''4 sol'' re'' sol''2 la''4 |
sib''2 la''4 sol''2\trill fad''4 |
sol''2 re''4 mib''2 re''4 |
do''2\trill sib'4 do''2 re''4 |
la'2\trill re''4 mib''8 re'' do'' sib' la' sol' |
fad'4 re' sib' do''2 re''4 |
la'2\trill
<>^\markup\whiteout Tous \rondeau
<>^\markup\whiteout Hautbois sol''4 re''2 mib''4 |
si'2 sol'4 do''2 re''4\trill |
mib''2 sol''4 re''2 mib''4 |
si'2 sol'4 do''2 re''4\trill |
mib''2 re''4 do''2\trill sib'4 |
la'2 re''4 la' sib'2 |
fad'2 re''4 sol''( fa'')\trill mi'' |
fa'' \appoggiatura mi''8 re''4 re'' sol'' la''8 sol'' fa'' mi'' |
fa''4 \appoggiatura mi''8 re''4 sol'' fa'' mi''2\trill |
re''2
<>^\markup\whiteout Tous \rondeau
