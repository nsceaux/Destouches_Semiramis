\set Score.currentBarNumber = 15
\key re \minor \midiTempo#160
\time 6/4 \partial 1
\beginMark "Rondeau" s1 s1.*7 s2 \bar "||"
s1 s1.*7 s2 \bar "||"
\beginMarkSmall "[Rondeau]" s1 s1.*3 s2 \bar "||"
s1 s1.*8 s2 \bar "||"
\beginMarkSmall "[Rondeau]" s1 s1.*3 s2 \bar "|."
