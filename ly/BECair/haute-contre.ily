\clef "haute-contre"
\setMusic #'rondeau {
  r4 r r la'4 |
  sol'2 fad'4 sol' sol' fad' |
  sol'2 re'4 r r la' |
  sol'2 fad'4 sol' sol' fad' |
  sol'2
}
r4 r2*3/2 | R1.*3 | r4 r
\keepWithTag #'() \rondeau
r4 r2*3/2 | R1.*7 | r4 r
\rondeau
r4 r2*3/2 | R1.*8 | r4 r
\rondeau
