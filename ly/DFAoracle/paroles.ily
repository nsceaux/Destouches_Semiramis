Pour a -- pai -- ser mon cou -- roux lé -- gi -- ti -- me,
A -- mes -- tris, c’est trop peu de vœux que tu me fais ;
Au tom -- beau de Ni -- nus va t’of -- frir en vic -- ti -- me
Pour m’as -- su -- rer le sang qu’e -- xi -- gent mes de -- crets.
