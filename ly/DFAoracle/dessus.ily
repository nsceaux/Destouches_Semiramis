\clef "dessus2" <>^"Violons" sib'2 r mib' |
mib'? r re' |
re' r do' |
do' sib la |
sib r r |
r r do''4. sib'8 |
la'2 r sib'4. la'8 |
sol'2 r la'4. sol'8 |
fad'2 r fa'4. sol'8 |
mi'2 r mi'4. fa'8 |
re'2 r sol' |
sol' r fad' |
sol' r re' |
re' r re' |
sol' r do'' |
do'' r si' |
do'' r r |
r r do'' |
do'' r sib' |
la' r la' |
sib'1. |
