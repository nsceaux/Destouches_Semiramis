\clef "vbasse" <>^\markup\character L'Oracle
R1.*5 |
mib'2 mib'4 mib' mib'2~ |
mib' re'2. re'4 |
re'1 do'4 mib' |
la2 la4 r la4. sib8 |
sib2 r la4. la8 |
la1 sol2 |
do'2. do'4 do' la |
\appoggiatura la8 sib2 r sib4 sib |
\appoggiatura do'16 si1 si4 do' |
do'1 do'4 do' |
re'1 re'4 mib' |
mib'1 mib'2 |
do'2 do'4 do' do' do' |
\appoggiatura sol16 fad1 sol2 |
sol2. sol4 sol fad |
sol1. |
