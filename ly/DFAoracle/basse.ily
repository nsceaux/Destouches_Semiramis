\clef "basse" sol,2 r r |
fad,4. mi,8 fa,2 r |
mi,4. re,8 mib,2 r |
re,4. do,8 re,2 re, |
sol, r r |
R1. |
fad4. mi8 fa2 r |
mi4. re8 mib2 r |
re4. do8 re2 r |
dod4. si,8 do2 r |
si,4. la,8 sib,2 r |
la,4. sol,8 la,2 r |
sol,4. fa,8 sol,2 r |
fa,4. mib,8 fa,2 r |
mib,4. re,8 mib,2 r |
re,4. do,8 re,2 r |
do,4. si,,8 do,2 r |
R1. |
re4. do8 re2 r |
re,1. |
sol, |
