\clef "basse" do2. |
fa |
sol2 do4 |
sol,4. sol8 fa mi |
re4. mi8 fad re |
sol fa sol la sol fa |
mi re mi fa mi re |
do4 si, la, sol, fad, sol, |
re,2. |
re |
dod2 do4 |
sib,2. |
do2 re4 |
mib8 do re4 re, |
sol, \clef "alto" sol'8^"notes égales" fa' mi' do' |
fa' mi' re'4 do' |
si8 do' si la sol fa |
\clef "basse" mi4 fa8 mi re do |
sol fa sol la sol fa |
mi re do re mi do |
re mi re do si,4 |
la, re re, |
sol,2. |
r4 r sol |
fa8 mi re mi fa4 |
sol4 la la, |
re8 dod re mi fa sol |
la sib la sol fad4 |
sol8 la sol fa mi4 |
fa sol sol, |
do2 do'4 |
si4. la8 sold1 |
la sol2 |
fa mi |
re1 |
fad |
sol1 fa2 |
mi re |
sol mi1 |
fa2 fad4 sol8 do |
sol,2. |
do~ |
do | \allowPageTurn
fa |
mi4. re8 do4 sol sol,2 |
do2. |
re |
mi |
fa |
mi |
sold |
la4. sol8 la si |
do'2 do4 |
sol2 sol,4 |
do4. re8 mi do |
re4 do si, do re re, |
sol2 fa |
mi re |
do sib, |
la, sol, |
fa, mi, |
re, re |
sol la |
sol4. la8 sol fa mi re |
do4 r r do8 do |
sol la si sol do'4 do |
sol8 la sol fa mi4 do |
re re8 mi fad4 re |
sol fad mi la, |
re re'8 do' si do' si la sol4 sol8 fa |
mi4 do re do |
si, do sol, sol8 fa mi4 sol |
fa8 mi fa re la sol fa4 |
mi mi8 fad sold4 mi |
la4 la8 sol fa sol fa mi |
re4 sol do8 re do si, |
la,4 la8 mi fa4 re |
sol sol8 la si4 sol |
do fa re8 mi fa re |
la4 mi fa mi |
re re8 do sol4 sol, |
do2. r4 | \allowPageTurn
r2 r4 do |
fa2. fa4 |
mi8 fa sol fa mi fa mi re |
do2 dod |
re4 re si,8 la, si, do |
re4 re8 mi fad4 re |
sol sol8 la si4 sol |
mi4 mi8 re do4 do |
fa fa8 mi re4 si, |
mi4 mi8 re do re do si, |
la,4 fa8 mi re do si, la, |
mi4 re mi mi, |
la, la8 sol fad4 re |
sol sol,8 la, si,4 sol, |
do4 do8 re mi4 do |
sol4 sol fa8 sol fa mi |
re mi fa sol la4 fa |
sol fa sol sol, |
do2. |
re2 mi4 |
fa4. mi8 re do |
si,2. |
do1~ |
do2 si, |
la,4 la,8 si, do4 re |
mi4. si,8 la,4 sol, |
re8 mi fad re sol fa mi re |
do2 re4 re, |
sol,1 do2 |
fa1 mi8 fa sol sol, |
do2. |
