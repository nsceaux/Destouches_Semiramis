\tag #'(amestris basse) {
  Rei -- ne, je vais rem -- plir le des -- tin qui m’ap -- pel -- le ;
  A mon ser -- ment vous me ver -- rez fi -- del -- le.
  Tan -- dis que vous sui -- vrez les tra -- ces des He -- ros,
  Dans u -- ne re -- traite __ é -- ter -- nel -- le,
  Mon cœur va cher -- cher son re -- pos.
}
\tag #'(semiramis basse) {
  Au re -- pos de ces lieux vô -- tre cœur s’in -- te -- res -- se,
  Nô -- tre fé -- li -- ci -- té ne dé -- pend que de vous,
  Fil -- le de Ju -- pi -- ter, vous se -- rez sa Prê -- tres -- se,
  Ses fa -- veurs par vos mains vont des -- cen -- dre sur nous.
  Ba -- by -- lonne est l’ob -- jet du cou -- roux qui l’a -- ni -- me,
  Et ce Dieu tant de fois in -- vo -- qué par mes pleurs,
  De -- man -- de dès long- tems u -- ne gran -- de vic -- ti -- me ;
  Je la cher -- chois en vain : vôtre ef -- fort ma -- gna -- ni -- me,
  Sans nous cou -- ter __ du sang, fi -- ni -- ra nos mal -- heurs.
}
\tag #'(amestris basse) {
  J’offre à nos Dieux des jours trop peu di -- gnes d’en -- vi -- e.
}
\tag #'(semiramis basse) {
  Ces Dieux en ont- ils fait de plus heu -- reux pour moi ?
  Aux loix d’un In -- con -- nu je vais être as -- ser -- vi -- e,
  Ar -- sane en ce mo -- ment va de -- ve -- nir mon Roi.
}
\tag #'(amestris basse) {
  Zo -- ro -- astre es -- pe -- roit re -- ce -- voir vô -- tre foi.
}
\tag #'(semiramis basse) {
  A -- mes -- tris, mal -- gré- moi, je lui fais cette of -- fen -- se.
}
\tag #'(amestris basse) {
  Eh ! ne crai -- gnez- vous point les traits de sa ven -- gean -- ce ?
  Zo -- ro -- as -- tre com -- mande à cent peu -- ples di -- vers,
  C’est peu que sa va -- leur ait fait trem -- bler __ la Ter -- re,
  Ce nou -- veau Con -- que -- rant à domp -- té les En -- fers ;
  Les se -- crets de l’O -- lympe à ses yeux sont ou -- verts :
  Sa voix for -- ce les Dieux à lan -- cer __ le Ton -- ner -- re.
  Sa voix for -- ce les Dieux à lan -- cer __ le Ton -- ner -- re.
}
\tag #'(semiramis basse) {
  Ar -- sane est a -- do -- ré du peuple & des sol -- dats,
  Dans l’âge où l’on com -- mence à s’ins -- truire aux com -- bats,
  Il est maî -- tre de la Vic -- toi -- re. __
  J’ai vû tous nos Guer -- riers, sou -- te -- nus par son bras,
  Ra -- ni -- mer leur ar -- deur à l’é -- clat de sa gloi -- re.
  J’ai vû tous nos Guer -- riers, sou -- te -- nus par son bras,
  Ra -- ni -- mer leur ar -- deur à l’é -- clat de sa gloi -- re.
}
\tag #'(amestris basse) {
  Puis -- se- t’il de nos maux ef -- fa -- cer la me -- moi -- re !
}
\tag #'(semiramis basse) {
  Je vais a -- me -- ner vô -- tre Roi :
  Par -- ta -- gez les hon -- neurs de cette au -- gus -- te fê -- te,
  Le sang, qui nous u -- nit, vous en fait u -- ne loi.
  Que du Ban -- deau sa -- cré vos main or -- nent sa Tê -- te.
}
