s2.*2 <7>2.
s4. <6 4>8 <6 4> <6+> s2.*2 <6>2 <6>8 <6>
s4 <6> <6+>2 <5/> \new FiguredBass <_+>2. <6-> <7->2 <6 4+>4

<6>2. <6 5>2 <7 _+>4 s <4>4. <_+>8 s2 <6>4 s <6>2 <6>2\figExtOn <6>4\figExtOff
<6>2 <6>4 s2. <6>2 <6>4 <_+>2 <6>4 <7> <7 _+>2
s2. s2 s8 <4+> <6>4.\figExtOn <6>8\figExtOff s4 <6 5 _-> <5 4> <_+> <"">4.\figExtOn <"">8\figExtOff <6>4
s2 <5/>4 s2 <6>4 <6> <7>2 s2. <6+>2 <6 5/>1 <_+>8*7\figExtOn <_+>8\figExtOff s2 <6>
<7 5/>4 <6+> <_+>1 <6 5/>

<"">1\figExtOn <"">2\figExtOff <6> <7> s
<6> <5/> s <5/> s2.*2
s2. <6 4 2> <6>4. <6> <4>4 <3>2 s2. <9 7>4 <8 6>2
<7 _+>2 <6 4>4 <7>2 <6>4 <_+>2. <5/> s
s2.*2 s2 <6>4 <_+>\figExtOn <_+>\figExtOff <6> <6 5> <5 4> <3>
<"">2\figExtOn <"">\figExtOff <6> <7>4 <6> <"">2\figExtOn

<""> <_+> <_+>\figExtOff <6> <7>4 <6+> s2
s s <7>4 <6+> s1
s1 <"">4.\figExtOn <"">8\figExtOff s2 s2
<6>4 <6> <_+>4.\figExtOn <_+>8\figExtOff s2 s4 <6>4 <7> <7 _+> <_+>2 <6>
s2 <6>4 <6> <_+>\figExtOn <_+>\figExtOff <6>1 <6+>4\figExtOn <6+>\figExtOff
<6>4\figExtOn <6>8\figExtOff <6> s4 <7>8 <6> <_+>4.\figExtOn <_+>8\figExtOff s2 <_+> <6>4. <6+>8

s4 <7> s2 s4. <6>8 s2 <"">4.\figExtOn <"">8\figExtOff <6>2 s1
s4 <6>2 <6>4 <7> <6> <5 4>4 <3> s1*2
<6 4>2.\figExtOn <6>4\figExtOff <6>4\figExtOn <6>8\figExtOff <4> <6>4. <6>8 s2
<6 5/> <_+>2 <6> <5 4> <6> <"">2\figExtOn <"">\figExtOff
<6>1 s4 <6> <6>2 <_+> <6>4. <6+>8
s4 <6>8 <6+> s <4+> <6+>4 <6 4>2 <_+> s <6> <"">2\figExtOn <"">\figExtOff

s2 <6> s4. <4+>8 <6>4. <6+>8 s2
s4 <6> <4>2 <3> s2. <7>2 <5/>4 s2 <6>4
<5/>2. s1. <6+>2 s4. <5/>8
<5>4 <6> s4. <6>8 <6+>2 <_+>4.\figExtOn <_+>8\figExtOff s4 <6>8 <6>
s2 <7 _+> s1.
s2 <4+> <6>