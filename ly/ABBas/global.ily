\key do \major
\digitTime\time 3/4 \midiTempo#100 s2.*7
\time 3/2 \midiTempo#200 \grace s8 s1.
\digitTime\time 3/4 \midiTempo#100 s2.*23
\time 3/2 \midiTempo#120 s1.*2
\time 2/2 s1*3
\time 3/2 s1.
\time 2/2 s1
\time 3/2 s1.
\time 2/2 s1
\digitTime\time 3/4 \midiTempo#120 \grace s8 s2.*16
\time 3/2 \midiTempo#120 s1.
\time 2/2 s1*13
\time 3/2 s1.
\time 2/2 s1
\time 3/2 s1.
\time 2/2 s1*28
\digitTime\time 3/4 \midiTempo#120 s2.*4
\digitTime\time 2/2 \midiTempo#120 s1*6
\time 3/2 s1.*2
\digitTime\time 3/4 \midiTempo#120 \grace s8 s2.
