<<
  %% Amestris
  \tag #'(amestris basse) {
    \amestrisMark sol'4 sol'8 r16 sol' sol'8 la' |
    la'2 si'8 do'' |
    si'2\trill si'8 do'' |
    \appoggiatura do''8 re''4 re''8 do'' re'' mi'' |
    \appoggiatura mi'' fa'' fa'' fa'' mi'' re'' do'' |
    si'4\trill si'8 r si'4 |
    do''4. do''8 do'' re'' |
    \appoggiatura re''8 mi''4 re'' do'' si' la' si' |
    fad'2.\trill |
    sib'4. sib'8 sib' sib' |
    sib'4.( la'8) la' la' |
    re'4 re' re'' |
    la'2 la'8 sib' |
    sol'2\trill sol'8 fad' |
    sol'2 <<
      \tag #'basse { s4 s2.*16 s1.*2 s1*3 s1. s1 s1. s1 s2. \amestrisMark }
      \tag #'amestris { r4 | R2.*16 R1.*2 R1*3 R1. R1 R1. R1 R2. }
    >>
    mi''2 fa''8 sol'' |
    \appoggiatura fa''8 mi''2 \appoggiatura re''8 do''4 |
    si'2\trill re''8 mi'' |
    do''4.\trill\prall si'8 do''4 |
    mi''4( re''2)\trill |
    do''2 <<
      \tag #'basse { s4 s2.*9 s1. s4 \amestrisMark }
      \tag #'amestris { r4 | R2.*9 R1. r4 }
    >> re''8 re'' si'4 si'8 sol' |
    do''4 do''8 do'' re''4 mi''8 fa'' |
    mi''2\trill <<
      \tag #'basse { s2 s1*2 s2 \amestrisMark }
      \tag #'amestris { r2 | R1*2 | r2 }
    >> fa''4 fa''16 mi'' re'' do'' |
    si'4.\trill si'8 do'' do'' do'' la' |
    re''2 re''4 r |
    r4 sol'8^"mesuré" sol' do''4 do''8 do'' |
    \appoggiatura do''8 re''4 re''8 re'' mi''4\trill mi''8 fa'' |
    re''4.\trill si'8 do'' do'' do'' mi'' |
    la'4. la'8 la'4 re'' |
    si'8.[ do''16 re'' do'' si' la']\melisma sol'4.\melismaEnd la'8 |
    fad'4 fad' r re''8 re'' si'4\trill si'8 sol' |
    do''4 mi''8 do'' la'4 la'8 re'' |
    sol'2 r4 si'8. do''16 dod''4 dod''8 re'' |
    re''4 la'8 si' do''4 do''8 re'' |
    \appoggiatura re''8 mi''2. mi''4 |
    dod''2\trill la'4 re''8 mi'' |
    fa''2 mi''4 sol'' |
    do''16[ re'' do'' re'' mi'' re'' do'' si']( la'4) la'8 fa'' |
    re''4\trill re''8 re'' sol''2 |
    mi''4 do''8 la' fa''2 |
    mi''4 do'' la'16[ sib' la' sib' do'' sib' la' sol']\melisma |
    fa'4\melismaEnd fa''8 mi'' re''2\trill |
    do''2. <<
      \tag #'basse { s4 | s1*18 | s2 \amestrisMark }
      \tag #'amestris { r4 | R1*18 | r4 r }
    >> do''8 do'' |
    do''2 sib'8 do'' |
    \appoggiatura sib'8 la'2 sib'8 do'' |
    fa'2 fa'8 mi' |
    mi'2 mi'4
    \tag #'amestris { r4 | R1*5 R1*2 R2. }
  }
  %% Semiramis
  \tag #'(semiramis basse) {
    <<
      \tag #'basse { s2.*7 s1. s2.*6 s2 \semiramisMark }
      \tag #'semiramis { \clef "vbas-dessus" R2.*7 R1. R2.*6 | r4 r }
    >> do''8 si' |
    la'4 si' do'' |
    sol'2 sol'8 sol' |
    do''2 re''8 mi'' |
    re''2\trill re''4 |
    do'' do''8 do'' do''\trill si' |
    la'2\trill re''8 mi'' |
    do''2\trill\prall si'8 do'' |
    si'2.\trill |
    sol'4 sol'8 sol' sol' sol' |
    re''2 re''8 mi'' |
    mi''2\trill re''8 mi'' |
    \appoggiatura mi''8 fa''4 re'' re''8. mi''16 |
    \appoggiatura re''8 do''4 do'' si'16[ la'8 si'16] |
    si'2\trill do''8 mi'' |
    re''4.\trill do''8 si'4 |
    \appoggiatura si'8 do''2 mi''8 mi'' |
    sold'4\trill sold'8 la' si'4 si'8 do'' re''4 re''8 mi'' |
    dod''4\trill dod'' r mi''8 mi'' mi''4 mi''8 mi'' |
    la'4 la'8 sib' \appoggiatura la'8 sol'4 sol'8 la' |
    fad'4.\trill la'8 la' la' la' la' |
    re''4 la'8 si' do''4 do''8 re'' |
    si'2\trill si'4 r8 si' si' si' do'' re'' |
    sol'4 do''8 do'' fa''4 fa''8 fa'' |
    re''4\trill re'' do'' do''8 sol' sib'4( la'8) sib' |
    la'4\trill re''8 mi'' do''4\trill\prall si'8 do'' |
    \appoggiatura do''8 re''2. |
    <<
      \tag #'basse { s2.*5 s2 \semiramisMark }
      \tag #'semiramis { R2.*5 | r4 r }
    >> mi''4 |
    mi''4. re''8 re'' re'' |
    si'4.\trill si'8 do'' si' |
    la'2( sold'8) la' |
    sold'2\trill mi''4 |
    si'4. si'8 si' do'' |
    do''2 do''8 re'' |
    mi''4. re''8 do''4 |
    \appoggiatura do''8 re''4 re''8 r si'4 |
    mi''4. re''8 do'' si' |
    la'2\trill re''4 do''8 si' la'4\trill si' |
    sol'4 <<
      \tag #'basse { s2. s1 s2 \semiramisMark }
      \tag #'semiramis { r4 r2 | R1 | r2 }
    >> r4 mi''8 mi'' |
    dod''2\trill dod''4. mi''8 |
    la'4 la'8 sib' sol'4\trill fa'8 mi' |
    \appoggiatura mi'8 fa'4 fa' <<
      \tag #'basse { s2 s1*7 s1. s1 s1. s1*9 s2. \semiramisMark }
      \tag #'semiramis { r2 | R1*7 R1. R1 R1. R1*9 | r2 r4 }
    >> mi''4 |
    mi''2 mi''4 fa''8 sol'' |
    re''4. re''8 si'\trill si' do'' re'' |
    sol'4. si'8 do'' do'' do'' re'' |
    \appoggiatura re''8 mi''4 mi''8 do'' la'4\trill la'8 si' |
    fad'2 re''4 re'' |
    la'\trill la' re'' do''8 re'' |
    si'4. la'8( sol'4) r8 si' |
    do''2 mi''4 mi''8 do'' |
    la'4\trill re''8 do'' si'4\trill do''8 re'' |
    sold'4 mi'8 mi' la'4 la'8 si' |
    \appoggiatura si'8 do''4 re''8 mi'' fa''4 re''8 do'' |
    do''2( si')\trill |
    la'2 r4 r8 re'' |
    si'4. la'8 sol'4 fa' |
    mi'4 mi'8 fa' sol'4 sol'8 do'' |
    si'4 si'8 dod'' re''4 re''8 mi'' |
    \appoggiatura mi''8 fa''4 la'8 si' do''4 re''8 mi'' |
    mi''2( re'')\trill |
    do''2 <<
      \tag #'basse { s4 s2.*3 s2. \semiramisMark }
      \tag #'semiramis { r4 | R2.*3 | r2 r4 }
    >> r8 sol' |
    do''4 do''8 do'' sold'4 sold'8 la' |
    la'4 do''8 re'' mi''4 re''8 do'' |
    si' si' do'' re'' do''4.\trill si'8 |
    la'4 la'8 re'' si' si' do'' re'' |
    \appoggiatura re''8 mi''4 do''8 la' fad'4 fad'8 sol' |
    sol'2 re''4 re''8 mi'' mi''4.\trill re''8 |
    do''4. la'8 si'2 do''4 do''8 si' |
    \appoggiatura si'8 do''2 do''4 |
  }
>>