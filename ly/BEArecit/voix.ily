\zoroastreMarkText "Gravement" fa2 fa4 sib |
re2 r4 <>^\markup\italic { Plus vivement } re'8 re' |
sol2 r4 do'8 do' |
fad4 re8 mi fad4 sol8 la |
\appoggiatura la8 sib4. sol8 do' do' sol do' |
la2 r4 r8 la sib la sol fa |
mi4.\trill la8 la2 r8 mi fad sol |
fad4. mi8( re4) r4 |
sib2 sib,8 do re mib |
fa4. fa8 sol sol la sib |
la4\trill la8 fa sib4 r8 sol |
mib'4. re'8 do'[ sib] la[ sol] |
re'4 la8 sib fad4 fad8 la |
re4. re8 sib4. sol8 |
mib'4. re'8 do'[ sib] la[ sol] |
re'2. sol8 mi fad4\trill fad8 sol |
sol2. |
