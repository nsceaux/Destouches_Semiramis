\clef "basse" sib,1~ |
sib, |
mib2 do |
re~ re8 do sib, la, |
sol,2 mi, |
fa,1 sol,2 |
la, dod1 |
re2~ re4.*5/6 mib16 re do |
sib,1 |
la,2 sol, |
fa,4 fa re mib |
do1~ |
do |
sib,2 sol, |
do1 |
do2. sib,8 do re4 re, |
sol,2. |
