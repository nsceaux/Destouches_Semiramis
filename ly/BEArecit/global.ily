\key re \minor
\time 2/2 \midiTempo#160 s1*5
\time 3/2 s1.*2
\digitTime\time 2/2 s1*8
\time 3/2 s1.
\digitTime\time 3/4 s2. \bar "|."
