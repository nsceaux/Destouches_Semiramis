Qu’ai-je en -- ten -- du ? quel soup -- çon ! quel ef -- froi
Dans mon cœur a -- gi -- té, s’é -- le -- ve mal -- gré moi !
Je veux les é -- clair -- cir... A -- mour, soy -- ez mon gui -- de :
Mais si je n’ai brû -- lé que pour u -- ne per -- fi -- de,
Fu -- reur, Fu -- reur, pour me van -- ger je n’é -- cou -- te que toi.
Fu -- reur, Fu -- reur, pour me van -- ger je n’é -- cou -- te que toi.
