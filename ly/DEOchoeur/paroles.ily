Quel bruit af -- freux nous fait trem -- bler !
Quel bruit af -- freux nous fait trem -- bler !
Sous nos pas chan -- ce -- lans se dé -- ro -- be la Ter -- re,
Le Dieu me -- na -- ce, il s’ar -- me, il lan -- ce le Ton -- ner -- re,
\tag #'(vdessus vtaille) { il lan -- ce, }
\tag #'vhaute-contre { il lan -- ce le Ton -- ner -- re, }
il lan -- ce le Ton -- ner -- re,
\tag #'(vdessus vtaille vbasse) { E -- cou -- tez, fre -- mis -- sez, }
E -- cou -- tez, fre -- mis -- sez, il est prêt à par -- ler.
