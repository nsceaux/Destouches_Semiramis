\clef "taille" re'2 r |
r4 sol' sol' sol' |
mib'2 mib' |
re'4( re') re'( re') |
re'( re') re'( re') |
mib'( mib') mib'( mib') |
re'( re') re'( re') |
re'4 r r2 |
R1 |
re'4( re') re'( re') |
re'( re') mib'( re') |
re'( mib') mib' do' |
sib( sib) sib( sib) |
sib( sib) fa'( fa') |
mib'( mib') mib'( mib') |
sol' mib' mib'( mib') |
re' re' re' re' |
re' sib do'( do') |
do'( do') re' re' |
sib( sib) sol( sol) |
lab( lab) lab( sol) |
sol sol' sol'( sol') |
mi'( mi') mi'( mi') |
la do' do'( do') |
re' la' la'( la') |
sol'( sol') sol'( sol') |
sol' sol' sol' mi' |
mi'( mi') re'( re') |
re'( re') re' mi' |
dod'8 re' dod' re' mi' re' dod' mi' |
re'4( re') re' re' |
re'4. re'8 re'4. dod'8 |
re'4 fad' la' fad' |
fad'?( fad') fad'( fad') |
sol'4 r re'4. mib'8 |
do'2 do'4. re'8 |
sib2 do'4. do'8 |
do'2 re'4. sol8 |
sib2 la4 sol |
sol2 sol4 fad |
\custosNote sol4
