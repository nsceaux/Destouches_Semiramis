\clef "haute-contre" sib'2 r |
r4 re'' re'' re'' |
do''4. do''8 do''4. sib'8 |
la'4( la') la'( la') |
sol'( sol') la'( la') |
sib'( sib') la'( la') |
fad' sol' sol' fad' |
sol' r r2 |
R1 |
fad'4( fad') fad'( fad') |
sol'( sol') la' fad' |
sol'( sol') sol'( sol') |
fa'( fa') sol'( sol') |
re'( re') sib'( sib') |
sib'( sib') sib'( sib') |
do''( do'') do''( do'') |
la'( la') la'( la') |
sol'( sol') sol'( sol') |
la'( la') lab'( lab') |
sol'( sol') sol'( sol') |
fa'( fa') fa'( fa') |
mi' do'' do''( do'') |
sol'( sol') sol'( mi') |
fa' la' la'( la') |
la' re'' re''( re'') |
sol' si' si'( si') |
mi'' mi'' mi'' mi'' |
la'( la') la'( la') |
fa'( fa') fa' sib' |
la'4. la'8 la'4. la'8 |
la'4( la') la'( la') |
la'4. la'8 la'4. la'8 |
la'4( la') la'( la') |
la'( la') la'( la') |
sib'4 r r2 |
R1 |
r2 sol'4. sol'8 |
la'2 la'4. sib'8 |
sol'2 re'4 re' |
mib'2 mib'4 re' |
\custosNote re'4
