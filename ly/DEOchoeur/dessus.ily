\clef "dessus" <>^"Tous" r4 re''8 mi'' fad'' sol'' la'' fad'' |
sib''2~ sib''8 sib'' la'' sib'' |
sol''4. la''16 sib'' la''4.\trill sol''8 |
fad'' sol'' fad'' mi'' re'' do'' sib' la' |
sib'4 sib'8 do'' re'' mib'' fa'' re'' |
sol'' lab'' sol'' fa'' mib'' re'' do'' sib' |
la' la' sib' do'' re'' do'' sib' la' |
sol'4 r r2 |
R1 |
r8 la'' la'' la'' fad'' fad'' fad'' la'' |
re'' re'' re'' re'' do'' do'' do'' re'' |
sib'4( sib') do''( do'') |
re''( re'') mib''( mib'') |
fa''8 fa'' fa'' fa'' re'' re'' re'' fa'' |
sib' sib'' sib'' sib'' sol'' sol'' sol'' sib'' |
mib'' sol'' la'' sib'' do''' sib'' la'' sol'' |
fad''4 re''8 mi'' fad'' sol'' la'' fad'' |
sib''4 sol''8 fa'' mib'' re'' do'' sib' |
la' sib' do'' la' re'' mi'' fa'' re'' |
sol''4 sib''8 la'' sol'' fa'' mib'' do'' |
fa''4. fa''8 fa''4. mi''16 re'' |
mi''!4 do''8 re'' mi'' fa'' sol'' la'' |
sib''4. sib''8 sib'' sol'' do''' do''' |
do'''2 r8 do'' re'' mi'' |
fa'' mi'' fa'' sol'' fa'' sol'' fa'' mi'' |
re''2 r8 re'' mi'' fa'' |
sol'' fa'' sol'' la'' sol'' la'' sol'' fa'' |
mi''4.*13/12 mi''32 fa'' sol'' la''4. la''8 |
la''4. sol''16 fa'' sol''4. sol''8 |
sol'' la'' sol'' fa'' mi'' fa'' sol'' la'' |
fa''4 re'' re''4. re''8 |
mi''8 fa'' mi'' fa'' sol'' la'' sol'' la'' |
fad''4 re''8 mi'' fad'' sol'' la'' sib'' |
do'''4 do'''8 sib'' la'' sol'' fad'' mi'' |
re''4 r sol''4. sol''8 |
sol''2 fa''4. fa''8 |
fa''2 mib''4. mib''8 |
mib''2 re''4. re''8 |
re''2 do''4 sib' |
la'2 la'4 la' |
\custosNote sib'4
