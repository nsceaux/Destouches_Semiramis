\clef "basse" sol,2 r |
r4 sol,8 la, sib, do re sib, |
mib2 do |
re4( re) fad( fad) |
sol( sol) fa( fa) |
mib( mib) do( do) |
re sol, re, re |
sol la sib do' |
re'( re') sol( sol) |
re( re) do( do) |
sib,( sib,) la, re |
sol( sol) la( la) |
sib( sib) mib( mib) |
sib,( sib,) lab,( lab,) |
sol,( sol,) mib( mib) |
do( do) do( do) |
re( re) re( re) |
sol( sol) sol( sol) |
fad( fad) fa( fa) |
mi( mi) mib( mib) |
re( re) re sol |
do( do) do( do) |
do( do) do( do) |
fa( fa) fa( fa) |
re( re) re( re) |
sol( sol) sol( sol) |
mi( mi) mi( mi) |
la8 sib la sol fa mi fa re |
sib4. sib8 sib4. sol8 |
la4 la, la, la |
re'8 do' sib la sol fa mi re |
la4. la8 la4. la,8 |
re4( re) re( re) |
do( do) do( do) |
sib, r r2 |
la, r |
sol, r |
fa, r |
mi, fad,4 sol, |
do2 do4 re |
\custosNote sol,
