\clef "vtaille" R1*6 |
r2 r4 re' |
re' fad' sol' sol' |
re'2 re' |
re'1 |
r2 r4 re' |
re' mib' mib' do' |
sib2 sib |
sib1 |
R1*3 |
sib2 do' |
do'2 re'4. re'8 |
sib2 sol4 sol |
lab2 lab4 sol |
sol2 sol4 r |
r4 r8 do' do'4. do'8 |
do'2 do'4 r |
r2 r4 re' |
re'2 re'4 r |
r2 r4 mi' |
mi'4. mi'8 re'4. re'8 |
re'2 re'4 mi' |
dod'8[\melisma re' dod' re'] mi'[ re' dod' mi']( |
re'2)\melismaEnd re'4 re' |
re'4. re'8 re'4. dod'8 |
re'2 re'4 r |
R1 |
r2 re'4. mib'8 |
do'2 do'4. re'8 |
sib2 do'4. do'8 |
do'2 re'4. sol8 |
sib2 la4 sol |
sol2 sol4 fad |
sol1 |
