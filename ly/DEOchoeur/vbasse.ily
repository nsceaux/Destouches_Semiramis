\clef "vbasse" R1*6 |
r2 r4 re |
sol la sib do' |
re'2 sol |
re1 |
r2 r4 re |
sol sol la la |
sib2 mib |
sib,1 |
R1*3 |
sol2 sol |
fad fa4. fa8 |
mi2 mib4 mib |
re2 re4 sol |
do2 do4 r |
r4 r8 do mi4. do8 |
fa2 fa4 r |
r2 r4 re |
sol2 sol4 r |
r2 r4 mi |
la8[\melisma sib la sol] fa[ mi fa re]( |
sib4.)\melismaEnd sib8 sib4. sol8 |
la2 la4 la |
re'8[\melisma do' sib la] sol[ fa mi re]( |
la4.)\melismaEnd la8 la4. la,8 |
re2 re4 r |
R1 |
r2 sib4. sib8 |
\appoggiatura sib16 la2 la4. sib8 |
\appoggiatura la16 sol2 sol4. sol8 |
\appoggiatura sol16 fa2 fa4. sol8 |
\appoggiatura fa8 mi2 fad4 sol |
do2 do4 re |
sol,1 |
