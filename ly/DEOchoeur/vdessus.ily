\clef "vdessus" R1*6 |
r2 r4 la' |
sib' do'' re'' mi'' |
fad''2 sol'' |
la''1 |
r2 r4 la' |
sib' sib' do'' do'' |
re''2 mib'' |
fa''1 |
R1*3 |
re''2 mib'' |
mib'' re''4. re''8 |
re''2 do''4 do'' |
do''2 do''4 si' |
do''2 do''4 r |
r4 r8 sol' sol'4. do''8 |
la'2 la'4 r |
r2 r4 la' |
si'2 si'4 r |
r2 r4 si' |
dod''4. dod''8 re''4. fa''8 |
re''2\trill re''4 sol'' |
mi''8[\melisma fa'' mi'' re''] dod''[ re'' mi'' dod'']( |
fa''2)\melismaEnd fa''4 fa'' |
mi''4. mi''8 mi''4. la''8 |
fad''2 fad''4 r |
R1 |
r2 sol''4. sol''8 |
sol''2 fa''4. fa''8 |
fa''2 mib''4. mib''8 |
mib''2 re''4. re''8 |
re''2 do''4 sib' |
la'2 la'4 la' |
sib'1 |
\bar "|."
