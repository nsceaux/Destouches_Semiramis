\clef "vhaute-contre" R1*6 |
r2 r4 fad' |
sol' do'' sib' sib' |
la'2 sib' |
fad'1 |
r2 r4 fad' |
sol' sol' sol' sol' |
fa'2 sol' |
re'1 |
R1*3 |
sol'2 sol' |
la'2 lab'4. lab'8 |
sol'2 sol'4 sol' |
fa'2 fa'4 fa' |
mi'2 mi'4 r |
r4 r8 mi' sol'4. mi'8 |
fa'2 fa'4 r |
r2 r4 fa' |
sol'2 sol'4 r |
r2 r4 sol' |
la'4. la'8 la'4. la'8 |
fa'2 fa'4 sib' |
la'4. la'8 la'4. la'8 |
la'2 la'4 la' |
la'4. la'8 la'4. la'8 |
la'2 la'4 r |
R1*3 |
r2 sol'4. sol'8 |
la'2 la'4. sib'8 |
sol'2 re'4 re' |
mib'2 mib'4 re' |
re'1 |
